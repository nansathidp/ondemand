from fabric.api import env, run, local, hosts, settings, abort, cd, prefix
from fabric.contrib import files

# sudo apt-get install fabric
# fab deploy:domain=aisondemand2.conicle.com

USER = 'conicle'
HOST = '139.59.238.171'

CODE_DIR = None
ENV_DIR = None
UWSGI_DIR = '/home/conicle/uwsgi'

CONFIG = {
    'uat2-cimb.conicle.com': {
        'is_docker': True,
        'user': 'conicle',
        'host': '103.246.16.187',
        'deploy_path': '/var/app/conicle/docker/uat2-cimb.conicle.com/script/deploy.sh'
    },
    'mcdonalds.conicle.com': {
        'user': 'conicle',
        'host': '139.59.238.171',
        'project': 'mcdonalds.conicle.com',
        'repo': 'git@bitbucket.org:adiinz/git-ondemand.git',
        'branch': 'develop',
        'code_path': '/home/conicle/django',
        'env': '/home/conicle/env',
    },    
    'ondemand.conicle.com': {
        'user': 'conicle',
        'host': '103.246.16.187',
        'project': 'ondemand2.conicle.com',
        'repo': 'git@bitbucket.org:adiinz/git-ondemand.git',
        'branch': 'master',
        'code_path': '/var/app/conicle/django',
        'env': '/var/app/conicle/env',        
    },
    'dev.ondemand.conicle.com': {
        'user': 'conicle',
        'host': '139.59.238.171',
        'project': 'dev.ondemand.conicle.com',
        'repo': 'git@bitbucket.org:adiinz/git-ondemand.git',
        'branch': 'develop',
        'code_path': '/home/conicle/django',
        'env': '/home/conicle/env',        
    },
    'aisondemand.conicle.com': {
        'user': 'conicle',
        'host': '103.246.16.187',
        'project': 'aisondemand2.conicle.com',
        'repo': 'git@bitbucket.org:adiinz/git-ondemand.git',
        'branch': 'release-ais',
        'code_path': '/var/app/conicle/django',
        'env': '/var/app/conicle/env',
        'is_rqscheduler': True,
    },
    'kiatnakin.conicle.com': {
        'user': 'conicle',
        'host': '139.59.238.171',
        'project': 'kiatnakin.conicle.com',
        'repo': 'git@bitbucket.org:adiinz/git-ondemand.git',
        'branch': 'develop',
        'code_path': '/home/conicle/django',
        'env': '/home/conicle/env',
    },
    'cimb.conicle.com': {
        'user': 'conicle',
        'host': '139.59.238.171',
        'project': 'cimb.conicle.com',
        'repo': 'git@bitbucket.org:adiinz/git-ondemand.git',
        'branch': 'develop',
        'code_path': '/home/conicle/django',
        'env': '/home/conicle/env',
    },
    't-learning.conicle.com': {
        'user': 'conicle',
        'host': '139.59.238.171',
        'project': 't-learning.conicle.com',
        'repo': 'git@bitbucket.org:adiinz/git-ondemand.git',
        'branch': 'develop',
        'code_path': '/home/conicle/django',
        'env': '/home/conicle/env',
    },
    'kingpower.conicle.com': {
        'user': 'conicle',
        'host': '139.59.238.171',
        'project': 'kingpower.conicle.com',
        'repo': 'git@bitbucket.org:adiinz/git-ondemand.git',
        'branch': 'develop',
        'code_path': '/home/conicle/django',
        'env': '/home/conicle/env',
    },
}


def deploy(domain=None, pip=False):
    if domain is None or not domain in CONFIG:
        print('fab deploy:domain=xxx')
        print('domain -> ', CONFIG.keys())
        exit()

    global USER
    global HOST
    global CODE_DIR
    global ENV_DIR
    
    config = CONFIG[domain]
    USER = config['user']
    HOST = config['host']
    with settings(user=USER, host_string=HOST):
        if 'is_docker' in config and config['is_docker']:
            run(config['deploy_path'])
            exit()
        code_git(config['code_path'],
                 config['project'],
                 config['repo'],
                 config['branch'])
        CODE_DIR = config['code_path']
        ENV_DIR = config['env']
        print('Pip Update =>', pip)
        if pip:
            env_requirements(config['project'])
        django_migrate(domain=config['project'])
        django_collectstatic(domain=config['project'])
        uwsgi_touch(domain=config['project'])
        
        run('supervisorctl restart %s-rqworker'%config['project'])
        if 'is_rqscheduler' in config and config['is_rqscheduler']:         
            run('supervisorctl restart %s-rqscheduler'%config['project'])
        
def code_git(path, domain, repo, branch):
    with settings(user=USER, host_string=HOST):
        with cd(path):
            if files.exists(domain):
                print('Found. do pull...')
                with cd(domain):
                    run('git checkout %s'%branch)
                    run('git pull origin %s'%branch)
                    run('git submodule update --recursive --remote --init')
            else:
                print('Not Found. do clone...')
                run('git clone %s %s'%(repo, domain))
                with cd(domain):
                    run('git checkout %s'%branch)
                    run('git submodule update --recursive --remote --init')
        
def django_migrate(domain=None):
    if domain is None:
        print('fab django_migrate:domain=xxx')
        exit()

    with settings(user=USER, host_string=HOST):
        with cd('%s/%s'%(CODE_DIR, domain)), prefix('source %s/%s/bin/activate'%(ENV_DIR, domain)):
            run('./manage.py migrate')

def django_collectstatic(domain=None):
    if domain is None:
        print('fab django_collectstatic:domain=xxx')
        exit()

    with settings(user=USER, host_string=HOST):
        with cd('%s/%s'%(CODE_DIR, domain)), prefix('source %s/%s/bin/activate'%(ENV_DIR, domain)):
            run('./manage.py collectstatic --noinput')

def uwsgi_touch(domain=None):
    if domain is None:
        print('fab uwsgi_touch:domain=xxx')
        exit()

    with settings(user=USER, host_string=HOST):
        with cd('%s/%s'%(UWSGI_DIR, domain)):
            run('touch uwsgi.touch')
            
def env_requirements(domain=None):
    if domain is None:
        print('fab env_requirements:domain=xxx')
        exit()

    with settings(user=USER, host_string=HOST):
        with cd('%s/%s'%(CODE_DIR, domain)), prefix('source %s/%s/bin/activate'%(ENV_DIR, domain)):
            run('pip install -r requirements.txt')
