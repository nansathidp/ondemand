from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from utils.cached.time_out import get_time_out

from .models import Account, Access, Level


# Level
def cached_access_level(is_force=False):
    key = '%s_access_level' % settings.CACHED_PREFIX
    result = None if is_force else cache.get(key)
    if result is None:
        result = Level.objects.all().order_by('level')
        cache.set(key, result, get_time_out())
    return None if result == -1 else result


# Account
def cached_access_account_permission(account_id, is_force=False):
    key = '%s_account_permission_level_%s' % (settings.CACHED_PREFIX, account_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Account.objects.select_related('level').get(account_id=account_id)
        except ObjectDoesNotExist:
            result = -1
        cache.set(key, result, get_time_out())
    return None if result == -1 else result


def cached_access_account_permission_delete(account_id):
    key = '%s_access_account_permission_%s' % (settings.CACHED_PREFIX, account_id)
    cache.delete(key)


# Content Permission
def cached_access_permission(content_id, content_type_id, is_force=False):
    key = '%s_access_permission_%s_%s' % (settings.CACHED_PREFIX, content_id, content_type_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Access.objects.get(content=content_id, content_type_id=content_type_id)
        except ObjectDoesNotExist:
            result = -1
        cache.set(key, result, get_time_out())
    return None if result == -1 else result


def cached_access_permission_delete(content_id, content_type_id):
    key = '%s_access_permission_%s_%s' % (settings.CACHED_PREFIX, content_id, content_type_id)
    cache.delete(key)
