from django.contrib import admin

from utils.cached.content_type import cached_content_type_admin_item_inline
from .models import Account as AccessAccount, Access, Level
from account.models import Account


@admin.register(Level)
class AccessAdmin(admin.ModelAdmin):
    list_display = ('name', 'level')


@admin.register(AccessAccount)
class UserPermissionAdmin(admin.ModelAdmin):
    list_display = ('account', 'level')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "account":
            kwargs["queryset"] = Account.objects.filter(id=request.user.id)
        return super(UserPermissionAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Access)
class ContentPermissionAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'content', 'level')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'content_type':
            kwargs["queryset"] = cached_content_type_admin_item_inline()
        return super(ContentPermissionAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
