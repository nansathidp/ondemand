from django.db import models
from account.models import Account


class Level(models.Model):
    name = models.CharField(max_length=32)
    level = models.IntegerField(default=0)

    def __str__(self):
        return str(self.name)

    def save(self, *args, **kwargs):
        from .cached import cached_access_level
        cached_access_level(is_force=True)
        super(self.__class__, self).save(*args, **kwargs)

    def api_display(self):
        return {'name': self.name,
                'level': self.level}

    def get_access_level(self):
        from .cached import cached_access_level
        access_level_list = []
        for level in cached_access_level():
            if level.level >= self.level:
                access_level_list.append(level)
        return access_level_list


class Account(models.Model):
    account = models.ForeignKey(Account, related_name='account_account_related')
    level = models.ForeignKey(Level)

    def save(self, *args, **kwargs):
        from .cached import cached_access_account_permission_delete
        cached_access_account_permission_delete(account_id=self.account_id)
        super(self.__class__, self).save(*args, **kwargs)

    @staticmethod
    def push_debug(account, level_id, access_name):
        access = Level.objects.filter(level=level_id).first()
        if access is None:
            access = Level.objects.create(name=access_name.lower(), level=level_id)
        access_account = Account.objects.filter(account=account).first()
        if access_account is None:
            Account.objects.create(account=account, level=access)
        elif access_account.level != access.level:
            access_account.level = access
            access_account.save()

    @staticmethod
    def push(account, access_name):
        level, created = Level.objects.get_or_create(name=access_name.lower())
        account = Account.objects.filter(account=account).first()
        if account is None:
            Account.objects.create(account=account, level=level)
        else:
            if account.level != level:
                account.level = level
                account.save(update_fields=['level'])


class Access(models.Model):
    content_type = models.ForeignKey('contenttypes.ContentType')
    content = models.BigIntegerField(db_index=True)
    level = models.ForeignKey(Level)

    def save(self, *args, **kwargs):
        from .cached import cached_access_permission_delete
        cached_access_permission_delete(content_id=self.content, content_type_id=self.content_type.id)
        super(self.__class__, self).save(*args, **kwargs)

    def api_display(self):
        level_list = []
        for access_level in self.level.get_access_level():
            level_list.append(access_level.api_display())

        return {'require_level': self.level.api_display(),
                'level_list': level_list}

    @staticmethod
    def get_permission(content_id, content_type_id, account):
        from .cached import cached_access_account_permission, cached_access_permission
        content_permission = cached_access_permission(content_id, content_type_id)
        if content_permission is None:
            return content_permission, False
        elif content_permission.level.level == 0:
            return content_permission, False
        else:
            if account is None or not account.is_authenticated():
                return content_permission, True
            elif not account.is_authenticated():
                return content_permission, True
            else:
                account_permission = cached_access_account_permission(account.id)
                if account_permission is None:
                    return content_permission, True
                else:
                    return content_permission, account_permission.level.level <= content_permission.level.level
