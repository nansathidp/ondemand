from django.conf.urls import url

from .views_content import content_views
app_name = 'access-dashboard'
urlpatterns = [
    url(r'^content/$', content_views, name='content'),  # TODO: testing
]
