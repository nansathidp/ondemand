from django import forms
from django.forms import CharField, PasswordInput

from ..models import Account


class AccountForm(forms.ModelForm):

    password = CharField(widget=PasswordInput())

    class Meta:
        model = Account
        fields = ['email', 'password', 'first_name', 'last_name', 'image']
        labels = {
            'email': 'Email',
            'password': 'Password',
            'first_name': 'First Name',
            'last_name': 'Last Name',
            'image': 'Profile Image',
        }

        """
        help_texts = {
            'name': '(60)',
            'tutor': 'hold Ctrl button to multiple select',
            'overview': '(400)'
        }
        """

    def __init__(self, *args, **kwargs):
        super(AccountForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({'type': 'file'})
