from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404

from ondemand.views_decorator import check_project
from .models import Inbox


@check_project
@login_required
def detail_view(request, inbox_id):
    inbox = get_object_or_404(Inbox, pk=inbox_id)
    return render(request, 'inbox/detail.html', {'inbox': inbox})
