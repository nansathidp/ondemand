from django.conf import settings
from django.core.cache import cache
from .models import Count

from utils.cached.time_out import get_time_out


def cached_inbox_count(account_id, is_force=False):
    key = '%s_inbox_count_%s' % (settings.CACHED_PREFIX, account_id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Count.objects.filter(account_id=account_id).first()
        if result is None:
            result = Count.objects.create(account_id=account_id)
            result.update_count(is_update_cached=False)
        cache.set(key, result, get_time_out())
    return result


def cached_inbox_count_delete(account_id):
    key = '%s_inbox_count_%s' % (settings.CACHED_PREFIX, account_id)
    cache.delete(key)


def cached_inbox_count_update(inbox_count):
    key = '%s_inbox_count_%s' % (settings.CACHED_PREFIX, inbox_count.account_id)
    cache.set(key, inbox_count, get_time_out())
