from django.contrib import admin

from .models import Inbox, Member, Count, Read
from account.models import Account


@admin.register(Inbox)
class InboxAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'account', 'type',
        'status',
        'datetime_send',
        'timestamp'
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "account":
            kwargs["queryset"] = Account.objects.filter(id=request.user.id)
        return super(InboxAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ('inbox', 'account', 'status', 'sort', 'timestamp')
    readonly_fields = ['account']


@admin.register(Count)
class CountAdmin(admin.ModelAdmin):
    list_display = ('account', 'count')


@admin.register(Read)
class ReadAdmin(admin.ModelAdmin):
    list_display = ('inbox', 'account', 'channel', 'timestamp')
