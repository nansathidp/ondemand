from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

from program.models import Program, Item as ProgramItem

register = template.Library()

@register.simple_tag
def read_tag(member, read_list, channel):
    is_found = False
    for _ in read_list:
        if _.channel == channel and member.account_id == _.account_id:
            is_found = True
            break
        
    if is_found:
        result = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
    else:
        result = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-red"></i>'
    return mark_safe(result)
