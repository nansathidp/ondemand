from django.conf.urls import url

from .views import inbox_view, inbox_item_view, inbox_popup_item_view
from .views_detail import detail_view
from .views_read import read_view

# from .views_inbox import inbox_view, inbox_popup_view
# from .views_create import create_view
# from .views_notification import notification_view

app_name = 'inbox'
urlpatterns = [
    url(r'^$', inbox_view, name='home'),
    url(r'^(\d+)/$', detail_view, name='detail'),

    url(r'^item/$', inbox_item_view, name='item'),
    url(r'^popup/item/$', inbox_popup_item_view),
    url(r'^read/$', read_view, name='read'),
    # url(r'^notification/$', notification_view, name='notification'),
    # url(r'^notification/popup/$', notification_view, name='notification_popup'),
    # url(r'^create/$', create_view, name='create'),
]
