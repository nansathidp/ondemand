from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

from .models import Inbox
from .views import json_render


@csrf_exempt
@login_required
def read_view(request):
    inbox_id = request.POST.get('inbox_id', None)
    if inbox_id is not None:
        try:
            inbox = Inbox.objects.get(id=inbox_id)
        except:
            return json_render({}, 700)
        inbox_count = inbox.read(request.user.id, 1)
    else:
        return json_render({}, 202)
    return json_render({'count': inbox_count.count}, 200)
