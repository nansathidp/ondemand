from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from ondemand.views_decorator import check_project
from .models import Inbox
from datetime import datetime, timedelta


@check_project
@login_required
def inbox_view(request):
    offset_day = datetime.now()-timedelta(days=10)
    inbox_list = Inbox.objects.filter(member__account=request.user, timestamp__gte=offset_day, status=1)
    return render(request, 'inbox/inbox.html', {
        'inbox_list': inbox_list
    })

