from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect

from ..models import Inbox


def create_view(request):
    if not request.user.has_perm('inbox.add_inbox',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    inbox = Inbox.objects.create(account=request.user,
                                 type=6,
                                 status=-1)

    inbox.content = inbox.id
    inbox.content_type = settings.CONTENT_TYPE('inbox', 'inbox')
    inbox.save(update_fields=['content', 'content_type'])

    return redirect('dashboard:inbox:account', inbox.id)
