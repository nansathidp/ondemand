from django.conf import settings
from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Inbox, Member
from account.models import Account

from .views import check_access, pull_breadcrumb
from account.jobs import _email
import xlrd


def _push(inbox, email, count, account_list):
    email = email.strip()

    if email.find('@') == -1:
        email = '%s@%s' % (email, settings.DEFAULT_EMAIL_SUFFIX)

    account = account_list.filter(email=email).first()
    if account:
        if not Member.objects.filter(inbox=inbox, account=account).exists():
            Member.objects.create(inbox=inbox, account=account, status=1, sort=count)


def _excel(assignment, book, count, account_list):
    sh = book.sheet_by_index(0)
    for rx in range(sh.nrows):
        email = _email(sh.cell(rx, 0).value, sh.cell(rx, 0).ctype)
        count += 1
        _push(assignment, email, count, account_list)


@check_access
def account_view(request, inbox_id):
    inbox = request.inbox
    if not request.user.has_perm('inbox.add_inbox',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    if inbox.status == 1:
        return redirect('dashboard:inbox:summary', inbox.id)

    error = False
    if request.method == 'POST':
        data = request.FILES['import']
        count = inbox.member_set.count()
        account_list = Inbox.account_list(request)
        try:
            book = xlrd.open_workbook(file_contents=data.read())
            _excel(inbox, book, count, account_list)
        except:
            try:
                for line in data:
                    count += 1
                    _push(inbox, line.decode('utf-8').replace(',', '').replace('\n', ''), count)
            except:
                error = True

    member_list = inbox.member_set.select_related('account').all()
    if settings.ACCOUNT__HIDE_INACTIVE:
        member_list = member_list.exclude(account__is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        member_list = member_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)
    return render(request,
                  'inbox/dashboard/account.html',
                  {'SIDEBAR': 'inbox',
                   'TAB': 'account',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'inbox': inbox,
                   'member_list': member_list,
                   'error': error})
