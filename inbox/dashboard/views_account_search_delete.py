from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from ..models import Inbox, Member


@csrf_exempt
def account_search_delete_view(request, inbox_id):
    if not request.user.has_perm('inbox.add_inbox',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    inbox = get_object_or_404(Inbox, id=inbox_id)
    
    html = ''
    if request.method == 'POST':
        try:
            member = Member.objects.filter(inbox=inbox,
                                           id=int(request.POST.get('member', -1))).first()
        except:
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
            return JsonResponse({'html': html})

        html = '<i class="zmdi zmdi-plus-circle-o zmdi-hc-fw insert-account" account="%s" style="cursor: pointer"></i>'%(member.account_id)
        member.delete()
    return JsonResponse({'html': html})
