from django import forms

from ..models import Inbox


class InboxForm(forms.ModelForm):
    class Meta:
        model = Inbox
        fields = ['title', 'body']

        labels = {
            'title': 'Subject',
            'body': 'Message',
        }
