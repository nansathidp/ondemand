from django.conf.urls import url

from .views import home_view
from .views_account import account_view
from .views_account_delete import account_delete_view
from .views_account_search import account_search_view
from .views_account_search_add import account_search_add_view
from .views_account_search_delete import account_search_delete_view
from .views_create import create_view
from .views_message import message_view
from .views_summary import summary_view

# from .views_account_submit import account_submit_view

app_name = 'inbox'
urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^create/$', create_view, name='create'),

    url(r'^(\d+)/account/$', account_view, name='account'),
    url(r'^(\d+)/account/search/$', account_search_view, name='account_search'),
    url(r'^(\d+)/account/search/add/$', account_search_add_view, name='account_search_add'),
    url(r'^(\d+)/account/search/delete/$', account_search_delete_view, name='account_search_delete'),
    url(r'^(\d+)/account/delete/$', account_delete_view, name='account_delete'),
    # url(r'^(\d+)/account/submit/$', account_submit_view, name='account_submit'),

    url(r'^(\d+)/message/$', message_view, name='message'),
    url(r'^(\d+)/summary/$', summary_view, name='summary'),
]
