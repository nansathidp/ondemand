import inspect
from functools import wraps

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404

from utils.paginator import paginator
from ..models import Inbox


def check_access(view_func):
    def _decorator(request, *args, **kwargs):
        inbox_id = args[0]
        request.inbox = get_object_or_404(Inbox, id=inbox_id)
        if not request.inbox.check_access(request):
            raise PermissionDenied
        response = view_func(request, *args, **kwargs)
        return response
    return wraps(view_func)(_decorator)


def home_view(request):
    inbox_list = Inbox.access_list(request)
    if inbox_list is None:
        raise PermissionDenied
    if settings.ACCOUNT__HIDE_INACTIVE:
        inbox_list = inbox_list.exclude(account__is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        inbox_list = inbox_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)
    inbox_list = paginator(request, inbox_list)
    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Direct Message Management'},
    ]

    return render(request,
                  'inbox/dashboard/home.html',
                  {'SIDEBAR': 'inbox',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'inbox_list': inbox_list})


def pull_breadcrumb(course=None):
    breadcrumb_list = [{'is_active': False,
                        'title': 'Direct Message Management',
                        'url': reverse('dashboard:inbox:home')}]
    caller = inspect.stack()[1][3]
    if caller == 'account_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Select Learners to Message.'})
    elif caller == 'summary_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Summary'})
                                
    elif caller == 'message_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Message.'})

    return breadcrumb_list
