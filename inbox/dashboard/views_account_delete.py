from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from ..models import Inbox, Member


@csrf_exempt
def account_delete_view(request, inbox_id):
    if not request.user.has_perm('inbox.add_inbox',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    inbox = get_object_or_404(Inbox, id=inbox_id)

    if request.method == 'POST':
        try:
            Member.objects.filter(inbox=inbox,
                                  id=int(request.POST.get('member', -1))).delete()
        except:
            pass

    member_list = inbox.member_set.all()
    return render(request,
                  'inbox/dashboard/account_block.html',
                  {'inbox': inbox,
                   'member_list': member_list})
