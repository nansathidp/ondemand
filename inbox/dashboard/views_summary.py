from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect

from .views import check_access, pull_breadcrumb
from ..models import Read


@check_access
def summary_view(request, inbox_id):
    inbox = request.inbox
    if not request.user.has_perm('inbox.add_inbox',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    if inbox.status != 1:
        return redirect('dashboard:inbox:account', inbox.id)

    inbox.count_send = inbox.member_set.all().count()
    inbox.save(update_fields=['count_send'])
    member_list = inbox.member_set.all()
    read_list = inbox.read_set.all()

    try:
        open = float(inbox.count_read) / float(inbox.count_send) * 100.0
    except:
        open = 0
    try:
        open_web = float(Read.objects.filter(inbox=inbox, channel=1).count()) / float(inbox.count_read) * 100.0
    except:
        open_web = 0
    try:
        open_mobile = float(Read.objects.filter(inbox=inbox, channel=2).count()) / float(inbox.count_read) * 100.0
    except:
        open_mobile = 0
    try:
        not_open = float(inbox.count_send - inbox.count_read) / float(inbox.count_send) * 100.0
    except:
        not_open = 0
    if settings.ACCOUNT__HIDE_INACTIVE:
        member_list = member_list.exclude(account__is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        member_list = member_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)
    return render(request,
                  'inbox/dashboard/summary.html',
                  {'SIDEBAR': 'inbox',
                   'TAB': 'summary',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'inbox': inbox,
                   'member_list': member_list,
                   'read_list': read_list,
                   'open': open,
                   'open_web': open_web,
                   'open_mobile': open_mobile,
                   'not_open': not_open})
