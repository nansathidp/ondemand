from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt

from department.cached import cached_department
from utils.filter import get_q
from ..models import Inbox
from account.models import Account
from department.models import Department
if settings.PROJECT == 'ais':
    from ais.models import AccountInfo
if settings.IS_ORGANIZATION:
    from organization.models import Parent

from dashboard.views import config_view, paginator


@csrf_exempt
def account_search_view(request, inbox_id):
    if not request.user.has_perm('inbox.add_inbox',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    inbox = get_object_or_404(Inbox, id=inbox_id)
    member_list = inbox.member_set.all()
    q_department_list = Department.pull_list()
    q_name = ''
    account_list = []
    type = None
    if request.method == 'POST':
        type = request.POST.get('type', None)
        q_name = request.POST.get('q_name', '')
        if type == 'one':
            if request.user.has_perm('inbox.view_inbox', group=request.DASHBOARD_GROUP) or \
               request.user.has_perm('inbox.view_own_inbox', group=request.DASHBOARD_GROUP):
                account_list = Account.objects.filter(is_active=True).order_by('email')
            elif request.user.has_perm('inbox.view_org_inbox', group=request.DASHBOARD_GROUP):
                if settings.IS_ORGANIZATION:
                    account_list = Account.objects.filter(org_parent_account_set__parent=request.user,
                                                          is_active=True).order_by('email')
                else:
                    return config_view(request)
            else:
                raise PermissionDenied
            
            if len(q_name) > 0:
                account_list = account_list.filter(Q(email__icontains=q_name) |
                                                   Q(first_name__icontains=q_name))

            q_department = get_q(request, 'q_department')
            if q_department != -1:
                department = cached_department(q_department)
                if department is not None:
                    child_department_list = department.get_all_child()
                    account_list = account_list.filter(department_member_set__department__in=child_department_list)
            if settings.ACCOUNT__HIDE_INACTIVE:
                account_list = account_list.exclude(is_active=False)
            if settings.ACCOUNT__HIDE_ID_LIST:
                account_list = account_list.exclude(id__in=settings.ACCOUNT__HIDE_ID_LIST)
            account_list = paginator(request, account_list)
    return render(request,
                  'inbox/dashboard/account_search.html',
                  {'inbox': inbox,
                   'type': type,
                   'q_name': q_name,
                   'member_list': member_list,
                   'account_list': account_list,
                   'q_department': q_department,
                   'q_department_list': q_department_list})
