from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from account.models import Account
from ..models import Inbox, Member


@csrf_exempt
def account_search_add_view(request, inbox_id):
    if not request.user.has_perm('inbox.add_inbox',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    inbox = get_object_or_404(Inbox, id=inbox_id)
    if inbox.status == 3:
        html = '<i class="zmdi zmdi-close-circle text-red"></i>'
        return JsonResponse({'html': html})

    if request.method == 'POST':
        try:
            account_id = int(request.POST.get('account', -1))
            account = Account.objects.get(id=account_id)
        except:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
            return JsonResponse({'html': html})
        
        if request.user.has_perm('inbox.view_inbox', group=request.DASHBOARD_GROUP) or \
           request.user.has_perm('inbox.view_own_inbox', group=request.DASHBOARD_GROUP):
            pass
        elif request.user.has_perm('inbox.view_org_inbox', group=request.DASHBOARD_GROUP):
            if not account.org_parent_account_set.filter(parent=request.user).exists():
                raise PermissionDenied
        else:
            raise PermissionDenied

        member = Member.objects.filter(inbox=inbox,
                                       account=account).first()
        if member is not None:
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green search-delete-member" member="%s" style="cursor: pointer"></i>'%member.id
        else:
            member = Member.objects.create(inbox=inbox,
                                           account=account)
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green search-delete-member" member="%s" style="cursor: pointer"></i>'%member.id
    return JsonResponse({'html': html})
