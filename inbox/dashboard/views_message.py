import django_rq
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect

from inbox.cached import cached_inbox_count
from mailer.job.send_email import _send_inbox
from notification.jobs import _push
from .form import InboxForm
from .views import check_access, pull_breadcrumb


@check_access
def message_view(request, inbox_id):
    inbox = request.inbox
    if not request.user.has_perm('inbox.add_inbox',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    if inbox.status == 1:
        return redirect('dashboard:inbox:summary', inbox.id)

    if request.method == 'POST':
        account_list = []
        inbox_form = InboxForm(request.POST, instance=inbox)
        if inbox_form.is_valid():
            inbox = inbox_form.save()
            if 'send' in request.POST:
                inbox.send()
                for member in inbox.member_set.select_related('account').all():
                    account_list.append(member.account)
                    if settings.IS_ALERT_DIRECT_MSG:

                        # Inbox & Email
                        django_rq.enqueue(_send_inbox, inbox.id, member.account_id)

                        # Push Notification
                        if not settings.IS_ALERT_AIS:
                            django_rq.enqueue(_push, request.APP.id, inbox.title, member.account_id)
                        # Message.push(request.APP.id, inbox.title, member.account_id)

                    inbox_count = cached_inbox_count(member.account_id)
                    inbox_count.update_count(is_update_cached=True)

                if settings.IS_ALERT_AIS:
                    from ais.jobs import push_with_email
                    django_rq.enqueue(push_with_email, inbox.title, account_list, 'iOS')
                    django_rq.enqueue(push_with_email, inbox.title, account_list, 'Android')

                return redirect('dashboard:inbox:summary', inbox.id)
    inbox_form = InboxForm(instance=inbox)
    return render(request,
                  'inbox/dashboard/message.html',
                  {'SIDEBAR': 'inbox',
                   'TAB': 'message',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'inbox': inbox,
                   'inbox_form': inbox_form})
