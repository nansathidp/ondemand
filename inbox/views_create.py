from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from account.models import Account


@login_required
def create_view(request):
    if request.method == 'POST':
        account = Account.objects.get(id=1024)
        # room = Room.pull([request.user, account])
        # room.push(request.user, 1, request.POST.get('message', ''), None)
    return render(request,
                  'inbox/create.html',
                  {})
