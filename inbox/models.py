from django.conf import settings
from django.db import models


class Inbox(models.Model):
    from django.conf import settings

    TYPE_CHOICES = (
        (1, 'Chat'),
        (2, 'Support Chat'),
        (3, 'Tutor Promote'),
        (4, 'Test Push'),
        (5, 'System Auto'),
        (6, 'Direct Message'),
        (7, 'Assignment'),
    )

    STATUS_CHOICES = (
        (-1, 'Draft'),
        (1, 'Sent'),
    )

    account = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)  # Send msg
    type = models.IntegerField(choices=TYPE_CHOICES)

    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+', null=True, blank=True)
    content = models.BigIntegerField(default=-1)

    title = models.CharField(max_length=140)
    body = models.TextField(blank=True)
    image = models.ImageField(upload_to='inbox/image/', null=True, blank=True)

    count_read = models.IntegerField(default=0)
    count_send = models.IntegerField(default=1)

    status = models.IntegerField(choices=STATUS_CHOICES, default=1)
    datetime_send = models.DateTimeField(null=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'view_org', 'add', 'change', 'delete')

    @property
    def url(self):
        from django.conf import settings
        from django.urls import reverse
        url = None
        if self.content_type_id == settings.CONTENT_TYPE('course', 'course').id:
            url = reverse('course:detail', args=[self.content])
        elif self.content_type_id == settings.CONTENT_TYPE('question', 'activity').id:
            url = reverse('question:detail', args=[self.content])
        elif self.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
            url = reverse('program:detail', args=[self.content])
        elif self.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
            url = reverse('program:detail', args=[self.content])
        elif self.content_type_id == settings.CONTENT_TYPE('inbox', 'inbox').id:
            url = reverse('inbox:detail', args=[self.content])
        return url

    @staticmethod
    def access_list(request):
        if request.user.has_perm('inbox.view_inbox',
                                 group=request.DASHBOARD_GROUP):
            return Inbox.objects.filter(type=6).order_by('-timestamp')
        elif request.user.has_perm('inbox.view_own_inbox',
                                   group=request.DASHBOARD_GROUP):
            return Inbox.objects.filter(account=request.user,
                                        type=6).order_by('-timestamp')
        elif request.user.has_perm('inbox.view_org_inbox',
                                   group=request.DASHBOARD_GROUP):
            return Inbox.objects.filter(account=request.user,
                                        type=6).order_by('-timestamp')
        else:
            return None

    @staticmethod
    def account_list(request):
        from account.models import Account
        if request.user.has_perm('inbox.view_inbox', group=request.DASHBOARD_GROUP):
            return Account.objects.all().order_by('email')
        elif request.user.has_perm('inbox.view_org_inbox', group=request.DASHBOARD_GROUP):
            if settings.IS_ORGANIZATION:
                return Account.objects.filter(org_parent_account_set__parent=request.user).order_by('email')
            else:
                return Account.objects.none()
        else:
            return Account.objects.none()

    def is_read(self, account):
        if account is None:
            return False
        return self.read_set.filter(account=account).exists()

    def is_read_chanel(self, account, channel):
        if account is None:
            return False
        return self.read_set.filter(account=account,
                                    channel=channel).exists()

    def check_access(self, request):
        if request.user.has_perm('inbox.view_inbox',
                                 group=request.DASHBOARD_GROUP):
            return True
        elif request.user.has_perm('inbox.view_own_inbox',
                                   group=request.DASHBOARD_GROUP):
            return True
        elif request.user.has_perm('inbox.view_org_inbox',
                                   group=request.DASHBOARD_GROUP):
            return True
        else:
            return False

    @staticmethod
    def push_message(type, content_type, content, title, body, status, account_list):
        inbox_member_list = []
        inbox = Inbox.objects.create(type=type,
                                     content_type=content_type,
                                     content=content,
                                     title=title,
                                     body=body,
                                     status=status)
        for account in account_list:
            inbox_member_list.append(Member(inbox=inbox, account=account))
        Member.objects.bulk_create(inbox_member_list)

    # For Notification
    @staticmethod
    def push(page, content, app, account, type, title, msg, image=None, is_push_mobile=True):
        from notification.models import Message, Android, AndroidKey, AndroidMessage, Ios, IphoneKey, IphoneMessage
        from .cached import cached_inbox_count
        inbox = Inbox.objects.create(account=None,
                                     type=type,
                                     title=title,
                                     body=msg,
                                     image=image)

        inbox_count = cached_inbox_count(account.id)
        inbox_count.update_count()

        Member.objects.create(inbox=inbox, account=account)
        if is_push_mobile:
            message = Message.objects.filter(page=page,
                                             content=content,
                                             status=-2).first()
            if message is None:
                message = Message.objects.create(app=app,
                                                 message=msg,
                                                 page=page,
                                                 content=content,
                                                 status=-2)
            else:
                message.message = msg
                message.save(update_fields=['message'])

            # Android
            android = Android.objects.filter(account=account).first()
            if android is not None:
                android_message = AndroidMessage.objects.filter(inbox=inbox,
                                                                message=message,
                                                                android=android).first()
                if android_message is None:
                    android_message = AndroidMessage.objects.create(inbox=inbox,
                                                                    app=message.app,
                                                                    message=message,
                                                                    android=android)
                android_message.send(AndroidKey.objects.all()[0])

            # iPhone
            iphone = Ios.objects.filter(account=account).first()
            if iphone is not None:
                iphone_message = IphoneMessage.objects.filter(inbox=inbox,
                                                              message=message,
                                                              iphone=iphone).first()
                if iphone_message is None:
                    iphone_message = IphoneMessage.objects.create(inbox=inbox,
                                                                  app=message.app,
                                                                  message=message,
                                                                  iphone=iphone)
                iphone_message.send(IphoneKey.objects.all()[0])

    @staticmethod
    def pull_notification_count(user_id):
        from inbox.cached import cached_inbox_count
        return cached_inbox_count(user_id, is_force=True)

    def api_display(self, account):
        from django.conf import settings
        from django.utils import timezone
        # tz_timestamp = timezone.get_current_timezone().normalize(self.timestamp)
        try:
            tz_timestamp = timezone.get_current_timezone().normalize(self.datetime_send)
        except: # Fix add Datetime_send -*-
            self.datetime_send = self.timestamp
            self.save()
            tz_timestamp = timezone.get_current_timezone().normalize(self.timestamp)
        return {'id': self.id,
                'title': self.title,
                'message': self.body,
                'url': self.url,
                'is_read': self.is_read(account),
                'is_read_web': self.is_read_chanel(account, 1),
                'is_read_mobile': self.is_read_chanel(account, 2),
                'content': self.content,
                'content_type_name': self.content_type.name if self.content_type else None,
                'timestamp': tz_timestamp.strftime(settings.TIMESTAMP_FORMAT_)}

    def send(self):
        from django.utils import timezone
        count_send = 0
        self.status = 1
        self.count_send = count_send
        self.datetime_send = timezone.now()
        self.save(update_fields=['status', 'count_send', 'datetime_send'])

    def read(self, account_id, channel):
        from django.db.models import Count
        from .cached import cached_inbox_count

        read = Read.objects.filter(inbox=self,
                                   account_id=account_id,
                                   channel=channel).first()
        if read is None:
            read = Read.objects.create(inbox=self,
                                       account_id=account_id,
                                       channel=channel)
        inbox_count = cached_inbox_count(account_id)
        inbox_count.update_count()
        self.count_read = Read.objects.filter(inbox=self).values('account_id').annotate(Count('id')).order_by().count()
        self.save(update_fields=['count_read'])
        return inbox_count


class Member(models.Model):
    from django.conf import settings

    STATUS_CHOICES = (
        (-1, 'Import Not Found.'),
        (1, 'Basic'),
    )

    inbox = models.ForeignKey(Inbox)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name='inbox_account_set')
    status = models.IntegerField(choices=STATUS_CHOICES, default=1)
    sort = models.IntegerField(default=0, db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)


class Count(models.Model):
    from django.conf import settings

    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    count = models.IntegerField(default=0)

    def update_count(self, is_update_cached=True):
        from .cached import cached_inbox_count_update
        count_all = Inbox.objects.filter(member__account_id=self.account_id, status=1).count()
        count_read = Inbox.objects.filter(member__account_id=self.account_id, status=1, read__account_id=self.account_id).distinct().count()
        self.count = count_all - count_read
        self.save(update_fields=['count'])
        if is_update_cached:
            cached_inbox_count_update(self)


class Read(models.Model):
    from django.conf import settings

    CHANNEL_CHOICES = (
        (1, 'Web'),
        (2, 'App'),
    )

    inbox = models.ForeignKey(Inbox)
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    channel = models.IntegerField(choices=CHANNEL_CHOICES)
    timestamp = models.DateTimeField(auto_now_add=True)
