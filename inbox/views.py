import datetime

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils import timezone

from .api.v1.views import json_render
from .models import Inbox, Read

OFFSET_DAY = 60


@login_required
def inbox_view(request):
    return render(request,
                  'inbox/notification.html',
                  {})


@login_required
def inbox_item_view(request):
    code = 200
    perpage = 20
    result = {
        'inbox_list': [],
        'is_next': True
    }

    page = request.GET.get('page', 1)
    offset_day = timezone.now() - datetime.timedelta(days=OFFSET_DAY)

    inbox_list = Inbox.objects.filter(member__account=request.user,
                                      status=1).order_by('-timestamp')
    paginator = Paginator(inbox_list, perpage)
    try:
        inbox_list = paginator.page(page)
    except PageNotAnInteger:
        inbox_list = paginator.page(1)
        result['is_next'] = False
    except EmptyPage:
        inbox_list = paginator.page(paginator.num_pages)
        result['is_next'] = False

    read_list = Read.objects.values_list(
        'inbox_id',
        flat=True
    ).filter(
        account=request.user,
        inbox__in=inbox_list
    )
    for inbox in inbox_list:
        result['inbox_list'].append(
            render_to_string(
                'inbox/item/item_notification.html',
                {
                    'inbox': inbox,
                    'is_read': True if inbox.id in read_list else False,
                    'is_read_web': inbox.is_read_chanel(request.user, 1)
                }
            )
        )
    return json_render(result, code)


@login_required
def inbox_popup_item_view(request):
    code = 200
    result = {
        'inbox_list': []
    }
    offset_day = timezone.now() - datetime.timedelta(days=OFFSET_DAY)
    inbox_list = Inbox.objects.filter(member__account=request.user,
                                      status=1,
                                      timestamp__gte=offset_day).order_by('-timestamp')[:3]
    for inbox in inbox_list:
        result['inbox_list'].append(
            render_to_string(
                'inbox/item/item_notification_popup.html',
                {
                    'inbox': inbox,
                    'is_read': inbox.is_read(request.user),
                    'is_read_web': inbox.is_read_chanel(request.user, 1)
                }
            )
        )
    return json_render(result, code)
