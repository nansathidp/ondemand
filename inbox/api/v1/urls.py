from django.conf.urls import url

from .views_list import list_view
from .views_read import read_view

urlpatterns = [
    url(r'^$', list_view),  # TODO: testing
    url(r'^(\d+)/read/$', read_view),  # TODO: testing
]
