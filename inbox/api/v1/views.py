from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist

_STATUS_MSG = {
    200: 'Success',
    201: 'Require POST method.',
    202: 'Require UUID.',
    204: 'Require token.',
    209: 'Require Store.',
}

try:
    from api.views_api import STATUS_MSG
except ObjectDoesNotExist:
    STATUS_MSG = _STATUS_MSG


def _json_render(result, status):
    try:
        status_msg = _STATUS_MSG[status]
    except:
        try:
            status_msg = STATUS_MSG[status]
        except:
            status_msg = 'Unknown.'

    data = {'status': status,
            'status_msg': status_msg,
            'result': result}
    return JsonResponse(data, json_dumps_params={'indent': 2})

try:
    json_render = _json_render
except ObjectDoesNotExist:
    from api.views import json_render
    json_render = json_render

from api.views import check_auth
from api.v5.views import check_key
from api.v5.views import check_require
