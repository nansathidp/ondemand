from .views import json_render, check_key, check_auth

from ...models import Inbox


def read_view(request, inbox_id):
    response = check_key(request)
    if response is not None:
        return response

    account, code = check_auth(request)
    if code != 200:
        return json_render({}, code)

    try:
        inbox = Inbox.objects.get(id=inbox_id)
    except Inbox.ObjectDoesNotExist:
        return json_render({}, 700)

    inbox_count = inbox.read(account.id, 2)
    return json_render({'count': inbox_count.count}, 200)
