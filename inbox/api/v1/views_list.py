from .views import json_render, check_key, check_require

from ...models import Inbox

from account.cached import cached_api_account_auth


def list_view(request):
    result = {}

    # Check Require
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request, require_token=True)
    if code != 200:
        return json_render(result, code)

    # Authentication require
    account = cached_api_account_auth(token, uuid)
    if account is None:
        return json_render({}, 400)

    result['inbox_list'] = []
    for inbox in Inbox.objects.filter(member__account_id=account.id, status=1).order_by('-timestamp'):
        result['inbox_list'].append(inbox.api_display(account))
    return json_render(result, code)
