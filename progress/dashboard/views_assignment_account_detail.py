from django.shortcuts import render, get_object_or_404
from django.conf import settings

from assignment.models import Assignment, Result
from account.models import Account
from progress.models import Progress
from order.models import Item as OrderItem

from .views import pull_breadcrumb
from utils.paginator import paginator

def assignment_account_detail_view(request, assignment_id, account_id):
    assignment = get_object_or_404(Assignment, id=assignment_id)
    account = get_object_or_404(Account, id=account_id)

    result_list = Result.objects.select_related('item') \
                                .filter(assignment=assignment,
                                        member__account=account,
                                        status=1)
    progress_list = []
    for result in result_list:
        if result.order_item is None:
            continue
        order_item = OrderItem.pull(result.order_item_id)
        progress = Progress.pull(order_item,
                                 None,
                                 settings.CONTENT_TYPE_ID(result.item.content_type_id),
                                 result.item.content)
        progress_list.append(progress)
    return render(request,
                  'progress/dashboard/account_detail.html',
                  {
                      'SIDEBAR': 'assignment',
                      'BREADCRUMB_LIST': pull_breadcrumb({'assignment_id': assignment.id, 'account_id': account.id}),
                      'assignment': assignment,
                      'account': account,
                      'content_type': settings.CONTENT_TYPE('assignment', 'assignment'),
                      'progress_list': progress_list,
                  })
