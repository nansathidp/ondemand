from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404

from account.models import Account
from progress.models import Progress, Account as ProgressAccount

from utils.paginator import paginator
from utils.filter import get_q
from utils.breadcrumb import get_breadcrumb
from ..models import Progress, Account as ProgressAccount

def _pack_result(progress_account):
    d = {}
    for code in ProgressAccount.CODE_LIST:
        for _ in ProgressAccount.FIELD_LIST:
            d['%s%s__sum'%(code, _)] = getattr(progress_account, '%s%s'%(code, _), 0)
        in_progress = d.get('%sin_progress__sum'%code, 0)
        completed = d.get('%scompleted__sum'%code, 0)
        failed = d.get('%sfailed__sum'%code, 0)
        expired = d.get('%sexpired__sum'%code, 0)
        try:
            d['%spercent'%code] = int(float(completed) / float(in_progress + completed + failed + expired) * 100.0)
        except:
            d['%spercent'%code] = 0
    return d

def account_detail_view(request, account_id):
    if request.user.has_perm('progress.view_progress',
                             group=request.DASHBOARD_GROUP):
        account = get_object_or_404(Account, id=account_id)
    elif request.user.has_perm('progress.view_own_progress',
                               group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    elif request.user.has_perm('progress.view_org_progress',
                               group=request.DASHBOARD_GROUP):
        account = get_object_or_404(Account, id=account_id)
        if settings.IS_ORGANIZATION:
            if not account.org_child_set.filter(account=request.user).exists():
                raise PermissionDenied
    else:
        raise PermissionDenied

    if request.GET.get('action', None) == 'update':
        ProgressAccount.update_progress(account.id)
    progress_account = ProgressAccount.pull(account.id, True)
    progress_result = _pack_result(progress_account)

    progress_list = Progress.objects.filter(account=account, is_root=True)
    progress_list = paginator(request, progress_list)
    return render(request,
                  'progress/dashboard/account_detail.html',
                  {
                      'SIDEBAR': 'progress-account',
                      'BREADCRUMB_LIST': get_breadcrumb(['progress.account_view', 'progress.account_detail_view'], **{'account_id': account.id}),
                      'account': account,
                      'progress_account': progress_account,
                      'progress_result': progress_result,
                      'progress_list': progress_list,
                      'STATUS_CHOICES': Progress.STATUS_CHOICES
                  })
