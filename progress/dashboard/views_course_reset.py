from django.shortcuts import redirect, get_object_or_404
from django.conf import settings

from progress.models import Progress, VideoLog
from course.models import Material

from progress.cached import cached_progress_update, cached_progress_videolog_last_delete_v2

import datetime

def course_reset_view(request, course_id, progress_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress.duration = datetime.timedelta(0)
    progress.percent = 0
    progress.save(update_fields=['duration', 'percent'])
    cached_progress_update(progress)

    for _ in Progress.objects.filter(item_id=progress.item_id,
                                     content_type=settings.CONTENT_TYPE('course', 'material')):
        _.duration = datetime.timedelta(0)
        _.percent = 0
        _.save(update_fields=['duration', 'percent'])
        cached_progress_update(_)
        
    for _ in VideoLog.objects.filter(progress=progress):
        cached_progress_videolog_last_delete_v2(_)
        _.delete()
        
    return redirect('dashboard:progress:course_detail', course_id)
