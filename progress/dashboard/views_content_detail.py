import datetime

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, Http404
from django.utils import timezone

from alert.models import Alert
from content.models import Location as ContentLocation
from course.models import Course
from program.models import Program
from question.models import Activity
from utils.breadcrumb import get_breadcrumb
from utils.content_type import get_content_type_name
# from .views_course import _account_accept, _progress_content
from utils.paginator import paginator
from ..models import Progress, Content as ProgressContent


def _content_detail(request, content, content_type, content_type_name,
                    SIDEBAR, BREADCRUMB_LIST):
    if request.user.has_perm('progress.view_progress',
                             group=request.DASHBOARD_GROUP):
        progress_list = Progress.objects.filter(
            content_type=content_type,
            content=content.id,
            item__status=2,
            is_root=True
        )
    elif request.user.has_perm('progress.view_own_progress',
                               group=request.DASHBOARD_GROUP):
        if request.DASHBOARD_PROVIDER is not None:
            if content.provider != request.DASHBOARD_PROVIDER:
                raise PermissionDenied
        elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
            if content.provider_id != request.DASHBOARD_SUB_PROVIDER.provider_id:
                raise PermissionDenied
        else:
            raise PermissionDenied
        progress_list = Progress.objects.filter(
            content_type=content_type,
            content=content.id,
            item__status=2,
            is_root=True
        )
    elif request.user.has_perm('progress.view_org_progress',
                               group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    else:
        raise PermissionDenied

    progress_content_list = ProgressContent.objects.filter(
        location=ContentLocation.pull_first(content_type,
                                            content.id),
        content_type=content_type,
        content=content.id
    )
    # progress_content_list = ProgressContent.objects.filter(location__isnull=True,
    #                                                        content_type=content_type,
    #                                                        content=content.id)
    progress_result = ProgressContent.pull_summary(progress_content_list)
    if content_type.app_label != 'program':
        progress_content_location_list = ProgressContent.objects.filter(location__isnull=False,
                                                                        content_type=content_type,
                                                                        content=content.id)
    else:
        progress_content_location_list = None
    if settings.ACCOUNT__HIDE_INACTIVE:
        progress_list = progress_list.exclude(account__is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        progress_list = progress_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)
    progress_list = paginator(request, progress_list)
    for progress in progress_list:
        if content_type.app_label == 'course':
            progress.url = reverse('dashboard:progress:detail_course', args=[progress.id, content.id])
        elif content_type.app_label == 'question':
            if SIDEBAR == 'progress-account':
                progress.url = reverse('dashboard:question:score_account', args=[progress.content,
                                                                                 progress.account_id,
                                                                                 progress.location_id])
            else:
                progress.url = reverse('dashboard:progress:detail_question', args=[progress.id,
                                                                                   content.id])
        elif content_type.app_label == 'program':
            if SIDEBAR == 'progress-program':
                progress.url = reverse('dashboard:progress:detail_program', args=[progress.id,
                                                                                  content.id])
            else:
                progress.url = reverse('dashboard:progress:detail_onboard', args=[progress.id,
                                                                                  content.id])
        else:
            progress.url = '#'


    alert = Alert.pull_inprogress(request.user, content_type, content.id)
    if 'ais' in settings.INSTALLED_APPS:
        project = True
    else:
        project = False
    return render(request,
                  'progress/dashboard/content_detail.html',
                  {
                      'SIDEBAR': SIDEBAR,
                      'BREADCRUMB_LIST': BREADCRUMB_LIST,
                      'content': content,
                      'content_type': content_type,
                      'content_type_name': content_type_name,
                      'progress_result': progress_result,
                      'progress_content_location_list': progress_content_location_list,
                      'progress_list': progress_list,
                      'alert': alert,
                      'project': project
                  })


def content_detail_course_view(request, course_id):
    content = Course.pull(course_id)
    if content is None:
        raise Http404
    content_type = settings.CONTENT_TYPE('course', 'course')
    return _content_detail(request,
                           content,
                           content_type,
                           get_content_type_name(content_type),
                           'progress-course',
                           get_breadcrumb(['progress.content_course_view',
                                           'progress.content_detail_course_view'],
                                          **{'content_id': content.id}))


def content_detail_question_view(request, activity_id):
    content = Activity.pull(activity_id)
    if content is None:
        raise Http404
    content_type = settings.CONTENT_TYPE('question', 'activity')
    return _content_detail(request,
                           content,
                           content_type,
                           get_content_type_name(content_type),
                           'progress-question',
                           get_breadcrumb(['progress.content_question_view',
                                           'progress.content_detail_question_view'],
                                          **{'content_id': content.id}))


def content_detail_program_view(request, program_id):
    content = Program.pull(program_id)
    if content is None:
        raise Http404
    content_type = settings.CONTENT_TYPE('program', 'program')
    return _content_detail(request,
                           content,
                           content_type,
                           get_content_type_name(content_type),
                           'progress-program',
                           get_breadcrumb(['progress.content_program_view',
                                           'progress.content_detail_program_view'],
                                          **{'content_id': content.id}))


def content_detail_onboard_view(request, program_id):
    content = Program.pull(program_id)
    if content is None:
        raise Http404
    content_type = settings.CONTENT_TYPE('program', 'onboard')
    return _content_detail(request,
                           content,
                           content_type,
                           get_content_type_name(content_type),
                           'progress-onboard',
                           get_breadcrumb(['progress.content_onboard_view',
                                           'progress.content_detail_onboard_view'],
                                          **{'content_id': content.id}))
