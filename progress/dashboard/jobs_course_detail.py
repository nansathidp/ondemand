import json

from django.conf import settings
from django.utils import timezone

from alert.models import Alert
from content.models import Location as ContentLocation
from course.models import Course, Material
from progress.models import Progress, VideoLog
from provider.models import Provider
from question.models import Activity, Score
from .jobs import alert_fail
from .jobs import get_account_department, get_account_direct_manager
from .jobs import make_xls_detail, _datetime, _percent, get_account_info

if settings.PROJECT == 'ais':
    from ais.models import AccountInfo
if settings.IS_SUB_PROVIDER:
    from subprovider.models import SubProvider


def export_course_detail_job(alert_id):
    try:
        alert = Alert.objects.get(id=alert_id)
    except:
        return None

    if alert.content == -1:
        alert_fail(alert)
        return None
    else:
        course = Course.pull(alert.content)
        if course is None:
            alert_fail(alert)
            return None

    ACCOUNT_INFO = {}
    PROGRESS_MATERIAL = {}
    PROGRESS_MATERIAL_QUESTION = {}

    account_department = {}
    account_direct_manager = {}
    account_id_list = set()

    VIDEO_LOG = {}

    kwargs = json.loads(alert.json_kwargs)
    if not all([_ in kwargs for _ in ['name', 'perm', 'provider', 'sub_provider']]):
        alert_fail(alert)
        return None

    if kwargs['provider']:
        provider = Provider.pull(kwargs['provider'])
    else:
        provider = None
    if kwargs['sub_provider']:
        sub_provider = SubProvider.pull(kwargs['sub_provider'])
    else:
        sub_provider = None

    content_type = settings.CONTENT_TYPE('course', 'course')
    if kwargs['perm'] == 'view':
        progress_list = Progress.objects.filter(
            content_type=content_type,
            content=course.id,
            is_root=True
        ).select_related(
            'account',
            'item'
        )
    elif kwargs['perm'] == 'own':
        if provider:
            if course.provider_id != provider.id:
                alert_fail(alert)
                return None
        elif settings.IS_SUB_PROVIDER and sub_provider:
            if course.provider_id != sub_provider.provider_id:
                alert_fail(alert)
                return None
        else:
            alert_fail(alert)
            return None
        progress_list = Progress.objects.filter(
            content_type=content_type,
            content=course.id,
            is_root=True
        ).select_related(
            'account',
            'item'
        )
    else:
        alert_fail(alert)
        return None

    report_list = []
    if settings.ACCOUNT__HIDE_INACTIVE:
        progress_list = progress_list.exclude(account__is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        progress_list = progress_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)

    material_list = Material.objects.filter(
        course=course,
    ).select_related(
        'outline'
    ).order_by(
        'outline__sort',
        'sort'
    )

    _progress_material(course, PROGRESS_MATERIAL)
    activity_id_list = _progress_material_question(course, PROGRESS_MATERIAL_QUESTION)
    _video_log(course, VIDEO_LOG)

    score_list = Score.objects.filter(
        activity_id__in=activity_id_list
    )
    for _ in progress_list:
        if not _.account_id in account_id_list:
            account_id_list.add(_.account_id)
    get_account_department(account_id_list, account_department)
    get_account_direct_manager(account_id_list, account_direct_manager)


    if settings.PROJECT == 'ais':
        account_info_list = list(AccountInfo.objects.filter(
            account_id__in=account_id_list
        ))
        for _ in account_id_list:
            for account_info in account_info_list:
                if _ == account_info.account_id:
                    ACCOUNT_INFO[_] = account_info
                    break
            if not _ in ACCOUNT_INFO:
                ACCOUNT_INFO[_] = None

    count = 0
    start = a = timezone.now()
    for progress in progress_list:
        count += 1
        account = progress.account
        order_item = progress.item

        account_info = get_account_info(account, ACCOUNT_INFO)
        for material in material_list:
            data = {
                'name': course.name,
                'enroll_timestamp': order_item.timestamp,
                'status': progress.get_status_display(),
                'employee': account_info['employee'],
                'account': account,
                'buh': account_info['buh'],
                'bu': account_info['bu'],
                'section': account_info['section'],
                'material': material,
                # 'progress_material': progress_material,
                'progress_material': None,
                'interval': '-',
                'score': '-',
                'score_percent': '-',
                'timestamp': None
            }
            data.update(account_info)
            if material.type == 0 or material.type == 4:
                key = '%s_%s' % (material.id, order_item.id)
                if key in PROGRESS_MATERIAL:
                    data['progress_material'] = PROGRESS_MATERIAL[key]
                key = '%s_%s_%s_%s' % (order_item.id, progress.id, account.id, material.id)
                if key in VIDEO_LOG:
                    data['interval'] = VIDEO_LOG[key]
                    data['timestamp'] = data['progress_material'].timeupdate
            elif material.type == 1 or material.type == 2:
                key = '%s_%s' % (material.id, order_item.id)
                if key in PROGRESS_MATERIAL:
                    data['progress_material'] = PROGRESS_MATERIAL[key]
                if data['progress_material'] and data['progress_material'].status == 2:
                    data['interval'] = 1
                    data['timestamp'] = data['progress_material'].timeupdate
                else:
                    data['interval'] = 0
            elif material.type == 3:
                key = '%s_%s' % (material.question_id, order_item.id)
                if key in PROGRESS_MATERIAL_QUESTION:
                    data['progress_material'] = PROGRESS_MATERIAL_QUESTION[key]
                activity = Activity.pull(material.question_id)
                if activity:
                    content_location = ContentLocation.pull_first(
                        settings.CONTENT_TYPE('question', 'activity'),
                        activity.id,
                        parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                        parent1_content=course.id,
                        parent2_content_type=settings.CONTENT_TYPE('course', 'material'),
                        parent2_content=material.id
                    )
                    score = _score(
                        score_list,
                        content_location,
                        account,
                        order_item
                    )
                    if score:
                        data['interval'] = '%s/%s' % (score.submit_number, score.max_submit)
                        data['score'] = '%s/%s' % (score.score, score.max_score)
                        try:
                            data['score_percent'] = float(score.score) / float(score.max_score) * 100.0
                        except:
                            data['score_percent'] = 0
                        data['timestamp'] = score.timeupdate
            # print('------------> \n', data)
            report_list.append(pack_data(data, account_department, account_direct_manager))
        b = timezone.now()
        # print(count, '-> time use:', b - start, ' Diff: ', b - a)
        a = b
    # print('time use: ', timezone.now() - start)
    return make_xls_detail(
        report_list,
        get_column_list(),
        alert
    )


def pack_data(data, account_department, account_direct_manager):
    row_xls = []
    row_xls.append(data['name'])
    row_xls.append(_datetime(data['enroll_timestamp']))
    row_xls.append(data['status'])
    row_xls.append(data['employee'])
    row_xls.append(data['account'].first_name)
    row_xls.append(data['account'].last_name)
    row_xls.append(data['account'].email)
    if settings.PROJECT == 'ais':
        row_xls.append(data['buh'])
        row_xls.append(data['bu'])

    row_xls.append(account_department[data['account'].id])
    if settings.PROJECT == 'ais':
        row_xls.append(data['section'])
    row_xls.append(account_direct_manager[data['account'].id])
    row_xls.append(data['material'].outline.section)
    row_xls.append(data['material'].get_type_display())
    row_xls.append(data['material'].name)
    if data['progress_material']:
        row_xls.append(data['progress_material'].get_status_display())
        row_xls.append(_percent(data['progress_material'].percent))
    else:
        row_xls.append('-')
        row_xls.append('-')
    row_xls.append(data['interval'])
    row_xls.append(data['score'])
    row_xls.append(_percent(data['score_percent']))
    row_xls.append(_datetime(data['timestamp']))
    return row_xls


def get_column_list():
    result = []
    result.append('Course Name')
    result.append('Enroll Time')
    result.append('Course Status')
    if settings.PROJECT == 'ais':
        result.append('PIN Code')
    else:
        result.append('Employee')
    result.append("First Name")
    result.append('Last Name')
    result.append('Email')
    if settings.PROJECT == 'ais':
        result.append('BUH')
        result.append('BU')
    result.append('Department')
    if settings.PROJECT == 'ais':
        result.append('Section')
    result.append('Line Manager')
    result.append('Section Course')
    result.append('Content Type')
    result.append('Content Name')
    result.append('Content Status')
    result.append('Progress %')
    result.append('Interval')
    result.append('Score')
    result.append('Score %')
    result.append('Progress Time')
    return result


def _progress_material(course, PROGRESS_MATERIAL):
    progress_material_list = list(Progress.objects.filter(
        content_type=settings.CONTENT_TYPE('course', 'material'),
        content__in=Material.objects.values_list(
            'id', flat=True
        ).filter(
            course=course,
            type__in=[0, 1, 2, 4, 5]
        )
    ))
    for _ in progress_material_list:
        key = '%s_%s' % (_.content, _.item_id)
        PROGRESS_MATERIAL[key] = _


def _progress_material_question(course, PROGRESS_MATERIAL_QUESTION):
    activity_id_list = Material.objects.values_list(
        'question_id', flat=True
    ).filter(
        course=course,
        type=3
    )
    progress_material_question_list = list(Progress.objects.filter(
        content_type=settings.CONTENT_TYPE('question', 'activity'),
        content__in=activity_id_list
    ))
    for _ in progress_material_question_list:
        key = '%s_%s' % (_.content, _.item_id)
        PROGRESS_MATERIAL_QUESTION[key] = _
    return activity_id_list


def _video_log(course, VIDEO_LOG):
    video_log_list = list(VideoLog.objects.filter(
        material_id__in=Material.objects.values_list(
            'id', flat=True
        ).filter(
            course=course,
            type__in=[0, 4]
        )
    ))
    for _ in video_log_list:
        key = '%s_%s_%s_%s' % (_.item_id, _.progress_id, _.account_id, _.material_id)
        if key in VIDEO_LOG:
            VIDEO_LOG[key] += _.credit_use
        else:
            VIDEO_LOG[key] = _.credit_use


def _score(score_list, content_location, account, order_item):
    for _ in score_list:
        if _.item_id == order_item.id and _.location_id == content_location.id and _.account_id == account.id and _.status in [
            2, 3, 5]:
            return _
    return None
