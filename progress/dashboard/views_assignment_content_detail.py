from django.shortcuts import render, get_object_or_404
from django.conf import settings

from assignment.models import Assignment, Item, Result
from account.models import Account
from progress.models import Progress
from order.models import Item as OrderItem

from .views import pull_breadcrumb
from utils.paginator import paginator
from utils.content import get_content

def assignment_content_detail_view(request, assignment_id, item_id):
    assignment = get_object_or_404(Assignment, id=assignment_id)
    item = get_object_or_404(Item, id=item_id)
    
    result_list = Result.objects.filter(assignment=assignment,
                                        item=item,
                                        status=1)
    progress_list = []
    for result in result_list:
        if result.order_item is None:
            continue
        order_item = OrderItem.pull(result.order_item_id)
        progress = Progress.pull(order_item,
                                 None,
                                 settings.CONTENT_TYPE_ID(result.item.content_type_id),
                                 result.item.content)
        progress.url = '#'
        progress_list.append(progress)

    return render(request,
                  'progress/dashboard/content_detail.html',
                  {
                      'SIDEBAR': 'assignment',
                      'BREADCRUMB_LIST': pull_breadcrumb({'assignment_id': assignment.id, 'item_id': item.id}),
                      'progress_list': progress_list,
                  })
