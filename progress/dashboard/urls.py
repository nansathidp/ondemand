from django.conf.urls import url

from .views_account import account_view
from .views_account_detail import account_detail_view
from .views_account_detail_load import account_detail_load_view
from .views_assignment_account import assignment_account_view
from .views_assignment_account_detail import assignment_account_detail_view
from .views_assignment_content import assignment_content_view
from .views_assignment_content_detail import assignment_content_detail_view
from .views_content import content_course_view, content_question_view, content_program_view, content_onboard_view
from .views_content_account import content_account_view, account_content_progress_view
from .views_content_detail import content_detail_course_view, content_detail_question_view, content_detail_program_view, \
    content_detail_onboard_view
from .views_course_material import course_material_view, \
    course_material_account_view, course_material_account_program_view, \
    course_material_program_view, course_material_onboard_view
from .views_detail import detail_account_view, detail_account_program_view, detail_account_course_view, \
    detail_account_program_course_view, \
    detail_course_view, \
    detail_course_question_view, detail_course_question_program_view, detail_course_question_onboard_view, \
    detail_question_view, \
    detail_program_view, detail_program_item_view, \
    detail_onboard_view, detail_onboard_item_view
from .views_export import export_report_account, export_report_course, export_report_test, \
    export_report_program, export_report_onboard, export_report_course_detail, export_report_test_detail
from .views_graph import graph_account_view, graph_course_view, graph_question_view, graph_program_view, \
    graph_onboard_view
from .views_graph_detail import graph_detail_account_view, graph_detail_course_view, graph_detail_question_view, \
    graph_detail_program_view, graph_detail_onboard_view
from .views_question_score import question_score_view, \
    question_score_account_view, question_score_account_program_view, question_score_account_course_view, \
    question_score_account_program_course_view, \
    question_score_course_view, \
    question_score_program_view, question_score_program_course_view, \
    question_score_onboard_view, question_score_onboard_course_view

app_name = 'progress'
urlpatterns = [
    url(r'^(\d+)/account/$', detail_account_view, name='detail_account'),
    url(r'^(\d+)/account/program/(\d+)/$', detail_account_program_view, name='detail_account_program'),
    url(r'^(\d+)/account/course/(\d+)/$', detail_account_course_view, name='detail_account_course'),
    url(r'^(\d+)/account/program/(\d+)/course/(\d+)/$', detail_account_program_course_view,
        name='detail_account_program_course'),

    url(r'^(\d+)/course/(\d+)/$', detail_course_view, name='detail_course'),
    url(r'^(\d+)/course/(\d+)/question/$', detail_course_question_view, name='detail_course_question'),
    url(r'^(\d+)/course/(\d+)/question/program/(\d+)/$', detail_course_question_program_view,
        name='detail_course_question_program'),
    url(r'^(\d+)/course/(\d+)/question/onboard/(\d+)/$', detail_course_question_onboard_view,
        name='detail_course_question_onboard'),

    url(r'^(\d+)/question/(\d+)/$', detail_question_view, name='detail_question'),

    url(r'^(\d+)/program/(\d+)/$', detail_program_view, name='detail_program'),
    url(r'^(\d+)/program/(\d+)/(\d+)/$', detail_program_item_view, name='detail_program_item'),
    url(r'^(\d+)/onboard/(\d+)/$', detail_onboard_view, name='detail_onboard'),
    url(r'^(\d+)/onboard/(\d+)/(\d+)/$', detail_onboard_item_view, name='detail_onboard_item'),

    url(r'^(\d+)/course/material/(\d+)/$', course_material_view, name='course_material'),
    url(r'^(\d+)/course/material/(\d+)/account/$', course_material_account_view, name='course_material_account'),
    url(r'^(\d+)/course/material/(\d+)/account/program/(\d+)/$', course_material_account_program_view,
        name='course_material_account_program'),

    url(r'^(\d+)/course/material/(\d+)/program/(\d+)/$', course_material_program_view, name='course_material_program'),
    url(r'^(\d+)/course/material/(\d+)/onboard/(\d+)/$', course_material_onboard_view, name='course_material_onboard'),

    url(r'^(\d+)/question/score/(\d+)/$', question_score_view, name='question_score'),
    url(r'^(\d+)/question/score/(\d+)/account/$', question_score_account_view, name='question_score_account'),
    url(r'^(\d+)/question/score/(\d+)/account/program/(\d+)/$',
        question_score_account_program_view, name='question_score_account_program'),
    url(r'^(\d+)/question/score/(\d+)/account/course/(\d+)/$',
        question_score_account_course_view, name='question_score_account_course'),
    url(r'^(\d+)/question/score/(\d+)/account/program/(\d+)/course/(\d+)/$',
        question_score_account_program_course_view, name='question_score_account_program_course'),

    url(r'^(\d+)/question/score/(\d+)/course/(\d+)/$',
        question_score_course_view, name='question_score_course'),
    url(r'^(\d+)/question/score/(\d+)/program/(\d+)/$',
        question_score_program_view, name='question_score_program'),
    url(r'^(\d+)/question/score/(\d+)/program/(\d+)/course/(\d+)/$',
        question_score_program_course_view, name='question_score_program_course'),
    url(r'^(\d+)/question/score/(\d+)/onboard/(\d+)/$',
        question_score_onboard_view, name='question_score_onboard'),
    url(r'^(\d+)/question/score/(\d+)/onboard/(\d+)/course/(\d+)/$',
        question_score_onboard_course_view, name='question_score_onboard_course'),

    url(r'^account/$', account_view, name='account'),
    url(r'^account/(\d+)/$', account_detail_view, name='account_detail'),
    url(r'^account/(\d+)/(\w+)/(\d+)/(\d+)/$', account_content_progress_view, name='account_content_progress'),

    url(r'^account/(\d+)/load/$', account_detail_load_view, name='account_detail_load'),

    # url(r'^course/(\d+)/(\d+)/material/(\d+)/(\d+)/$', course_material_view, name='course_material'),

    url('^assignment/(\d+)/account/$', assignment_account_view, name='assignment_account'),
    url('^assignment/(\d+)/account/(\d+)/$', assignment_account_detail_view, name='assignment_account_detail'),
    url('^assignment/(\d+)/content/$', assignment_content_view, name='assignment_content'),
    url('^assignment/(\d+)/content/(\d+)/$', assignment_content_detail_view, name='assignment_content_detail'),

    url(r'^course/$', content_course_view, name='content_course'),
    url(r'^question/$', content_question_view, name='content_question'),
    url(r'^program/$', content_program_view, name='content_program'),
    url(r'^onboard/$', content_onboard_view, name='content_onboard'),
    # url(r'^login/$', content_login_view, name='content_login'),

    url(r'^account/report$', export_report_account, name='content_account_report'),
    url(r'^course/report$', export_report_course, name='content_course_report'),
    url(r'^course/(\d+)/report$', export_report_course_detail, name='content_course_detail_report'),
    url(r'^question/report$', export_report_test, name='content_question_report'),
    url(r'^question/(\d+)/report$', export_report_test_detail, name='content_question_detail_report'),
    url(r'^program/report$', export_report_program, name='content_program_report'),
    url(r'^onboard/report$', export_report_onboard, name='content_onboard_report'),

    url(r'^account/graph/$', graph_account_view, name='graph_account'),
    url(r'^course/graph/$', graph_course_view, name='graph_course'),
    url(r'^question/graph/$', graph_question_view, name='graph_question'),
    url(r'^program/graph/$', graph_program_view, name='graph_program'),
    url(r'^onboard/graph/$', graph_onboard_view, name='graph_onboard'),

    url(r'^course/(\d+)/$', content_detail_course_view, name='content_detail_course'),
    url(r'^question/(\d+)/$', content_detail_question_view, name='content_detail_question'),
    url(r'^program/(\d+)/$', content_detail_program_view, name='content_detail_program'),
    url(r'^onboard/(\d+)/$', content_detail_onboard_view, name='content_detail_onboard'),

    url(r'^account/(\d+)/graph/$', graph_detail_account_view, name='graph_detail_account'),
    url(r'^course/(\d+)/graph/$', graph_detail_course_view, name='graph_detail_course'),
    url(r'^question/(\d+)/graph/$', graph_detail_question_view, name='graph_detail_question'),
    url(r'^program/(\d+)/graph/$', graph_detail_program_view, name='graph_detail_program'),
    url(r'^onboard/(\d+)/graph/$', graph_detail_onboard_view, name='graph_detail_onboard'),

    # url(r'^(\w+)/$', content_view, name='content'),
    # url(r'^(\w+)/(\d+)/$', content_detail_view, name='content_detail'),
    url(r'^(\w+)/(\d+)/account/(\d+)/(\d+)/$', content_account_view, name='content_account'),

    # url(r'^onboard/(\d+)/account/(\d+)/clean/(\d+)/$', onboard_account_clean_view, name='onboard_account_clean'),

    # url(r'^update/$', update_view, name='update'),
    # url(r'^course/(\d+)/$', course_view, name='course'),
]
