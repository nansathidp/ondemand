from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404

from account.models import Account
from progress.models import Progress, Account as ProgressAccount

from utils.paginator import paginator
from utils.filter import get_q
from utils.breadcrumb import get_breadcrumb
from ..models import Progress, Account as ProgressAccount
from .views_account import cal_percent

@csrf_exempt
def account_detail_load_view(request, account_id):
    if request.user.has_perm('progress.view_progress',
                             group=request.DASHBOARD_GROUP):
        account = get_object_or_404(Account, id=account_id)
    elif request.user.has_perm('progress.view_own_progress',
                               group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    elif request.user.has_perm('progress.view_org_progress',
                               group=request.DASHBOARD_GROUP):
        account = get_object_or_404(Account, id=account_id)
        if settings.IS_ORGANIZATION:
            if not account.org_child_set.filter(account=request.user).exists():
                raise PermissionDenied
    else:
        raise PermissionDenied

    progress_list = Progress.objects.filter(account=account, is_root=True)
    q_status = get_q(request, 'q_status')
    q_content_type = get_q(request, 'q_content_type', type='str')
    if q_status != -1:
        progress_list = progress_list.filter(status=q_status)
    if q_content_type != -1:
        if q_content_type == 'course':
            content_type = settings.CONTENT_TYPE('course', 'course')
        elif q_content_type == 'question':
            content_type = settings.CONTENT_TYPE('question', 'activity')
        elif q_content_type == 'program':
            content_type = settings.CONTENT_TYPE('program', 'program')
        else:
            content_type = None
        if content_type is not None:
            progress_list = progress_list.filter(content_type=content_type)
    progress_list = paginator(request, progress_list)
    return render(request,
                  'progress/dashboard/account_detail_block.html',
                  {
                      'progress_list': progress_list
                  })
