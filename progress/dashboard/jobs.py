import datetime
import json
import uuid

from dateutil import tz
from django.conf import settings
from django.template.loader import render_to_string
from django.utils import timezone
from xlwt import *

from account.models import Account
from alert.models import Alert
from content.models import Location as ContentLocation
from course.models import Course, Material
from department.models import Member as DepartmentMember
from mailer.models import Mailer
from organization.models import Parent
from progress.models import Progress, Content as ProgressContent, VideoLog
from provider.models import Provider
from question.models import Activity, Score

if settings.PROJECT == 'ais':
    from ais.models import AccountInfo
if settings.IS_SUB_PROVIDER:
    from subprovider.models import SubProvider

add_palette_colour("colour_col_4", 0x19)
add_palette_colour("colour_col_5", 0x20)
add_palette_colour("colour_col_6", 0x21)
add_palette_colour("colour_col_7", 0x22)

style = easyxf("borders: left thin, right thin, top thin, bottom thin;")
style_1 = easyxf("align: horiz center; borders: left thin, right thin, top thin, bottom thin;")
style_col_4 = easyxf(
    "align: horiz center; pattern: pattern solid, fore_colour colour_col_4; borders: left thin, right thin, top thin, bottom thin;")
style_col_5 = easyxf(
    "align: horiz center; pattern: pattern solid, fore_colour colour_col_5; borders: left thin, right thin, top thin, bottom thin;")
style_col_6 = easyxf(
    "align: horiz center; pattern: pattern solid, fore_colour colour_col_6; borders: left thin, right thin, top thin, bottom thin;")
style_col_7 = easyxf(
    "align: horiz center; pattern: pattern solid, fore_colour colour_col_7; borders: left thin, right thin, top thin, bottom thin;")


def alert_fail(alert):
    alert.status = -1
    alert.save()


def get_account_info(account, ACCOUNT_INFO):
    data = {}
    if settings.PROJECT == 'ais':
        account_info = ACCOUNT_INFO[account.id]
        if account_info is not None:
            data['employee'] = account_info.employee
            data['buh'] = account_info.buh
            data['bu'] = account_info.bu
            data['section'] = account_info.section
        else:
            data['employee'] = ''
            data['buh'] = ''
            data['bu'] = ''
            data['section'] = ''
    else:
        data['employee'] = account.employee
        data['buh'] = ''
        data['bu'] = ''
        data['section'] = ''
    return data


def get_account_department(account_id_list, account_department):
    department_member_list = list(DepartmentMember.objects.filter(
        account_id__in=account_id_list
    ).select_related('department'))
    for _ in account_id_list:
        for department_member in department_member_list:
            if _ == department_member.account_id:
                account_department[_] = department_member.department.name
                break
        if not _ in account_department:
            account_department[_] = '-'


def get_account_direct_manager(account_id_list, account_direct_manager):
    parent_list = list(Parent.objects.filter(
        account_id__in=account_id_list
    ).values_list('account_id', 'parent__email'))
    for _ in account_id_list:
        for parent in parent_list:
            if _ == parent[0]:
                account_direct_manager[_] = parent[1]
                break
        if not _ in account_direct_manager:
            account_direct_manager[_] = '-'


def _progress_material(material, progress_material_list, order_item):
    for _ in progress_material_list:
        if _.content == material.id and _.item_id == order_item.id:
            progress_material_list.remove(_)
            return _
    return None


def _progress_material_question(material, progress_material_list, order_item):
    for _ in progress_material_list:
        if _.content == material.question_id and _.item_id == order_item.id:
            progress_material_list.remove(_)
            return _
    return None


def _video_log_first(video_log_list, order_item, progress, account, material):
    for _ in video_log_list:
        if _.item_id == order_item.id and _.progress_id == progress.id and _.account_id == account.id and _.material_id == material.id:
            return _
    return None


def _video_log_sum(video_log_list, order_item, progress, account, material):
    result = datetime.timedelta(0)
    count = 0
    for _ in video_log_list:
        if _.item_id == order_item.id and _.progress_id == progress.id and _.account_id == account.id and _.material_id == material.id:
            result += _.credit_use
            video_log_list.pop(count)
        else:
            count += 1
    return result


def make_xls_detail(content_list, columns, alert):
    workbook = Workbook()
    workbook.set_colour_RGB(0x19, 253, 236, 202)
    workbook.set_colour_RGB(0x20, 207, 222, 186)
    workbook.set_colour_RGB(0x21, 250, 192, 195)
    workbook.set_colour_RGB(0x22, 232, 232, 232)
    worksheet = workbook.add_sheet("Progress Report")

    for index in range(len(columns)):
        worksheet.col(index).width = 15 * 256
        if columns[index] == 'Course Name' or columns[index] == 'Test Name' or columns[index] == 'Category':
            worksheet.col(index).width = 55 * 256
        elif columns[index] == 'Progress Time' or columns[index] == 'Enroll Time' or columns[index] == 'Expired Date' or \
                        columns[index] == 'Submit Date' or columns[index] == 'Added Date':
            worksheet.col(index).width = 24 * 256
        elif columns[index] == 'Email' or columns[index] == 'Line Manager':
            worksheet.col(index).width = 24 * 256
        elif columns[index] == 'Department' or columns[index] == "First Name" or columns[index] == 'Last Name':
            worksheet.col(index).width = 24 * 256

    num_row = 0
    pin = 0
    progree = 0
    score = 0
    for col in range(len(columns)):
        if columns[index] == 'PIN Code' or columns[index] == 'Employee':
            worksheet.write(num_row, col, columns[col], style)
            pin = index
        elif columns[index] == 'Progress %':
            worksheet.write(num_row, col, columns[index], style_col_7)
            progree = index
        elif columns[index] == 'Score %':
            worksheet.write(num_row, col, columns[col], style_col_7)
            score = index
        else:
            worksheet.write(num_row, col, columns[col], style_1)

    for content in range(len(content_list)):
        num_row += 1
        row = content_list[content]

        for col_num in range(len(row)):
            if col_num == pin:
                worksheet.write(num_row, col_num, str(row[col_num]), style)
            elif col_num == progree:
                worksheet.write(num_row, col_num, str(row[col_num]), style_col_7)
            elif col_num == score:
                worksheet.write(num_row, col_num, str(row[col_num]), style_col_7)
            else:
                worksheet.write(num_row, col_num, str(row[col_num]), style_1)

    alert.filename = '%s.xls' % (uuid.uuid4())
    workbook.save('%s/%s' % (alert.get_export_path(), alert.filename))
    alert.status = 2
    alert.save()
    return send_to_mail(alert, alert.filename)


def make_xls(content_list, columns, alert, content_type, SIDEBAR=''):
    kwargs = json.loads(alert.json_kwargs)
    for content in content_list:
        content.progress_content = ProgressContent.pull(
            ContentLocation.pull_first(
                content_type,
                content.id
            ).id,
            content_type.id,
            content.id
        )

    workbook = Workbook()
    workbook.set_colour_RGB(0x19, 253, 236, 202)
    workbook.set_colour_RGB(0x20, 207, 222, 186)
    workbook.set_colour_RGB(0x21, 250, 192, 195)
    workbook.set_colour_RGB(0x22, 232, 232, 232)
    worksheet = workbook.add_sheet("Progress Report")

    if SIDEBAR == 'progress-program' or SIDEBAR == '':
        worksheet.col(2).width = 24 * 256
        worksheet.col(3).width = 18 * 256
    else:  # onboard
        worksheet.col(2).width = 18 * 256
        worksheet.col(3).width = 10 * 256
    worksheet.col(0).width = 55 * 256
    worksheet.col(4).width = 14 * 256
    worksheet.col(5).width = 15 * 256
    worksheet.col(6).width = 13 * 256
    worksheet.col(7).width = 8 * 256
    worksheet.col(8).width = 10 * 256
    worksheet.col(9).width = 24 * 256

    row_num = 0
    q_name = kwargs['name'].strip()
    if len(q_name) > 0:
        worksheet.write(row_num, 0, 'Search by: %s' % q_name, style_1)
        row_num += 1

    for col_num in range(len(columns)):
        if col_num == 5:
            worksheet.write(row_num, col_num, columns[col_num], style_col_4)
        elif col_num == 6:
            worksheet.write(row_num, col_num, columns[col_num], style_col_5)
        elif col_num == 6:
            worksheet.write(row_num, col_num, columns[col_num], style_col_6)
        elif col_num == 7:
            worksheet.write(row_num, col_num, columns[col_num], style_col_6)
        elif col_num == 8:
            worksheet.write(row_num, col_num, columns[col_num], style_col_7)
        else:
            worksheet.write(row_num, col_num, columns[col_num], style_1)
    for content in content_list:
        row_num += 1

        if SIDEBAR == 'progress-program' or SIDEBAR == '':
            col_1 = content.category.name
            time_zone = timezone.localtime(content.timestamp, tz.tzlocal())
            col_2 = time_zone.strftime('%Y-%m-%d %H:%M:%S')
        else:
            col_2 = content.version
            time_zone = timezone.localtime(content.timestamp, tz.tzlocal())
            col_1 = time_zone.strftime('%Y-%m-%d %H:%M:%S')

        row = [content.name, col_1, col_2, content.progress_content.count, content.progress_content.in_progress,
               content.progress_content.completed, content.progress_content.failed, content.progress_content.expired,
               str(content.progress_content.get_percent()) + "%"]

        for col_num in range(len(row)):
            if col_num == 0:
                worksheet.write(row_num, col_num, row[col_num], style)
            elif col_num == 5:
                worksheet.write(row_num, col_num, row[col_num], style_col_4)
            elif col_num == 6:
                worksheet.write(row_num, col_num, row[col_num], style_col_5)
            elif col_num == 6:
                worksheet.write(row_num, col_num, row[col_num], style_col_6)
            elif col_num == 7:
                worksheet.write(row_num, col_num, row[col_num], style_col_6)
            elif col_num == 8:
                worksheet.write(row_num, col_num, row[col_num], style_col_7)
            else:
                worksheet.write(row_num, col_num, row[col_num], style_1)

    alert.filename = '%s.xls' % (uuid.uuid4())
    workbook.save('%s/%s' % (alert.get_export_path(), alert.filename))
    alert.status = 2
    alert.save()
    return send_to_mail(alert, alert.filename)


def send_to_mail(alert, filename):
    download_link = render_to_string(
        'progress/dashboard/generate_download_link.html',
        {
            'SITE_URL': settings.SITE_URL,
            'alert': alert,
            'filename': filename,
        }
    )

    Mailer.push_send(
        settings.MAILER_SENDER,
        alert.account.email,
        'Export Progress Report',
        download_link,
        9
    )


def _datetime(value):
    if value is None or value == '-':
        return '-'
    else:
        current_tz = timezone.get_current_timezone()
        _ = current_tz.normalize(value.astimezone(current_tz))
        return _.strftime('%Y-%m-%d %H:%M:%S')


def _percent(value):
    if value is None or type(value) == type(''):
        return '-'
    else:
        if value > 100.0:
            value = 100.0
        return '{0:.1f}%'.format(value)


def set_format_percent(percent=None):
    if percent is None:
        return '-'
    else:
        return '{0:.1f}%'.format(percent)


def set_date_local(date):
    if date is None or date == '-':
        date = '-'
    else:
        current_tz = timezone.get_current_timezone()
        date = current_tz.normalize(date.astimezone(current_tz))
        date = date.strftime('%Y-%m-%d %H:%M:%S')
    return date


def set_format_percent(percent=None):
    if percent is None:
        return '-'
    else:
        if percent > 100.0:
            percent = 100.0
        return '{0:.1f}%'.format(percent)


def compare_name(name):
    import re
    from django.utils import timezone

    # Compile word english
    regex = re.compile('\w')
    rename_list = regex.findall(name)
    rename = ''

    for _ in rename_list:
        rename += _

    # compile word thai
    if rename is None or rename == '':
        th = u'[\u0E01-\u0E59]+'
        rename = re.search(th, name, re.U)
    _timezone = timezone.localtime(timezone.now(), tz.tzlocal())
    date = _timezone.strftime('%Y-%m-%d-%H-%M')
    return rename + '_' + date
