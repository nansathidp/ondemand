from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.conf import settings

from ..models import Progress, VideoLog
from program.models import Program
from order.models import Item
from account.models import Account
from question.models import Answer, Score, ScoreSection
from task.models import Task, Approve

from ..cached import cached_progress_delete

def onboard_account_clean_view(request, program_id, account_id, order_item_id):
    if request.user.has_perm('progress.view_course'):
        program = get_object_or_404(Program.objects.select_related('provider', 'account'), id=program_id)
    elif request.user.has_perm('progress.view_own_course'):
        #TODO: chack permission
        program = get_object_or_404(Program.objects.select_related('provider', 'account'), id=program_id)
    else:
        raise PermissionDenied

    #content_type_program = settings.CONTENT_TYPE('program', 'program')
    #content_type_program_item = settings.CONTENT_TYPE('program', 'item')
    content_type_task = settings.CONTENT_TYPE('task', 'task')
            
    account = get_object_or_404(Account, id=account_id)
    order_item = get_object_or_404(Item, id=order_item_id)
    
    Answer.objects.filter(item=order_item).delete()
    Score.objects.filter(item=order_item).delete()
    ScoreSection.objects.filter(item=order_item).delete()

    VideoLog.objects.filter(item=order_item).delete()

    Approve.objects.filter(item=order_item).delete()
                      
    for progress in Progress.objects.filter(item=order_item):
        cached_progress_delete(progress)
        progress.delete()

    for item in program.item_set.all():
        if item.content_type_id == content_type_task.id:
            Task.push_approve(order_item,
                              item.content,
                              order_item.content,
                              account.id)
            
    Progress.pull(order_item,
                  None,
                  settings.CONTENT_TYPE_ID(order_item.content_type_id),
                  order_item.content)
    return redirect('dashboard:progress:onboard_detail', program.id)
