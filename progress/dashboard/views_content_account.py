from django.conf import settings
from django.shortcuts import render, get_object_or_404

from progress.models import Progress
from account.models import Account
from course.models import Course
from order.models import Item as OrderItem
from content.models import Location as ContentLocation

from utils.breadcrumb import get_breadcrumb
from utils.content_type import get_content_type_name

from .views_content import get_model, get_content_type

def _content_account(request, content_type, content, account, progress):
    
    content_location = get_object_or_404(ContentLocation, id=progress.location_id)
    
    order_item = OrderItem.pull(progress.item_id)

    #Breadcrumb
    request.content_location = content_location
    request.content = content
    request.account = account

    content_type_name = get_content_type_name(content_type)
    request.content_type = content_type
    request.content_type_name = content_type_name
    request.code = code
    request.content_id = content_id
    return render(request,
                  'progress/dashboard/content_account.html',
                  {
                      'SIDEBAR': 'progress-%s' % code,
                      'BREADCRUMB_LIST': get_breadcrumb(request),
                      'TAB': 'course',
                      'content': content,
                      'content_type': content_type,
                      'content_type_name': content_type_name,
                      'account': account,
                      'progress': progress,
                      'content_location': content_location,
                      'order_item': order_item
                  })

def content_account_view(request, code, content_id, account_id, progress_id):
    Model = get_model(code)
    content_type = get_content_type(code)

    content = Model.pull(content_id)
    account = Account.pull(account_id)
    progress = get_object_or_404(Progress, id=progress_id, account=account, content=content_id)

def account_content_progress_view(request, account_id, code, content_id, progress_id):
    pass
