from django.shortcuts import redirect
from django.core.exceptions import PermissionDenied

from ..models import Progress

def update_view(request):
    if not request.user.has_perm('progress.view_progress'):
        raise PermissionDenied

    for progress in Progress.objects.filter(is_update=False):
        progress.content_update()
        
    ref = request.META.get('HTTP_REFERER', None)
    if ref is not None:
        return redirect(ref)
    return redirect('dashboard:home')
