import json

from django.conf import settings
from django.utils import timezone

from alert.models import Alert
from progress.models import Progress
from provider.models import Provider
from question.models import Activity, Score
from .jobs import alert_fail
from .jobs import get_account_department, get_account_direct_manager
from .jobs import make_xls_detail, _datetime, get_account_info, _percent

if settings.PROJECT == 'ais':
    from ais.models import AccountInfo
if settings.IS_SUB_PROVIDER:
    from subprovider.models import SubProvider


def export_question_detail_job(alert_id):
    try:
        alert = Alert.objects.get(id=alert_id)
    except:
        return None

    if alert.content == -1:
        alert_fail(alert)
        return None
    else:
        activity = Activity.pull(alert.content)
        if alert is None:
            alert_fail(alert)
            return None

    ACCOUNT_INFO = {}
    account_department = {}
    account_direct_manager = {}
    account_id_list = set()

    kwargs = json.loads(alert.json_kwargs)
    if not all([_ in kwargs for _ in ['name', 'perm', 'provider', 'sub_provider']]):
        alert_fail(alert)
        return None

    if kwargs['provider']:
        provider = Provider.pull(kwargs['provider'])
    else:
        provider = None
    if kwargs['sub_provider']:
        sub_provider = SubProvider.pull(kwargs['sub_provider'])
    else:
        sub_provider = None

    content_type = settings.CONTENT_TYPE('question', 'activity')
    if kwargs['perm'] == 'view':
        progress_list = Progress.objects.filter(
            content_type=content_type,
            content=activity.id,
            item__status=2,
            is_root=True
        ).select_related(
            'account',
            'item'
        )
    elif kwargs['perm'] == 'own':
        if provider:
            if activity.provider_id != provider.id:
                alert_fail(alert)
                return None
        elif settings.IS_SUB_PROVIDER and sub_provider:
            if activity.provider_id != sub_provider.provider_id:
                alert_fail(alert)
                return None
        else:
            alert_fail(alert)
            return None
        progress_list = Progress.objects.filter(
            content_type=content_type,
            content=activity.id,
            item__status=2,
            is_root=True
        ).select_related(
            'account',
            'item'
        )
    else:
        alert_fail(alert)
        return None

    report_list = []
    if getattr(settings, 'ACCOUNT__HIDE_INACTIVE', None) is None:
        settings.ACCOUNT__HIDE_INACTIVE = False
    if getattr(settings, 'ACCOUNT__HIDE_ID_LIST', None) is None:
        settings.ACCOUNT__HIDE_ID_LIST = []
    if settings.ACCOUNT__HIDE_INACTIVE:
        progress_list = progress_list.exclude(account__is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        progress_list = progress_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)

    score_dict = {}
    material = None
    _score(activity, material, score_dict)

    for _ in progress_list:
        if not _.account_id in account_id_list:
            account_id_list.add(_.account_id)
    get_account_department(account_id_list, account_department)
    get_account_direct_manager(account_id_list, account_direct_manager)

    if settings.PROJECT == 'ais':
        account_info_list = list(AccountInfo.objects.filter(
            account_id__in=account_id_list
        ))
        for _ in account_id_list:
            for account_info in account_info_list:
                if _ == account_info.account_id:
                    ACCOUNT_INFO[_] = account_info
                    break
            if not _ in ACCOUNT_INFO:
                ACCOUNT_INFO[_] = None

    count = 0
    start = a = timezone.now()
    for progress in progress_list:
        print('debug: ', progress)
        count += 1
        account = progress.account
        order_item = progress.item
        account_info = get_account_info(account, ACCOUNT_INFO)

        expired_date = order_item.expired_date()
        expired_day = order_item.expired_day()

        if material:
            key = '%s_%s' % (progress.item_id, material.id)
        else:
            key = '%s' % (progress.item_id)

        if key in score_dict:
            for score in score_dict[key]:
                data = {
                    'name': activity.name,
                    'category': activity.category.name,
                    'add_by': order_item.order.get_method_display(),
                    'timestamp': order_item.timestamp,
                    'expired_date': expired_date,
                    'day_left': expired_day,
                    'employee': account_info['employee'],
                    'account': account,
                    'section': account_info['section'],
                    'progress': progress,
                    'score': score,
                }
                report_list.append(pack_data(data, account_department, account_direct_manager))
        else:
            data = {
                'name': activity.name,
                'category': activity.category.name,
                'add_by': order_item.order.get_method_display(),
                'timestamp': order_item.timestamp,
                'expired_date': expired_date,
                'day_left': expired_day,
                'employee': account_info['employee'],
                'account': account,
                'section': account_info['section'],
                'progress': None,
                'score': None
            }
            report_list.append(pack_data(data, account_department, account_direct_manager))

    return make_xls_detail(
        report_list,
        get_column_list(),
        alert
    )


def pack_data(data, account_department, account_direct_manager):
    row_xls = []
    row_xls.append(data['name'])
    row_xls.append(data['category'])
    row_xls.append(data['add_by'])
    row_xls.append(_datetime(data['timestamp']))

    row_xls.append(data['expired_date'])
    row_xls.append(data['day_left'])
    row_xls.append('-')
    row_xls.append(data['employee'])
    row_xls.append(data['account'].first_name)
    row_xls.append(data['account'].last_name)
    row_xls.append(data['account'].email)

    row_xls.append(account_department[data['account'].id])
    if settings.PROJECT == 'ais':
        row_xls.append(data['section'])
    row_xls.append(account_direct_manager[data['account'].id])

    if data['progress']:
        row_xls.append(data['progress'].get_status_display())
        row_xls.append(_percent(data['progress'].percent))
    else:
        row_xls.append('-')
        row_xls.append('-')

    if data['score']:
        interval = '%s/%s' % (data['score'].submit_number, data['score'].max_submit)
        row_xls.append(interval)
        row_xls.append(_datetime(data['score'].timeupdate))
        row_xls.append(data['score'].max_score)
        row_xls.append(data['score'].score)
        row_xls.append(data['score'].get_status_display())
    else:
        row_xls.append('-')
        row_xls.append('-')
        row_xls.append('-')
        row_xls.append('-')
        row_xls.append('-')
    return row_xls


def get_column_list():
    result = []
    result.append('Test Name')
    result.append('Category')
    result.append('Added By')
    result.append('Added Date')
    result.append('Expired Date')
    result.append('Days Left')
    result.append('Status')

    if settings.PROJECT == 'ais':
        result.append('PIN Code')
    else:
        result.append('Employee')
    result.append("First Name")
    result.append('Last Name')
    result.append('Email')
    if settings.PROJECT == 'ais':
        result.append('Section')
    result.append('Department')
    result.append('Line Manager')

    result.append('Progress Status')
    result.append('Progress %')

    result.append('Interval')
    result.append('Submit Date')
    result.append('Full Score')
    result.append('Learner Score')
    result.append('Test Result')
    return result


def _score(activity, material, score_dict):
    score_list = Score.objects.filter(
        activity=activity,
        material=material
    ).order_by('submit_number')
    for score in score_list:
        if material:
            key = '%s_%s' % (score.item_id, material.id)
        else:
            key = '%s' % (score.item_id)
        if key in score_dict:
            score_dict[key].append(score)
        else:
            score_dict[key] = [score]
