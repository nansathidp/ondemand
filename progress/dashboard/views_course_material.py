from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404

from progress.models import Progress
from account.models import Account
from course.models import Course, Material
from order.models import Item as OrderItem
from content.models import Location as ContentLocation
from program.models import Program

from order.cached import cached_order_item_is_purchased
from ..models import VideoLog
from utils.breadcrumb import get_breadcrumb
from utils.content_type import get_content_type_name
from utils.content import get_content

# TODO : check permission
def _course_material(request, progress, progress_course,
                     SIDEBAR, BREADCRUMB_LIST,
                     progress_program=None):
    material = Material.pull(progress.content)
    account = Account.pull(progress.account_id)
    course = Course.pull(progress_course.content)
    order_item = OrderItem.pull(progress.item_id)

    log_list = VideoLog.objects.filter(material=material,
                                       item=order_item).order_by('-timestamp')
    
    return render(request,
                  'progress/dashboard/course_material.html',
                  {
                      'SIDEBAR': SIDEBAR,
                      'BREADCRUMB_LIST': BREADCRUMB_LIST,
                      'course': course,
                      'account': account,
                      'order_item': order_item,
                      'log_list': log_list,
                      'progress': progress,
                      'progress_program': progress_program,
                      'progress_course': progress_course,
                  })

def course_material_view(request, progress_id, progress_course_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)
    return _course_material(request, progress, progress_course,
                            'progress-course',
                            get_breadcrumb(['progress.content_course_view',
                                            'progress.content_detail_course_view',
                                            'progress.detail_course_view',
                                            'progress.course_material_view'],
                                           **{'detail_course__content_type_name': content_type_name,
                                              'detail_course__progress_id': progress_course.id,
                                              'detail_course__content_id': progress_course.content,
                                              'content_id': progress_course.content}))

def course_material_account_view(request, progress_id, progress_course_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)
    return _course_material(request, progress, progress_course,
                            'progress-account',
                            get_breadcrumb(['progress.account_view',
                                            'progress.account_detail_view',
                                            'progress.detail_account_view',
                                            'progress.course_material_account_view'],
                                           **{'account_id': progress.account_id,
                                              'detail_account__content_type_name': get_content_type_name(settings.CONTENT_TYPE('course', 'course')),
                                              'detail_account__progress_id': progress_course.id}))

def course_material_account_program_view(request, progress_id, progress_course_id, progress_program_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)
    progress_program = get_object_or_404(Progress, id=progress_program_id)
    return _course_material(request, progress, progress_course,
                            'progress-account',
                            get_breadcrumb(['progress.account_view',
                                            'progress.account_detail_view',
                                            'progress.detail_account_view',
                                            'progress.detail_account_program_view',
                                            'progress.course_material_account_program_view'],
                                           **{'account_id': progress.account_id,
                                              'detail_account__content_type_name': 'program',
                                              'detail_account__progress_id': progress_program.id,
                                              'detail_account_program__content_type_name': 'course',
                                              'detail_account_program__progress_id': progress_course.id,
                                              'detail_account_program__progress_program_id': progress_program.id}),
                            progress_program=progress_program)

def course_material_program_view(request, progress_id, progress_course_id, progress_program_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)
    progress_program = get_object_or_404(Progress, id=progress_program_id)

    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_name_course = get_content_type_name(content_type_course)

    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_type_name_program = get_content_type_name(content_type_program)
    return _course_material(request, progress, progress_course,
                            'progress-program',
                            get_breadcrumb(['progress.content_program_view',
                                            'progress.content_detail_program_view',
                                            'progress.detail_program_view',
                                            'progress.detail_program_item_view',
                                            'progress.course_material_program_view'],
                                           **{'detail_program__content_type_name': content_type_name_program,
                                              'detail_program__progress_id': progress_program.id,
                                              'detail_program__content_id': progress_program.content,
                                              'detail_program_item__content_type_name': content_type_name_course,
                                              'detail_program_item__progress_id': progress_course.id,
                                              'detail_program_item__content_id': progress_program.content,
                                              'detail_program_item__progress_program_id': progress_program.id,
                                              'content_id': progress_course.content}))

def course_material_onboard_view(request, progress_id, progress_course_id, progress_program_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)
    progress_program = get_object_or_404(Progress, id=progress_program_id)

    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_name_course = get_content_type_name(content_type_course)

    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_program = get_content(progress_program.content_type_id, progress_program.content)
    content_type_name_program = get_content_type_name(content_type_program, content=content_program)
    return _course_material(request, progress, progress_course,
                            'progress-onboard',
                            get_breadcrumb(['progress.content_onboard_view',
                                            'progress.content_detail_onboard_view',
                                            'progress.detail_onboard_view',
                                            'progress.detail_onboard_item_view',
                                            'progress.course_material_onboard_view'],
                                           **{'detail_onboard__content_type_name': content_type_name_program,
                                              'detail_onboard__progress_id': progress_program.id,
                                              'detail_onboard__content_id': progress_program.content,
                                              'detail_onboard_item__content_type_name': content_type_name_course,
                                              'detail_onboard_item__progress_id': progress_course.id,
                                              'detail_onboard_item__content_id': progress_program.content,
                                              'detail_onboard_item__progress_onboard_id': progress_program.id,
                                              'content_id': progress_course.content}))
