import inspect
from django.core.urlresolvers import reverse

def _caller(filename, function):
    return '%s.%s'%(filename.split('/')[-3], function)

def _breadcrumb(stack_list, d):
    result = []
    for stack in stack_list:
        if stack == 'assignment.home_view':
            result.append({'is_active': False,
                           'title': 'Assignment Management',
                           'url': reverse('dashboard:assignment:home')})
        elif stack == 'progress.assignment_account_view':
            result.append({'is_active': False,
                           'title': 'Learner Progress',
                           'url': reverse('dashboard:progress:assignment_account', args=[d['assignment_id']])})
        elif stack == 'progress.assignment_account_detail_view':
            result.append({'is_active': False,
                           'title': 'Learner Progress Summary',
                           'url': reverse('dashboard:progress:assignment_account_detail', args=[d['assignment_id'], d['account_id']])})
        elif stack == 'progress.assignment_content_view':
            result.append({'is_active': False,
                           'title': 'Learning Item Progress',
                           'url': reverse('dashboard:progress:assignment_content', args=[d['assignment_id']])})
        elif stack == 'progress.assignment_content_detail_view':
            result.append({'is_active': False,
                           'title': 'Learning Item Progress Summary',
                           'url': reverse('dashboard:progress:assignment_content_detail', args=[d['assignment_id'], d['item_id']])})
            
    if len(result) > 0:
        result[-1]['is_active'] = True
    return result

def pull_breadcrumb(d={}):
    _ = inspect.stack()[1]
    caller = _caller(_[1], _[3])
    if caller == 'progress.assignment_account_view':
        return _breadcrumb(['assignment.home_view', 'progress.assignment_account_view'], d)
    elif caller == 'progress.assignment_account_detail_view':
        return _breadcrumb(['assignment.home_view', 'progress.assignment_account_view', 'progress.assignment_account_detail_view'], d)
    elif caller == 'progress.assignment_content_view':
        return _breadcrumb(['assignment.home_view', 'progress.assignment_content_view'], d)
    elif caller == 'progress.assignment_content_detail_view':
        return _breadcrumb(['assignment.home_view', 'progress.assignment_content_view', 'progress.assignment_content_detail_view'], d)
    else:
        return []
    
