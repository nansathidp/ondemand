import datetime

import django_rq
from django.apps import apps
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render
from django.utils import timezone

from alert.models import Alert
from content.models import Location as ContentLocation
from course.models import Course
from program.models import Program
from progress.jobs import content_progress_hard_job
from progress.models import Content as ProgressContent
from question.models import Activity
from utils.breadcrumb import get_breadcrumb
from utils.content_type import get_content_type_name
from utils.filter import get_q_dashboard
from utils.paginator import paginator


def get_model(code):
    if code == 'course':
        return apps.get_model('course', 'course')
    elif code == 'question':
        return apps.get_model('question', 'activity')
    elif code == 'program' or code == 'onboard':
        return apps.get_model('program', 'program')
    elif code == 'assignment':
        return apps.get_model('assignment', 'assignment')
    else:
        raise Http404


def get_content_type(code):
    if code == 'course':
        return settings.CONTENT_TYPE('course', 'course')
    elif code == 'question':
        return settings.CONTENT_TYPE('question', 'activity')
    elif code == 'program' or code == 'onboard':
        return settings.CONTENT_TYPE('program', 'program')
    elif code == 'assignment':
        return settings.CONTENT_TYPE('assignment', 'assignment')
    else:
        raise Http404


def _content(request, content_list, content_type, content_type_name,
             SIDEBAR, BREADCRUMB_LIST):
    request.q_name = " ".join(get_q_dashboard(request, 'name', 'str').split())
    if request.q_name != '':
        content_list = content_list.filter(name__icontains=request.q_name)

    if request.user.has_perm('progress.view_progress',
                             group=request.DASHBOARD_GROUP):
        progress_content_list = ProgressContent.objects.filter(
            is_root=True,
            content_type=content_type,
            content__in=content_list.values_list('id', flat=True),
        )
    elif request.user.has_perm('progress.view_own_progress', group=request.DASHBOARD_GROUP):
        if request.DASHBOARD_PROVIDER:
            content_list = content_list.filter(provider=request.DASHBOARD_PROVIDER)
            progress_content_list = ProgressContent.objects.filter(
                is_root=True,
                content_type=content_type,
                content__in=content_list.values_list('id', flat=True),
            )
        elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
            from subprovider.models import Item
            content_list = content_list.filter(id__in=Item.objects
                                               .values_list('content', flat=True)
                                               .filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                                       content_type=content_type))
            progress_content_list = ProgressContent.objects.filter(
                is_root=True,
                content_type=content_type,
                content__in=content_list.values_list('id', flat=True),
            )
        else:
            content_list = []
            progress_content_list = ProgressContent.objects.filter(
                is_root=True,
                content_type=content_type,
                content__in=content_list,
            )

    elif request.user.has_perm('progress.view_org_progress',
                               group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    else:
        raise PermissionDenied

    if request.GET.get('hard', '') == 'True':
        django_rq.enqueue(content_progress_hard_job, content_type.id)

    content_list = paginator(request, content_list)
    for content in content_list:
        if content_type.app_label == 'course':
            content.url_detail = reverse('dashboard:progress:content_detail_course', args=[content.id])
        elif content_type.app_label == 'question':
            content.url_detail = reverse('dashboard:progress:content_detail_question', args=[content.id])
        elif content_type.app_label == 'program':
            if SIDEBAR == 'progress-program':
                content.url_detail = reverse('dashboard:progress:content_detail_program', args=[content.id])
            else:
                content.url_detail = reverse('dashboard:progress:content_detail_onboard', args=[content.id])
        else:
            content.url_detail = '#'
        content.progress_content = ProgressContent.pull(
            ContentLocation.pull_first(content_type,
                                       content.id).id,
            content_type.id,
            content.id
        )

    progress_result = ProgressContent.pull_summary(progress_content_list)

    alert = Alert.pull_inprogress(request.user, content_type, -1)
    return render(request,
                  'progress/dashboard/content.html',
                  {
                      'SIDEBAR': SIDEBAR,
                      'BREADCRUMB_LIST': BREADCRUMB_LIST,
                      'content_type': content_type,
                      'content_type_name': content_type_name,
                      'content_list': content_list,
                      'progress_result': progress_result,
                      'q_name': request.q_name,
                      'alert': alert,
                  })


def content_course_view(request):
    content_list = Course.objects.all().order_by('-timestamp')
    content_type = settings.CONTENT_TYPE('course', 'course')
    return _content(request,
                    content_list,
                    content_type,
                    get_content_type_name(content_type),
                    'progress-course',
                    get_breadcrumb(['progress.content_course_view']))


def content_question_view(request):
    content_list = Activity.objects.all().order_by('-timestamp')
    content_type = settings.CONTENT_TYPE('question', 'activity')
    return _content(request,
                    content_list,
                    content_type,
                    get_content_type_name(content_type),
                    'progress-question',
                    get_breadcrumb(['progress.content_question_view']))


def content_program_view(request):
    content_list = Program.objects.filter(type=1).order_by('-timestamp')
    content_type = settings.CONTENT_TYPE('program', 'program')
    return _content(request,
                    content_list,
                    content_type,
                    get_content_type_name(content_type),
                    'progress-program',
                    get_breadcrumb(['progress.content_program_view']))


def content_onboard_view(request):
    content_list = Program.objects.filter(type=2).order_by('-timestamp')
    content_type = settings.CONTENT_TYPE('program', 'onboard')
    return _content(request,
                    content_list,
                    content_type,
                    get_content_type_name(content_type, code='onboard'),
                    'progress-onboard',
                    get_breadcrumb(['progress.content_onboard_view']))
