from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.core.urlresolvers import reverse

from assignment.models import Assignment
from account.models import Account

from .views import pull_breadcrumb
from utils.paginator import paginator
from utils.content import get_content

def assignment_content_view(request, assignment_id):
    assignment = get_object_or_404(Assignment, id=assignment_id)
    item_list = assignment.item_set.all()

    item_list = paginator(request, item_list)
    for item in item_list:
        content = get_content(item.content_type_id, item.content)
        item.image = content.image
        item.name = content.name
        item.timestamp = content.timestamp
        item.category_id = content.category_id
        if item.content_type_id == settings.CONTENT_TYPE('program', 'program').id and content.type == 2:
            item.version = content.version
            
        item.url_detail = reverse('dashboard:progress:assignment_content_detail', args=[assignment.id, item.id])
    
    return render(request,
                  'progress/dashboard/assignment_content.html',
                  {
                      'SIDEBAR': 'assignment',
                      'BREADCRUMB_LIST': pull_breadcrumb({'assignment_id': assignment.id}),
                      'content_list': item_list
                  })
