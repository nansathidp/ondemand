from django.conf import settings
from django.shortcuts import render, get_object_or_404
from django.http import Http404
from django.apps import apps
from django.core.urlresolvers import reverse

from progress.models import Progress
from program.models import Program
from account.models import Account
from course.models import Course
from question.models import Activity
from order.models import Item as OrderItem
from content.models import Location as ContentLocation

from utils.breadcrumb import get_breadcrumb
from utils.content_type import get_content_type_name
from utils.content import get_content

from .views_content import get_model, get_content_type


def _detail(request, progress, content_type, content, content_type_name,
            account_url,
            SIDEBAR, BREADCRUMB_LIST,
            progress_program=None, progress_course=None):
    return render(request,
                  'progress/dashboard/detail.html',
                  {
                      'SIDEBAR': SIDEBAR,
                      'BREADCRUMB_LIST': BREADCRUMB_LIST,
                      'TAB': 'course',
                      'content': content,
                      'content_type': content_type,
                      'content_type_name': content_type_name,
                      'account': Account.pull(progress.account_id),
                      'account_url': account_url,
                      'progress': progress,
                      'progress_program': progress_program,
                      'progress_course': progress_course,
                      'content_location': ContentLocation.pull(progress.location_id),
                      'order_item': OrderItem.pull(progress.item_id)
                  })


def _get_model_content_type_id(content_type_id):
    if content_type_id == settings.CONTENT_TYPE('course', 'course').id:
        return apps.get_model('course', 'course')
    elif content_type_id == settings.CONTENT_TYPE('question', 'activity').id:
        return apps.get_model('question', 'activity')
    elif content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
        return apps.get_model('program', 'program')
    elif content_type_id == settings.CONTENT_TYPE('program', 'program').id:
        return apps.get_model('program', 'program')
    elif content_type_id == settings.CONTENT_TYPE('assignment', 'assignment').id:
        return apps.get_model('assignment', 'assignment')
    else:
        raise Http404


def detail_account_view(request, progress_id):
    progress = get_object_or_404(Progress, id=progress_id)
    Model = _get_model_content_type_id(progress.content_type_id)
    content = Model.pull(progress.content)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type, content=content)
    if progress.content_type_id == settings.CONTENT_TYPE('course', 'course').id:
        progress_course = progress
    else:
        progress_course = None
    return _detail(request, progress, content_type, content, content_type_name,
                   reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-account',
                   get_breadcrumb(['progress.account_view',
                                   'progress.account_detail_view',
                                   'progress.detail_account_view'],
                                  **{'account_id': progress.account_id,
                                     'detail_account__content_type_name': content_type_name,
                                     'detail_account__progress_id': progress.id}),
                   progress_course=progress_course)


def detail_account_program_view(request, progress_id, progress_program_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_program = get_object_or_404(Progress, id=progress_program_id)

    content = get_content(progress.content_type_id, progress.content)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type, content=content)
    if progress.content_type_id == settings.CONTENT_TYPE('course', 'course').id:
        progress_course = progress
    else:
        progress_course = None
    return _detail(request, progress, content_type, content, content_type_name,
                   reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-account',
                   get_breadcrumb(['progress.account_view',
                                   'progress.account_detail_view',
                                   'progress.detail_account_view',
                                   'progress.detail_account_program_view'],
                                  **{'account_id': progress.account_id,
                                     'detail_account__content_type_name': 'program',
                                     'detail_account__progress_id': progress_program.id,
                                     'detail_account_program__content_type_name': content_type_name,
                                     'detail_account_program__progress_id': progress.id,
                                     'detail_account_program__progress_program_id': progress_program.id}),
                   progress_program=progress_program,
                   progress_course=progress_course)


def detail_account_course_view(request, progress_id, progress_course_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)

    content = get_content(progress.content_type_id, progress.content)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type, content=content)
    return _detail(request, progress, content_type, content, content_type_name,
                   reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-account',
                   get_breadcrumb(['progress.account_view',
                                   'progress.account_detail_view',
                                   'progress.detail_account_view',
                                   'progress.detail_account_course_view'],
                                  **{'account_id': progress.account_id,
                                     'detail_account__content_type_name': 'course',
                                     'detail_account__progress_id': progress_course.id,
                                     'detail_account_course__content_type_name': content_type_name,
                                     'detail_account_course__progress_id': progress.id,
                                     'detail_account_course__progress_course_id': progress_course.id}),
                   progress_course=progress_course)


def detail_account_program_course_view(request, progress_id, progress_program_id, progress_course_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_program = get_object_or_404(Progress, id=progress_program_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)

    content = get_content(progress.content_type_id, progress.content)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type, content=content)
    return _detail(request, progress, content_type, content, content_type_name,
                   reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-account',
                   get_breadcrumb(['progress.account_view',
                                   'progress.account_detail_view',
                                   'progress.detail_account_view',
                                   'progress.detail_account_program_view',
                                   'progress.detail_account_program_course_view'],
                                  **{'account_id': progress.account_id,
                                     'detail_account__content_type_name': 'program',
                                     'detail_account__progress_id': progress_program.id,
                                     'detail_account_program__content_type_name': 'course',
                                     'detail_account_program__progress_id': progress_course.id,
                                     'detail_account_program__progress_program_id': progress_program.id,
                                     'detail_account_program_course__content_type_name': content_type_name,
                                     'detail_account_program_course__progress_id': progress.id,
                                     'detail_account_program_course__progress_program_id': progress_program.id,
                                     'detail_account_program_course__progress_course_id': progress_course.id}),
                   progress_program=progress_program,
                   progress_course=progress_course)


def detail_course_view(request, progress_id, course_id):
    content = Course.pull(course_id)
    progress = get_object_or_404(Progress, id=progress_id, content=content.id)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type, content=content)
    return _detail(request, progress, content_type, content, content_type_name,
                   '#',  # reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-course',
                   get_breadcrumb(['progress.content_course_view',
                                   'progress.content_detail_course_view',
                                   'progress.detail_course_view'],
                                  **{'detail_course__content_type_name': content_type_name,
                                     'detail_course__progress_id': progress.id,
                                     'detail_course__content_id': content.id,
                                     'content_id': content.id}))


def detail_course_question_view(request, progress_id, progress_course_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)

    content = Activity.pull(progress.content)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)

    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_name_course = get_content_type_name(content_type_course)

    return _detail(request, progress, content_type, content, content_type_name,
                   '#',  # reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-course',
                   get_breadcrumb(['progress.content_course_view',
                                   'progress.content_detail_course_view',
                                   'progress.detail_course_view',
                                   'progress.detail_course_question_view'],
                                  **{'detail_course__content_type_name': content_type_name_course,
                                     'detail_course__progress_id': progress_course.id,
                                     'detail_course__content_id': progress_course.content,
                                     'detail_course_question__content_type_name': content_type_name,
                                     'detail_course_question__progress_id': progress.id,
                                     'detail_course_question__content_id': progress.content,
                                     'content_id': progress_course.content}),
                   progress_course=progress_course)


def detail_course_question_program_view(request, progress_id, progress_course_id, progress_program_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)
    progress_program = get_object_or_404(Progress, id=progress_program_id)

    content = Activity.pull(progress.content)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)

    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_type_name_program = get_content_type_name(content_type_program)

    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_name_course = get_content_type_name(content_type_course)

    return _detail(request, progress, content_type, content, content_type_name,
                   '#',  # reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-program',
                   get_breadcrumb(['progress.content_program_view',
                                   'progress.content_detail_program_view',
                                   'progress.detail_program_view',
                                   'progress.detail_program_item_view',
                                   'progress.detail_course_question_program_view'],
                                  **{'detail_program__content_type_name': content_type_name_program,
                                     'detail_program__progress_id': progress_program.id,
                                     'detail_program__content_id': progress_program.content,
                                     'detail_program_item__content_type_name': content_type_name_course,
                                     'detail_program_item__progress_id': progress_course.id,
                                     'detail_program_item__content_id': content.id,
                                     'detail_program_item__progress_program_id': progress_program.id,
                                     'detail_course_question_program__content_type_name': content_type_name,
                                     'detail_course_question_program__progress_id': progress.id,
                                     'detail_course_question_program__content_id': progress.content,
                                     'detail_course_question_program__progress_program_id': progress.content,
                                     'content_id': progress_program.content}),
                   progress_course=progress_course,
                   progress_program=progress_program)


def detail_course_question_onboard_view(request, progress_id, progress_course_id, progress_program_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)
    progress_program = get_object_or_404(Progress, id=progress_program_id)

    content = Activity.pull(progress.content)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)

    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_type_name_program = get_content_type_name(content_type_program)

    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_name_course = get_content_type_name(content_type_course)

    return _detail(request, progress, content_type, content, content_type_name,
                   '#',  # reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-onboard',
                   get_breadcrumb(['progress.content_onboard_view',
                                   'progress.content_detail_onboard_view',
                                   'progress.detail_onboard_view',
                                   'progress.detail_onboard_item_view',
                                   'progress.detail_course_question_onboard_view'],
                                  **{'detail_onboard__content_type_name': content_type_name_program,
                                     'detail_onboard__progress_id': progress_program.id,
                                     'detail_onboard__content_id': progress_program.content,
                                     'detail_onboard_item__content_type_name': content_type_name_course,
                                     'detail_onboard_item__progress_id': progress_course.id,
                                     'detail_onboard_item__content_id': content.id,
                                     'detail_onboard_item__progress_onboard_id': progress_program.id,
                                     'detail_course_question_onboard__content_type_name': content_type_name,
                                     'detail_course_question_onboard__progress_id': progress.id,
                                     'detail_course_question_onboard__content_id': progress.content,
                                     'detail_course_question_onboard__progress_program_id': progress.content,
                                     'content_id': progress_program.content}),
                   progress_course=progress_course,
                   progress_program=progress_program)


def detail_question_view(request, progress_id, activity_id):
    progress = get_object_or_404(Progress, id=progress_id)

    content = Activity.pull(progress.content)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)

    return _detail(request, progress, content_type, content, content_type_name,
                   '#',  # reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-question',
                   get_breadcrumb(['progress.content_question_view',
                                   'progress.content_detail_question_view',
                                   'progress.detail_question_view'],
                                  **{'detail_question__content_type_name': content_type_name,
                                     'detail_question__progress_id': progress.id,
                                     'detail_question__content_id': progress.content,
                                     'content_id': content.id}))


def detail_program_view(request, progress_id, program_id):
    progress = get_object_or_404(Progress, id=progress_id)
    content = Program.pull(program_id)

    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type, content=content)
    return _detail(request, progress, content_type, content, content_type_name,
                   reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-program',
                   get_breadcrumb(['progress.content_program_view',
                                   'progress.content_detail_program_view',
                                   'progress.detail_program_view'],
                                  **{'detail_program__content_type_name': content_type_name,
                                     'detail_program__progress_id': progress.id,
                                     'detail_program__content_id': content.id,
                                     'content_id': content.id}))


def detail_program_item_view(request, progress_id, program_id, progress_program_id):
    progress = get_object_or_404(Progress, id=progress_id)
    content = get_content(progress.content_type_id, progress.content)
    progress_program = get_object_or_404(Progress, id=progress_program_id)

    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type, content=content)

    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_type_name_program = get_content_type_name(content_type_program)

    return _detail(request, progress, content_type, content, content_type_name,
                   reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-program',
                   get_breadcrumb(['progress.content_program_view',
                                   'progress.content_detail_program_view',
                                   'progress.detail_program_view',
                                   'progress.detail_program_item_view'],
                                  **{'detail_program__content_type_name': content_type_name_program,
                                     'detail_program__progress_id': progress_program.id,
                                     'detail_program__content_id': progress_program.content,
                                     'detail_program_item__content_type_name': content_type_name,
                                     'detail_program_item__progress_id': progress.id,
                                     'detail_program_item__content_id': content.id,
                                     'detail_program_item__progress_program_id': progress_program.id,
                                     'content_id': content.id}),
                   progress_program=progress_program)


def detail_onboard_view(request, progress_id, onboard_id):
    progress = get_object_or_404(Progress, id=progress_id)
    content = Program.pull(onboard_id)

    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type, content=content)
    return _detail(request, progress, content_type, content, content_type_name,
                   reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-onboard',
                   get_breadcrumb(['progress.content_onboard_view',
                                   'progress.content_detail_onboard_view',
                                   'progress.detail_onboard_view'],
                                  **{'detail_onboard__content_type_name': content_type_name,
                                     'detail_onboard__progress_id': progress.id,
                                     'detail_onboard__content_id': content.id,
                                     'content_id': content.id}))


def detail_onboard_item_view(request, progress_id, onboard_id, progress_program_id):
    progress = get_object_or_404(Progress, id=progress_id)
    content = get_content(progress.content_type_id, progress.content)
    progress_program = get_object_or_404(Progress, id=progress_program_id)

    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type, content=content)

    content_type_program = settings.CONTENT_TYPE('program', 'onboard')
    content_program = get_content(progress_program.content_type_id, progress_program.content)
    content_type_name_program = get_content_type_name(content_type_program, content=content_program)

    return _detail(request, progress, content_type, content, content_type_name,
                   reverse('dashboard:progress:account_detail', args=[progress.account_id]),
                   'progress-onboard',
                   get_breadcrumb(['progress.content_onboard_view',
                                   'progress.content_detail_onboard_view',
                                   'progress.detail_onboard_view',
                                   'progress.detail_onboard_item_view'],
                                  **{'detail_onboard__content_type_name': content_type_name_program,
                                     'detail_onboard__progress_id': progress_program.id,
                                     'detail_onboard__content_id': progress_program.content,
                                     'detail_onboard_item__content_type_name': content_type_name,
                                     'detail_onboard_item__progress_id': progress.id,
                                     'detail_onboard_item__content_id': content.id,
                                     'detail_onboard_item__progress_onboard_id': progress_program.id,
                                     'content_id': content.id}),
                   progress_program=progress_program)
