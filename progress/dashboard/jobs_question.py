import json

from django.conf import settings

from alert.models import Alert
from question.models import Activity
from provider.models import Provider
from .jobs import alert_fail
from .jobs import make_xls

if settings.IS_SUB_PROVIDER:
    from subprovider.models import SubProvider

def export_question_job(alert_id):
    try:
        alert = Alert.objects.get(id=alert_id)
    except:
        return None

    kwargs = json.loads(alert.json_kwargs)
    if not all([_ in kwargs for _ in ['name', 'perm', 'provider', 'sub_provider']]):
        alert_fail(alert)
        return None

    if kwargs['provider']:
        provider = Provider.pull(kwargs['provider'])
    else:
        provider = None

    if kwargs['sub_provider']:
        sub_provider = SubProvider.pull(kwargs['sub_provider'])
    else:
        sub_provider = None

    content_list = Activity.objects.all().order_by('-timestamp')
    content_type = settings.CONTENT_TYPE('question', 'activity')
    if kwargs['perm'] == 'view':
        pass
    elif kwargs['perm'] == 'own':
        if provider:
            content_list = content_list.filter(provider=provider)
        elif settings.IS_SUB_PROVIDER and sub_provider is not None:
            from subprovider.models import Item
            content_list = content_list.filter(
                id__in=Item.objects.values_list('content', flat=True)
                    .filter(sub_provider=sub_provider,
                            content_type=content_type)
            )
        else:
            content_list = []
    else:
        alert_fail(alert)
        return None

    q_name = kwargs['name'].strip()
    if len(q_name) > 0:
        content_list = content_list.filter(name__icontains=q_name)

    columns = [
        'TEST', 'CATEGORY', 'CREATE DATE', 'TOTAL ADDED',
        'IN PROGRESS', 'COMPLETED', 'FAILED', 'EXPIRED',
        '% PERCENT COMPLETED'
    ]
    return make_xls(content_list, columns, alert, content_type)
