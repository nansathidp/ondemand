from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db.models import Sum
from django.shortcuts import render

from account.models import Account
from department.models import Department
from progress.models import Account as ProgressAccount

if settings.IS_ORGANIZATION:
    pass
from alert.models import Alert

from department.cached import cached_department
from utils.paginator import paginator
from utils.filter import get_q_dashboard
from utils.breadcrumb import get_breadcrumb
from dashboard.views import config_view
from progress.jobs import account_progress_hard_job
import django_rq
from django.utils import timezone
import datetime

def cal_percent(progress_result):
    for code in ProgressAccount.CODE_LIST:
        in_progress = progress_result.get('%sin_progress__sum'%code, 0)
        completed = progress_result.get('%scompleted__sum'%code, 0)
        failed = progress_result.get('%sfailed__sum'%code, 0)
        expired = progress_result.get('%sexpired__sum'%code, 0)
        try:
            progress_result['%spercent'%code] = int(float(completed) / float(in_progress + completed + failed + expired) * 100.0)
        except:
            progress_result['%spercent'%code] = 0


def account_view(request):
    if request.user.has_perm('progress.view_progress',
                             group=request.DASHBOARD_GROUP):
        department_list = Department.objects.filter(parents__isnull=True)
        account_list = Account.objects.filter(
            progress_account_set__is_root=True,
            progress_account_set__count__gt=0
        ).order_by('-progress_account_set__timeupdate')
    elif request.user.has_perm('progress.view_own_progress',
                               group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    elif request.user.has_perm('progress.view_org_progress',
                               group=request.DASHBOARD_GROUP):
        # department_list = Department.objects.filter(
        #     parents__isnull=True
        # )
        department_list = Department.pull_user_level(request.user)
        if settings.IS_ORGANIZATION:
            account_list = Account.objects.filter(
                org_child_set__account=request.user,
                progress_account_set__count__gt=0
            ).distinct()
        else:
            return config_view(request)
    else:
        raise PermissionDenied

    if request.GET.get('hard', '') == 'True':
        django_rq.enqueue(account_progress_hard_job)

    q_name = " ".join(get_q_dashboard(request, 'name', 'str').split())
    if q_name != '' and q_name != -1:
        account_list = account_list.filter(Q(first_name__icontains=q_name) |
                                           Q(last_name__icontains=q_name))
    if q_name == -1:
        q_name = ''

    q_department = get_q_dashboard(request, 'department', 'int')
    if q_department:
        department = cached_department(q_department)
        if department:
            q_department_list = department.get_all_child()
            account_list = account_list.filter(department_member_set__department__in=q_department_list)

    sum_list = []
    for code in ProgressAccount.CODE_LIST:
        for _ in ProgressAccount.FIELD_LIST:
            sum_list.append(Sum('%s%s'%(code, _)))

    if settings.ACCOUNT__HIDE_INACTIVE:
        account_list = account_list.exclude(is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        account_list = account_list.exclude(id__in=settings.ACCOUNT__HIDE_ID_LIST)
    progress_result = ProgressAccount.objects.filter(
        account__in=account_list,
        is_root=True
    ).aggregate(*sum_list)
    cal_percent(progress_result)
    account_list = paginator(request, account_list)
    for account in account_list:
        account.progress = ProgressAccount.pull(account.id, True)
        account.url_detail = reverse('dashboard:progress:account_detail', args=[account.id])

    alert = Alert.objects.filter(
        account=request.user,
        page=settings.CONTENT_TYPE('progress', 'account'),
        status__in=[1, 2]
    ).first()
    if alert is not None and timezone.now() - alert.timestamp > datetime.timedelta(minutes=5):
        Alert.objects.filter(
            account=request.user,
            page=settings.CONTENT_TYPE('progress', 'account'),
            status__in=[1, 2]
        ).update(status=-1)
        alert = None
    return render(request,
                  'progress/dashboard/account.html',
                  {
                      'SIDEBAR': 'progress-account',
                      'BREADCRUMB_LIST': get_breadcrumb(['progress.account_view']),
                      'TAB': 'home',
                      'account_list': account_list,
                      'department_list': department_list,
                      'progress_result': progress_result,
                      'alert': alert,

                      'q_name': q_name,
                      'q_department': q_department,
                      'q_department_list': department_list,
                      # 'q_level': q_level,
                      # 'q_level_list': q_level_list
                  })
