from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse

from assignment.models import Assignment
from account.models import Account

from .views import pull_breadcrumb
from utils.paginator import paginator

def assignment_account_view(request, assignment_id):
    assignment = get_object_or_404(Assignment, id=assignment_id)
    account_list = Account.objects.filter(assignment_account_set__assignment=assignment)
        
    account_list = paginator(request, account_list)
    for account in account_list:
        account.url_detail = reverse('dashboard:progress:assignment_account_detail', args=[assignment.id, account.id])

    return render(request,
                  'progress/dashboard/assignment_account.html',
                  {
                      'SIDEBAR': 'assignment',
                      'BREADCRUMB_LIST': pull_breadcrumb({'assignment_id': assignment.id}),
                      'assignment': assignment,
                      'account_list': account_list
                  })
