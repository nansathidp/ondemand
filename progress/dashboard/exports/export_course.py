from dateutil import tz

from django.conf import settings
from django.utils import timezone

from progress.models import Progress
from course.models import Course
from ais.models import AccountInfo
from question.models import Score
from content.models import Location as ContentLocation
from order.models import Item


def report():
    content_type = settings.CONTENT_TYPE('course', 'course')
    content_type_material = settings.CONTENT_TYPE('course', 'material')
    content_type_activity = settings.CONTENT_TYPE('question', 'activity')
    course_list = Course.objects.all().order_by('-timestamp')

    accountinfo_list = AccountInfo.objects.all()
    report_list = []

    for course in course_list:
        if course:
            parent1_content_type = content_type
            parent1_content = course.id
            location_course = get_location(content_type=content_type, content_id=course.id)
            for material in course.material_set.all():

                # content type is video
                if material.type == 0:
                    content_type_temp = content_type_material
                    parent2_content = None
                    parent2_content_type = None

                    for accountinfo in accountinfo_list:

                        if accountinfo:

                            location_material = get_location(content_type=content_type_temp, content_id=material.id,
                                                             parent1_content_type=parent1_content_type,
                                                             parent1_content=parent1_content,
                                                             parent2_content_type=parent2_content_type,
                                                             parent2_content=parent2_content)
                            progress_material_list = Progress.objects.filter(item__content_type=content_type,
                                                                             item__content=course.id,
                                                                             content_type=content_type_temp,
                                                                             content=material.id,
                                                                             account=accountinfo.account)
                            if progress_material_list:

                                progress_course = Progress.objects.filter(item__content_type=content_type,
                                                                          content_type=content_type,
                                                                          content=course.id,
                                                                          account=accountinfo.account).first()
                                order_item = progress_course.item
                                order_temp = get_order_item(order_item.id)

                                enroll = order_temp.order.timestamp
                                progress_complete_time = enroll

                                score = '-'
                                score_percent = '-'
                                for progress_material in progress_material_list:
                                    interval = progress_material.duration_display()
                                    report_list.append(pack_data(course.name, enroll, course.get_status_display(),
                                                                 accountinfo.employee, accountinfo.account.first_name,
                                                                 accountinfo.account.last_name,
                                                                 accountinfo.account.email,
                                                                 accountinfo.account.get_department_display(),
                                                                 material.outline.section,
                                                                 accountinfo.account.get_direct_manager_email_display(),
                                                                 material.get_type_display(), material.name,
                                                                 progress_material.percent,
                                                                 progress_material.get_status_display(), interval,
                                                                 score, score_percent, progress_complete_time))
                                    # content type is web
                elif material.type == 1:
                    content_type_temp = content_type_material
                    parent2_content = None
                    parent2_content_type = None
                    for accountinfo in accountinfo_list:
                        if accountinfo:
                            location_material = get_location(content_type=content_type_temp, content_id=material.id,
                                                             parent1_content_type=parent1_content_type,
                                                             parent1_content=parent1_content,
                                                             parent2_content_type=parent2_content_type,
                                                             parent2_content=parent2_content)
                            progress_material_list = Progress.objects.filter(item__content_type=content_type,
                                                                             item__content=course.id,
                                                                             content_type=content_type_temp,
                                                                             content=material.id,
                                                                             account=accountinfo.account)
                            if progress_material_list:
                                progress_course = Progress.objects.filter(item__content_type=content_type,
                                                                          content_type=content_type,
                                                                          content=course.id,
                                                                          account=accountinfo.account).first()

                                order_item = progress_course.item
                                order_temp = get_order_item(order_item.id)

                                enroll = order_temp.order.timestamp
                                progress_complete_time = enroll

                                interval = 1
                                score = '-'
                                score_percent = '-'
                                for progress_material in progress_material_list:
                                    report_list.append(pack_data(course.name, enroll, course.get_status_display(),
                                                                 accountinfo.employee, accountinfo.account.first_name,
                                                                 accountinfo.account.last_name,
                                                                 accountinfo.account.email,
                                                                 accountinfo.account.get_department_display(),
                                                                 material.outline.section,
                                                                 accountinfo.account.get_direct_manager_email_display(),
                                                                 material.get_type_display(), material.name,
                                                                 progress_material.percent,
                                                                 progress_material.get_status_display(), interval,
                                                                 score, score_percent, progress_complete_time))

                                    # content type is doc
                elif material.type == 2:
                    content_type_temp = content_type_material
                    parent2_content = None
                    parent2_content_type = None
                    for accountinfo in accountinfo_list:
                        if accountinfo:

                            location_material = get_location(content_type=content_type_temp, content_id=material.id,
                                                             parent1_content_type=parent1_content_type,
                                                             parent1_content=parent1_content,
                                                             parent2_content_type=parent2_content_type,
                                                             parent2_content=parent2_content)
                            progress_material_list = Progress.objects.filter(item__content_type=content_type,
                                                                             item__content=course.id,
                                                                             content_type=content_type_temp,
                                                                             content=material.id,
                                                                             account=accountinfo.account)
                            if progress_material_list:
                                progress_course = Progress.objects.filter(item__content_type=content_type,
                                                                          content_type=content_type,
                                                                          content=course.id,
                                                                          account=accountinfo.account).first()
                                order_item = progress_course.item
                                order_temp = get_order_item(order_item.id)

                                enroll = order_temp.order.timestamp
                                progress_complete_time = enroll

                                interval = 1
                                score = '-'
                                score_percent = '-'
                                for progress_material in progress_material_list:
                                    report_list.append(pack_data(course.name, enroll, course.get_status_display(),
                                                                 accountinfo.employee, accountinfo.account.first_name,
                                                                 accountinfo.account.last_name,
                                                                 accountinfo.account.email,
                                                                 accountinfo.account.get_department_display(),
                                                                 material.outline.section,
                                                                 accountinfo.account.get_direct_manager_email_display(),
                                                                 material.get_type_display(), material.name,
                                                                 progress_material.percent,
                                                                 progress_material.get_status_display(), interval,
                                                                 score, score_percent, progress_complete_time))

                                    # content type is test
                elif material.type == 3:
                    content_type_temp = content_type_activity
                    parent2_content = material.id
                    parent2_content_type = content_type_material
                    for accountinfo in accountinfo_list:
                        if accountinfo:
                            item_list = Item.objects.filter(account=accountinfo.account,content_type=content_type)
                            for item in item_list:
                                if item:

                                    location_material = get_location(content_type=content_type_temp,
                                                                     content_id=material.question.id,
                                                                     parent1_content_type=parent1_content_type,
                                                                     parent1_content=parent1_content,
                                                                     parent2_content_type=parent2_content_type,
                                                                     parent2_content=parent2_content)
                                    progress_material_list = Progress.objects.filter(
                                                                                     location=location_material,
                                                                                     content_type=content_type_temp,
                                                                                     content=material.question.id,
                                                                                     account=accountinfo.account)

                                    if progress_material_list.first() is None:
                                        progress_course = Progress.objects.filter(item=item,
                                                                                  content_type=content_type,
                                                                                  content=course.id,
                                                                                  account=accountinfo.account).first()
                                        enroll = order_temp.order.timestamp
                                        interval = '0/0'
                                        progress_material_percent = '-'
                                        progress_material_status = '-'
                                        score = '-'


                                        score_percent = '-'
                                        report_list.append(pack_data(course.name, enroll,
                                                                     course.get_status_display(),
                                                                     accountinfo.employee,
                                                                     accountinfo.account.first_name,
                                                                     accountinfo.account.last_name,
                                                                     accountinfo.account.email,
                                                                     accountinfo.account.get_department_display(),
                                                                     material.outline.section,
                                                                     accountinfo.account.get_direct_manager_email_display(),
                                                                     material.get_type_display(), material.question,
                                                                     progress_material_percent,
                                                                     progress_material_status,
                                                                     interval,
                                                                     score, score_percent,
                                                                     None
                                                                     )
                                                           )
                                        break

                                    if progress_material_list:
                                        progress_course = Progress.objects.filter(
                                                                                  content_type=content_type,
                                                                                  content=course.id,
                                                                                  account=accountinfo.account).first()
                                        # order_item = progress_course.item
                                        score_list = Score.objects.filter(item=item,
                                                                          account=accountinfo.account,
                                                                          # location=location_material,
                                                                          activity=material.question).order_by(
                                            'timestamp')

                                        order_item = item
                                        order_temp = get_order_item(item.id)

                                        enroll = order_temp.order.timestamp
                                        progress_complete_time = progress_course.timeupdate

                                        max = score_list.count()

                                        for progress_material in progress_material_list:
                                            for score_item in score_list:
                                                interval = str(score_item.submit_number) + "/" + str(max)
                                                score_percent = '-'
                                                score = '0/0'
                                                if score_item:
                                                    max_score = score_item.max_score
                                                    score = str(score_item.score) + '/' + str(max_score)
                                                    score_percent_temp = (score_item.score / max_score) * 100
                                                    score_percent = str(score_percent_temp) + '%'

                                                report_list.append(pack_data(course.name, enroll,
                                                                             course.get_status_display(),
                                                                             accountinfo.employee,
                                                                             accountinfo.account.first_name,
                                                                             accountinfo.account.last_name,
                                                                             accountinfo.account.email,
                                                                             accountinfo.account.get_department_display(),
                                                                             material.outline.section,
                                                                             accountinfo.account.get_direct_manager_email_display(),
                                                                             material.get_type_display(), material.question,
                                                                             progress_material.percent,
                                                                             progress_material.get_status_display(),
                                                                             interval,
                                                                             score, score_percent,
                                                                             progress_complete_time
                                                                             )
                                                                   )
                elif material.type == 4:
                    content_type_temp = content_type_material
                    parent2_content = material.id
                    parent2_content_type = None
                    for accountinfo in accountinfo_list:
                        if accountinfo:

                            location_material = get_location(content_type=content_type_temp, content_id=material.id,
                                                             parent1_content_type=parent1_content_type,
                                                             parent1_content=parent1_content,
                                                             parent2_content_type=parent2_content_type,
                                                             parent2_content=parent2_content)
                            progress_material_list = Progress.objects.filter(item__content_type=content_type,
                                                                             item__content=course.id,
                                                                             content_type=content_type_temp,
                                                                             content=material.id,
                                                                             account=accountinfo.account)
                            if progress_material_list:
                                progress_course = Progress.objects.filter(item__content_type=content_type,
                                                                          content_type=content_type,
                                                                          content=course.id,
                                                                          account=accountinfo.account).first()
                                order_item = progress_course.item
                                order_temp = get_order_item(order_item.id)

                                enroll = order_temp.order.timestamp
                                progress_complete_time = enroll

                                score = '-'
                                score_percent = '-'

                                for progress_material in progress_material_list:
                                    interval = progress_material.duration_display()
                                    report_list.append(pack_data(course.name, enroll, course.get_status_display(),
                                                                 accountinfo.employee, accountinfo.account.first_name,
                                                                 accountinfo.account.last_name,
                                                                 accountinfo.account.email,
                                                                 accountinfo.account.get_department_display(),
                                                                 material.outline.section,
                                                                 accountinfo.account.get_direct_manager_email_display(),
                                                                 material.get_type_display(), material.name,
                                                                 progress_material.percent,
                                                                 progress_material.get_status_display(), interval,
                                                                 score, score_percent, progress_complete_time))


def get_location(content_type, content_id,
                 parent1_content_type=None, parent1_content=-1,
                 parent2_content_type=None, parent2_content=-1,
                 parent3_content_type=None, parent3_content=-1):
    return ContentLocation.objects.filter(content_type=content_type,
                                          content=content_id,
                                          parent1_content_type=parent1_content_type,
                                          parent1_content=parent1_content,
                                          parent2_content_type=parent2_content_type,
                                          parent2_content=parent2_content,
                                          parent3_content_type=parent3_content_type,
                                          parent3_content=parent3_content) \
        .first()


def get_order_item(id):
    return Item.objects.filter(id=id).first()


def pack_data(course_name, enroll, course_status, pin, firs_name, last_name, email, department, section, line,
              content_type, content_name, percent, content_status, interval, score, score_percen, progress_time):
    d = {}

    enroll_timezone = timezone.localtime(enroll, tz.tzlocal())
    enroll_time = enroll_timezone.strftime('%Y-%m-%d %H:%M:%S')

    progress_timezone = timezone.localtime(progress_time, tz.tzlocal())
    progress_time = progress_timezone.strftime('%Y-%m-%d %H:%M:%S')
    row_data = []
    row_data.append(course_name)
    row_data.append(enroll_time)
    row_data.append(course_status)
    row_data.append(pin)
    row_data.append(firs_name)
    row_data.append(last_name)
    row_data.append(email)
    row_data.append(department)
    row_data.append(section)
    row_data.append(line)
    row_data.append(content_type)
    row_data.append(content_name)
    row_data.append(content_status)
    row_data.append(percent)
    row_data.append(interval)
    row_data.append(score)
    row_data.append(score_percen)
    row_data.append(progress_time)

    return row_data


def get_column():

    column = []
    column.append('Course Name')
    column.append('Enroll Time')
    column.append('Course Status')
    column.append('PIN Code')
    column.append("First Name")
    column.append('Last Name')
    column.append('Email')
    column.append('Department')
    column.append('Section')
    column.append('Line Manager')
    column.append('Content Type')
    column.append('Content Name')
    column.append('Content Status')
    column.append('Progress %')
    column.append('Interval')
    column.append('Score')
    column.append('Score %')
    column.append('Progress Time')

    return column

