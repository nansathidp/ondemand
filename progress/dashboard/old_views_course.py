from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ..models import Course as ProgressCourse
from course.models import Course
from department.models import Department

from utils.paginator import paginator
from dashboard.breadcrumb import breadcrumb

def course_view(request, course_id):
    course = get_object_or_404(Course, id=course_id)
    if request.user.has_perm('progress.view_course'):
        progress_course_list = ProgressCourse.objects.filter(content=course).order_by('-percent')
    elif request.user.has_perm('progress.view_own_course'):
        try:
            group = request.user.groups.all()[0]
        except:
            group = None
        if group is None:
            department_list = []
        elif group.id == 4: #epartment Admin
            department_list = Department.objects.filter(member__account=request.user)
            progress_course_list = ProgressCourse.objects.filter(content=course,
                                                                 item__account__department_member_set__department__in=department_list).order_by('-percent')
        elif group.id == 7: #Learning Center
            progress_course_list = ProgressCourse.objects.filter(content=course).order_by('-percent')
        elif group.id == 8: #Instructor
            progress_course_list = ProgressCourse.objects.filter(content=course).order_by('-percent')
        else:
            progress_course_list = []
    else:
        raise PermissionDenied

    progress_course_list = paginator(request, progress_course_list)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Progress Course',
         'url': reverse('dashboard:progress:course')},
        {'is_active': True,
         'title': 'Course: %s'%course.name}
    ]
    return render(request,
                  'progress/dashboard/course.html',
                  {'SIDEBAR': 'progress',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'course': course,
                   'progress_course_list': progress_course_list})
