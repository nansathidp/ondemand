from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404

from progress.models import Progress
from program.models import Program
from question.models import Activity, Score
from utils.breadcrumb import get_breadcrumb
from utils.content_type import get_content_type_name


def _question_score(request, progress, score,
                    summary_url,
                    url_question_score,
                    SIDEBAR, BREADCRUMB_LIST,
                    progress_program=None, progress_course=None):
    return render(request,
                  'progress/dashboard/question_score.html',
                  {
                      'SIDEBAR': SIDEBAR,
                      'BREADCRUMB_LIST': BREADCRUMB_LIST,
                      'activity': Activity.pull(score.activity_id),
                      'score': score,
                      'summary_url': summary_url,
                      'url_question_score': url_question_score,
                      'progress_program': progress_program,
                      'progress_course': progress_course,
                  })


def question_score_view(request, progress_id, score_id):
    progress = get_object_or_404(Progress, id=progress_id)
    score = get_object_or_404(Score, id=score_id)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)

    return _question_score(request, progress, score,
                           '#', #reverse('dashboard:progress:detail_account', args=[progress.id]),
                           reverse('dashboard:progress:detail_question', args=[progress.id, progress.content]),
                           'progress-question',
                           get_breadcrumb(['progress.content_question_view',
                                           'progress.content_detail_question_view',
                                           'progress.detail_question_view',
                                           'progress.question_score_view'],
                                          **{'detail_question__content_type_name': content_type_name,
                                             'detail_question__progress_id': progress.id,
                                             'detail_question__content_id': progress.content,
                                             'content_id': progress.content}))


def question_score_account_view(request, progress_id, score_id):
    progress = get_object_or_404(Progress, id=progress_id)
    score = get_object_or_404(Score, id=score_id)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)
    return _question_score(request, progress, score,
                           reverse('dashboard:progress:detail_account', args=[progress.id]),
                           reverse('dashboard:progress:detail_account', args=[progress.id]),
                           'progress-account',
                           get_breadcrumb(['progress.account_view',
                                           'progress.account_detail_view',
                                           'progress.detail_account_view',
                                           'progress.question_score_account_view'],
                                          **{'account_id': progress.account_id,
                                             'detail_account__content_type_name': content_type_name,
                                             'detail_account__progress_id': progress.id}))


def question_score_account_program_view(request, progress_id, score_id, progress_program_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_program = get_object_or_404(Progress, id=progress_program_id)
    score = get_object_or_404(Score, id=score_id)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)
    return _question_score(request, progress, score,
                           reverse('dashboard:progress:detail_account', args=[progress.id]),
                           reverse('dashboard:progress:detail_account_program',
                                   args=[progress.id, progress_program.id]),
                           'progress-account',
                           get_breadcrumb(['progress.account_view',
                                           'progress.account_detail_view',
                                           'progress.detail_account_view',
                                           'progress.detail_account_program_view',
                                           'progress.question_score_account_program_view'],
                                          **{'account_id': progress.account_id,
                                             'detail_account__content_type_name': 'program',
                                             'detail_account__progress_id': progress_program.id,
                                             'detail_account_program__content_type_name': 'test',
                                             'detail_account_program__progress_id': progress.id,
                                             'detail_account_program__progress_program_id': progress_program.id}),
                           progress_program=progress_program)


def question_score_account_course_view(request, progress_id, score_id, progress_course_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)
    score = get_object_or_404(Score, id=score_id)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)
    return _question_score(request, progress, score,
                           reverse('dashboard:progress:detail_account', args=[progress.id]),
                           reverse('dashboard:progress:detail_account_course', args=[progress.id, progress_course.id]),
                           'progress-account',
                           get_breadcrumb(['progress.account_view',
                                           'progress.account_detail_view',
                                           'progress.detail_account_view',
                                           'progress.detail_account_course_view',
                                           'progress.question_score_account_course_view'],
                                          **{'account_id': progress.account_id,
                                             'detail_account__content_type_name': 'course',
                                             'detail_account__progress_id': progress_course.id,
                                             'detail_account_course__content_type_name': 'test',
                                             'detail_account_course__progress_id': progress.id,
                                             'detail_account_course__progress_course_id': progress_course.id}),
                           progress_course=progress_course)


def question_score_account_program_course_view(request, progress_id, score_id, progress_program_id, progress_course_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_program = get_object_or_404(Progress, id=progress_program_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)
    score = get_object_or_404(Score, id=score_id)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)
    return _question_score(request, progress, score,
                           reverse('dashboard:progress:detail_account', args=[progress.id]),
                           reverse('dashboard:progress:detail_account_program',
                                   args=[progress.id, progress_program.id]),
                           'progress-account',
                           get_breadcrumb(['progress.account_view',
                                           'progress.account_detail_view',
                                           'progress.detail_account_view',
                                           'progress.detail_account_program_view',
                                           'progress.detail_account_program_course_view',
                                           'progress.question_score_account_program_course_view'],
                                          **{'account_id': progress.account_id,
                                             'detail_account__content_type_name': 'program',
                                             'detail_account__progress_id': progress_program.id,
                                             'detail_account_program__content_type_name': 'course',
                                             'detail_account_program__progress_id': progress_course.id,
                                             'detail_account_program__progress_program_id': progress_program.id,
                                             'detail_account_program_course__content_type_name': 'test',
                                             'detail_account_program_course__progress_id': progress.id,
                                             'detail_account_program_course__progress_program_id': progress_program.id,
                                             'detail_account_program_course__progress_course_id': progress_course.id}),
                           progress_program=progress_program,
                           progress_course=progress_course)
    
def question_score_course_view(request, progress_id, score_id, progress_course_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)
    score = get_object_or_404(Score, id=score_id)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)

    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_name_course = get_content_type_name(content_type_course)
    return _question_score(request, progress, score,
                           reverse('dashboard:progress:detail_account', args=[progress.id]),
                           reverse('dashboard:progress:detail_course_question', args=[progress.id, progress_course.id]),
                           'progress-account',
                           get_breadcrumb(['progress.content_course_view',
                                           'progress.content_detail_course_view',
                                           'progress.detail_course_view',
                                           'progress.detail_course_question_view',
                                           'question_score_course_view'],
                                          **{'detail_course__content_type_name': content_type_name_course,
                                             'detail_course__progress_id': progress_course.id,
                                             'detail_course__content_id': progress_course.content,
                                             'detail_course_question__content_type_name': content_type_name,
                                             'detail_course_question__progress_id': progress.id,
                                             'detail_course_question__content_id': progress.content,
                                             'content_id': progress_course.content}),
                           progress_course=progress_course)


def question_score_program_view(request, progress_id, score_id, progress_program_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_program = get_object_or_404(Progress, id=progress_program_id)

    content = Activity.pull(progress.content)
    score = get_object_or_404(Score, id=score_id)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)

    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_type_name_program = get_content_type_name(content_type_program)

    return _question_score(request, progress, score,
                           reverse('dashboard:progress:detail_account', args=[progress.id]),
                           reverse('dashboard:progress:detail_program_item', args=[progress.id, progress_program.content, progress_program.id]),
                           'progress-program',
                           get_breadcrumb(['progress.content_program_view',
                                           'progress.content_detail_program_view',
                                           'progress.detail_program_view',
                                           'progress.detail_program_item_view',
                                           'progress.question_score_program_view'],
                                          **{'detail_program__content_type_name': content_type_name_program,
                                             'detail_program__progress_id': progress_program.id,
                                             'detail_program__content_id': progress_program.content,
                                             'detail_program_item__content_type_name': content_type_name,
                                             'detail_program_item__progress_id': progress.id,
                                             'detail_program_item__content_id': progress_program.content,
                                             'detail_program_item__progress_program_id': progress_program.id,
                                             'content_id': progress_program.content}),
                           progress_program=progress_program)
    
def question_score_program_course_view(request, progress_id, score_id, progress_program_id, progress_course_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_course = get_object_or_404(Progress, id=progress_course_id)
    progress_program = get_object_or_404(Progress, id=progress_program_id)

    content = Activity.pull(progress.content)
    score = get_object_or_404(Score, id=score_id)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)

    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_name_course = get_content_type_name(content_type_course)

    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_type_name_program = get_content_type_name(content_type_program)

    return _question_score(request, progress, score,
                           reverse('dashboard:progress:detail_account', args=[progress.id]),
                           reverse('dashboard:progress:detail_course_question_program', args=[progress.id, progress_course.id, progress_program.id]),
                           'progress-program',
                           get_breadcrumb(['progress.content_program_view',
                                           'progress.content_detail_program_view',
                                           'progress.detail_program_view',
                                           'progress.detail_program_item_view',
                                           'progress.detail_course_question_program_view',
                                           'progress.question_score_program_course_view'],
                                          **{'detail_program__content_type_name': content_type_name_program,
                                             'detail_program__progress_id': progress_program.id,
                                             'detail_program__content_id': progress_program.content,
                                             'detail_program_item__content_type_name': content_type_name_course,
                                             'detail_program_item__progress_id': progress_course.id,
                                             'detail_program_item__content_id': content.id,
                                             'detail_program_item__progress_program_id': progress_program.id,
                                             'detail_course_question_program__content_type_name': content_type_name,
                                             'detail_course_question_program__progress_id': progress.id,
                                             'detail_course_question_program__content_id': progress_course.id,
                                             'detail_course_question_program__progress_program_id': progress_program.id,
                                             'content_id': progress_program.content}),
                           progress_course=progress_course,
                           progress_program=progress_program)

def question_score_onboard_view(request, progress_id, score_id, progress_program_id):
    progress = get_object_or_404(Progress, id=progress_id)
    progress_program = get_object_or_404(Progress, id=progress_program_id)

    content = Activity.pull(progress.content)
    score = get_object_or_404(Score, id=score_id)
    content_type = settings.CONTENT_TYPE_ID(progress.content_type_id)
    content_type_name = get_content_type_name(content_type)

    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_type_name_program = get_content_type_name(content_type_program,
                                                      content=Program.pull(progress_program.content))

    return _question_score(request, progress, score,
                           reverse('dashboard:progress:detail_account', args=[progress.id]),
                           reverse('dashboard:progress:detail_onboard_item', args=[progress.id, progress_program.content, progress_program.id]),
                           'progress-onboard',
                           get_breadcrumb(['progress.content_onboard_view',
                                           'progress.content_detail_onboard_view',
                                           'progress.detail_onboard_view',
                                           'progress.detail_onboard_item_view',
                                           'progress.question_score_onboard_view'],
                                          **{'detail_onboard__content_type_name': content_type_name_program,
                                             'detail_onboard__progress_id': progress_program.id,
                                             'detail_onboard__content_id': progress_program.content,
                                             'detail_onboard_item__content_type_name': content_type_name,
                                             'detail_onboard_item__progress_id': progress.id,
                                             'detail_onboard_item__content_id': progress_program.content,
                                             'detail_onboard_item__progress_onboard_id': progress_program.id,
                                             'content_id': progress_program.content}),
                           progress_program=progress_program)


def question_score_onboard_course_view(request, progress_id, score_id, progress_onboard_id, progress_course_id):
    pass
