from django.http import JsonResponse
from django.conf import settings
from utils.content_type import get_content_type_name
from django.db.models import Sum
from django.db.models import Q

from course.models import Course
from question.models import Activity
from program.models import Program
from account.models import Account
from progress.models import Progress, Content as ProgressContent
from progress.models import Account as ProgressAccount

from department.cached import cached_department
from utils.filter import get_q_dashboard

def course_summary_graph(request):
    content_list = Course.objects.all().order_by('-timestamp')

    request.q_name = get_q_dashboard(request, 'name', 'str').strip()
    if request.q_name != '':
        content_list = content_list.filter(name__icontains=request.q_name)

    content_type = settings.CONTENT_TYPE('course', 'course')
    content_type_name = get_content_type_name(content_type)

    progress_content_list = ProgressContent.objects.filter(is_root=True,
                                                           content_type=content_type,
                                                           content__in=content_list.values_list('id', flat=True))

    progress_result = ProgressContent.pull_summary(progress_content_list)
    return send_json(content_type_name, progress_result)

def question_summary_graph(request):
    content_list = Activity.objects.all().order_by('-timestamp')

    request.q_name = get_q_dashboard(request, 'name', 'str').strip()
    if request.q_name != '':
        content_list = content_list.filter(name__icontains=request.q_name)

    content_type = settings.CONTENT_TYPE('question', 'activity')
    content_type_name = get_content_type_name(content_type)

    progress_content_list = ProgressContent.objects.filter(is_root=True,
                                                           content_type=content_type,
                                                           content__in=content_list.values_list('id', flat=True))

    progress_result = ProgressContent.pull_summary(progress_content_list)
    return send_json(content_type_name, progress_result)

def program_summary_graph(request):
    content_list = Program.objects.filter(type=1).order_by('-timestamp')

    request.q_name = get_q_dashboard(request, 'name', 'str').strip()
    if request.q_name != '':
        content_list = content_list.filter(name__icontains=request.q_name)

    content_type = settings.CONTENT_TYPE('program', 'program')
    content_type_name = get_content_type_name(content_type)

    progress_content_list = ProgressContent.objects.filter(is_root=True,
                                                           content_type=content_type,
                                                           content__in=content_list.values_list('id', flat=True))

    progress_result = ProgressContent.pull_summary(progress_content_list)
    return send_json(content_type_name, progress_result)

def onboard_summary_graph(request):
    content_list = Program.objects.filter(type=2).order_by('-timestamp')

    request.q_name = get_q_dashboard(request, 'name', 'str').strip()
    if request.q_name != '':
        content_list = content_list.filter(name__icontains=request.q_name)

    content_type = settings.CONTENT_TYPE('program', 'onboard')
    content_type_name = get_content_type_name(content_type)

    progress_content_list = ProgressContent.objects.filter(is_root=True,
                                                           content_type=content_type,
                                                           content__in=content_list.values_list('id', flat=True))

    progress_result = ProgressContent.pull_summary(progress_content_list)
    return send_json(content_type_name, progress_result)

def account_summary_graph(request):
    account_list = Account.objects.filter(progress_account_set__is_root=True) \
                                  .order_by('-progress_account_set__timeupdate')

    q_name = get_q_dashboard(request, 'name', 'str').strip()
    if q_name != '':
        account_list = account_list.filter(Q(first_name__icontains=q_name) |
                                           Q(last_name__icontains=q_name))

    q_department = get_q_dashboard(request, 'department', 'int')
    if q_department:
        department = cached_department(q_department)
        if department is not None:
            q_department_list = department.get_all_child()
            account_list = account_list.filter(department_member_set__department__in=q_department_list)


    sum_list = []
    for code in ProgressAccount.CODE_LIST:
        for _ in ProgressAccount.FIELD_LIST:
            sum_list.append(Sum('%s%s'%(code, _)))
    progress_result = ProgressAccount.objects.filter(account__in=account_list,
                                                     is_root=True).aggregate(*sum_list)
    return send_json_account(progress_result)

def send_json_account(progress_result):
    return JsonResponse({
                'total_item': progress_result['count__sum'],
                'pie_label': ['Self-Enroll', 'Assignment'],
                'pie_data': [
                    progress_result['count_register__sum'],
                    progress_result['count_assignment__sum']
                ],
                'label': ['In Progress', 'Completed', 'Failed', 'Expired'],
                'all_content_data': [
                    progress_result['in_progress__sum'],
                    progress_result['completed__sum'],
                    progress_result['failed__sum'],
                    progress_result['expired__sum']
                ],
                'course_data': [
                    progress_result['course_in_progress__sum'],
                    progress_result['course_completed__sum'],
                    progress_result['course_failed__sum'],
                    progress_result['course_expired__sum'],
                ],
                'question_data': [
                    progress_result['question_in_progress__sum'],
                    progress_result['question_completed__sum'],
                    progress_result['question_failed__sum'],
                    progress_result['question_expired__sum'],
                ],
                'program_data': [
                    progress_result['program_in_progress__sum'],
                    progress_result['program_completed__sum'],
                    progress_result['program_failed__sum'],
                    progress_result['program_expired__sum'],
                ],
                'onboard_data': [
                    progress_result['onboard_in_progress__sum'],
                    progress_result['onboard_completed__sum'],
                    progress_result['onboard_failed__sum'],
                    progress_result['onboard_expired__sum'],
                ],
            })

def send_json(content_type_name, progress_result):
    return JsonResponse({
                'content_type_name': content_type_name,
                # 'total_add': progress_result['count__sum'],
                # 'by_standalone': progress_result['standalone__sum'],
                # 'by_included': progress_result['included__sum'],
                # 'learners': progress_result['learner__sum'],
                'pie_label': ['Self-Enroll', 'Assignment'],
                'pie_data': [
                    progress_result['register_count__sum'],
                    progress_result['assignment_count__sum'],
                ],
                'label': ['In Progress', 'Completed', 'Failed', 'Expired'],
                'self_enroll_item': progress_result['register_count__sum'],
                # 'self_enroll_percent': progress_result['register_percent'],
                'self_enroll_data': [
                    progress_result['register_in_progress__sum'],
                    progress_result['register_completed__sum'],
                    progress_result['register_failed__sum'],
                    progress_result['register_expired__sum'],
                ],
                'assignment_item': progress_result['assignment_count__sum'],
                # 'assignment_percent': progress_result['assignment_percent'],
                'assignment_data': [
                    progress_result['assignment_in_progress__sum'],
                    progress_result['assignment_completed__sum'],
                    progress_result['assignment_failed__sum'],
                    progress_result['assignment_expired__sum'],
                ]
            })
