import pytz
from django.conf import settings
from django.db.models import Q
from django.utils import timezone
from xlwt import Workbook

from account.models import Account
from alert.models import Alert
from department.cached import cached_department
from progress.models import Account as ProgressAccount
from .jobs import alert_fail
from .jobs import send_to_mail
from .jobs import style, style_1, style_col_4, style_col_5, style_col_6, style_col_7
import uuid
import json


def export_account_job(alert_id):
    try:
        alert = Alert.objects.get(id=alert_id)
    except:
        return None

    kwargs = json.loads(alert.json_kwargs)
    if not all([_ in kwargs for _ in ['name', 'perm']]):
        alert_fail(alert)
        return None

    if kwargs['perm'] == 'view':
        account_list = Account.objects.filter(
            progress_account_set__is_root=True,
            progress_account_set__count__gt=0
        ).order_by('-progress_account_set__timeupdate')
    elif kwargs['perm'] == 'org':
        if settings.IS_ORGANIZATION:
            account_list = Account.objects.filter(
                org_child_set__account=request.user,
                progress_account_set__count__gt=0
            ).distinct()
        else:
            alert_fail(alert)
            return None
    else:
        alert_fail(alert)
        return None

    q_name = kwargs['name'].strip()
    if len(q_name) > 0:
        account_list = account_list.filter(
            Q(first_name__icontains=q_name) |
            Q(last_name__icontains=q_name)
        )

    q_department = kwargs['department']
    if q_department:
        department = cached_department(q_department)
        if department:
            q_department_list = department.get_all_child()
            account_list = account_list.filter(
                department_member_set__department__in=q_department_list
            )

    if settings.ACCOUNT__HIDE_INACTIVE:
        account_list = account_list.exclude(is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        account_list = account_list.exclude(id__in=settings.ACCOUNT__HIDE_ID_LIST)
    for account in account_list:
        account.progress = ProgressAccount.pull(account.id, True)

    columns = [
        'EMAIL','EMPLOYEE', 'LEARNER', 'DEPARTMENT', 'POSITION',
        'IN PROGRESS', 'COMPLETED', 'FAILED', 'EXPIRED',
        '% PERCENT COMPLETED', 'INCOMING DUE DATE'
    ]

    workbook = Workbook()
    workbook.set_colour_RGB(0x19, 253, 236, 202)
    workbook.set_colour_RGB(0x20, 207, 222, 186)
    workbook.set_colour_RGB(0x21, 250, 192, 195)
    workbook.set_colour_RGB(0x22, 232, 232, 232)
    worksheet = workbook.add_sheet("Progress Report")

    worksheet.col(0).width = 30 * 256
    worksheet.col(1).width = 30 * 256
    worksheet.col(2).width = 25 * 256
    worksheet.col(3).width = 14 * 256
    worksheet.col(4).width = 14 * 256
    worksheet.col(5).width = 15 * 256
    worksheet.col(6).width = 13 * 256
    worksheet.col(7).width = 8 * 256
    worksheet.col(8).width = 10 * 256
    worksheet.col(9).width = 24 * 256
    worksheet.col(10).width = 22 * 256
    row_num = 0
    if len(q_name) > 0:
        worksheet.write(row_num, 0, 'Search by: %s' % q_name, style_1)
        row_num += 1
    if q_department and department:
        worksheet.write(row_num, 0, 'Department Filder by: %s' % department.name, style_1)
        row_num += 1

    for col_num in range(len(columns)):
        if col_num == 5:
            worksheet.write(row_num, col_num, columns[col_num], style_col_4)
        elif col_num == 6:
            worksheet.write(row_num, col_num, columns[col_num], style_col_5)
        elif col_num == 7:
            worksheet.write(row_num, col_num, columns[col_num], style_col_6)
        elif col_num == 8:
            worksheet.write(row_num, col_num, columns[col_num], style_col_7)
        else:
            worksheet.write(row_num, col_num, columns[col_num], style_1)
    for account in account_list:
        row_num += 1
        if account.progress.due_date is None:
            duedate = "-"
        else:
            date = account.progress.due_date

            time_zone = timezone.localtime(date, pytz.timezone('Asia/Bangkok'))
            duedate = time_zone.strftime('%Y-%m-%d %H:%M:%S')

        position = "-"
        for department_member in account.department_member_set.all():
            if department_member.position:
                position = department_member.position
            else:
                position = "-"

        row = [
            account.email,
            account.employee if account.employee else '-',
            str(account.first_name) + " " + str(account.last_name),
            account.get_department_display(),
            position,
            account.progress.in_progress,
            account.progress.completed,
            account.progress.failed,
            account.progress.expired,
            str(account.progress.get_percent()) + "%",
            duedate
        ]
        for col_num in range(len(row)):
            if col_num == 0 or col_num == 2:
                worksheet.write(row_num, col_num, row[col_num], style)
            elif col_num == 5:
                worksheet.write(row_num, col_num, row[col_num], style_col_4)
            elif col_num == 6:
                worksheet.write(row_num, col_num, row[col_num], style_col_5)
            elif col_num == 7:
                worksheet.write(row_num, col_num, row[col_num], style_col_6)
            elif col_num == 8:
                worksheet.write(row_num, col_num, row[col_num], style_col_7)
            else:
                worksheet.write(row_num, col_num, row[col_num], style_1)

    alert.filename = '%s.xls' % (uuid.uuid4())
    workbook.save('%s/%s' % (alert.get_export_path(), alert.filename))

    alert.status = 2
    alert.save()
    return send_to_mail(alert, alert.filename)