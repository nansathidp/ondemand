import json

import django_rq
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from alert.models import Alert
from .jobs_account import export_account_job
from .jobs_course import export_course_job
from .jobs_course_detail import export_course_detail_job
from .jobs_onboard import export_onboard_job
from .jobs_program import export_program_job
from .jobs_question import export_question_job
from .jobs_question_detail import export_question_detail_job


@csrf_exempt
def export_report_account(request):
    if request.user.has_perm('progress.view_progress',
                             group=request.DASHBOARD_GROUP):
        perm = 'view'
    elif request.user.has_perm('progress.view_own_progress',
                               group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    elif request.user.has_perm('progress.view_org_progress',
                               group=request.DASHBOARD_GROUP):
        if settings.IS_ORGANIZATION:
            perm = 'org'
        else:
            raise PermissionDenied
    else:
        raise PermissionDenied

    kwargs = {
        'name': request.POST.get('name'),
        'department': request.POST.get('department'),
        'perm': perm
    }
    alert = Alert.objects.create(
        account=request.user,
        page=settings.CONTENT_TYPE('progress', 'account'),
        json_kwargs=json.dumps(kwargs, indent=2),
        status=1,
    )
    django_rq.enqueue(export_account_job, alert.id)
    return JsonResponse({'alert_id': alert.id})

def _kwargs(request):
    if request.user.has_perm('progress.view_progress',
                             group=request.DASHBOARD_GROUP):
        perm = 'view'
        provider = None
        sub_provider = None
    elif request.user.has_perm('progress.view_own_progress', group=request.DASHBOARD_GROUP):
        if request.DASHBOARD_PROVIDER:
            perm = 'own'
            provider = request.DASHBOARD_PROVIDER.id
            sub_provider = None
        elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
            perm = 'own'
            provider = request.DASHBOARD_PROVIDER.id
            sub_provider = request.DASHBOARD_SUB_PROVIDER.id
        else:
            perm = 'own'
            provider = None
            sub_provider = None
    elif request.user.has_perm('progress.view_org_progress',
                               group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    else:
        raise PermissionDenied
    kwargs = {
        'name': request.POST.get('name', ''),
        'perm': perm,
        'provider': provider,
        'sub_provider': sub_provider
    }
    return kwargs

@csrf_exempt
def export_report_course(request):
    kwargs = _kwargs(request)
    alert = Alert.objects.create(
        account=request.user,
        page=settings.CONTENT_TYPE('course', 'course'),
        json_kwargs=json.dumps(kwargs, indent=2),
        status = 1,
    )
    django_rq.enqueue(export_course_job, alert.id)
    return JsonResponse({'alert_id': alert.id})


@csrf_exempt
def export_report_course_detail(request, content_id):
    kwargs = _kwargs(request)
    alert = Alert.objects.create(
        account=request.user,
        page=settings.CONTENT_TYPE('course', 'course'),
        content=content_id,
        json_kwargs=json.dumps(kwargs, indent=2),
        status=1,
    )
    django_rq.enqueue(export_course_detail_job, alert.id)
    return JsonResponse({'alert_id': alert.id})


@csrf_exempt
def export_report_test(request):
    kwargs = _kwargs(request)
    alert = Alert.objects.create(
        account=request.user,
        page=settings.CONTENT_TYPE('question', 'activity'),
        json_kwargs=json.dumps(kwargs, indent=2),
        status=1,
    )
    queue = django_rq.get_queue('default')
    queue.enqueue(export_question_job, alert.id)
    return JsonResponse({'alert_id': alert.id})

@csrf_exempt
def export_report_test_detail(request, content_id):
    kwargs = _kwargs(request)
    alert = Alert.objects.create(
        account=request.user,
        page=settings.CONTENT_TYPE('question', 'activity'),
        content=content_id,
        json_kwargs=json.dumps(kwargs, indent=2),
        status=1,
    )
    django_rq.enqueue(export_question_detail_job, alert.id)
    return JsonResponse({'alert_id': alert.id})

@csrf_exempt
def export_report_program(request):
    kwargs = _kwargs(request)
    alert = Alert.objects.create(
        account=request.user,
        page=settings.CONTENT_TYPE('program', 'program'),
        json_kwargs=json.dumps(kwargs, indent=2),
        status=1,
    )
    queue = django_rq.get_queue('default')
    queue.enqueue(export_program_job, alert.id)
    return JsonResponse({'alert_id': alert.id})


@csrf_exempt
def export_report_onboard(request):
    kwargs = _kwargs(request)
    alert = Alert.objects.create(
        account=request.user,
        page=settings.CONTENT_TYPE('program', 'onboard'),
        json_kwargs=json.dumps(kwargs, indent=2),
        status=1,
    )
    queue = django_rq.get_queue('default')
    queue.enqueue(export_onboard_job, alert.id)
    return JsonResponse({'alert_id': alert.id})
