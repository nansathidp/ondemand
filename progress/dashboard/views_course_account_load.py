from django.conf import settings
from django.shortcuts import render, get_object_or_404

from progress.models import Progress
from account.models import Account
from course.models import Course
from order.models import Item as OrderItem
from content.models import Location as ContentLocation

from utils.breadcrumb import get_breadcrumb


def course_account_load_view(request, course_id, account_id, content_location_id):
    course = get_object_or_404(Course, id=course_id)
    account = get_object_or_404(Account, id=account_id)
    request.location = get_object_or_404(ContentLocation, id=content_location_id)

    return render(request,
                  'progress/dashboard/course_account_block.html',
                  {})
