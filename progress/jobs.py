from django.conf import settings

from .models import Progress, Account as ProgressAccount, Content

from course.models import Course
from question.models import Activity
from program.models import Program
from account.models import Account
from order.models import Item as OrderItem

from .cached import cached_progress_content_delete
from progress.cached import cached_progress_delete
from order.cached import cached_order_item_update

def content_update_progress_job(location_id, content_type_id, content_id):
    try:
        Content.update_progress(location_id, content_type_id, content_id)
    except:
        pass

def account_progress_hard_job():
    for account in Account.objects.all():
        try:
            ProgressAccount.update_progress(account.id)
        except:
            raise
            pass
        
def _hard(content_list, progress_content_list):
    content_list = list(content_list)
    for progress_content in progress_content_list:
        is_found = False
        for content in content_list:
            if content.id == progress_content.content:
                is_found = True
                break

        if is_found:
            try:
                Content.update_progress(progress_content.location_id,
                                        progress_content.content_type_id,
                                        progress_content.content)
            except:
                raise
                pass
        else:
            cached_progress_content_delete(progress_content)
            progress_content.delete()

def _course():
    content_list = Course.objects.all()
    progress_content_list = Content.objects.filter(content_type=settings.CONTENT_TYPE('course', 'course'))
    _hard(content_list, progress_content_list)

def _question():
    content_list = Course.objects.all()
    progress_content_list = Content.objects.filter(content_type=settings.CONTENT_TYPE('question', 'activity'))
    _hard(content_list, progress_content_list)

def _onboard():
    content_list = Course.objects.all()
    progress_content_list = Content.objects.filter(content_type=settings.CONTENT_TYPE('program', 'onboard'))
    _hard(content_list, progress_content_list)

def _program():
    content_list = Course.objects.all()
    progress_content_list = Content.objects.filter(content_type=settings.CONTENT_TYPE('program', 'program'))
    _hard(content_list, progress_content_list)
    
    # Fix Check content_type program -> onboard (in Order.Item)
    _content_type = settings.CONTENT_TYPE('program', 'program')
    _content_type_onboard = settings.CONTENT_TYPE('program', 'onboard')
    for program in Program.objects.filter(type=2):
        order_item_list = OrderItem.objects.filter(content_type__in=[_content_type, _content_type_onboard],
                                                   content=program.id)
        for order_item in order_item_list:
            order_item.content_type = _content_type_onboard
            order_item.save()
            cached_order_item_update(order_item)

            for progress in Progress.objects.filter(item=order_item):
                if progress.is_root:
                    progress.content_type = _content_type_onboard
                    progress.save()
                cached_progress_delete(progress)
                    
def content_progress_hard_job(content_type_id):
    # print ('Debug: content_progress_hard_job', content_type_id)
    if content_type_id == settings.CONTENT_TYPE('course', 'course').id:
        _course()
    elif content_type_id == settings.CONTENT_TYPE('question', 'activity').id:
        _question()
    elif content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
        _onboard()
    elif content_type_id == settings.CONTENT_TYPE('program', 'program').id:
        _program()

