from django import template
from django.conf import settings

from ..models import Progress, Content
from content.models import Location as ContentLocation

register = template.Library()


@register.simple_tag
def pull_progress_tag(order_item, content_location, content_type, content_id):
    return Progress.pull(order_item,
                         content_location,
                         content_type,
                         content_id)


@register.simple_tag
def pull_progress_content_tag(location_id, content_type_id, content_id):
    return Content.pull(location_id,
                        content_type_id,
                        content_id)


def _get_progress(material, order_item, content_location_material, program):
    # TODO: remove content_location
    return material.get_progress(order_item, program)

    # if material.type == 3:
    #     if program and program.type == 2:
    #         content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
    #                                                       material.question_id,
    #                                                       parent1_content_type=settings.CONTENT_TYPE('program', 'onboard'),
    #                                                       parent1_content=program.id,
    #                                                       parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
    #                                                       parent2_content=material.course_id,
    #                                                       parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
    #                                                       parent3_content=material.id)
    #     elif program:
    #         content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
    #                                                       material.question_id,
    #                                                       parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
    #                                                       parent1_content=program.id,
    #                                                       parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
    #                                                       parent2_content=material.course_id,
    #                                                       parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
    #                                                       parent3_content=material.id)
    #     else:
    #         content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
    #                                                       material.question_id,
    #                                                       parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
    #                                                       parent1_content=material.course_id,
    #                                                       parent2_content_type=settings.CONTENT_TYPE('course', 'material'),
    #                                                       parent2_content=material.id)

    #     progress = Progress.pull(order_item,
    #                              content_location,
    #                              settings.CONTENT_TYPE('question', 'activity'),
    #                              material.question_id)
    # else:
    #     progress = Progress.pull(order_item,
    #                              content_location_material,
    #                              settings.CONTENT_TYPE('course', 'material'),
    #                              material.id)
    # return progress


@register.inclusion_tag('course/templatetags/progress.html')
def progress_new_tag(material, order_item, content_location_material, program):
    return {'progress': _get_progress(material, order_item, content_location_material, program)}


@register.simple_tag
def progress_circle_tag(material, material_item, order_item, content_location_material, program):
    if material.id == material_item.id:
        return 'fa-play-circle'
    else:
        progress = _get_progress(material_item, order_item, content_location_material, program)
        if progress.status == 2:
            return 'fa-check-circle'
        elif progress.status == 3:
            return 'fa-times-circle red'
        else:
            return 'fa-circle-thin'
    return 'fa-circle-thin'
