from django.db import models


class Progress(models.Model):
    from django.conf import settings
    import datetime

    STATUS_CHOICES = (
        (1, 'In Progress'),
        (2, 'Completed'),
        (3, 'Failed'),
        (4, 'Expired'),
        (5, 'Verifying'),  # For Question
    )
    
    item = models.ForeignKey('order.Item')
    location = models.ForeignKey('content.Location', null=True, related_name='+') #TODO remove null=True
    account = models.ForeignKey(settings.AUTH_USER_MODEL)

    content_type = models.ForeignKey('contenttypes.ContentType')
    content = models.BigIntegerField(db_index=True)

    is_root = models.BooleanField(default=True)
    percent = models.FloatField(default=0.0)
    score = models.IntegerField(default=0)  # for Question
    duration = models.DurationField(default=datetime.timedelta(0))  # for course.Material (Video)

    # For Group of item (Package/Program/OnBoard)
    content_count = models.IntegerField(default=0)
    content_complete = models.IntegerField(default=0)

    # Course.Outline, Question.Activity.Section
    section_count = models.IntegerField(default=0)
    section_complete = models.IntegerField(default=0)

    # Question.Activity.Question
    material_count = models.IntegerField(default=0)
    material_complete = models.IntegerField(default=0)

    # Check False When Update Course/Question/Task and add,delete in Program
    is_update = models.BooleanField(default=True, db_index=True)

    status = models.IntegerField(choices=STATUS_CHOICES, default=1)
    timeupdate = models.DateTimeField(auto_now=True, db_index=True)
    
    class Meta:
        default_permissions = ('view', 'view_own', 'view_org', 'add', 'change', 'delete')
        ordering = ['-timeupdate']

    def __str__(self):
        return str(self.id)
    
    @staticmethod
    def pull(order_item, content_location, content_type, content_id):
        from .cached import cached_progress
        return cached_progress(order_item,
                               content_location,
                               content_type,
                               content_id)

    def content_update_progress(self):
        from .jobs import content_update_progress_job
        import django_rq
        
        django_rq.enqueue(content_update_progress_job,
                          self.location_id,
                          self.content_type_id,
                          self.content)
        
    def push_expired(self):
        if self.status == 1:
            from .cached import cached_progress_update
            self.status = 4
            self.save(update_fields=['status', 'timeupdate'])
            cached_progress_update(self)
        
    def duration_display(self):
        from utils.duration import duration_display
        return duration_display(self.duration)

        # 2017-10-27
        # s = self.duration.total_seconds()
        # d, r = divmod(s, 60 * 60 * 24)
        # h, r = divmod(r, 3600)
        # m, s = divmod(r, 60)
        # if d > 0:
        #     return '%d Day %d:%02d:%02d' % (d, h, m, s)
        # elif h > 0:
        #     return '%d:%02d:%02d' % (h, m, s)
        # else:
        #     return '%d:%02d' % (m, s)

    def is_active(self, order_item):
        import datetime
        if self.status == 4:
            return False
        elif self.duration > order_item.credit > datetime.timedelta(0):
            self.push_expired()
            return False
        elif order_item.is_expired():
            self.push_expired()
            return False
        return True

    def api_display(self):
        from django.conf import settings

        def _progress_status(percent): #Fix For old api (Course)
            if percent == 0.0:
                return 0
            elif 0 < percent < 90:
                return 1
            else:
                return 2

        status_display = self.get_status_display()
        if self.content_type_id == settings.CONTENT_TYPE('question', 'activity').id and self.status == 2:
            if settings.PROJECT != 'cimb':
                status_display = 'Passed'
        return {
            'id': self.id,
            'score': self.score,
            'status': self.status,
            'status_display': status_display,
            'progress_status': _progress_status(self.percent),  # Fix For old api (Course)
            'note': 'progress_status is Deprecated!! Do not use.',
            'duration': self.duration_display(),
            'percent': round(self.percent),
            'is_complete': True if self.status == 2 else False,
            'content_complete': self.content_complete,
            'content_count': self.content_count,
            'material_count': self.material_count,
            'material_complete': self.material_complete,
            'timeupdate': self.timeupdate.strftime(settings.TIME_FORMAT)
        }

    def get_content_cached(self):
        from utils.content_cached import _get_content_cached
        _get_content_cached(self)

    def get_account_cached(self):
        from account.cached import cached_account
        self.account_cached = cached_account(self.account_id)

    def content_update(self):
        from django.conf import settings
        from course.models import Course
        from program.models import Program

        def _update(self, count):
            # TODO : Check material_count
            self.content_count = count['content_count']
            self.section_count = count['section_count']
            self.material_count = count['material_count']
            self.is_update = True
            self.save(update_fields=['content_count', 'section_count', 'material_count', 'is_update'])

        if self.is_update:
            return
        if self.status == 2:
            if self.material_count == 0:
                pass
            else:
                if not self.is_update:  # Fix clean case: loop update
                    self.is_update = True
                    self.save(update_fields=['is_update'])
                return

        content_type_program = settings.CONTENT_TYPE('program', 'program')
        content_type_course = settings.CONTENT_TYPE('course', 'course')

        if self.content_type_id == content_type_program.id:
            program = Program.pull(self.content)
            if program is not None:
                count = program.progress_count()
                _update(self, count)
                self.check_complete_program(self.item, content_type_program)
        elif self.content_type_id == content_type_course.id:
            course = Course.pull(self.content)
            if course is not None:
                count = course.progress_count()
                _update(self, count)
                self.check_complete_course(self.item, course)

    def check_complete_program(self, order_item, content_type): # TODO: Fix content_type (program / onboard)
        from django.conf import settings

        from program.models import Program
        from content.models import Location as ContentLocation        
        from .models import Account as ProgressAccount
        from account.cached import cached_account_delete
        from .cached import cached_progress_update

        status_old = self.status
        program = Program.pull(self.content)
        self.content_complete = 0
        self.section_complete = 0
        self.material_complete = 0
        is_fail = False
        for _item in program.item_set.all():
            _content_type = settings.CONTENT_TYPE_ID(_item.content_type_id)
            _content_location = ContentLocation.pull_first(_content_type,
                                                           _item.content,
                                                           content_type,
                                                           program.id)
            _progress = Progress.pull(order_item,
                                      _content_location,
                                      _content_type,
                                      _item.content)

            if _progress:
                if _progress.status == 3:
                    is_fail = True
                self.content_complete += _progress.content_complete
                self.section_complete += _progress.section_complete
                self.material_complete += _progress.material_complete

        if is_fail:
            self.status = 3
        elif self.content_complete == self.content_count:
            self.percent = 100.0
            self.status = 2

        try:
            self.percent = float(self.content_complete) / float(self.content_count) * 100.0
            if self.percent > 100.0:
                self.percent = 100.0
        except:
            self.percent = 0.0

        self.save(update_fields=['percent', 'content_complete', 'section_complete', 'material_complete', 'status', 'timeupdate'])
        cached_progress_update(self)
        if status_old != self.status:
            ProgressAccount.update_progress(self.account_id)
            self.content_update_progress()
            if self.status == 2 and program.type == 2:
                cached_account_delete(self.account_id)


    def check_complete_course(self, order_item, course, question_status=None):
        from django.conf import settings

        from course.models import Material
        from .models import Account as ProgressAccount
        from content.models import Location as ContentLocation
        
        from .cached import cached_progress_update

        status_old = self.status
        material_id_list = Material.objects.values_list('id', flat=True) \
                                           .filter(course=course)

        material_complete = Progress.objects.filter(item_id=self.item_id,
                                                    content_type=settings.CONTENT_TYPE('course', 'material'),
                                                    content__in=material_id_list,
                                                    status=2) \
                                            .count()
        for _material in Material.objects.filter(course=course, type=3):
            if order_item.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
                _content_location = ContentLocation.pull_first(
                    settings.CONTENT_TYPE('question', 'activity'),
                    _material.question_id,
                    parent1_content_type=settings.CONTENT_TYPE('program', 'onboard'),
                    parent1_content=order_item.content,
                    parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                    parent2_content=course.id,
                    parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
                    parent3_content=_material.id
                )
            elif order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
                _content_location = ContentLocation.pull_first(
                    settings.CONTENT_TYPE('question', 'activity'),
                    _material.question_id,
                    parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                    parent1_content=order_item.content,
                    parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                    parent2_content=course.id,
                    parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
                    parent3_content=_material.id
                )
            else:
                _content_location = ContentLocation.pull_first(
                    settings.CONTENT_TYPE('question', 'activity'),
                    _material.question_id,
                    parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                    parent1_content=course.id,
                    parent2_content_type=settings.CONTENT_TYPE('course', 'material'),
                    parent2_content=_material.id
                )
            _progress = Progress.pull(order_item,
                                      _content_location,
                                      settings.CONTENT_TYPE('question', 'activity'),
                                      _material.question_id)
            if _progress.status == 2:
                material_complete += 1
        self.material_complete = material_complete
        try:
            self.percent = float(self.material_complete) / float(self.material_count) * 100.0
        except:
            self.percent = 0.0
        if self.material_complete >= self.material_count:
            self.percent = 100.0
            self.content_complete = 1
            self.status = 2
        else:
            self.content_complete = 0
        if question_status is not None and question_status == 3:
            self.status = 3
        #self.save(update_fields=['percent', 'content_complete', 'material_count', 'material_complete', 'status', 'timeupdate'])
        self.save()
        cached_progress_update(self)
        if status_old != self.status:
            ProgressAccount.update_progress(self.account_id)
            self.content_update_progress()

        if order_item.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
            if self.status == 2:
                progress = Progress.pull(order_item,
                                         ContentLocation.pull(order_item.content_location_id),
                                         settings.CONTENT_TYPE('program', 'onboard'),
                                         order_item.content)
                if progress is not None:
                    progress.check_complete_program(self.item, settings.CONTENT_TYPE('program', 'onboard'))
        elif order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
            if self.status == 2:
                progress = Progress.pull(order_item,
                                         ContentLocation.pull(order_item.content_location_id),
                                         settings.CONTENT_TYPE('program', 'program'),
                                         order_item.content)
                if progress is not None:
                    progress.check_complete_program(self.item, settings.CONTENT_TYPE('program', 'program'))

    def check_complete_question(self, order_item, activity, content_location,
                                material, # TODO : Cleen?
                                status):
        from django.conf import settings
        from .models import Account as ProgressAccount
        from content.models import Location as ContentLocation
        from .cached import cached_progress_update

        status_old = self.status
        if status == 5:
            self.percent = 0
            self.status = status
        elif activity.pass_score == 0:
            self.percent = 100
            self.status = 2
        elif self.score >= activity.pass_score:
            self.percent = 100
            self.status = 2
        else:
            self.percent = 0
            if activity.is_submit(order_item, content_location):
                self.status = 1
            else:
                self.status = 3

        if self.status == 2:
            self.content_complete = 1
            self.section_complete = self.section_count
            self.material_complete = self.material_count
        else:
            self.content_complete = 0
            self.section_complete = 0
            self.material_complete = 0

        self.save(update_fields=['score', 'percent', 'content_complete', 'section_complete', 'material_complete', 'status', 'timeupdate'])
        cached_progress_update(self)
        if status_old != self.status:
            ProgressAccount.update_progress(self.account_id)
            self.content_update_progress()
            
        if material is not None:
            if order_item.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
                _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                               material.course_id,
                                                               parent1_content_type=settings.CONTENT_TYPE('program', 'onboard'),
                                                               parent1_content=order_item.content)
            elif order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
                _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                               material.course_id,
                                                               parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                               parent1_content=order_item.content)
            else:
                _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                               material.course_id)
            progress = Progress.pull(self.item,
                                     _content_location,
                                     settings.CONTENT_TYPE('course', 'course'),
                                     material.course_id)
            if progress is not None:
                progress.check_complete_course(self.item, material.course, question_status=self.status)
        elif order_item.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
            _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('program', 'onboard'),
                                                           order_item.content)
            progress = Progress.pull(order_item,
                                     _content_location,
                                     settings.CONTENT_TYPE('program', 'onboard'),
                                     order_item.content)
            if progress is not None:
                progress.check_complete_program(order_item, settings.CONTENT_TYPE('program', 'onboard'))
        elif order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
            _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('program', 'program'),
                                                           order_item.content)
            progress = Progress.pull(order_item,
                                     _content_location,
                                     settings.CONTENT_TYPE('program', 'program'),
                                     order_item.content)
            if progress is not None:
                progress.check_complete_program(order_item, settings.CONTENT_TYPE('program', 'program'))

                
    def check_complete_task(self, approve):
        from django.conf import settings
        from .models import Account as ProgressAccount, Content as ProgressContent
        from content.models import Location as ContentLocation
        from .cached import cached_progress_update

        status_old = self.status
        if approve.is_approve:
            self.percent = 100.0
            self.content_complete = 1
            self.section_complete = 1
            self.material_complete = 1
            self.status = 2
        else:
            self.percent = 0.0
            self.content_complete = 0
            self.section_complete = 0
            self.material_complete = 0
            self.status = 1
        self.save(update_fields=['percent', 'content_complete', 'section_complete', 'material_complete', 'status', 'timeupdate'])
        cached_progress_update(self)
        if status_old != self.status:
            ProgressAccount.update_progress(self.account_id)

        ProgressContent.update_progress(self.location_id,
                                        settings.CONTENT_TYPE('task', 'task').id,
                                        self.content)


        if self.item.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
            progress = Progress.pull(self.item,
                                     ContentLocation.pull(self.item.content_location_id),
                                     settings.CONTENT_TYPE('program', 'onboard'),
                                     self.item.content)
            if progress is not None:
                progress.check_complete_program(self.item, settings.CONTENT_TYPE('program', 'onboard'))
        elif self.item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
            progress = Progress.pull(self.item,
                                     ContentLocation.pull(self.item.content_location_id),
                                     settings.CONTENT_TYPE('program', 'program'),
                                     self.item.content)
            if progress is not None:
                progress.check_complete_program(self.item, settings.CONTENT_TYPE('program', 'program'))

    def check_complete_scorm(self, order_item, material, percent):
        from django.conf import settings
        from content.models import Location as ContentLocation
        from .cached import cached_progress_update

        self.percent = percent
        if percent == 100:
            self.status = 2
        else:
            self.status = 1
        self.save(update_fields=['percent', 'status'])
        cached_progress_update(self)

        if order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
            _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                           material.course_id,
                                                           parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                           parent1_content=order_item.content)
        else:
            _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                           material.course_id)
        progress = Progress.pull(order_item,
                                 _content_location,
                                 settings.CONTENT_TYPE('course', 'course'),
                                 material.course_id)
        if progress is not None:
            progress.check_complete_course(order_item, material.course)

    # For Web Link And Docs
    def check_complete_material(self, order_item, material):
        from django.conf import settings

        from content.models import Location as ContentLocation
        
        from .cached import cached_progress_update
        
        self.material_complete = 1
        self.status = 2
        self.percent = 100
        self.save(update_fields=['material_complete', 'status', 'percent', 'timeupdate'])
        cached_progress_update(self)

        if order_item.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
            _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                           material.course_id,
                                                           parent1_content_type=settings.CONTENT_TYPE('program', 'onboard'),
                                                           parent1_content=order_item.content)
        elif order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
            _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                           material.course_id,
                                                           parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                           parent1_content=order_item.content)
        else:
            _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                           material.course_id)
        progress = Progress.pull(order_item,
                                 _content_location,
                                 settings.CONTENT_TYPE('course', 'course'),
                                 material.course_id)
        if progress is not None:
            progress.check_complete_course(order_item, material.course)

    def read_material(self):
        from .cached import cached_progress_update
        self.status = 2
        self.save(update_fields=['status'])
        cached_progress_update(self)

    def push_material(self, progress_course, order_item, account, material, action, duration):
        from django.conf import settings
        from django.utils import timezone
        from django.db.models import Sum

        from content.models import Location as ContentLocation
        
        from .cached import cached_progress_update, cached_progress_videolog_last_v2, cached_progress_videolog_last_update_v2

        import datetime, math

        IS_DEBUG = False

        now = timezone.now()
        is_fail = False
        video_log = cached_progress_videolog_last_v2(progress_course, material)
        if IS_DEBUG:
            if video_log is not None:
                print('video_log : ', video_log.action)
            else:
                print('video_log : None')
            print('action : ', action)
        credit_use = 0
        if action == 1:
            pass
        elif action == 2:
            if video_log is not None:
                if video_log.action in [1, 3, 5, 6]:
                    credit_use = (duration - video_log.duration).total_seconds()
                elif video_log.action == 2:
                    is_fail = True
                elif video_log.action == 4:
                    credit_use = (now - video_log.timestamp).total_seconds()
                else:
                    pass
            else:
                is_fail = True
        elif action == 3:
            pass
        elif action == 4:
            if video_log is not None:
                if video_log.action in [1, 3, 4, 6]:
                    credit_use = (now - video_log.timestamp).total_seconds()
            else:
                is_fail = True
        elif action == 5:
            if video_log is not None:
                credit_use = (duration - video_log.duration).total_seconds()
            else:
                is_fail = True
        elif action == 6:
            if video_log is not None:
                if video_log.action in [1, 3, 4, 6]:
                    credit_use = (duration - video_log.duration).total_seconds()
                else:
                    is_fail = True
            else:
                is_fail = True
        if video_log is not None:
            if video_log.action == action and video_log.duration == duration and video_log.percent == self.percent and credit_use == 0:
                return
        if credit_use > 120:
            credit_use = 120

        if IS_DEBUG:
            print('Debug Progress: credit_use =', credit_use)
        video_log = VideoLog.objects.create(item=order_item,
                                            progress=progress_course,
                                            account=account,
                                            material=material,
                                            action=action,
                                            duration=duration,
                                            percent=self.percent,
                                            is_fail=is_fail,
                                            credit_use=datetime.timedelta(seconds=credit_use))
        
        #Percent
        is_progress_change = False
        if credit_use > 0:
            result = VideoLog.objects.filter(item=order_item,
                                             material=material) \
                                     .aggregate(Sum('credit_use'))
            try:
                credit_use_total = result['credit_use__sum'].total_seconds()
                if IS_DEBUG:
                    print('debug: credit_use_total >>', credit_use_total)
                credit_use_percent = int(math.ceil((credit_use_total/float(material.video_duration.total_seconds())) * 100.0))
                if credit_use_percent > 100:
                    credit_use_percent = 100
            except:
                credit_use_total = 0
                credit_use_percent = 0
        else:
            credit_use_percent = 0

        if IS_DEBUG:
            print('debug: Duration Old = ', self.duration)
        if duration > self.duration or action == 5:
            self.duration = duration
            is_progress_change = True
        if IS_DEBUG:
            print('debug: Duration New = ', self.duration)
        try:
            duration_percent = int(math.ceil((duration.total_seconds()/float(material.video_duration.total_seconds())) * 100.0))
            if duration_percent > 100 or action == 5:
                duration_percent = 100
        except:
            duration_percent = 0

        if IS_DEBUG:
            print('Progress Debug: ', 'Credit=', credit_use_percent, 'Duration=', duration_percent)
            print('Old Percent:', self.percent)
        if credit_use_percent < duration_percent:
            if credit_use_percent > self.percent:
                self.percent = credit_use_percent
                is_progress_change = True
                if self.percent >= 80:
                    self.status = 2
                    self.material_complete = 1
                    self.percent = 100
        else:
            if duration_percent > self.percent:
                self.percent = duration_percent
                is_progress_change = True
                if self.percent >= 90:
                    self.status = 2
                    self.material_complete = 1
                    self.percent = 100
        if IS_DEBUG:
            print('New Percent:', self.percent)
        video_log.percent = self.percent
        video_log.save(update_fields=['percent'])
        cached_progress_videolog_last_update_v2(video_log)
        if order_item.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
            _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                           material.course_id,
                                                           parent1_content_type=settings.CONTENT_TYPE('program', 'onboard'),
                                                           parent1_content=order_item.content)
        elif order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
            _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                           material.course_id,
                                                           parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                           parent1_content=order_item.content)
        else:
            _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                           material.course_id)
        if is_progress_change:
            self.save(update_fields=['duration', 'percent', 'status', 'material_complete'])
            cached_progress_update(self)

            #push to progress
            progress = Progress.pull(order_item,
                                     _content_location,
                                     settings.CONTENT_TYPE('course', 'course'),
                                     material.course_id)
            if progress is not None and self.status == 2:
                progress.check_complete_course(order_item, material.course)

        #Update CreditUse
        progress = Progress.pull(order_item,
                                 _content_location,
                                 settings.CONTENT_TYPE('course', 'course'),
                                 material.course_id)
        if progress is not None:
            try:
                result = VideoLog.objects.filter(item_id=self.item_id,
                                                 material__course_id=material.course_id) \
                                         .aggregate(Sum('credit_use'))
                if progress.duration != result['credit_use__sum']:
                    progress.duration = result['credit_use__sum']
                    progress.save(update_fields=['duration'])
                    cached_progress_update(progress)
                    #print('debug: progress course: ', progress.duration)
            except:
                pass
                #print('debug: progress course: fail')

                
class VideoLog(models.Model):
    from django.conf import settings

    ACTION_CHOICES = (
        (0, '- (mobile)'),
        (1, 'Start'),
        (2, 'Pause'),
        (3, 'Resume'),
        (4, 'Seek'),
        (5, 'Finish'),
        (6, 'Ping'),
    )

    item = models.ForeignKey('order.Item')
    progress = models.ForeignKey(Progress)
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    material = models.ForeignKey('course.Material')
    action = models.IntegerField(choices=ACTION_CHOICES, default=0, db_index=True)
    duration = models.DurationField(default=0)
    percent = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    is_fail = models.BooleanField(default=False)
    credit_use = models.DurationField()

    class Meta:
        ordering = ['-timestamp']


class Account(models.Model):
    from django.conf import settings

    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='progress_account_set')
    is_root = models.BooleanField(default=True)
    
    in_progress = models.IntegerField(default=0)
    completed = models.IntegerField(default=0)
    failed = models.IntegerField(default=0)
    expired = models.IntegerField(default=0)
    count = models.IntegerField(default=0)
    count_assignment = models.IntegerField(default=0)
    count_register = models.IntegerField(default=0)

    course_in_progress = models.IntegerField(default=0)
    course_completed = models.IntegerField(default=0)
    course_failed = models.IntegerField(default=0)
    course_expired = models.IntegerField(default=0)
    course_count = models.IntegerField(default=0)
    course_count_assignment = models.IntegerField(default=0)
    course_count_register = models.IntegerField(default=0)

    question_in_progress = models.IntegerField(default=0)
    question_completed = models.IntegerField(default=0)
    question_failed = models.IntegerField(default=0)
    question_expired = models.IntegerField(default=0)
    question_verifying = models.IntegerField(default=0)
    question_count = models.IntegerField(default=0)
    question_count_assignment = models.IntegerField(default=0)
    question_count_register = models.IntegerField(default=0)

    program_in_progress = models.IntegerField(default=0)
    program_completed = models.IntegerField(default=0)
    program_failed = models.IntegerField(default=0)
    program_expired = models.IntegerField(default=0)
    program_count = models.IntegerField(default=0)
    program_count_assignment = models.IntegerField(default=0)
    program_count_register = models.IntegerField(default=0)

    onboard_in_progress = models.IntegerField(default=0)
    onboard_completed = models.IntegerField(default=0)
    onboard_failed = models.IntegerField(default=0)
    onboard_expired = models.IntegerField(default=0)
    onboard_count = models.IntegerField(default=0)
    onboard_count_assignment = models.IntegerField(default=0)
    onboard_count_register = models.IntegerField(default=0)
    
    due_date = models.DateTimeField(null=True)
    timeupdate = models.DateTimeField(auto_now=True, db_index=True)

    CODE_LIST = ['', 'course_', 'question_', 'program_', 'onboard_']
    FIELD_LIST = ['in_progress', 'completed', 'failed', 'expired', 'count', 'count_assignment', 'count_register']
    
    class Meta:
        ordering = ['-timeupdate']

    @staticmethod
    def pull(account_id, is_root):
        from .cached import cached_progress_account
        return cached_progress_account(account_id, is_root)

    @staticmethod
    def _update_progress(account_id, is_root):
        import datetime
        from django.conf import settings
        
        from django.utils import timezone
        from order.models import Item as OrderItem
        from program.models import Program
        from .cached import cached_progress_account, cached_progress_account_update

        progress_account = cached_progress_account(account_id, is_root)

        def _set_zoro(code):
            for _ in ['in_progress', 'completed', 'failed', 'expired', 'count', 'count_assignment', 'count_register']:
                setattr(progress_account, '%s%s'%(code, _), 0)

        def _count(code, status, order_item):
            if status == 1 or status == 5:
                _ = 'in_progress'
            elif status == 2:
                _ = 'completed'
            elif status == 3:
                _ = 'failed'
            elif status == 4:
                _ = 'expired'
            else:
                return None
            n = getattr(progress_account, '%s%s'%(code, _), 0)
            setattr(progress_account, '%s%s'%(code, _), n+1)

            n = getattr(progress_account, '%scount'%(code), 0)
            setattr(progress_account, '%scount'%(code), n+1)
            if order_item.order.method == 21:
                n = getattr(progress_account, '%scount_assignment'%(code), 0)
                setattr(progress_account, '%scount_assignment'%(code), n+1)
            else:
                n = getattr(progress_account, '%scount_register'%(code), 0)
                setattr(progress_account, '%scount_register'%(code), n+1)
                
        for _ in ['', 'course_', 'question_', 'program_', 'onboard_']:
            _set_zoro(_)
        progress_account.question_verifying = 0
        
        progress_list = Progress.objects.filter(account_id=progress_account.account_id)
        if is_root:
            progress_list = progress_list.filter(is_root=True)
        for progress in progress_list:
            order_item = OrderItem.pull(progress.item_id)
            if progress.content_type_id == settings.CONTENT_TYPE('course', 'course').id:
                _count('', progress.status, order_item)
                _count('course_', progress.status, order_item)
            elif progress.content_type_id == settings.CONTENT_TYPE('question', 'activity').id:
                _count('', progress.status, order_item)
                _count('question_', progress.status, order_item)
                if progress.status == 5:
                    n = getattr(progress_account, 'question_verifying', 0)
                    setattr(progress_account, 'question_verifying', n+1)
            elif progress.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
                program = Program.pull(progress.content)
                _count('', progress.status, order_item)
                if program.type == 1:
                    pass # TODO : Find BUG!!
                else:
                    _count('onboard_', progress.status, order_item)
            elif progress.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
                program = Program.pull(progress.content)
                _count('', progress.status, order_item)
                if program and program.type == 1:
                    _count('program_', progress.status, order_item)
                else:
                    pass # TODO : Find BUG!!
            
        progress_account.due_date = None
        for item in OrderItem.objects.filter(account_id=progress_account.account_id,
                                             start__isnull=False,
                                             expired__gt=datetime.timedelta(0)):
            _due_date = item.start + item.expired
            if progress_account.due_date is None:
                progress_account.due_date = _due_date
            elif _due_date > timezone.now() and _due_date > progress_account.due_date:
                progress_account.due_date = _due_date

        progress_account.save()
        cached_progress_account_update(progress_account)
        
    @staticmethod
    def update_progress(account_id):
        Account._update_progress(account_id, False)
        Account._update_progress(account_id, True)

    def get_percent(self):
        try:
            return int(float(self.completed) / float(self.in_progress + self.completed + self.failed + self.expired) * 100.0)
        except:
            return 0


class AccountContent(models.Model):
    from django.conf import settings

    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    provider = models.ForeignKey('provider.Provider', null=True, on_delete=models.CASCADE)

    course = models.IntegerField(default=0)
    program = models.IntegerField(default=0)
    question = models.IntegerField(default=0)
    live = models.IntegerField(default=0)

    timeupdate = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        ordering = ['-timeupdate']

    def update_count(self, course=True, package=True, program=True, question=True, live=True, lesson_subscribe=True, lesson_owner=True):
        from django.conf import settings
        from order.models import Item
        from .cached import cached_progress_account_content_update
        from program.cached import cached_onboard_id_list
        if course:
            if self.provider is None:
                self.course = Item.objects.filter(account_id=self.account_id,
                                                  order__status=3,
                                                  status=2,
                                                  content_type=settings.CONTENT_TYPE('course', 'course')).count()
            else:
                self.course = Item.objects.filter(account_id=self.account_id,
                                                  order__status=3,
                                                  status=2,
                                                  content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  providers=self.provider).distinct().count()
        if package:
            if self.provider is None:
                self.package = Item.objects.filter(account_id=self.account_id,
                                                   order__status=3,
                                                   status=2,
                                                   content_type=settings.CONTENT_TYPE('package', 'package')).count()
            else:
                self.package = Item.objects.filter(account_id=self.account_id,
                                                   order__status=3,
                                                   status=2,
                                                   content_type=settings.CONTENT_TYPE('package', 'package'),
                                                   providers=self.provider).count()
        if program:
            onboard_id_list = cached_onboard_id_list()
            self.program = Item.objects.filter(account_id=self.account_id,
                                               order__status=3,
                                               status=2,
                                               content_type=settings.CONTENT_TYPE('program', 'program')).exclude(content__in=onboard_id_list).count()
        if question:
            self.question = Item.objects.filter(account_id=self.account_id,
                                                order__status=3,
                                                status=2,
                                                content_type=settings.CONTENT_TYPE('question', 'activity')).count()
        if live:
            self.live = Item.objects.filter(account_id=self.account_id,
                                            order__status=3,
                                            status=2,
                                            content_type=settings.CONTENT_TYPE('live', 'live')).count()

        self.save()
        cached_progress_account_content_update(self)


class Content(models.Model):
    location = models.ForeignKey('content.Location', null=True, related_name='+')
    is_root = models.BooleanField(default=True)
    
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    content = models.BigIntegerField(db_index=True)
    
    in_progress = models.IntegerField(default=0)
    completed = models.IntegerField(default=0)
    failed = models.IntegerField(default=0)
    expired = models.IntegerField(default=0)
    verifying = models.IntegerField(default=0) #Use in Dashboard -> Question
    count = models.IntegerField(default=0)
    standalone = models.IntegerField(default=0)
    included = models.IntegerField(default=0)
    learner = models.IntegerField(default=0)

    assignment_in_progress = models.IntegerField(default=0)
    assignment_completed = models.IntegerField(default=0)
    assignment_failed = models.IntegerField(default=0)
    assignment_expired = models.IntegerField(default=0)
    assignment_count = models.IntegerField(default=0)
    
    register_in_progress = models.IntegerField(default=0)
    register_completed = models.IntegerField(default=0)
    register_failed = models.IntegerField(default=0)
    register_expired = models.IntegerField(default=0)
    register_count = models.IntegerField(default=0)
    
    last_added = models.DateTimeField(null=True)
    due_date = models.DateTimeField(null=True) #OnBoard
    timeupdate = models.DateTimeField(auto_now=True, db_index=True)

    CODE_LIST = ['', 'assignment_', 'register_']
    FIELD_LIST = ['in_progress', 'completed', 'failed', 'expired', 'count']

    class Meta:
        ordering = ['-timeupdate']

    @staticmethod
    def pull(location_id, content_type_id, content_id):
        from .cached import cached_progress_content
        return cached_progress_content(location_id, content_type_id, content_id)

    @staticmethod
    def pull_summary(progress_content_list):
        from django.db.models import Sum
        sum_list = []
        for code in Content.CODE_LIST:
            for _ in Content.FIELD_LIST:
                sum_list.append(Sum('%s%s'%(code, _)))
        sum_list.append(Sum('standalone'))
        sum_list.append(Sum('included'))
        sum_list.append(Sum('learner'))
        progress_result = progress_content_list.aggregate(*sum_list)
        try:
            progress_result['register_percent'] = int(float(progress_result['register_completed__sum']) / float(progress_result['register_completed__sum'] + progress_result['register_failed__sum'] + progress_result['register_expired__sum']) * 100.0)
        except:
            progress_result['register_percent'] = 0
        try:
            progress_result['assignment_percent'] = int(float(progress_result['assignment_completed__sum']) / float(progress_result['assignment_completed__sum'] + progress_result['assignment_failed__sum'] + progress_result['assignment_expired__sum']) * 100.0)
        except:
            progress_result['assignment_percent'] = 0
        return progress_result

    @staticmethod
    def update_progress(location_id, content_type_id, content_id):
        import datetime
        from django.utils import timezone
        from django.conf import settings

        from order.models import Item as OrderItem
        from content.models import Location as ContentLocation
        
        from .cached import cached_progress_content_update

        progress_content = Content.pull(location_id, content_type_id, content_id)
        progress_content_all = Content.pull(None, content_type_id, content_id)
        content_location = ContentLocation.pull(location_id)
        if content_location and content_location.parent1_content_type_id is None:
            progress_content.is_root = True
        else:
            progress_content.is_root = False
        progress_content_all.is_root = False
        def _count(_p, code, status):
            if status == 1 or status == 5:
                _ = 'in_progress'
            elif status == 2:
                _ = 'completed'
            elif status == 3:
                _ = 'failed'
            elif status == 4:
                _ = 'expired'
            else:
                return None
            n = getattr(_p, '%s%s'%(code, _), 0)
            setattr(_p, '%s%s'%(code, _), n+1)

            n = getattr(_p, '%scount' % code, 0)
            setattr(_p, '%scount' % code , n+1)

            # Dashboard -> Question
            if status == 5:
                n = getattr(_p, '%sverifying' % code, 0)
                setattr(_p, '%sverifying' % code, n+1)

        for code in Content.CODE_LIST:
            for field in Content.FIELD_LIST:
                setattr(progress_content, '%s%s' % (code, field), 0)
                setattr(progress_content_all, '%s%s' % (code, field), 0)
                
        progress_content.verifying = 0
        progress_content.standalone = 0
        progress_content.included = 0

        progress_content_all.verifying = 0
        progress_content_all.standalone = 0
        progress_content_all.included = 0

        ACCOUNT_DICT = {}
        ACCOUNT_DICT_ALL = {}
        progress_list = Progress.objects.filter(
            content_type_id=content_type_id,
            content=content_id,
            item__status=2
        )
        if settings.ACCOUNT__HIDE_INACTIVE:
            progress_list = progress_list.exclude(account__is_active=False)
        if settings.ACCOUNT__HIDE_ID_LIST:
            progress_list = progress_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)
        for progress in progress_list:
            order_item = OrderItem.pull(progress.item_id)
            if progress.is_root:
                _count(progress_content, '', progress.status)
                if order_item.order.method == 21:
                    _count(progress_content, 'assignment_', progress.status)
                else:
                    _count(progress_content, 'register_', progress.status)
                ACCOUNT_DICT[progress.account_id] = True
                progress_content.standalone += 1
                progress_content_all.standalone += 1

            _count(progress_content_all, '', progress.status)
            if order_item.order.method == 21:
                _count(progress_content_all, 'assignment_', progress.status)
            else:
                _count(progress_content_all, 'register_', progress.status)
            ACCOUNT_DICT_ALL[progress.account_id] = True
            progress_content.included += 1
            progress_content_all.included += 1
                                
        progress_content.learner = len(ACCOUNT_DICT)
        progress_content.learner_all = len(ACCOUNT_DICT_ALL)
        
        progress_content.due_date = None
        for item in OrderItem.objects.filter(content_type_id=content_type_id, content=content_id):
            if item.expired != datetime.timedelta(0) and item.expired is not None and item.start is not None:
                _due_date = item.start + item.expired
                if progress_content.due_date is None:
                    progress_content.due_date = _due_date
                elif _due_date > timezone.now() and _due_date < progress_content.due_date:
                    progress_content.due_date = _due_date
            if item.timestamp is not None:
                if progress_content.last_added is None:
                    progress_content.last_added = item.timestamp
                elif progress_content.last_added < item.timestamp:
                    progress_content.last_added = item.timestamp

        progress_content.save()
        progress_content_all.save()
        
        cached_progress_content_update(progress_content)
        cached_progress_content_update(progress_content_all)
        return progress_content

    def get_percent(self):
        try:
            return int(float(self.completed) / float(self.completed + self.failed + self.expired) * 100.0)
        except:
            return 0

    def get_content(self):
        from utils.content import get_content
        self.content_object = get_content(self.content_type_id, self.content) # TODO : remove
        return get_content(self.content_type_id, self.content)

class Scorm(models.Model):
    material = models.ForeignKey('course.Material')
    total_lesson = models.IntegerField(default=0)

    @staticmethod
    def pull(material):
        scorm = Scorm.objects.filter(material=material).first()
        if scorm is None:
            scorm = Scorm.objects.create(material=material,
                                         total_lesson=1)
        return scorm
                                         
class ScormLesson(models.Model):
    import datetime
    
    scorm = models.ForeignKey(Scorm)
    location = models.CharField(max_length=120, db_index=True)
    duration = models.DurationField(default=datetime.timedelta(0))
    sort = models.IntegerField(default=0, db_index=True)

    def __str__(self):
        return self.location
    
    @staticmethod
    def pull(material, value):
        scorm = Scorm.pull(material)
        scorm_lesson = ScormLesson.objects.filter(scorm=scorm,
                                                  location=value) \
                                          .first()
        if scorm_lesson is None:
            scorm_lesson = ScormLesson.objects.create(scorm=scorm,
                                                      location=value,
                                                      sort=ScormLesson.objects.filter(scorm=scorm).count()+1)
            scorm.total_lesson = scorm_lesson.sort
            scorm.save()
        return scorm_lesson
        
class ScormLog(models.Model):
    from django.conf import settings
    import datetime

    STATUS_CHOICES = (
        (1, 'Start'),
        (2, 'Completed'),
        (3, 'Supend Data'),
    )
    
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    scorm = models.ForeignKey(Scorm, null=True)
    lesson = models.ForeignKey(ScormLesson, null=True)
    status = models.IntegerField(choices=STATUS_CHOICES)
    duration = models.DurationField(default=datetime.timedelta(0))
    data = models.CharField(max_length=255, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']

    @staticmethod
    def push(order_item, account, material, element, value):
        from django.utils import timezone

        from course.jobs import progress_scorm
        import django_rq
        
        if order_item.account_id != account.id:
            return
        elif material is None:
            return
        
        scorm = Scorm.pull(material)
        if element == 'cmi.core.lesson_location':
            scorm_lesson = ScormLesson.pull(material, value)
            ScormLog.objects.create(account=account,
                                    scorm=scorm,
                                    lesson=scorm_lesson,
                                    status=1)
        elif element == 'cmi.core.lesson_status':
            log_last = ScormLog.objects.filter(lesson__scorm=scorm,
                                               status=1) \
                                       .select_related('lesson') \
                                       .first()
            if log_last is not None and log_last.status == 3:
                return
            elif log_last is not None:
                duration = timezone.now() - log_last.timestamp
                ScormLog.objects.create(account=account,
                                        scorm=scorm,
                                        lesson_id=log_last.lesson_id,
                                        status=2,
                                        duration=duration)
            else:
                ScormLog.objects.create(account=account,
                                        scorm=scorm,
                                        lesson=None,
                                        status=2)
            django_rq.enqueue(progress_scorm, order_item.id, scorm.id)
        elif element == 'cmi.suspend_data':
            ScormLog.objects.create(account=account,
                                    scorm=scorm,
                                    lesson=None,
                                    status=3,
                                    data=value)
            django_rq.enqueue(progress_scorm, order_item.id, scorm.id)
                                    
