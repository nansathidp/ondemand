from django.contrib import admin

from .models import Progress, Content, VideoLog, Account, AccountContent, Scorm, ScormLesson, ScormLog


@admin.register(Progress)
class ProgressAdmin(admin.ModelAdmin):
    list_display = ('id', 'location_id', 'account', 'item', 'content_type', 'content', 'is_root', 'percent', 'score',
                    # 'content_complete', 'content_count',
                    # 'section_complete', 'section_count',
                    # 'material_complete', 'material_count',
                    'get_content', 'get_section', 'get_material',
                    'is_update', 'status',
                    'timeupdate')
    readonly_fields = ('account', 'item')
    list_filter = ('content_type', )
    
    def get_content(self, obj):
        return '%s/%s' % (obj.content_complete, obj.content_count)

    get_content.short_description = 'Content'

    def get_section(self, obj):
        return '%s/%s' % (obj.section_complete, obj.section_count)

    get_section.short_description = 'Section'

    def get_material(self, obj):
        return '%s/%s' % (obj.material_complete, obj.material_count)

    get_material.short_description = 'Material'


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('account', 'is_root', 'in_progress', 'completed', 'failed', 'expired', 'count', 'timeupdate')
    search_fields = ('account__email', )

@admin.register(AccountContent)
class AccountContentAdmin(admin.ModelAdmin):
    list_display = ('account', 'provider', 'course', 'program', 'timeupdate')
    readonly_fields = ('account', 'provider')


@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    list_display = ('id', 'location_id', 'is_root', 'content_type', 'content',
                    'in_progress', 'completed', 'failed', 'expired', 'verifying',
                    'count', 'learner', 'last_added', 'due_date', 'timeupdate')
    list_filter = ('content_type', )
    

@admin.register(VideoLog)
class VideoLogAdmin(admin.ModelAdmin):
    list_display = (
    'get_item', 'get_progress', 'get_account', 'get_material', 'action', 'duration', 'percent', 'timestamp',
    'is_fail', 'credit_use')
    readonly_fields = ['item', 'progress', 'account', 'material']

    def get_item(self, obj):
        return obj.item_id

    get_item.short_description = 'Item ID'

    def get_progress(self, obj):
        return obj.progress_id

    get_progress.short_description = 'Progress (Course)'

    def get_account(self, obj):
        return obj.account_id

    get_account.short_description = 'Account ID'

    def get_material(self, obj):
        return obj.material_id

    get_material.short_description = 'Material ID'

@admin.register(Scorm)
class ScormAdmin(admin.ModelAdmin):
    list_display = ('material', 'total_lesson')

@admin.register(ScormLesson)
class ScormLessonAdmin(admin.ModelAdmin):
    list_display = ('scorm', 'location', 'duration', 'sort')

@admin.register(ScormLog)
class ScormLogAdmin(admin.ModelAdmin):
    list_display = ('account', 'lesson', 'status', 'duration', 'data', 'timestamp')
    
