from django.conf import settings
from django.core.cache import cache

from .models import Progress, Account as ProgressAccount, Content as ProgressContent, VideoLog, AccountContent

from utils.cached.time_out import get_time_out
from utils.content import get_content

def _progress_create(order_item, content_location, content_type, content, count):
    if order_item.content_type_id == content_type.id:
        is_root = True
    else:
        is_root = False
    return Progress.objects.create(item=order_item,
                                   location=content_location,
                                   account_id=order_item.account_id,
                                   content_type=content_type,
                                   is_root=is_root,
                                   content=content,
                                   percent=0,
                                   content_count=count['content_count'],
                                   section_count=count['section_count'],
                                   material_count=count['material_count'])


def cached_progress(order_item, content_location, content_type, content_id, is_force=False):
    if content_location is None:
            return None

    key = '%s_progress_%s_%s_%s_%s' % (settings.CACHED_PREFIX, order_item.id, content_location.id, content_type.id, content_id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Progress.objects.filter(item=order_item,
                                         location=content_location,
                                         content_type=content_type,
                                         content=content_id) \
                                 .first()
        if result is None:
            content_object = get_content(content_type.id, content_id)
            if content_object is not None:
                count = content_object.progress_count()
                result = _progress_create(order_item, content_location, content_type, content_id, count)
                cache.set(key, result, get_time_out())
        else:
            cache.set(key, result, get_time_out())

    return None if result == -1 else result


def cached_progress_update(progress):
    key = '%s_progress_%s_%s_%s_%s' % (settings.CACHED_PREFIX, progress.item_id, progress.location_id, progress.content_type_id, progress.content)
    cache.set(key, progress, get_time_out())


def cached_progress_delete(progress):
    key = '%s_progress_%s_%s_%s_%s'%(settings.CACHED_PREFIX, progress.item_id, progress.location_id, progress.content_type_id, progress.content)
    cache.delete(key)


def cached_progress_account(account_id, is_root):
    if is_root:
        key = '%s_progress_account_%s_1'%(settings.CACHED_PREFIX, account_id)
    else:
        key = '%s_progress_account_%s_0'%(settings.CACHED_PREFIX, account_id)
    result = cache.get(key)
    if result is None:
        result = ProgressAccount.objects.filter(account_id=account_id,
                                                is_root=is_root).first()
        if result is None:
            result = ProgressAccount.objects.create(account_id=account_id,
                                                    is_root=is_root)
            cache.set(key, result, get_time_out())
            ProgressAccount._update_progress(account_id, is_root)
        else:
            cache.set(key, result, get_time_out())
    return result


def cached_progress_account_update(progress_account):
    if progress_account.is_root:
        key = '%s_progress_account_%s_1' % (settings.CACHED_PREFIX, progress_account.account_id)
    else:
        key = '%s_progress_account_%s_0' % (settings.CACHED_PREFIX, progress_account.account_id)
    cache.set(key, progress_account, get_time_out())


def cached_progress_account_content(account_id, provider):
    if provider is None:
        key = '%s_progress_account_content_%s' % (settings.CACHED_PREFIX, account_id)
    else:
        key = '%s_progress_account_content_%s_%s' % (settings.CACHED_PREFIX, account_id, provider.id)
    result = cache.get(key)
    if result is None:
        if provider is None:
            result = AccountContent.objects.filter(account_id=account_id, provider__isnull=True).first()
        else:
            result = AccountContent.objects.filter(account_id=account_id, provider=provider).first()
        if result is None:
            result = AccountContent.objects.create(account_id=account_id, provider=provider)
        result.update_count()
        cache.set(key, result, get_time_out())
    return result


def cached_progress_account_content_update(account_content):
    if account_content.provider is None:
        key = '%s_progress_account_content_%s' % (settings.CACHED_PREFIX, account_content.account_id)
    else:
        key = '%s_progress_account_content_%s_%s' % (settings.CACHED_PREFIX, account_content.account_id, account_content.provider_id)
    cache.set(key, account_content, get_time_out())


def cached_progress_content(location_id, content_type_id, content_id):
    if location_id is None:
        key = '%s_progress_content_%s_%s'%(settings.CACHED_PREFIX,
                                           content_type_id,
                                           content_id)
    else:
        key = '%s_progress_content_location_%s_%s_%s'%(settings.CACHED_PREFIX,
                                                       location_id,
                                                       content_type_id,
                                                       content_id)
        
    result = cache.get(key)
    if result is None:
        if location_id is None:
            result = ProgressContent.objects.filter(location__isnull=True,
                                                    content_type_id=content_type_id,
                                                    content=content_id) \
                                            .first()
            if result is None:
                result = ProgressContent.objects.create(location=None,
                                                        content_type_id=content_type_id,
                                                        content=content_id)
        else:
            result = ProgressContent.objects.filter(location_id=location_id,
                                                    content_type_id=content_type_id,
                                                    content=content_id) \
                                            .first()
            if result is None:
                result = ProgressContent.objects.create(location_id=location_id,
                                                        content_type_id=content_type_id,
                                                        content=content_id)

        cache.set(key, result, get_time_out())
    return result


def cached_progress_content_delete(progress_content):
    if progress_content.location_id is None:
        key = '%s_progress_content_%s_%s'%(settings.CACHED_PREFIX,
                                           progress_content.content_type_id,
                                           progress_content.content)
    else:
        key = '%s_progress_content_location_%s_%s_%s'%(settings.CACHED_PREFIX,
                                                       progress_content.location_id,
                                                       progress_content.content_type_id,
                                                       progress_content.content)
    cache.delete(key)

    
def cached_progress_content_update(progress_content):
    if progress_content.location_id is None:
        key = '%s_progress_content_%s_%s'%(settings.CACHED_PREFIX,
                                           progress_content.content_type_id,
                                           progress_content.content)
    else:
        key = '%s_progress_content_location_%s_%s_%s'%(settings.CACHED_PREFIX,
                                                       progress_content.location_id,
                                                       progress_content.content_type_id,
                                                       progress_content.content)
    cache.set(key, progress_content, get_time_out())


def cached_progress_videolog_last_v2(progress_course, material, is_force=False):
    key = '%s_progress_videolog_last_v2_%s_%s'%(settings.CACHED_PREFIX, progress_course.id, material.id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = VideoLog.objects.filter(progress=progress_course,
                                         material=material).order_by('-timestamp').first()
        if result is None:
            result = -1
        cache.set(key, result, get_time_out())
    return None if result == -1 else result

def cached_progress_videolog_last_update_v2(video_log):
    key = '%s_progress_videolog_last_v2_%s_%s'%(settings.CACHED_PREFIX,
                                                video_log.progress_id,
                                                video_log.material_id)
    cache.set(key, video_log, get_time_out())

def cached_progress_videolog_last_delete_v2(video_log):
    key = '%s_progress_videolog_last_v2_%s_%s'%(settings.CACHED_PREFIX,
                                                video_log.progress_id,
                                                video_log.material_id)
    cache.delete(key)
