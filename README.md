# README #

### What is this repository for? ###

* Quick summary
* Version 1.0

### How do I get set up develop environment ? ###

* sudo apt-get purge mysql-client-core-5.5
* sudo apt-get install mysql-server
* sudo apt-get install mysql-client
* sudo apt-get install python-mysqldb
* sudo apt-get install python-pip
* sudo apt-get install libmysqlclient-dev
* sudo apt-get install build-essential libssl-dev libffi-dev python-dev
* sudo apt-get install libsasl2-dev libldap2-dev libssl-dev
* sudo apt-get install redis-server
* sudo apt-get install git
* sudo apt-get install libxml2-dev libxmlsec1-dev

### How do I get set up? ###
* midir ~/env
* virtualenv -p /usr/bin/python3.5 ~/env/ondemand
* source ~/env/ondemand/bin/active
* pip install -r requirements.txt


### Template local_setting.py ###
import os, django
from django.conf import settings

DEBUG = True
PROJECT = 'project_name'
APP_THEME = 'project_theme'

ALLOWED_HOSTS = ['127.0.0.1']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'ondemand',
        'USER': 'root',
        'PASSWORD' : '',
        'OPTIONS': {
            'init_command': 'SET storage_engine=INNODB',
        }
    }
}

EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''

TEMPLATES[0]['APP_DIRS'] = True
TEMPLATES[0]['DIRS'] = []
TEMPLATES[0]['DIRS'].extend([
    os.path.join(BASE_DIR, 'templates', project),
    os.path.join(BASE_DIR, 'templates'),
])

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}
