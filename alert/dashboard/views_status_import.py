from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.conf import settings
from django.utils import timezone
from alert.models import Alert
import datetime

@csrf_exempt
def status_import_view(request, page, content=-1):
    code = ''
    if page == 'question':
        page = settings.CONTENT_TYPE('question', 'activity')
    elif page == 'assignment':
        page = settings.CONTENT_TYPE('assignment', 'assignment')
    elif page == 'account':
        page = settings.CONTENT_TYPE('account', 'account')
        content = -1
    elif page == 'department':
        page = None
        code = 'department'
    elif page == 'om':
        page = None
        code = 'om'
    else:
        return JsonResponse({})

    result = {}
    if request.method == 'POST':
        alert = Alert.objects.filter(
            code=code,
            page=page,
            content=content,
        ).first()
        if alert:
            if timezone.now() - alert.timestamp > datetime.timedelta(minutes=20):
                Alert.objects.filter(
                    code=code,
                    page=page,
                    status=1
                ).update(status=-1)
                result['id'] = -1
                result['status'] = 0
            else:
                result['id'] = alert.id
                result['status'] = alert.status
        else:
            result['id'] = -1
            result['status'] = 0
    return JsonResponse(result)
