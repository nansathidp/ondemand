from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse

from alert.models import Alert

@csrf_exempt
def status_view(request):
    result = {}
    if request.method == 'POST':
        alert = get_object_or_404(Alert, id=request.POST.get('alert_id', -1))
        result['status'] = alert.status
        if alert.status == 2:
            result['url'] = reverse('dashboard:alert:export', args=[alert.filename])
            result['html'] = """
<a href="%s">
  <button class="exportReportDashboard"><i class="zmdi zmdi-download zmdi-hc-fw"></i>Download</button>
</a>
"""%(result['url'])
            
    return JsonResponse(result)
