import os
import shutil

import django_rq
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from account.dashboard.jobs_om_cimb import om_cimb_job
from account.jobs import _import as account_import
from alert.models import Alert
from assignment.jobs import _import as assignment_import
from department.dashboard.jobs import import_department_job
from question.jobs import _import as question_import
from utils.resumable import Resumable
from django.core.exceptions import PermissionDenied

def _complete(resumable, alert, path):
    storage = FileSystemStorage(location=os.path.join(settings.BASE_DIR, 'alert', 'upload'))
    filename = '%s.dat' % alert.file_input
    if os.path.isfile('%s/%s' % (os.path.join(settings.BASE_DIR, 'alert', 'upload'), filename)):
        os.remove('%s/%s' % (os.path.join(settings.BASE_DIR, 'alert', 'upload'), filename))
    storage.save(filename, resumable)
    resumable.delete_chunks()
    shutil.rmtree(path)
    alert.file_input = filename
    alert.filename_input = alert.filename
    alert.status = 1
    alert.save()
    if alert.page_id == settings.CONTENT_TYPE('account', 'account').id:
        django_rq.enqueue(account_import, alert.id)
    elif alert.page_id == settings.CONTENT_TYPE('question', 'activity').id:
        django_rq.enqueue(question_import, alert.id)
    elif alert.page_id == settings.CONTENT_TYPE('assignment', 'assignment').id:
        django_rq.enqueue(assignment_import, alert.id)
    # elif alert.page_id == settings.CONTENT_TYPE('department', 'department').id:
    #     django_rq.enqueue(import_department_job, alert.id)
    elif alert.code == 'department':
        django_rq.enqueue(import_department_job, args=(alert.id,), timeout=600) # 10 Mins.
    elif alert.code == 'om':
        django_rq.enqueue(om_cimb_job, args=(alert.id,), timeout=1200)  # 20 Mins.


@csrf_exempt
def resumable_view(request, page, content=-1):
    code = ''
    if page == 'account':
        if settings.PROJECT == 'cimb':
            raise PermissionDenied
        page = settings.CONTENT_TYPE('account', 'account')
    elif page == 'question':
        page = settings.CONTENT_TYPE('question', 'activity')
    elif page == 'assignment':
        page = settings.CONTENT_TYPE('assignment', 'assignment')
    elif page == 'department':
        page = None
        code = 'department'
    elif page == 'om':
        page = None
        code = 'om'
    else:
        return HttpResponse()

    if request.method == 'POST':
        chunk = request.FILES.get('file')

        alert = Alert.pull_upload(
            request.user,
            code,
            page,
            content,
            request.POST.get('resumableFilename')
        )
        resumable = Resumable(
            alert.get_upload_path(),
            alert.file_input,
            request.POST
        )

        if resumable.chunk_exists:
            return HttpResponse('chunk already exists')
        resumable.process_chunk(chunk)
        if resumable.is_complete:
            _complete(resumable,
                      alert,
                      alert.get_upload_path())
        return HttpResponse()
    else:
        alert = Alert.pull_upload(
            request.user,
            code,
            page,
            content,
            request.GET.get('resumableFilename')
        )
        resumable = Resumable(
            alert.get_upload_path(),
            alert.file_input,
            request.GET
        )
        if not (resumable.chunk_exists or resumable.is_complete):
            return HttpResponse('chunk not found', status=404)
        else:
            if resumable.is_complete:
                _complete(resumable,
                          alert,
                          alert.get_upload_path())
            return HttpResponse('chunk already exists')
