from django.conf.urls import url

from .views_export import export_view
from .views_input import input_view
from .views_resumable import resumable_view
from .views_status import status_view
from .views_status_import import status_import_view

app_name = 'alert'
urlpatterns = [
    url(
        r'^resumable/(account|question|assignment|department|om)/$',
        resumable_view,
        name='resumable'
    ),
    url(
        r'^resumable/(account|question|assignment|department|om)/(\d+)/$',
        resumable_view,
        name='resumable'
    ),
    url(r'^status/$', status_view, name='status'),
    url(
        r'^status/import/(department|om)/$',
        status_import_view,
        name='status_import'
    ),
    url(
        r'^status/import/(account|question|assignment|om)/(\d+)/$',
        status_import_view,
        name='status_import'
    ),
    url(r'^input/(\d+)/$', input_view, name='input'),
    url(r'^export/(.*)', export_view, name='export'),
]
