import os
from wsgiref.util import FileWrapper
from django.shortcuts import redirect
from django.conf import settings
from django.http import HttpResponse

from alert.models import Alert


def export_view(request, filename):
    alert = Alert.objects.filter(
        # account=request.user,
        filename=filename,
    ).first()
    if alert is not None:
        if request.user.is_authenticated:
            if settings.ROLE_SUPER_ADMIN_ID != -1:
                if alert.account != request.user and not request.user.groups.filter(id=settings.ROLE_SUPER_ADMIN_ID).exists():
                    alert = None
        else:
            alert = None

    if alert is None:
        Alert.objects.filter(
            account=request.user,
            status__in=[0, 1]
        ).update(status=-1)
        return redirect('dashboard:home')

    path = os.path.join(settings.BASE_DIR, 'alert', 'export')
    _ = '%s/%s' % (path, alert.filename)
    wrapper = FileWrapper(open(_, 'rb'))
    response = HttpResponse(wrapper, content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=%s' % alert.get_user_filename()
    if alert.status == 2:
        alert.status = 3
        alert.save()
    return response
