import os
from wsgiref.util import FileWrapper
from django.shortcuts import redirect
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.http import HttpResponse

from alert.models import Alert


def input_view(request, alert_id):
    alert = get_object_or_404(Alert, id=alert_id)
    if alert is not None:
        if request.user.is_authenticated:
            if alert.account != request.user and not request.user.is_superuser:
                alert = None
        else:
            alert = None

    if alert is None:
        return redirect('dashboard:home')

    path = os.path.join(settings.BASE_DIR, 'alert', 'upload')
    _ = '%s/%s' % (path, alert.file_input)
    wrapper = FileWrapper(open(_, 'rb'))
    response = HttpResponse(wrapper, content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=%s' % alert.filename_input
    return response
