from django.db import models


class Alert(models.Model):
    STATUS_CHOICES = (
        (-4, 'Error'),
        (-3, 'File Error'),
        (-2, 'Result Delete'),
        (-1, 'Fail.'),
        (0, 'Upload'),
        (1, 'Process'),
        (2, 'Success'),
        (3, 'Download'),
    )

    account = models.ForeignKey('account.Account')
    code = models.CharField(max_length=12, db_index=True)
    page = models.ForeignKey('contenttypes.ContentType', null=True, blank=True)
    content = models.BigIntegerField(default=-1, db_index=True)
    json_kwargs = models.TextField(blank=True)
    json_result = models.TextField(blank=True)

    file_input = models.CharField(max_length=255, blank=True)
    filename_input = models.CharField(max_length=255, blank=True)

    filename = models.CharField(max_length=255, blank=True, db_index=True)
    file_output = models.CharField(max_length=255, blank=True)
    result = models.TextField(blank=True)

    status = models.IntegerField(choices=STATUS_CHOICES)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']

    @staticmethod
    def pull_upload(account, code, page, content, filename):
        from django.utils import timezone
        import uuid
        alert = Alert.objects.filter(
            account=account,
            code=code,
            page=page,
            content=content,
            filename=filename,
            status=0
        ).first()
        if alert is None:
            now = timezone.now()
            uuid = '%s-%s-%s' % (now.year, now.month, uuid.uuid4())
            alert = Alert.objects.create(
                account=account,
                code=code,
                page=page,
                content=content,
                file_input=uuid,
                filename=filename,
                status=0
            )
        return alert

    @staticmethod
    def pull_inprogress(account, content_type, content):
        from django.utils import timezone
        import datetime

        alert = Alert.objects.filter(
            account=account,
            page=content_type,
            status__in=[-1, 1, 2]
        ).first()
        if alert is not None and timezone.now() - alert.timestamp > datetime.timedelta(minutes=5):
            Alert.objects.filter(
                account=account,
                page=content_type,
                status__in=[1, 2]
            ).update(status=-1)
            alert = None
        return alert

    def get_upload_path(self):
        from django.conf import settings
        import os

        path = os.path.join(settings.BASE_DIR, 'alert', 'upload', self.file_input)
        if not os.path.isdir(path):
            os.makedirs(path)
        return path

    def get_export_path(self):
        from django.conf import settings
        import os

        path = os.path.join(settings.BASE_DIR, 'alert', 'export')
        if not os.path.isdir(path):
            os.makedirs(path)
        return path

    def get_user_filename(self):
        from django.conf import settings
        from django.utils import timezone

        def _perfix():
            if self.status == -3:
                return 'FILE_ERROR_LOG'
            elif self.status == -4:
                return 'ERROR_LOG'
            else:
                return 'LOG'

        current_tz = timezone.get_current_timezone()
        local = current_tz.normalize(self.timestamp.astimezone(current_tz))

        if self.page_id == settings.CONTENT_TYPE('progress', 'account').id:
            filename = 'LearnerProgress_'
        elif self.page_id == settings.CONTENT_TYPE('course', 'course').id:
            filename = 'CourseProgress_'
        elif self.page_id == settings.CONTENT_TYPE('question', 'activity').id:
            filename = 'TestProgress_'
        elif self.page_id == settings.CONTENT_TYPE('question', 'question').id:
            filename = 'TestDetailProgress_'
        elif self.page_id == settings.CONTENT_TYPE('program', 'program').id:
            filename = 'ProgramProgress_'
        elif self.page_id == settings.CONTENT_TYPE('program', 'onboard').id:
            filename = 'OnboardProgress_'
        elif self.code == 'department':
            filename = '%s_[%s]_%s.log' % (
                _perfix(),
                self.filename_input,
                local.strftime("%Y%m%d_%H%M%S")
            )
            return filename
        elif self.code == 'om':
            filename = '%s_[%s]_%s.log' % (
                _perfix(),
                self.filename_input,
                local.strftime("%Y%m%d_%H%M%S")
            )
            return filename
        else:
            filename = ''

        filename += local.strftime("%Y-%m-%d--%H-%M")
        return '%s.xls' % filename

    def get_json_result(self):
        _json_result = getattr(self, '_json_result', None)
        if _json_result:
            return _json_result
        else:
            import json
            try:
                _json_result = json.loads(self.json_result)
            except:
                _json_result = {
                    'error': 0,
                    'warning': 0
                }
            setattr(self, '_json_result', _json_result)
            return _json_result
