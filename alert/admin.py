from django.contrib import admin

from .models import Alert


@admin.register(Alert)
class AlertAdmin(admin.ModelAdmin):
    list_display = ('account', 'code', 'page', 'content', 'filename', 'status', 'timestamp')
