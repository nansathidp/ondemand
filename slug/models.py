from django.db import models

#Migrate Status

#course.Course
#institute.Institute (not)
#lesson.Category
#subject.Subject -> category.Category
#campaign.Campaign
#event.Event
#provider.Provider **

class Slug(models.Model):
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    content = models.BigIntegerField(db_index=True)

    slug = models.SlugField(max_length=120, db_index=True, allow_unicode=True)
