from django.shortcuts import render


def permission_403_view(request):
    return render(request,
                  'dashboard/403.html',
                  {})
