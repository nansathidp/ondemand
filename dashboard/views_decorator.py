from functools import wraps
from provider.models import Provider
from category.models import Category


def file_response(view_func):
    def _decorator(request, *args, **kwargs):
        response = view_func(request, *args, **kwargs)
        return response
    return wraps(view_func)(_decorator)


def check(view_func):
    def _decorator(request, *args, **kwargs):
        response = view_func(request, *args, **kwargs)
        return response
    return wraps(view_func)(_decorator)

"""
Not use:
- Content Provider can't not display other Provider?
"""


def filter_param(view_func):
    def _decorator(request, *args, **kwargs):
        request.q_name = request.GET.get('q_name', None)
        request.provider_list = Provider.pull_list()
        request.category_list = Category.pull_list()
        response = view_func(request, *args, **kwargs)
        return response
    return wraps(view_func)(_decorator)

