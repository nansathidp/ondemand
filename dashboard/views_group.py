from django.conf import settings
from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied
from django.shortcuts import render

PERMISSION_RULE_LIST = [
    # Dashboard
    {'app': 'dashboard',
     'model': 'dashboard',
     'title': 'Dashboard',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['access'],
     'dependency_list': {}},

    # Account Management
    {'app': 'account',
     'model': 'account',
     'title': 'Learner',
     'is_header': True,
     'header_title': 'Account Management',
     'content_type_id': None,
     'permission_list': ['view', 'view_org', 'add'],
     'dependency_list': {}},
    {'app': 'department',
     'model': 'department',
     'title': 'Organization',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['view', 'change'],
     'dependency_list': {'change': ['view']}},
    {'app': 'auth',
     'model': 'permission',
     'title': 'Admin Account',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['add', 'change', 'delete'],
     'dependency_list': {}},
    {'app': 'auth',
     'model': 'group',
     'title': 'Admin Authority',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['change'],
     'dependency_list': {}},
    {'app': 'provider',
     'model': 'provider',
     'title': 'Learning Center Account',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['view', 'view_own', 'add', 'change', 'delete'],
     'dependency_list': {'change': ['view', 'view_own'],
                         'delete': ['view', 'view_own']}},
]

if settings.IS_SUB_PROVIDER:
    PERMISSION_RULE_LIST.extend([
        {'app': 'subprovider',
         'model': 'subprovider',
         'title': 'Content SubProvider Account',
         'is_header': False,
         'header_title': '',
         'content_type_id': None,
         'permission_list': ['view', 'add', 'change', 'delete'],
         'dependency_list': {'change': ['view'],
                             'delete': ['view']}},
    ])

if settings.IS_CAREER_LEVEL:
    PERMISSION_RULE_LIST.extend([
        {'app': 'level',
         'model': 'level',
         'title': 'Career Level',
         'is_header': False,
         'header_title': '',
         'content_type_id': None,
         'permission_list': ['view', 'change'],
         'dependency_list': {'change': ['view']}},
    ])

PERMISSION_RULE_LIST.extend([

    # Content Management
    {'app': 'featured',
     'model': 'banner',
     'title': 'Banner',
     'is_header': True,
     'header_title': 'Content Management',
     'content_type_id': None,
     'permission_list': ['view', 'add', 'change', 'delete'],
     'dependency_list': {'change': ['view'],
                         'delete': ['view']}},
    {'app': 'featured',
     'model': 'announcement',
     'title': 'Announcements',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['view', 'add', 'change'],
     'dependency_list': {'change': ['view']}},

    # {'app': 'term',
    #  'model': 'term',
    #  'title': 'Terms & Conditions',
    #  'is_header': False,
    #  'header_title': '',
    #  'content_type_id': None,
    #  'permission_list': ['view', 'change'],
    #  'dependency_list': {'change': ['view']}},

    {'app': 'category',
     'model': 'category',
     'title': 'Content Category',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['view', 'add', 'change'],
     'dependency_list': {'change': ['view']}},

    # {'app': 'course',
    #  'model': 'courselevel',
    #  'title': 'Content Level',
    #  'is_header': False,
    #  'header_title': '',
    #  'content_type_id': None,
    #  'permission_list': ['view', 'add', 'change', 'delete'],
    #  'dependency_list': {'change': ['view'],
    #                      'delete': ['view']}},

    {'app': 'tutor',
     'model': 'tutor',
     'title': 'Instructor',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['view', 'view_own', 'add', 'change'],
     'dependency_list': {'change': ['view']}},

    # Learning Management
    {'app': 'course',
     'model': 'course',
     'title': 'Course Management',
     'is_header': True,
     'header_title': 'Learning Management',
     'content_type_id': None,
     'permission_list': ['view', 'view_own', 'add', 'change'],
     'dependency_list': {'add': ['view', 'view_own'],
                         'change': ['view', 'view_own']}},
    {'app': 'question',
     'model': 'activity',
     'title': 'Test Management',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['view', 'view_own', 'add', 'change'],
     'dependency_list': {'add': ['view', 'view_own'],
                         'change': ['view', 'view_own']}},
    {'app': 'program',
     'model': 'program',
     'title': 'Program Management',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['view', 'view_own', 'add', 'change'],
     'dependency_list': {'add': ['view', 'view_own'],
                         'change': ['view', 'view_own']}},
    {'app': 'task',
     'model': 'task',
     'title': 'TO-DO Management',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['view', 'view_own', 'view_org', 'add', 'change'],
     'dependency_list': {'add': ['view', 'view_own'],
                         'change': ['view', 'view_own', 'view_org']}},
    {'app': 'program',
     'model': 'onboard',
     'title': 'OnBoard Management',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['view', 'add', 'change'],
     'dependency_list': {'add': ['view'],
                         'change': ['view']}},
    # {'app': 'live',
    #  'model': 'live',
    #  'title': 'Live Management',
    #  'is_header': False,
    #  'header_title': '',
    #  'content_type_id': None,
    #  'permission_list': ['view', 'add', 'change'],
    #  'dependency_list': {'add': ['view'],
    #                      'change': ['view']}},

    # Assignment Management
    {'app': 'assignment',
     'model': 'assignment',
     'title': 'Assignment',
     'is_header': True,
     'header_title': 'Assignment Management',
     'content_type_id': None,
     'permission_list': ['view', 'view_own', 'view_org', 'add', 'delete'],
     'dependency_list': {'add': ['view', 'view_own', 'view_org']}},
    {'app': 'order',
     'model': 'order',
     'title': 'Assignment List & Registration List',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['view', 'view_own', 'view_org'],
     'dependency_list': {}},

    # {'app': 'notification',
    #  'model': 'message',
    #  'title': 'Alert & Notificaton',
    #  'is_header': False,
    #  'header_title': '',
    #  'content_type_id': None,
    #  'permission_list': ['view', 'change'],
    #  'dependency_list': {'change': ['view']}},
    
    {'app': 'inbox',
     'model': 'inbox',
     'title': 'Direct Message',
     'is_header': False,
     'header_title': '',
     'content_type_id': None,
     'permission_list': ['view', 'view_org', 'add'],
     'dependency_list': {'add': ['view', 'view_own', 'view_org']}},
    # Progress & Report
    {'app': 'progress',
     'model': 'progress',
     'title': 'Progress & Report',
     'is_header': True,
     'header_title': 'Progress & Report',
     'content_type_id': None,
     'permission_list': ['view', 'view_own', 'view_org'],
     'dependency_list': {}},
])

if not settings.IS_ORGANIZATION:
    for _ in PERMISSION_RULE_LIST:
        for p in _['permission_list']:
            if p == 'view_org':
                _['permission_list'].remove(p)


def _get_content_type_id():
    global PERMISSION_RULE_LIST
    for _ in PERMISSION_RULE_LIST:
        if settings.PROJECT == 'cimb' and _['app'] == 'account' and _['model'] == 'account':
            if 'add' in _['permission_list']:
                _['permission_list'].remove('add')
        try:
            _['content_type_id'] = settings.CONTENT_TYPE(_['app'], _['model']).id
        except:
            pass


def group_view(request):
    if not request.user.has_perm('auth.change_group',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    group_list = Group.objects.prefetch_related('permissions').all().order_by('group__sort')
    _get_content_type_id()

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Admin Authority'}
    ]
    return render(request,
                  'dashboard/group.html',
                  {'SIDEBAR': 'group',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'group_list': group_list,
                   'PERMISSION_RULE_LIST': PERMISSION_RULE_LIST})
