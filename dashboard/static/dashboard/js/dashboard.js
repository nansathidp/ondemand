
var options = {
    series: {
        shadowSize: 0,
        curvedLines: { //This is a third party plugin to make curved lines
            apply: false,
            active: true,
            monotonicFit: true
        },
        lines: {
            show: true,
            lineWidth: 0,
        },
        points: { show: true }
    },

    grid: {
        borderWidth: 0,
        labelMargin:10,
        hoverable: true,
        clickable: true,
        mouseActiveRadius:6,

    },
    xaxis: {
        tickDecimals: 0,
        ticks: true,
        show: true,
    },

    yaxis: {
        // tickDecimals: 0,
        show: true,
        minTickSize:3,
        ticks: true,
        // labelWidth: 25,
    },

    legend: {
        show: false
    }
};


function graph_active_user(data){
  if ($("#curved-line-chart-active-user")[0]) {
      $.plot($("#curved-line-chart-active-user"), [
          {data: data, lines: { show: true, fill: 0.98 }, label: 'Active User', stack: true, color: '#f1dd2c' }
      ], options);
  }
}

function graph_new_user(data){
  if ($("#curved-line-chart-new-user")[0]) {
      $.plot($("#curved-line-chart-new-user"), [
          {data: data, lines: { show: true, fill: 0.98 }, label: 'New User', stack: true, color: '#f1dd2c' }
      ], options);
  }
}
