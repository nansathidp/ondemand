from django.test import TestCase
from django.urls import reverse

from account.models import Account
from django.contrib.auth.models import Group


class DashboardViewTests(TestCase):

    def test_anonymous_home(self):
        response = self.client.get(reverse('dashboard:home'))
        self.assertRedirects(response, reverse('dashboard:login'))


class DashboardLoginViewTests(TestCase):

    def setUp(self):
        self.user = Account.objects.create_user('dashboard@test.com',
                                                '123456')
        group = Group.objects.create(name='Dashboard')
        self.user.groups.add(group)
                             
    def test_access(self):
        response = self.client.get(reverse('dashboard:login'))
        self.assertEqual(response.status_code, 200)

    def test_email(self):
        response = self.client.post(reverse('dashboard:login'),
                                    {})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['error'], None)
        
        response = self.client.post(reverse('dashboard:login'),
                                    {'email': ''})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['error'], None)

        response = self.client.post(reverse('dashboard:login'),
                                    {'email': 'dashboard@test.com'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['error'], None)

    def test_password(self):
        response = self.client.post(reverse('dashboard:login'),
                                    {'email': '',
                                     'password': ''})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['error'], None)

        response = self.client.post(reverse('dashboard:login'),
                                    {'email': 'dashboard@test.com'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['error'], None)

        response = self.client.post(reverse('dashboard:login'),
                                    {'email': 'dashboard@test.com',
                                     'password': ''})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['error'], None)

    def test_incorrect(self):
        response = self.client.post(reverse('dashboard:login'),
                                    {'email': 'dashboardx@test.com',
                                     'password': '123456'})
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.context['error'], None)

        response = self.client.post(reverse('dashboard:login'),
                                    {'email': 'dashboard@test.com',
                                     'password': '1234567'})
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.context['error'], None)

        response = self.client.post(reverse('dashboard:login'),
                                    {'email': 'dashboardx@test.com',
                                     'password': '1234567'})
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.context['error'], None)

    def test_correct(self):
        response = self.client.post(reverse('dashboard:login'),
                                    {'email': 'dashboard@test.com',
                                     'password': '123456'})
        self.assertEqual(response.status_code, 302)