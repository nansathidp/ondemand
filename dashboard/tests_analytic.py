import datetime

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone


class DashboardLoginViewTests(TestCase):

    def setUp(self):
        self.date_end = '2016-11-30'
        self.date_start = '2016-11-01'
                             
    def test_active_user_view(self):
        response = self.client.get('%s?start=%s&end=%s' % (reverse('dashboard:api_active_user'), self.date_start, self.date_end))
        self.assertEqual(response.status_code, 302)

    def test_city_view(self):
        response = self.client.get('%s?start=%s&end=%s' % (reverse('dashboard:api_country'), self.date_start, self.date_end))
        self.assertEqual(response.status_code, 302)

    def test_session_view(self):
        response = self.client.get('%s?start=%s&end=%s' % (reverse('dashboard:api_session'), self.date_start, self.date_end))
        self.assertEqual(response.status_code, 302)

    def test_os_view(self):
        response = self.client.get('%s?start=%s&end=%s' % (reverse('dashboard:api_os'), self.date_start, self.date_end))
        self.assertEqual(response.status_code, 302)
