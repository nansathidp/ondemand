from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import PermissionDenied
from django.conf import settings

from django.contrib.auth.models import Group, Permission

from .views_group import PERMISSION_RULE_LIST, _get_content_type_id


@csrf_exempt
def group_check_view(request):
    if not request.user.has_perm('auth.change_group',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    result = {}
    if request.method == 'POST':
        result['is_success'] = True
        result['html'] = ''
        result['is_reload'] = False
        group_id = request.POST.get('group', -1)
        content_type_id = request.POST.get('content_type', -1)
        model = request.POST.get('model', -1)
        permission = request.POST.get('permission', -1)
        try:
            group = Group.objects.get(id=group_id)
        except:
            result['is_success'] = False
        if result['is_success'] and content_type_id != -1:
            content_type = settings.CONTENT_TYPE_ID(content_type_id)
            if content_type is None:
                result['is_success'] = False
        else:
            result['is_success'] = False

        if result['is_success'] and model != -1:
            PERMISSION_RULE = None
            _get_content_type_id()
            for _ in PERMISSION_RULE_LIST:
                if _['content_type_id'] == content_type.id:
                    PERMISSION_RULE = _
            if PERMISSION_RULE is None:
                result['is_success'] = False
        else:
            result['is_success'] = False
        
        if result['is_success'] and permission != -1 and model != -1:
            try:
                permission_object = Permission.objects.get(content_type=content_type,
                                                           codename='%s_%s'%(permission, content_type.model))
            except:
                result['is_success'] = False
        else:
            result['is_success'] = False

        if result['is_success']:
            if group.permissions.filter(id=permission_object.id).exists():
                group.permissions.remove(permission_object)
                result['is_check'] = False
            else:
                if permission in PERMISSION_RULE['dependency_list']:
                    is_found = False
                    for _ in PERMISSION_RULE['dependency_list'][permission]:
                        if group.permissions.filter(content_type=content_type,
                                                    codename='%s_%s'%(_, content_type.model)).exists():
                            is_found = True
                    if not is_found:
                        result['is_success'] = False
                        result['is_check'] = True
                        result['html'] = 'Please select one permission: ' + ' or '.join(PERMISSION_RULE['dependency_list'][permission])
                    else:
                        group.permissions.add(permission_object)
                        result['is_check'] = True
                else:
                    group.permissions.add(permission_object)
                    result['is_check'] = True
                if result['is_check'] and permission.find('view') == 0:
                    codename = '%s_%s'%(permission, content_type.model)
                    if permission == 'view':
                        result['is_reload'] = True
                        group.permissions.remove(*Permission.objects.filter(content_type=content_type,
                                                                            codename__in=['view_own_%s'%content_type.model, 'view_org_%s'%content_type.model]))
                    elif permission == 'view_own':
                        result['is_reload'] = True
                        group.permissions.remove(*Permission.objects.filter(content_type=content_type,
                                                                            codename__in=['view_%s'%content_type.model, 'view_org_%s'%content_type.model]))
                    elif permission == 'view_org':
                        result['is_reload'] = True
                        group.permissions.remove(*Permission.objects.filter(content_type=content_type,
                                                                            codename__in=['view_%s'%content_type.model, 'view_own_%s'%content_type.model]))
                        
    return JsonResponse(result)
