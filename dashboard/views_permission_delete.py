from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from django.contrib.auth.models import Group

from account.models import Account
from department.models import Admin as DepartmentAdmin

from account.cached import cached_auth_permission_delete, cached_auth_group_list_delete


def permission_delete_view(request, group_id, account_id):
    if not request.user.has_perm('auth.delete_permission',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    group = get_object_or_404(Group, id=group_id)
    account = get_object_or_404(Account, id=account_id, is_active=True)
    account.groups.remove(group)
    # if group.id == 4:
    #     department_id = request.GET.get('department', None)
    #     if department_id is not None:
    #         DepartmentAdmin.objects.filter(department_id=department_id,
    #                                        account=account).delete()
    #         if DepartmentAdmin.objects.filter(account=account).count() == 0:
    #             account.groups.remove(group)
    # else:
    #     account.groups.remove(group)

    cached_auth_group_list_delete(account)
    cached_auth_permission_delete(account.id)
    cached_auth_permission_delete(account.id, group)
    return redirect('dashboard:permission')
