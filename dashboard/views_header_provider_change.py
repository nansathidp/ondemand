from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

from provider.models import Provider

@login_required
def header_provider_change_view(request, provider_id):
    provider = get_object_or_404(Provider,
                                 id=provider_id,
                                 account__account=request.user)
    request.session['dashboard_provider_id'] = provider.id
    request.session['dashboard_sub_provider_id'] = -1
    
    referer = request.META.get('HTTP_REFERER', None)
    if referer is None:
        return redirect('dashboard:home')
    else:
        return redirect(referer)
