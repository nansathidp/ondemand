from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied
from django.shortcuts import render
from django.conf import settings

from account.cached import cached_auth_group_list_delete, cached_auth_permission_delete
from account.models import Account
from department.models import Department, Admin as DepartmentAdmin


def permission_view(request):
    if not request.user.has_perm('auth.change_permission',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    group_list = Group.objects.all().order_by('id')
    for group in group_list:
        account_list = group.user_set.all()
        if settings.ACCOUNT__HIDE_INACTIVE:
            account_list = account_list.exclude(is_active=False)
        if settings.ACCOUNT__HIDE_ID_LIST:
            account_list = account_list.exclude(id__in=settings.ACCOUNT__HIDE_ID_LIST)
        group.account_list = account_list
    department_list = Department.objects.all()
    department_admin_list = DepartmentAdmin.objects.all()
    error = None
    if request.method == 'POST':
        if not request.user.has_perm('auth.add_permission', group=request.DASHBOARD_GROUP):
            raise PermissionDenied
        for group in group_list:
            email = request.POST.get('%s' % group.id, '')
            if len(email) > 0:
                account = Account.objects.filter(email=email.lower(), is_active=True).first()
                if account is not None and settings.ACCOUNT__HIDE_INACTIVE and not account.is_active:
                    account = None
                if account is not None and settings.ACCOUNT__HIDE_ID_LIST and account.id in settings.ACCOUNT__HIDE_ID_LIST:
                    account = None
                if account is not None:
                    if not account.groups.filter(id=group.id).exists():
                        account.groups.add(group)
                        cached_auth_group_list_delete(account)
                        cached_auth_permission_delete(account.id)
                        cached_auth_permission_delete(account.id, group)

                    # if group.id == 4:
                    #    department_id = request.POST.get('department', '-1')
                    #    if department_id != '-1':
                    #        department = get_object_or_404(Department, id=department_id)
                    #        account.groups.clear()
                    #        account.groups.add(group)
                    #        if not DepartmentAdmin.objects.filter(department=department, account=account).exists():
                    #            DepartmentAdmin.objects.create(department=department,
                    #                                           account=account)
                    # else:
                    #    account.groups.clear()
                    #    account.groups.add(group)

                    else:
                        error = 'Account duplicated.'
                else:
                    error = 'Account not found.'

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Admin Account'}
    ]
    return render(request,
                  'dashboard/permission.html',
                  {'SIDEBAR': 'permission',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'group_list': group_list,
                   'department_list': department_list,
                   'department_admin_list': department_admin_list,
                   'error': error})
