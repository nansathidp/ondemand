from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

from django.contrib.auth.models import Group

#from account.cached import cached_account

@login_required
def header_group_change_view(request, group_id):
    group = get_object_or_404(Group, id=group_id)
    request.session['dashboard_group_id'] = group.id
    #cached_account(request.user.id, is_force=True)
    
    referer = request.META.get('HTTP_REFERER', None)
    return redirect('dashboard:home')
    # if referer is None:
    #     return redirect('dashboard:home')
    # else:
    #     return redirect(referer)
