import datetime

from django.contrib.auth import logout
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect
from django.conf import settings

from account.models import Account
from analytic.models import Log
from course.models import Course
from program.models import Program
from question.models import Activity
from .views import filter_date


def dashboard_view(request):
    if request.DASHBOARD_GROUP is None:
        check_login_and_permission = request.user.has_perm('dashboard.access_dashboard')
        if not check_login_and_permission:
            logout(request)
            return redirect('dashboard:login')
    if not request.user.has_perm('dashboard.access_dashboard', group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    start, end = filter_date(request)

    offset = 30
    stat = {}
    value_list = []
    date_gte = datetime.datetime.today() - datetime.timedelta(days=offset)
    date_list = [(date_gte + datetime.timedelta(days=x)).strftime('%Y-%m-%d') for x in range(0, offset)]
    for date in date_list:
        stat[date] = 0

    try:
        log_list = Log.objects.filter(date__gte=date_gte).order_by('date')
        for log in log_list:
            date = log.date.strftime('%Y-%m-%d')
            stat[date] += log.value
    except:
        pass

    for date in date_list:
        value_list.append(stat[date])

    count_course = Course.objects.all().count()
    count_program = Program.objects.all().count()
    count_test = Activity.objects.all().count()

    account_list = Account.objects.all()
    if settings.ACCOUNT__HIDE_INACTIVE:
        account_list = account_list.exclude(is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        account_list = account_list.exclude(id__in=settings.ACCOUNT__HIDE_ID_LIST)
    count_account = account_list.count()
    return render(request,
                  'dashboard/dashboard.html',
                  {'SIDEBAR': 'home',
                   'start': start,
                   'end': end,
                   'date_list': date_list,
                   'value_list': value_list,
                   'count_course': count_course,
                   'count_program': count_program,
                   'count_test': count_test,
                   'count_account': count_account
                   })
