from django import template

register = template.Library()

@register.simple_tag
def group_check(role, permission):
    is_found = False
    for p in role.permissions.all():
        if p == permission:
            return 'text-green'
    else:
        return 'text-red'
    
@register.simple_tag
def group_check2(role, content_type_id, model, codename):
    is_found = False
    for p in role.permissions.all():
        if p.content_type_id == content_type_id and p.codename == '%s_%s'%(codename, model):
            return 'text-green'
    else:
        return 'text-red'

@register.simple_tag
def group_check3(role, content_type_id, model, codename):
    is_found = False
    for p in role.permissions.all():
        if p.content_type_id == content_type_id and p.codename == '%s_%s'%(codename, model):
            return 'checked'
    else:
        return ''
