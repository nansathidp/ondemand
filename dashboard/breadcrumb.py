from django.core.urlresolvers import reverse, get_resolver
import inspect

ITEM_LIST = {
    'course.dashboard.views.home_view': {'url': 'dashboard:course:home',
                                         'title': 'Course Management'},
    'course.dashboard.views_create.create_view': {'url': None,
                                                  'title': 'Course Create'},
}

STACK_LIST = {
    'course.dashboard.views.home_view': ['dashboard:course:home'],
    'course.dashboard.views_create.create_view': ['dashboard:course:home', 'dashboard:course:create'],
}

def gen(item, is_active):
    path = reverse(item)
    try:
        title = get_resolver(None).resolve(path).kwargs['title']
    except:
        title = item.split(':')[-1]
        
    result = {'is_active': is_active,
              'title': title}
    
    if not is_active:
        try:
            result['url'] = path
        except:
            result['url'] = None
    return result

def breadcrumb(request):
    call = inspect.stack()[1]
    path = call[1]
    path = path.split('/')[-3:]
    path[-1] = path[-1].replace('.py', '')
    view = call[3]

    code = '%s.%s'%('.'.join(path), view)
    result_list = []
    if code in STACK_LIST:
        count = 0
        for item in STACK_LIST[code]:
            count += 1
            if len(STACK_LIST[code]) != count:
                result_list.append(gen(item, False))
            else:
                result_list.append(gen(item, True))
    return result_list
    
