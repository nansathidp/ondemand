from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def condition_view(request):
    return render(request,
                  'dashboard/condition.html',
                  {})


def config_view(request):
    return render(request,
                  'dashboard/config.html',
                  {})


def paginator(request, content_list):
    # Copy From https://docs.djangoproject.com/en/1.9/topics/pagination/#using-paginator-in-a-view
    paginator = Paginator(content_list, 30)
    if request.method == 'GET':
        page = request.GET.get('page')
    elif request.method == 'POST':
        page = request.POST.get('page')

    try:
        contacts = paginator.page(page)
    except PageNotAnInteger:
        contacts = paginator.page(1)
    except EmptyPage:
        contacts = paginator.page(paginator.num_pages)

    return contacts


def filter_date(request):
    from django.utils import timezone
    import datetime
    date_start = None
    date_end = None

    try:
        start = request.GET.get('start', None).split('/')
        end = request.GET.get('end', None).split('/')
        date_start = timezone.make_aware(datetime.datetime(int(start[2]), int(start[1]), int(start[0]), 0, 0, 0)).date()
        date_end = timezone.make_aware(datetime.datetime(int(end[2]), int(end[1]), int(end[0]), 23, 59, 59)).date()
    except:
        date_start = None
        date_end = None

    if date_start is None or date_end is None:
        date_end = timezone.now().date()
        date_start = date_end - datetime.timedelta(days=30)
    return date_start, date_end
