from django.shortcuts import render
from django.core.exceptions import PermissionDenied
from django.conf import settings

from alert.models import Alert

def import_view(request):
    if not request.user.is_authenticated:
        raise PermissionDenied
    elif settings.ROLE_SUPER_ADMIN_ID != -1:
        if not request.user.groups.filter(id=settings.ROLE_SUPER_ADMIN_ID).exists():
            raise PermissionDenied
    elif not request.user.is_superuser:
        raise PermissionDenied
    
    alert_list = Alert.objects.filter(
        code__in=['department', 'om']
    ).select_related('account')
    return render(request,
                  'dashboard/import.html',
                  {'alert_list': alert_list})
