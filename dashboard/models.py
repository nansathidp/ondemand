from django.db import models
from django.conf import settings

# REPORT_DAY_TYPE = (
#     (0, 'Total sell'),
#     (1, 'Take Course Free'), #not use?
#     (2, 'Course Sell'),
#     (3, 'Package Sell'),
#     (4, 'Coin Sell'),
#
#     (10, 'Duration All Course'),
#     (11, 'Duration Free Course'),
#     (12, 'Duration Paid Course'),
#
#     (20, 'Course Views'),
#     (21, 'Course Add to Cart'),
#     (22, 'Course Buy'),
#     (23, 'Course Add to Libary'),
#
#     (30, 'Package Views'),
#     (31, 'Package Add to Cart'),
#     (32, 'Package Buy'),
#
#     (101, 'Learners Free'),
#     (102, 'Learners Paid'),
# )


class Dashboard(models.Model):
    
    class Meta:
        default_permissions = ('access', 'add', 'change', 'delete')

#
# class Config(models.Model):
#     institute = models.ForeignKey('institute.Institute')
#     start = models.DateField()

# class ReportDay(models.Model):
#     institute = models.ForeignKey('institute.Institute')
#     date = models.DateField(db_index=True)
#     type = models.IntegerField(choices=REPORT_DAY_TYPE)
#     value = models.FloatField()
#     last_update = models.DateTimeField(auto_now=True)
#
#     class Meta:
#         """
#         Don't Change.
#         It Effective chart order.
#         """
#         ordering = ['date']
#
#     @staticmethod
#     def push(institute, date, type, value):
#         report_day = ReportDay.objects.filter(institute=institute,
#                                               date=date,
#                                               type=type).first()
#         if report_day is None:
#             report_day = ReportDay.objects.create(institute=institute,
#                                                   date=date,
#                                                   type=type,
#                                                   value=value)
#         else:
#             if report_day.value != value:
#                 report_day.value = value
#                 report_day.save(update_fields=['value'])
#
#     @staticmethod
#     def pull(institute, type_list, start, end):
#         import time
#         result = {}
#         for type in type_list:
#             result[type] = []
#         for report_day in ReportDay.objects.filter(institute=institute,
#                                                    date__range=(start, end),
#                                                    type__in=type_list):
#             result[report_day.type].append({'time': int(time.mktime(report_day.date.timetuple())) * 1000,
#                                             'value': report_day.value})
#         return result
#
#     @staticmethod
#     def pull_sum(institute, type, start, end):
#         from django.db.models import Sum
#         return ReportDay.objects.filter(institute=institute,
#                                         date__range=(start, end),
#                                         type=type).aggregate(Sum('value'))['value__sum']


# class PaidUser(models.Model):
#     institute = models.ForeignKey('institute.Institute')
#     date = models.DateField(db_index=True)
#     account_id = models.BigIntegerField()


# class Learner(models.Model):
#     TYPE = (
#         (1, 'Free'),
#         (2, 'Paid')
#     )
#
#     institute = models.ForeignKey('institute.Institute')
#     date = models.DateField(db_index=True)
#     account_id = models.BigIntegerField()
#     type = models.IntegerField(choices=TYPE)


# class BestSeller(models.Model):
#     institute = models.ForeignKey('institute.Institute')
#     content_type = models.ForeignKey('contenttypes.ContentType')
#     content = models.BigIntegerField(db_index=True)
#     date = models.DateField(db_index=True)
#     net = models.FloatField()


# class TopView(models.Model):
#     institute = models.ForeignKey('institute.Institute')
#     content_type = models.ForeignKey('contenttypes.ContentType')
#     content = models.BigIntegerField(db_index=True)
#     date = models.DateField(db_index=True)
#     value = models.IntegerField()


class Group(models.Model):
    parent = models.OneToOneField('auth.Group')
    sort = models.IntegerField(default=1, db_index=True)
    
#class Permission(models.Model):
#    name = models.CharField(max_length=120)
#    code = models.CharField(max_length=60, db_index=True)
#    sort = models.IntegerField()
#    
#    class Meta:
#        ordering = ['sort']

#class Role(models.Model):
#    name = models.CharField(max_length=120)
#    accounts = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True)
#    permissions = models.ManyToManyField(Permission, blank=True)
#    sort = models.IntegerField()
#    
#    class Meta:
#        ordering = ['sort']

# class Own(models.Model):
#     account = models.ForeignKey(settings.AUTH_USER_MODEL)
#     content_type = models.ForeignKey('contenttypes.ContentType')
#     content = models.BigIntegerField(db_index=True)


# class Log(models.Model):
#     LOG_TYPE = (
#         (801, 'POS buy Course Success.'),
#         (802, 'POS buy Course Fail.'),
#         (811, 'POS buy Package Success.'),
#         (812, 'POS buy Package Fail.'),
#         (821, 'POS buy Coin Success.'),
#         (822, 'POS buy Coin Fail.'),
#     )
#
#     institute_id = models.BigIntegerField(db_index=True)
#     admin_id = models.BigIntegerField()
#     admin_email = models.CharField(max_length=512)
#     admin_display = models.CharField(max_length=120)
#     account_id = models.BigIntegerField()
#     account_email = models.CharField(max_length=512, blank=True)
#     account_display = models.CharField(max_length=120, blank=True)
#     type = models.IntegerField(choices=LOG_TYPE)
#     note = models.TextField(blank=True)
#     timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
#
#     class Meta:
#         ordering = ['-timestamp']
#
#     @staticmethod
#     def push(institute_id, admin_id, admin_email, admin_display, account_id, account_email, account_display, type, note):
#         Log.objects.create(institute_id=institute_id,
#                            admin_id=admin_id,
#                            admin_email=admin_email,
#                            admin_display=admin_display,
#                            account_id=account_id,
#                            account_email=account_email,
#                            account_display=account_display,
#                            type=type,
#                            note=note)
