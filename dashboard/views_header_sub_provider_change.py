from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

from subprovider.models import SubProvider


@login_required
def header_sub_provider_change_view(request, sub_provider_id):
    sub_provider = get_object_or_404(SubProvider,
                                     id=sub_provider_id,
                                     account__account=request.user)
    request.session['dashboard_provider_id'] = -1
    request.session['dashboard_sub_provider_id'] = sub_provider.id

    ref = request.META.get('HTTP_REFERER', None)
    if ref is None:
        return redirect('dashboard:home')
    else:
        return redirect(ref)
