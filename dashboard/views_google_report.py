from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.core.exceptions import PermissionDenied

from .views import filter_date
from .views_api_analytic import active_user_view, session_view, city_view, device_view, os_view

import xlwt


herder_style = xlwt.easyxf('font: bold on, height 450;')
bold_style = xlwt.easyxf('font: bold on')


def _active_user_report(request, start, end, wb):
    result_active_user_view = active_user_view(request, start=start, end=end, is_json=False)
    try:
        web_data_list = result_active_user_view['web']['reports'][0]['data']['rows']
    except:
        web_data_list = []
    try:
        mobile_data_list = result_active_user_view['mobile']['reports'][0]['data']['rows']
    except:
        mobile_data_list = []

    ws = wb.add_sheet('Active User')
    ws.row(0).height_mismatch = True
    ws.row(0).height = 256 * 3
    ws.col(0).width = 256 * 20
    ws.col(1).width = 256 * 20
    ws.col(2).width = 256 * 20
    ws.col(3).width = 256 * 20

    ws.write(0, 0, 'Active User', herder_style)
    ws.write(1, 0, 'Date', bold_style)
    ws.write(1, 1, 'Session Web', bold_style)
    ws.write(1, 2, 'Session Mobile', bold_style)
    ws.write(1, 3, 'Total Session', bold_style)

    start_row = 2
    map_dict = {}
    for data in mobile_data_list:
        date = data['dimensions'][0]
        value = data['metrics'][0]['values'][0]
        if date in map_dict:
            map_dict[date].update({
                'session_mobile': value
            })
        else:
            map_dict[date] = {
                'session_web': 0,
                'session_mobile': value
            }

    for data in web_data_list:
        date = data['dimensions'][0]
        value = data['metrics'][0]['values'][0]
        if date in map_dict:
            map_dict[date].update({
                'session_web': value
            })
        else:
            map_dict[date] = {
                'session_web': value,
                'session_mobile': 0
            }

    for key, value in sorted(map_dict.items()):
        ws.write(start_row, 0, key)
        ws.write(start_row, 1, int(value['session_web']))
        ws.write(start_row, 2, int(value['session_mobile']))
        ws.write(start_row, 3, int(value['session_web']) + int(value['session_mobile']))
        start_row += 1


def _usage_report(request, start, end, wb):
    result_usage_view = session_view(request, start=start, end=end, is_json=False)
    header_data_list = result_usage_view['web']['reports'][0]['columnHeader']['metricHeader']['metricHeaderEntries']
    try:
        web_data_list = result_usage_view['web']['reports'][0]['data']['rows']
    except:
        web_data_list = []

    try:
        mobile_data_list = result_usage_view['mobile']['reports'][0]['data']['rows']
    except:
        mobile_data_list = []
    ws = wb.add_sheet(' Usage Statistics')
    ws.row(0).height_mismatch = True
    ws.row(0).height = 256 * 3
    ws.col(0).width = 256 * 20
    ws.col(1).width = 256 * 20
    ws.col(2).width = 256 * 20
    ws.col(3).width = 256 * 20
    ws.write(0, 0, 'Usage Statistics', herder_style)
    ws.write(1, 0, 'Usage Statistics', bold_style)
    ws.write(1, 1, 'Web Statistics', bold_style)
    ws.write(1, 2, 'Mobile Statistics', bold_style)
    ws.write(1, 3, 'Total Statistics', bold_style)
    start_row = 2
    index = 0

    style = xlwt.easyxf('alignment: horiz centre;')

    for data in header_data_list:
        ws.write(start_row, 0, data['name'])

        try:
            value_web = 0
            value_web = web_data_list[0]['metrics'][0]['values'][index]
            if 'Duration' in data['name']:
                m, s = divmod(float(value_web), 60)
                h, m = divmod(m, 60)
                _vw = "%d:%02d:%02d" % (h, m, s)
            else:
                _vw = round(float(value_web), 1)
            ws.write(start_row, 1, _vw, style)
        except:
            ws.write(start_row, 1, '-', style)

        try:
            value_web = 0
            value_mobile = mobile_data_list[0]['metrics'][0]['values'][index]
            if 'Duration' in data['name']:
                m, s = divmod(float(value_mobile), 60)
                h, m = divmod(m, 60)
                _vm = "%d:%02d:%02d" % (h, m, s)
            else:
                _vm = round(float(value_mobile), 1)
            ws.write(start_row, 2, _vm, style)
        except:
            ws.write(start_row, 2, '-', style)

        try:
            value_total = float(value_web) + float(value_mobile)
            if 'Duration' in data['name']:
                m, s = divmod(float(value_total), 60)
                h, m = divmod(m, 60)
                _vt = "%d:%02d:%02d" % (h, m, s)
            else:
                _vt = round(float(value_total), 1)
            ws.write(start_row, 3, _vt, style)
        except:
            ws.write(start_row, 3, '-', style)
        start_row += 1
        index += 1


def _os_report(request, start, end, wb):
    result_os_view = os_view(request, start=start, end=end, is_json=False)
    data_list = result_os_view['reports'][0]['data']['rows']
    ws = wb.add_sheet('Usage Operation Systems')
    ws.row(0).height_mismatch = True
    ws.row(0).height = 256 * 3
    ws.col(0).width = 256 * 20
    ws.col(1).width = 256 * 20
    ws.write(0, 0, 'Usage Operation Systems', herder_style)
    ws.write(1, 0, 'Operation Systems', bold_style)
    ws.write(1, 1, 'Session', bold_style)
    start_row = 2
    for data in data_list:
        ws.write(start_row, 0, data['dimensions'][0])
        ws.write(start_row, 1, data['metrics'][0]['values'][0])
        start_row += 1


def _location_report(request, start, end, wb):
    result_location_view = city_view(request, start=start, end=end, is_json=False)
    data_list = result_location_view['reports'][0]['data']['rows']
    ws = wb.add_sheet('Usage locations')
    ws.row(0).height_mismatch = True
    ws.row(0).height = 256 * 3
    ws.col(0).width = 256 * 20
    ws.col(1).width = 256 * 20
    ws.write(0, 0, 'Usage locations', herder_style)
    ws.write(1, 0, 'Usage locations', bold_style)
    ws.write(1, 1, 'Session', bold_style)
    start_row = 2
    for data in data_list:
        ws.write(start_row, 0, data['dimensions'][0])
        ws.write(start_row, 1, data['metrics'][0]['values'][0])
        start_row += 1


def _device_report(request, start, end, wb):
    result_device_view = device_view(request, start=start, end=end, is_json=False)
    data_list = result_device_view['reports'][0]['data']['rows']
    ws = wb.add_sheet('Usage device')
    ws.row(0).height_mismatch = True
    ws.row(0).height = 256 * 3
    ws.col(0).width = 256 * 20
    ws.col(1).width = 256 * 20
    ws.write(0, 0, 'Usage device', herder_style)
    ws.write(1, 0, 'Usage device', bold_style)
    ws.write(1, 1, 'Session', bold_style)
    start_row = 2
    for data in data_list:
        ws.write(start_row, 0, data['dimensions'][0])
        ws.write(start_row, 1, data['metrics'][0]['values'][0])
        start_row += 1


def report_view(request):
    if not request.user.has_perm('dashboard.access_dashboard', group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    start, end = filter_date(request)

    if start is None or end is None:
        return HttpResponseBadRequest('Require Start - End')

    file_name = 'Dashboard Report %s - %s.xls' % (start.strftime('%m/%d/%Y'), end.strftime('%m/%d/%Y'))
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=%s' % file_name

    start = start.strftime('%Y-%m-%d')
    end = end.strftime('%Y-%m-%d')

    wb = xlwt.Workbook()
    _active_user_report(request, start, end, wb)
    _usage_report(request, start, end, wb)
    _os_report(request, start, end, wb)
    _location_report(request, start, end, wb)
    _device_report(request, start, end, wb)
    wb.save(response)
    return response
