from django.shortcuts import render
from django.core.exceptions import PermissionDenied

from .views import filter_date


def dashboard_view(request):
    if not request.user.has_perm('dashboard.access_dashboard', group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    start, end = filter_date(request)
    return render(request,
                  'dashboard/home.html',
                  {'SIDEBAR': 'home',
                   'start': start,
                   'end': end})
