from axes.decorators import watch_login
from django.conf import settings
from django.conf.urls import url, include
from django.utils.module_loading import import_string

from .views_403 import permission_403_view
from .views_500 import try_500_view
from .views_api_analytic import active_user_view, city_view, session_view, os_view, device_view
from .views_google_report import report_view
from .views_group import group_view
from .views_group_check import group_check_view
from .views_header_group_change import header_group_change_view
from .views_header_provider_change import header_provider_change_view
from .views_import import import_view
from .views_login import login_view
from .views_logout import logout_view
from .views_permission import permission_view
from .views_permission_delete import permission_delete_view

app_name = 'dashboard'
urlpatterns = [
    url(r'^$', import_string(settings.VIEW_DASHBOARD), name='home'),  # TODO: testing
    url(r'^report/$', report_view, name='report'),  # TODO: testing
    # url(r'^$', dashboard_view, name='home'),  # TODO: testing

    url(r'^login/$', watch_login(login_view), name='login'),  # TODO: testing
    url(r'^logout/$', logout_view, name='logout'),  # TODO: testing
    url(r'^403/$', permission_403_view, name='403'),  # TODO: testing
    url(r'^500/$', try_500_view),

    url(r'^header/group/(\d+)/change/$', header_group_change_view, name='header_group_change'),  # TODO: testing
    url(r'^header/provider/(\d+)/change/$', header_provider_change_view, name='header_provider_change'),

    url(r'^import/$', import_view, name='import'),

    url(r'^alert/', include('alert.dashboard.urls')),

    # Account Management
    url(r'^learner/', include('account.dashboard.urls')),  # TODO: testing
    url(r'^department/', include('department.dashboard.urls')),  # TODO: testing
    url(r'^provider/', include('provider.dashboard.urls')),

    url(r'^permission/$', permission_view, name='permission'),  # TODO: testing
    url(r'^permission/(\d+)/delete/(\d+)/$', permission_delete_view, name='permission_delete'),  # TODO: testing

    url(r'^role/$', group_view, name='group'),  # TODO: testing
    url(r'^role/check/$', group_check_view, name='group_check'),  # TODO: testing

    url(r'^level/', include('level.dashboard.urls')),  # TODO: testing

    # Login Report
    url(r'^active/', include('active.dashboard.urls')),

    # Content Management
    url(r'^featured/', include('featured.dashboard.urls')),
    url(r'^category/', include('category.dashboard.urls')),

    # url(r'^featured/', include('featured.dashboard.urls')), #TODO: testing
    url(r'^instructor/', include('tutor.dashboard.urls')),  # TODO: testing
    url(r'^term/', include('term.dashboard.urls')),  # TODO: testing

    # Learning Management
    url(r'^course/', include('course.dashboard.urls')),
    url(r'^question/', include('question.dashboard.urls')),  # TODO: testing
    url(r'^program/', include('program.dashboard.urls')),  # TODO: testing
    url(r'^to-do/', include('task.dashboard.urls')),  # TODO: testing
    url(r'^onboard/', include('program.dashboard.onboard.urls')),  # TODO: testing
    url(r'^lesson/', include('lesson.dashboard.urls')),  # TODO: testing
    url(r'^live/', include('live.dashboard.urls')),  # TODO: testing

    # Assignment Management
    url(r'^assignment/', include('assignment.dashboard.urls')),  # TODO: testing
    url(r'^assignment-auto/', include('assignment_auto.dashboard.urls')),  # TODO: testing
    url(r'^order/', include('order.dashboard.urls')),  # TODO: testing
    url(r'^notification/', include('notification.dashboard.urls')),  # TODO: testing
    url(r'^inbox/', include('inbox.dashboard.urls')),  # TODO: testing

    # Progress & Report
    url(r'^progress/', include('progress.dashboard.urls')),  # TODO: testing

    # Log
    url(r'^log/', include('log.dashboard.urls')),

    # Other
    url(r'^coin/', include('coin.dashboard.urls')),  # TODO: testing
    url(r'^access/', include('access.dashboard.urls')),  # TODO: testing
    url(r'^dependency/', include('dependency.dashboard.urls')),  # TODO: testing
    url(r'^ads/', include('ads.dashboard.urls')),  # TODO: testing

    # JSON
    url(r'^api/active-user/$', active_user_view, name='api_active_user'),  # TODO: testing
    url(r'^api/country/$', city_view, name='api_country'),  # TODO: testing
    url(r'^api/session/$', session_view, name='api_session'),  # TODO: testing
    url(r'^api/os/$', os_view, name='api_os'),  # TODO: testing
    url(r'^api/device/$', device_view, name='api_device'),  # TODO: testing
]

if settings.IS_SUB_PROVIDER:
    from .views_header_sub_provider_change import header_sub_provider_change_view

    urlpatterns += [
        url(r'^header/sub-provider/(\d+)/change/$', header_sub_provider_change_view, name='header_sub_provider_change'),
        # TODO: testing
        url(r'^sub-provider/', include('subprovider.dashboard.urls')),  # TODO: testing
    ]
