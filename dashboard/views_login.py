from django.conf import settings
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect, HttpResponsePermanentRedirect
from account.login_policy import LoginPolicy
from account.cached import cached_account, cached_auth_permission, cached_auth_group_list_delete, \
    cached_auth_group_delete
from provider.cached import cached_provider_account_delete

if settings.IS_SUB_PROVIDER:
    from subprovider.cached import cached_sub_provider_account_delete


def login_view(request):
    if request.user.is_authenticated():
        return redirect('dashboard:home')
    error = None
    if request.method == 'POST':
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        if email is not None and password is not None and \
                        len(email) > 0 and len(password) > 0:
            account = authenticate(email=email, password=password)
            if account is None:
                error = 'The username or password you entered is incorrect.'
            else:
                # if not account.groups.exists() and not account.is_superuser: #Not use is_superuser in Dashboard
                if not account.groups.exists():
                    error = 'No permission.'
                else:
                    login_policy = LoginPolicy(account=account, request=request)
                    if login_policy.is_validate():
                        login_policy.login()
                        # login(request, account)
                        account = cached_account(account.id, is_force=True)
                        cached_auth_group_list_delete(account)
                        cached_auth_permission(account.id, None, is_force=True)
                        for group in account.groups.all():
                            cached_auth_group_delete(group.id)
                            cached_auth_permission(account.id, group, is_force=True)
                        cached_provider_account_delete(account.id)
                        request.DASHBOARD_PROVIDER = None
                        request.DASHBOARD_SUB_PROVIDER = None
                        if settings.IS_SUB_PROVIDER:
                            cached_sub_provider_account_delete(account.id)
                        return redirect('dashboard:home')
                    else:
                        result = login_policy.get_result()
                        link = result['result']['link']
                        if link:
                            return HttpResponsePermanentRedirect(settings.SITE_URL+link)
                        else:
                            error = result['status_msg']
                            return render(request,
                                          'dashboard/login.html',
                                          {'error': error})

    return render(request,
                  'dashboard/login.html',
                  {'error': error})
