from django.conf import settings
from django.contrib.auth.models import Permission
from django.core.cache import cache

from utils.cached.time_out import get_time_out, get_time_out_hour


def cached_dashboard_permission(account, is_force=False):
    key = '%s_dashboard_permission_%s' % (settings.CACHED_PREFIX, account.id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Permission.objects.filter(role__accounts=account).distinct()
        cache.set(key, result, get_time_out_hour())
    return result


def cached_dashboard_permission_delete(account):
    key = '%s_dashboard_permission_%s' % (settings.CACHED_PREFIX, account.id)
    cache.delete(key)
