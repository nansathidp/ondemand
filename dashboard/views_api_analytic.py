from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from api.views_api import json_render

from .views_google_oauth_cert import initialize_analytic, get_device_report, get_city_report, get_session_by_time
from .views_google_oauth_cert import get_session_report, get_os_report


WEB = 'web'
MOBILE = 'mobile'


@csrf_exempt
def active_user_view(request, start=None, end=None, is_json=True):
    result = {}
    if start is None:
        start = request.GET.get('start', None)
    if end is None:
        end = request.GET.get('end', None)
    analytics = initialize_analytic('analytics', 'v4')
    result['web'] = get_session_by_time(analytics=analytics, service_id=settings.GOOGLE_ANALYTIC_SERVICE_ID, start=start, end=end, filter_exp=WEB)
    result['mobile'] = get_session_by_time(analytics=analytics, service_id=settings.GOOGLE_ANALYTIC_SERVICE_ID, start=start, end=end, filter_exp=MOBILE)
    if not is_json:
        return result
    return json_render(result, 200)


@csrf_exempt
def session_view(request, start=None, end=None, is_json=True):
    result = {}
    if start is None:
        start = request.GET.get('start', None)
    if end is None:
        end = request.GET.get('end', None)
    analytics = initialize_analytic('analytics', 'v4')
    result['web'] = get_session_report(analytics=analytics, service_id=settings.GOOGLE_ANALYTIC_SERVICE_ID, start=start, end=end, filter_exp=WEB)
    result['mobile'] = get_session_report(analytics=analytics, service_id=settings.GOOGLE_ANALYTIC_SERVICE_ID, start=start, end=end, filter_exp=MOBILE)
    if not is_json:
        return result
    return json_render(result, 200)


@csrf_exempt
def city_view(request, start=None, end=None, is_json=True):
    if start is None:
        start = request.GET.get('start', None)
    if end is None:
        end = request.GET.get('end', None)
    analytics = initialize_analytic('analytics', 'v4')
    result = get_city_report(analytics=analytics, service_id=settings.GOOGLE_ANALYTIC_SERVICE_ID, start=start, end=end)
    if not is_json:
        return result
    return json_render(result, 200)


@csrf_exempt
def device_view(request, start=None, end=None, is_json=True):
    if start is None:
        start = request.GET.get('start', None)
    if end is None:
        end = request.GET.get('end', None)
    analytics = initialize_analytic('analytics', 'v4')
    result = get_device_report(analytics=analytics, service_id=settings.GOOGLE_ANALYTIC_SERVICE_ID, start=start, end=end)
    if not is_json:
        return result
    return json_render(result, 200)


@csrf_exempt
def os_view(request, start=None, end=None, is_json=True):
    if start is None:
        start = request.GET.get('start', None)
    if end is None:
        end = request.GET.get('end', None)
    analytics = initialize_analytic('analytics', 'v4')
    result = get_os_report(analytics=analytics, service_id=settings.GOOGLE_ANALYTIC_SERVICE_ID, start=start, end=end)
    if not is_json:
        return result
    return json_render(result, 200)
