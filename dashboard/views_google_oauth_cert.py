"""A simple example of how to access the Google Analytics API."""

import os

import httplib2
from apiclient.discovery import build
from django.conf import settings
from django.core.cache import cache
from utils.cached.time_out import get_time_out, get_time_out_day
from oauth2client.service_account import ServiceAccountCredentials

BASE = os.path.dirname(os.path.abspath(__file__))
SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
DISCOVERY_URI = ('https://analyticsreporting.googleapis.com/$discovery/rest')

KEY_FILEPATH = os.path.join(BASE, settings.GOOGLE_SERVICE_KEY)

FILTER_CHOICE = {
    'web': 'ga:deviceCategory==desktop',
    'mobile': 'ga:deviceCategory==tablet,ga:deviceCategory==mobile'
}


def initialize_analytic(api_name, api_version, is_force=True):
    key = '%s_analytics_init_%s_%s' % (settings.CACHED_PREFIX, api_name, api_version)
    result = None if is_force else cache.get(key)
    if result is None:
        credentials = ServiceAccountCredentials.from_p12_keyfile(
            settings.GOOGLE_ANALYTIC_SERVICE_ACCOUNT_EMAIL, KEY_FILEPATH, scopes=SCOPES)
        http = credentials.authorize(httplib2.Http())
        result = build(api_name, api_version, http=http, discoveryServiceUrl=DISCOVERY_URI)
        cache.set(key, result, get_time_out())
    return result


def report_map(d):
    report = {}
    headers = d['reports'][0]['columnHeader']['metricHeader']['metricHeaderEntries']
    values = d['reports'][0]['data']['rows'][0]['metrics'][0]['values']
    index = 0
    for header in headers:
        report[header['name']] = values[index]
        index += 1
    return report


def get_session_by_time(analytics, service_id, start, end, filter_exp, is_force=False):
    key = '%s_analytics_report_session_by_time_%s_%s_%s_%s' % (settings.CACHED_PREFIX, service_id, filter_exp, start, end)
    result = None if is_force else cache.get(key)
    if result is None:
        result = analytics.reports().batchGet(
            body={
                'reportRequests': [
                    {
                        'viewId': service_id,
                        'dateRanges': [{'startDate': start, 'endDate': end}],
                        'metrics': [{'expression': 'ga:sessions'}],
                        'dimensions': [{'name': 'ga:date'}],
                        'filtersExpression': FILTER_CHOICE[filter_exp]
                    }]

            }
        ).execute()
        cache.set(key, result, get_time_out_day())
    return result


def get_session_report(analytics, service_id, start, end, filter_exp,is_force=False):
    key = '%s_analytics_report_session_%s_%s_%s_%s' % (settings.CACHED_PREFIX, service_id, filter_exp, start, end)
    result = None if is_force else cache.get(key)
    if result is None:
        result = analytics.reports().batchGet(
            body={
                'reportRequests': [
                    {
                        'viewId': service_id,
                        'dateRanges': [{'startDate': start, 'endDate': end}],
                        'metrics': [{'expression': 'ga:sessions'},
                                    {'expression': 'ga:users'},
                                    {'expression': 'ga:percentNewSessions'},
                                    {'expression': 'ga:sessionDuration'},
                                    {'expression': 'ga:screenviews'},
                                    {'expression': 'ga:avgSessionDuration'},
                                    {'expression': 'ga:screenviewsPerSession'},
                                    ],
                        'filtersExpression': FILTER_CHOICE[filter_exp]
                    }]
            }
        ).execute()
        cache.set(key, result, get_time_out_day())
    return result


def get_device_report(analytics, service_id, start, end, is_force=False):
    key = '%s_analytics_report_device_%s_%s_%s' % (settings.CACHED_PREFIX, service_id, start, end)
    result = None if is_force else cache.get(key)
    if result is None:
        result = analytics.reports().batchGet(
            body={
                'reportRequests': [
                    {
                        'viewId': service_id,
                        'dateRanges': [{'startDate': start, 'endDate': end}],
                        'metrics': [{'expression': 'ga:sessions'}],
                        'dimensions': [{'name': 'ga:deviceCategory'}],
                        'orderBys': [{'fieldName': 'ga:sessions', 'sortOrder': 'DESCENDING'}]
                    }]
            }
        ).execute()
        cache.set(key, result, get_time_out_day())
    return result


def get_os_report(analytics, service_id, start, end, is_force=False):
    key = '%s_analytics_report_os_%s_%s_%s' % (settings.CACHED_PREFIX, service_id, start, end)
    result = None if is_force else cache.get(key)
    if result is None:
        result = analytics.reports().batchGet(
            body={
                'reportRequests': [
                    {
                        'viewId': service_id,
                        'dateRanges': [{'startDate': start, 'endDate': end}],
                        'metrics': [{'expression': 'ga:sessions'}],
                        'dimensions': [{'name': 'ga:operatingSystem'}],
                        'orderBys': [{'fieldName': 'ga:sessions', 'sortOrder': 'DESCENDING'}]
                    }]
            }
        ).execute()
        cache.set(key, result, get_time_out_day())
    return result


def get_city_report(analytics, service_id, start, end, is_force=False):
    key = '%s_analytics_report_city_%s_%s_%s' % (settings.CACHED_PREFIX, service_id, start, end)
    result = None if is_force else cache.get(key)
    if result is None:
        result = analytics.reports().batchGet(
            body={
                'reportRequests': [
                    {
                        'viewId': service_id,
                        'dateRanges': [{'startDate': start, 'endDate': end}],
                        'metrics': [{'expression': 'ga:sessions'}],
                        'dimensions': [{'name': 'ga:city'}],
                        'orderBys': [{'fieldName': 'ga:sessions', 'sortOrder': 'DESCENDING'}]
                    }]
            }
        ).execute()
        cache.set(key, result, get_time_out_day())
    return result
