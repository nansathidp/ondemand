from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

from promote.models import Promote, Code, ShareFacebookLog

import requests, json

def facebook_post_exits(account, promote, post_id, access_token):
    share_facebook_log = ShareFacebookLog(account=account,
                                          promote=promote,
                                          post_id=post_id,
                                          status=1,
                                          access_token=access_token)
    payload = {'access_token': access_token}
    try:
        r = requests.get('https://graph.facebook.com/v2.3/%s/'%post_id, params=payload)
        result = r.text
    except:
        result = ''
        share_facebook_log.status = -1
    if share_facebook_log.status == 1:
        try:
            response = json.loads(result)
        except:
            share_facebook_log.status = 0
    if share_facebook_log.status == 1:
        share_facebook_log.response = json.dumps(response, indent=2)
        if 'error' in response:
            share_facebook_log.status = 2
    else:
        share_facebook_log.response = result    
    share_facebook_log.save()
    return share_facebook_log.status

@csrf_exempt
@login_required
def code_view(request):
    code = None
    code_list = list()
    status = None
    max_gen = False
    if request.method == 'POST':
        promote = get_object_or_404(Promote, id=request.POST.get('promote_id', 0))
        post_id = request.POST.get('post_id', 0)
        access_token = request.POST.get('access_token', '')
        #status = facebook_post_exits(request.user, promote, post_id, access_token)
        status = 1
        if status == 1 and promote.rule == 1:
            if Code.objects.filter(promote=promote, account=request.user).count() < promote.max_gen:
                code = Code(promote=promote,
                            account=request.user)
                code.gen()
                code.save()
            else:
                max_gen = True
                code_list = Code.objects.filter(promote=promote, account=request.user)
        elif status == 1 and promote.rule == 2:
            code = None
    return render(request,
                  'promote/facebook_code.html',
                  {'status': status,
                   'code': code,
                   'max_gen': max_gen,
                   'code_list': code_list})
