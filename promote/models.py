from django.db import models
from django.conf import settings


class Promote(models.Model):
    RULE_CHOICES = (
        (1, 'Share Facebook'),
        (2, 'One Code'),
        (3, 'Free (Public)'),
        (4, 'Free (Private)'),
    )

    TYPE_CHOICES = (
        (1, 'Per Order'),
        (2, 'Per Item (Limit)'),
    )

    DISCOUNT_TYPE_CHOICES = (
        (1, 'Price'),
        (2, 'Percent'),
    )

    name = models.CharField(max_length=120)
    rule = models.IntegerField(choices=RULE_CHOICES)
    type = models.IntegerField(choices=TYPE_CHOICES)
    discount_type = models.IntegerField(choices=DISCOUNT_TYPE_CHOICES) 
    discount = models.FloatField(default=-1.0)

    max_gen = models.IntegerField(default=1)
    max_use = models.IntegerField(default=1)
    code = models.CharField(max_length=12, blank=True)
    
    start = models.DateTimeField(db_index=True)
    end = models.DateTimeField(db_index=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')
        ordering = ['start']
        
    def __str__(self):
        return self.name
    
    @staticmethod
    def pull_one():
        from django.utils import timezone
        now = timezone.now()
        try:
            return Promote.objects.filter(start__lte=now, end__gte=now)[0]
        except:
            return None

    @staticmethod
    def push(order, user, code):
        from django.utils import timezone
        from order.models import Promote as OrderPromote
        is_success = False
        now = timezone.now()
        promote = Promote.objects.filter(code__iexact=code, start__lte=now, end__gte=now).first()
        if promote is not None and not order.promote_set.filter(promote_item=promote).exists():
            if promote.rule == 2 and promote.type == 1:
                order_promote = OrderPromote.objects.create(order=order,
                                                            promote_item=promote,
                                                            item=None,
                                                            code=None)
                order.cal_price()
            elif promote.rule == 2 and promote.type == 2:
                order_item_list = list(order.item_set.all())
                promote_item_list = list(promote.item_set.all())
                is_found = False
                order_item_found = None
                for order_item in order_item_list:
                    for promote_item in promote_item_list:
                        if order_item.content_type_id == promote_item.content_type_id and order_item.content == promote_item.content:
                            is_found = True
                            order_item_found = order_item
                            break
                    if is_found:
                        break
                if is_found:
                    order_promote = OrderPromote.objects.create(order=order,
                                                                promote_item=promote,
                                                                item=order_item_found,
                                                                code=None)
                    order.cal_price()
            elif promote.rule == 3 and promote.type == 1: #Free order
                pass
            elif promote.rule == 3 and promote.type == 2: #Free Item
                order_item_list = list(order.item_set.all())
                promote_item_list = list(promote.item_set.all())
                is_found = False
                order_item_found = None
                for order_item in order_item_list:
                    for promote_item in promote_item_list:
                        if order_item.content_type_id == promote_item.content_type_id and order_item.content == promote_item.content:
                            is_found = True
                            order_item_found = order_item
                            break
                    if is_found:
                        break
                if is_found:
                    order_promote = OrderPromote.objects.create(order=order,
                                                                promote_item=promote,
                                                                item=order_item_found,
                                                                code=None)
                    order.cal_price()
                    
        return is_success
    
class Item(models.Model):
    promote = models.ForeignKey(Promote)
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='promote_item_set')
    content = models.BigIntegerField(db_index=True)

class Code(models.Model):
    promote = models.ForeignKey(Promote, related_name='code_set')
    account = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)
    code = models.CharField(max_length=8, db_index=True)
    timestamp_gen = models.DateTimeField(auto_now_add=True)
    timestamp_use = models.DateTimeField(null=True, blank=True, db_index=True)

    def __str__(self):
        return self.code
    
    def gen(self):
        import random
        c = 'ABCDEFGHJKMNPQRSTUVXYZ23456789'
        while True:
            code = ''.join(random.choice(c) for _ in range(6))
            if Code.objects.filter(promote=self.promote, code=code).count() == 0:
                self.code = code
                break
            
    @staticmethod
    def push(order, account, code_input):
        from django.utils import timezone
        from django.db.models import Q
        
        from order.models import Promote as OrderPromote
        is_success = False
        code = Code.objects.filter(~Q(promote__rule=4),
                                   Q(account=account),
                                   Q(code__iexact=code_input),
                                   Q(timestamp_use__isnull=True)).first()
        if code is not None:
            order_promote_list = order.promote_set.filter(promote_item_id=code.promote_id)
            is_use = False
            use_count = 0
            for order_promote in order_promote_list:
                use_count += 1
                if order_promote.code == code:
                    is_use = True
            if not is_use and use_count < code.promote.max_use:
                OrderPromote.objects.create(order=order,
                                            promote_item=code.promote,
                                            item=None,
                                            code=code)
                code.timestamp_use = timezone.now()
                code.save(update_fields=['timestamp_use'])
                order.cal_price()
                is_success = True
        else:
            code = Code.objects.filter(promote__rule=4, code__iexact=code_input, timestamp_use__isnull=True).first() #Free (Private)
            if code is not None:
                if not OrderPromote.objects.filter(code=code).exists():
                    OrderPromote.objects.create(order=order,
                                                promote_item=code.promote,
                                                item=None,
                                                code=code)
                    code.account = account
                    code.timestamp_use = timezone.now()
                    code.save(update_fields=['account', 'timestamp_use'])
                    order.cal_price()
                    is_success = True
        return is_success
        
class ShareFacebookLog(models.Model):
    STATUS_CHOICES = (
        (-2, 'Not Request (Rule Fail)'),
        (-1, 'Request Fail'),
        (0, 'Response Json Fail'),
        (1, 'Success'),
        (2, 'Error'),
    )

    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    promote = models.ForeignKey(Promote)
    post_id = models.CharField(max_length=24, blank=True)
    access_token = models.CharField(max_length=512, blank=True)
    response = models.TextField(blank=True)
    status = models.IntegerField(choices=STATUS_CHOICES)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']
        
