from django.shortcuts import render, redirect
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from .models import Promote
from order.models import Order, Item
from course.models import Course
from package.models import Package
from coin.models import Coin, Volume as CoinVolume
from content.models import Location as ContentLocation

import json

def redeem_views(request):
    result = None
    debug = None
    if request.method == 'POST' and request.user.is_authenticated():
        code = request.POST.get('code', None)
        content_type_course = settings.CONTENT_TYPE('course', 'course')
        content_type_package = settings.CONTENT_TYPE('package', 'package')
        content_type_coin = settings.CONTENT_TYPE('coin', 'coin')
        now = timezone.now()

        if code is not None:
            promotion = Promote.objects.filter(rule=3, code__iexact=code).first()
            if promotion is not None:
                item_list = promotion.item_set.all()                
                for item in item_list:
                    debug = 'Loop'
                    if Item.objects.filter(account=request.user, status=2, content_type=item.content_type, content=item.content).count() == 0:
                        if item.content_type == content_type_course:
                            debug = 'Course Start'
                            content = Course.objects.filter(id=item.content).first()
                            if content is not None:
                                content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                                              content.id)
                                order_item = Order.buy(content_location,
                                                       request.APP,
                                                       1,
                                                       content_type_course,
                                                       content,
                                                       request.user,
                                                       is_checkout=True,
                                                       method=17)
                                order = order_item.order
                            else:
                                debug = 'Course'
                        elif item.content_type == content_type_coin:
                            debug = 'Coin Start'
                            content = Coin.objects.filter(id=item.content).first()
                            if content is not None:
                                order = Order.pull_create(account=request.user, store=1, status=1, method=17, app=request.APP, timecomplete=now) # TODO: remove
                                order.buy_coin(content, request.user, 1)
                            else:
                                debug = 'Coin'
                        else:
                            debug = 'Not found'
                    else:
                        debug = 'Item'
                # Complete Order
                # order.clear_price()
                # for item in order.item_set.all():
                #     item.status = 2
                #     item.save(update_fields=['status'])
                #     if item.content_type.id == content_type_coin.id:
                #         if order.status == 3:
                #             continue
                #         coin = Coin.objects.get(id=item.content)
                #         if coin.type == 0:
                #             CoinVolume.push(request.APP, order.account, coin.volume)
                #         elif coin.type == 1:
                #             data = json.loads(item.data)
                #             CoinVolume.push(request.APP, order.account, data['price'])
                # order.status = 1
                # order.extra = 1
                # order.status = 3
                # order.timecomplete = now
                # order.save()
                # return redirect('profile')

                if order.item_set.all().count() > 0:
                    order.push_promote_code(request.user, code)
                    return redirect('order:cart', order.id)
                else:
                    order.delete()
                    result = 'คุณมีคอร์สนี้อยู่แล้ว ไม่สามารถใช้ซ้ำได้'
                
            else:
                result = 'รหัสส่วนลดไม่ถูกต้อง'

    return render(request, 'promote/redeem.html', {'result': result, 'debug': debug})

