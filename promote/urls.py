from django.conf.urls import url

#TODO: migrate url format to from .views_xxx import xxx_view :)
from . import views_facebook_code
from . import views_redeem

app_name = 'promote'
urlpatterns = [
    url(r'^facebook/code/$', views_facebook_code.code_view, name='facebook_code'), #TODO: testing
    url(r'^redeem/$', views_redeem.redeem_views, name='redeem'), #TODO: testing
]
