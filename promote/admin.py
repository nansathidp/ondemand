from django.contrib import admin

from utils.cached.content_type import cached_content_type_admin_item_inline
from promote.models import *

class ItemInline(admin.TabularInline):
    model = Item
    fields = ('id', 'content_type', 'content')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'content_type':
            kwargs["queryset"] = cached_content_type_admin_item_inline()
        return super(ItemInline, self).formfield_for_foreignkey(db_field, request, **kwargs)

@admin.register(Promote)
class PromoteAdmin(admin.ModelAdmin):
    list_display = ('name', 'rule', 'type', 'discount_type', 'discount', 'max_gen', 'max_use', 'code', 'start', 'end')
    inlines = [ItemInline]
    
@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('promote', 'content_type', 'content')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'content_type':
            kwargs["queryset"] = cached_content_type_admin_item_inline()
        return super(ItemAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

@admin.register(Code)
class CodeAdmin(admin.ModelAdmin):
    list_display = ('promote', 'account', 'code', 'timestamp_gen', 'timestamp_use')
    readonly_fields = ['account']
    
@admin.register(ShareFacebookLog)
class ShareFacebookLogAdmin(admin.ModelAdmin):
    list_display = ('account', 'promote', 'post_id', 'status', 'timestamp')
