from django.test import TestCase

from .models import Category

import random
import string


def _random():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(8))


PROJECT_LIST = ['aisondemand', 'com.pacrim.ondemand', 'demo.ondemand']


def create_category():
    return Category.objects.create(name=_random())
