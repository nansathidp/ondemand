from django.conf import settings
from django.core.cache import cache


from .models import Category
from utils.cached.time_out import get_time_out, get_time_out_week


def cached_category(category_id, is_force=False):
    try:
        category_id = int(category_id)
    except:
        return None
    key = '%s_category_%s' % (settings.CACHED_PREFIX, category_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Category.objects.get(id=category_id)
            cache.set(key, result, get_time_out())
        except:
            result = -1
    return None if result == -1 else result


def cached_category_delete(category):
    key = '%s_category_%s' % (settings.CACHED_PREFIX, category.id)
    cache.delete(key)


def cached_category_list(is_force=False):
    key = '%s_category_list' % settings.CACHED_PREFIX
    result = None if is_force else cache.get(key)
    if result is None:
        result = Category.objects.filter(is_display=True).order_by('sort')
        cache.set(key, result, get_time_out())
    return result


def cached_category_list_delete():
    key = '%s_category_list' % settings.CACHED_PREFIX
    cache.delete(key)

