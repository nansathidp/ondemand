from django import forms
from django.conf import settings
from ..models import Category


class CategoryForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = ['name', 'image', 'image_featured', 'image_cover', 'is_display']

        labels = {
            'tutor': 'Instructor',
            'is_display': 'Display',
        }

        help_texts = {
            'name': 'Limit at 120 characters',
            'image': settings.HELP_TEXT_IMAGE('category'),
            'image_featured': settings.HELP_TEXT_IMAGE('featured'),
            'image_cover': settings.HELP_TEXT_IMAGE('cover'),
        }

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({'type': 'file'})
        self.fields['image_cover'].widget.attrs.update({'type': 'file'})