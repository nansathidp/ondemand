from django.conf.urls import url

from .views import home_view
from .views_create import create_view
from .views_detail import detail_view

app_name = 'category'
urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^create/$', create_view, name='create'),
    url(r'^(\d+)/$', detail_view, name='detail'),
]
