from django.shortcuts import render
from django.core.exceptions import PermissionDenied

from ..models import Category

from utils.paginator import paginator


def home_view(request):
    if not request.user.has_perm('category.view_category',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    category_list = Category.objects.all()
    category_list = paginator(request, category_list)
    
    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Category Management'}
    ]
    return render(request,
                  'category/dashboard/home.html',
                  {'SIDEBAR': 'category',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'category_list': category_list})
