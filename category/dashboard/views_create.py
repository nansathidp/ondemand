from django.shortcuts import render, redirect
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from .forms import CategoryForm

from ..cached import cached_category_list_delete
from utils.crop import crop_image


def create_view(request):
    if not request.user.has_perm('category.add_category',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    if request.method == 'POST':
        category_form = CategoryForm(request.POST, request.FILES)
        if category_form.is_valid():
            category = category_form.save()
            cached_category_list_delete()
            crop_image(category, request)
            return redirect('dashboard:category:home')
    else:
        category_form = CategoryForm()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Category Management',
         'url': reverse('dashboard:category:home')},
        {'is_active': True,
         'title': 'Create'},
    ]
    return render(request,
                  'category/dashboard/create.html',
                  {'SIDEBAR': 'category',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'category_form': category_form})
