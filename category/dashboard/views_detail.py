from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from .forms import CategoryForm
from ..models import Category

from ..job.count_content import _count_content

from ..cached import cached_category_list_delete, cached_category_delete
from utils.crop import crop_image
import django_rq


def detail_view(request, category_id):
    if not request.user.has_perm('category.change_category',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    category = get_object_or_404(Category, id=category_id)

    if request.method == 'POST':
        category_form = CategoryForm(request.POST, request.FILES, instance=category)
        if category_form.is_valid():
            category = category_form.save()
            crop_image(category, request)
            cached_category_delete(category)
            cached_category_list_delete()
            # django_rq.enqueue(_count_content, category.id)
            category_form = CategoryForm(instance=category)
    else:
        category_form = CategoryForm(instance=category)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Category Management',
         'url': reverse('dashboard:category:home')},
        {'is_active': True,
         'title': category.name},
    ]
    return render(request, 'category/dashboard/detail.html',
                  {'SIDEBAR': 'category',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'information',
                   'category_form': category_form,
                   'category': category})
