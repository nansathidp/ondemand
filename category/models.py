from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=120)
    desc = models.TextField(blank=True)

    image = models.ImageField(upload_to='category/', blank=True)
    image_cover = models.ImageField(upload_to='category/cover/', blank=True)
    image_featured = models.ImageField(upload_to='category/featured/', blank=True)

    sort = models.IntegerField(db_index=True, default=0)
    is_display = models.BooleanField(default=True, db_index=True)

    extra = models.TextField(blank=True)

    class Meta:
        default_permissions = ('view', 'add', 'change', 'delete')  # not use view_own
        ordering = ['sort']

    def __str__(self):
        return self.name

    @staticmethod
    def pull(id):
        from .cached import cached_category
        return cached_category(id)

    @staticmethod
    def pull_list():
        from .cached import cached_category_list
        return cached_category_list()

    @staticmethod
    def featured_list():
        return Category.objects.filter(is_display=True) \
                               .exclude(image_featured=None) \
                               .order_by('sort')

    def api_display_title(self):
        return {
            'id': self.id,
            'name': self.name
        }

    def api_display(self):
        return {
            'id': self.id,
            'name': self.name,
            'image': self.image.url if self.image else None,
            'image_cover': self.image_cover.url if self.image_cover else None,
            'image_featured': self.image_featured.url if self.image_featured else None
        }
