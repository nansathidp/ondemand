from category.models import Category


def _count_content(category_id):
    category = Category.objects.get(id=category_id)
    category.count_content()
