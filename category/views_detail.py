from django.conf import settings
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import Http404
from django.shortcuts import render, get_object_or_404

from course.cached import pack_course
from course.models import Course
from ondemand.views_base import content_sort
from ondemand.views_decorator import check_project, param_filter
from program.cached import pack_program
from program.models import Program
from provider.cached import cached_provider_list
from rating.models import Rating
from .cached import cached_category
from .models import Category


@check_project
@param_filter
def detail_view(request, category_id):
    menu = None
    category = cached_category(category_id)
    if category is None:
        raise Http404('Category does not exist')

    # Course & Program
    program_id_list = Program.objects.values_list('id', flat=True).filter(is_display=True, category_id=category_id)[:4]
    course_id_list = Course.objects.values_list('id', flat=True).filter(is_display=True, category_id=category_id)
    content_type = settings.CONTENT_TYPE('course', 'course')

    if request.provider:
        course_id_list = course_id_list.filter(provider=request.provider)

    course_id_list = content_sort(request, course_id_list, content_type)
    paginator = Paginator(course_id_list, 24)
    try:
        pager = paginator.page(request.page)
    except PageNotAnInteger:
        pager = paginator.page(1)
    except EmptyPage:
        pager = paginator.page(paginator.num_pages)

    course_list = []
    for course in pack_course(course_id_list):
        course.tutor_list = course.get_tutor_list()
        course.rating = Rating.pull_first(content_type, course.id)
        course_list.append(course)

    return render(request,
                  'category/detail.html',
                  {'DISPLAY_SUBMENU': True,
                   'ROOT_PAGE': '%s' % category.id,
                   'course_list': course_list,
                   'program_list': pack_program(program_id_list),
                   'category': category,
                   'pager': pager,
                   'menu': menu,
                   'sort': request.sort,
                   'provider_list': cached_provider_list()})


def slug_detail_view(request, slug):
    try:
        category = get_object_or_404(Category, slug=slug)
    except:
        raise Http404
    return detail_view(request, category.id)
