from django import template

from ..models import Category

register = template.Library()

@register.simple_tag
def pull_category_tag(id):
    return Category.pull(id)
