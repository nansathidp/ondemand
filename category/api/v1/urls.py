from django.conf.urls import url
from .views_list import list_view
from .views_detail import detail_view
from .views_detail_grid import grid_detail_view

urlpatterns = [
    url(r'^$', list_view),  # TODO: testing
    url(r'^(\d+)/$', detail_view),  # TODO: testing
    url(r'^(\d+)/grid/$', grid_detail_view),  # TODO: testing
]
