from .views import json_render, check_key
from ...cached import cached_category_list


def list_view(request):
    result = {}
    code = 200
    response = check_key(request)
    if response is not None:
        return response

    result['category_list'] = []
    for category in cached_category_list():
        result['category_list'].append(category.api_display())
    return json_render(result, code)
