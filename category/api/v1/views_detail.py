from .views import json_render, check_key

from tutor.models import Tutor
from course.models import Course

from category.cached import cached_category
from provider.models import Provider
from program.models import Program
from api.views_api import init_section
from .views import check_require


def detail_view(request, category_id):
    result = {}
    response = check_key(request)
    if response is not None:
        return response

    code, token, uuid, store, version = check_require(request, require_token=False)
    if code != 200:
        return json_render(result, code)

    result['section_list'] = list()
    category = cached_category(category_id)
    if category is None:
        return json_render(result,  900)

    result['category'] = category.api_display()

    tutor_id_list = []
    provider_list = []
    course_list = []
    program_list = []

    try:
        provider_id = request.GET.get('provider')
        provider_item = Provider.objects.filter(id=provider_id).first()
    except:
        provider_item = None

    course_id_list = Course.pull_category(category_id, provider_item)
    result['course_id_list'] = list(course_id_list)
    for course_id in course_id_list:
        course = Course.pull(course_id)
        course_list.append(course)
        for tutor in course.tutor.all():
            if tutor.id not in tutor_id_list:
                tutor_id_list.append(tutor.id)
                for provider in tutor.providers.all():
                    if provider not in provider_list:
                        provider_list.append(Provider.pull(provider.id))

    program_id_list = Program.pull_category(category_id, provider_item)
    for program_id in program_id_list:
        program = Program.pull(program_id)
        program_list.append(program)
        if program.provider not in provider_list:
            provider_list.append(Provider.pull(program.provider.id))

    d = init_section('Featured Courses', 4, -1, -1, False)
    d['course_list'] = [course.api_display_title(store=store) for course in course_list[:10]]
    if len(d['course_list']) > 0:
        result['section_list'].append(d)

    d = init_section('Courses', 4, 4, category_id, True)
    d['course_list'] = [course.api_display_title(store=store) for course in course_list[:10]]
    if len(d['course_list']) > 0:
        result['section_list'].append(d)

    d = init_section('Program', 8, 4, category_id, True)
    d['program_list'] = [program.api_display(store=store) for program in program_list[:10]]
    if len(d['program_list']) > 0:
        result['section_list'].append(d)

    # TODO: Use Cached
    if len(provider_list) > 0:
        d = init_section('Featured Learning Center', 9, -1, -1, False)
        d['provider_list'] = [provider.api_display_title() for provider in provider_list[:10]]
        result['section_list'].append(d)

    # TODO: Use Cached
    d = init_section('Featured Instructors', 3, -1, -1, False)
    d['tutor_list'] = []
    for tutor in Tutor.objects.filter(id__in=tutor_id_list,
                                      is_display=True).exclude(image_featured__isnull=True).distinct()[:10]:
        if tutor.image_featured:
            d['tutor_list'].append(tutor.api_display_title())

    if len(d['tutor_list']) > 0:
        result['section_list'].append(d)

    return json_render(result, code)
