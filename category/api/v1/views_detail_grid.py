from .views import json_render, check_key, check_require

from ...models import Category
from course.models import Course


def grid_detail_view(request, category_id):
    result = {}

    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request, require_token=False)
    if code != 200:
        return json_render(result, code)

    try:
        category = Category.objects.get(id=category_id)
    except Category.DoesNotExist:
        return json_render({}, 900)

    result['course_list'] = []
    result['category'] = category.api_display()
    course_list = Course.pull_category(category_id)
    for course_id in course_list:
        course = Course.pull(course_id)
        if course is not None:
            result['course_list'].append(course.api_display_title(store, version, request.APP.app_version))

    return json_render(result, code)
