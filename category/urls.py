from django.conf.urls import url

from .views import home_view
from .views_detail import detail_view, slug_detail_view

app_name = 'category'
urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^(\d+)/$', detail_view, name='detail'),
    url(r'^(?P<slug>[-\w\d]+)/$', slug_detail_view, name='detail'),
]
