from django.shortcuts import render

from .models import Category
from ondemand.views_decorator import check_project


@check_project
def home_view(request):
    category_list = Category.objects.filter(is_display=True).order_by('sort')
    return render(request,
                  'category/home.html',
                  {'DISPLAY_SUBMENU': True,
                   'ROOT_PAGE': 'subject',
                   'category_list': category_list})
