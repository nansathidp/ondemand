from django.db import models
from django.conf import settings

REQUEST_TYPE = (
    (1, 'Web'),
    (2, 'Mobile API'),
    (3, 'Mobile WebView'),
)

REQUEST_STEP = (
    (1, 'Request Pay'),
    (2, 'Check Pay Success'),
    (3, 'Response Back RE-Check'),
)

REQUEST_RESULT = (
    (0, '-'),
    (20, 'Success'),
    (21, 'Except Search Result'),
    (22, 'Result != 00'),
)

RESPONSE_CODE = (
    (-1, '-'),
    (1, 'A รอการชำระเงิน'),
    (2, 'S ชำระเงินแล้ว'),
    (3, 'E หมดอายุ'),
    (4, 'C ยกเลิก'),
    (5, 'N ไม่พบข้อมูล inv หรือ Username ไม่มีสิทธิ์ใช้ Service Inquiry'),
    (6, 'การเชื่อมต่อมีปัญหาทำให้ไม่สามารถ Connect API ได้'),
)


class Request(models.Model):
    order = models.ForeignKey('order.Order', related_name='payatall_request_set')
    payload = models.TextField()
    html = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    
    class Meta:
        ordering = ['-timestamp']

    @staticmethod
    def send(order, domain, base_url):
        from django.conf import settings
        from django.utils import timezone
        from django.core.urlresolvers import reverse
        import requests, json, datetime
        request = Request.objects.filter(order=order).first()
        if request is None:
            if len(order.account.tel) != 10:
                tel = settings.CONFIG(domain, 'CONTACT_NUMBER').replace('-', '')
            else:
                tel = order.account.tel
            customer_name = order.account.get_escape_name()
            if len(customer_name) == 0:
                customer_name = settings.CONFIG(domain, 'BASE_NAME')
            payload = {'mid': settings.CONFIG(domain, 'PAYATALL_MERCHANT_ID'),
                       'inv': str(order.id),
                       'desp': str(order.id),
                       'amt': '%.2f'%order.net,
                       'curr_type': 'THB',
                       'serviceid': '01',
                       'language': 'TH',
                       'customer_name': customer_name,
                       'customer_email': order.account.email,
                       'customer_phone': tel,
                       'resp_url_back': '%s%s'%(base_url, reverse('order:payatall_success', args=[order.id])),
                       'resp_url_cancel': '%s%s'%(base_url, reverse('order:payatall_cancel', args=[order.id])),
                       'resp_url_confirm': '%s%s'%(base_url, reverse('order:payatall_confirm', args=[order.id])),
                       'expired_date': (timezone.now() + datetime.timedelta(days=2)).strftime('%Y-%m-%d %H:%M:%S'),
                       'message_slip1': '',
                       'message_slip2': '',
                       'channel': ''}
            r = requests.post(settings.CONFIG(domain, 'PAYATALL_URL'), data=payload)
            r.encoding = 'utf-8'
            request = Request.objects.create(order=order,
                                             payload=json.dumps(payload, indent=2),
                                             html=r.text)
            order.status = 2
            order.method = 16
            order.save(update_fields=['status', 'method'])
        elif order.status == 1:
            order.status = 2
            order.method = 16
            order.save(update_fields=['status', 'method'])
        return request


class Response(models.Model):
    text = models.TextField(blank=True)
    inv = models.CharField(max_length=36, blank=True)
    order = models.ForeignKey('order.Order', related_name='payatall_response_set', null=True)
    ptransid = models.IntegerField(default=-1)
    amt = models.FloatField(default=0.0)
    status = models.IntegerField(choices=RESPONSE_CODE, default=-1)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']


class Inquiry(models.Model):
    payload = models.TextField()
    text = models.TextField(blank=True)
    inv = models.CharField(max_length=36, blank=True)
    order = models.ForeignKey('order.Order', related_name='payatall_inquiry_set', null=True)
    ptransid = models.IntegerField(default=-1)
    amt = models.FloatField(default=0.0)
    status = models.IntegerField(choices=RESPONSE_CODE, default=-1)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']

    @staticmethod
    def send_inquiry(app, domain, order):
        import requests, json, traceback
        payload = {'username': settings.CONFIG(domain, 'PAYATALL_USERNAME'),
                   'password': settings.CONFIG(domain, 'PAYATALL_PASSWORD'),
                   'mid': settings.CONFIG(domain, 'PAYATALL_MERCHANT_ID'),
                   'serviceid': '01',
                   'inv': str(order.id),
                   'amt': order.net}
        inquiry = Inquiry.objects.create(order=order,
                                         payload=json.dumps(payload, indent=2))
        try:
            r = requests.post(settings.CONFIG(domain, 'PAYATALL_INQUIRY_URL'),
                              data=payload)
            result = json.loads(r.text)
            inquiry.text = json.dumps(result, indent=2)
            inquiry.inv = result['inv']
            inquiry.ptransid = result['ptransid']
            inquiry.amt = float(result['amt'])
            if result['status'] == 'A':
                inquiry.status = 1
            elif result['status'] == 'S':
                inquiry.status = 2
            elif result['status'] == 'E':
                inquiry.status = 3
            elif result['status'] == 'C':
                inquiry.status = 4
            elif result['status'] == 'N':
                inquiry.status = 5
            elif result['status'] == 'Z':
                inquiry.status = 6
        except:
            inquiry.text += '\n---\n'+traceback.format_exc()
        inquiry.save(update_fields=['text', 'inv', 'ptransid', 'amt', 'status'])
        if inquiry.status == 2 and order.net == inquiry.amt:
            order.buy_success(app, domain, method=16)
