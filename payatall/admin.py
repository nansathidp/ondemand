from django.contrib import admin
from payatall.models import Request, Response, Inquiry

@admin.register(Request)
class RequestAdmin(admin.ModelAdmin):
    list_display = ('order', 'timestamp')

@admin.register(Response)
class ResponseAdmin(admin.ModelAdmin):
    list_display = ('inv', 'order', 'ptransid', 'amt', 'status', 'timestamp')

@admin.register(Inquiry)
class InquiryAdmin(admin.ModelAdmin):
    list_display = ('inv', 'order', 'ptransid', 'amt', 'status', 'timestamp')
    
