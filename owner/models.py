from django.db import models


class Owner(models.Model):
    from django.conf import settings

    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    content_type = models.ForeignKey('contenttypes.ContentType')
    content = models.BigIntegerField(db_index=True)

    group = models.ForeignKey('auth.Group', null=True, blank=True)
    provider = models.ForeignKey('provider.Provider', null=True, blank=True, on_delete=models.SET_NULL)

    display = models.CharField(max_length=120)
