from django.contrib import admin

from .models import Owner

@admin.register(Owner)
class OwnerAdmin(admin.ModelAdmin):
    list_display = ('account', 'content_type', 'content', 'group', 'provider', 'display')
