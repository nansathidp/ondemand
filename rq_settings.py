from ondemand.settings import RQ_DB

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379
REDIS_DB = RQ_DB

QUEUES = ['low']
