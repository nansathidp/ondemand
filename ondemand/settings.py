import os
import json
import datetime

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = 'oirlphi8-3ff)vxqy05o52k2xa(=#u_a1d+69eu&_2vpq9+)xm'

DEBUG = True
SITE_ID = 1
ALLOWED_HOSTS = []

AUTH_USER_MODEL = 'account.Account'

CODE = 'ondemand'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'dbbackup',
    'rest_framework',
    'django_filters',
    'rest_framework_swagger',

    'department',
    'level',
    'account',
    'inbox',
    'access',

    'content',
    'owner',
    'category',
    'featured',
    'ads',
    'course',
    'pool',
    'stream',
    'lesson',
    'tutor',
    'notification',
    'active',

    'provider',
    'dashboard',
    'analytic',
    'rating',
    'discussion',

    'price',
    'store',
    'order',
    'program',
    'coin',

    'progress',
    'purchase',
    'paysbuy',
    'payatall',
    'aismpay',
    'mpay',
    'promote',
    'contact',
    'manual',
    'task',
    'term',

    'live',

    'api',

    'app',
    'key',

    'console',
    'mailer',
    'question',
    'dependency',
    'assignment',
    'assignment_auto',

    'log',
    'alert',

    'bootstrapform',  # remove (use include template)
    'django_rq',
    'scheduler',
    'axes'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'ondemand.middleware_auth.AuthenticationMiddleware',
    'ondemand.middleware_app.MiddlewareApp',
    'ondemand.middleware_dashboard.MiddlewareDashboard',
]

# MIDDLEWARE_CLASSES = [
#     'django.middleware.security.SecurityMiddleware',
#     'django.middleware.locale.LocaleMiddleware',
#     'django.middleware.common.CommonMiddleware',
#     'django.middleware.csrf.CsrfViewMiddleware',
#     'django.middleware.clickjacking.XFrameOptionsMiddleware',
#     'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
#
#     'ondemand.middleware.MySessionMiddleware',
#     'ondemand.middleware.MyAuthenticationMiddleware',
#     'ondemand.middleware.MySessionAuthenticationMiddleware',
#     'ondemand.middleware.MyMessageMiddleware',
#     'ondemand.middleware_auth.AuthenticationMiddleware',
#     'ondemand.middleware_app.MiddlewareApp',
#     'ondemand.middleware_dashboard.MiddlewareDashboard',
# ]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'ondemand.middleware_auth.ModelBackend',
)

ROOT_URLCONF = 'ondemand.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates/')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'ondemand.context_processors.perms',
                'ondemand.context_processors.templates_config'
            ],
        },
    },
]

WSGI_APPLICATION = 'ondemand.wsgi.application'

DBBACKUP_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
DBBACKUP_STORAGE_OPTIONS = {
    'access_key': 'my_id',
    'secret_key': 'my_secret',
    'bucket_name': 'my_bucket_name'
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

TIME_ZONE = 'Asia/Bangkok'

TIME_FORMAT = '%e %B %Y'
TIME_FULL_FORMAT = '%e %B %Y %H:%M:%S'
TIME_SHORT_FORMAT = '%e/%m/%y %H:%M:%S'

DATETIME_FORMAT = 'Y-m-d H:i:s'
TIMESTAMP_FORMAT_ = '%Y-%m-%d %H:%M:%S'

DATE_FORMAT = 'd M Y'
DATE_FORMAT_ = '%d %b %Y'

DATETIME_FORMAT_ = '%d %b %Y, %H:%M'

LANGUAGE_CODE = 'en'

LANGUAGES = (
    ('th', 'Thai'),
    ('en', 'English'),
)

USE_I18N = True

USE_L10N = False

USE_TZ = True

SITE_URL = 'http://127.0.0.1:8000'
QUESTION_INFORMATION_UPLOAD_TYPE = None
SERVER_UPLOAD_URL = 'http://103.246.16.183/connect/'
SERVER_IP = '127.0.0.1'

STATIC_URL = '/static/'

LOGIN_URL = '/login/'

MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

STATIC_ROOT = os.path.join(BASE_DIR, 'deploy_static/')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static/'),
)

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale/')
]

CACHED_PREFIX = 'ONDEMAND'

# Project Name
PROJECT = None

# Feature Toggle
IS_ONBOARD = False
IS_SUB_PROVIDER = False
IS_ORGANIZATION = False
IS_CONTENT_ACCESSIBILITY = False
IS_CONTENT_CREDIT = False
IS_PRICE = False
IS_SLUG = False
IS_INBOX = False
IS_CAREER_LEVEL = False
IS_ALLOW_DELETE_CONTENT = True

IS_ALERT_AIS = False
IS_ALERT_ASSIGNMENT = False
IS_ALERT_DIRECT_MSG = False

IS_SIGNUP = False

# LOGIN MODULE

IS_FORGOT_PASSWORD = False
IS_FORCE_RESET_PASSWORD = False

IS_FIRST_LOGIN = False
IS_ENABLE_ACCOUNT = False


# END

IS_PRODUCTION = False
IS_LIMIT_ACTIVE = False
IS_LOGIN_ALERT = False

IS_IP_UPLOAD = False
IS_IPTABLE = False
IS_LESSON = False
IS_LIVE = False

IPTABLE__ALLOW = ['192.168.0.0/24']
IPTABLE_UPLOAD_ALLOW = []

ACCOUNT__LOGIN_KEY = 'username_or_email' # username, email, username_or_email, uid # !! not imperment
ACCOUNT__HIDE_INACTIVE = False
ACCOUNT__HIDE_ID_LIST = []
ACCOUNT__FORCE_ACTIVE_ID_LIST = [] # For OM

ORDER__IS_ASSIGN_REPORT = True
ORDER__IS_ENROLL_REPORT = True

TEST__MAX_CHOICE = 10

COURSE__MATERIAL_IS_SCORM = False

AXES_LOCK_OUT_AT_FAILURE = False

# TODO: Handle
IS_ADMIN_AUTHORITY_ADJUSTABLE = True


# Config View

VIEW_DASHBOARD = 'dashboard.views_dashboard.dashboard_view'

# CONFIG
_CONTEXT_CONFIG = {}
with open('%s/ondemand/config.json' % BASE_DIR) as file:
    _CONTEXT_CONFIG = json.load(file)
if os.path.exists('%s/ondemand/local_config.json' % BASE_DIR):
    with open('%s/ondemand/local_config.json' % BASE_DIR) as file:
        _CONTEXT_CONFIG.update(json.load(file))


def _get_config(domain, key):
    result = None
    domain = domain.split(':')[0]
    domain = domain.replace('www.', '')
    if domain in _CONTEXT_CONFIG:
        if key in _CONTEXT_CONFIG[domain]:
            result = _CONTEXT_CONFIG[domain][key]
    if result is None:
        for _key in _CONTEXT_CONFIG.keys():
            if _key.find('domain') != -1:
                if key in _CONTEXT_CONFIG[_key]:
                    result = _CONTEXT_CONFIG[_key][key]
                    break
        if result is None:
            if key in _CONTEXT_CONFIG['default']:
                result = _CONTEXT_CONFIG['default'][key]
    return result


CONFIG = _get_config

# CONTENT TYPE
_CACHED_CONTENT_TYPE = {}
_CACHED_CONTENT_TYPE_ID = {}


def _content_type(app, model):
    key = '%s_%s' % (app, model)
    if key in _CACHED_CONTENT_TYPE:
        return _CACHED_CONTENT_TYPE[key]

    from django.contrib.contenttypes.models import ContentType
    try:
        content_type = ContentType.objects.get(app_label=app, model=model)
    except:
        return None
    _CACHED_CONTENT_TYPE[key] = content_type
    return content_type


def _content_type_id(content_type_id):
    if content_type_id == -1 or content_type_id is None:
        return None
    key = '%s' % content_type_id
    if key in _CACHED_CONTENT_TYPE_ID:
        return _CACHED_CONTENT_TYPE_ID[key]
    from django.contrib.contenttypes.models import ContentType
    try:
        content_type = ContentType.objects.get(id=content_type_id)
    except:
        return None
    _CACHED_CONTENT_TYPE_ID[key] = content_type
    return content_type


FILE_UPLOAD_PERMISSIONS = 0o755
CONTENT_TYPE = _content_type
CONTENT_TYPE_ID = _content_type_id

# Media Server
NIMBLE_URL = 'https://live-s1.cocodemy.com:8443/content'
NIMBLE_HTTP_URL = 'http://live-s1.cocodemy.com:8081/content'
NIMBLE_LIVE_URL = 'https://live-s1.cocodemy.com:8443/live'

# Apple in-app-purchase
PURCHASE_APPLE_URL = 'https://buy.itunes.apple.com/verifyReceipt'
PURCHASE_APPLE_URL_SANDBOX = 'https://sandbox.itunes.apple.com/verifyReceipt'
PURCHASE_APPLE_PASSWORD = ''

# Google Analytic
GOOGLE_ANALYTIC_SERVICE_ID = ''

GOOGLE_ANALYTIC_SERVICE_ACCOUNT_EMAIL = 'pacrim@pacrim-connect.iam.gserviceaccount.com'
GOOGLE_SERVICE_KEY = 'key/Pacrim-Connect-2ccd16584cba.p12'

# Mailer
MAILER_SENDER = 'contact@conicle.com'
MAILER_SERVICE = 'DJANGO_SMTP'  # ['SMTP', 'MANDRILL', 'GOOGLE_APP', 'SES']
MAILER_PORT = 25
MAILER_DELAY = 2 # Send mail job delay

DATA_UPLOAD_MAX_MEMORY_SIZE = 26214400

MANDRILL_API_KEY = ''

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_SUBJECT_PREFIX = ''

# ALERT
ALERT_AUTH_GROUP_ID = []
SYSTEM_ADMIN_EMAIL = []

# Default
DEFAULT_EMAIL_SUFFIX = 'conicle.com'

IMAGE_SIZE = {
    'course': {
        'width': 400,
        'height': 400
    },
    'question': {
        'width': 400,
        'height': 400
    },
    'program': {
        'width': 400,
        'height': 400
    },
    'live': {
        'width': 600,
        'height': 364
    },
    'category': {
        'width': 600,
        'height': 200
    },
    'tutor': {
        'width': 400,
        'height': 400
    },
    'provider': {
        'width': 400,
        'height': 400
    },
    'banner': {
        'width': 800,
        'height': 360
    },
    'cover': {
        'width': 1600,
        'height': 400
    },
    'featured': {
        'width': 500,
        'height': 250
    }
}

BLEACH_ALLOW_TAG = ['p', 'br', 'div', 'span', 'strong', 'b', 'i', 'u', 'ul', 'li', 'ol', 'img', 'em']


def _image_help_text(type):
    try:
        width = IMAGE_SIZE[type]['width']
        height = IMAGE_SIZE[type]['height']
    except:
        width = 0
        height = 0
    return 'Image size %s x %s pixels.' % (width, height)


HELP_TEXT_IMAGE = _image_help_text

# MARK REMOVE #
MY_IP = '192.168.56.1'  # For gen stream sign [local]
VERSION = '1.2'


def get_base_url(request):
    if request.is_secure():
        scheme = 'https://'
    else:
        scheme = 'http://'
    return scheme + request.get_host()


BASE_URL = get_base_url

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 50
}

# END MARK REMOVE #

TIME_ACTIVE = datetime.timedelta(6 * 365 / 12)
MAX_USER = 1000
CONTENT_LIMIT = 30

ROLE_SUPER_ADMIN_ID = -1
ROLE_MANAGER_ID = -1

# SECURITY POLICY MAIN

# LENGTH PASSWORD (Char)
MINIMUM_LENGTH_PASSWORD = 8
MAXIMUM_LENGTH_PASSWORD = 32

# PASSWORD AGE TO FORCE SET NEW ONE (Day)
LIMIT_AGE_PASSWORD = 10000

# LINK RESET VALID (Day)

# STRENGTH PASSWORD BY MAX IS 4 OR MIN IS 0
LEVEL_STRENGTH_PASSWORD = 0

# SET PASSWORD CHECK USED BY DAY UNIT
LIMIT_PASSWORD_USED = 0


from .local_settings import *

if PROJECT in ['ais', 'cimb']:
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,
        'handlers': {
            'conicle': {
                'level': 'ERROR',
                'class': 'ondemand.loggers.LogHandler'
            }
        },
        'loggers': {
            'django.request': {
                'handlers': ['conicle'],
                'level': 'ERROR'
            },
            '*': {
                'handlers': ['conicle'],
                'level': 'ERROR'
            },
        }
    }
    if DEBUG:
        LOGGING = None


if IS_PRODUCTION and PROJECT == 'ais':
    from Crypto.Cipher import AES
    import base64

    cipher = AES.new('1C/zvi4xbXPaSCFyoseRBzZKtjhZ+Fu+', AES.MODE_ECB)
    DATABASES['default']['PASSWORD'] = cipher.decrypt(base64.b64decode(DATABASES['default']['PASSWORD'])).strip()
