from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

from .views_home import home_view

from account.views_login import login_view
from account.views_signup import signup_view

from .views_static import policies_view, support_view, about_view, roadmap_view
from .views_static import contact_view, user_report_view

from account.urls import urlpatterns_profile
from course.views_search import search_view as course_search_view
from tutor.views_detail import detail_view as tutor_detail_view

urlpatterns = [
    url(r'^$', home_view, name='home'),

    url(r'^login/$', login_view, name='login'),
    url(r'^signup/$', signup_view, name='signup'),

    #TODO: testing
    
    #API
    url(r'^api/', include('api.urls')), #TODO: testing

    #User Web
    url(r'^policies/$', policies_view, name='policies'), #TODO: testing
    url(r'^support/$', support_view, name='support'), #TODO: testing
    url(r'^roadmap/$', roadmap_view, name='roadmap'), #TODO: testing
    url(r'^about/$', about_view, name='about'), #TODO: testing

    url(r'^tutor/(\d+)/$', tutor_detail_view, name='tutor_detail'), #TODO: testing

    # Static template
    url(r'^contact/$', contact_view, name="contact"), #TODO: testing
    url(r'^report/problem/$', user_report_view, name="user_report"), #TODO: testing
    url(r'^search/$', course_search_view, name="search"), #TODO: testing
    url(r'^manual/', include('manual.urls', 'manual')), #TODO: testing

    url(r'^account/', include('account.urls', 'account')), #TODO: testing
    url(r'^profile/', include(urlpatterns_profile)), #TODO: testing

    url(r'^course/', include('course.urls', 'course')),
    url(r'^lesson/', include('lesson.urls', 'lesson')), #TODO: testing
    url(r'^question/', include('question.urls', 'question')), #TODO: testing
    url(r'^live/', include('live.urls', 'live')), #TODO: testing
    url(r'^task/', include('task.urls', 'task')), #TODO: testing
    url(r'^rating/', include('rating.urls', 'rating')), #TODO: testing
    url(r'^discussion/', include('discussion.urls')), #TODO: testing
    
    url(r'^category/', include('subject.urls', 'category')), #TODO: testing
    url(r'^subject/', include('subject.urls', 'subject')), #TODO: testing

    url(r'^console/', include('console.urls', 'console')), #TODO: testing
    url(r'^order/', include('order.urls', 'order')), #TODO: testing
    url(r'^promote/', include('promote.urls', 'promote')), #TODO: testing
    url(r'^package/', include('package.urls', 'package')), #TODO: testing
    url(r'^program/', include('program.urls', 'program')), #TODO: testing
    url(r'^dashboard/', include('dashboard.urls')), #TODO: testing
    url(r'^institute/', include('institute.urls', 'institute')), #TODO: testing
    url(r'^notification/', include('notification.urls', 'notification')), #TODO: testing
    url(r'^mailer/', include('mailer.urls', 'mailer')), #TODO: testing
    url(r'^inbox/', include('inbox.urls', 'inbox')), #TODO: testing
    url(r'^term/', include('term.urls', 'term')), #TODO: testing

    url(r'^admin/', include(admin.site.urls)), #TODO: testing

    url(r'^django-rq/', include('django_rq.urls')),
]

if settings.DEBUG:
    from django.views.static import serve
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}) #TODO: testing
    ]
    try:
        import debug_toolbar
        urlpatterns += url(r'^__debug__/', include(debug_toolbar.urls)) #TODO: testing
    except:
        pass
