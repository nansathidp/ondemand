from django.shortcuts import render

from package.models import Package

from highlight.cached import cached_web_banner


def promotion_view(request):
    debug = request.GET.get('debug', 0)
    try:
        package_list = Package.objects.filter(is_display=True, is_use=True).order_by('sort')
        banner = cached_web_banner(request.APP, 1)
    except:
        package = None
    if int(debug) == 1:
        template = 'main/_promotion.html'
    else:
        template = 'main/promotion.html'

    return render(request,
                  template,
                  {'ROOT_PAGE': 'promotion',
                   'package_list': package_list,
                   'banner': banner})


def promotion_sci_view(request):
    package_list = [package for package in Package.objects.filter(is_display=True, is_use=True, group=2)]
    return render(request,
                  'main/promotion_detail.html',
                  {'ROOT_PAGE': 'promotion',
                   'package_list': package_list})


def promotion_art_view(request):
    package_list = [package for package in Package.objects.filter(is_display=True, is_use=True, group=3)]
    return render(request,
                  'main/promotion_detail.html',
                  {'ROOT_PAGE': 'promotion',
                   'package_list': package_list})
