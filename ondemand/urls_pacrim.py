from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

from account.views_library import library_view

from account.views_login import login_view
from account.views_signup import signup_view
from account.views_forgot_password import forgot_password_view
from account.views_reset_password import reset_password_view

from .views_static import about_view, contact_view

from account.urls import urlpatterns_profile
from tutor.views_detail import detail_view as tutor_detail_view

urlpatterns = [
    url(r'^$', library_view, name='home'),

    url(r'^login/$', login_view, name='login'),
    url(r'^signup/$', signup_view, name='signup'),
    url(r'^forgot/password/$', forgot_password_view, name='forgot_password'),
    url(r'^reset/password/$', reset_password_view, name='reset_password'),

    # TODO: TEST

    # Account
    url(r'^account/', include('account.urls')),
    url(r'^profile/', include(urlpatterns_profile)),

    # Content
    url(r'^course/', include('course.urls')),
    url(r'^lesson/', include('lesson.urls')),
    url(r'^question/', include('question.urls')),
    url(r'^live/', include('live.urls')),
    url(r'^task/', include('task.urls')),
    url(r'^category/', include('category.urls')),
    url(r'^program/', include('program.urls')),
    url(r'^provider/', include('provider.urls')),
    url(r'^instructor/', include('tutor.urls')),
    url(r'^rating/', include('rating.urls')),
    url(r'^discussion/', include('discussion.urls')),

    # Order & Payment
    url(r'^order/', include('order.urls')),

    # Notify
    url(r'^notification/', include('notification.urls')),
    url(r'^mailer/', include('mailer.urls')),
    url(r'^inbox/', include('inbox.urls')),

    # Informative
    url(r'^about/$', about_view, name='about'),
    url(r'^term/', include('term.urls', 'term')),

    # Admin
    url(r'^console/', include('console.urls')),
    url(r'^dashboard/', include('dashboard.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^django-rq/', include('django_rq.urls')),
]

if settings.DEBUG:
    from django.views.static import serve
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT})
    ]
    try:
        import debug_toolbar
        urlpatterns += [
            url(r'^__debug__/', include(debug_toolbar.urls))
        ]
    except:
        pass
