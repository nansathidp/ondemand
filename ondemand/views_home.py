from django.shortcuts import render
from django.conf import settings

from course.models import Course
from question.models import Activity
from program.models import Program
from rating.models import Rating

from course.cached import pack_course
from .views_decorator import check_project
from course.cached import cached_home_course_feature, cached_course_popular
from lesson.cached import cached_lesson_home
# from tutor.cached import cached_tutor_feature
# from highlight.cached import cached_highlight_home_category, cached_home_highlight_event, cached_home_highlight_course, cached_web_banner
# from highlight.cached import cached_highlight_announcement


@check_project
def home_view(request):

    question_list = []
    program_list = []
    course_popular_list = []

    lesson_list = cached_lesson_home()
    highlight_list = cached_highlight_home_category()
    announcement = cached_highlight_announcement(request.APP)

    highlight_event_list = cached_home_highlight_event()
    banner_list = cached_web_banner(request.APP, 0)

    # For Click
    course_featured_list, course_season_list, course_trick_list = cached_home_highlight_course()

    # AIS
    if settings.PROJECT == 'ais' or settings.PROJECT == 'demo':
        course_list = Course.pull_latest_list(request.APP, 8)
        course_popular_list = Rating.popular_list(settings.CONTENT_TYPE('course', 'course'), 8)
    else:
        course_list = cached_home_course_feature(request.APP)

    # Unilever
    feature_tutor_list = cached_tutor_feature()
    #feature_subject_list = cached_subject_feature() #FIXME @kidsdev

    return render(request,
                  'home.html',
                  {'ROOT_PAGE': 'home',
                   'DISPLAY_SUBMENU': True,
                   'DISPLAY_CHAT': True,
                   'announcement': announcement,
                   'banner_list': banner_list,
                   'course_list': course_list,
                   'course_popular_list': course_popular_list,
                   'lesson_list': lesson_list,
                   'question_list': question_list,
                   'program_list': program_list,
                   'course_featured_list': course_featured_list,
                   'course_season_list': course_season_list,
                   'course_trick_list': course_trick_list,
                   # 'highlight_institute_list': highlight_institute_list,
                   'highlight_event_list': highlight_event_list,
                   'highlight_list': highlight_list,
                   'feature_tutor_list': feature_tutor_list,
                   #'feature_subject_list': feature_subject_list #FIXME @kidsdev
                  })
