from django.contrib.sessions.middleware import SessionMiddleware
from django.contrib.auth.middleware import AuthenticationMiddleware, SessionAuthenticationMiddleware
from django.contrib.messages.middleware import MessageMiddleware

class MySessionMiddleware(SessionMiddleware):
    def process_request(self, request):
        if request.path_info[0:5] == '/api/':
            return
        super(MySessionMiddleware, self).process_request(request)

    def process_response(self, request, response):
        if request.path_info[0:5] == '/api/':
            return response
        return super(MySessionMiddleware, self).process_response(request, response)

class MyAuthenticationMiddleware(AuthenticationMiddleware):
    def process_request(self, request):
        if request.path_info[0:5] == '/api/':
            return
        super(MyAuthenticationMiddleware, self).process_request(request)

    #def process_response(self, request, response):
    #    if request.path_info[0:5] == '/api/':
    #        return response
    #    return super(MyAuthenticationMiddleware, self).process_response(request, response)

class MySessionAuthenticationMiddleware(SessionAuthenticationMiddleware):
    def process_request(self, request):
        if request.path_info[0:5] == '/api/':
            return
        super(MySessionAuthenticationMiddleware, self).process_request(request)

    #def process_response(self, request, response):
    #    if request.path_info[0:5] == '/api/':
    #        return response
    #    return super(MySessionAuthenticationMiddleware, self).process_response(request, response)

class MyMessageMiddleware(MessageMiddleware):
    def process_request(self, request):
        if request.path_info[0:5] == '/api/':
            return
        super(MyMessageMiddleware, self).process_request(request)

    def process_response(self, request, response):
        if request.path_info[0:5] == '/api/':
            return response
        return super(MyMessageMiddleware, self).process_response(request, response)
