from django.test import TestCase
from django.urls import reverse
from django.conf import settings

PROJECT_LIST = ['ais', 'pacrim', 'demo']


class AnonymousTests(TestCase):
    def test_home(self):
        response = self.client.get(reverse('home'))
        if settings.PROJECT in PROJECT_LIST:
            self.assertEqual(response.status_code, 302)
        else:
            self.assertEqual(response.status_code, 200)

    def test_login(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)

    def test_signup(self):
        response = self.client.get(reverse('signup'))
        self.assertEqual(response.status_code, 200)

    def test_forgot_password(self):
        response = self.client.get(reverse('forgot_password'))
        self.assertEqual(response.status_code, 200)

    def test_resetpassword(self):
        response = self.client.get(reverse('reset_password'))
        if settings.PROJECT in PROJECT_LIST:
            self.assertEqual(response.status_code, 302)
        else:
            self.assertRedirects(response, reverse('home'))
