import logging
from django.views.debug import ExceptionReporter
from copy import copy
from django.db.utils import OperationalError
from django.utils import timezone

class LogHandler(logging.Handler):
    def __init__(self):
        logging.Handler.__init__(self)

    def format_subject(self, subject):
        return subject.replace('\n', '\\n').replace('\r', '\\r')

    def emit(self, record):
        try:
            request = record.request
            subject = '%s (%s IP): %s' % (
                record.levelname,
                ('internal' if request.META.get('REMOTE_ADDR') in settings.INTERNAL_IPS
                 else 'EXTERNAL'),
                record.getMessage()
            )
        except Exception:
            subject = '%s: %s' % (
                record.levelname,
                record.getMessage()
            )
            request = None
        subject = self.format_subject(subject)

        no_exc_record = copy(record)
        no_exc_record.exc_info = None
        no_exc_record.exc_text = None
        if record.exc_info:
            exc_info = record.exc_info
        else:
            exc_info = (None, record.getMessage(), None)
        reporter = ExceptionReporter(request, is_email=False, *exc_info)
        message = "%s\n\n%s" % (
            self.format(no_exc_record), reporter.get_traceback_text()
        )

        if reporter.exc_type and reporter.exc_type is OperationalError:
            log = '/app/log/learndi/db.log'
            with open(log, 'a+') as f:
                f.write('%s : %s\n' % (timezone.now(), reporter.exc_value))
                f.close()
        else:
            try:
                from log.models import Error
                Error.objects.create(
                    subject=subject,
                    level=record.levelname,
                    message=message
                )
            except:
                pass
        return
