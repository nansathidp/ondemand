from axes.decorators import watch_login
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.flatpages import views
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

from account.urls import urlpatterns_profile
from account.views_forgot_password import forgot_password_view, save_password_view
from account.views_login import login_view
from account.views_reset_password import reset_password_view
from account.views_signup import signup_view
from ais.views import home_view
from app.api import views as view_apple
from course.views_search import search_view as course_search_view
from pool.rest.views import PoolViesSet
from saml.views import acs
from .views_flatpage import flatpage_view

router = DefaultRouter()
router.register(r'pool', PoolViesSet)

urlpatterns = [
    url(r'^$', home_view, name='home'),

    url(r'^login/$', watch_login(login_view), name='login'),
    url(r'^signup/$', signup_view, name='signup'),
    url(r'^forgot/password/$', forgot_password_view, name='forgot_password'),
    url(r'^forgot/password/save/$', save_password_view, name='forgot_password_save'),
    url(r'^resetpassword/$', reset_password_view, name='reset_password'),
    # url(r'^about/$', about_view, name='about'),
    # url(r'^contact/$', contact_view, name='contact'),
    url(r'^search/$', course_search_view, name='search'),
    url(r'^apple-app-site-association', view_apple.views_apple, name='apple'),
    url(r'^api/', include('api.urls')),
    url(r'^instructor/', include('tutor.urls')),
    url(r'^account/oauth/$', acs),  # Production
    url(r'^saml2/acs/', acs),  # Staging
    url(r'^saml/', include('saml.urls')),  # LOGIN_URL
    url(r'^account/', include('account.urls')),
    url(r'^profile/', include(urlpatterns_profile)),
    url(r'^rating/', include('rating.urls')),
    url(r'^manual/', include('manual.urls')),
    url(r'^discussion/', include('discussion.urls')),

    url(r'^category/', include('category.urls')),

    url(r'^course/', include('course.urls')),
    url(r'^question/', include('question.urls')),
    url(r'^program/', include('program.urls')),
    url(r'^task/', include('task.urls')),
    url(r'^lesson/', include('lesson.urls')),

    url(r'^console/', include('console.urls')),
    url(r'^order/', include('order.urls')),
    url(r'^dashboard/', include('dashboard.urls')),
    url(r'^provider/', include('provider.urls')),

    url(r'^notification/', include('notification.urls')),
    url(r'^mailer/', include('mailer.urls')),
    url(r'^inbox/', include('inbox.urls')),

    url(r'^rest/docs', get_swagger_view(title='Conicle API Docs.')),
    url(r'^rest/', include(router.urls)),

    url(r'^django-admin/login/', watch_login(admin.site.login)),
    url(r'^django-admin/django-rq/', include('django_rq.urls')),
    url(r'^django-admin/', include(admin.site.urls)),

    url(r'^about/$', flatpage_view, name="about"),
    url(r'^contact/$', flatpage_view, name="contact"),
    url(r'^term/$', flatpage_view, name="term"),
]

if settings.DEBUG:
    from django.views.static import serve

    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT})
    ]
    try:
        import debug_toolbar

        urlpatterns += [
            url(r'^__debug__/', include(debug_toolbar.urls))
        ]
    except:
        pass