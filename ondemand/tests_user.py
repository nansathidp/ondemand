from django.test import TestCase
from django.urls import reverse

from account.models import Account

import random
import string


def _random():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(8))


class UserTests(TestCase):
    def setUp(self):
        self.client.force_login(Account.objects.create_user('%s@test.com' % _random(),
                                                            '123456'))

    def test_home(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        response = self.client.get(reverse('login'))
        self.assertRedirects(response, reverse('home'))

    def test_signup(self):
        response = self.client.get(reverse('signup'))
        self.assertRedirects(response, reverse('home'))

    def test_forgot_password(self):
        response = self.client.get(reverse('forgot_password'))
        self.assertEqual(response.status_code, 200)

    def test_resetpassword(self):
        response = self.client.get(reverse('reset_password'))
        self.assertRedirects(response, reverse('home'))
