from functools import wraps

from django.conf import settings
from django.contrib.auth import logout
from django.core.urlresolvers import reverse
from django.shortcuts import redirect

from category.models import Category
from provider.models import Provider


def check_project(view_func):
    def _decorator(request, *args, **kwargs):
        try:
            next_url = request.get_full_path()
        except:
            next_url = None
        project_list = ['pacrim', 'demo', 'ondemand', 'fwd', 'cimb', 'thanachart', 'mcdonalds']
        project_list = []
        if settings.PROJECT == 'ais':
            if not request.user.is_authenticated():
                return redirect(settings.LOGIN_URL)
        elif settings.PROJECT not in project_list:
            if not request.user.is_authenticated():
                # return redirect('%s?next=%s' % (reverse('login'), next_url))
                return redirect('login')  # VA Debug
        if request.user.is_authenticated():
            if not request.user.is_active:
                logout(request)
        response = view_func(request, *args, **kwargs)
        return response

    return wraps(view_func)(_decorator)


def check_ip(view_func):
    def _decorator(request, *args, **kwargs):
        from api.views_ais_privilege import get_client_ip
        import ipaddress
        request.is_upload = False
        ip = get_client_ip(request)
        for allow in settings.IPTABLE_UPLOAD_ALLOW:
            if ipaddress.ip_address(ip) in ipaddress.ip_network(allow):
                request.is_upload = True
        response = view_func(request, *args, **kwargs)
        return response
    return wraps(view_func)(_decorator)


def param_filter(view_func):
    def _decorator(request, *args, **kwargs):
        provider_id = request.GET.get('provider', 'all')
        request.provider = None
        if provider_id != 'all':
            try:
                request.provider = Provider.pull(int(provider_id))
            except:
                pass

        category_id = request.GET.get('category', 'all')
        request.category = None
        if category_id != 'all':
            try:
                request.category = Category.pull(int(category_id))
            except:
                pass

        request.level_id = request.GET.get('level', 'all')
        request.sort = request.GET.get('sort', 'latest')
        request.page = request.GET.get('page', 1)
        response = view_func(request, *args, **kwargs)
        return response

    return wraps(view_func)(_decorator)
