from django.contrib.auth import get_user_model, _get_user_session_key
from django.utils.functional import SimpleLazyObject
from django.conf import settings

from django.contrib.auth.models import AnonymousUser
from account.models import Session

from account.cached import cached_account
import hashlib


class ModelBackend(object):
    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()

        # Unilever
        if password is None:
            user = UserModel.objects.filter(email=kwargs['email']).first()
            if user is not None:
                return user
        elif username is None:
            username = kwargs.get(UserModel.USERNAME_FIELD)

        try:
            user = UserModel._default_manager.get_by_natural_key(username)
            # ClickForClever
            if password is not None and user.pair_password == hashlib.md5.new(password).hexdigest():
                user.set_password(password)
                user.save()
                return user
            else:
                if 'login_as' in kwargs:
                    return user
        except UserModel.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a non-existing user (#20760).
            UserModel().set_password(password)
        except:
            return None

    def get_user(self, user_id):
        try:
            # UserModel = get_user_model()
            # return UserModel.objects.get(pk=user_id)
            return cached_account(user_id)
        except:
            return None

    def user_can_authenticate(self, user):
        is_active = getattr(user, 'is_active', None)
        return is_active or is_active is None

        # def has_perm(self, user_obj, perm, obj=None):
        #    print(self, perm)
        #    return False


class AuthenticationMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try:
            user_id = _get_user_session_key(request)
            user = cached_account(user_id)
            if getattr(settings, 'SESSION_SINGLE', False):
                Session.push(user, request.session.session_key)
            if user is None:
                request.user = SimpleLazyObject(lambda: AnonymousUser())
            else:
                request.user = SimpleLazyObject(lambda: user)
        except:
            request.user = SimpleLazyObject(lambda: AnonymousUser())

        response = self.get_response(request)
        return response


class UidBackend(object):
    def authenticate(self, username=None, password='', **kwargs):
        UserModel = get_user_model()

        uid = kwargs.get('email', None)
        if uid is None:
            uid = username # /admin/

        if uid is None or len(uid) == 0:
            return None

        user = UserModel.objects.filter(uid=uid).first()
        if 'login_as' in kwargs:
            return user

        if user and user.check_password(password) and self.user_can_authenticate(user):
            return user
        elif user and password is None and self.user_can_authenticate(user):
            return user
        else:
            return None

    def get_user(self, user_id):
        try:
            return cached_account(user_id)
        except:
            return None

    def user_can_authenticate(self, user):
        is_active = getattr(user, 'is_active', None)
        return is_active or is_active is None
