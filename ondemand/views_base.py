from django.conf import settings
from django.shortcuts import render
from rating.models import Rating

from api.views_api import STATUS_MSG


def response(request, code):
    return render(request,
                  'response.html',
                  {'response': STATUS_MSG[code],
                   'code': code})


def content_sort(request, query_set, content_type=None):

    # Sort type 1 : Latest
    # Sort type 2 : A-Z
    # Sort type 3 : Z-A
    # Sort type 4 : Popular

    sort = request.GET.get('sort', '').lower()
    if sort == 'latest':
        query_set = query_set.order_by('-timestamp')
    elif sort == 'atoz':
        query_set = query_set.order_by('name')
    elif sort == 'ztoa':
        query_set = query_set.order_by('-name')
    elif sort == 'popular':
        if content_type is None:
            content_type = request.GET.get('content_type', None)
            _content_type = None
            if content_type == 'course':
                _content_type = settings.CONTENT_TYPE('course', 'course')
            elif content_type == 'program':
                _content_type = settings.CONTENT_TYPE('program', 'program')
        else:
            _content_type = content_type
            
        if _content_type is not None:
            query_set = Rating.objects.values_list('content', flat=True) \
                                      .filter(content_type=_content_type,
                                              content__in=query_set) \
                                      .order_by('-weight')
    else:
        query_set = query_set.order_by('sort', '-timestamp')
    return query_set


