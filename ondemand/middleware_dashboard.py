from django.shortcuts import redirect
from django.conf import settings


from account.cached import cached_auth_group
from provider.cached import cached_provider_account, cached_provider
if settings.IS_SUB_PROVIDER:
    from subprovider.cached import cached_sub_provider_account, cached_sub_provider


class MiddlewareDashboard(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.path.find('/dashboard/') == 0 and request.path.find('/dashboard/login/') == -1:
            if not request.user.is_authenticated:
                return redirect('dashboard:login')
            
        if request.path.find('/rest/') == 0:
            is_permission = False
        elif request.path.find('/api/') == 0:
            is_permission = False
        else:
            is_permission = True

        # if  is_permission and request.path.find('/dashboard/login/') == -1:
        #     if not request.user.is_authenticated:
        #         return redirect('dashboard:login')
        
        if is_permission:
            ref = request.META.get('HTTP_REFERER', '') 
            if ref.find('/dashboard/') != -1 and request.path.find('/dashboard/login/') == 0:
                if not request.user.is_authenticated:
                    is_permission = False
            elif request.path.find('/login/') == 0:
                is_permission = False

        if is_permission and not request.user.is_authenticated:
            request.DASHBOARD_GROUP_LIST = []
            request.DASHBOARD_GROUP = None
            request.DASHBOARD_PROVIDER_LIST = []
            request.DASHBOARD_PROVIDER = None
            if settings.IS_SUB_PROVIDER:
                request.DASHBOARD_SUB_PROVIDER_LIST = []
                request.DASHBOARD_SUB_PROVIDER = None
        elif is_permission:
            request.DASHBOARD_GROUP_LIST = request.user.get_auth_group_list()
            group_id = request.session.get('dashboard_group_id', None)
            if group_id is None:
                try:
                    request.DASHBOARD_GROUP = request.DASHBOARD_GROUP_LIST[0]
                    request.session['dashboard_group_id'] = request.DASHBOARD_GROUP.id
                except:
                    request.DASHBOARD_GROUP = None
            else:
                request.DASHBOARD_GROUP = cached_auth_group(group_id)
                if request.DASHBOARD_GROUP is not None:
                    request.session['dashboard_group_id'] = request.DASHBOARD_GROUP.id

            request.DASHBOARD_PROVIDER_LIST = cached_provider_account(request.user.id)
            provider_id = request.session.get('dashboard_provider_id', None)
            if provider_id is None:
                try:
                    request.DASHBOARD_PROVIDER = request.DASHBOARD_PROVIDER_LIST[0]
                    request.session['dashboard_provider_id'] = request.DASHBOARD_PROVIDER.id
                except:
                    request.DASHBOARD_PROVIDER = None
            else:
                request.DASHBOARD_PROVIDER = cached_provider(provider_id)
                if request.DASHBOARD_PROVIDER is not None:
                    request.session['dashboard_provider_id'] = request.DASHBOARD_PROVIDER.id

            if settings.IS_SUB_PROVIDER:
                request.DASHBOARD_SUB_PROVIDER_LIST = cached_sub_provider_account(request.user.id)
                sub_provider_id = request.session.get('dashboard_sub_provider_id', None)
                if sub_provider_id is None:
                    try:
                        request.DASHBOARD_SUB_PROVIDER = request.DASHBOARD_SUB_PROVIDER_LIST[0]
                        request.session['dashboard_sub_provider_id'] = request.DASHBOARD_SUB_PROVIDER.id
                    except:
                        request.DASHBOARD_SUB_PROVIDER = None
                else:
                    request.DASHBOARD_SUB_PROVIDER = cached_sub_provider(sub_provider_id)
                    if request.DASHBOARD_SUB_PROVIDER is not None:
                        request.session['dashboard_sub_provider_id'] = request.DASHBOARD_SUB_PROVIDER.id
        else:
            pass

        response = self.get_response(request)
        return response