from django.shortcuts import render
from django.conf import settings

from mailer.models import Mailer
from ondemand.views_decorator import check_project


@check_project
def contact_view(request):
    is_success = False
    if request.method == "POST":
        site_url = request.get_host()
        contact_email = settings.CONFIG(request.get_host(), 'CONTACT_EMAIL')
        Mailer.send_contact_email(request.POST, contact_email, site_url)
        is_success = True
    return render(request,
                  'static/contact.html', {'ROOT_PAGE': 'contact',
                                          'DISPLAY_SUBMENU': True,
                                          'success': is_success,
                                          })

@check_project
def user_report_view(request):
    is_success = False
    if request.method == "POST":
        contact_email = settings.CONFIG(request.get_host(), 'CONTACT_EMAIL')
        Mailer.send_contact_email(request.POST, contact_email, request.get_host())
        is_success = True
    return render(request,
                  'static/user_report.html', {'ROOT_PAGE': 'contact',
                                              'DISPLAY_SUBMENU': True,
                                              'success': is_success,
                                              })

@check_project
def support_view(request):
    return render(request,
                  'static/support.html', {})

@check_project
def about_view(request):
    return render(request,
                  'static/about.html',
                  {'ROOT_PAGE': 'about'})

@check_project
def policies_view(request):
    return render(request,
                  'static/policies.html', {
                      'ROOT_PAGE': 'policy',
                  })

@check_project
def roadmap_view(request):
    return render(request,
                  'static/roadmap.html', {
                      'DISPLAY_SUBMENU': True,
                      'ROOT_PAGE': 'roadmap',
                  })

@check_project
def manual_learn_view(request):
    return render(request,
                  'static/terms.html', {})

@check_project
def analytic_view(request):
    return render(request,
                  'static/analytic.html', {})

@check_project
def activity_view(request):
    return render(request, 'static/activity.html',
                  {
                      'IMAGE_URL': '%s/clickforclever/images/activity/activity_share.jpg' % settings.STATIC_URL,
                      'TITLE': 'สอบติดรับสิทธิ์บินฟรีตลอดปี',
                      'DESC': 'โพสต์ของใครที่เพื่อนๆ มากดไลค์ กดแชร์มากที่สุด เข้ารอบ 20 คนสุดท้ายที่ได้รับตั๋วทันที',
                  }
                  )

@check_project
def app_download_view(request):
    return render(request,
                  'course/video_download_mobile.html')


