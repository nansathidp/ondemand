from django.contrib.flatpages.models import FlatPage
from django.shortcuts import render


def flatpage_view(request):
    flatpage = None
    if request.path in ['/about/', '/contact/', '/term/']:
        root_page = request.path
        flatpage = FlatPage.objects.filter(url=request.path).first()
        if flatpage is None:
            flatpage = FlatPage.objects.create(url=request.path)
    return render(request,
                  'flatpages/default.html',
                  {
                      'flatpage': flatpage,
                      'ROOT_PAGE': root_page.replace('/', ''),
                  })
