import time

from django.conf import settings
from django.utils.translation import activate

from account.cached import cached_onboard_account
from category.models import Category
from provider.models import Provider


def get_inbox_count(request):
    from inbox.models import Inbox
    if request.user.is_authenticated:
        return Inbox.pull_notification_count(request.user.id).count
    else:
        return 0


def get_onboard_status(request):
    if not settings.IS_ONBOARD:
        return False

    if request.user.is_authenticated:
        return cached_onboard_account(request.user)
    else:
        return False


def templates_config(request):
    if request.path_info[0:5] == '/api/':
        return {}

    if request.is_secure():
        scheme = 'https://'
    else:
        scheme = 'http://'

    domain = request.get_host().split(':')[0]
    if domain.find('webview') == 0:
        is_webview = True
    else:
        is_webview = False

    result = {
        'APP': request.APP,
        'PROJECT': settings.PROJECT,
        'APP_NAME': settings.CONFIG(domain, 'APP_NAME'),
        'APP_THEME': settings.CONFIG(domain, 'APP_THEME'),
        'DEBUG': settings.DEBUG,
        'BASE_URL': scheme + request.get_host(),
        'DOMAIN': request.get_host(),
        'VERSION': settings.VERSION,
        'RANDOM': time.time(),
        'BASE_DESC': settings.CONFIG(domain, 'BASE_DESC'),
        'COIN_VOLUME': 0,
        'INBOX_COUNT': get_inbox_count(request),
        'IS_ONBOARD': settings.IS_ONBOARD,
        'IS_SUB_PROVIDER': settings.IS_SUB_PROVIDER,
        'IS_ORGANIZATION': settings.IS_ORGANIZATION,
        'IS_CONTENT_ACCESSIBILITY': settings.IS_CONTENT_ACCESSIBILITY,
        'IS_WEBVIEW': is_webview,
        'IS_CONTENT_CREDIT': settings.IS_CONTENT_CREDIT,
        'IS_PRICE': settings.IS_PRICE,
        'IS_LESSON': settings.IS_LESSON,
        'IS_LIVE': settings.IS_LIVE,
        'IS_SIGNUP': settings.IS_SIGNUP,
    }

    result['BASE_NAME'] = settings.CONFIG(domain, 'BASE_NAME')
    result['BASE_TAGNAME'] = settings.CONFIG(domain, 'BASE_TAGNAME')
    result['BASE_KEYWORD'] = settings.CONFIG(domain, 'BASE_KEYWORD')
    result['FB_APP_ID'] = settings.CONFIG(domain, 'FB_APP_ID')
    result['FB_PAGE_URL'] = settings.CONFIG(domain, 'FB_PAGE_URL')
    result['TWITTER_ACCOUNT'] = settings.CONFIG(domain, 'TWITTER_ACCOUNT')
    result['GOOGLE_API_KEY'] = settings.CONFIG(domain, 'GOOGLE_API_KEY')
    result['PLATFORM_ID'] = settings.CONFIG(domain, 'PLATFORM_ID')
    result['CONTACT_EMAIL'] = settings.CONFIG(domain, 'CONTACT_EMAIL')
    result['GOOGLE_ANALYTICS_KEY'] = settings.CONFIG(domain, 'GOOGLE_ANALYTICS_KEY')
    result['GOOGLE_SITE_KEY'] = settings.CONFIG(domain, 'GOOGLE_SITE_KEY')
    result['CONTACT_EMAIL'] = settings.CONFIG(domain, 'CONTACT_EMAIL')
    result['APP_STORE_URL'] = settings.CONFIG(domain, 'APP_STORE_URL')
    result['PLAY_STORE_URL'] = settings.CONFIG(domain, 'PLAY_STORE_URL')
    result['INAPP_PREFIX'] = settings.CONFIG(domain, 'INAPP_PREFIX')
    result['FB_PIXEL_ID'] = settings.CONFIG(domain, 'FB_PIXEL_ID')
    result['MPAY_SID'] = settings.CONFIG(domain, 'MPAY_SID')
    result['MPAY_MERCHANT_ID'] = settings.CONFIG(domain, 'MPAY_MERCHANT_ID')
    result['MPAY_SECRET_KEY'] = settings.CONFIG(domain, 'MPAY_SECRET_KEY')

    activate(settings.LANGUAGE_CODE)

    if request.path.find('/dashboard/') == 0 and request.path.find('/dashboard/login/') == -1:
        try:
            result['IMAGE_SIZE'] = settings.IMAGE_SIZE
            result['IMAGE_SIZE'] = settings.IMAGE_SIZE
            result['ORDER__IS_ASSIGN_REPORT'] = settings.ORDER__IS_ASSIGN_REPORT
            result['ORDER__IS_ENROLL_REPORT'] = settings.ORDER__IS_ENROLL_REPORT
            result['DASHBOARD_GROUP'] = request.DASHBOARD_GROUP
            result['DASHBOARD_GROUP_LIST'] = request.DASHBOARD_GROUP_LIST
            result['DASHBOARD_PROVIDER'] = request.DASHBOARD_PROVIDER
            result['DASHBOARD_PROVIDER_LIST'] = request.DASHBOARD_PROVIDER_LIST
            result['IS_CAREER_LEVEL'] = settings.IS_CAREER_LEVEL
            if settings.IS_SUB_PROVIDER:
                result['DASHBOARD_SUB_PROVIDER'] = request.DASHBOARD_SUB_PROVIDER
                result['DASHBOARD_SUB_PROVIDER_LIST'] = request.DASHBOARD_SUB_PROVIDER_LIST
        except:
            pass
    else:
        result.update({
            'HEADER_COURSE_CATEGORY': Category.pull_list(),
            'HEADER_PROVIDER': Provider.pull_list(),
            'IS_ONBOARD_ACTIVE': get_onboard_status(request)})

    return result


# Copy From django.contrib.auth.context_processors
class PermLookupDict(object):
    def __init__(self, user, group, app_label):
        self.user = user
        self.group = group
        self.app_label = app_label

    def __repr__(self):
        return str(self.user.get_all_permissions())

    def __getitem__(self, perm_name):
        return self.user.has_perm("%s.%s" % (self.app_label, perm_name), self.group)

    def __iter__(self):
        # To fix 'item in perms.someapp' and __getitem__ interaction we need to
        # define __iter__. See #18979 for details.
        raise TypeError("PermLookupDict is not iterable.")

    def __bool__(self):
        return self.user.has_module_perms(self.app_label)

    def __nonzero__(self):      # Python 2 compatibility
        return type(self).__bool__(self)


class PermWrapper(object):
    def __init__(self, user, group, path, is_superuser):
        self.user = user
        self.group = group
        self.path = path
        self.is_superuser = is_superuser

    def __getitem__(self, app_label):
        return PermLookupDict(self.user, self.group, app_label)

    def __iter__(self):
        # I am large, I contain multitudes.
        raise TypeError("PermWrapper is not iterable.")

    def __contains__(self, perm_name):
        """
        Admin (path) and SuperUser
        """
        if self.path.find('/admin/') == 0 and self.is_superuser:
            return True
        """
        Lookup by "someapp" or "someapp.someperm" in perms.
        """
        if '.' not in perm_name:
            # The name refers to module.
            return bool(self[perm_name])
        app_label, perm_name = perm_name.split('.', 1)
        return self[app_label][perm_name]


def perms(request):
    if hasattr(request, 'user'):
        user = request.user
        is_superuser = user.is_superuser
    else:
        from django.contrib.auth.models import AnonymousUser
        user = AnonymousUser()
        is_superuser = False

    if request.path.find('/dashboard/') == 0 and request.path.find('/dashboard/login/') == -1:
        group = request.DASHBOARD_GROUP
    else:
        group = None
    return {
        'perms': PermWrapper(user, group, request.path, is_superuser),
    }
