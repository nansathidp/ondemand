from app.cached import cached_app

class MiddlewareApp(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Axes set to is_ajax() is True
        if request.path == '/api/v5/account/login/':
            request.META['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'
        request.APP = cached_app(request.get_host())
        response = self.get_response(request)

        return response
