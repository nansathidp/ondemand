from django.conf.urls import url
from rest_framework import routers

from ais.views_loadtest import ais_loadtest_home_view, ais_loadtest_course_view
from pool.rest.views import PoolViesSet
from .urls import urlpatterns

router = routers.DefaultRouter()
router.register(r'pool', PoolViesSet)

urlpatterns += [
    url(r'^ais-loadtest/home/$', ais_loadtest_home_view),
    url(r'^ais-loadtest/course/(\d+)/$', ais_loadtest_course_view),
]
