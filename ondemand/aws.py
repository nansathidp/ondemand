import os, base64
import json, rsa, time

def signed_url(resource, timeout=60):
    expires = int(time.time()) + timeout
    policy = {
        'Statement': [{
            'Resource': resource,
            'Condition': {
                'DateLessThan': {
                    'AWS:EpochTime': expires
                }
            }
        }]
    }

    policy = json.dumps(policy, separators=(',',':'))
    sig = rsa.PrivateKey.load_pkcs1("""-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAg76cWZ6rHNUFySmJSqT/W3/0llvlUOv8UUeTzhDwoa3PeRdU
Hqra4uIS4M5yG9w5w1odIttm0F/CjuBQbVTKhM4Vch+8NV9ihpod8iBWgY83rfLc
AZvMoAOKMX1zE9DFMnHeEROqWzhDYxaR5PPp4zHNdr9ytfIQj5k1yIBDOifXlook
n2mTiDZSjYRcE4flw1L5/RQgDXPgtu9Lc+RxH7G4uK3Epp2reBCQvkVBmrrqFrWD
V3EFEO7Qq3tjQ4TplpJdp1mZFey7X1fCQ39PHXL2SdynZC52m5i/FdS77/affMd8
ARGYc9uX34tQTCMbD3vgyB7sqP1O5xAiNu711wIDAQABAoIBADi8Glrh4LCe8Ens
EErL+Ygu92bVFlEd/2AFYhqI4wfNcR6ltD0suOyiDR0w6qUi1uFQ6mE7ePBdfMzI
vvoGXNyKX2PxVLzrYeOzdRfzYy2SPpkzkBlFPf7e52Fes/MhofqSXN6fyGpWNXbz
nkqp29xSczgjREQkhYtBoI7C5bCiF0kuiUF4kodBYD9m8kqEOQVamn23LemdiJ7e
Bc6f8jyMhy6i1hiJAa/dHvMEWLvHwT/43yj+OJjlYbbwKGQeQ2LDIQqafTVc49Gx
zOBk5fWWKWQ9qEIpkdvWXknKYYWIpwbFm5DfMTy3JIsEV2BYEl63yTnRz+e4gs4K
1NOFc3kCgYEA/cbe2RTRlbuSYustRrjcjqz86FX0Vfh7sqtlaU3OyMar+52a5FQ3
kSlcMefMxlqj9/s1eDDBFdwR7FTO7jSGY5ScYs4eY8pertf9Y3Wm1ZdTEF974P06
RELe07nla/SUgxF8ZC60mB8xBN0FzRF0DsmUmY0YGWFLjb//wM8LejsCgYEAhOYQ
6xWaO1qo7e5OCKIKk6fVEWxU2Mc2SxXmhkjmXVtBhDjxb3GZz/TvNpcXb+uFFLSJ
21UB5nPJMbCcJ4U7nAfoETy9fbwC8LjT3bWVcIx2v9xTw2Ie7ZsTZArPatTua1a0
039cOrNzZ4hunWRJYMSLw+s5GN/Ca+k2+ibb3RUCgYADYmWSa46n+opnO0+tQ8Xd
OT1eM9dAVkIADf0aPGlnkA63GlD4fo8HJpAOMlMuygKpyfXalnvcuMScbD0ePCEW
ruKvBqStHAPLPSIp82W4L35yF3PlejsOsf2awT3oBKj+XntUMrNB3BihQflT1MnB
cLrEmXcaz4tcZpUYkypKBQKBgESDPtGSz3It6MzQf9Yfg4hfi45x5MPk326SGce8
qVYTh5Ecsgsb64mz4eaoueJu7qqf8DnsdxBRPLWrMLWHEBX+VXsdbyr01Cc5ga/Y
vvPI1idO9LfTS9J+kd6PXCLdJxSbHJm0HN3+SU5knXHyk4mZidw01binEloDXt5T
TiGJAoGASQ909I9K4o5eaaLIY0mF0//VqInhUj/AXeoei/OwbcseJhDnMSkLv24M
PYK5NUB3axbbLK/RNihdK3jOfwZdSrxp24b8+bERHP4LhabZ2VnbGeMcRE5mYywE
dGtW4jaU6d3sqIalqTY/ypvNnC0yYFY21fXVn3U9CpNKqL/bZUQ=
-----END RSA PRIVATE KEY-----""")
    sig = rsa.sign(policy, sig, 'SHA-1')
    sig = base64.b64encode(sig).replace('+', '-').replace('=', '_').replace('/', '~')

    return {'href': 'mp4:%s?Expires=%s&Signature=%s&Key-Pair-Id=%s' % (resource, expires, sig, 'APKAIDUW6DG4M56PV74A'),
            'rtmp': 'rtmp://s2cgo4064hf36y.cloudfront.net/cfx/st',
            'rtmpt': 'rtmpt://s2cgo4064hf36y.cloudfront.net/cfx/st',
            'rtmpe': 'rtmpe://s2cgo4064hf36y.cloudfront.net/cfx/st',
            'rtmpte': 'rtmpte://s2cgo4064hf36y.cloudfront.net/cfx/st'}
