from django.shortcuts import redirect
from django.core.urlresolvers import reverse


def _redirect_url(request, url):
    domain = request.get_host()
    if "ais" in domain:
        subdomain = "webviewais"
        domain = 'cocodemy.com'
    else:
        subdomain = "webview"

    return 'https://%s.%s%s?mobile=1' % (subdomain, domain, url)


def payment_info_view(request):
    return redirect(_redirect_url(request, reverse('manual:payment')))
