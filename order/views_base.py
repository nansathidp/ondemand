from django.conf import settings


def url_scheme(request):
    return request.is_secure() and "https://" or "http://"


def get_mobile_url(request, url, token, uuid, store):
    url = '%s%s%s?code=%s&key=%s&token=%s&uuid=%s&store=%s' % (url_scheme(request),
                                                               request.get_host(),
                                                               url,
                                                               request.code,
                                                               request.key,
                                                               token,
                                                               uuid,
                                                               store)
    return url


def get_payment_url(request, order, token, uuid, store):
    from django.core.urlresolvers import reverse
    if settings.PROJECT == 'unilever':
        return get_mobile_url(request, reverse('order:webview_test_payment_success', args=[order.id]), token, uuid, store)
    else:
        return get_mobile_url(request, reverse('order:webiew_payment_list', args=[order.id]), token, uuid, store)