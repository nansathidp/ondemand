from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.utils import timezone

from order.models import Order
from course.models import Course
# from package.models import Package
# from event.models import Event
from promote.models import Promote
from coin.models import Coin

from coin.cached import cached_leverage


@login_required
def cart_view(request, order_id):
    order = get_object_or_404(Order, id=order_id, account=request.user)
    #Debug
    #order.cal_price()
    
    order_list = Order.objects.filter(account=request.user, status=1)
    course_id_list = []
    package_id_list = []
    item_list = []
    course_list = []
    package_list = []
    coin_list = []
    now = timezone.now()
    is_coin_pay = False

    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_package = settings.CONTENT_TYPE('package', 'package')
    content_type_coin = settings.CONTENT_TYPE('coin', 'coin')

    for item in order.item_set.select_related('content_type').all():
        d = {}
        d['id'] = item.id
        d['content_id'] = item.content
        d['item_obj'] = item
        d['item_amount'] = item.amount
        d['item_price'] = item.price
        d['item_discount'] = item.discount
        d['is_allow_promote'] = item.is_allow_promote
        if item.content_type_id == content_type_course.id:
            course_id_list.append(item.content)
            d['content_type'] = content_type_course
        elif item.content_type_id == content_type_package.id:
            package_id_list.append(item.content)
            d['content_type'] = content_type_package
        elif item.content_type_id == content_type_coin.id:
            d['content_type'] = content_type_coin
        item_list.append(d)

    ## Start Move to Step Change Price in Other Item at 2016-01-15 ##
    is_update_price = False
    for item in item_list:
        if 'content_type' in item and item['content_type'] == content_type_course:
            for course_id in course_id_list:
                if item['content_id'] == course_id:
                    course = Course.pull(course_id)
                    if course is None:
                        continue
                    course.cached_price() # TODO cleanup
                    if course.price_web != item['item_price'] or course.price_web_discount != item['item_discount']:
                        item_obj = item['item_obj']
                        item_obj.price = course.price_web
                        item_obj.discount = course.price_web_discount
                        item_obj.save(update_fields=['price', 'discount'])
                        is_update_price = True
                    course.item_id = item['id']
                    course.content_id = item['content_id']
                    course.item_obj = item['item_obj']
                    course.item_amount = item['item_amount']
                    course.item_price = course.price_web
                    course.item_discount = course.price_web_discount
                    course.is_allow_promote = item['is_allow_promote']
                    course.content_type = item['content_type']
                    course_list.append(course)
                    break
        elif 'content_type' in item and item['content_type'] == content_type_package:
            package = Package.objects.get(id=item['content_id'])
            item['obj'] = package
            if package.price != item['item_price'] or package.discount != item['item_discount'] or package.is_allow_promote != item['is_allow_promote']:
                item_obj = item['item_obj']
                item_obj.price = item['item_price'] = package.price
                item_obj.discount = item['item_discount'] = package.discount
                item_obj.is_allow_promote = item['is_allow_promote'] = package.is_allow_promote
                item_obj.save(update_fields=['price', 'discount', 'is_allow_promote'])
                is_update_price = True
            package_list.append(item)
        elif 'content_type' in item and item['content_type'] == content_type_coin:
            try:
                coin = Coin.objects.get(id=item['content_id'])
            except:
                order.item_set.filter(id=item['content_id']).delete()
                continue
            item['obj'] = coin
            if coin.type == 1:
                pass
            elif coin.price != item['item_price'] or coin.discount != item['item_discount'] or coin.is_allow_promote != item['is_allow_promote']:
                item_obj = item['item_obj']
                item_obj.price = item['item_price'] = coin.price
                item_obj.discount = item['item_discount'] = coin.discount
                item_obj.is_allow_promote = item['is_allow_promote'] = coin.is_allow_promote
                item_obj.save(update_fields=['price', 'discount', 'is_allow_promote'])
                is_update_price = True
            coin_list.append(item)
    if is_update_price:
        order.cal_price()
    ## End Move to Step Change Price in Other Item at 2016-01-15 ##

    if request.method == 'POST' and 'redeem' in request.POST:
        code = request.POST.get('code', '')
        order.push_promote_code(request.user, code)

    promote_list = []
    p = order.price
    for promote in order.promote_set.select_related('code', 'code__promote').all():
        d = {}
        d['obj'] = promote
        d['code_obj'] = promote.code
        d['promote_obj'] = promote.promote_item
        if promote.promote_item.discount_type == 2:
            d['percent_discount'] = p*(promote.promote_item.discount/100.0)
            p -= d['percent_discount']
        promote_list.append(d)

    #Promote Code (Facebook Share)
    is_show_promote = True
    promote = Promote.pull_one()
    try:
        event = Event.objects.filter(is_display=True, time_end__gte=now).order_by('?')[0]
    except:
        event = None
    if promote is None:
        is_show_promote = False
    else:
        if event is None:
            is_show_promote = False

    content_type_coin = settings.CONTENT_TYPE('coin', 'coin')
    if not order.item_set.filter(content_type_id=content_type_coin.id).exists():
        is_coin_pay = True

    leverage = cached_leverage(request.APP)
    param = {'order': order,
             'order_list': order_list,
             'course_list': course_list,
             'package_list': package_list,
             'promote_list': promote_list,
             'coin_list': coin_list,
             'is_show_promote': is_show_promote,
             'is_coin_pay': is_coin_pay,
             'is_coin_valid': False,
             'coin_balance': order.net*leverage.value - 0,
             'promote': promote,
             'event': event}
    if event is not None:
        try:
            param['IMAGE_URL'] = event.image.url
        except:
            param['IMAGE_URL'] = None
        param['TITLE'] = event.name
        param['DESC'] = event.desc

    return render(request,
                  'order/cart.html',
                  param)
