from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from order.models import Promote

@login_required
def promote_remove_view(request, promote_id):
    promote = get_object_or_404(Promote.objects.select_related('order', 'code'), id=promote_id, order__account=request.user)
    order = promote.order
    code = promote.code
    if code is not None:
        code.timestamp_use = None
        code.save(update_fields=['timestamp_use'])
    promote.delete()
    order.cal_price()
    return redirect('order:home')

