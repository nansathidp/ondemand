from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.conf import settings

from order.models import Order
from aismpay.models import OtpRequest

@login_required
def aismpay_otprequest_view(request, order_id):
    order = get_object_or_404(Order, id=order_id, account=request.user)
    content_type = settings.CONTENT_TYPE('order', 'order')
    result = None
    if request.method == 'POST':
        tel_input = request.POST.get('tel', None)
        tel = tel_input.replace('-', '').replace(' ', '')
        if tel[0] == '+':
            tel = tel[1:]
        elif tel[0] == '0':
            tel = '66%s'%tel_input[1:]
        is_success, otp_request = OtpRequest.send(request.user, tel, tel_input, content_type, order.id)
        if is_success:
            return redirect('order:aismpay_otpconfirm', otp_request.id)
        else:
            result = otp_request.status
    return render(request,
                  'order/aismpay_otprequest.html',
                  {'result': result})
