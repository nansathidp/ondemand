import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings

from order.models import Item

from assignment.job.assignment_delete import _delete_order_item

def _item_delete(item_id):
    item = Item.objects.filter(id=item_id).first()
    if item is None:
        return None
    _delete_order_item(item)
        
