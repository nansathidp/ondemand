from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.conf import settings

from ondemand.views_base import response
from order.models import Order
from aismpay.models import OtpRequest

from account.cached import cached_api_account_auth

def webview_aismpay_otprequest_view(request, order_id):
    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)

    if uuid is None:
        return response(request, 202)
    if token is None:
        return response(request, 204)

    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return response(request, 703)
    except:
        return response(request, 209)

    account = cached_api_account_auth(token, uuid)
    order = get_object_or_404(Order, id=order_id, account=account)
    content_type = settings.CONTENT_TYPE('order', 'order')
    result = None
    if request.method == 'POST':
        tel_input = request.POST.get('tel', None)
        if len(tel_input) >= 10:
            tel = tel_input.replace('-', '').replace(' ', '')
            if tel[0] == '+':
                tel = tel[1:]
            elif tel[0] == '0':
                tel = '66%s' % tel_input[1:]
            is_success, otp_request = OtpRequest.send(account, tel, tel_input, content_type, order.id)
            if is_success:
                return redirect('%s?uuid=%s&token=%s&store=%s'%(reverse('order:webview_aismpay_otpconfirm', args=(otp_request.id,)), uuid, token, store))
            else:
                result = otp_request.status
    return render(request,
                  'order/webview_aismpay_otprequest.html',
                  {'result': result})
