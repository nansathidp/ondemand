from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from account.models import Account
from course.models import Course
from question.models import Activity
from department.models import Department

def assigment_result_view(request):
    if not request.user.has_perm('order.add_order'):
        raise PermissionDenied

    type = request.session.get('assigment_type', None)
    if type == 'account':
        account = get_object_or_404(Account, id=request.session.get('assigment_account_id', -1))
    elif type == 'department':
        department = get_object_or_404(Department, id=request.session.get('assigment_department_id', -1))
    else:
        return redirect('dashboard:order-dashboard:assigment')

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Assigment',
         'url': reverse('dashboard:order-dashboard:assigment')},
        {'is_active': False,
         'title': 'Select Content.',
         'url': reverse('dashboard:order-dashboard:assigment_step2')},
        {'is_active': True,
         'title': 'Result.'}
    ]
    return render(request,
                  'order/dashboard/assigment_result.html',
                  {'SIDEBAR': 'assigment',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'msg': request.session.get('assigment_msg', -1)})
