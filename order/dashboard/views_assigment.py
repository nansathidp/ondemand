from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.db.models import Q

from account.models import Account
from department.models import Department

def assigment_view(request):
    if not request.user.has_perm('order.add_order'):
        raise PermissionDenied

    account_list = []
    if request.method == 'POST':
        if 'search' in request.POST:
            account_list = Account.objects.filter(Q(email__icontains=request.POST.get('email', '')) | Q(first_name__icontains=request.POST.get('email', '')))[:10]
        elif 'select' in request.POST:
            department = get_object_or_404(Department, id=request.POST.get('department', -1))
            request.session['assigment_type'] = 'department'
            request.session['assigment_department_id'] = department.id
            return redirect('dashboard:order-dashboard:assigment_step2')
        
    department_list = Department.objects.all()
    
    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Assigment'}
    ]
    return render(request,
                  'order/dashboard/assigment.html',
                  {'SIDEBAR': 'assigment',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'account_list': account_list,
                   'department_list': department_list})
