from django.conf.urls import url

from .views import home_view
from .views_delete import delete_view
from .views_summary import summary_view

from .views_assigment import assigment_view
from .views_assigment_account import assigment_account_view
from .views_assigment_step2 import assigment_step2_view
from .views_assigment_course import assigment_course_view
from .views_assigment_coin import assigment_coin_view
from .views_assigment_question import assigment_question_view
from .views_assigment_result import assigment_result_view


app_name = 'order-dashboard'
urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^(\d+)/delete/$', delete_view, name='delete'),
    url(r'^summary/$', summary_view, name='summary'),

    url(r'^assigment/$', assigment_view, name='assigment'),  # TODO: testing
    url(r'^assigment/account/(\d+)/$', assigment_account_view, name='assigment_account'),  # TODO: testing
    url(r'^assigment/step2/$', assigment_step2_view, name='assigment_step2'),  # TODO: testing
    url(r'^assigment/course/(\d+)/$', assigment_course_view, name='assigment_course'),  # TODO: testing
    url(r'^assigment/coin/(\d+)/$', assigment_coin_view, name='assigment_coin'),  # TODO: testing
    # url(r'^assigment/programe/(\d+)/$', assigment_package_view, name='assigment_package'), #TODO: testing
    url(r'^assigment/test/(\d+)/$', assigment_question_view, name='assigment_question'),  # TODO: testing
    url(r'^assigment/result/$', assigment_result_view, name='assigment_result'),  # TODO: testing
]
