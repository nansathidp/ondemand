from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from account.models import Account
from department.models import Department
from course.models import Course
from question.models import Activity
from coin.models import Coin

def assigment_step2_view(request):
    if not request.user.has_perm('order.add_order'):
        raise PermissionDenied

    type = request.session.get('assigment_type', None)
    account = None
    department = None
    if not type in ['account', 'department']:
        return redirect('dashboard:order-dashboard:assigment')
    elif type == 'account':
        account = get_object_or_404(Account, id=request.session.get('assigment_account_id', -1))
    elif type == 'department':
        department = get_object_or_404(Department, id=request.session.get('assigment_department_id', -1))

    activity_list = Activity.objects.all()
    if request.APP.id == 3:
        course_list = []
        coin_list = Coin.objects.filter(is_use=True)
    else:
        course_list = Course.objects.all()
        coin_list = []
    
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Assigment',
         'url': reverse('dashboard:order-dashboard:assigment')},
        {'is_active': True,
         'title': 'Select Content.'}
    ]
    return render(request,
                  'order/dashboard/assigment_step2.html',
                  {'SIDEBAR': 'assigment',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'type': type,
                   'account': account,
                   'department': department,
                   'course_list': course_list,
                   'activity_list': activity_list,
                   'coin_list': coin_list})
