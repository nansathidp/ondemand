from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Item
from ..job.item_delete import _item_delete

import django_rq

def delete_view(request, item_id):
    if not request.user.has_perm('order.delete_order', group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    item = get_object_or_404(Item, id=item_id)
    item.status = -1
    item.save(update_fields=['status'])
    django_rq.enqueue(_item_delete, item.id)
    return redirect('dashboard:order-dashboard:home')
