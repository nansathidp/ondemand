from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.conf import settings

from ..models import Order, Item
from account.models import Account
from course.models import Course
from price.models import Price
from question.models import Activity
from department.models import Department
from notification.models import Queue

from course.cached import cached_course_video_sum_duration

def push_course(request, account, course):
    content_type = settings.CONTENT_TYPE('course', 'course')
    
    #store = int(store)
    store = 5
    price = Price.pull(content_type, course.id, store)
    
    start = None
    msg = 200
    if Item.objects.filter(order__account=account,
                           status=2,
                           content_type_id=content_type.id,
                           content=course.id).exists():
        msg = 400
        #Log.push(request.DASHBOARD_INSTITUTE_ACCOUNT.institute_id,
        #         request.user.id,
        #         request.user.email,
        #         '%s %s'%(request.user.first_name, request.user.last_name),
        #         account.id,
        #         account.email,
        #         '%s %s'%(account.first_name, account.last_name),
        #         802,
        #         'course exists.')
        #return redirect('institute:pos_fail', account.id)
    else:
        if store == 6:
            store_course = StoreCourse.objects.filter(store=6,
                                                      course=course,
                                                      is_use=True).first()
            if store_course is not None:
                credit = store_course.credit
                start = timezone.now()
            else:
                credit = course.credit
                start = None
        else:
            credit = course.credit
            start = None
        start = timezone.now()
        order = Order.pull_create(request.APP,
                                  account,
                                  store,
                                  price=price.price,
                                  discount=price.discount,
                                  net=price.net,
                                  status=3,
                                  method=15,
                                  timecomplete=timezone.now())

        item = Item.objects.create(order=order,
                                   account=account,
                                   content_type=content_type,
                                   content=course.id,
                                   content_name=course.name,
                                   amount=1,
                                   price=price.price,
                                   discount=price.discount,
                                   net=price.net,
                                   is_shipping=False,
                                   is_free=False,
                                   is_assign=True,
                                   status=2,
                                   start=start,
                                   total_duration=cached_course_video_sum_duration(course),
                                   credit=credit,
                                   expired=course.expired,
                                   is_display=True)
        # item.push()
        order.buy_success(request.APP, request.get_host().split(':')[0])
        Queue.push_course(request.APP.id, item.id, course.id, account.id)
        
        #bill = Bill.get_bill(order)
        #Mailer.send_billing_email(order, bill, request.get_host())
        #Log.push(request.DASHBOARD_INSTITUTE_ACCOUNT.institute_id,
        #         request.user.id,
        #         request.user.email,
        #         '%s %s'%(request.user.first_name, request.user.last_name),
        #         account.id,
        #         account.email,
        #         '%s %s'%(account.first_name, account.last_name),
        #         801,
        #         'success.')

        #content = Course.objects.filter(id=item.content).first()
        #tutor = content.tutor.all().first()
        #if tutor is not None:
        #    tutor_id = tutor.id
        #else:
        #    tutor_id = None

        return msg
    
def assigment_course_view(request, course_id):
    if not request.user.has_perm('order.add_order'):
        raise PermissionDenied

    type = request.session.get('assigment_type', None)
    course = get_object_or_404(Course, id=course_id)
    msg = 200
    if type == 'account':
        account = get_object_or_404(Account, id=request.session.get('assigment_account_id', -1))
        msg = push_course(request, account, course)
    elif type == 'department':
        department = get_object_or_404(Department, id=request.session.get('assigment_department_id', -1))
        for member in department.member_set.all():
            msg = push_course(request, member.account, course)
    else:
        return redirect('dashboard:order-dashboard:assigment')

    request.session['assigment_msg'] = msg
    return redirect('dashboard:order-dashboard:assigment_result')
