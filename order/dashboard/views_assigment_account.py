from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from account.models import Account


def assigment_account_view(request, account_id):
    if not request.user.has_perm('order.add_order'):
        raise PermissionDenied

    account = get_object_or_404(Account, id=account_id)
    request.session['assigment_type'] = 'account'
    request.session['assigment_account_id'] = account.id
    return redirect('dashboard:order-dashboard:assigment_step2')
