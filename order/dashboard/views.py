from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.db.models import Sum
from django.shortcuts import render

from dashboard.views import condition_view, config_view
from utils.filter import get_q
from utils.paginator import paginator
from ..models import Item


def home_view(request):
    # TODO: Filter with permission
    item_list = Item.access_list(request)
    if item_list is None:
        raise PermissionDenied
    elif item_list == []:
        return condition_view(request)
    elif item_list is False:
        return config_view(request)

    side_bar = 'order_list'
    header_bar = 'Order List'

    filter = get_q(request, 'filter', is_get=True, default='normal', type='str')
    start = get_q(request, 'start', default=None, type='date')
    end = get_q(request, 'end', default=None, type='date')
    if start is not None and end is not None:
        item_list = item_list.filter(order__timecomplete__range=(start, end))
    if filter == 'normal':
        side_bar = 'register_list'
        header_bar = 'Registration List'
        item_list = item_list.exclude(order__method__in=[11, 6, 15, 21])
    elif filter == 'ios':
        item_list = item_list.filter(order__method=11)
    elif filter == 'mpay':
        item_list = item_list.filter(order__method=6)
    elif filter == 'pos':
        side_bar = 'assign_list'
        header_bar = 'Assignment List'
        item_list = item_list.filter(order__method=15, order__store=5)
    elif filter == 'posontop':
        item_list = item_list.filter(order__method=15, order__store=6)
    elif filter == 'assign':
        side_bar = 'assign_list'
        header_bar = 'Assignment List'
        item_list = item_list.filter(order__method=21)

    result = item_list.aggregate(Sum('net'), Sum('fee'))
    if settings.ACCOUNT__HIDE_INACTIVE:
        item_list = item_list.exclude(account__is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        item_list = item_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)
    item_list = paginator(request, item_list)
    for item in item_list:
        item.type = settings.CONTENT_TYPE_ID(item.content_type_id)

    try:
        net = float(result['net__sum'])
    except:
        net = 0.0
    try:
        fee = float(result['fee__sum'])
    except:
        fee = 0.0
    net_revenues = net - fee
    return render(request,
                  'order/dashboard/home.html',
                  {'SIDEBAR': side_bar,
                   'header_bar': header_bar,
                   'item_list': item_list,
                   'filter': filter,
                   'start': start,
                   'end': end,
                   'fee': fee,
                   'net': net_revenues})
