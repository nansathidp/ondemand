from django.shortcuts import render
from django.core.exceptions import PermissionDenied

from ..models import Summary
from utils.paginator import paginator


def summary_view(request):
    if not request.user.has_perm('order.view_order') and not request.user.has_perm('order.view_own_order'):
        raise PermissionDenied
    summary_list = Summary.objects.all()
    summary_list = paginator(request, summary_list)
    for summary in summary_list:
        summary.get_content()
    
    return render(request,
                  'order/dashboard/summary.html',
                  {'SIDEBAR': 'order_summary',
                   'summary_list': summary_list})
