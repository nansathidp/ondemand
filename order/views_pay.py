from django.shortcuts import redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.conf import settings

from order.models import Order
from mailer.models import Mailer
from paysbuy.models import Request as PaysbuyRequest

import requests, re, json

@login_required
def pay_view(request, order_id):
    try:
        method = int(request.GET.get('method', 0))
    except:
        method = 0
    order = get_object_or_404(Order, id=order_id, account=request.user)

    if method == 2:
        order.method = 2
    elif method == 6:
        order.method = 5
    elif method == 4:
        order.method = 4
        order.status = 2
    elif method == 5:
        order.method = 3
    order.status = 2
    order.save(update_fields=['method', 'status'])
    
    if method == 4:
        return redirect('order:bank_info', order.id)
    
    payload = {'psbID': settings.CONFIG(request.get_host(),'PAYSBUY_ID'),
               'username': settings.CONFIG(request.get_host(),'PAYSBUY_USERNAME'),
               'secureCode': settings.CONFIG(request.get_host(),'PAYSBUY_CODE'),
               'inv': order.id,
               'itm': settings.CONFIG(request.get_host(),'BASE_NAME'),
               'amt': order.net,
               'paypal_amt': '',
               'curr_type': 'TH',
               'com': '',
               'method': method,
               'language': 'T',
               'resp_front_url': '%s%s'%(settings.BASE_URL(request), reverse('order:status', args=[order.id])),
               'resp_back_url': '%s%s'%(settings.BASE_URL(request), reverse('order:response')),
               'opt_fix_redirect': '',
               'opt_fix_method': '1',
               'opt_name': '',
               'opt_email': '',
               'opt_mobile': '',
               'opt_address': '',
               'opt_detail': '',
               'opt_param': ''}

    paysbuy_request = PaysbuyRequest.objects.create(order=order,
                                                    request=json.dumps(payload, indent=2),
                                                    type=1,
                                                    step=1)
                                     
    r = requests.post('%s/api_paynow/api_paynow.asmx/api_paynow_authentication_v3'%settings.CONFIG(request.get_host(), 'PAYSBUY_URL'), data=payload, verify=False)
    paysbuy_request.response = json.dumps(r.text, indent=2)
    paysbuy_request.save(update_fields=['response'])
    
    try:
        result = re.search('00(.+)</string>', r.text).group(1)
        return redirect('%s/paynow.aspx?refid=%s'%(settings.CONFIG(request.get_host(),'PAYSBUY_URL'), result))
    except:
        pass
    return redirect('order:home')
