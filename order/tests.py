import random
import string

from django.test import TestCase

from account.models import Account
from app.models import App
from course.models import Course
from category.tests import create_category


def _random():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(8))


def create_app():
    return App.objects.create(name=_random())


def create_user():
    return Account.objects.create_user('%s@test.com' % _random(),
                                       '123456')


def create_course():
    return Course.objects.create(name=_random(),
                                 category=create_category())
