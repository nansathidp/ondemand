from django.shortcuts import render, get_object_or_404, redirect
from api.v4.views import json_render

from django.core.urlresolvers import reverse

from aismpay.models import OtpRequest, OtpConfirm, ReserveVolume, Level

from account.cached import cached_api_account_auth
from ondemand.views_base import response

def webview_aismpay_otpconfirm_view(request, otp_request_id):
    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)

    if uuid is None:
        return response(request, 202)
    if token is None:
        return response(request, 204)

    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return response(request, 703)
    except:
        return response(request, 209)

    account = cached_api_account_auth(token, uuid)
    if account is None:
        return response(request, 400)
    otp_request = get_object_or_404(OtpRequest, id=otp_request_id, account=account)
    result = None
    if request.method == 'POST':
        otp = request.POST.get('otp', '')
        is_success, otp_confirm = OtpConfirm.send(otp_request, otp)
        if is_success:
            level = Level.pull(otp_request.content_type,
                               otp_request.content_type.get_object_for_this_type(id=otp_request.content))
            if level is not None:
                is_success, reserve_volume = ReserveVolume.send(account,
                                                                1,
                                                                otp_request.content_type,
                                                                otp_request.content,
                                                                otp_confirm.tel,
                                                                level.volume,
                                                                level.service,
                                                                level.volume,
                                                                otp_request.transaction_id,
                                                                otp_confirm.pass_key,
                                                                'Info',
                                                                otp_confirm)
                return redirect('%s?uuid=%s&token=%s&store=%s' % (reverse('order:webview_aismpay_captcha', args=(reserve_volume.id,)), uuid, token, store))
            else:
                result = 'ราคานี้ ไม่สามารถซื้อด้วย AIS mPay ได้'

        else:
            result = otp_confirm.status

    return render(request,
                  'order/webview_aismpay_otpconfirm.html',
                  {'result': result,
                   'otp_request': otp_request})
