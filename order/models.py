from django.db import models
from django.conf import settings


class Order(models.Model):
    from store.models import STORE_CHOICES

    PAYMENT_METHOD_CHOICES = (
        (-1, 'Self-enroll'),
        (1, 'PS:Paysbuy'),
        (2, 'PS:Credit Card'),
        (3, 'PS:OnlineBanking'),
        (4, 'BankTranfer'),
        (5, 'PS:Cash'),
        (6, 'OP Charging'),
        (7, 'PS:Paypal'),
        (8, 'PS:Alipay'),
        (9, 'PS:Paysbuy Mobile Credit Card'),
        (10, 'Counter service'),
        (11, 'Apple In-app purchase'),
        (12, 'Line pay'),
        (13, 'Click Point'),
        (14, 'Coin'),
        (15, 'POS'),
        (16, 'Pay@All'),
        (17, 'Redeem'),
        (18, 'MPay'),
        (19, 'MPay:CreditCard'),
        (20, 'MPay:Offline'),
        (21, 'Assignment'),
    )

    STATUS_CHOICES = (
        (-1, 'Admin Cancel'),
        (1, 'Cart'),
        (2, 'Payment'),
        (3, 'Success'),
    )

    APPROVE_EXTRA = (
        (-1, 'For test'),
        (0, 'Normal'),
        (1, 'Force Approve'),
    )

    pair_id = models.BigIntegerField(blank=True, default=-1)  # For Migration : ClickForClever

    app = models.ForeignKey('app.App', blank=True, null=True)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='order_account')

    content_count = models.IntegerField(default=1)
    sum_price = models.FloatField(default=0.0)  # Sum Item.content_price
    sum_discount = models.FloatField(default=0.0)  # Sum Item.content_discount
    price = models.FloatField()  # sum_price - sum_discount
    discount = models.FloatField()  # Top-Up discount
    net = models.FloatField(db_index=True)

    method = models.IntegerField(choices=PAYMENT_METHOD_CHOICES, db_index=True, default=-1)
    status = models.IntegerField(choices=STATUS_CHOICES, db_index=True, default=1)
    store = models.IntegerField(choices=STORE_CHOICES, db_index=True, default=-1)
    extra = models.IntegerField(choices=APPROVE_EXTRA, default=0)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    timecomplete = models.DateTimeField(blank=True, null=True, db_index=True)

    is_test = models.BooleanField(default=False)

    class Meta:
        default_permissions = ('view', 'view_own', 'view_org', 'add', 'change', 'delete')
        ordering = ['-timestamp']

    def __str__(self):
        return str(self.id)

    @property
    def is_free(self):
        return self.net == 0

    @staticmethod
    def pull(id):
        from .cached import cached_order
        return cached_order(id)

    @staticmethod
    def pull_first(account, store, app=None): #TODO: clean app parameter
        if app is None:
            order = Order.objects.filter(account=account, status=1, store=store).first()
        else:
            order = Order.objects.filter(app=app, account=account, status=1, store=store).first()
        if order is None:
            order = Order.pull_create(app, account, store)
        return order

    @staticmethod
    def pull_create(app, account, store=0, price=0.0, discount=0.0, net=0.0, status=1, method=-1, timecomplete=None):
        order = Order.objects.create(app=app,
                                     account=account,
                                     price=price,
                                     discount=discount,
                                     net=net,
                                     status=status,
                                     method=method,
                                     store=store,
                                     timecomplete=timecomplete)
        return order

    @staticmethod
    def buy(content_location, app, store, content_type, content, account, is_checkout=False, method=-1, order=None, start=None):
        from django.conf import settings
        from django.utils import timezone
        from price.models import Price
        from order.cached import cached_order_item_update, cached_order_item_account
        import datetime

        price = Price.pull(content_type, content.id, store)

        if order is not None:
            pass
        elif is_checkout or price.net == 0:
            order = Order.pull_create(app,
                                      account,
                                      store=store,
                                      method=method)
        else:
            order = Order.pull_first(account, store, app)

        if int(store) == 3:
            store = 4

        count_content = 1
        if price.net == 0:
            item_status = 2
        else:
            item_status = 1
        is_shipping = False
        _start = start

        if _start is None:
            _start = timezone.now()

        total_duration = datetime.timedelta(0)

        if content_type == settings.CONTENT_TYPE('question', 'activity'):
            content.credit = datetime.timedelta(0)

        is_cart = order.item_set.filter(content_type=content_type,
                                        content=content.id,
                                        status=1).exists()
        if is_cart and price.net > 0:
            return None

        item_libary = Item.objects.filter(content_location=content_location,
                                          account=account,
                                          content_type=content_type,
                                          content=content.id,
                                          status=2).first()
        if item_libary is not None:
            return item_libary

        # TODO:HANDLE TYPE COIN
        if price.net == 0:
            order.content_count = 1
            order.status = 3
            order.timecomplete = timezone.now()
            order.save(update_fields=['content_count', 'status', 'timecomplete'])

        item = Item.objects.create(order=order,
                                   content_location=content_location,
                                   account=account,
                                   content_type=content_type,
                                   content=content.id,
                                   content_name=content.name,
                                   content_count=count_content,
                                   amount=1,
                                   price=price.price,
                                   discount=price.discount,
                                   net=price.net,
                                   start=_start,
                                   is_free=price.is_free,
                                   status=item_status,
                                   is_shipping=is_shipping,
                                   total_duration=total_duration,
                                   credit=content.credit,
                                   expired=content.expired,
                                   is_display=True)

        provider = getattr(content, 'provider', None)
        if provider is not None:
            item.providers.set([provider])

        cached_order_item_update(item)
        cached_order_item_account(account, is_force=True)

        order.cal_price()
        return item

    # TODO: remove
    def buy_coin(self, coin, account, store, data={}):
        from django.conf import settings

        from order.cached import cached_order_item_update, cached_order_item_account
        import datetime, json

        content_type = settings.CONTENT_TYPE('coin', 'coin')
        if not coin.is_active():
            return None
        if coin.type == 0:
            price = coin.price
            discount = coin.discount
            net = coin.get_net()
        elif coin.type == 1:
            price = data['price']
            discount = -1.0
            net = data['price']
        item = Item.objects.create(order=self,
                                   account=self.account,
                                   content_type=content_type,
                                   content=coin.id,
                                   content_name=coin.name,
                                   content_count=0,
                                   amount=1,
                                   data=json.dumps(data, indent=2),
                                   price=price,
                                   discount=discount,
                                   net=net,
                                   is_free=False,
                                   status=1,
                                   total_duration=datetime.timedelta(0),
                                   credit=datetime.timedelta(0),
                                   expired=datetime.timedelta(0),
                                   is_allow_promote=coin.is_allow_promote,
                                   is_display=True)
        # item.push()
        cached_order_item_update(item)
        cached_order_item_account(account, is_force=True)
        self.cal_price()

    def buy_success(self, app, domain, method=None):
        from django.conf import settings
        from django.utils import timezone

        from mailer.models import Mailer
        from coin.models import Coin, Volume as CoinVolume
        from program.models import Item as ProgramItem
        from task.models import Task
        from notification.models import Queue
        from progress.models import Progress, Account as ProgressAccount, Content as ProgressContent
        from content.models import Location as ContentLocation
        from analytic.models import Stat
        from account.cached import cached_onboard_account_delete
        from utils.content import get_content
        from progress.cached import cached_progress_account_content, cached_progress_account
        from order.cached import cached_order_item_account_delete
        import json

        self.status = 3
        if method is not None:
            self.method = method
        self.timecomplete = timezone.now()
        self.save(update_fields=['status', 'method', 'timecomplete'])
        self.item_set.update(status=2)

        #Fee
        #this magic number user in script/migrate_order_item_fee.py !!!

        for item in self.item_set.all():
            if self.method in [1, 2, 3, 4, 5, 7, 8, 9, 10, 12, 16, 18, 19, 20]:
                item.fee = item.net * 0.04
                item.save(update_fields=['fee'])
            elif self.method in [6, 11]:
                item.fee = item.net * 0.3
                item.save(update_fields=['fee'])
            elif self.method == 21:
                item.start = timezone.now()
                item.save(update_fields=['start'])

        if self.method not in [-1, 6, 11, 15, 21]:
            bill = Bill.get_bill(self)
            Mailer.send_billing_email(self, bill, domain)

        content_type_onboard = settings.CONTENT_TYPE('program', 'onboard')
        content_type_program = settings.CONTENT_TYPE('program', 'program')
        content_type_course = settings.CONTENT_TYPE('course', 'course')
        content_type_material = settings.CONTENT_TYPE('course', 'material')        
        content_type_question = settings.CONTENT_TYPE('question', 'activity')
        content_type_task = settings.CONTENT_TYPE('task', 'task')
        content_type_coin = settings.CONTENT_TYPE('coin', 'coin')

        for item in self.item_set.all():
            # TODO : use RQ
            content_location = ContentLocation.pull(item.content_location_id)
            Progress.pull(item,
                          content_location,
                          settings.CONTENT_TYPE_ID(item.content_type_id),
                          item.content)
            ProgressContent.update_progress(content_location.id, item.content_type_id, item.content)
            # END TODO

            stat_code = None
            # Check onboard
            if item.content_type_id == content_type_onboard.id:
                program = get_content(item.content_type_id, item.content)
                if program is not None:
                    if program.type == 1: # Program
                        stat_code = 'program_%s'%item.content
                    elif program.type == 2:  # Onboarding
                        stat_code = 'program_onboard_%s' % item.content
                        cached_onboard_account_delete(self.account_id)
                account_content = cached_progress_account_content(self.account_id, None)
                account_content.update_count(course=False, package=False, program=True, question=False, lesson_subscribe=False, lesson_owner=False)
                for provider in item.providers.all():
                    account_content = cached_progress_account_content(self.account_id, provider)
                    account_content.update_count(course=False, package=False, program=True, question=False, lesson_subscribe=False, lesson_owner=False)

                for program_item in ProgramItem.objects.filter(program_id=item.content):
                    # TODO : use RQ
                    content_location_item = ContentLocation.pull_first(settings.CONTENT_TYPE_ID(program_item.content_type_id),
                                                                       program_item.content,
                                                                       parent1_content_type=content_type_onboard,
                                                                       parent1_content=item.content)
                    Progress.pull(item,
                                  content_location_item,
                                  settings.CONTENT_TYPE_ID(program_item.content_type_id),
                                  program_item.content)
                    ProgressContent.update_progress(content_location_item.id,
                                                    program_item.content_type_id,
                                                    program_item.content)
                    # Question in Course in Program
                    if settings.CONTENT_TYPE_ID(program_item.content_type_id) == content_type_course:
                        course = get_content(program_item.content_type_id, program_item.content)
                        if course:
                            for material in course.material_set.filter(type=3):
                                content_location_material = ContentLocation.pull_first(content_type_question,
                                                                                       material.question_id,
                                                                                       parent1_content_type=content_type_onboard,
                                                                                       parent1_content=item.content,
                                                                                       parent2_content_type=content_type_course,
                                                                                       parent2_content=course.id,
                                                                                       parent3_content_type=content_type_material,
                                                                                       parent3_content=material.id)
                                Progress.pull(item,
                                              content_location_material,
                                              content_type_question,
                                              material.question_id)
                                ProgressContent.update_progress(content_location_material.id,
                                                                content_type_question.id,
                                                                material.question_id)
                    # END TODO
                    if program_item.content_type_id == content_type_task.id:
                        Task.push_approve(item, program_item.content, program_item.program_id, self.account_id)
                Queue.push_program(app.id, item.id, item.content, self.account_id)

            # Check program
            elif item.content_type_id == content_type_program.id:
                continue
                program = get_content(item.content_type_id, item.content)
                if program is not None:
                    if program.type == 1: # Program
                        stat_code = 'program_%s'%item.content
                    elif program.type == 2:  # Onboarding
                        stat_code = 'program_onboard_%s' % item.content
                        cached_onboard_account_delete(self.account_id)
                account_content = cached_progress_account_content(self.account_id, None)
                account_content.update_count(course=False, package=False, program=True, question=False, lesson_subscribe=False, lesson_owner=False)
                for provider in item.providers.all():
                    account_content = cached_progress_account_content(self.account_id, provider)
                    account_content.update_count(course=False, package=False, program=True, question=False, lesson_subscribe=False, lesson_owner=False)

                for program_item in ProgramItem.objects.filter(program_id=item.content):
                    # TODO : use RQ
                    content_location_item = ContentLocation.pull_first(settings.CONTENT_TYPE_ID(program_item.content_type_id),
                                                                       program_item.content,
                                                                       parent1_content_type=content_type_program,
                                                                       parent1_content=item.content)
                    Progress.pull(item,
                                  content_location_item,
                                  settings.CONTENT_TYPE_ID(program_item.content_type_id),
                                  program_item.content)
                    ProgressContent.update_progress(content_location_item.id,
                                                    program_item.content_type_id,
                                                    program_item.content)
                    # Question in Course in Program
                    if settings.CONTENT_TYPE_ID(program_item.content_type_id) == content_type_course:
                        course = get_content(program_item.content_type_id, program_item.content)
                        if course:
                            for material in course.material_set.filter(type=3):
                                content_location_material = ContentLocation.pull_first(content_type_question,
                                                                                       material.question_id,
                                                                                       parent1_content_type=content_type_program,
                                                                                       parent1_content=item.content,
                                                                                       parent2_content_type=content_type_course,
                                                                                       parent2_content=course.id,
                                                                                       parent3_content_type=content_type_material,
                                                                                       parent3_content=material.id)
                                Progress.pull(item,
                                              content_location_material,
                                              content_type_question,
                                              material.question_id)
                                ProgressContent.update_progress(content_location_material.id,
                                                                content_type_question.id,
                                                                material.question_id)
                    # END TODO
                    if program_item.content_type_id == content_type_task.id:
                        Task.push_approve(item, program_item.content, program_item.program_id, self.account_id)
                Queue.push_program(app.id, item.id, item.content, self.account_id)

            # Check course
            elif item.content_type_id == content_type_course.id:
                stat_code = 'course_%s' % item.content
                account_content = cached_progress_account_content(self.account_id, None)
                account_content.update_count(course=True, package=False, program=False, question=False, lesson_subscribe=False, lesson_owner=False)
                for provider in item.providers.all():
                    account_content = cached_progress_account_content(self.account_id, provider)
                    account_content.update_count(course=True, package=False, program=False, question=False, lesson_subscribe=False, lesson_owner=False)
                Queue.push_course(app.id, item.id, item.content, self.account_id)
                # TODO : user RQ (find question in course)
                course = get_content(item.content_type_id, item.content)
                if course:
                    for material in course.material_set.filter(type=3):
                        content_location_material = ContentLocation.pull_first(content_type_question,
                                                                               material.question_id,
                                                                               parent1_content_type=content_type_course,
                                                                               parent1_content=course.id,
                                                                               parent2_content_type=content_type_material,
                                                                               parent2_content=material.id)
                        Progress.pull(item,
                                      content_location_material,
                                      content_type_question,
                                      material.question_id)
                        ProgressContent.update_progress(content_location_material.id,
                                                        content_type_question.id,
                                                        material.question_id)
                    # END TODO

            elif item.content_type_id == content_type_question.id:
                stat_code = 'question_%s' % item.content
                account_content = cached_progress_account_content(self.account_id, None)
                account_content.update_count(course=False, package=False, program=False, question=True, lesson_subscribe=False, lesson_owner=False)
                for provider in item.providers.all():
                    account_content = cached_progress_account_content(self.account_id, provider)
                    account_content.update_count(course=False, package=False, program=False, question=True, lesson_subscribe=False, lesson_owner=False)
                Queue.push_question(app.id, item.id, item.content, self.account_id)
            elif item.content_type_id == content_type_task.id:
                stat_code = 'task_%s' % item.content
            elif item.content_type_id == content_type_coin.id:
                stat_code = 'coin_%s' % item.content
                coin = Coin.objects.get(id=item.content)
                if coin.type == 0:
                    CoinVolume.push(app, self.account, coin.volume)
                elif coin.type == 1:
                    data = json.loads(item.data)
                    CoinVolume.push(app, self.account, data['price'])
            if stat_code is not None:
                if self.method == 21:
                    stat_code += '_assign'
                elif item.is_free:
                    stat_code += '_libary'
                else:
                    stat_code += '_buy'
                Stat.push(stat_code, None, None)

        # TODO : use RQ
        # Cached Update 
        cached_order_item_account_delete(self.account_id)
        ProgressAccount.update_progress(self.account_id)
        account_content = cached_progress_account_content(self.account_id, app.provider)
        account_content.update_count()

    def remove_item(self, item_id):
        self.item_set.filter(id=item_id).delete()
        self.cal_price()

    def cal_price(self):
        from promote.models import Item as PromoteItem
        DEBUG = False
        discount_percent = 0.0
        discount_price = 0.0
        promote_per_item_list = list()
        for order_promote in self.promote_set.select_related('code', 'code__promote').all():
            if order_promote.promote_item.type != 2 and order_promote.promote_item.discount_type == 1:
                discount_price += order_promote.promote_item.discount
            elif order_promote.promote_item.type != 2:
                discount_percent += order_promote.promote_item.discount
            elif order_promote.promote_item.type == 2:
                promote_per_item_list.append(order_promote.promote_item.id)

        if DEBUG:
            print('Discount Percent: {}, Discount Print: {}'.format(discount_percent, discount_price))
            print('Discount Per Item: {}'.format(promote_per_item_list))
        price = 0.0
        price_allow_promote = 0.0
        discount = 0.0
        if len(promote_per_item_list) > 0:
            promote_item_list = list(PromoteItem.objects.filter(promote__id__in=promote_per_item_list))
        self.content_count = 0
        for item in self.item_set.all():
            self.content_count += item.content_count
            item_net = item.net
            if item.discount == -1.0:
                p = item.price * float(item.amount)
            else:
                p = item.discount * item.amount
            item.net = p
            if DEBUG:
                print('P: {}'.format(p))
            # Promote
            if item.is_allow_promote:
                d = p * discount_percent / 100.0
                discount += d
                price_allow_promote += p - d
                if DEBUG:
                    print('is_allow_promote > discount (d): {} price_allow: {}'.format(d, p - d))
            if len(promote_per_item_list) > 0:
                for promote_item in promote_item_list:
                    if promote_item.content_type_id == item.content_type_id and item.content == promote_item.content:
                        if promote_item.promote.rule == 3 and promote_item.promote.type == 2:
                            item.net = 0.0
                        if promote_item.promote.rule == 4 and promote_item.promote.type == 2:
                            item.net = 0.0
                        elif promote_item.promote.discount_type == 1:
                            item.net = item.net - promote_item.promote.discount
                            if item.net < 0.0:
                                item.net = 0.0
                        elif promote_item.promote.discount_type == 2:
                            item.net = item.net * (promote_item.promote.discount / 100.0)
                            if item.net < 0.0:
                                item.net = 0.0
                        break
            price += item.net
            if item.net != item_net:
                item.save(update_fields=['net'])
        if discount_price > price_allow_promote:
            discount += price_allow_promote
        else:
            discount += discount_price
        if DEBUG:
            print('Price: {}'.format(price))
            print('Discount: {}'.format(discount))
            print('Net: {}'.format(price - discount))
        self.price = price
        self.discount = discount
        self.net = price - discount
        self.save(update_fields=['content_count', 'price', 'discount', 'net'])

    def api_display(self):
        d = {}
        d['id'] = self.id
        d['price'] = self.price
        d['discount'] = self.discount
        d['net'] = self.net
        d['method'] = self.method
        d['method_display'] = self.get_method_display()
        d['status'] = self.status
        d['status_display'] = self.get_status_display()
        d['store'] = self.store
        d['store_display'] = self.get_store_display()
        d['item_count'] = self.item_set.count()
        d['timestamp'] = self.timestamp.strftime(settings.TIMESTAMP_FORMAT_)
        return d

    def push_promote_code(self, user, code):
        from promote.models import Promote as PromotePromote, Code
        code = code.strip()
        if len(code) == 0:
            return
        is_success = Code.push(self, user, code)
        if not is_success:
            is_success = PromotePromote.push(self, user, code)

    def clear_price(self):
        self.price = 0
        self.discount = 0
        self.net = 0
        self.sum_price = 0
        self.sum_discount = 0
        self.is_test = True
        self.save()
        self.item_set.all().update(price=0.0,
                                   discount=0.0,
                                   net=0.0,
                                   is_shipping=False,
                                   is_free=False,
                                   status=2)

    def revoke(self):
        self.price = 0
        self.discount = 0
        self.net = 0
        self.sum_price = 0
        self.sum_discount = 0
        self.status = -1
        self.save()
        self.item_set.all().update(price=0.0,
                                   discount=0.0,
                                   net=0.0,
                                   is_shipping=False,
                                   is_free=False,
                                   status=-1)

    def is_shipping(self):
        return self.item_set.filter(is_shipping=True).count() > 0


class Bill(models.Model):
    order = models.OneToOneField(Order)
    no = models.IntegerField(db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']

    def __str__(self):
        return str(self.no)

    def get_vat(self):
        return round(self.order.net * 7 / 107, 2)

    def get_price(self):
        return self.order.net - self.get_vat()

    def get_net(self):
        return self.order.net

    @staticmethod
    def get_bill(order):
        if order.status != 3:
            return None

        bill = Bill.objects.filter(order=order).first()
        if bill is not None:
            return bill

        try:
            no = Bill.objects.order_by('-no').first().no + 1
        except:
            no = 1
        return Bill.objects.create(order=order, no=no)


class Item(models.Model):
    import datetime

    STATUS_CHOICES = (
        (-1, 'Admin Cancel'),
        (1, 'Waiting for payment'),
        (2, 'Active'),
        (3, 'Expired'),
    )

    order = models.ForeignKey(Order)
    content_location = models.ForeignKey('content.Location', null=True)

    providers = models.ManyToManyField('provider.Provider')
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='order_item_set', null=True, default=None)

    content_type = models.ForeignKey('contenttypes.ContentType')
    content = models.BigIntegerField(db_index=True)

    content_name = models.CharField(max_length=120, blank=True)
    content_count = models.IntegerField(default=0)  # for Package
    amount = models.IntegerField()

    # Keep extend data for item (1.coin type user volume)
    data = models.TextField(blank=True, default='')

    content_price = models.FloatField(default=0.0)
    content_discount = models.FloatField(default=0.0)
    price = models.FloatField()  # content_price or content_discount
    discount = models.FloatField()
    net = models.FloatField(db_index=True)
    fee = models.FloatField(default=0.0)

    is_shipping = models.BooleanField(default=False)
    is_free = models.BooleanField(default=False, db_index=True)
    is_assign = models.BooleanField(default=False, db_index=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=1, db_index=True)

    duration = models.DurationField(default=datetime.timedelta(0))  # TODO: remove
    total_duration = models.DurationField(default=datetime.timedelta(0))  # TODO: remove
    use_credit = models.DurationField(default=datetime.timedelta(0))  # TODO: remove
    percent = models.IntegerField(default=0)  # TODO: remove use Progress

    credit = models.DurationField(default=datetime.timedelta(0)) # TODO: remove
    expired = models.DurationField(default=datetime.timedelta(0))

    start = models.DateTimeField(null=True, blank=True)

    is_allow_promote = models.BooleanField(default=True)
    is_display = models.BooleanField(default=True, db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)


    class Meta:
        ordering = ['-timestamp']

    def __str__(self):
        return '(%s) %s : %s' % (self.id, self.content_type, self.content)

    @staticmethod
    def pull(id):
        from .cached import cached_order_item
        return cached_order_item(id)

    @staticmethod
    def pull_purchased(content_location, account, content_type, content_id):
        # TODO : cached
        return Item.objects.filter(content_location=content_location,
                                   account=account,
                                   content_type=content_type,
                                   content=content_id,
                                   order__status=3,
                                   status=2) \
                           .first()

        # TODO : remove
        # from .cached import cached_order_item_is_purchased
        # return cached_order_item_is_purchased(account, content_type, content_id)

    @staticmethod
    def pull_onboard_item(account):
        from django.conf import settings
        from program.models import Program
        
        onboard_id_list = Program.objects.values_list('id', flat=True).filter(type=2)
        order_item = Item.objects.filter(order__account=account,
                                         content_type=settings.CONTENT_TYPE('program', 'onboard'),
                                         content__in=onboard_id_list,
                                         status=2) \
                                 .order_by('-order__timecomplete') \
                                 .first()
        return order_item

    @staticmethod
    def access_list(request):
        from django.conf import settings
        if request.user.has_perm('order.view_order',
                                 group=request.DASHBOARD_GROUP):
            return Item.objects.select_related('order') \
                               .filter(status=2) \
                               .exclude(order__timecomplete=None)
        elif request.user.has_perm('order.view_own_order',
                                   group=request.DASHBOARD_GROUP):
            if request.DASHBOARD_PROVIDER is not None:
                return Item.objects.select_related('order') \
                                   .filter(status=2,
                                           providers=request.DASHBOARD_PROVIDER) \
                                   .exclude(order__timecomplete=None)
            elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                from subprovider.models import Item as SubProviderItem
                return Item.objects.select_related('order') \
                                   .filter(status=2,
                                           providers=request.DASHBOARD_SUB_PROVIDER.provider,
                                           id__in=SubProviderItem.objects
                                           .values_list('content', flat=True)
                                           .filter(sub_provider=request.DASHBOARD_SUB_PROVIDER))
            else:
                return []
        elif request.user.has_perm('order.view_org_order',
                                   group=request.DASHBOARD_GROUP):
            if settings.IS_ORGANIZATION:
                from organization.models import Parent
                return Item.objects.select_related('order') \
                                   .filter(status=2,
                                           account__in=Parent.objects \
                                           .values_list('account_id', flat=True)
                                           .filter(parent=request.user)) \
                                   .exclude(order__timecomplete=None)
            else:
                return False
        else:
            return None

    def is_active(self, is_check=False):
        from django.utils import timezone
        from .cached import cached_order_item, cached_order_item_update
        import datetime

        order_item = cached_order_item(self.id)
        if self.status == 3 and not is_check:
            result = False
        elif datetime.timedelta(0) > self.credit > self.use_credit:
            result = False
            # TODO
            # self.status = 3
            # self.save(update_fields=['status'])
            # result = False
        elif self.expired > datetime.timedelta(0) and self.start is not None and self.start + self.expired < timezone.now():
            # OLD
            # self.status = 3
            # self.save(update_fields=['status'])
            # result = False
            # Onboard Flow
            result = True
            order_item.push_expired()
        else:
            if self.status == 3:
                self.status = 2
                self.save(update_fields=['status'])
            result = True
        if order_item.status != self.status:
            cached_order_item_update(self)
        return result

    def use_credit_hour(self):
        return int(self.use_credit.total_seconds() / (60 * 60))

    def credit_hour(self):
        return int(self.credit.total_seconds() / (60 * 60))

    def credit_remain_hour(self):
        from utils.duration import duration_display
        from utils.content import get_content
        from progress.models import Progress
        from content.models import Location as ContentLocation
        import datetime
        content = get_content(self.content_type_id, self.content)
        if content is None:
            return 'Unlimited'
        else:
            try:
                s = content.credit.total_seconds()
            except:
                s = 0
        progress = Progress.pull(
            self,
            ContentLocation.pull(self.content_location_id),
            settings.CONTENT_TYPE_ID(self.content_type_id),
            self.content
        )
        try:
            s = s - progress.duration.total_seconds()
        except:
            s = 0
        if s < 0:
            s = 0

        return duration_display(datetime.timedelta(seconds=s))

        # 2017-10-27
        # if self.credit == datetime.timedelta(0):
        #     return 'Unlimited'
        # s = self.credit.total_seconds()
        d, r = divmod(s, 60 * 60 * 24)
        h, r = divmod(r, 3600)
        m, s = divmod(r, 60)
        # return '%02d:%02d:%02d' % (h, m, s)
        if d > 0:
            return '%d Day %d:%02d:%02d' % (d, h, m, s)
        elif h > 0:
            return '%d:%02d:%02d' % (h, m, s)
        else:
            return '%d:%02d' % (m, s)

    def push_expired(self):
        from progress.models import Progress, Account as ProgressAccount
        for progress in Progress.objects.filter(item=self):
            progress.push_expired()
        ProgressAccount.update_progress(self.account_id)

    # Fix CIMB 2017-10-12 (Web)
    def time_left(self):
        from utils.duration import duration_display
        from django.utils import timezone
        import datetime

        if self.expired == datetime.timedelta(0):
            return 'Unlimited'

        if self.start is None:
            return 'Not start'

        if self.is_expired():
            return 'Expired'

        s = (self.expired - (timezone.now() - self.start)).total_seconds()
        return duration_display(datetime.timedelta(seconds=s))

        # 2017-10-27
        # d, r = divmod(s, 60 * 60 * 24)
        # h, r = divmod(r, 3600)
        # m, s = divmod(r, 60)
        # if d > 0:
        #     return '%d Day %d:%02d:%02d' % (d, h, m, s)
        # elif h > 0:
        #     return '%d:%02d:%02d' % (h, m, s)
        # else:
        #     return '%d:%02d' % (m, s)

    # Mark not use
    def expired_day(self):
        from django.utils import timezone
        import datetime

        if self.expired == datetime.timedelta(0):
            return 'Unlimited'

        if self.start is None:
            return 'Not start'

        expire_total_seconds = (self.expired - (timezone.now() - self.start)).total_seconds()
        expire = int(expire_total_seconds / float(60 * 60 * 24))
        if expire > 0:
            return '%s Days.' % expire

        h, r = divmod(expire_total_seconds, 3600)
        m, s = divmod(r, 60)

        if self.is_expired():
            return 'Expired'

        if h + m + s > 0:
            if h > 0:
                return '%d hrs, %d min' % (h, m)
            elif m > 0:
                return '%d min' % m
            elif s > 0:
                return '%d second' % s
        return 'Unknown'

    def is_expired(self):
        from django.utils import timezone
        import datetime
        if self.status == 3:
            return True
        elif self.expired == datetime.timedelta(0):
            return False
        else:
            expire_total_seconds = (self.expired - (timezone.now() - self.start)).total_seconds()
            if expire_total_seconds < 0:
                self.push_expired()
                # TODO: Mark remove @din
            return expire_total_seconds < 0

    def set_expired(self):
        if self.is_expired():
            self.push_expired()

    def api_expired_day(self):
        from utils.duration import duration_display
        from django.utils import timezone
        import datetime
        if self.expired == datetime.timedelta(0):
            return 0, 'No Expiry'

        if self.start is None:
            return -1, '-'

        if self.is_expired():
            return -1, 'Expired'

        s = (self.expired - (timezone.now() - self.start)).total_seconds()
        return 1, duration_display(datetime.timedelta(seconds=s))

        # 2017-10-27
        # d, r = divmod(s, 60 * 60 * 24)
        # h, r = divmod(s, 3600)
        # m, s = divmod(r, 60)
        # if d > 0:
        #     return 1, '%d Day %d:%02d:%02d' % (d, h, m, s)
        # elif h > 0:
        #     return 1, '%d:%02d:%02d' % (h, m, s)
        # else:
        #     return 1, '%d:%02d' % (m, s)

    def expired_date(self):
        import datetime
        if self.expired == datetime.timedelta(0):
            return None
        elif self.start is None:
            return None
        else:
            return self.start + self.expired

    def period_progress(self):
        from django.utils import timezone
        import datetime
        if self.expired == datetime.timedelta(0):
            return '-'
        if self.start is None:
            return '0%'
        else:
            if self.start + self.expired <= timezone.now():
                return '100%'
            else:
                total = self.expired.total_seconds()
                period = (timezone.now() - self.start).total_seconds()
                return '%s%%' % (int(float(period)/float(total)*100.0))

    def get_name(self):
        return self.content_type.get_object_for_this_type(id=self.content).name

    # TODO : remove call when use
    def get_content(self):
        from utils.content import get_content
        return get_content(self.content_type_id, self.content)

    def get_net_price(self):
        if self.discount == -1:
            return self.price
        else:
            return self.discount

    def api_progress_display(self):
        from django.conf import settings
        from django.utils import timezone
        from progress.models import Progress
        from content.models import Location as ContentLocation
        
        result = {'id': self.id,
                  'expired_remain_date': self.api_expired_day()[1],
                  'credit_remain_hour': self.credit_remain_hour(),
                  'percent': self.percent,
                  'start': '-',
                  'end': '-'}

        if self.start:
            result['start'] = timezone.get_current_timezone().normalize(self.start).strftime(settings.DATETIME_FORMAT_)

        if self.expired_date():
            result['end'] = timezone.get_current_timezone().normalize(self.expired_date()).strftime(settings.DATETIME_FORMAT_)

        if self.order.method == 22:
            result['add_method'] = 'Assignment'
        else:
            result['add_method'] = 'Self Enrollment'

        progress = Progress.pull(self,
                                 ContentLocation.pull(self.content_location_id),
                                 settings.CONTENT_TYPE_ID(self.content_type_id),
                                 self.content)
        if progress is not None:
            result['percent'] = int(progress.percent)
            result['is_complete'] = True if progress.status == 2 else False
            result['content_count'] = progress.content_count
            result['content_complete'] = progress.content_complete
            result['section_count'] = progress.section_count
            result['section_complete'] = progress.section_complete
            result['material_count'] = progress.material_count
            result['material_complete'] = progress.material_complete
            result['status'] = progress.status
            result['status_display'] = progress.get_status_display()
        else:
            result['is_complete'] = False
            result['content_count'] = 0
            result['content_complete'] = 0
            result['section_count'] = 0
            result['section_complete'] = 0
            result['material_count'] = 0
            result['material_complete'] = 0
            result['status'] = 0
            result['status_display'] = ''
        return result

    @staticmethod
    def api_pull(content_location, order_item_id, content_type, content_id, account):
        if order_item_id is not None:
            order_item = Item.pull(order_item_id)
            if order_item is None:
                return None, 700
            if not order_item.is_active():
                return None, 700
            elif account is None:
                return None, 700
            elif account.id != order_item.account_id:
                return None, 700
        elif account is not None:
            order_item = Item.pull_purchased(content_location,
                                             account,
                                             content_type,
                                             content_id)
        else:
            order_item = None
        return order_item, 200
            
    # For Course not Package
    def api_display(self, store=1):
        from django.conf import settings
        from progress.models import Progress
        from content.models import Location as ContentLocation
        
        d = {'id': self.id,
             'status': self.status,
             'amount': self.amount}

        progress = Progress.pull(self,
                                 ContentLocation.pull(self.content_location_id),
                                 settings.CONTENT_TYPE_ID(self.content_type_id),
                                 self.content)
        if progress is not None:
            d['percent'] = int(progress.percent)
            d['progress'] = progress.api_display()
        else:
            d['percent'] = 0
            d['progress'] = None
        return d

    def get_duration_display(self):
        if self.duration.total_seconds() % 3600 == 0:
            duration = "%d" % (int(self.duration.total_seconds() / 3600))
        else:
            duration = "%d:%02d" % (int(self.duration.total_seconds() / 3600), int((self.duration.total_seconds() / 60) % 60))
        return duration

    @staticmethod
    def accessibility(account, course, item=None): # TODO : add content_location
        from django.utils import timezone
        from api.views_api import STATUS_MSG
        import datetime
        d = {'code': 200,
             'msg': STATUS_MSG[200]}

        if item is None:
            item = Item.pull_purchased(None, account, settings.CONTENT_TYPE('course', 'course'), course.id) # FIXME : @kidsdev (content_location)

        if item is None:
            d['code'] = 723
            d['msg'] = STATUS_MSG[723]
        else:
            d['id'] = item.id
        try:
            d['progress'] = item.percent
        except:
            d['progress'] = 0

        if item is not None and item.credit == datetime.timedelta(0):
            d['expired'] = 0
            d['expired_str'] = 'Unlimited'
        elif item is not None and item.start is None:
            d['expired'] = item.credit.days
            d['expired_str'] = '%s days.' % item.credit.days
        elif item is not None:
            diff = timezone.now() - item.start
            if (item.expired - diff) <= datetime.timedelta(0):
                d['expired'] = 0
                d['expired_str'] = 'Expired.'
            else:
                d['expired'] = (item.expired - diff).days + 1
                d['expired_str'] = '%s day.' % ((item.expired - diff).days + 1)

        try:
            s = item.use_credit.total_seconds()
            h, r = divmod(s, 3600)
            m, s = divmod(r, 60)
            d['credit_use'] = '%02d:%02d hrs.' % (int(h), int(m))
        except:
            d['credit_use'] = '00:00'
        if item is not None and item.credit == datetime.timedelta(0):
            d['credit_remain'] = 'Unlimited'
        elif item is not None:
            try:
                s = (item.credit - item.use_credit).total_seconds()
                if s >= 0:
                    h, r = divmod(s, 3600)
                    m, s = divmod(r, 60)
                    d['credit_remain'] = '%02d:%02d hrs.' % (int(h), int(m))
                else:
                    d['credit_remain'] = 'Expired.'
            except:
                s = item.credit.total_seconds()
                h, r = divmod(s, 3600)
                m, s = divmod(r, 60)
                d['credit_remain'] = '%02d:%02d hrs.' % (int(h), int(m))
        d['device_list'] = []
        for device in account.device_set.select_related('uuid').all():
            d['device_list'].append(device.api_display())
        return d

    def net_revenues(self):
        return self.net - self.fee


class Promote(models.Model):
    order = models.ForeignKey(Order)
    promote_item = models.ForeignKey('promote.Promote')
    item = models.OneToOneField(Item, null=True)
    code = models.ForeignKey('promote.Code', related_name='order_promote_set', null=True)
    timestamp = models.DateTimeField(auto_now_add=True)


class BankTranferRecord(models.Model):
    BANK_LIST = (
        (-1, '-'),
        (1, 'SCB'),
        (2, 'KBANK'),
        (3, 'BBL'),
        (4, 'BAY'),
        (5, 'TMB'),
        (6, 'KTB'),
    )

    TRANFER_EXTRA = (
        (0, 'Normal'),
        (1, 'Credit'),
        (2, 'Gift'),
        (3, 'Mock'),
    )

    order = models.ForeignKey(Order, null=True, blank=True)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='tranfer_record_account', null=True, blank=True)
    price = models.FloatField()
    bank = models.IntegerField(choices=BANK_LIST)
    tel = models.CharField(max_length=30)
    tranfertime = models.DateTimeField(null=True, blank=True)
    image = models.ImageField(upload_to='banktransfer/image', null=True, blank=True)
    extra = models.IntegerField(choices=TRANFER_EXTRA, default=0)
    is_approve = models.BooleanField(default=False)
    timeupdate = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    raw = models.TextField(blank=True)

    class Meta:
        ordering = ['-timestamp']

    def save(self, *args, **kwargs):
        from utils.models_image_delete import image_delete_pre_save
        image_delete_pre_save(self)
        super(self.__class__, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        from utils.models_image_delete import image_delete_pre_delete
        image_delete_pre_delete(self)
        super(self.__class__, self).delete(*args, **kwargs)


class Summary(models.Model):
    # institute = models.ManyToManyField('institute.Institute')
    provider = models.ManyToManyField('provider.Provider') #Wait for migrate Institute -> Provider
    content_type = models.ForeignKey('contenttypes.ContentType')
    content = models.BigIntegerField(db_index=True)
    count = models.IntegerField(default=0)
    library = models.IntegerField(default=0) #Free
    sell = models.IntegerField(default=0)
    assign = models.IntegerField(default=0) #Pos / Assigment (Free || Sell)

    net = models.FloatField(default=0.0)
    fee = models.FloatField(default=0.0)
    timeupdate = models.DateTimeField(auto_now=True)

    @staticmethod
    def push(item):
        from django.utils import timezone
        now = timezone.now()
        summary = Summary.objects.filter(content_type_id=item.content_type_id,
                                         content=item.content).first()
        if summary is None:
            assign = libary = sell = 0
            if item.is_assign:
                assign = 1
            elif item.is_free:
                libary = 1
            else:
                sell = 1
            summary = Summary.objects.create(content_type_id=item.content_type_id,
                                             content=item.content,
                                             count=1,
                                             assign=assign,
                                             libary=libary,
                                             sell=sell)
        else:
            summary.count += 1
            if item.is_assign:
                summary.assign += 1
                summary.save(update_fields=['count', 'assign'])
            elif item.is_free:
                summary.libary += 1
                summary.save(update_fields=['count', 'libary'])
            else:
                summary.sell += 1
                summary.save(update_fields=['count', 'sell'])

    def get_content(self):
        from django.conf import settings

        from course.models import Course
        from question.models import Activity

        content_type_course = settings.CONTENT_TYPE('course', 'course')
        content_type_package = settings.CONTENT_TYPE('package', 'package')
        content_type_question = settings.CONTENT_TYPE('question', 'activity')

        if self.content_type_id == content_type_course.id:
            course = Course.pull(self.content)
            if course is not None:
                self.name = course.name
                self.type = content_type_course.model
        elif self.content_type_id == content_type_question.id:
            activity = Activity.pull(self.content)
            if activity is not None:
                self.name = activity.name
                self.type = content_type_question.model

    def net_revenues(self):
        return self.net - self.fee
