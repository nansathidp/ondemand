from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.utils import timezone

from order.models import Order
from coin.models import Coin, Volume as CoinVolume, Log as CoinLog

from coin.cached import cached_leverage, cached_coin_volume_update
from progress.cached import cached_progress_account_content


@login_required
def coin_pay_view(request, order_id):
    order = get_object_or_404(Order, id=order_id, account=request.user)
    leverage = cached_leverage(request.APP)
    coin_volume = CoinVolume.pull(request.user)
    msg = None
    if order.net * leverage.value > coin_volume.volume :
        msg = 'มีเหรียญไม่พอ'
        return redirect('coin:list')
    else:
        leverage = cached_leverage(request.APP)
        coin_volume.volume -= (order.net*leverage.value)
        coin_volume.save(update_fields=['volume'])
        cached_coin_volume_update(request.user, coin_volume)
        CoinLog.push(request.user, 2, order.net*leverage.value, 'Buy Order: %s'%order.id)
        order.status = 3
        order.method = 14
        order.timecomplete = timezone.now()
        order.save(update_fields=['status', 'method', 'timecomplete'])
        order.item_set.update(status=2)
        account_content = cached_progress_account_content(order.account_id, None)
        account_content.update_count(course=True, package=True, program=True, question=True,
                                     lesson_subscribe=True, lesson_owner=True)
        return redirect('profile')
    coin_list = Coin.objects.filter(is_use=True)
    return render(request,
                  'order/coin_pay.html',
                  {'order': order,
                   'coin_list': coin_list,
                   'msg': msg})
