from django.shortcuts import get_object_or_404, render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required

from django.http import Http404
from django.conf import settings

from order.models import Order, Bill
from paysbuy.models import Request as PaysbuyRequest, Response as PaysbuyResponse

from .views_paysbuy import response_data

import requests, re, json


def paysbuy_success(request, order, type=1, step=2):
    result = 1
    payload = {'psbID': settings.CONFIG(request.get_host(), 'PAYSBUY_ID'),
               'biz': settings.CONFIG(request.get_host(), 'PAYSBUY_USERNAME'),
               'secureCode': settings.CONFIG(request.get_host(), 'PAYSBUY_CODE'),
               'invoice': order.id}
    paysbuy_request = PaysbuyRequest.objects.create(order=order,
                                                    request=json.dumps(payload, indent=2),
                                                    type=type,
                                                    step=step)
    r = requests.post('%s/psb_ws/getTransaction.asmx/getTransactionByInvoice' % settings.CONFIG(request.get_host(), 'PAYSBUY_URL'),
                      data=payload, verify=False)
    paysbuy_request.response = r.text
    try:
        if re.search('<result>([0-9]*)</result>', r.text).group(1) == '00':
            order.buy_success(request.APP, request.get_host().split(':')[0])
            paysbuy_request.result = 20
        else:
            result = -1
            paysbuy_request.result = 22
    except:
        result = -2
        paysbuy_request.result = 21
    paysbuy_request.save(update_fields=['response', 'result'])
    return result


@csrf_exempt
@login_required
def status_view(request, order_id):
    order = get_object_or_404(Order, id=order_id)
    if order.account == request.user:
        is_success, order_id, paysbuy_response = response_data(request, 1)
        if is_success and order_id == order.id:
            result = paysbuy_success(request, order)
            if result == 1:
                return redirect('order:paymentsuccess', order.id)
        elif paysbuy_response.status == 2:
            return redirect('order:paymentsuccess', order.id)
        return redirect('order:paymentfail', order.id)
    else:
        return redirect('order:home')


def status_fail_view(request, order_id):
    order = get_object_or_404(Order, id=order_id)
    if order.account == request.user:
        return render(request, 'order/fail.html')
    else:
        raise Http404("Order invalid")


def status_success_view(request, order_id):
    order = get_object_or_404(Order, id=order_id)
    if order.account == request.user:
        return render(request, 'order/success.html',
                      {'method': int(order.method),
                       'order': order})
    else:
        return redirect('order:home')
