from django.shortcuts import render, redirect, get_object_or_404
from api.v4.views import json_render
from django.conf import settings
from ondemand.views_base import response

from order.models import Order
from mailer.models import Mailer

from account.cached import cached_api_account_auth
from .views_banktransfer import push_bank_tranfer_record

def webview_bankinfo_view(request, order_id):
    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)

    if uuid is None:
        return response(request, 202)
    if token is None:
        return response(request, 204)
    
    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return response(request, 703)
    except:
        return response(request, 209)

    account = cached_api_account_auth(token, uuid)
    if account is None:
        return response(request, 400)

    order = get_object_or_404(Order, id=order_id, account=account, status__in=[1, 2])
    if order.method != 4 or order.status != 2:
        order.method = 4
        order.status = 2
        order.save(update_fields=['method', 'status'])
        
    contact_email = settings.CONFIG(request.get_host(), 'CONTACT_EMAIL')
    site_url = settings.BASE_URL(request)
    Mailer.send_order_email(order, contact_email, site_url)

    if request.method == 'POST':
        push_bank_tranfer_record(request, order, account)
        return redirect('order:webview_banktranfer_success')
    else:
        success = False
    return render(request,
                  'order/webview_bankinfo.html',
                  {
                    'order': order,
                    'user': account,
                    'success': success
                   })
