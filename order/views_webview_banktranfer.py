from django.shortcuts import render

from order.models import Order, BankTranferRecord

from ondemand.views_base import response
from account.cached import cached_api_account_auth

def webview_banktranfer_list_view(request):
    order_list = []
    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)
    if uuid is None:
        return response(request, 202)
    if token is None:
        return response(request, 204)
    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return response(request, 703)
    except:
        return response(request, 209)

    account = cached_api_account_auth(token, uuid)
    if request.APP.is_master:
        history_order_list = Order.objects.filter(account=account, price__gt=0)
    else:
        history_order_list = Order.objects.filter(account=account, price__gt=0, app=request.APP)
    for order in history_order_list:
        if order.method != 4:
            continue
        bank_transfer_record = BankTranferRecord.objects.filter(order=order)
        if not bank_transfer_record:
            order.has_transfer = False
            order_list.append(order)
        else:
            order.has_transfer = True

    return render(request,
                  'order/webview_banktranfer.html',
                  {'order_list': order_list,
                   'uuid': uuid,
                   'token': token,
                   'store': store,
                   'user': account})
