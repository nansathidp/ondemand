from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404

from order.models import Order, BankTranferRecord
from order.views_banktransfer import push_bank_tranfer_record


@login_required
def history_view(request):
    order_id = request.POST.get('order_id', None)
    if request.method == 'POST' and order_id is not None:
        order = get_object_or_404(Order, id=order_id, account=request.user)
        push_bank_tranfer_record(request, order, request.user)
        success = True
    else:
        success = False
    order_list = Order.objects.filter(account=request.user, price__gt=0)
    return render(request,
                  'order/history.html',
                  {'order_list': order_list,
                   'success': success})


@login_required
def send_bankslip_view(request):
    order_list = []

    order_id = request.POST.get('order_id', None)
    if request.method == 'POST' and order_id is not None:
        order = get_object_or_404(Order, id=order_id, account=request.user)
        push_bank_tranfer_record(request, order, request.user)
        success = True
    else:
        success = False

    history_order_list = Order.objects.filter(account=request.user, price__gt=0)
        
    for order in history_order_list:
        if order.method != 4:
            continue
        bank_transfer_record = BankTranferRecord.objects.filter(order=order)  
        if not bank_transfer_record:
            order.has_transfer = False
            order_list.append(order)
        else:
            order.has_transfer = True
    return render(request,
                  'order/history.html',
                  {'order_list': order_list,
                   'title': "แจ้งชำระเงิน (เฉพาะโอนเงินทางธนาคาร)",
                   'success': success})


@login_required
def history_detail_view(request, order_id):
    order = get_object_or_404(Order, id=order_id, account=request.user)
    return render(request,
                  'order/orderdetail.html',
                  {'order': order})
