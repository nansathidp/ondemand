from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

from order.models import Order


@login_required
def home_view(request):
    order = Order.pull_first(request.user, 1, request.APP)
    return redirect('order:cart', order.id)
