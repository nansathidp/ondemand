from django.shortcuts import get_object_or_404, render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404

from order.models import BankTranferRecord

@login_required
def approve_view(request, banktransfer_id):
    if not request.user.is_admin:
        raise Http404

    bank_transfer_record = get_object_or_404(BankTranferRecord, id=banktransfer_id)

    order = bank_transfer_record.order
    order.buy_success(request.APP, request.get_host().split(':')[0])

    bank_transfer_record.is_approve = True
    bank_transfer_record.save(update_fields=['is_approve'])
    return render(request,
                  'order/bank_tranfer_approve.html',
                  {'order': order})
