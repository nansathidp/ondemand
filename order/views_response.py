from api.views_api import json_render
from django.views.decorators.csrf import csrf_exempt

from order.models import Order
from paysbuy.models import Response as PaysbuyResponse
from order.views_status import paysbuy_success

import re
import traceback

@csrf_exempt
def response_view(request):
    if request.method == 'POST':
        response = request.body
        if type(response) == type(b''):
            response = response.decode('utf-8')
        #print(response)    
        paysbuy_response = PaysbuyResponse.objects.create(text=response, type=2)
        try:
            try:
                paysbuy_response.method = int(re.search('method=([0-9]*)', response).group(1))
            except:
                pass
            
            result = re.match('result=([0-9]*)&', response).group(1)
            order_id = int(result[2:])
            payment_status = int(result[:2])
            order = Order.objects.get(id=order_id)
            paysbuy_response.order = order
            paysbuy_response.status = payment_status
            paysbuy_response.amt = int(re.search('amt=([0-9]*)', response).group(1))
            paysbuy_response.approve_code = re.search('apCode=([0-9]*)', response).group(1)
            
            paysbuy_response.save()
            if payment_status == 0:
                paysbuy_success(request, order, step=3)
        except:
            paysbuy_response.text = '%s\r\n=======\r\n%s'%(paysbuy_response.text,
                                                           traceback.format_exc())
            paysbuy_response.save()

    return json_render({}, 200)
