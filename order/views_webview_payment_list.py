from django.shortcuts import render, get_object_or_404
from django.conf import settings

from order.models import Order
from aismpay.models import Level

from account.cached import cached_api_account_auth
from ondemand.views_base import response


def webview_payment_list_view(request, order_id):
    is_display_mpay = False
    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)
    if uuid is None:
        return response(request, 202)
    if token is None:
        return response(request, 204)

    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return response(request, 703)
    except:
        return response(request, 209)
    token = token.replace('display_header=0', '')
    account = cached_api_account_auth(token, uuid)

    if account is None:
        return response(request, 400)

    order = get_object_or_404(Order, id=order_id, account=account, status__in=[1, 2])
    if request.method == 'POST' and 'redeem' in request.POST:
        code = request.POST.get('code', '')
        order.push_promote_code(account, code)

    if settings.CONFIG(request.get_host(), 'PLATFORM_ID') == 2:
        content_type_order = settings.CONTENT_TYPE('order', 'order')
        level = Level.pull(content_type_order, order)
        if level is not None:
            is_display_mpay = True

    return render(request,
                  'order/webview_payment_list.html',
                  {'address': account.address_set.first(),
                   'uuid': uuid,
                   'token': token,
                   'store': store,
                   'order': order,
                   'is_shipping': order.is_shipping(),
                   'user': account,
                   'is_display_mpay': is_display_mpay})
