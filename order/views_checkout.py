from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.conf import settings

from order.models import Order
from aismpay.models import Level
from coin.models import Volume as CoinVolume

from coin.cached import cached_leverage

@login_required
def checkout_view(request, order_id=None):
    if order_id is None:
        order = Order.pull_first(request.user, 1, request.APP)
    else:
        order = get_object_or_404(Order, id=order_id, account=request.user)

    if order.net == 0.0:
        order.buy_success(request.APP, request.get_host().split(':')[0])
        return redirect('profile')

    if request.method == 'POST' and 'redeem' in request.POST:
        code = request.POST.get('code', '')
        order.push_promote_code(request.user, code)
    mpay = request.GET.get('mpay', None)
    is_display_mpay = False
    is_coin_pay = False
    if settings.CONFIG(request.get_host(), 'PLATFORM_ID') == 2:
        content_type_order = settings.CONTENT_TYPE('order', 'order')
        level = Level.pull(content_type_order, order)
        if level is not None:
            is_display_mpay = True
    content_type_coin = settings.CONTENT_TYPE('coin', 'coin')
    if not order.item_set.filter(content_type_id=content_type_coin.id).exists():
        is_coin_pay = True
    coin_volume = CoinVolume.pull(request.user)
    leverage = cached_leverage(request.APP)
    is_coin_valid = False
    return render(request,
                  'order/checkout.html',
                  {'order': order,
                   'address': request.user.address_set.first(),
                   'is_display_mpay': is_display_mpay,
                   'is_coin_pay': is_coin_pay,
                   'is_shipping': order.is_shipping(),
                   'is_coin_valid': is_coin_valid,
                   'mpay': mpay,
                   'coin_volume': coin_volume})
