import requests
import re
import json

from django.shortcuts import redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.conf import settings

from ondemand.views_base import response
from order.models import Order
from paysbuy.models import Request as PaysbuyRequest
from account.cached import cached_api_account_auth


def webview_paysbuy_view(request, order_id):
    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)

    if uuid is None:
        return response(request, 202)
    if token is None:
        return response(request, 204)

    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return response(request, 703)
    except:
        return response(request, 209)

    account = cached_api_account_auth(token, uuid)
    if account is None:
        return response(request, 400)

    order = get_object_or_404(Order, id=order_id, account=account, status__in=[1, 2])

    try:
        method = int(request.GET.get('method', 0))
    except:
        method = 0
        
    if method == 2:
        order.method = method
    elif method == 3:
        order.method = method
        method = 5
    elif method == 6:
        order.method = 5
    order.status = 2
    order.save(update_fields=['method', 'status'])

    payload = {'psbID': settings.CONFIG(request.get_host(), 'PAYSBUY_ID'),
               'username': settings.CONFIG(request.get_host(), 'PAYSBUY_USERNAME'),
               'secureCode': settings.CONFIG(request.get_host(), 'PAYSBUY_CODE'),
               'inv': order.id,
               'itm': settings.CONFIG(request.get_host(), 'BASE_NAME'),
               'amt': order.net,
               'paypal_amt': '',
               'curr_type': 'TH',
               'com': '',
               'method': method,
               'language': 'T',
               'resp_front_url': '%s%s' % (settings.BASE_URL(request), reverse('order:webview_paysbuy_status', args=[order.id])),
               'resp_back_url': '%s%s' % (settings.BASE_URL(request), reverse('order:response')),
               'opt_fix_redirect': '',
               'opt_fix_method': '1',
               'opt_name': '',
               'opt_email': '',
               'opt_mobile': '',
               'opt_address': '',
               'opt_detail': '',
               'opt_param': 'device_display=m'}

    paysbuy_request = PaysbuyRequest(order=order,
                                     request=json.dumps(payload, indent=2),
                                     type=3,
                                     step=1)
    paysbuy_request.save()
    r = requests.post(
        '%s/api_paynow/api_paynow.asmx/api_paynow_authentication_v3' % settings.CONFIG(request.get_host(), 'PAYSBUY_URL'),
        data=payload, verify=False)
    paysbuy_request.response = json.dumps(r.text, indent=2)
    paysbuy_request.save(update_fields=['response'])

    result = re.search('00(.+)</string>', r.text).group(1)
    return redirect('%s/paynow.aspx?refid=%s' % (settings.CONFIG(request.get_host(), 'PAYSBUY_URL'), result))
