from django.test import TestCase
from django.conf import settings

from .models import Order, Item

from .tests import create_app, create_user, create_course

# class LocationTests(TestCase):
    
#     def test_buy(self):
#         app = create_app()

#         order_item = Order.buy(app,
#                                1,
#                                settings.CONTENT_TYPE('course', 'course'),
#                                create_course(),
#                                create_user())
#         self.assertEqual(type(order_item), Item)
