from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.conf import settings

from order.models import Order
from payatall.models import Request

import datetime

@login_required
def payatall_pay_view(request, order_id):
    order = get_object_or_404(Order, id=order_id, account=request.user)
    payatall_request = Request.send(order, request.get_host(), settings.BASE_URL(request))
    return HttpResponse(payatall_request.html)
