from .models import Order
from paysbuy.models import Response as PaysbuyResponse

import re
import traceback

def response_data(request, type_payment):
    is_success = True
    order_id = None
    response = request.body
    if type(response) == type(b''):
        response = response.decode('utf-8')
    paysbuy_response = PaysbuyResponse(text=response, type=type_payment)
    try:
        try:
            paysbuy_response.method = int(re.search('method=([0-9]*)', response).group(1))
        except:
            pass
        
        result = re.match('result=([0-9]*)&', response).group(1)
        order_id = int(result[2:])
        paysbuy_response.status = int(result[:2])
        if paysbuy_response.status != 0:
            is_success = False
        paysbuy_response.order = Order.objects.get(id=order_id)
        paysbuy_response.amt = int(re.search('amt=([0-9]*)', response).group(1))
        paysbuy_response.approve_code = re.search('apCode=([0-9]*)', response).group(1)
    except:
        is_success = False
        paysbuy_response.text = '%s\r\n=======\r\n%s'%(paysbuy_response.text,
                                                       traceback.format_exc())
    paysbuy_response.save()
    return is_success, order_id, paysbuy_response
