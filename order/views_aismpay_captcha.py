from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404

from aismpay.models import ReserveVolume, ConfirmCaptha, ChargeReservation
from order.models import Order
from order.views_status import status_success_view


@login_required
def aismpay_captcha_view(request, reserve_volume_id):
    reserve_volume = get_object_or_404(ReserveVolume, id=reserve_volume_id, account=request.user)
    result = None
    if request.method == 'POST':
        captcha = request.POST.get('captcha', '')
        is_success, confirm_captha = ConfirmCaptha.send(reserve_volume, captcha)
        if is_success:
            is_success, charge_reservation = ChargeReservation.send(confirm_captha)
            if is_success:
                try:
                    order = Order.objects.get(id=reserve_volume.content)
                except:
                    return 'Order Not Found'
                order.buy_success(request.APP, request.get_host().split(':')[0])
                return status_success_view(request, order.id)
            else:
                result = charge_reservation.result
        else:
            result = confirm_captha.result
    return render(request,
                  'order/aismpay_captcha.html',
                  {'reserve_volume': reserve_volume,
                   'result': result})
