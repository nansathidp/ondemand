from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone

from django.conf import settings
from ondemand.views_base import response
from order.models import Order, Bill
from api.v4.views import json_render
from account.cached import cached_api_account_auth
from aismpay.models import ReserveVolume, ConfirmCaptha, ChargeReservation
from mailer.models import Mailer


def webview_aismpay_captcha_view(request, reserve_volume_id):
    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)

    if uuid is None:
        return response(request, 202)
    if token is None:
        return response(request, 204)

    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return response(request, 703)
    except:
        return response(request, 209)

    account = cached_api_account_auth(token, uuid)
    if account is None:
        return response(request, 400)

    reserve_volume = get_object_or_404(ReserveVolume, id=reserve_volume_id, account=account)
    result = None
    if request.method == 'POST':
        captcha = request.POST.get('captcha', '')
        is_success, confirm_captha = ConfirmCaptha.send(reserve_volume, captcha)
        if is_success:
            is_success, charge_reservation = ChargeReservation.send(confirm_captha)
            if is_success:
                try:
                    order = Order.objects.get(id=reserve_volume.content)
                except:
                    return "ไม่พบรายการสั่งซื้อ"
                order.buy_success(request.APP, request.get_host().split(':')[0], 6)
                return redirect('order:webview_paysbuy_status_success', order.id)
            else:
                result = charge_reservation.result
        else:
            result = confirm_captha.result
    return render(request,
                  'order/webview_aismpay_captcha.html',
                  {'reserve_volume': reserve_volume,
                   'result': result})
