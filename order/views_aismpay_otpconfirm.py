from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from order.models import Order
from aismpay.models import OtpRequest, OtpConfirm, ReserveVolume, Level

@login_required
def aismpay_otpconfirm_view(request, otp_request_id):
    otp_request = get_object_or_404(OtpRequest, id=otp_request_id, account=request.user)
    result = None
    if request.method == 'POST':
        otp = request.POST.get('otp', '')
        is_success, otp_confirm = OtpConfirm.send(otp_request, otp)
        if is_success:
            level = Level.pull(otp_request.content_type, otp_request.content_type.get_object_for_this_type(id=otp_request.content))
            if level is not None:
                is_success, reserve_volume = ReserveVolume.send(request.user,
                                                                1,
                                                                otp_request.content_type,
                                                                otp_request.content,
                                                                otp_confirm.tel,
                                                                level.volume,
                                                                level.service,
                                                                level.volume,
                                                                otp_request.transaction_id,
                                                                otp_confirm.pass_key,
                                                                'Info',
                                                                otp_confirm)
                return redirect('order:aismpay_captcha', reserve_volume.id)
            else:
                result = 'ราคานี้ ไม่สามารถซื้อด้วย AIS mPay ได้'
        else:
            result = otp_confirm.status

    return render(request,
                  'order/aismpay_otpconfirm.html',
                  {'result': result,
                    'otp_request': otp_request})
