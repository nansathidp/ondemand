from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.utils import timezone

from order.models import Order, BankTranferRecord
from mailer.models import Mailer
import datetime, json

def push_bank_tranfer_record(request, order, account):
    """
    use order.views_banktransfer.py and order.views_webview_bankinfo.py
    """
    raw = {'date': request.POST.get('transfer_date', '-'),
           'hours': request.POST.get('hours', '-'),
           'minutes': request.POST.get('minutes', '-'),
           'amount': request.POST.get('amount', '-')}
    try:
        image = request.FILES['bank_transfer_file']
        if image.size > 2097152:
            image = None
            raw['image'] = 'File size is too large.'
    except:
        image = None
    try:
        month_date_year = request.POST.get('transfer_date')
        mdy_split = month_date_year.split('/')
        hours = request.POST.get('hours')
        minutes = request.POST.get('minutes')
        #transfer_time = datetime.datetime(int(mdy_split[2]), int(mdy_split[0]), int(mdy_split[1]), int(hours), int(minutes), 00)
        transfer_time = timezone.make_aware(datetime.datetime(int(mdy_split[2]), int(mdy_split[0]), int(mdy_split[1]), int(hours), int(minutes), 00))
    except:
        transfer_time = None

    try:
        amount = float(request.POST.get('amount').replace(',', ''))
    except:
        amount = 0.0
    bank = request.POST.get('bank', -1)
    bank_transfer_record = BankTranferRecord.objects.create(order=order,
                                                            account=account,
                                                            price=amount,
                                                            bank=bank,
                                                            tel=request.POST.get('tel'),
                                                            image=image,
                                                            tranfertime=transfer_time,
                                                            raw=json.dumps(raw, indent=2))
    contact_email = settings.CONFIG(request.get_host(), 'CONTACT_EMAIL')
    site_url = settings.BASE_URL(request)
    Mailer.send_notify_email(order, bank_transfer_record, contact_email, site_url)
    #debug
    #Mailer.send_notify_email(order, bank_transfer_record, 'kidsdev@gmail.com', site_url)

@login_required
def bank_info_view(request, order_id):
    order = get_object_or_404(Order, id=order_id, account=request.user)
    if order.method != 4:
        return redirect('order:history')
    if order.net == 0.0 or order.price == 0.0:
        return redirect('order:home')
    
    contact_email = settings.CONFIG(request.get_host(), 'CONTACT_EMAIL')
    site_url = settings.BASE_URL(request)
    Mailer.send_order_email(order, contact_email, site_url)
    
    if request.method == 'POST':
        push_bank_tranfer_record(request, order, request.user)
        success = True
    else:
        success = False
        
    # ? user can try to submit payment?
    #if BankTranferRecord.objects.filter(order=order).count() > 0:
    #    redirect('order:paymentsuccess', order.id, 4)
    return render(request,
                  'order/bankinfo.html',
                  {'order': order,
                   'success': success})
