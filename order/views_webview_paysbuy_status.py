from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from .models import Order
from .views_paysbuy import response_data
from .views_status import paysbuy_success


@csrf_exempt
def webview_paysbuy_status_view(request, order_id):
    order = get_object_or_404(Order, id=order_id)
    is_success, order_id, paysbuy_response = response_data(request, 3)
    if paysbuy_response.status == 0 and order_id == order.id:
        result = paysbuy_success(request, order)
        return redirect('order:webview_paysbuy_status_success', order.id)
    elif paysbuy_response.status == 2:
        if paysbuy_response.method == 6:
            return redirect('order:webview_paysbuy_status_counterservice_success')
    return redirect('order:webview_paysbuy_status_fail')


def webview_paysbuy_fail_view(request):
    return render(request,
                  'order/webview_paysbuy_fail.html',
                  {})


def webview_paysbuy_success_view(request, order_id):
    order = get_object_or_404(Order, id=order_id)
    return render(request,
                  'order/webview_paysbuy_success.html',
                  {'order': order})


def webview_paysbuy_success_banktranfer_view(request):
    return render(request, 'order/webview_banktranfer_success.html', {})


def webview_paysbuy_success_counterservice_view(request):
    return render(request,
                  'order/webview_counterservice_success.html',
                  {})

# TEST ZONE


def webview_success_view(request, order_id):
    order = get_object_or_404(Order, id=order_id)
    return render(request,
                  'order/webview_payment_success.html',
                  {'order': order})
