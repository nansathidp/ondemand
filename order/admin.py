from django.contrib import admin

from django.utils import timezone
from django.core.urlresolvers import reverse
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from order.models import *
from coin.models import Coin, Volume as CoinVolume
from order.models import Bill

from progress.cached import cached_progress_account_content
import datetime
import json


class ItemInline(admin.TabularInline):
    model = Item
    fields = (
        'id', 'content_type', 'content', 'content_name', 'amount', 'price', 'discount', 'net', 'status', 'is_shipping',
        'use_credit',
        'percent', 'expired', 'start', 'is_display')
    readonly_fields = ['status']


class OrderPriceListFilter(admin.SimpleListFilter):
    title = _('Order type')
    parameter_name = 'order_type'

    def lookups(self, request, model_admin):
        return (('Free', _('Free')),
                ('Paid', _('Paid')))

    def queryset(self, request, queryset):
        if self.value() == 'Free':
            return queryset.filter(net=0)
        if self.value() == 'Paid':
            return queryset.filter(net__gt=0)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'account', 'price', 'discount', 'net', 'status', 'method', 'app', 'store', 'timecomplete', 'timestamp')
    list_filter = ('account__is_admin', OrderPriceListFilter, 'store', 'method', 'status', 'app')
    search_fields = ['account__email', 'id']
    readonly_fields = ('id', 'account', 'status')
    actions = ['order_approve', 'order_approve_for_tester', 'order_item_start', 'revoke']
    inlines = [ItemInline]

    def order_approve(self, request, queryset):
        now = timezone.now()
        content_type_coin = settings.CONTENT_TYPE('coin', 'coin')
        for order in queryset:
            # Sent Mail
            for item in order.item_set.all():
                item.status = 2
                account_content = cached_progress_account_content(item.account_id, None)
                account_content.update_count(course=True, package=True, program=True, question=True,
                                             lesson_subscribe=True, lesson_owner=True)
                item.save(update_fields=['status'])
                if item.content_type.id == content_type_coin.id:
                    if order.status == 3:
                        continue
                    coin = Coin.objects.get(id=item.content)
                    if coin.type == 0:
                        CoinVolume.push(request.APP, order.account, coin.volume)
                    elif coin.type == 1:
                        data = json.loads(item.data)
                        CoinVolume.push(request.APP, order.account, data['price'])
            bill = Bill.get_bill(order)
        queryset.update(extra=1, status=3, timecomplete=now)
    order_approve.short_description = "Approve"

    def order_approve_for_tester(self, request, queryset):
        now = timezone.now()
        content_type_coin = settings.CONTENT_TYPE('coin', 'coin')
        for order in queryset:
            order.clear_price()
            for item in order.item_set.all():
                if item.content_type.id == content_type_coin.id:
                    if order.status == 3:
                        continue
                    coin = Coin.objects.get(id=item.content)
                    if coin.type == 0:                        
                        CoinVolume.push(request.APP, order.account, coin.volume)
                    elif coin.type == 1:
                        data = json.loads(item.data)
                        CoinVolume.push(request.APP, order.account, data['price'])
            bill = Bill.objects.filter(order=order).delete()
            order.buy_success(request.APP, request.get_host().split(':')[0])
        queryset.update(extra=1, status=3, timecomplete=now)
    order_approve_for_tester.short_description = "Approve For Tester"

    def revoke(self, request, queryset):
        from order.cached import cached_order_item_account_delete
        now = timezone.now()
        for order in queryset:
            order.revoke()
            Bill.objects.filter(order=order).delete()
            cached_order_item_account_delete(order.account_id)
        queryset.update(status=-1, timecomplete=now)

    revoke.short_description = "Revoke"

    def order_item_start(self, request, queryset):
        now = timezone.now()
        for order in queryset:
            if order.status == 3:
                order.item_set.filter(start=None).update(start=now)
    order_item_start.short_description = "Set item start"


@admin.register(Bill)
class BillAdmin(admin.ModelAdmin):
    list_display = ('order', 'get_order_account', 'get_net', 'get_method', 'get_order_app', 'no', 'timestamp')
    list_filter = ('order__method',)

    def get_order_account(self, obj):
        return obj.order.account.email
    get_order_account.short_description = 'Account'

    def get_order_app(self, obj):
        return obj.order.app

    get_order_app.short_description = 'App'

    def get_net(self, obj):
        return obj.order.net

    get_net.short_description = 'Net'
    get_net.admin_order_field = 'order__net'

    def get_method(self, obj):
        return obj.order.get_method_display()

    get_method.short_description = 'Method'
    get_method.admin_order_field = 'order__method'


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_order', 'content_location', 'get_content_type', 'content',
                    'amount', 'price', 'discount', 'net', 'is_free', 'status', 'duration',
                    'total_duration', 'use_credit', 'percent', 'credit', 'expired', 'start', 'is_display', 'timestamp')
    readonly_fields = ('account', 'order', 'content_type')
    list_filter = ('content_type', 'status', 'order__status')
    actions = ['reset_start', 'set_package_expire', 'increase_expire_date_1', 'increase_expire_date_2', 'increase_expire_date_5']
    search_fields = ['id', 'content']

    def set_package_expire(self, request, queryset):
        import pytz
        expire_date = datetime.datetime(2016, 5, 15, 0, 0, 0).replace(tzinfo=pytz.utc)
        now = datetime.datetime.now().replace(tzinfo=pytz.utc)
        for item in queryset:
            if item.start is None:
                item.start = now
            item.expired = expire_date - item.start
            item.save()

    def reset_start(self, request, queryset):
        queryset.update(start=None)

    def increase_expire_date_1(self, request, queryset):
        for item in queryset:
            item.expired = item.expired + datetime.timedelta(days=1)
            item.save(update_fields=['expired'])

    increase_expire_date_1.short_description = "Expire date + 1 Day"

    def increase_expire_date_2(self, request, queryset):
        for item in queryset:
            item.expired = item.expired + datetime.timedelta(days=2)
            item.save(update_fields=['expired'])

    increase_expire_date_2.short_description = "Expire date + 2 Day"

    def increase_expire_date_5(self, request, queryset):
        for item in queryset:
            item.expired = item.expired + datetime.timedelta(days=5)
            item.save(update_fields=['expired'])

    increase_expire_date_5.short_description = "Expire date + 5 Day"

    def get_order(self, obj):
        return obj.order_id

    get_order.short_description = 'Order ID'

    def get_content_type(self, obj):
        return settings.CONTENT_TYPE_ID(obj.content_type_id).model
    get_content_type.short_description = 'TYPE'


@admin.register(Promote)
class PromoteAdmin(admin.ModelAdmin):
    list_display = ('order', 'item', 'code', 'timestamp')


@admin.register(BankTranferRecord)
class BankTranferRecordAdmin(admin.ModelAdmin):
    list_display = (
        'order', 'account', 'get_net', 'price', 'bank', 'tel', 'tranfertime', 'image', 'is_approve', 'timestamp',
        'approve')
    readonly_fields = (
        'order', 'account', 'get_net', 'price', 'tel', 'tranfertime', 'is_approve', 'timestamp',
        'approve')

    def approve(self, instance):
        if instance.id is None:
            return '-'
        else:
            url = reverse('order:bank_transfer_approve', args=(instance.id,))
            return format_html(u'<a href="{}" target="_blank">Approve</a>', url)

    def get_net(self, obj):
        return obj.order.net

    get_net.short_description = 'Order Price'
    get_net.admin_order_field = 'order__net'
