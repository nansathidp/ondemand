from django.conf.urls import url

from .views import home_view
from .views_cart import cart_view
from .views_checkout import checkout_view
from .views_remove import remove_view
from .views_promote import promote_remove_view
from .views_pay import pay_view
from .views_response import response_view

from .views_history import history_view, send_bankslip_view, history_detail_view
from .views_banktransfer import bank_info_view
from .views_banktransfer_approve import approve_view

from .views_status import status_view, status_fail_view, status_success_view
from .views_coin_pay import coin_pay_view

from .views_payatall_pay import payatall_pay_view
from .views_payatall_response import payatall_success_view, payatall_cancel_view, payatall_confirm_view
from .views_aismpay_otprequest import aismpay_otprequest_view
from .views_aismpay_otpconfirm import aismpay_otpconfirm_view
from .views_aismpay_captcha import aismpay_captcha_view

from .views_webview_banktranfer import webview_banktranfer_list_view
from .views_webview_payment_list import webview_payment_list_view

from .views_webview_paysbuy import webview_paysbuy_view
from .views_webview_paysbuy_status import webview_paysbuy_status_view, webview_paysbuy_fail_view, webview_paysbuy_success_view, webview_paysbuy_success_banktranfer_view, webview_paysbuy_success_counterservice_view, webview_success_view

from .views_webview_bankinfo import webview_bankinfo_view
from .views_webview_banktranfer_success import webview_banktranfer_success_view
from .views_webview_aismpay_otprequest import webview_aismpay_otprequest_view
from .views_webview_aismpay_otpconfirm import webview_aismpay_otpconfirm_view
from .views_webview_aismpay_captcha import webview_aismpay_captcha_view

from .views_webview_payatall_pay import webview_payatall_pay_view

app_name = 'order'
urlpatterns = [
    url(r'^$', home_view, name='home'), #TODO: testing
    url(r'^(\d+)/$', cart_view, name='cart'), #TODO: testing
    url(r'^checkout/$', checkout_view, name='checkout'), #TODO: testing
    url(r'^checkout/(\d+)/$', checkout_view, name='checkout'), #TODO: testing

    url(r'^item/(\d+)/remove/$', remove_view, name='item_remove'), #TODO: testing
    url(r'^promote/(\d+)/remove/$', promote_remove_view, name='promote_remove'), #TODO: testing

    url(r'^(\d+)/pay/$', pay_view, name='pay'), #TODO: testing
    url(r'^response/$', response_view, name='response'), #TODO: testing

    url(r'^history/$', history_view, name='history'), #TODO: testing
    url(r'^history/tranfer/$', send_bankslip_view, name='history_tranfer'), #TODO: testing
    url(r'^detail/(\d+)/$', history_detail_view, name='detail'), #TODO: testing

    url(r'^(\d+)/bankinfo/$', bank_info_view, name='bank_info'), #TODO: testing
    url(r'^bank_transfer_approve/(\d+)/$', approve_view, name='bank_transfer_approve'), #TODO: testing

    url(r'^(\d+)/status/$', status_view, name='status'), #TODO: testing
    url(r'^(\d+)/status/fail/$', status_fail_view, name='paymentfail'), #TODO: testing
    url(r'^(\d+)/status/success/$', status_success_view, name='paymentsuccess'), #TODO: testing

    #Coin
    url(r'^coin/(\d+)/pay/$', coin_pay_view, name='coin_pay'), #TODO: testing

    #PAY@ALL
    url(r'^payatall/(\d+)/pay/$', payatall_pay_view, name='payatall_pay'), #TODO: testing
    url(r'^payatall/(\d+)/response/success/$', payatall_success_view, name='payatall_success'), #TODO: testing
    url(r'^payatall/(\d+)/response/cancel/$', payatall_cancel_view, name='payatall_cancel'), #TODO: testing
    url(r'^payatall/(\d+)/response/confirm/$', payatall_confirm_view, name='payatall_confirm'), #TODO: testing

    #AIS M Pay
    url(r'^aismpay/otprequest/(\d+)/$', aismpay_otprequest_view, name='aismpay_otprequest'), #TODO: testing
    url(r'^aismpay/otpconfirm/(\d+)/$', aismpay_otpconfirm_view, name='aismpay_otpconfirm'), #TODO: testing
    url(r'^aismpay/captcha/(\d+)/$', aismpay_captcha_view, name='aismpay_captcha'), #TODO: testing

    #MPay
    # url(r'^mpay/request/(\d+)/$', aismpay_otprequest_view, name='mpay_request'), #TODO: testing

    #WebView
    url(r'^webview/banktranfer/list/$', webview_banktranfer_list_view, name='webview_banktranfer_list'), #TODO: testing
    url(r'^webview/(\d+)/payment/list/$', webview_payment_list_view, name='webiew_payment_list'), #TODO: testing
    url(r'^webview/(\d+)/paysbuy/$', webview_paysbuy_view, name='webview_paysbuy'), #TODO: testing
    url(r'^webview/(\d+)/paysbuy/status/$', webview_paysbuy_status_view, name='webview_paysbuy_status'), #TODO: testing
    url(r'^webview/paysbuy/status/fail/$', webview_paysbuy_fail_view, name='webview_paysbuy_status_fail'), #TODO: testing
    url(r'^webview/(\d+)/paysbuy/status/success/$', webview_paysbuy_success_view, name='webview_paysbuy_status_success'), #TODO: testing
    url(r'^webview/paysbuy/status/banktranfer/success/$', webview_paysbuy_success_banktranfer_view, name='webview_paysbuy_status_banktranfer_success'), #TODO: testing
    url(r'^webview/paysbuy/status/counterservice/success/$', webview_paysbuy_success_counterservice_view, name='webview_paysbuy_status_counterservice_success'), #TODO: testing

    url(r'^webview/(\d+)/payment/bankinfo/$', webview_bankinfo_view, name='webview_bankinfo'), #TODO: testing
    url(r'^webview/payment/banktranfer/success/$', webview_banktranfer_success_view, name='webview_banktranfer_success'), #TODO: testing
    url(r'^webview/aismpay/otprequest/(\d+)/$', webview_aismpay_otprequest_view, name='webview_aismpay_otprequest'), #TODO: testing
    url(r'^webview/aismpay/otpconfirm/(\d+)/$', webview_aismpay_otpconfirm_view, name='webview_aismpay_otpconfirm'), #TODO: testing
    url(r'^webview/aismpay/captcha/(\d+)/$', webview_aismpay_captcha_view, name='webview_aismpay_captcha'), #TODO: testing
    url(r'^webview/payatall/(\d+)/pay/$', webview_payatall_pay_view, name='webview_payatall_pay'), #TODO: testing

    # Webview Test Success

    url(r'^webview/test/(\d+)/payment/status/success/$', webview_success_view, name='webview_test_payment_success'), #TEST] #TODO: testing

]
