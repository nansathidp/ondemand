from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

from order.models import Order

@login_required
def remove_view(request, item_id):
    order = Order.pull_first(request.user, 1, request.APP)
    order.remove_item(item_id)
    return redirect('order:home')
