from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.conf import settings

from order.models import Order
from payatall.models import Request

from account.cached import cached_api_account_auth
from ondemand.views_base import response

def webview_payatall_pay_view(request, order_id):
    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)

    if uuid is None:
        return response(request, 202)
    if token is None:
        return response(request, 204)

    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return response(request, 703)
    except:
        return response(request, 209)

    account = cached_api_account_auth(token, uuid)
    if account is None:
        return response(request, 400)
    order = get_object_or_404(Order, id=order_id, account=account)
    payatall_request = Request.send(order, request.get_host(), settings.BASE_URL(request))
    return HttpResponse(payatall_request.html)
