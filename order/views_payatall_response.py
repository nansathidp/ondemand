from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from payatall.models import Response, Inquiry
from order.models import Order

import json, traceback

@login_required
def payatall_success_view(request, order_id):
    return redirect('home')
    #return render(request,
    #              'order/payatall_success.html',
    #              {})

def payatall_cancel_view(request, order_id):
    return redirect('home')

@csrf_exempt
def payatall_confirm_view(request, order_id):
    if request.method == 'POST':
        d = {}
        for key, value in request.POST.items():
            d[key] = value
        response = Response.objects.create(text=json.dumps(d, indent=2))
        try:
            response.inv = d['inv']
            try:
                response.order = Order.objects.get(id=int(response.inv))
            except:
                response.order = None
            response.ptransid = d['ptransid']
            response.amt = float(d['amt'])
            if d['status'] == 'A':
                response.status = 1
            elif d['status'] == 'S':
                response.status = 2
            elif d['status'] == 'E':
                response.status = 3
            elif d['status'] == 'C':
                response.status = 4
            elif d['status'] == 'N':
                response.status = 5
            elif d['status'] == 'Z':
                response.status = 6
            response.save(update_fields=['inv', 'order', 'ptransid', 'amt', 'status'])
            if response.order is not None and response.status == 2 and response.amt == response.order.net:
                Inquiry.send_inquiry(request.APP, request.get_host().split(':')[0], response.order)
        except:
            response.text += '\n---\n'+traceback.format_exc()
            response.save(update_fields=['text'])
    return HttpResponse('S')
