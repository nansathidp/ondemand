from django import template

from order.models import Order, Item

register = template.Library()

@register.simple_tag
def pull_order(id):
    return Order.pull(id)

@register.simple_tag
def pull_order_item(id):
    return Item.pull(id)
