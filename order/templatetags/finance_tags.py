from django import template

register = template.Library()


@register.filter
def price_tag(price):
    if price == -1:
        return '-'
    elif price == 0:
        return 'FREE'
    else:
        return '%d BATH' % price
