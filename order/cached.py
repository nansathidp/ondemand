from django.conf import settings
from django.core.cache import cache

from order.models import Order, Item

from utils.cached.time_out import get_time_out


def cached_order(id):
    key = '%s_order_%s' % (settings.CACHED_PREFIX, id)
    result = cache.get(key)
    if result is None:
        result = Order.objects.filter(id=id).first()
        if result is not None:
            cache.set(key, result, get_time_out())
        else:
            return None
    return result

def cached_order_delete(id):
    key = '%s_order_%s' % (settings.CACHED_PREFIX, id)
    cache.delete(key)
    
def cached_order_item(item_id, is_force=False):
    key = '%s_order_item_%s' % (settings.CACHED_PREFIX, item_id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Item.objects.select_related('order').filter(id=item_id).first()
        if result is None:
            result = -1
        cache.set(key, result, get_time_out())
    return None if result == -1 else result


def cached_order_item_update(item):
    key = '%s_order_item_%s' % (settings.CACHED_PREFIX, item.id)
    cache.set(key, item, get_time_out())


def cached_order_item_delete(item):
    key = '%s_order_item_%s' % (settings.CACHED_PREFIX, item.id)
    cache.delete(key)


def cached_order_item_account(account, is_force=False):
    key = '%s_order_item_account_%s' % (settings.CACHED_PREFIX, account.id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Item.objects.values_list('id', 'content_type', 'content').filter(account=account, status=2)
        cache.set(key, result, get_time_out())
    return result


def cached_order_item_account_delete(account_id):
    key = '%s_order_item_account_%s' % (settings.CACHED_PREFIX, account_id)
    cache.delete(key)


def _item_purchased(content_type, order_item_list, item_list):
    for item in order_item_list:
        for item_id in item_list:
            if item[1] == content_type.id and item[2] == item_id:
                return item[0]
    return None


def _course_purchased(order_item_list, content_type, content_id):
    from program.cached import cached_program_has_course
    item_id = None
    for item in order_item_list:
        if item[1] == content_type.id and item[2] == content_id:
            item_id = item[0]
            break

    # In Program
    if item_id is None:
        content_type_program = settings.CONTENT_TYPE('program', 'program')
        program_list = cached_program_has_course(content_id)
        item_id = _item_purchased(content_type_program, order_item_list, program_list)

    if item_id is not None:
        return cached_order_item(item_id)
    else:
        return None


def _question_purchased(order_item_list, content_type, content_id):
    from program.cached import cached_program_has_question
    item_id = None
    for item in order_item_list:
        if item[1] == content_type.id and item[2] == content_id:
            item_id = item[0]
            break

    # In Program
    if item_id is None:
        content_type_program = settings.CONTENT_TYPE('program', 'program')
        program_list = cached_program_has_question(content_id)
        item_id = _item_purchased(content_type_program, order_item_list, program_list)

    if item_id is not None:
        return cached_order_item(item_id)
    else:
        return None


def _live_purchased(order_item_list, content_type, content_id):
    item_id = None

    for item in order_item_list:
        if item[1] == content_type.id and item[2] == content_id:
            item_id = item[0]
            break

    if item_id is not None:
        return cached_order_item(item_id)
    else:
        return None


def _course_purchased(order_item_list, content_type, content_id):
    from program.cached import cached_program_has_course
    item_id = None
    for item in order_item_list:
        if item[1] == content_type.id and item[2] == content_id:
            item_id = item[0]
            break

    # In Program
    if item_id is None:
        content_type_program = settings.CONTENT_TYPE('program', 'program')
        program_list = cached_program_has_course(content_id)
        item_id = _item_purchased(content_type_program, order_item_list, program_list)

    if item_id is not None:
        return cached_order_item(item_id)
    else:
        return None


def _course_material_purchased(order_item_list, content_type, content_id):
    from course.models import Material
    material = Material.pull(content_id)
    if material is None:
        return None
    else:
        return _course_purchased(order_item_list, content_type, material.course_id)


def _program_purchased(order_item_list, content_type, content_id):
    item_id = None
    for item in order_item_list:
        if item[1] == content_type.id and item[2] == content_id:
            item_id = item[0]
            break
    if item_id is not None:
        return cached_order_item(item_id)
    else:
        return None


# TODO: rename and use in class Item.pull only.!!
def cached_order_item_is_purchased(account, content_type, content_id, is_force=True):
    order_item_list = cached_order_item_account(account, is_force=is_force)
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_course_material = settings.CONTENT_TYPE('course', 'material')
    content_type_question = settings.CONTENT_TYPE('question', 'activity')
    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_type_live = settings.CONTENT_TYPE('live', 'live')

    if content_type.id == content_type_course.id:
        order_item = _course_purchased(order_item_list, content_type, content_id)
    elif content_type.id == content_type_course_material.id:
        order_item = _course_material_purchased(order_item_list, content_type_course, content_id)
    elif content_type.id == content_type_question.id:
        order_item = _question_purchased(order_item_list, content_type, content_id)
    elif content_type.id == content_type_program.id:
        order_item = _program_purchased(order_item_list, content_type, content_id)
    elif content_type.id == content_type_live.id:
        order_item = _live_purchased(order_item_list, content_type, content_id)
    else:
        order_item = None
    return order_item
