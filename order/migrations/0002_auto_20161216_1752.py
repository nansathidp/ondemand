# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-16 10:52
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('promote', '0001_initial'),
        ('provider', '0001_initial'),
        ('order', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app', '0001_initial'),
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='promote',
            name='code',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='order_promote_set', to='promote.Code'),
        ),
        migrations.AddField(
            model_name='promote',
            name='item',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='order.Item'),
        ),
        migrations.AddField(
            model_name='promote',
            name='order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='order.Order'),
        ),
        migrations.AddField(
            model_name='promote',
            name='promote_item',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='promote.Promote'),
        ),
        migrations.AddField(
            model_name='order',
            name='account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='order_account', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='order',
            name='app',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.App'),
        ),
        migrations.AddField(
            model_name='item',
            name='account',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='order_item_set', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='item',
            name='content_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='item',
            name='order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='order.Order'),
        ),
        migrations.AddField(
            model_name='item',
            name='providers',
            field=models.ManyToManyField(to='provider.Provider'),
        ),
        migrations.AddField(
            model_name='bill',
            name='order',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='order.Order'),
        ),
        migrations.AddField(
            model_name='banktranferrecord',
            name='account',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tranfer_record_account', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='banktranferrecord',
            name='order',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='order.Order'),
        ),
    ]
