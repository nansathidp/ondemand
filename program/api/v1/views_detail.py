from django.conf import settings

from ...models import Program
from analytic.models import Stat
from app.views import update_checkout_type
from access.models import Access as AccessContent
from dependency.models import Dependency
from rating.models import Rating
from content.models import Location as ContentLocation
from order.models import Item as OrderItem
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage, Page
from .views import check_key, check_require
from .views import json_render

from price.models import Price
from progress.models import Progress

from account.cached import cached_api_account_auth  # TODO: remove use check_auth (course/api/v1/views_push.py)

CONTENT_TYPE = {
    'course': 'Course',
    'activity': 'Test',
    'task': 'To-Do',
    'milestone': 'Milestone',
}

CONTENT_TYPE_ID = {
    'course': 0,
    'activity': 1,
    'task': 2,
    'milestone': 3,
}


def _review(request, content_location, course, account, page_number=1, program=None):
    result= {}
    code, token, uuid, store, install_version = check_require(request)
    if code != 200:
        return json_render({}, code)
    if program is None:
        rating =  Rating.objects.filter(
            content_type=settings.CONTENT_TYPE('course', 'course').id,
            content = course.id
        ).first()
    else:
        rating = Rating.objects.filter(
            content_type=settings.CONTENT_TYPE('program', 'program').id,
            content = program.id
        ).first()

    result['is_rated'] = Rating.is_rated(
        settings.CONTENT_TYPE('course', 'course').id,
        course.id,
        account
    )
    result = rating.api_display()
    temp_review = []
    review_list = rating.get_log_list()
    paginator = Paginator(review_list, 20)
    result['total'] = paginator.num_pages
    try:
        review = paginator.page(page_number)
        result['next'] = review.next_page_number()
        result['previous'] = review.previous_page_number()
        result['count'] = 20
    except PageNotAnInteger:
        review = paginator.page(1)
        result['next'] = 1
        result['previous'] = 1
    except EmptyPage:
        result['next'] = 1
        result['previous'] = 1
        review = paginator.page(paginator.num_pages)

    for review_item in review.object_list:
        temp_review.append(review_item.api_display())

    result['review_list'] = temp_review
    return json_render(result, code)


def detail_view(request, program_id, order_item_id=None):
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render({}, code)

    program = Program.pull(program_id)
    if program is None:
        return json_render({}, 4000)
    
    if program.type == 2:
        _content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        _content_type = settings.CONTENT_TYPE('program', 'program')

    price = Price.pull(_content_type, program_id, store)
    price_set = price.api_display()
    
    result = program.api_display(store, version, request.APP.app_version)

    result.update({
        'is_purchased': False,
        'price_set': price_set,
        'count_course': program.get_extra_value('count_course'),
        'count_test': program.get_extra_value('count_question'),
        'count_task': program.get_extra_value('count_task')
    })

    content_location = ContentLocation.pull_first(
        _content_type,
        program.id
    )
    # Account
    account = cached_api_account_auth(token, uuid)
    if order_item_id is not None:
        order_item = OrderItem.pull(order_item_id)
        if order_item is None:
            return json_render({}, 700)
        else:
            if account is None:
                return json_render({}, 700)
            elif account.id != order_item.account_id:
                return json_render({}, 700)
    elif account is not None:
        order_item = OrderItem.pull_purchased(
            content_location,
            account,
            _content_type,
            program.id
        )
    else:
        order_item = None

    if order_item is not None:
        result['is_purchased'] = True
        result['order_item_id'] = order_item.id
        result['item'] = order_item.api_progress_display()
        progress = Progress.pull(order_item,
                                 content_location,
                                 _content_type,
                                 program_id)
        if progress is not None:
            result['progress'] = progress.api_display()
        else:
            result['progress'] = None
        result['is_rated'] = Rating.is_rated(
            _content_type,
            program.id,
            account
        )
    else:
        result['progress'] = None
        result['is_rated'] = True  # Fix For Android, IOS check other object
        # result['is_rated'] = False
    result['content_list'] = []

    # Dependency
    result['dependency_list'] = []
    is_dependency, dependency_list = Dependency.check_content2(_content_type,
                                                               program.id,
                                                               account)
    for parent in dependency_list:
        result['dependency_list'].append(parent.api_display())
    result['is_dependency'] = is_dependency

    for program_item in program.item_set.all():
        program_item.get_content_cached()
        d = {
            'id': program_item.content,
            'content_type_id': CONTENT_TYPE_ID[program_item.content_type_cached.name],
            'content_type_display': CONTENT_TYPE[program_item.content_type_cached.name],
            'progress': None,
            'image': None,
        }

        try:
            d['image'] = program_item.content_cached.image.url if program_item.content_cached is not None and bool(program_item.content_cached.image) else None
        except:
            d['image'] = None

        if program_item.content_type_id == settings.CONTENT_TYPE('program', 'milestone').id:
            d['desc'] = program_item.content_cached.desc

        if order_item is not None:
            _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE_ID(program_item.content_type_id),
                                                           program_item.content,
                                                           parent1_content_type=_content_type,
                                                           parent1_content=program.id)

            _progress = Progress.pull(order_item, _content_location, program_item.content_type, program_item.content)
            d['dependency'] = program_item.check_dependency(order_item)
            if _progress is not None:
                d['progress'] = _progress.api_display()
        else:
            d['dependency'] = None
        try:
            d['name'] = program_item.content_cached.name
            result['content_list'].append(d)
        except:
            massage_error = 'error'

    install_version = request.GET.get('version', 0)
    update_checkout_type(result, install_version, request.APP.app_version)
    content_permission, is_require_permission = AccessContent.get_permission(program_id,
                                                                             _content_type.id,
                                                                             account)
    result.update({
        'content_permission': content_permission.api_display() if content_permission is not None else None,
        'is_require_permission': is_require_permission,
    })

    Stat.push('program_%s' % program.id, 'API_V5', request.META.get('HTTP_USER_AGENT', ''))        
    return json_render(result, code)
