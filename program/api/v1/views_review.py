from django.conf import settings
from django.db.models import Count
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ...models import Program
from rating.models import Rating, Log
from .views import check_key, check_require
from .views import json_render

CONTENT_TYPE = {
    'course': 'Course',
    'activity': 'Test',
    'task': 'To-Do',
    'milestone': 'Milestone',
}

CONTENT_TYPE_ID = {
    'course': 0,
    'activity': 1,
    'task': 2,
    'milestone': 3,
}


def review_view(request, program_id, order_item_id=None):
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render({}, code)

    program = Program.pull(program_id)
    if program is None:
        return json_render({}, 4000)

    if program.type == 2:
        _content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        _content_type = settings.CONTENT_TYPE('program', 'program')

    page_number = request.GET.get('page')
    if page_number is None:
        page_number = 1

    rating = Rating.objects.filter(content_type=_content_type.id,
                                   content=program_id).first()

    weight_list = Log.objects.filter(rating=rating).order_by().values('weight').annotate(
        count=Count('weight')).distinct()
    if rating is None:
        return json_render({}, 4000)
    wieght = [1, 2, 3, 4, 5]
    result = rating.api_display()
    total_content = result['count']
    temp_review = []
    review_list = rating.get_log_list()
    paginator = Paginator(review_list, 20)
    result['total'] = paginator.num_pages
    try:
        review = paginator.page(page_number)
        result['next'] = review.next_page_number()
        result['previous'] = review.previous_page_number()
        result['count'] = 20


    except PageNotAnInteger:
        review = paginator.page(1)
        result['next'] = 1
        result['previous'] = 1


    except EmptyPage:
        result['next'] = 1
        result['previous'] = 1
        review = paginator.page(paginator.num_pages)

    for review_item in review.object_list:
        temp_review.append(review_item.api_display())

    for weight_item in weight_list:
        weight_item['percen'] = (weight_item['count'] / total_content) * 100
        wieght.pop(wieght.index(weight_item['weight']))
    result['weight_list'] = list(weight_list)
    for wieght_item in wieght:
        result['weight_list'].append({'weight': wieght_item, 'count': 0, 'percen': 0})

    result['review_list'] = temp_review
    return json_render(result, code)
