from account.cached import cached_api_account_auth
from order.models import Item
from program.models import Program
from .views import check_key, check_require, json_render
from .views_detail import detail_view


def onboarding_view(request):
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render({}, code)
    account = cached_api_account_auth(token, uuid)
    if account is None:
        return json_render({}, 400)

    order_item = Item.pull_onboard_item(account)
    if order_item is None:
        return json_render({}, 4000)

    program = Program.pull(order_item.content)
    if program is None:
        return json_render({}, 4000)

    return detail_view(request, program.id, order_item_id=order_item.id)
