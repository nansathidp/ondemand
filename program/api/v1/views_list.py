from django.conf import settings

from .views import check_key, check_require
from .views import json_render

from ...models import Program
from ...cached import cached_program
from category.models import Category
from api.v5.views import get_filter_list
from api.v5.views_decorator import param_filter
from ondemand.views_base import content_sort


@param_filter
def list_view(request):
    result = {}
    response = check_key(request)
    if response is not None:
        return response

    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render(result, code)

    get_filter_list(result,
                    category_list=Category.pull_list(),
                    is_display_provider=request.is_display_provider == 't',
                    is_display_category=request.is_display_category == 't',
                    is_display_popular=True)

    program_id_list = Program.objects.values_list('id', flat=True).filter(type=1, is_display=True)

    if request.category_id != 'all':
        try:
            program_id_list = program_id_list.filter(category_id=request.category_id)
        except:
            try:
                if request.provider_id != 'all':
                    program_id_list = program_id_list.filter(provider_id=request.provider_id)
            except:
                program_id_list = program_id_list.all()
    if request.provider_id != 'all':
        try:
            program_id_list = program_id_list.filter(provider_id=request.provider_id)
        except:
            try:
                program_id_list = program_id_list.filter(category_id=request.category_id)
            except:
                program_id_list = program_id_list.all()
    # Filter Provider
    # if request.provider_id != 'all' and request.provider_id is not None:
    #     program_id_list = program_id_list.filter(provider_id=request.provider_id)
    #
    # if request.category_id != 'all':
    #     program_id_list = program_id_list.filter(category_id=request.category_id)

    # Sort
    program_id_list = content_sort(request, program_id_list, settings.CONTENT_TYPE('program', 'program'))

    result['program_list'] = [cached_program(program_id).api_display_title() for program_id in program_id_list]
    return json_render(result, code)
