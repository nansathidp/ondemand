from django.conf.urls import url

from .views_buy import buy_view
from .views_detail import detail_view
from .views_list import list_view
from .views_onboard import onboarding_view
from .views_review import review_view

urlpatterns = [
    url(r'^$', list_view),
    url(r'^(\d+)/$', detail_view),
    url(r'^(\d+)/review/$', review_view),
    url(r'^(\d+)/(\d+)/$', detail_view),
    url(r'^(\d+)/buy/$', buy_view),
    url(r'onboarding/$', onboarding_view),
]
