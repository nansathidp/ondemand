from django.conf import settings
from django.core.cache import cache

from .models import Program, Item, Milestone

from utils.cached.time_out import get_time_out_day, get_time_out


def pack_program(program_id_list):
    program_list = []
    for program_id in program_id_list:
        program = cached_program(program_id)
        if program is not None:
            program_list.append(program)
    return program_list


def cached_program_delete(program_id, is_force=False):
    key = '%s_program_%s' % (settings.CACHED_PREFIX, program_id)
    cache.delete(key)
def cached_program(program_id, is_force=False):
    key = '%s_program_%s' % (settings.CACHED_PREFIX, program_id)
    result = None if is_force else cache.get(key)
    content_type_program = settings.CONTENT_TYPE('program', 'program')
    if result is None:
        try:
            result = Program.objects.get(id=program_id)
            cache.set(key, result, get_time_out_day())
            return result
        except:
            return None
    return None if result == -1 else result


def cached_onboard_id_list(is_force=False):
    key = '%s_onboard_id_list' % settings.CACHED_PREFIX
    result = None if is_force else cache.get(key)
    if result is None:
        result = Program.objects.values_list('id', flat=True).filter(type=2)
        cache.set(key, result, get_time_out())
    return result


def cached_program_update(program):
    key = '%s_program_%s' % (settings.CACHED_PREFIX, program.id)
    cache.set(key, program, get_time_out_day())


def cached_program_item(item_id, is_force=False):
    key = '%s_program_item_%s'%(settings.CACHED_PREFIX, item_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Item.objects.get(id=item_id)
        except:
            result = -1
        cache.set(key, result, get_time_out_day())
    return None if result == -1 else result


def cached_program_item_content(program_id, content_type_id, content, is_force=False):
    key = '%s_program_item_content_%s_%s_%s'%(settings.CACHED_PREFIX, program_id, content_type_id, content)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Item.objects.get(program_id=program_id,
                                      content_type_id=content_type_id,
                                      content=content)
        except:
            result = -1
        cache.set(key, result, get_time_out_day())
    return None if result == -1 else result


def cached_program_milestone(milestone_id, is_force=False):
    key = '%s_program_milestone_%s'%(settings.CACHED_PREFIX, milestone_id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Milestone.objects.filter(id=milestone_id).first()
        if result is None:
            result = -1
        cache.set(key, result, get_time_out_day())
    return None if result == -1 else result


#Use in Progress.Course update
def cached_program_item_by_course(program_id, course_id, is_force=False):
    key = '%s_program_item_by_course_%s_%s'%(settings.CACHED_PREFIX, program_id, course_id)
    result = None if is_force else cache.get(key)
    if result is None:
        content_type_course = settings.CONTENT_TYPE('course', 'course')
        result = Item.objects.filter(program_id=program_id,
                                     content_type=content_type_course,
                                     content=course_id).first()
        if result is None:
            result = -1
        cache.set(key, result, get_time_out_day())
    return None if result == -1 else result


def cached_api_program_list(store, is_force=False):
    key = '%s_api_program_list_store_%s' % (settings.CACHED_PREFIX, store)
    result = None if is_force else cache.get(key)
    if result is None:
        result = []
        for program_id in Program.objects.values_list('id', flat=True).filter(type=1):
            program = cached_program(program_id)
            if program is not None:
                result.append(program.api_display_title(store=store))
        cache.set(key, result, get_time_out())
    return result


#User in Order.Item purchased (cached)
def cached_program_has_course(course_id, is_force=False):
    key = '%s_program_has_course_%s'%(settings.CACHED_PREFIX, course_id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Program.objects.values_list('id', flat=True).filter(item__content_type=settings.CONTENT_TYPE('course', 'course'),
                                                                     item__content=course_id)
        cache.set(key, result, get_time_out_day())
    return result


def cached_program_has_question(activity_id, is_force=False):
    key = '%s_program_has_question_%s'%(settings.CACHED_PREFIX, activity_id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Program.objects.values_list('id', flat=True).filter(item__content_type=settings.CONTENT_TYPE('question', 'activity'),
                                                                     item__content=activity_id)
        cache.set(key, result, get_time_out_day())
    return result


def cached_program_has_live(live_id, is_force=False):
    key = '%s_program_has_live_%s' % (settings.CACHED_PREFIX, live_id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Program.objects.values_list('id', flat=True).filter(item__content_type=settings.CONTENT_TYPE('live', 'live'),
                                                                     item__content=live_id)
        cache.set(key, result, get_time_out_day())
    return result



