from django import template
from django.utils.safestring import mark_safe
from django.conf import settings

register = template.Library()

@register.simple_tag
def item_sort_tag(count, item_list):
    result = ''
    item = item_list[count]
    content_type_milestone = settings.CONTENT_TYPE('program', 'milestone')
    if item.content_type_id == content_type_milestone.id:
        return ''

    if count > 0:
        p = item_list[count-1]
        if int(p.sort) == int(item.sort):
            result += '<a href="#" class="item-sort btn bgm-teal btn-sm" item="%s" action="up"><i class="zmdi zmdi-triangle-up zmdi-hc-fw"></i></a>'%(item.id)

    if count < len(item_list)-1:
        n = None
        for _ in item_list[count+1:]:
            if _.content_type_id != content_type_milestone.id:
                n = item_list[count+1]
        if n is not None and int(item.sort) == int(n.sort):
            result += ' <a href="#" class="item-sort btn bgm-teal btn-sm" item="%s" action="down"><i class="zmdi zmdi-triangle-down zmdi-hc-fw"></i></a>'%(item.id)

    return mark_safe(result)
