from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

from program.models import Program, Item as ProgramItem

register = template.Library()

@register.simple_tag
def course_check_tag(program, course):
    content_type = settings.CONTENT_TYPE('course', 'course')
    if ProgramItem.objects.filter(program_id=program.id, content_type=content_type, content=course.id).exists():
        result = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
    else:
        result = '<i class="zmdi zmdi-plus-circle-o zmdi-hc-fw insert-course" course="%s" style="cursor: pointer"></i>'%(course.id)
    return mark_safe(result)

@register.simple_tag
def activity_check_tag(program, activity):
    content_type = settings.CONTENT_TYPE('question', 'activity')
    if ProgramItem.objects.filter(program_id=program.id, content_type=content_type, content=activity.id).exists():
        result = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
    else:
        result = '<i class="zmdi zmdi-plus-circle-o zmdi-hc-fw insert-question" question="%s" style="cursor: pointer"></i>'%(activity.id)
    return mark_safe(result)

@register.simple_tag
def task_check_tag(program, task):
    content_type = settings.CONTENT_TYPE('task', 'task')
    if ProgramItem.objects.filter(program_id=program.id, content_type=content_type, content=task.id).exists():
        result = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
    else:
        result = '<i class="zmdi zmdi-plus-circle-o zmdi-hc-fw insert-task" task="%s" style="cursor: pointer"></i>'%(task.id)
    return mark_safe(result)
