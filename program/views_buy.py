from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

from content.models import Location as ContentLocation
from dependency.models import Dependency
from order.models import Order
from .models import Program


@login_required
def buy_view(request, program_id):
    program = Program.pull(program_id)
    if program is None:
        return redirect('home')

    is_dependency, dependency_list = Dependency.check_content2(settings.CONTENT_TYPE('program', 'program'),
                                                               program.id,
                                                               request.user)
    if is_dependency:
        return redirect('program:detail', program.id)

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('program', 'program'),
                                                  program.id)
    order_item = Order.buy(content_location,
                           request.APP,
                           1,
                           settings.CONTENT_TYPE('program', 'program'),
                           program,
                           request.user)

    if order_item is not None:
        order = order_item.order

    if order.status == 3 and order_item.is_free:
        order_item.order.buy_success(request.APP, request.get_host().split(':')[0])
        return redirect('program:detail', program_id)
    else:
        return redirect('order:checkout', order.id)
