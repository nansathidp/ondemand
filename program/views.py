from django.conf import settings
from django.shortcuts import render

from ondemand.views_base import content_sort
from ondemand.views_decorator import check_project, param_filter
from .cached import pack_program

from program.models import Program
from category.models import Category
from provider.models import Provider
from rating.models import Rating

@check_project
@param_filter
def home_view(request):
    program_id_list = Program.objects.values_list('id', flat=True) \
                                     .filter(type=1, is_display=True)

    if request.category is not None:
        program_id_list = program_id_list.filter(category=request.category)

    if request.provider is not None:
        program_id_list = program_id_list.filter(provider=request.provider)

    content_type = settings.CONTENT_TYPE('program', 'program')
    program_id_list = content_sort(request, program_id_list, content_type)
    program_list = pack_program(program_id_list)

    for _ in program_list:
        _.rating = Rating.pull_first(content_type,
                                     _.id)
    return render(request,
                  'program/home.html',
                  {'ROOT_PAGE': 'program',
                   'DISPLAY_SUBMENU': True,
                   'program_list': program_list,
                   'category': request.category,
                   'provider': request.provider,
                   'content_type_str': 'program',
                   'sort': request.sort,
                   'provider_list': Provider.pull_list(),
                   'category_list': Category.pull_list()})
