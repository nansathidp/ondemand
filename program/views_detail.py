from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.conf import settings
from django.http import Http404

from program.models import Program
from access.models import Access
from dependency.models import Dependency
from rating.models import Rating
from order.models import Item as OrderItem
from progress.models import Progress
from content.models import Location as ContentLocation

from ondemand.views_decorator import check_project
from utils.content import get_content


@check_project
def detail_view(request, program_id, order_item_id=None):
    program = Program.pull(program_id)
    if program is None:
        return redirect('home')
    
    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(content_type,
                                                  program.id)

    if request.user.is_authenticated():
        if order_item_id is not None:
            order_item = OrderItem.pull(order_item_id)
            if order_item is None or request.user.id != order_item.account_id:
                return redirect('home')
        else:
            order_item = OrderItem.pull_purchased(content_location,
                                                  request.user,
                                                  content_type,
                                                  program.id)
            if order_item is not None:
                return redirect('program:detail', program.id, order_item.id)
    else:
        order_item = None

    if not program.is_display:
        preview = request.GET.get('preview', None)
        if preview and preview.lower() == 'true' and request.user.is_authenticated and program.check_access(request):
            pass
        elif order_item:
            pass
        else:
            raise Http404

    is_dependency, dependency_list = Dependency.check_content2(content_type,
                                                               program_id,
                                                               request.user)
    for parent in dependency_list:
        parent.get_content_cached()

    content_permission, is_require_permission = Access.get_permission(program_id,
                                                                      content_type,
                                                                      request.user)

    if order_item is not None:
        progress = Progress.pull(order_item,
                                 content_location,
                                 content_type,
                                 order_item.content)
    else:
        progress = None

    item_progress = {
        'course': 0,
        'course_all': 0,
        'question': 0,
        'question_all': 0,
        'task': 0,
        'task_all': 0,
        'milestone': 0,
        'milestone_all': 0,
    }

    item_list = program.item_set.all()
    is_milestone_complete = True
    for item in item_list:

        item.content_object = get_content(item.content_type_id, item.content)
        if item.content_object is None:
            continue
        if item.content_type_id == settings.CONTENT_TYPE('course', 'course').id:
            if order_item is None:
                item.url = reverse('course:detail_program', args=[program.id, item.content_object.id])
            else:
                item.url = reverse('course:detail_program', args=[program.id, item.content_object.id, order_item.id])
            item.icon_class = 'fa-play-circle'
            item.type = 'course'
        elif item.content_type_id == settings.CONTENT_TYPE('question', 'activity').id:
            if order_item is None:
                item.url = reverse('question:detail_program', args=[item.content_object.id, program.id])
            else:
                item.url = reverse('question:detail_program', args=[item.content_object.id, program.id, order_item.id])
            item.icon_class = 'fa-file-text'
            item.type = 'question'
        elif item.content_type_id == settings.CONTENT_TYPE('task', 'task').id:
            if order_item is None:
                item.url = reverse('task:detail', args=[item.content_object.id])
            else:
                item.url = reverse('task:detail_item', args=[item.content_object.id, order_item.id])
            item.icon_class = 'fa-check-square-o'
            item.type = 'task'
        elif item.content_type_id == settings.CONTENT_TYPE('program', 'milestone').id:
            item.url = None
            item.type = 'milestone'
            item.is_milestone_complete = is_milestone_complete
        else:
            item.url = None
            item.type = ''

        if item.type + '_all' in item_progress:
            item_progress[item.type + '_all'] += 1
        else:
            item_progress[item.type + '_all'] = 1

        if order_item is not None:
            item.content_location = ContentLocation.pull_first(settings.CONTENT_TYPE_ID(item.content_type_id),
                                                               item.content,
                                                               parent1_content_type=content_type,
                                                               parent1_content=program.id)

            item.progress = Progress.pull(order_item,
                                          item.content_location,
                                          settings.CONTENT_TYPE_ID(item.content_type_id),
                                          item.content)

            if item.progress and item.progress.status == 2:
                if item.type in item_progress:
                    item_progress[item.type] = 1
                else:
                    item_progress[item.type] += 1
                    
            # if item.progress is not None and item.progress.status == 2:
            #     if item.type in item_progress:
            #         item_progress[item.type] = 1
            #     else:
            #         item_progress[item.type] += 1

            #     # Check Milestone complete
            #     if is_milestone_complete:
            #         is_milestone_complete = item_progress in [2, 3]
            result = item.check_dependency(order_item)
            item.is_dependency = result['is_dependency']
        else:
            item.is_dependency = item.is_dependency()

    # extra = program.get_extra() # not update?

    def _display(code):
        # _ = extra.get('count_%s' % code, 0)
        # if _ > 0:
        _ = item_progress[code+'_all'] 
        if _:
            if order_item is None:
                item_progress['%s_display' % code] = _
            else:
                item_progress['%s_display' % code] = '%s/%s' % (item_progress[code], _)
        else:
            item_progress['%s_display' % code] = '-'
    _display('course')
    _display('question')
    _display('task')

    rating = Rating.pull_first(content_type, program.id)
    if program.type == 2:
        ROOT_PAGE = 'onboard'
    else:
        ROOT_PAGE = 'program'
    return render(request,
                  'program/detail.html',
                  {
                      'DISPLAY_SUBMENU': True,
                      'ROOT_PAGE': ROOT_PAGE,
                      'program': program,
                      'item_list': item_list,
                      'order_item': order_item,
                      'progress': progress,
                      'item_progress': item_progress,
                      'is_dependency': is_dependency,
                      'dependency_list': dependency_list,
                      'content_permission': content_permission,
                      'is_require_permission': is_require_permission,
                      'rating': rating,
                      'rating_log': rating.pull_log(request.user),
                      'rating_log_list': rating.pull_log_list()
                  })
