from django.contrib import admin

from program.models import Program, Item, Milestone
from account.models import Account
from utils.cached.content_type import cached_content_type_admin_item_inline


class ItemInline(admin.TabularInline):
    model = Item

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'content_type':
            kwargs["queryset"] = cached_content_type_admin_item_inline()
        return super(ItemInline, self).formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Program)
class ProgramAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_display')
    inlines = [
        ItemInline,
    ]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "account":
            kwargs["queryset"] = Account.objects.filter(id=request.user.id)
        return super(ProgramAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('program', 'content_type', 'content')


@admin.register(Milestone)
class MilestoneAdmin(admin.ModelAdmin):
    list_display = ('program', 'item', 'name', 'desc', 'timestamp')
