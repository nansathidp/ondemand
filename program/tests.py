from django.conf import settings
from django.test import TestCase

from program.models import Program
from price.models import Price
from .cached import cached_program
from category.tests import create_category

import random
import string


def _random():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(8))


PROJECT_LIST = ['aisondemand', 'com.pacrim.ondemand', 'demo.ondemand']


def create_program():
    return Program.objects.create(name=_random(),
                                  category=create_category(),
                                  is_display=True)


def create_paid_program():
    program = create_program()
    content_type_program = settings.CONTENT_TYPE('program', 'program')
    Price.objects.create(content_type=content_type_program,
                         content=program.id,
                         price=100,
                         store=1)
    cached_program(program.id, True)
    return program


class InitTests(TestCase):
    def test_create_program(self):
        program = create_program()
        self.assertIsNotNone(program)

    def test_create_paid_program(self):
        program = create_paid_program()
        content_type_program = settings.CONTENT_TYPE('program', 'program')
        price = Price.pull(content_type_program, program.id, 1)
        self.assertIsNotNone(program)
        self.assertIsNotNone(price)
        self.assertEqual(price.price, 100)
