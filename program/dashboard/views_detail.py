from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404

from utils.crop import crop_image
from .forms import ProgramForm
from ..cached import cached_program_update
from ..models import Program


def detail_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access(request):
        raise PermissionDenied

    is_change = program.check_change(request)

    if request.method == 'POST':
        if not is_change:
            raise PermissionDenied

        if request.user.has_perm('program.view_program'):
            program_form = ProgramForm(None, request.POST, request.FILES, instance=program)
        elif request.user.has_perm('program.view_own_program'):
            program_form = ProgramForm(request, request.POST, request.FILES, instance=program)
        else:
            raise PermissionDenied

        if program_form.is_valid():
            program = program_form.save()
            crop_image(program, request)
            program.push_sub_provider(request)
            cached_program_update(program)
            return redirect('dashboard:program-dashboard:detail', program.id)

    if request.user.has_perm('program.view_program'):
        program_form = ProgramForm(None, instance=program)
    elif request.user.has_perm('program.view_own_program'):
        program_form = ProgramForm(request, instance=program)
    else:
        raise PermissionDenied

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Program Management',
         'url': reverse('dashboard:program-dashboard:home')},
        {'is_active': True,
         'title': 'Program Information'}
    ]
    return render(request,
                  'program/dashboard/detail.html',
                  {'SIDEBAR': 'program',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'information',
                   'program': program,
                   'program_form': program_form,
                   'is_change': is_change})
