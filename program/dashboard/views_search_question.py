from django.conf import settings
from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Program
from question.models import Activity
from category.models import Category
from provider.models import Provider


def search_question_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access(request):
        raise PermissionDenied
    
    q_name = ''
    q_category = -1
    q_provider = -1
    
    activity_list = Activity.access_list(request)
    if request.method == 'POST':
        q_name = request.POST.get('q_name', '')
        activity_list = activity_list.filter(name__icontains=q_name)
        try:
            q_category = int(request.POST.get('category', -1))
            if q_category != -1:
                activity_list = activity_list.filter(category_id=q_category)
        except:
            pass
        try:
            q_provider = int(request.POST.get('provider', -1))
            if q_provider != -1:
                activity_list = activity_list.filter(provider_id=q_provider)
        except:
            pass
        activity_list = activity_list[:settings.CONTENT_LIMIT]
    else:
        activity_list = activity_list[:settings.CONTENT_LIMIT]
    return render(request,
                  'program/dashboard/search_question.html',
                  {'activity_list': activity_list,
                   'program': program,
                   'category_list': Category.objects.all(),
                   'provider_list': Provider.access(request),
                   'q_name': q_name,
                   'q_category': q_category,
                   'q_provider': q_provider})
