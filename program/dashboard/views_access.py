from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.core.exceptions import PermissionDenied

from ..models import Program
from access.models import Access, Level

from access.cached import cached_access_level, cached_access_permission


def access_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access(request):
        raise PermissionDenied

    level_list = cached_access_level()
    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_permission = cached_access_permission(program_id, content_type_program.id)

    if request.method == 'POST':
        level_id = request.POST.get('level_id', None)
        if level_id is None or int(level_id) == -1:
            Access.objects.filter(content_type_id=content_type_program.id, content=program_id).delete()
            content_permission = cached_access_permission(program_id, content_type_program.id, is_force=True)
        else:
            level = Level.objects.filter(id=level_id).first()
            try:
                content_permission = Access.objects.get(content_type=content_type_program, content=program_id)
                content_permission.level = level
                content_permission.save(update_fields=['level'])
            except Access.DoesNotExist:
                content_permission = Access(content_type=content_type_program,
                                            content=program_id,
                                            level=level)
                content_permission.save()

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Program Management'},
    ]
    return render(request,
                  'program/dashboard/access.html',
                  {'SIDEBAR': 'program',
                   'program': program,
                   'TAB': 'access',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'level_list': level_list,
                   'content_permission': content_permission})
