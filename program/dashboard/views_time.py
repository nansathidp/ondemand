import datetime

from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404

from ..cached import cached_program_update
from ..models import Program


def time_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access(request):
        raise PermissionDenied

    if request.method == 'POST':
        try:
            # if request.POST['credit'] == 'unlimited':
            #     program.credit = datetime.timedelta(0)
            # else:
            #     credit_day = int(request.POST.get('credit-day'))
            #     credit_hour = int(request.POST.get('credit-hour'))
            #     credit_minute = int(request.POST.get('credit-minute'))
            #     credit_second = 0
            #     program.credit = datetime.timedelta(days=credit_day, hours=credit_hour, minutes=credit_minute, seconds=credit_second)
            if request.POST['expired'] == 'unlimited':
                program.expired = datetime.timedelta(0)
            else:
                expired_day = int(request.POST.get('expired-day'))
                expired_hour = int(request.POST.get('expired-hour'))
                expired_minute = int(request.POST.get('expired-minute'))
                expired_second = 0
                program.expired = datetime.timedelta(days=expired_day, hours=expired_hour, minutes=expired_minute, seconds=expired_second)
            program.save(update_fields=['credit', 'expired'])
            cached_program_update(program)
        except:
            pass

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Program Management',
         'url': reverse('dashboard:program-dashboard:home')},
        {'is_active': False,
         'title': 'Program Information',
         'url': reverse('dashboard:program-dashboard:detail', args=[program.id])},
        {'is_active': True,
         'title': 'Schedule / Learning Credit'}
    ]
    return render(request,
                  'program/dashboard/time.html',
                  {'SIDEBAR': 'program',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'time',
                   'program': program})
