from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from ..models import Program
from dependency.models import Dependency

@csrf_exempt
def dependency_delete_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_change(request):
        raise PermissionDenied

    content_type = settings.CONTENT_TYPE('program', 'program')
    dependency = Dependency.pull(content_type,
                                 program.id)
    if request.method == 'POST':
        try:
            parent_id = int(request.POST.get('parent'))
            dependency.delete_parent(parent_id)
        except:
            pass

    parent_list = Dependency.pull_parent(content_type, program.id)
    return render(request,
                  'program/dashboard/dependency_block.html',
                  {'program': program,
                   'parent_list': parent_list})
