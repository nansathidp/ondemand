from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.conf import settings

from ..models import Program
from order.models import Item as OrderItem
from progress.models import Progress

from utils.paginator import paginator

def progress_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access(request):
        raise PermissionDenied

    content_type = settings.CONTENT_TYPE('program', 'program')
    progress_content = None
    
    progress_list = Progress.objects.filter(content_type=content_type,
                                            content=program.id,
                                            is_root=True)
    progress_list = paginator(request, progress_list)
    for progress in progress_list:
        progress.item_cached = OrderItem.pull(progress.item_id)
        #progress.get_content_cached()
        progress.get_account_cached()
    
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Program Management',
         'url': reverse('dashboard:program-dashboard:home')},
        {'is_active': False,
         'title': 'Information: %s'%program.name,
         'url': reverse('dashboard:program-dashboard:detail', args=[program.id])},
        {'is_active': True,
         'title': 'Progress'}
    ]
    return render(request,
                  'program/dashboard/progress.html',
                  {'SIDEBAR': 'program',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'program': program,
                   'progress_content': progress_content,
                   'progress_list': progress_list})
