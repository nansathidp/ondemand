from django import forms
from django.conf import settings
from ..models import Program
from provider.models import Provider


class ProgramForm(forms.ModelForm):
    class Meta:
        model = Program
        fields = ['name', 'category', 'provider', 'desc', 'condition', 'image', 'is_display']
        labels = {
            'name': 'Program Name',
            'provider': 'Learning Center',
            'desc': 'Description',
            'image': 'Cover Image',
            'is_display': 'Display',
        }
        help_texts = {
            'name': 'Limit at 120 characters',
            'image': settings.HELP_TEXT_IMAGE('program'),
            'desc': 'Limit at 250 characters'
        }

    def __init__(self, request=None, *args, **kwargs):
        super(ProgramForm, self).__init__(*args, **kwargs)
        if request is not None:
            self.fields['provider'].queryset = Provider.access(request)
