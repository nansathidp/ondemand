from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.conf import settings

from dependency.models import Dependency
from ..models import Program


def dependency_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access(request):
        raise PermissionDenied

    parent_list = Dependency.pull_parent(settings.CONTENT_TYPE('program', 'program'),
                                         program.id)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Program Management',
         'url': reverse('dashboard:program-dashboard:home')},
        {'is_active': True,
         'title': 'Dependency: %s'%program.name},
    ]
    return render(request,
                  'program/dashboard/dependency.html',
                  {'SIDEBAR': 'program',
                   'TAB': 'dependency',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'program': program,
                   'parent_list': parent_list})
