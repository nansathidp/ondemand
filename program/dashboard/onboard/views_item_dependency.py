from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from dependency.models import Dependency, Parent as DependencyParent
from ...models import Program, Item


@csrf_exempt
def item_dependency_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    item_list = program.item_set.exclude(content_type=settings.CONTENT_TYPE('program', 'milestone')).order_by('sort')
    if request.method == 'POST':
        try:
            item_id = int(request.POST.get('item', -1))
            item = Item.objects.get(program=program, id=item_id)
            item_list = item_list.exclude(id=item.id)
            dependency = Dependency.pull(settings.CONTENT_TYPE('program', 'item'),
                                         item.id)
            is_first = not dependency.parent_set.exists()
        except:
            item = None
            is_first = True
    else:
        item = None
        is_first = True

    for item in item_list:
        item.get_content_cached()

    return render(request,
                  'program/dashboard/onboard/item_dependency.html',
                  {'item_list': item_list,
                   'condition_choices': DependencyParent.CONDITION_CHOICES,
                   'item': item,
                   'is_first': is_first})
