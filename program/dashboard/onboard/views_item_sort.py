from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.db.models import F

from ...models import Program, Item
from progress.models import Progress

@csrf_exempt
def item_sort_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    if request.method == 'POST':
        try:
            item = Item.objects.filter(program=program,
                                       id=int(request.POST.get('item', -1))).first()
            sort_old = item.sort_user
            action = request.POST.get('action')

            content_type_milestone = settings.CONTENT_TYPE('program', 'milestone')
            
            if action == 'up':
                item_old = Item.objects.filter(program=program,
                                               sort_user__lt=sort_old).exclude(id=item.id,
                                                                               content_type=content_type_milestone).order_by('-sort_user').first()
                sort = -1
            elif action == 'down':
                item_old = Item.objects.filter(program=program,
                                               sort_user__gt=sort_old).exclude(id=item.id,
                                                                               content_type=content_type_milestone).first()
                sort = 1
            else:
                sort = None

            if all(_ is not None for _ in [sort, item, item_old]):
                item.sort_user += sort
                item.save(update_fields=['sort_user'])
                item_old.sort_user = sort_old
                item_old.save(update_fields=['sort_user'])
            program.sort_item()
        except:
            #raise
            pass

    item_list = program.item_set.all()
    for item in item_list:
        item.get_content_cached()

    return render(request,
                  'program/dashboard/onboard/item_block.html',
                  {'program': program,
                   'item_list': item_list})
