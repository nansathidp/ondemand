from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404

from category.models import Category
from course.models import Course
from provider.models import Provider
from ...models import Program


def search_course_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    q_name = ''
    q_category = -1
    q_provider = -1
    if request.method == 'POST':
        q_name = request.POST.get('q_name', '')
        course_list = Course.objects.filter(name__icontains=q_name)

        try:
            q_category = int(request.POST.get('category', -1))
            if q_category != -1:
                course_list = course_list.filter(category_id=q_category)
        except:
            pass

        try:
            q_provider = int(request.POST.get('provider', -1))
            if q_provider != -1:
                course_list = course_list.filter(provider_id=q_provider)
        except:
            pass
        course_list = course_list[:settings.CONTENT_LIMIT]
    else:
        course_list = Course.objects.all()[:settings.CONTENT_LIMIT]
    return render(request,
                  'program/dashboard/onboard/search_course.html',
                  {'course_list': course_list,
                   'program': program,
                   'category_list': Category.objects.all(),
                   'provider_list': Provider.objects.all(),
                   'q_name': q_name,
                   'q_category': q_category,
                   'q_provider': q_provider})
