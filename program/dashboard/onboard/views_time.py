from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ...models import Program

from ...cached import cached_program_update
import datetime


def time_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    if request.method == 'POST':
        try:
            if 'credit-unlimited' in request.POST:
                program.credit = datetime.timedelta(0)
            else:
                credit_day = int(request.POST.get('credit-day'))
                credit_hour = int(request.POST.get('credit-hour'))
                credit_minute = int(request.POST.get('credit-minute'))
                credit_second = int(request.POST.get('credit-second'))
                program.credit = datetime.timedelta(days=credit_day, hours=credit_hour, minutes=credit_minute, seconds=credit_second)
            if 'expired-unlimited' in request.POST:
                program.expired = datetime.timedelta(0)
            else:
                expired_day = int(request.POST.get('expired-day'))
                expired_hour = int(request.POST.get('expired-hour'))
                expired_minute = int(request.POST.get('expired-minute'))
                expired_second = int(request.POST.get('expired-second'))
                program.expired = datetime.timedelta(days=expired_day, hours=expired_hour, minutes=expired_minute, seconds=expired_second)
            program.save(update_fields=['credit', 'expired'])
            cached_program_update(program)
        except:
            pass

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Onboard Management',
         'url': reverse('dashboard:onboard-dashboard:home')},
        {'is_active': False,
         'title': 'Onboard Information',
         'url': reverse('dashboard:onboard-dashboard:detail', args=[program.id])},
        {'is_active': True,
         'title': 'Schedule / Learning Credit'}
    ]
    return render(request,
                  'program/dashboard/onboard/time.html',
                  {'SIDEBAR': 'onboard',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'time',
                   'program': program})
