from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from ...models import Program, Item as ProgramItem
from task.models import Task

from task.cached import cached_task_count_program

@csrf_exempt
def add_task_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    if request.method == 'POST':
        try:
            task = get_object_or_404(Task, id=int(request.POST.get('task', -1)))
        except:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
            return JsonResponse({'html': html})

    content_type = settings.CONTENT_TYPE('task', 'task')
    if ProgramItem.objects.filter(program_id=program.id, content_type=content_type, content=task.id).exists():
        html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
    else:
        try:
            ProgramItem.objects.create(program=program,
                                       content_type=content_type,
                                       content=task.id,
                                       sort=0.0)
            program.content_count(content_type)
            program.progress_update()
            cached_task_count_program(task.id, is_force=True)
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
        except:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
    return JsonResponse({'html': html})
