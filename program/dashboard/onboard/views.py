from django.core.exceptions import PermissionDenied
from django.shortcuts import render
from django.conf import settings
from utils.paginator import paginator
# from analytic.cached import cached_analytic_stat
from ...models import Program


def home_view(request):
    program_list = Program.access_list_onboard(request)
    if program_list is None:
        raise PermissionDenied

    program_list = paginator(request, program_list)
    for program in program_list:
        program.count_learner = 0
        # program.analytic_view = cached_analytic_stat('program_%s'%program.id)['stat'].value
        # program.analytic_libary = cached_analytic_stat('program_%s_libary'%program.id)['stat'].value
        # program.analytic_assign = cached_analytic_stat('program_%s_assign'%program.id)['stat'].value

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Onboard Management'}
    ]
    return render(request,
                  'program/dashboard/onboard/home.html',
                  {'SIDEBAR': 'onboard',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'program_list': program_list,
                   'content_type': settings.CONTENT_TYPE('program', 'onboard'),
                   'q_name': request.q_name,
                   'q_provider': request.q_provider,
                   'q_provider_list': request.q_provider_list,
                   'q_category': request.q_category,
                   'q_category_list': request.q_category_list})
