from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from program.models import Program
from progress.models import Progress


@csrf_exempt
def alert_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    is_progress_update = Progress.objects.filter(is_update=False,
                                                 content_type=settings.CONTENT_TYPE('program', 'program'),
                                                 content=program.id).exists()
    return render(request,
                  'program/dashboard/onboard/alert.html',
                  {'program': program,
                   'is_progress_update': is_progress_update})
