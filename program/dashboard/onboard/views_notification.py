from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ...models import Program
from notification.models import Android, Ios
from app.models import App
from inbox.models import Inbox

from utils.paginator import paginator
from analytic.cached import cached_analytic_stat


def notification_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    msg = None
    if request.method == 'POST':
        if 'android' in request.POST:
            android = Android.objects.filter(account__email=request.POST.get('email')).first()
            if android is None:
                msg = 'Android Device Not Found.'
            else:
                m = request.POST.get('message', '-')
                Inbox.push(5, program.id, request.APP, android.account, 4, m, m)
        elif 'ios' in request.POST:
            iphone = Ios.objects.filter(account__email=request.POST.get('email')).first()
            if iphone is None:
                msg = 'IOS Device Not Found.'
            else:
                m = request.POST.get('message', '-')
                Inbox.push(5, program.id, request.APP, iphone.account, 4, m, m)
            
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Onboard Management',
         'url': reverse('dashboard:onboard-dashboard:home')},
        {'is_active': False,
         'title': 'Onboard Information',
         'url': reverse('dashboard:onboard-dashboard:detail', args=[program.id])},
        {'is_active': True,
         'title': 'Notification'}
    ]
    return render(request,
                  'program/dashboard/onboard/notification.html',
                  {'SIDEBAR': 'onboard',
                   'TAB': 'notification',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'program': program,
                   'msg': msg})
