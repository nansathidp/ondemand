from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ...models import Program


def search_milestone_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    item_list = program.item_set.all()
    for item in item_list:
        item.get_content_cached()
    return render(request,
                  'program/dashboard/onboard/search_milestones.html',
                  {'program': program,
                   'item_list': item_list})
