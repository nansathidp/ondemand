from django import forms
from django.conf import settings
from ...models import Program


class ProgramForm(forms.ModelForm):
    class Meta:
        model = Program
        fields = ['name', 'category', 'provider', 'desc', 'condition', 'image', 'is_display']
        labels = {
            'name': 'Onboarding Name',
            'desc': 'Description',
            'image': 'Cover Image',
            'is_display': 'Display',
        }
        help_texts = {
            'name': 'Limit at 120 characters',
            'image': settings.HELP_TEXT_IMAGE('program'),
            'desc': 'Limit at 250 characters'
        }
