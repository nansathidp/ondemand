from django.conf.urls import url

from .views import home_view
from .views_add_course import add_course_view
from .views_add_milestone import add_milestone_view
from .views_add_question import add_question_view
from .views_add_task import add_task_view
from .views_alert import alert_view
from .views_create import create_view
from .views_detail import detail_view
from .views_item import item_view
from .views_item_delete import item_delete_view
from .views_item_dependency import item_dependency_view
from .views_item_dependency_add import item_dependency_add_view
from .views_item_dependency_delete import item_dependency_delete_view
from .views_item_sort import item_sort_view
from .views_notification import notification_view
from .views_progress import progress_view
from .views_progress_clean import progress_clean_view
from .views_search_course import search_course_view
from .views_search_milestone import search_milestone_view
from .views_search_question import search_question_view
from .views_search_task import search_task_view
from .views_time import time_view
from .views_update import update_view
from .views_version import version_view
from .views_version_duplicate import version_duplicate_view

app_name = 'onboard-dashboard'
urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^create/$', create_view, name='create'),
    url(r'^(\d+)/$', detail_view, name='detail'),

    url(r'^(\d+)/item/$', item_view, name='item'),
    url(r'^(\d+)/item/delete/$', item_delete_view, name='item_delete'),
    url(r'^(\d+)/item/dependency/$', item_dependency_view, name='item_dependency'),
    url(r'^(\d+)/item/dependency/add/$', item_dependency_add_view, name='item_dependency_add'),
    url(r'^(\d+)/item/(\d+)/dependency/(\d+)/delete/$', item_dependency_delete_view, name='item_dependency_delete'),

    url(r'^(\d+)/item/sort/$', item_sort_view, name='item_sort'),

    url(r'^(\d+)/search/course/$', search_course_view, name='search_course'),
    url(r'^(\d+)/item/add/course/', add_course_view, name='add_course'),

    url(r'^(\d+)/search/question/$', search_question_view, name='search_question'),
    url(r'^(\d+)/item/add/question/', add_question_view, name='add_question'),

    url(r'^(\d+)/search/task/$', search_task_view, name='search_task'),
    url(r'^(\d+)/item/add/task/', add_task_view, name='add_task'),

    url(r'^(\d+)/search/milestone/$', search_milestone_view, name='search_milestone'),
    url(r'^(\d+)/item/add/milestone/', add_milestone_view, name='add_milestone'),

    url(r'^(\d+)/time/$', time_view, name='time'),

    url(r'^(\d+)/version/$', version_view, name='version'),
    url(r'^(\d+)/version/duplicate/$', version_duplicate_view, name='version_duplicate'),

    url(r'^(\d+)/notification/$', notification_view, name='notification'),

    url(r'^(\d+)/progress/$', progress_view, name='progress'),
    url(r'^(\d+)/progress/(\d+)/clean/$', progress_clean_view, name='progress_clean'),

    url(r'^(\d+)/alert/$', alert_view, name='alert'),
    url(r'^(\d+)/update/$', update_view, name='update'),
]
