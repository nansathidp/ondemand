from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.conf import settings

from program.models import Program
from progress.models import Progress


def update_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    for progress in Progress.objects.filter(is_update=False,
                                            content_type=settings.CONTENT_TYPE('program', 'program'),
                                            content=program.id):
        progress.content_update()

    ref = request.META.get('HTTP_REFERER', None)
    if ref is not None:
        return redirect(ref)
    return redirect('dashboard:onboard-dashboard:item', program.id)
