from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404

from ...models import Program


def version_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    duplicate = program.duplicate_set.first()
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Onboard Management',
         'url': reverse('dashboard:onboard-dashboard:home')},
        {'is_active': False,
         'title': 'Onboard Information',
         'url': reverse('dashboard:onboard-dashboard:detail', args=[program.id])},
        {'is_active': True,
         'title': 'Schedule / Learning Credit'}
    ]
    return render(request,
                  'program/dashboard/onboard/version.html',
                  {'SIDEBAR': 'onboard',
                   'TAB': 'version',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'program': program,
                   'duplicate': duplicate})
