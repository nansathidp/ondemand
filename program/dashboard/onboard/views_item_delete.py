from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt

from ...models import Program, Item
from progress.models import Progress


@csrf_exempt
def item_delete_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    if request.method == 'POST':
        try:
            item = Item.objects.filter(program=program,
                                       id=int(request.POST.get('item', -1))) \
                               .first()
            item.delete()
            program.content_count(item.content_type)
            program.progress_update()
        except:
            pass

    program.sort_item()
    item_list = program.item_set.all()
    for item in item_list:
        item.get_content_cached()

    return render(request,
                  'program/dashboard/onboard/item_block.html',
                  {'program': program,
                   'item_list': item_list,
                   'is_update': Progress.objects.filter(is_update=False).exists()})
