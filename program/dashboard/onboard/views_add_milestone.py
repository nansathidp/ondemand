from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from ...models import Program, Item, Milestone
from task.models import Task
from progress.models import Progress

@csrf_exempt
def add_milestone_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    if request.method == 'POST':
        try:
            item_id = int(request.POST.get('item', -1))
            item = get_object_or_404(Item, id=item_id)
            sort = item.sort + 0.1
        except:
            item = None
            sort = 0.0
            item_id = -1
        topic = request.POST.get('topic', '')
        desc = request.POST.get('desc', '')
        milestone = Milestone.objects.create(program=program,
                                             item=item,
                                             name=topic,
                                             desc=desc)
        if item is not None:
            Item.objects.create(program=program,
                                content_type=settings.CONTENT_TYPE('program', 'milestone'),
                                content=milestone.id,
                                sort=sort)
            program.sort_item()
    item_list = program.item_set.all()
    for item in item_list:
        item.get_content_cached()
    return render(request,
                  'program/dashboard/onboard/item_block.html',
                  {'program': program,
                   'item_list': item_list,
                   'is_update': Progress.objects.filter(is_update=False).exists()})
