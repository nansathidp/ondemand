from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect, get_object_or_404

from ...models import Program, Item, Duplicate


def version_duplicate_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    _program = Program.objects.create(name=program.name,
                                      version=program.version+0.1,
                                      provider=program.provider,
                                      category=program.category,
                                      desc=program.desc,
                                      overview=program.overview,
                                      condition=program.condition,
                                      image=program.image,
                                      type=program.type,

                                      start=program.start,
                                      end=program.end,
                                      credit=program.credit,
                                      expired=program.expired,

                                      is_display=program.is_display,
                                      sort=program.sort,

                                      account=request.user)
    for item in program.item_set.all():
        Item.objects.create(program=_program,
                            content_type=item.content_type,
                            content=item.content,
                            sort=item.sort)

    _program.content_count(content_type=None)
    Duplicate.objects.create(parent=program,
                             parent_version=program.version,
                             program=_program,
                             program_version=_program.version,
                             account=request.user)
    
    return redirect('dashboard:onboard-dashboard:detail', _program.id)
