from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from question.models import Activity
from ...models import Program, Item as ProgramItem


@csrf_exempt
def add_question_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    if request.method == 'POST':
        try:
            activity = get_object_or_404(Activity, id=int(request.POST.get('question', -1)))
        except:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
            return JsonResponse({'html': html})

    content_type = settings.CONTENT_TYPE('question', 'activity')
    if ProgramItem.objects.filter(program_id=program.id, content_type=content_type, content=activity.id).exists():
        html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
    else:
        try:
            ProgramItem.objects.create(program=program,
                                       content_type=content_type,
                                       content=activity.id,
                                       sort=0.0)
            program.content_count(content_type)
            program.progress_update()
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
        except:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
    return JsonResponse({'html': html})
