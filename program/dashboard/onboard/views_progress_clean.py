from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.conf import settings

from ...models import Program
from progress.models import Progress# , Course as ProgressCourse
from question.models import Score, ScoreSection
from task.models import Task, Approve

from progress.cached import cached_progress_delete

def progress_clean_view(request, program_id, progress_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    progress = get_object_or_404(Progress.objects.select_related('item'), id=progress_id)
    order_item = progress.item
    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_type_program_item = settings.CONTENT_TYPE('program', 'item')
    
    for item in program.item_set.all():
        Progress.objects.filter(item=order_item,
                                content=item.id).delete()
        #cached_progress_delete(order_item.id, content_type_program_item.id, item.id)
        
        if item.content_type_id == settings.CONTENT_TYPE('course', 'course').id:
            pass
            #ProgressCourse.objects.filter(item=order_item,
            #                              content_id=item.content).delete()
        elif item.content_type_id == settings.CONTENT_TYPE('question', 'activity').id:
            Score.objects.filter(item=order_item,
                                 activity_id=item.content).delete()
            ScoreSection.objects.filter(item=order_item,
                                        activity_id=item.content).delete()
        elif item.content_type_id == settings.CONTENT_TYPE('task', 'task').id:
            Approve.objects.filter(task_id=item.content,
                                   program_id=program.id,
                                   account_id=order_item.account_id).delete()
            Task.push_approve(order_item,
                              item.content,
                              program.id,
                              order_item.account_id)
    Progress.objects.filter(item=order_item,
                            content_type=content_type_program).delete()
    cached_progress_delete(order_item.id, content_type_program.id, program.id)
    return redirect('dashboard:onboard-dashboard:progress', program.id)
