from django.shortcuts import render, get_object_or_404, redirect
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.conf import settings

from ...models import Program
from ...cached import cached_onboard_id_list
from content.models import Location as ContentLocation

from .forms import ProgramForm

from utils.crop import crop_image

def create_view(request):
    if not request.user.has_perm('program.add_onboard',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    if request.method == 'POST':
        program_form = ProgramForm(request.POST, request.FILES, instance=Program(account=request.user, type=2))
        if program_form.is_valid():
            program = program_form.save()
            crop_image(program, request)
            cached_onboard_id_list(is_force=True)
            content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('program', 'program'),
                                                          program.id)
            return redirect('dashboard:onboard-dashboard:item', program.id)
    else:
        program_form = ProgramForm()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Onboard Management',
         'url': reverse('dashboard:onboard-dashboard:home')},
        {'is_active': True,
         'title': 'Onboard Create'}
    ]
    return render(request,
                  'program/dashboard/onboard/create.html',
                  {'SIDEBAR': 'onboard',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'program_form': program_form})
