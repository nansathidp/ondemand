from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404

from utils.crop import crop_image
from .forms import ProgramForm
from ...cached import cached_program_update
from ...models import Program


def detail_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    if request.method == 'POST':
        program_form = ProgramForm(request.POST, request.FILES, instance=program)
        if program_form.is_valid():
            program = program_form.save()
            crop_image(program, request)
            cached_program_update(program)
            return redirect('dashboard:onboard-dashboard:item', program.id)
    else:
        program_form = ProgramForm(instance=program)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Onboard Management',
         'url': reverse('dashboard:onboard-dashboard:home')},
        {'is_active': True,
         'title': 'Onboard Information'}
    ]
    return render(request,
                  'program/dashboard/onboard/detail.html',
                  {'SIDEBAR': 'onboard',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'information',
                   'program': program,
                   'program_form': program_form})
