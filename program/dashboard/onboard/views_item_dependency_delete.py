from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from ...models import Program, Item
from dependency.models import Dependency, Parent as DependencyParent

from dependency.cached import cached_dependency_parent_delete
        
@csrf_exempt
def item_dependency_delete_view(request, program_id, item_id, parent_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access_onboard(request):
        raise PermissionDenied

    item = get_object_or_404(Item, program=program, id=item_id)
    content_type = settings.CONTENT_TYPE('program', 'item')
    dependency_parent = get_object_or_404(DependencyParent,
                                          dependency__content_type_id=content_type.id,
                                          dependency__content=item.id,
                                          id=parent_id)
    dependency_parent.delete()
    cached_dependency_parent_delete(content_type.id, item.id)
    program.sort_item()
    return redirect('dashboard:onboard-dashboard:item', program.id)
