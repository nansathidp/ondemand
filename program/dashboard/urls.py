from django.conf.urls import url

from .views import home_view
from .views_create import create_view
from .views_detail import detail_view

from .views_item import item_view
from .views_item_delete import item_delete_view
from .views_item_dependency import item_dependency_view
from .views_item_dependency_add import item_dependency_add_view
from .views_item_dependency_delete import item_dependency_delete_view

from .views_search_course import search_course_view
from .views_add_course import add_course_view

from .views_search_question import search_question_view
from .views_add_question import add_question_view

from .views_search_task import search_task_view
from .views_add_task import add_task_view

from .views_time import time_view

from .views_dependency import dependency_view
from .views_dependency_search import dependency_search_view
from .views_dependency_search_add import dependency_search_add_view
from .views_dependency_search_delete import dependency_search_delete_view
from .views_dependency_delete import dependency_delete_view

from .views_access import access_view
from .views_item_sort import item_sort_view
from .views_progress import progress_view
from .views_analytic import analytic_view

app_name = 'program-dashboard'
urlpatterns = [
    url(r'^$', home_view, name='home'),  # TODO: testing
    url(r'^create/$', create_view, name='create'),  # TODO: testing
    url(r'^(\d+)/$', detail_view, name='detail'),  # TODO: testing
    
    url(r'^(\d+)/item/$', item_view, name='item'),  # TODO: testing
    url(r'^(\d+)/item/sort/$', item_sort_view, name='sort'),  # TODO: testing
    url(r'^(\d+)/item/delete/$', item_delete_view, name='item_delete'),  # TODO: testing
    url(r'^(\d+)/item/dependency/$', item_dependency_view, name='item_dependency'),  # TODO: testing
    url(r'^(\d+)/item/dependency/add/$', item_dependency_add_view, name='item_dependency_add'),  # TODO: testing
    url(r'^(\d+)/item/(\d+)/dependency/(\d+)/delete/$', item_dependency_delete_view, name='item_dependency_delete'),  # TODO: testing
    
    url(r'^(\d+)/search/course/$', search_course_view, name='search_course'),  # TODO: testing
    url(r'^(\d+)/item/add/course/', add_course_view, name='add_course'),  # TODO: testing

    url(r'^(\d+)/search/question/$', search_question_view, name='search_question'),  # TODO: testing
    url(r'^(\d+)/item/add/question/', add_question_view, name='add_question'),  # TODO: testing

    url(r'^(\d+)/search/task/$', search_task_view, name='search_task'),  # TODO: testing
    url(r'^(\d+)/item/add/task/', add_task_view, name='add_task'),  # TODO: testing

    url(r'^(\d+)/access/$', access_view, name='access'),  # TODO: testing

    url(r'^(\d+)/time/$', time_view, name='time'),  # TODO: testing

    # Dependency
    url(r'^(\d+)/dependency/$', dependency_view, name='dependency'),
    url(r'^(\d+)/dependency/search/$', dependency_search_view, name='dependency_search'),
    url(r'^(\d+)/dependency/search/add/$', dependency_search_add_view, name='dependency_search_add'),
    url(r'^(\d+)/dependency/search/delete/$', dependency_search_delete_view, name='dependency_search_delete'),
    url(r'^(\d+)/dependency/delete/$', dependency_delete_view, name='dependency_delete'),

    # Progress
    url(r'^(\d+)/progress/$', progress_view, name='progress'),  # TODO: testing
    # Analytic
    url(r'^(\d+)/analytic/$', analytic_view, name='analytic'),  # TODO: testing
]
