from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from dependency.models import Dependency
from ..models import Program

from utils.content import get_content

@csrf_exempt
def dependency_search_add_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_change(request):
        raise PermissionDenied

    content_type = settings.CONTENT_TYPE('program', 'program')
    dependency = Dependency.pull(content_type,
                                 program.id)
    if request.method == 'POST':
        try:
            content_type_id = int(request.POST.get('content_type'))
            content_id = int(request.POST.get('content'))
            condition = int(request.POST.get('condition'))

            content = get_content(content_type_id, content_id)
            if condition in [1, 2] and content is not None:
                dependency.add_parent(settings.CONTENT_TYPE_ID(content_type_id),
                                      content.id,
                                      condition)
        except:
            pass

    parent_list = Dependency.pull_parent(content_type, program.id)
    return render(request,
                  'program/dashboard/dependency_block.html',
                  {'program': program,
                   'parent_list': parent_list})
