from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from ..models import Program, Item as ProgramItem
from course.models import Course


@csrf_exempt
def add_course_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access(request):
        raise PermissionDenied

    if request.method == 'POST':
        try:
            course = get_object_or_404(Course, id=int(request.POST.get('course', -1)))
        except:
            course  = None
        if course is not None and not course.check_access(request):
            course = None
        if course is None:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
            return JsonResponse({'html': html})

    content_type = settings.CONTENT_TYPE('course', 'course')
    if ProgramItem.objects.filter(program_id=program.id,
                                  content_type=content_type,
                                  content=course.id).exists():
        html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
    else:
        try:
            ProgramItem.objects.create(program=program,
                                       content_type=content_type,
                                       content=course.id,
                                       sort=0.0)
            program.content_count(content_type)
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
        except:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
    return JsonResponse({'html': html})
