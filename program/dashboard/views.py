from functools import wraps

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404

from analytic.cached import cached_analytic_stat
from dashboard.views import condition_view
from program.models import Program
from utils.paginator import paginator


def check_change(view_func):
    def _decorator(request, *args, **kwargs):
        program_id = args[0]
        request.program = get_object_or_404(Program, id=program_id)
        if not request.program.check_change(request):
            raise PermissionDenied
        response = view_func(request, *args, **kwargs)
        return response

    return wraps(view_func)(_decorator)


def home_view(request):
    program_list = Program.access_list(request)
    if program_list is None:
        raise PermissionDenied
    elif program_list == []:
        return condition_view(request)

    program_list = paginator(request, program_list)

    for program in program_list:
        program.analytic_view = cached_analytic_stat('program_%s' % program.id)['stat_now']
        # program.analytic_libary = cached_analytic_stat('program_%s_libary'%program.id)['stat_now']
        # program.analytic_assign = cached_analytic_stat('program_%s_assign'%program.id)['stat_now']

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Program Management'}
    ]
    return render(request,
                  'program/dashboard/home.html',
                  {'SIDEBAR': 'program',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'program_list': program_list,
                   'content_type': settings.CONTENT_TYPE('program', 'program'),
                   'q_name': request.q_name,
                   'q_provider': request.q_provider,
                   'q_provider_list': request.q_provider_list,
                   'q_category': request.q_category,
                   'q_category_list': request.q_category_list})
