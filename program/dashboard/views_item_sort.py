import copy
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from program.models import Item
from program.cached import cached_program_delete
from .views import check_change
from .log import log_item_sort


@csrf_exempt
@check_change
def item_sort_view(request, program_id):
    program = request.program
    if request.method == 'POST':
        item = get_object_or_404(Item,
                                 program=program,
                                 id=request.POST.get('item'))
        item_old = copy.copy(item)

        action = request.POST.get('action')

        item_sort = int(item.sort)
        if item_sort == 0:
            item_int_less = int(item.sort) + 1
            item_int_than = 0
        else:
            item_int_than = int(item.sort) - 1
            item_int_less = int(item.sort) + 1
        cached_program_delete(program.id)
        if action == 'up':

            item_swap = Item.objects.filter(program=program,
                                            sort__lt=item.sort,
                                            sort__gte=item_int_than) \
                .exclude(id=item.id).order_by('-sort') \
                .first()
            if item_swap is not None:
                item_swap.sort_user = item.sort_user
                item_swap.save(update_fields=['sort_user'])
            item.sort_user -= 1
            item.save(update_fields=['sort_user'])
            program.sort_item()
        elif action == 'down':
            item_swap = Item.objects.filter(program=program, sort__gt=item.sort,
                                            sort__lte=item_int_less) \
                .exclude(id=item.id).order_by('sort') \
                .first()
            if item_swap is not None:
                item_swap.sort_user = item.sort_user
                item_swap.save(update_fields=['sort_user'])
            item.sort_user += 1
            item.save(update_fields=['sort_user'])
            program.sort_item()
        log_item_sort(request, item_old, item)
        cached_program_delete(program.id)

    return JsonResponse({})
