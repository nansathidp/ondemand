from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.utils import timezone

from ..models import Program
from analytic.models import Log, Refer

import datetime

def analytic_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access(request):
        raise PermissionDenied
       
    now = timezone.now()

    start = now.date()-datetime.timedelta(days=30)
    end = now.date()
    code_view = 'program_%s'%program.id
    code_cart = 'program_%s_cart'%program.id
    code_buy = 'program_%s_buy'%program.id
    code_libary = 'program_%s_libary'%program.id
    if True:
        chart_result = Log.pull([code_view, code_libary], start, end)
        chart_view_list = chart_result[code_view]
        chart_libary_list = chart_result[code_libary]
        chart_cart_list = []
        chart_buy_list = []
    else:
        chart_result = Log.pull([code_view, code_cart, code_buy], start, end)
        chart_view_list = chart_result[code_view]
        chart_cart_list = chart_result[code_cart]
        chart_buy_list = chart_result[code_buy]
        chart_libary_list = []
    
    refer_list = Refer.pull(code_view, start, end)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Program Management',
         'url': reverse('dashboard:program-dashboard:home')},
        {'is_active': False,
         'title': 'Information: %s'%program.name,
         'url': reverse('dashboard:program-dashboard:detail', args=[program.id])},
        {'is_active': True,
         'title': 'Statistics'}
    ]
    return render(request,
                  'program/dashboard/analytic.html',
                  {'SIDEBAR': 'program',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'program': program,
                   'is_free': True,
                   'chart_view_list': chart_view_list,
                   'chart_cart_list': chart_cart_list,
                   'chart_buy_list': chart_buy_list,
                   'chart_libary_list': chart_libary_list,
                   'refer_list': refer_list})
