from django.conf import settings
from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Program
from task.models import Task
from provider.models import Provider


def search_task_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access(request):
        raise PermissionDenied
    
    q_name = ''
    q_provider = -1
    
    task_list = Task.access_list(request)
    if request.method == 'POST':
        q_name = request.POST.get('q_name', '')
        task_list = task_list.filter(name__icontains=q_name)
        try:
            q_provider = int(request.POST.get('provider', -1))
            if q_provider != -1:
                task_list = task_list.filter(provider_id=q_provider)
        except:
            pass
        task_list = task_list[:settings.CONTENT_LIMIT]
    else:
        task_list = task_list[:settings.CONTENT_LIMIT]
    return render(request,
                  'program/dashboard/search_task.html',
                  {'task_list': task_list,
                   'program': program,
                   'provider_list': Provider.access(request),
                   'q_name': q_name,
                   'q_provider': q_provider})
