from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ..models import Program
from progress.models import Progress


def item_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access(request):
        raise PermissionDenied

    item_list = program.item_set.all()
    for item in item_list:
        item.get_content_cached()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Program Management',
         'url': reverse('dashboard:program-dashboard:home')},
        {'is_active': False,
         'title': 'Program Information',
         'url': reverse('dashboard:program-dashboard:detail', args=[program.id])},
        {'is_active': True,
         'title': 'Outline & Dependency '}
    ]
    return render(request,
                  'program/dashboard/item.html',
                  {'SIDEBAR': 'program',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'item',
                   'program': program,
                   'item_list': item_list,
                   'is_update': Progress.objects.filter(is_update=False).exists()})
