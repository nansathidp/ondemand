from django.conf import settings
from django.core import serializers

from log.models import Log


def _provider(request):
    if request.DASHBOARD_PROVIDER is not None:
        return request.DASHBOARD_PROVIDER.id
    else:
        return -1


def _sub_provider(request):
    if settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
        return request.DASHBOARD_SUB_PROVIDER.id
    else:
        return -1


def _log(code, request, old, new, id):
    if old is None:
        old = ''
    elif type(old) == type([]):
        old = serializers.serialize('json', old)
    else:
        old = serializers.serialize('json', [old])
    if new is None:
        new = ''
    elif type(new) == type([]):
        new = serializers.serialize('json', new)
    else:
        new = serializers.serialize('json', [new])
    Log.push(code,
             _provider(request),
             _sub_provider(request),
             request.user,
             None,
             '',
             settings.CONTENT_TYPE('program', 'course'),
             id,
             old,
             new)


def log_create(request, program):
    _log('create', request, None, program, program.id)


def log_edit(request, program_old, program):
    _log('edit', request, program_old, program, program.id)


def log_item_add(request, item):
    _log('item_add', request, None, item, item.program_id)


def log_item_edit(request, item_old, item):
    _log('item_edit', request, item_old, item, item.program_id)


def log_item_sort(request, item_old, item):
    _log('item_sort', request, item_old, item, item.program_id)


def log_item_delete(request, item_old):
    _log('item_delete', request, item_old, None, item_old.program_id)


def log_material_add(request, material):
    _log('material_add', request, None, material, material.program_id)


def log_material_edit(request, material_old, material):
    _log('material_edit', request, material_old, material, material.program_id)


def log_material_sort(request, material_old, material):
    _log('material_sort', request, material_old, material, material.program_id)


def log_material_display(request, material_old, material):
    _log('material_display', request, material_old, material, material.program_id)


def log_material_delete(request, material_old):
    _log('material_delete', request, material_old, None, material_old.program_id)


def log_material_dependency_add(request, material, dependency_parent):
    _log('material_dependency_add', request, None, [material, dependency_parent], material.program_id)


def log_material_dependency_delete(request, material, dependency_parent_old):
    _log('material_dependency_delete', request, None, [material, dependency_parent_old], material.program_id)

