from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt

from ..models import Program, Item
from dependency.models import Dependency, Parent as DependencyParent

from dependency.cached import cached_dependency


@csrf_exempt
def item_dependency_view(request, program_id):
    program = get_object_or_404(Program, id=program_id)
    if not program.check_access(request):
        raise PermissionDenied

    item_id = int(request.POST.get('item', -1))
    item_list = program.item_set.all()
    item_list = item_list.exclude(id=item_id)
    for item in item_list:
        item.get_content_cached()
    if request.method == 'POST':
        try:

            item = get_object_or_404(Item, program=program, id=item_id)
            dependency = cached_dependency('program', item.id)
            is_first = not dependency.parent_set.exists()
        except:
            item = None
            is_first = True
    else:
        item = None
        is_first = True
    return render(request,
                  'program/dashboard/item_dependency.html',
                  {'item_list': item_list,
                   'condition_choices': DependencyParent.CONDITION_CHOICES,
                   'item': item,
                   'is_first': is_first})
