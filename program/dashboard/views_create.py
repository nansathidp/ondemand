from django.conf import settings
from django.shortcuts import render, get_object_or_404, redirect
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from rating.cached import cached_rating_popular_list_delete

from ..models import Program
from content.models import Location as ContentLocation

from .forms import ProgramForm

from utils.crop import crop_image


def create_view(request):
    if not request.user.has_perm('program.add_program',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    if request.method == 'POST':
        if request.user.has_perm('program.view_program',
                                 group=request.DASHBOARD_GROUP):
            program_form = ProgramForm(None, request.POST, request.FILES, instance=Program(type=1))
        elif request.user.has_perm('program.view_own_program',
                                   group=request.DASHBOARD_GROUP):
            program_form = ProgramForm(request, request.POST, request.FILES, instance=Program(type=1))
        else:
            raise PermissionDenied

        if program_form.is_valid():
            program = program_form.save()
            crop_image(program, request)
            program.push_sub_provider(request)
            cached_rating_popular_list_delete(settings.CONTENT_TYPE('course', 'course'))
            content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('program', 'program'),
                                                          program.id)
            return redirect('dashboard:program-dashboard:item', program.id)
    else:
        if request.user.has_perm('program.view_program', group=request.DASHBOARD_GROUP):
            program_form = ProgramForm(None)
        elif request.user.has_perm('program.view_own_program', group=request.DASHBOARD_GROUP):
            program_form = ProgramForm(request)
        else:
            raise PermissionDenied

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Program Management',
         'url': reverse('dashboard:program-dashboard:home')},
        {'is_active': True,
         'title': 'Program Create'}
    ]
    return render(request,
                  'program/dashboard/create.html',
                  {'SIDEBAR': 'program',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'program_form': program_form})
