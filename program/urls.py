from django.conf.urls import url

from .views import home_view
from .views_detail import detail_view
from .views_buy import buy_view
from .views_onboard import onboard_view

app_name = 'program'
urlpatterns = [
    url(r'^$', home_view, name='home'),  # TODO: testing
    url(r'^(\d+)/$', detail_view, name='detail'),  # TODO: testing
    url(r'^(\d+)/(\d+)/$', detail_view, name='detail'),  # TODO: testing
    url(r'^(\d+)/buy/$', buy_view, name='buy'),  # TODO: testing

    url(r'^onboarding/$', onboard_view, name='onboard'),
]
