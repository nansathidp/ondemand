# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-16 10:52
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('provider', '0001_initial'),
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('category', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Duplicate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('parent_version', models.FloatField()),
                ('program_version', models.FloatField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.BigIntegerField(db_index=True)),
                ('sort', models.FloatField(db_index=True, default=0.0)),
                ('sort_user', models.IntegerField(default=0)),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='program_item_set', to='contenttypes.ContentType')),
            ],
            options={
                'ordering': ['sort'],
            },
        ),
        migrations.CreateModel(
            name='Milestone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120)),
                ('desc', models.TextField(blank=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='program.Item')),
            ],
        ),
        migrations.CreateModel(
            name='Onboard',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'default_permissions': ('view', 'add', 'change'),
            },
        ),
        migrations.CreateModel(
            name='Program',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120)),
                ('version', models.FloatField(default=1.0)),
                ('desc', models.TextField(blank=True)),
                ('overview', models.TextField(blank=True)),
                ('condition', models.TextField(blank=True)),
                ('image', models.ImageField(upload_to='program/image/%Y/%m/')),
                ('type', models.IntegerField(choices=[(1, 'Program'), (2, 'Onboarding Program')], db_index=True, default=1)),
                ('start', models.DateTimeField(null=True)),
                ('end', models.DateTimeField(null=True)),
                ('credit', models.DurationField(default=datetime.timedelta(0))),
                ('expired', models.DurationField(default=datetime.timedelta(0))),
                ('is_display', models.BooleanField(default=False)),
                ('sort', models.IntegerField(db_index=True, default=0)),
                ('extra', models.TextField(blank=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('timeupdate', models.DateTimeField(auto_now=True)),
                ('account', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='category.Category')),
                ('provider', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='provider.Provider')),
            ],
            options={
                'ordering': ['sort'],
                'default_permissions': ('view', 'view_own', 'view_onboard', 'add', 'change', 'delete'),
            },
        ),
        migrations.AddField(
            model_name='milestone',
            name='program',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='program.Program'),
        ),
        migrations.AddField(
            model_name='item',
            name='program',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='program.Program'),
        ),
        migrations.AddField(
            model_name='duplicate',
            name='parent',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='duplicate_parent_set', to='program.Program'),
        ),
        migrations.AddField(
            model_name='duplicate',
            name='program',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='program.Program'),
        ),
    ]
