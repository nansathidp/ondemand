from django.shortcuts import redirect

from order.models import Item
from program.models import Program

from account.cached import cached_onboard_account
from ondemand.views_decorator import check_project
from .views_detail import detail_view


@check_project
def onboard_view(request):
    if request.user.is_authenticated():
        order_item = Item.pull_onboard_item(request.user)
        if order_item is None:
            cached_onboard_account(request.user, is_force=True)
            return redirect('home')

        program = Program.pull(order_item.content)
        if program is None:
            return redirect('login')
        else:
            return detail_view(request, program.id, order_item_id=order_item.id)
    else:
        return redirect('login')
