from django.db import models


class Onboard(models.Model):
    #Use Permission Only

    class Meta:
        default_permissions = ('view', 'add', 'change')


class Program(models.Model):
    from django.conf import settings
    import datetime

    TYPE_CHOICES = (
        (1, 'Program'),
        (2, 'Onboarding Program'),
    )

    name = models.CharField(max_length=120)
    version = models.FloatField(default=1.0)
    category = models.ForeignKey('category.Category')
    provider = models.ForeignKey('provider.Provider', null=True, on_delete=models.SET_NULL)

    desc = models.TextField(blank=True)
    overview = models.TextField(blank=True)
    condition = models.TextField(blank=True)
    image = models.ImageField(upload_to='program/image/%Y/%m/')
    type = models.IntegerField(choices=TYPE_CHOICES, default=1, db_index=True)

    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)
    credit = models.DurationField(default=datetime.timedelta(0))
    expired = models.DurationField(default=datetime.timedelta(0))

    is_display = models.BooleanField(default=False)
    sort = models.IntegerField(default=0, db_index=True)

    extra = models.TextField(blank=True)

    # TODO: Move to log
    account = models.ForeignKey(settings.AUTH_USER_MODEL, null=True)  # Created By
    timestamp = models.DateTimeField(auto_now_add=True)
    timeupdate = models.DateTimeField(auto_now=True)

    class Meta:
        default_permissions = ('view', 'view_own',
                               'view_onboard',  # Super Learning Center
                               'add', 'change', 'delete')
        ordering = ['sort']

    def __str__(self):
        return str(self.id)

    @staticmethod
    def pull(id):
        from .cached import cached_program
        return cached_program(id)

    @staticmethod
    def access_list(request):
        from django.conf import settings
        from provider.models import Provider
        from category.models import Category
        from django.core.exceptions import PermissionDenied

        from utils.filter import get_q_dashboard

        request.q_provider_list = Provider.pull_list()
        request.q_category_list = Category.pull_list()
        request.q_provider = get_q_dashboard(request, 'provider', 'int')

        if request.user.has_perm('program.view_program',
                                 group=request.DASHBOARD_GROUP):
            content_list = Program.objects.select_related('provider')  \
                                          .filter(type=1)
            if request.q_provider:
                content_list = content_list.filter(provider_id=request.q_provider)
        elif request.user.has_perm('program.view_own_program',
                                   group=request.DASHBOARD_GROUP):
            request.q_provider_list = None
            if request.DASHBOARD_PROVIDER is not None:
                content_list = Program.objects.select_related('provider') \
                                              .filter(type=1,
                                                      provider__in=Provider.access(request))
            elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                from subprovider.models import Item
                content_list = Program.objects.select_related('provider') \
                                              .filter(type=1,
                                                      id__in=Item.objects
                                                      .values_list('content', flat=True)
                                                      .filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                                              content_type=settings.CONTENT_TYPE('program', 'program')))
            else:
                content_list = Program.objects.none()
        else:
            # content_list = Program.objects.none()
            raise PermissionDenied

        request.q_name = " ".join(get_q_dashboard(request, 'name', 'str').split())
        if request.q_name != '':
            content_list = content_list.filter(name__icontains=request.q_name)
        request.q_category = get_q_dashboard(request, 'category', 'int')
        if request.q_category:
            content_list = content_list.filter(category=request.q_category)
        return content_list

    @staticmethod
    def access_list_onboard(request):
        from provider.models import Provider
        from category.models import Category
        from django.core.exceptions import PermissionDenied

        from utils.filter import get_q_dashboard

        request.q_provider_list = Provider.pull_list()
        request.q_category_list = Category.pull_list()
        request.q_provider = get_q_dashboard(request, 'provider', 'int')

        if request.user.has_perm('program.view_onboard',
                                 group=request.DASHBOARD_GROUP):
            content_list = Program.objects.select_related('provider') \
                                          .filter(type=2) \
                                          .order_by('-timestamp')
            if request.q_provider:
                content_list = content_list.filter(provider_id=request.q_provider)
        else:
            # content_list = Program.objectsnone()
            raise PermissionDenied

        request.q_name = " ".join(get_q_dashboard(request, 'name', 'str').split())
        if request.q_name != '':
            content_list = content_list.filter(name__icontains=request.q_name)
        request.q_category = get_q_dashboard(request, 'category', 'int')
        if request.q_category:
            content_list = content_list.filter(category=request.q_category)
        return content_list

    @staticmethod
    def pull_category(category_id, provider):
        return Program.objects.values_list('id', flat=True).filter(category_id=category_id, is_display=True, provider=provider)

    def check_access(self, request):
        from django.conf import settings
        if self.type != 1:
            return False
        if request.user.has_perm('program.view_program',
                                 group=request.DASHBOARD_GROUP):
            return True
        elif request.user.has_perm('program.view_own_program',
                                   group=request.DASHBOARD_GROUP):
            if self.provider.account_set.filter(account=request.user).exists():
                return True
            if settings.IS_SUB_PROVIDER:
                from subprovider.models import Item
                if request.DASHBOARD_SUB_PROVIDER is None:
                    return False
                else:
                    return Item.objects.filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                               content_type=settings.CONTENT_TYPE('program', 'program'),
                                               content=self.id).exists()
            else:
                return False
        else:
            return False

    def check_access_onboard(self, request):
        if self.type != 2:
            return False
        if request.user.has_perm('program.view_onboard',
                                 group=request.DASHBOARD_GROUP):
            return True
        else:
            return False

    def check_change(self, request):
        from django.conf import settings
        if request.user.has_perm('program.change_program',
                                 group=request.DASHBOARD_GROUP):
            if request.user.has_perm('program.view_program',
                                     group=request.DASHBOARD_GROUP):
                return True
            elif request.user.has_perm('program.view_own_program',
                                       group=request.DASHBOARD_GROUP):
                if self.provider.account_set.filter(account=request.user).exists():
                    return True
                if settings.IS_SUB_PROVIDER:
                    from subprovider.models import Item
                    if request.DASHBOARD_SUB_PROVIDER is None:
                        return False
                    else:
                        return Item.objects.filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                                   content_type=settings.CONTENT_TYPE('program', 'program'),
                                                   content=self.id).exists()
                else:
                    return False
            else:
                return False
        else:
            return False

    def push_sub_provider(self, request):
        from django.conf import settings
        if not settings.IS_SUB_PROVIDER:
            return
        from subprovider.models import SubProvider
        try:
            sub_provider = SubProvider.objects.get(id=int(request.POST.get('sub_provider')))
        except:
            return
        sub_provider.push_item(settings.CONTENT_TYPE('program', 'program'), self.id)

    def api_display_title(self, store=1, install_version=0.0, store_version=0.0):
        from price.models import Price
        from django.conf import settings
        from rating.models import Rating
        from app.views import update_checkout_type
        content_type_program = settings.CONTENT_TYPE('program', 'program')
        price = Price.pull(content_type_program, self.id, store)
        price_set = price.api_display()
        result = {
            'id': self.id,
            'name': self.name,
            'image': self.image.url if self.image else None,
            'price_set': price_set,
            'type': self.type,
            'type_display': self.get_type_display(),
            'rating': Rating.pull_first(content_type_program, self.id).api_display(),
            'category': {
                'id': self.category_id,
                'name': self.category.name
            }
        }
        update_checkout_type(result, install_version, store_version)
        return result

    def api_display(self, store, install_version=0.0, store_version=0.0):
        result = self.api_display_title(store, install_version, store_version)
        if self.provider:
            provider = self.provider.api_display_title()
        else:
            provider = None

        if self.category:
            category = self.category.api_display_title()
        else:
            category = None

        result.update(
            {
                'desc': self.desc,
                'overview': self.overview,
                'condition': self.condition,
                'expired': self.expired.days,
                'expired_display': self.get_expired_display(),
                'provider': provider,
                'category': category,
                'credit': int(self.credit.total_seconds()/3600)
            }
        )

        return result

    def get_extra(self):
        import json
        try:
            extra = json.loads(self.extra)
        except ValueError:
            extra = {}
        return extra

    def get_extra_value(self, value):
        def _get_value(extra, value):
            return extra[value]

        try:
            import json
            extra = json.loads(self.extra)
        except ValueError:
            extra = {}
        try:
            return _get_value(extra, value)
        except:
            return '-'

    def get_credit_unlimited(self):
        if int(self.credit.total_seconds()) == 0:
            return True
        else:
            return False

    def get_credit_hour(self):
        s = self.credit.total_seconds()
        d, s = divmod(s, 60*60*24)
        return int(s/(60*60))

    def get_credit_minute(self):
        s = self.credit.total_seconds()
        h, s = divmod(s, 60*60)
        return int(s/60)

    def get_credit_second(self):
        s = self.credit.total_seconds()
        h, s = divmod(s, 60*60)
        m, s = divmod(s, 60)
        return int(s)

    def get_expired_unlimited(self):
        if int(self.expired.total_seconds()) == 0:
            return True
        else:
            return False

    def get_expired_hour(self):
        s = self.expired.total_seconds()
        d, s = divmod(s, 60*60*24)
        return int(s/(60*60))

    def get_expired_minute(self):
        s = self.expired.total_seconds()
        h, s = divmod(s, 60*60)
        return int(s/60)

    def get_expired_second(self):
        s = self.expired.total_seconds()
        h, s = divmod(s, 60*60)
        m, s = divmod(s, 60)
        return int(s)

    def sort_item(self):
        from django.conf import settings
        from dependency.cached import cached_dependency_parent

        content_type = settings.CONTENT_TYPE('program', 'item')
        content_type_milestone = settings.CONTENT_TYPE('program', 'milestone')

        def find_level(item, parent_list, sort):
            _parent_list = []
            for parent in parent_list:
                _parent_list.append(parent)
            if len(_parent_list) > 0:
                max = sort
                for _item in _parent_list:
                    parent_list = cached_dependency_parent('', _item.content, content_type=content_type)
                    _ = find_level(_item, parent_list, sort+1.0)
                    if _ > max:
                        max = _
                return max
            else:
                return sort
        p = (int(self.item_set.count()/10)+1)*-1
        base = pow(10, p)

        for _item in self.item_set.all():
            if _item.content_type_id != content_type_milestone.id:
                parent_list = cached_dependency_parent('', _item.id, content_type=content_type)
                sort = find_level(_item, parent_list, _item.sort_user*base)
                _item.sort = sort
                _item.save(update_fields=['sort'])
        for _item in self.item_set.all():
            if _item.content_type_id == content_type_milestone.id:
                milestone = Milestone.objects.select_related('item').get(id=_item.content)
                _item.sort = milestone.item.sort + base
                _item.save(update_fields=['sort'])
                for item in Item.objects.filter(program_id=self.id,
                                                sort__gte=_item.sort,
                                                sort__lt=int(_item.sort) + 1).exclude(content_type=content_type_milestone):
                    _item.sort += base
                    item.sort = _item.sort
                    item.save(update_fields=['sort'])
        sort = 0
        for _item in self.item_set.exclude(content_type=content_type_milestone):
            _item.sort_user = sort
            _item.save(update_fields=['sort_user'])
            sort += 1

    def content_count(self, content_type=None):
        from django.conf import settings
        from .cached import cached_program_update
        import json

        try:
            extra = json.loads(self.extra)
        except ValueError:
            extra = {}

        if content_type == settings.CONTENT_TYPE('course', 'course'):
            extra['count_course'] = self.item_set.filter(content_type=content_type).count()
            cached_program_update(self)
        elif content_type == settings.CONTENT_TYPE('question', 'activity'):
            extra['count_question'] = self.item_set.filter(content_type=content_type).count()
            cached_program_update(self)
        elif content_type == settings.CONTENT_TYPE('task', 'task'):
            extra['count_task'] = self.item_set.filter(content_type=content_type).count()
        else:
            extra['count_course'] = self.item_set.filter(content_type=content_type).count()
            extra['count_question'] = self.item_set.filter(content_type=content_type).count()
            extra['count_task'] = self.item_set.filter(content_type=content_type).count()
            cached_program_update(self)

        self.extra = json.dumps(extra)
        self.save(update_fields=['extra'])
        cached_program_update(self)

    def progress_update(self):
        from django.conf import settings
        from progress.models import Progress

        Progress.objects.filter(content_type=settings.CONTENT_TYPE('program', 'program'),
                                content=self.id,
                                status__in=[1, 3, 5]) \
                        .update(is_update=False)

    def progress_count(self):
        from django.conf import settings
        from utils.content import get_content

        result = {'content_count': 0,
                  'section_count': 0,
                  'material_count': 0}

        for item in self.item_set.all():
            if item.content_type_id == settings.CONTENT_TYPE('program', 'milestone').id:
                continue
            content_object = get_content(item.content_type_id, item.content)
            if content_object is not None:
                d = content_object.progress_count()
                result['content_count'] += 1
                result['section_count'] += d['section_count']
                result['material_count'] += d['material_count']
        return result

    def get_expired_html(self):
        s = self.expired.total_seconds()
        h, s = divmod(s, 60*60)
        m, s = divmod(s, 60)
        print(h, m, s)
        return '%d.%d Hour(s)' % (h, m)

    def get_expired_display(self):
        # Fix CIMB 2017-10-24
        from utils.duration import duration_display
        if self.expired.total_seconds() > 0:
            return duration_display(self.expired)
        else:
            return 'Unlimited'

class Item(models.Model):
    program = models.ForeignKey(Program)
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='program_item_set')
    content = models.BigIntegerField(db_index=True)
    sort = models.FloatField(default=0.0, db_index=True)
    sort_user = models.IntegerField(default=0)

    class Meta:
        ordering = ['sort']

    def __str__(self):
        return str(self.content)

    def api_display(self):
        result = {'content': self.content }
        return result

    def get_dependency_parent(self):
        from dependency.cached import cached_dependency_parent
        parent_list = cached_dependency_parent('program', self.id)
        for parent in parent_list:
            parent.get_content_cached()
        return parent_list

    def get_content_cached(self, order_item=None):
        from utils.content_cached import _get_content_cached
        _get_content_cached(self, order_item)

    def get_url_progress(self, order_item, program):
        from django.conf import settings
        from django.core.urlresolvers import reverse

        from content.models import Location as ContentLocation

        content_type_course = settings.CONTENT_TYPE('course', 'course')
        content_type_question = settings.CONTENT_TYPE('question', 'activity')
        if self.content_type_id == content_type_course.id:
            location = ContentLocation.pull(content_type_course,
                                            self.content,
                                            order_item,
                                            program,
                                            None)
            self.url_progress = reverse('dashboard:progress:course_account', args=[self.content, order_item.account_id, location.id])
        elif self.content_type_id == content_type_question.id:
            location = ContentLocation.pull(content_type_question,
                                            self.content,
                                            order_item,
                                            program,
                                            None)
            self.url_progress = reverse('dashboard:question:score_account', args=[self.content, order_item.account_id, location.id])
        else:
            self.url_progress = '#'

    def is_dependency(self): #anonymous
        from dependency.cached import cached_dependency_parent
        parent_list = cached_dependency_parent('program', self.id)
        if len(parent_list) == 0:
            return False
        else:
            return True

    def check_dependency(self, item):
        from django.conf import settings
        from dependency.models import Dependency
        from dependency.cached import cached_dependency_parent

        content_type = settings.CONTENT_TYPE('program', 'item')
        result = {'is_dependency': False}
        parent_list = cached_dependency_parent('', self.id, content_type=content_type)
        if len(parent_list) == 0:
            result['is_dependency'] = False
        else:
            result['is_dependency'] = Dependency.check_dependency_item(item, parent_list)
        return result

    def progress_count(self):
        from utils.content import get_content

        result = {'content_count': 0,
                  'section_count': 0,
                  'material_count': 0}

        content_object = get_content(self.content_type_id, self.content)
        if content_object is not None:
            result = content_object.progress_count()
        return result


class Duplicate(models.Model):
    from django.conf import settings

    parent = models.ForeignKey(Program, related_name='duplicate_parent_set')
    parent_version = models.FloatField()
    program = models.ForeignKey(Program)
    program_version = models.FloatField()
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    timestamp = models.DateTimeField(auto_now_add=True)


class Milestone(models.Model):
    program = models.ForeignKey(Program)
    item = models.ForeignKey(Item)
    name = models.CharField(max_length=120)
    desc = models.TextField(blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def pull(milestone_id):
        return Milestone.objects.filter(id=milestone_id).first()

    def progress_count(self):
        return {
            'content_count': 0,
            'section_count': 0,
            'material_count': 0
        }
