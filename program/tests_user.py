from django.core.cache import cache
from django.test import TestCase
from django.urls import reverse

from account.models import Account
from program.tests import create_program

from .tests import _random


class UserTests(TestCase):

    def setUp(self):
        cache._cache.flush_all()
        self.user = Account.objects.create_user('%s@test.com' % _random(), '123456')
        self.client.force_login(self.user)

    def test_detail(self):
        program = create_program()
        response = self.client.get(reverse('program:detail', args=[program.id]))
        self.assertEqual(response.status_code, 200)

    def test_detai_item(self):
        program = create_program()
        response = self.client.get(reverse('program:detail', args=[program.id]))
        self.assertEqual(response.status_code, 200)

    def test_buy(self):
        program = create_program()
        response = self.client.get(reverse('program:buy', args=[program.id]))
        self.assertEqual(response.status_code, 302)
