from django.core.cache import cache
from django.test import TestCase
from django.urls import reverse

from ondemand import settings
from ondemand.tests import PROJECT_LIST
from program.tests import create_program


class AnonymousTests(TestCase):

    def setUp(self):
        cache._cache.flush_all()

    def test_detail(self):
        program = create_program()
        response = self.client.get(reverse('program:detail', args=[program.id]))
        if settings.PROJECT in PROJECT_LIST:
            self.assertEqual(response.status_code, 302)
        else:
            self.assertEqual(response.status_code, 200)

    def test_buy(self):
        program = create_program()
        response = self.client.get(reverse('program:buy', args=[program.id]))
        self.assertEqual(response.status_code, 302)
