import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.contrib.auth.models import Group
from department.models import Department, Member as DepartmentMember
from account.models import Account
from institute.models import Institute, Account as InstituteAccount

def get_department(name):
    name = name.strip()
    department = Department.objects.filter(name=name).first()
    if department is None:
        department = Department.objects.create(name=name,
                                               is_display=True)
    return department

def get_sub_department(parent, name):
    name = name.strip()
    department = Department.objects.filter(name=name).first()
    if department is None:
        department = Department.objects.create(name=name,
                                               is_display=True)
        department.parents.add(parent)
    else:
        department.parents.clear()
        department.parents.add(parent)
    return department

def get_group(name):
    name = name.strip().lower()
    group = None
    if name == 'learner':
        group = None
    elif name == 'instructure':
        #group = Group.objects.get(id=8)
        group = Group.objects.get(id=7)
    return group

def get_institute(name):
    name = name.strip()
    institute = Institute.objects.filter(name__startswith=name).first()
    if institute is None:
        institute = Institute.objects.create(name='%s Learning Center'%(name))
    return institute
    
if __name__ == '__main__':
    for line in sys.stdin:
        line = line.split(',')
        first_name = line[4].strip()
        last_name = line[5].strip()
        email = line[6].strip()
        if email.find('@') == -1:
            continue
        department = get_department(line[0])
        if len(line[1].strip()) > 0:
            department_sub1 = get_sub_department(department, line[1])
        else:
            department_sub1 = None

        if department_sub1 is not None and len(line[2].strip()) > 0:
            department_sub2 = get_sub_department(department_sub1, line[2])
        else:
            department_sub2 = None

        group = get_group(line[3])
        print(email)
        account = Account.objects.filter(email=email).first()
        if account is None:
            account = Account(email=email)
        account.first_name = first_name
        account.last_name = last_name
        account.set_password('1175')
        account.save()
        if group is not None:
            account.groups.clear()
            account.groups.add(group)
        DepartmentMember.objects.filter(account=account).delete()
        if department_sub2 is not None:
            DepartmentMember.push(department_sub2, account, '')
        elif department_sub1 is not None:
            DepartmentMember.push(department_sub1, account, '')
        else:
            DepartmentMember.push(department, account, '')

        if group is not None:
            institute = get_institute(line[0])
            InstituteAccount.push(institute, account)
