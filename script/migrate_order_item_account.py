import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from order.models import Item

import time

if __name__ == '__main__':
    count = 1
    for item in Item.objects.filter(account__isnull=True):
        print '%s >> %s at %s'%(count, item.id, item.timestamp)
        item.account = item.order.account
        item.save(update_fields=['account'])
        count += 1
        time.sleep(0.1)
