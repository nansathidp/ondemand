import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

import datetime
from dateutil import tz

from django.db.models import Count

from account.models import Account
from active.models import LoginLog, LoginDepartmentAmount
from department.models import Department, Admin, Member

if __name__ == '__main__':
    date_list = LoginLog.objects.all().extra(select={'datestamp': 'DATE(first_active)'}).values_list('datestamp', flat=True).order_by('datestamp').distinct()
    for date in date_list:
        date_min = datetime.datetime.combine(date, datetime.time.min).replace(tzinfo=tz.tzlocal())
        date_max = datetime.datetime.combine(date, datetime.time.max).replace(tzinfo=tz.tzlocal())
        active_user = LoginLog.objects.filter(first_active__range=(date_min, date_max)).values_list('account', flat=True).distinct()
        department_sum = Member.objects.filter(account__in=active_user).values('department').annotate(sum=Count('department'))
        for department in department_sum:
            department['date'] = date
            department_log = LoginDepartmentAmount.objects.filter(department=department['department'], date=department['date']).first()
            if department_log:
                department_log.sum = department['sum']
                department_log.save(update_fields=['sum'])
            else:
                LoginDepartmentAmount.objects.create(
                    department_id=department['department'],
                    sum=department['sum'],
                    date=department['date']
                )