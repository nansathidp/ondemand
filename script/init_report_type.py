import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.utils import timezone

from institute.models import Institute
import datetime
from conicle.script.cron.dashboard_report_day import cal_report_day_type0, cal_report_day_type2, cal_report_day_type3, cal_report_day_type4, cal_report_day_type20, cal_report_day_type21, cal_report_day_type22, cal_report_day_type23

if __name__ == '__main__':
    if len(sys.argv) > 1:
        try:
            t = int(sys.argv[1])
        except:
            print 'Type is Integer.'
            exit()
        if not t in [0, 2, 3, 4, 20, 21, 22, 23]:
            print 'Type is not Allow.'
            exit()
    else:
        print 'Type Report type.'
        exit()
    if len(sys.argv) == 3:
        try:
            date = datetime.datetime.strptime(sys.argv[2], '%Y-%m-%d')
            date_start = timezone.now().replace(year=date.year, month=date.month, day=date.day, hour=0, minute=0, second=0, microsecond=0)
        except:
            print 'Date Format YYYY-MM-DD'
            exit()
    else:
        date_start = timezone.now().replace(year=2015, month=10, day=1, hour=0, minute=0, second=0, microsecond=0)
    institute_list = Institute.objects.all()
    while date_start.date() <= timezone.now().date() - datetime.timedelta(days=1):
        print 'Cal : %s'%date_start.date()
        if t == 0:
            cal_report_day_type0(institute_list, date_start.date())
        elif t == 2:
            cal_report_day_type2(institute_list, date_start, date_start+datetime.timedelta(days=1))
        elif t == 3:
            cal_report_day_type3(institute_list, date_start, date_start+datetime.timedelta(days=1))
        elif t == 4:
            cal_report_day_type4(institute_list, date_start, date_start+datetime.timedelta(days=1))
        elif t == 20:
            cal_report_day_type20(institute_list, date_start.date())
        elif t == 21:
            cal_report_day_type21(institute_list, date_start.date())
        elif t == 22:
            cal_report_day_type22(institute_list, date_start.date())
        elif t == 23:
            cal_report_day_type23(institute_list, date_start.date())

        date_start += datetime.timedelta(days=1)
