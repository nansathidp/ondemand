__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from order.models import Order
from app.models import App
if __name__ == '__main__':
    # Click
    app_click = App.objects.get(id=3)
    for order in Order.objects.filter(app=None):
        order.app = app_click
        order.save(update_fields=['app'])
        print order.id