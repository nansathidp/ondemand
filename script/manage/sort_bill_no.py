import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from order.models import Bill

if __name__ == '__main__':
    bill_no = 1
    for bill in Bill.objects.all().order_by('no'):
        bill.no = bill_no
        bill_no += 1
        bill.save(update_fields=['no'])