__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from course.models import Course

if __name__ == '__main__':
    count = 0
    for course in Course.objects.filter(is_display=True):
        name = course.name.replace('\"','')
        name = name.replace(" : ","-")
        name = name.replace(" - ","-")
        name = name.replace(" ","-")
        name = name.replace("!", "")
        name = name.replace("----", "-")
        name = name.replace("---", "-")
        name = name.replace("--", "-")
        course.slug = name
        course.save(update_fields=['slug'])
        print course.id
