from operator import attrgetter

__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from stream.models import Stream

if __name__ == '__main__':
    list_video = []
    list_video_print = []
    course_list = [1446455501287312, 1446718859299124]
    duration_list = []
    for stream in Stream.objects.filter(is_verify_pass=False, video__course_id__in=course_list):
        video = stream.video
        if stream.video.duration is not None and stream.video.duration.total_seconds() != 0:
            duration = "%d:%02d:%02d" %(int(stream.video.duration.total_seconds()/3600),int((stream.video.duration.total_seconds()/60)%60),int((stream.video.duration.total_seconds())%60))
            video.duration_str = duration
            list_video.append(video)
            duration_list.append(duration)
        else:
            duration = "0:00:00"

    for video in sorted(list_video, key=attrgetter('duration')):
        list_video_print.append('%s|%s' % (video.id, video.duration_str))
    duration_list.sort()
    print list_video_print
    # print duration_list