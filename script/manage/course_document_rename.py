# -*- encoding: utf-8 -*-
__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from course.models import Video
if __name__ == '__main__':
        for video in Video.objects.filter(type=2):
            if video.pdf:
                if os.path.exists(video.pdf.path):
                    try:
                        output = '%s/%s.pdf' % (video.pdf.path.rsplit('/', 1)[0], video.id)
                        os.rename(video.pdf.path, output)
                        video.pdf = 'course/pdf/' + output.rsplit('/course/pdf/', 1)[1]
                        video.save(update_fields=['pdf'])
                        print video.pdf
                    except:
                        print video.id