__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from course.models import Course
if __name__ == '__main__':
    count = 0
    for course in Course.objects.filter(is_display=True):
        for outline in course.outline_set.all():
            for video in outline.video_set.filter(is_display=True):
                if video.image:
                    if not os.path.exists(video.image.path):
                        video.image = None
                        video.save(update_fields=['image'])
                        print video.id