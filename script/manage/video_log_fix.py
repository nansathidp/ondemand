import os, sys, django

import datetime
from django.db.models import Sum

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from order.cached import cached_order_item_update
from progress.models import Video,Log
from time import sleep
if __name__ == '__main__':
    time_query = datetime.timedelta(minutes=3)
    item_list = []
    count = 0
    count_log = Log.objects.filter(is_fail=False,credit_use__gte=time_query).exclude(credit_use=None).count()
    print 'Log count : %s' % count_log
    for log in Log.objects.filter(is_fail=False,credit_use__gte=time_query).exclude(credit_use=None):
        count_log += 1
        try:
            if log.credit_use > log.video.duration:
                print log.id
                count += 1
                if log.item not in item_list:
                    item_list.append(log.item)
                log.delete()
        except:
            print 'Except : %s' % log.id
    print 'Delete : %s' % count
    for item in item_list:
        sleep(0.05)
        item.use_credit = Log.objects.filter(item_id=item.id).aggregate(Sum('credit_use'))['credit_use__sum']
        item.is_active(True)
        item.save(update_fields=['use_credit'])
        cached_order_item_update(item)
        print 'Updated item : %s' % item.id