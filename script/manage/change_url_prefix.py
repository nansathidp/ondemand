__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from course.models import Course
from campaign.models import Campaign

if __name__ == '__main__':
    count = 0
    form = 'http://conicle.com'
    to = 'http://103.246.16.183'
    for course in Course.objects.filter(is_display=True):
        course.preview_video_path = course.preview_video_path.replace(form,to)
        print 'Course : %s' % course.id
        course.save(update_fields=['preview_video_path'])
    for campaign in Campaign.objects.all():
        campaign.video = campaign.video.replace(form,to)
        campaign.video_sample = campaign.video_sample.replace(form,to)
        print 'Campaign : %s' % campaign.id
        campaign.save(update_fields=['video','video_sample'])
        for preview in campaign.previewvideo_set.all():
            preview.path = preview.path.replace(form,to)
            print '  \  Preview : %s' % preview.id
            preview.save(update_fields=['path'])