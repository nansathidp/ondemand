__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from course.models import Course
if __name__ == '__main__':
    count = 0
    for course in Course.objects.filter(is_display=True):
        for outline in course.outline_set.all():
            for video in outline.video_set.filter(type__in=[1, 2], is_display=True):
                video.type_video = -1
                video.save(update_fields=['type_video'])
                print video.id