__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from course.models import Course
import datetime
if __name__ == '__main__':
    count = 0
    for course in Course.objects.filter(is_display=True):
        n = datetime.timedelta(0)
        course.duration = datetime.timedelta(0)
        for outline in course.outline_set.all():
            for video in outline.video_set.filter(type=0,is_display=True):
                n += video.duration
        course.duration = datetime.timedelta(seconds=n.total_seconds())
        course.save(update_fields=['duration'])
        print course.id