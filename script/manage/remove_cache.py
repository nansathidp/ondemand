__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from django.core.cache import cache
from memcached_stats import MemcachedStats

if __name__ == '__main__':

    if len(sys.argv) == 2:
        cached_key = sys.argv[1]
        mem = MemcachedStats()
        for cached in mem.key_details():
            key = ':'.join(cached[0].split(':')[2:])
            if cached_key in key:
                cache.delete(key)
                print key
    else:
        print 'Wrong parameter.'

