# -*- encoding: utf-8 -*-
__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
import os.path
import shutil
from django.conf import settings
from account.models import Account
if __name__ == '__main__':
    count = 0
    response = str()
    profile_path = settings.STATICFILES_DIRS[0] + 'images/profile_default.jpg'
    for account in Account.objects.filter():
        if account.image:
            if not os.path.exists(account.image.path):
                if not os.path.exists(os.path.dirname(account.image.path)):
                    os.makedirs(os.path.dirname(account.image.path))
                shutil.copy2(profile_path, account.image.path)
                print account.id
                count += 1
    print 'Account : %s' % count
