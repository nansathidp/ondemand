import datetime
import django
import os
import sys

from django.db.models import Sum

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from order.cached import cached_order_item_update
from progress.models import Log
from order.models import Item
from time import sleep
if __name__ == '__main__':
    time_query = datetime.timedelta(minutes=3)
    item_list = []
    count = 0

    for item in Item.objects.filter(is_free=False, status=2).order_by('-use_credit')[:500]:
        sleep(0.05)
        item.use_credit = Log.objects.filter(item_id=item.id).aggregate(Sum('credit_use'))['credit_use__sum']
        item.is_active(True)
        item.save(update_fields=['use_credit'])
        cached_order_item_update(item)
        print 'Updated item : %s' % item.id