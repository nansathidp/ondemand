import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings

from order.models import Item
from course.models import Course
from package.models import Package

if __name__ == '__main__':
    count = 0
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_package = settings.CONTENT_TYPE('package', 'package')
    for item in Item.objects.all():
        try:
            if item.content_type == content_type_course:
                course = Course.objects.get(id=item.content)
                item.content_name = course.name
                item.save(update_fields=['content_name'])
                print item.id
            elif item.content_type == content_type_package:
                package = Package.objects.get(id=item.content)
                item.content_name = package.name
                item.save(update_fields=['content_name'])
                print item.id
            else:
                print 'Not fount : %s' % item.id
        except:
            print 'Except : %s' % item.id
