import os, sys, django
import json
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from lesson.models import Lesson, Subscribe,Category,SlideImage
from account.models import Account
from random import randint

if __name__ == '__main__':
    BASE = os.path.dirname(os.path.abspath(__file__))
    jsonfile = open(BASE+'/lesson.json', "r")
    account  = Account.objects.get(email='kittitochp@gmail.com')
    for item in json.loads(jsonfile.read().replace("\'", "\\'"), strict=False):
        if item.get('publish') == 1 and item.get('type') == 'Lesson':
            lesson, created = Lesson.objects.get_or_create(name=item.get('subject'))
            lesson.desc = item.get('overview')
            lesson.credit = item.get('link')
            lesson.video = item.get('vdo')
            lesson.account = account
            # lesson.category = None
            if 'you' in item.get('vdo_type'):
                lesson.video_type = 2
            elif 'vim' in item.get('vdo_type'):
                lesson.video_type = 1
            else:
                lesson.video_type = 0
            lesson.save()
            #print item.get('subject') 
