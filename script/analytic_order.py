import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings
from order.models import Item
from analytic.models import Stat

def _push_stat(date, result):
    print('Date: ', date)
    for key in result:
        #print(key, ' -> ', result[key])
        Stat.set_value(key, date, result[key])
                 
if __name__ == '__main__':
    date = None
    result = {}
    for item in Item.objects.select_related('order').all():
        stat_code = None
        if date is None:
            date = item.timestamp.date()
        if date != item.timestamp.date():
            date = item.timestamp.date()
            _push_stat(date, result)
        item.get_content_cached()
        if item.content_type_cached == settings.CONTENT_TYPE('course', 'course'):
            stat_code = 'course_%s'%item.content
        elif item.content_type_cached == settings.CONTENT_TYPE('question', 'activity'):
            stat_code = 'question_%s'%item.content
        elif item.content_type_cached == settings.CONTENT_TYPE('program', 'program'):
            if item.content_cached is not None:
                if item.content_cached.type == 1: #Program
                    stat_code = 'program_%s'%item.content
                elif item.content_cached.type == 2: #Onboarding
                    stat_code = 'program_onboard_%s'%item.content
        elif item.content_type_cached == settings.CONTENT_TYPE('task', 'task'):
            stat_code = 'task_%s'%item.content
        if stat_code is not None:
            if item.order.method == 21:
                stat_code += '_assign'
            elif item.is_free:
                stat_code += '_libary'
            else:
                stat_code += '_buy'
            if stat_code in result:
                result[stat_code] += 1
            else:
                result[stat_code] = 1
                
        #print('Date', date)
    _push_stat(date, result)
