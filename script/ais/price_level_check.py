__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from aismpay.models import Level
from course.models import Price
if __name__ == '__main__':
    price_not_found = []
    price_level = [level.volume/100 for level in Level.objects.all()]
    multiple = 1
    if len(sys.argv) > 1:
        multiple = int(sys.argv[1])
    for price in Price.objects.filter(store=1).order_by('price'):
        if int(price.price) not in price_level and int(price.price) not in price_not_found:
            price_not_found.append(price.price)
            print (int(price.price) * multiple)