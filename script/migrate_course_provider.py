import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from course.models import Course


if __name__ == '__main__':
    course_list = Course.objects.filter(provider=None)
    for course in course_list:
        tutor = course.tutor.first()
        if tutor is not None:
            providers = tutor.providers.first()
            if providers is not None:
                course.provider = providers
                course.save(update_fields=['provider'])
                print('Course: %s' % course.name)
