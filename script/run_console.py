import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.utils import timezone
from django.conf import settings

from console.models import Deploy

import subprocess

if __name__ == '__main__':
    if Deploy.objects.filter(status=0).count() != 0:
        script = settings.CONSOLE_DEPLOY
        Deploy.objects.filter(status=0).update(status=1, start=timezone.now(), update=timezone.now())
        p = subprocess.Popen([script], stdout=subprocess.PIPE)
        out, err = p.communicate()
        Deploy.objects.filter(status=1).update(status=2, result='%s\r\n\r\n--- Error ---\r\n%s'%(out, err), update=timezone.now())
        [ deploy.delete() for deploy in Deploy.objects.all().order_by('-timestamp')[19:] ]
