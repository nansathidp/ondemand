import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from mailer.models import Send, Template

if __name__ == '__main__':
    template_list = Template.objects.all()
    for template in template_list:
        template.count_send = Send.objects.filter(template=template, is_send=True).count()
        template.save(update_fields=['count_send'])
