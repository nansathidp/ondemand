import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from order.models import Item

if __name__ == '__main__':
    for item in Item.objects.all():
        print 'Item: %s '%item.id,
        try:
            obj = item.content_type.get_object_for_this_type(id=item.content)
        except:
            print ' !!! Fail.'
            continue
        item.credit = obj.credit
        item.expired = obj.expired
        print '[credit= %s, expired= %s]'%(item.credit, item.expired)
        try:
            progress_course = item.course_set.all()[0]
            item.start = progress_course.start
            item.total_duration = progress_course.total_duration
            item.use_credit = progress_course.credit
        except:
            progress_course = None
        item.save()
        
