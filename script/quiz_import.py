import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from quiz.models import Faculty, Code, Result

if __name__ == '__main__':
    BASE = os.path.dirname(os.path.abspath(__file__))
    f = open(BASE+'/result.csv')
    for line in f.readlines():
        p = line.split(',')
        try:
            code = int(p[0])
            code_list = Code.objects.filter(code=code)
            if code_list.count() == 0:
                code = Code(code=code)
                code.save()
            else:
                code = code_list[0]
        except:
            code = None

        if code is not None:
            print '%s :>'%code.code,
            for f in p[1:]:
                f = f.strip()
                if len(f) > 0:
                    print f, 
                    faculty_list = Faculty.objects.filter(name=f)
                    if faculty_list.count() == 0:
                        faculty = Faculty(name=f)
                        faculty.save()
                    else:
                        faculty = faculty_list[0]
                    result_list = Result.objects.filter(code=code, faculty=faculty)
                    if result_list.count() == 0:
                        result = Result(code=code,
                                        faculty=faculty)
                        result.save()
            print '<'
