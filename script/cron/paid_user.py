import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.utils import timezone
from django.conf import settings

from institute.models import Institute, Course as InstituteCourse
from order.models import Item
from dashboard.models import PaidUser

from conicle.script.cron.dashboard_report_day import get_course_map, get_package_map
import datetime

def cal_paid_user(date_start, date_end):
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_package = settings.CONTENT_TYPE('package', 'package')
    PaidUser.objects.filter(date=date_start.date()).delete()
    for item in Item.objects.filter(order__status=3,
                                    order__is_test=False,
                                    order__net__gt=0.0,
                                    order__timecomplete__range=(date_start, date_end)):
        if item.content_type_id == content_type_course.id:
            institute_list = get_course_map(item.content)
            for institute in institute_list:
                if not PaidUser.objects.filter(institute=institute, account_id=item.account_id).exists():
                    PaidUser.objects.create(institute=institute,
                                            date=date_start.date(),
                                            account_id=item.account_id)
        elif item.content_type_id == content_type_package.id:
            institute_list = get_package_map(item.content)
            for institute in institute_list:
                if not PaidUser.objects.filter(institute=institute, account_id=item.account_id).exists():
                    PaidUser.objects.create(institute=institute,
                                            date=date_start.date(),
                                            account_id=item.account_id)
        else: #Coin
            pass
    
if __name__ == '__main__':
    if len(sys.argv) > 1:
        try:
            date = datetime.datetime.strptime(sys.argv[1], '%Y-%m-%d')
            date_start = timezone.now().replace(year=date.year, month=date.month, day=date.day, hour=0, minute=0, second=0, microsecond=0)
            date_end = date_start + datetime.timedelta(days=1)
        except:
            print 'Date Format YYYY-MM-DD'
            exit()
    else:
        date_start = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
        date_end = date_start + datetime.timedelta(days=1)
    #print date_start, date_end
    cal_paid_user(date_start, date_end)
