import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.db.models import Sum
from django.utils import timezone
from django.conf import settings

from dashboard.models import ReportDay
from institute.models import Institute, Course as InstituteCourse, Package as InstitutePackage, Coin as InstituteCoin
from order.models import Item
from package.models import Item as PackageItem
from analytic.models import Log as StatLog

import datetime

INSTITUTE_COURSE_MAP = {}
INSTITUTE_PACKAGE_MAP = {}
INSTITUTE_COIN_MAP = {}
content_type_course = settings.CONTENT_TYPE('course', 'course')
content_type_package = settings.CONTENT_TYPE('package', 'package')
content_type_coin = settings.CONTENT_TYPE('coin', 'coin')

def get_course_map(course_id):
    if course_id in INSTITUTE_COURSE_MAP:
        return INSTITUTE_COURSE_MAP[course_id]
    else:
        institute_list = []
        for institute in Institute.objects.filter(tutor__course__id=course_id):
            institute_list.append(institute)
        INSTITUTE_COURSE_MAP[course_id] = institute_list
        return institute_list

def get_package_map(package_id):
    if package_id in INSTITUTE_PACKAGE_MAP:
        return INSTITUTE_PACKAGE_MAP[package_id]
    else:
        institute_list = []
        for package_item in PackageItem.objects.filter(content_type=content_type_course, package_id=package_id):
            course_institute_list = get_course_map(package_item.content)
            institute_list = course_institute_list
            break  # Assume Course in Package same Institute
        INSTITUTE_PACKAGE_MAP[package_id] = institute_list
        return institute_list

def get_coin_map(coin_id):
    if coin_id in INSTITUTE_COIN_MAP:
        return INSTITUTE_COIN_MAP[coin_id]
    else:
        institute_list = []
        for institute in Institute.objects.filter(institute_coin_institite_set__coin_id=coin_id):
            institute_list.append(institute)
        INSTITUTE_COIN_MAP[coin_id] = institute_list
        return institute_list

def cal_report_day_type1(institute_list, date_start, date_end):
    result_list = {}
    for institute in institute_list:
        result_list[institute.id] = 0
    for item in Item.objects.filter(content_type=content_type_course,
                                    timestamp__range=(date_start, date_end),
                                    is_free=True):
        course_institute_list = get_course_map(item.content)
        for course_institute in course_institute_list:
            result_list[course_institute.id] += 1
    for institute in institute_list:
        ReportDay.push(institute, date_start.date(), 1, result_list[institute.id])

def cal_report_day_type2(institute_list, date_start, date_end):
    result_list = {}
    for institute in institute_list:
        result_list[institute.id] = 0
    for item in Item.objects.filter(content_type=content_type_course,
                                    order__status=3,
                                    order__is_test=False,
                                    order__timecomplete__range=(date_start, date_end),
                                    is_free=False):
        course_institute_list = get_course_map(item.content)
        for course_institute in course_institute_list:
            result_list[course_institute.id] += item.get_net_price()
    for institute in institute_list:
        ReportDay.push(institute, date_start.date(), 2, result_list[institute.id])

def cal_report_day_type3(institute_list, date_start, date_end):
    result_list = {}
    for institute in institute_list:
        result_list[institute.id] = 0
    for item in Item.objects.filter(content_type=content_type_package,
                                    order__is_test=False,
                                    order__timecomplete__range=(date_start, date_end),
                                    is_free=False):
        package_institute_list = get_package_map(item.content)
        for package_institute in package_institute_list:
            result_list[package_institute.id] += item.get_net_price()
    for institute in institute_list:
        ReportDay.push(institute, date_start.date(), 3, result_list[institute.id])

def cal_report_day_type4(institute_list, date_start, date_end):
    result_list = {}
    for institute in institute_list:
        result_list[institute.id] = 0
    for item in Item.objects.filter(content_type=content_type_coin,
                                    order__is_test=False,
                                    order__timecomplete__range=(date_start, date_end),
                                    is_free=False):
        package_institute_list = get_coin_map(item.content)
        for package_institute in package_institute_list:
            result_list[package_institute.id] += item.get_net_price()
    for institute in institute_list:
        ReportDay.push(institute, date_start.date(), 4, result_list[institute.id])

def cal_report_day_type0(institute_list, date):
    for institute in institute_list:
        value = 0.0
        try:
            value = float(ReportDay.objects.filter(date=date, type__in=[2, 3, 4], institute=institute).aggregate(Sum('value'))['value__sum'])
        except:
            pass
        ReportDay.push(institute, date, 0, value)

def cal_report_day_duration(institute_list, date_start, date_end):
    pass

def cal_report_day_type20(institute_list, date):
    for institute in institute_list:
        stat = 0
        for institute_course in InstituteCourse.objects.filter(institute=institute):
            stat_log = StatLog.objects.filter(stat__code='course_%s'%institute_course.course_id,
                                              date=date).first()
            if stat_log is not None:
                stat += stat_log.value
        ReportDay.push(institute, date, 20, stat)

def cal_report_day_type21(institute_list, date):
    for institute in institute_list:
        stat = 0
        for institute_course in InstituteCourse.objects.filter(institute=institute, is_free=False):
            stat_log = StatLog.objects.filter(stat__code='course_%s_cart'%institute_course.course_id,
                                              date=date).first()
            if stat_log is not None:
                stat += stat_log.value
        ReportDay.push(institute, date, 21, stat)

def cal_report_day_type22(institute_list, date):
    for institute in institute_list:
        stat = 0
        for institute_course in InstituteCourse.objects.filter(institute=institute, is_free=False):
            stat_log = StatLog.objects.filter(stat__code='course_%s_buy'%institute_course.course_id,
                                              date=date).first()
            if stat_log is not None:
                stat += stat_log.value
        ReportDay.push(institute, date, 22, stat)

def cal_report_day_type23(institute_list, date):
    for institute in institute_list:
        stat = 0
        for institute_course in InstituteCourse.objects.filter(institute=institute, is_free=True):
            stat_log = StatLog.objects.filter(stat__code='course_%s_libary'%institute_course.course_id,
                                              date=date).first()
            if stat_log is not None:
                stat += stat_log.value
        ReportDay.push(institute, date, 23, stat)

def cal_report_day_type30(institute_list, date):
    for institute in institute_list:
        stat = 0
        for institute_package in InstitutePackage.objects.filter(institute=institute):
            stat_log = StatLog.objects.filter(stat__code='package_%s'%institute_package.package_id,
                                              date=date).first()
            if stat_log is not None:
                stat += stat_log.value
        ReportDay.push(institute, date, 30, stat)

def cal_report_day_type31(institute_list, date):
    for institute in institute_list:
        stat = 0
        for institute_package in InstitutePackage.objects.filter(institute=institute):
            stat_log = StatLog.objects.filter(stat__code='package_%s_cart'%institute_package.package_id,
                                              date=date).first()
            if stat_log is not None:
                stat += stat_log.value
        ReportDay.push(institute, date, 31, stat)

def cal_report_day_type32(institute_list, date):
    for institute in institute_list:
        stat = 0
        for institute_package in InstitutePackage.objects.filter(institute=institute):
            stat_log = StatLog.objects.filter(stat__code='package_%s_buy'%institute_package.package_id,
                                              date=date).first()
            if stat_log is not None:
                stat += stat_log.value
        ReportDay.push(institute, date, 32, stat)
        
def cal_report_day(institute_list, date_start, date_end):
    cal_report_day_type1(institute_list, date_start, date_end)
    cal_report_day_type2(institute_list, date_start, date_end)
    cal_report_day_type3(institute_list, date_start, date_end)
    cal_report_day_type4(institute_list, date_start, date_end)
    cal_report_day_type0(institute_list, date_start.date())
    cal_report_day_duration(institute_list, date_start, date_end)

    cal_report_day_type20(institute_list, date_start.date())
    cal_report_day_type21(institute_list, date_start.date())
    cal_report_day_type22(institute_list, date_start.date())
    cal_report_day_type23(institute_list, date_start.date())

    cal_report_day_type30(institute_list, date_start.date())
    cal_report_day_type31(institute_list, date_start.date())
    cal_report_day_type32(institute_list, date_start.date())

if __name__ == '__main__':
    date_start = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
    date_end = date_start + datetime.timedelta(days=1)
    # print date_start, date_end
    # print date_start.date()
    institute_list = Institute.objects.all()
    cal_report_day(institute_list, date_start, date_end)
