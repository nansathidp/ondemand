import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.utils import timezone
from django.conf import settings

from institute.models import Institute, Course as InstituteCourse
from order.models import Item
from dashboard.models import Learner, ReportDay

from conicle.script.cron.dashboard_report_day import get_course_map, get_package_map
import datetime

def cal_learner(date_start, date_end):
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_package = settings.CONTENT_TYPE('package', 'package')
    Learner.objects.filter(date=date_start.date()).delete()
    for item in Item.objects.filter(order__status=3,
                                    order__is_test=False,
                                    #order__net__gt=0.0,
                                    order__timecomplete__range=(date_start, date_end)):
        if item.content_type_id == content_type_course.id:
            institute_list = get_course_map(item.content)
            for institute in institute_list:
                type = 1 if item.order.net == 0.0 else 2
                if not Learner.objects.filter(institute=institute, account_id=item.account_id, type=type).exists():
                    Learner.objects.create(institute=institute,
                                           date=date_start.date(),
                                           account_id=item.account_id,
                                           type=type)
        elif item.content_type_id == content_type_package.id:
            institute_list = get_package_map(item.content)
            for institute in institute_list:
                type = 1 if item.order.net == 0.0 else 2
                if not Learner.objects.filter(institute=institute, account_id=item.account_id, type=type).exists():
                    Learner.objects.create(institute=institute,
                                           date=date_start.date(),
                                           account_id=item.account_id,
                                           type=type)
        else: #Coin
            pass

    for institute in Institute.objects.all():
        ReportDay.push(institute, date_start.date(), 101, Learner.objects.filter(institute=institute, date=date_start.date(), type=1).count())
        ReportDay.push(institute, date_start.date(), 102, Learner.objects.filter(institute=institute, date=date_start.date(), type=2).count())
    
if __name__ == '__main__':
    if len(sys.argv) > 1:
        try:
            date = datetime.datetime.strptime(sys.argv[1], '%Y-%m-%d')
            date_start = timezone.now().replace(year=date.year, month=date.month, day=date.day, hour=0, minute=0, second=0, microsecond=0)
            date_end = date_start + datetime.timedelta(days=1)
        except:
            print 'Date Format YYYY-MM-DD'
            exit()
    else:
        date_start = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
        date_end = date_start + datetime.timedelta(days=1)
    #print date_start, date_end
    cal_learner(date_start, date_end)
