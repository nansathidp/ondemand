import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.db.models import F
from django.utils import timezone

from order.models import Item

if __name__ == '__main__':
    now = timezone.now()
    Item.objects.filter(is_free=False, status=2, start__isnull=False, expired__lt=now-F('start')).update(status=3)
