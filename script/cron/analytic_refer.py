import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.utils import timezone

from analytic.models import Stat, Log, Raw, Refer

import httpagentparser
import datetime, json

def cal(stat_id, date, refer):
    Refer.objects.filter(stat_id=stat_id, date=date).delete()
    result = {}
    for ref, user_agent in refer:
        #print ref, httpagentparser.simple_detect(user_agent)
        if ref in result:
            result[ref] += 1
        else:
            result[ref] = 1
    for ref, value in result.items():
        Refer.objects.create(stat_id=stat_id,
                             refer=ref,
                             value=value,
                             date=date)
        
if __name__ == '__main__':
    if len(sys.argv) > 1:
        try:
            date = datetime.datetime.strptime(sys.argv[1], '%Y-%m-%d').date()
        except:
            print 'Date Format YYYY-MM-DD'
            exit()
    else:
        now = timezone.now()
        date = now.date() - datetime.timedelta(days=1)
    raw_list = Raw.objects.filter(date=date).order_by('stat')
    stat_id = None
    for raw in raw_list:
        if stat_id is None:
            stat_id = raw.stat_id
            refer = json.loads(raw.refer)
        elif stat_id != raw.stat_id:
            cal(stat_id, date, refer)
            stat_id = raw.stat_id
            refer = json.loads(raw.refer)
        else:
            refer += json.loads(raw.refer)
    cal(stat_id, date, refer)
