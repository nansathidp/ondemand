import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from order.models import Order
from order.views_status import paysbuy_success

from django.utils import timezone
import datetime

if __name__ == '__main__':
    method_list = [1, 2, 3, 5]
    for order in Order.objects.filter(status=2,
                                      method__in=method_list,
                                      timestamp__gt=timezone.now()-datetime.timedelta(days=4)):
        print(order.id)