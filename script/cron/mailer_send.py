import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from mailer.models import Send

import time

LIMIT = 3000


def send_email():
    count = 0
    send_list = Send.objects.filter(is_send=False)[0:LIMIT]
    send_count = send_list.count()
    for send in send_list:
        try:
            count += 1
            print ('%s of %s Sending to --> %s' % (count, send_count, send.account_email))
            success = send.send_appengine()
            if success is False:
                return False
        except:
            print ('%s of %s Except' % (count, send_count))
    return True if count >= LIMIT else False


if __name__ == '__main__':
    while True:
        next = send_email()
        if next:
            time.sleep(10)
        else:
            break
