import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.utils import timezone

from order.models import Order
import datetime

if __name__ == '__main__':
    Order.objects.filter(status=1, timestamp__lt=timezone.now()-datetime.timedelta(days=4)).delete()