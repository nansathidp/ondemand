import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.db.models import Sum
from django.conf import settings

from order.models import Item
from progress.models import Course as ProgressCourse
from course.models import Course, Video

from package.cached import cached_package
from course.cached import cached_course_video_sum_duration, \
    cached_course_video_sum_duration_by_package, cached_package_item_id_list
import datetime


def course_duration(course):
    result = Video.objects.filter(course=course).aggregate(Sum('duration'))['duration__sum']
    if result is None:
        result = datetime.timedelta(0)
    return result


def package_duration(package, content_type_course):
    course_id_list = [item['content'] for item in
                      package.item_set.filter(content_type=content_type_course).values('content')]
    result = Video.objects.filter(course__id__in=course_id_list).aggregate(Sum('duration'))['duration__sum']
    if result is None:
        result = datetime.timedelta(0)
    return result


if __name__ == '__main__':
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_package = settings.CONTENT_TYPE('package', 'package')

    total_duration_result = {}
    total_duration_package_result = {}

    for item in Item.objects.filter(is_free=False, status=2):
        total_duration = None
        if item.content_type_id == content_type_course.id:
            if item.content in total_duration_result:
                total_duration = total_duration_result[item.content]
            else:
                course = Course.pull(item.content)
                # total_duration = cached_course_video_sum_duration(course)
                total_duration = course_duration(course)
                total_duration_result[item.content] = total_duration
        elif item.content_type_id == content_type_package.id:
            if item.content in total_duration_package_result:
                total_duration = total_duration_package_result[item.content]
            else:
                package = cached_package(item.content)
                # total_duration = cached_course_video_sum_duration_by_package(package)
                total_duration = package_duration(package, content_type_course)
                total_duration_package_result[item.content] = total_duration

        if total_duration != item.total_duration:
            item.total_duration = total_duration
            item.save(update_fields=['total_duration'])

    for progress_course in ProgressCourse.objects.filter(item__is_free=False, status=1):
        if progress_course.content_id in total_duration_result:
            total_duration = total_duration_result[progress_course.content_id]
        else:
            course = Course.pull(progress_course.content_id)
            # total_duration = cached_course_video_sum_duration(course)
            total_duration = course_duration(course)
            total_duration_result[progress_course.content_id] = total_duration

        if progress_course.total_duration != total_duration:
            progress_course.total_duration = total_duration
            progress_course.save(update_fields=['total_duration'])
