import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.utils import timezone
from django.conf import settings

from institute.models import Institute, Course as InstituteCourse, Package as InstitutePackage
from analytic.models import Log as StatLog
from dashboard.models import TopView

import datetime

def process_course(institute, date, content_type):
    for institute_course in InstituteCourse.objects.filter(institute=institute):
        stat_log = StatLog.objects.filter(stat__code='course_%s'%institute_course.course_id,
                                          date=date).first()
        if stat_log is not None:
            TopView.objects.create(institute=institute,
                                   content_type=content_type,
                                   content=institute_course.course_id,
                                   date=date,
                                   value=stat_log.value)

def process_package(institute, date, content_type):
    for institute_package in InstitutePackage.objects.filter(institute=institute):
        stat_log = StatLog.objects.filter(stat__code='package_%s'%institute_package.package_id,
                                          date=date).first()
        if stat_log is not None:
            TopView.objects.create(institute=institute,
                                   content_type=content_type,
                                   content=institute_package.package_id,
                                   date=date,
                                   value=stat_log.value)

def cal_top_view(date):
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_package = settings.CONTENT_TYPE('package', 'package')
    TopView.objects.filter(date=date).delete()
    for institute in Institute.objects.all():
        process_course(institute, date, content_type_course)
        process_package(institute, date, content_type_package)
        
if __name__ == '__main__':
    if len(sys.argv) > 1:
        try:
            date = datetime.datetime.strptime(sys.argv[1], '%Y-%m-%d').date()
        except:
            print 'Date Format YYYY-MM-DD'
            exit()
    else:
        date = timezone.now().date()
    cal_top_view(date)
