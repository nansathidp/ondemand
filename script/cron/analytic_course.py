import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.utils import timezone
from django.conf import settings

from institute.models import Course as InstituteCourse
from order.models import Item
from analytic.models import Stat, Log

import httpagentparser
import datetime, json

def cal(institute_course, date_start, date_end):
    course_id = institute_course.course_id
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    if institute_course.is_free:
        #Libary
        count = Item.objects.filter(content_type_id=content_type_course.id,
                                    content=course_id,
                                    order__is_test=False,
                                    timestamp__range=(date_start, date_end)).count()
        code = 'course_%s_libary'%course_id
        Stat.push_value(code, date_start.date(), count, is_cron=True)
    else:
        #Cart
        count = Item.objects.filter(content_type_id=content_type_course.id,
                                    content=course_id,
                                    order__is_test=False,
                                    timestamp__range=(date_start, date_end)).count()
        #if count > 0:
        code = 'course_%s_cart'%course_id
        Stat.push_value(code, date_start.date(), count, is_cron=True)

        #Buy
        count = Item.objects.filter(content_type_id=content_type_course.id,
                                    content=course_id,
                                    order__status=3,
                                    order__is_test=False,
                                    order__timecomplete__range=(date_start, date_end)).count()
        #if count > 0:
        code = 'course_%s_buy'%course_id
        Stat.push_value(code, date_start.date(), count, is_cron=True)
    
if __name__ == '__main__':
    if len(sys.argv) > 1:
        try:
            date = datetime.datetime.strptime(sys.argv[1], '%Y-%m-%d')
            date_start = timezone.now().replace(year=date.year, month=date.month, day=date.day, hour=0, minute=0, second=0, microsecond=0)
            date_end = date_start + datetime.timedelta(days=1)
        except:
            print 'Date Format YYYY-MM-DD'
            exit()
    else:
        date_start = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
        date_end = date_start + datetime.timedelta(days=1)
    #print date_start, date_end
    for institute_course in InstituteCourse.objects.all():
        cal(institute_course, date_start, date_end)
        
