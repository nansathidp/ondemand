import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings

from mailer.models import Template, Send
from account.models import Account

import uuid, time

LIMIT = 5000


def create_email():
    template = Template.objects.filter(status__in=[2, 3]).first()
    if template is not None:
        count = 0
        send_list = []
        for account in Account.objects.all().order_by('id')[
                       template.create_limit_start:template.create_limit_start + LIMIT]:
            count += 1
            if account.is_valid_email() and account.pair_id == -1:
                send_list.append(Send(template=template,
                                      account_id=account.id,
                                      account_email=account.email,
                                      code=str(uuid.uuid4()).replace('-', ''),
                                      is_read=False,
                                      is_send=False))
        if len(send_list) > 0:
            Send.objects.bulk_create(send_list)
        template.status = 3 if count >= LIMIT else 5
        template.create_limit_start += count
        template.save(update_fields=['status', 'create_limit_start'])
    return template


if __name__ == '__main__':
    while True:
        template = create_email()
        if template is None:
            break
        elif template.status == 3:
            time.sleep(10)
        else:
            break
