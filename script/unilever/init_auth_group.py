import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
print(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.contrib.auth.models import Group, Permission

if __name__ == '__main__':
    #Init Super Admin
    group = Group.objects.filter(name='Super Admin').first()
    if group is None:
        group = Group.objects.create(name='Super Admin')

    group.permissions.set(Permission.objects.all())
    print(group.permissions.count())
