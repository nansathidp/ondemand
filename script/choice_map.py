import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "aisadmission.settings")
django.setup()

from quiz.models import Choice

if __name__ == '__main__':
    for choice in Choice.objects.all():
       if 0 < choice.type < 10:
           print '%s : %s to '%(choice.id, choice.get_type_display()),
           if choice.type == 1:
               choice.type = 60
           elif choice.type == 2:
               choice.type = 30
           elif choice.type == 3:
               choice.type = 50
           elif choice.type == 4:
               choice.type = 20
           elif choice.type == 5:
               choice.type = 40
           elif choice.type == 6:
               choice.type = 10
           print choice.get_type_display()
           choice.save()
