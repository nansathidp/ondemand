import django
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from dateutil import tz
from scheduler.models import RepeatableJob
import datetime


def repeatable(name, callable, enabled, queue, job_id, scheduled_time, interval, interval_unit):
    RepeatableJob.objects.create(
        name=name,
        callable=callable,
        enabled=enabled,
        queue=queue,
        job_id=job_id,
        scheduled_time=scheduled_time,
        interval=interval,
        interval_unit=interval_unit
    )


if __name__ == '__main__':
    tz_local = tz.tzlocal()
    names = ['auto_age_password']
    for name in names:
        name_ = RepeatableJob.objects.filter(name=name)
        if not name_:
            if name == 'auto_age_password':
                RepeatableJob.objects.create(
                    name='auto_age_password',
                    callable="account.jobs._disabled_account_age",
                    enabled=True,
                    queue="default",
                    job_id="dc2b15c4-1ef4-44ec-99d7-bb4f156ddeb5",
                    scheduled_time=datetime.datetime.strptime("2017-07-12 00:00:00", "%Y-%m-%d %H:%M:%S").replace(
                        tzinfo=tz_local),
                    interval=1,
                    interval_unit="days"
                )
