__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from django.contrib.contenttypes.models import ContentType
from course.models import Course
from stream.models import Stream, Config
if __name__ == '__main__':
    # Bio
    course_list = [1434015936002812]
    # Chem
    course_list += [1433755674002356, 1433753744002297, 1433754347002318, 1433754693002339, 1433755674002356, 1433757371002373, 1433757938002387, 1433758106002396, 1433758245002405, 1433758452002417, 1433760016002431, 1433760295002449, 1433760519002459, 1433760717002473, 1433760871002484]
    # PAT
    course_list += [1434447467002511]
    print course_list
    config = Config.objects.filter(is_default=True).first()
    content_type = ContentType.objects.get(app_label='course', model='course')
    for course_id in course_list:
        course = Course.objects.get(id=course_id)
        for video in course.video_set.filter(type=0, status=1):
            if len(video.path) > 0:
                if Stream.objects.filter(config=config, video=video).count() == 0:
                    stream = Stream(config=config, video=video, media=video.path)
                    stream.save()
                    print video.id


