__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from order.models import Item
from stream.models import Config,Allow
if __name__ == '__main__':
    # Bio
    content_list = [1434015936002812]
    # Chem
    content_list += [1433755674002356, 1433753744002297, 1433754347002318, 1433754693002339, 1433755674002356, 1433757371002373, 1433757938002387, 1433758106002396, 1433758245002405, 1433758452002417, 1433760016002431, 1433760295002449, 1433760519002459, 1433760717002473, 1433760871002484]
    # PAT
    content_list += [1434447467002511]
    # Package
    content_list += []
    account_list = []
    config = Config.objects.filter(is_default=True).first()
    for item in Item.objects.filter(content__in=content_list,order__status=3):
        if item.order.account not in account_list:
            account_list.append(item.order.account)
    account_allow_list = [allow.account for allow in Allow.objects.all()]

    for account in account_list:
        if account not in account_allow_list:
            Allow.objects.create(config=config, account=account,is_default=True)
            print account.id




