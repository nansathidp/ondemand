# -*- encoding: utf-8 -*-
__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ondemand.settings')
django.setup()
import os
from account.models import Account
from department.models import Department

from category.models import Category
from tutor.models import Tutor
from provider.models import Provider

from course.models import Course
from program.models import Program
from question.models import Activity
from task.models import Task
from rating.models import Rating, Log
from discussion.models import Topic, Role, Message
from notification.models import Message

from featured.models import  Highlight, Banner, Featured

from assignment.models import Assignment
from order.models import Order

from dependency.models import Dependency

from alert.models import Alert
from inbox.models import Inbox
from mailer.models import Mailer

from term.models import Term
from log.models import Log

from content.models import Location

from organization.models import Child, Parent

from progress.models import Account as ProgressAccount, Content, AccountContent

from django.core.cache import cache
from memcached_stats import MemcachedStats


def _cached_reset():
    mem = MemcachedStats()
    for cached in mem.key_details():
        cached_key = ':'.join(cached[0].split(':')[2:])
        cache.delete(cached_key)

if __name__ == '__main__':

    print('Remove :', 'None Conicle Email')
    Account.objects.filter(email__icontains='cimb').delete()

    print('Remove :', 'Activity')
    Activity.objects.all().delete()

    print('Remove :', 'Program')
    Program.objects.all().delete()

    print('Remove :', 'Task')
    Task.objects.all().delete()

    print('Remove :', 'Course')
    Course.objects.all().delete()

    print('Remove :', 'Department')
    Department.objects.all().delete()

    print('Remove :', 'Category')
    Category.objects.all().delete()

    print('Remove :', 'Tutor')
    Tutor.objects.all().delete()

    print('Remove :', 'Provider')
    Provider.objects.all().delete()

    print('Remove :', 'Highlight')
    Highlight.objects.all().delete()
    Banner.objects.all().delete()
    Featured.objects.all().delete()

    print('Remove :', 'Notification')
    Message.objects.all().delete()

    print('Remove :', 'Review')
    Rating.objects.all().delete()
    Log.objects.all().delete()

    print('Remove :', 'Assignment & Order')
    Assignment.objects.all().delete()
    Order.objects.all().delete()

    print('Remove :', 'Alert')
    Alert.objects.all().delete()

    print('Remove :', 'Organization')
    Child.objects.all().delete()
    Parent.objects.all().delete()

    print('Remove :', 'Progress')
    Location.objects.all().delete()
    ProgressAccount.objects.all().delete()
    Content.objects.all().delete()
    AccountContent.objects.all().delete()

    print('Remove :', 'Other')
    Alert.objects.all().delete()
    Dependency.objects.all().delete()
    Inbox.objects.all().delete()
    Term.objects.all().delete()
    Log.objects.all().delete()
    Mailer.objects.all().delete()
    Message.objects.all().delete()
    Role.objects.all().delete()
    Topic.objects.all().delete()

    _cached_reset()
