# -*- encoding: utf-8 -*-
__author__ = 'din'
import django
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ondemand.settings')
django.setup()
from account.models import Account
from department.models import Department

from django.core.cache import cache
from memcached_stats import MemcachedStats


def _cached_reset():
    mem = MemcachedStats()
    for cached in mem.key_details():
        cached_key = ':'.join(cached[0].split(':')[2:])
        cache.delete(cached_key)

if __name__ == '__main__':

    print('Remove :', 'None Conicle Email')
    Account.objects.filter(email__icontains='cimb').delete()

    print('Remove :', 'Department')
    Department.objects.all().delete()

    _cached_reset()
