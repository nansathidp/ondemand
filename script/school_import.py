import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from school.models import School
from django.conf import settings

if __name__ == '__main__':
    BASE = os.path.dirname(os.path.abspath(__file__))
    f = open(BASE+'/school.csv')
    count = 0
    school_bulk = []
    for line in f.readlines():
        p = line.split(',')
        name = p[0].rstrip().strip()
        if len(name) > 3:
            school_list = School.objects.filter(name=name)
            if school_list.count() == 0:
                school = School(name=name)
            else:
                school = school_list[0]
            school.subdistric = p[6].rstrip().strip()
            school.district = p[7].rstrip().strip()
            school.province = p[8].rstrip().strip()
            school.zipcode = p[9].rstrip().strip()
            school.is_display = True
            try:
                school.save()
                print '%s -> %d'%(name,count)
            except:
                print '+ '
                for _ in p:
                    print _,
                print ''
                print "++%s++"%p[9].rstrip().strip()
                raise

        count += 1
    print "Completed"
