import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.contrib.contenttypes.models import ContentType
from course.models import Course
from purchase.models import PriceMatrix,Apple
from django.conf import settings
import math

if __name__ == '__main__':
    content_type = ContentType.objects.get(app_label='course', model='course')
    price_apple_list = []
    for course in Course.objects.all():
        try:
            Apple.objects.get(content_type=content_type, content=course.id)
        except :
            try:
                price = course.get_price(store=1)
                if price != None:
                    tier = PriceMatrix.objects.get(tier=int(math.ceil(price.price/34.0)))
                    if Apple.objects.filter(name=course.name).count() == 0:
                        price_apple = Apple(name=course.name,
                                            content_type=content_type,
                                            content=course.id,
                                            type=4,
                                            price_tier=tier)
                    price_apple_list.append(price_apple)
                    print course.id
                else:
                    print 'None'
            except:
                print "Fail : %s"%(course.id)
    Apple.objects.bulk_create(price_apple_list)
