import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from provider.models import Provider, Branch as ProviderBranch, Account as ProviderAccount
from institute.models import Institute, Branch as InstituteBranch,  Account as InstituteAccount
from tutor.models import Tutor


def _push_provider(institute):
    provider = Provider.objects.filter(migrate_institute=institute.id).first()
    if provider is None:
        print('\_ Create.')
        provider = Provider(migrate_institute=institute.id)
    else:
        print('\_ Update.')
        
    provider.name = institute.name
    provider.slug = institute.slug
    provider.theme = institute.theme
    
    provider.image = institute.image
    provider.image_featured = institute.image_featured
    provider.image_cover = institute.image_cover
    provider.image_logo = institute.image_logo

    provider.desc = institute.desc
    provider.keywords = institute.keywords

    provider.sort = institute.sort
    provider.is_display = institute.is_display

    provider.save()
    return provider


def _push_branch(provider):
    for institute_branch in InstituteBranch.objects.filter(institute_id=provider.migrate_institute):
        provider_branch = ProviderBranch.objects.filter(provider__migrate_institute=provider.migrate_institute,
                                                        name=institute_branch.name).first()
        if provider_branch is None:
            print(' |_ Branch: Create >> ', institute_branch.name)
            ProviderBranch.objects.create(provider=provider,
                                          name=institute_branch.name,
                                          sort=institute_branch.sort)
        else:
            print(' |_ Branch: Found. >> ', institute_branch.name)


def _push_account(provider):
    for institute_account in InstituteAccount.objects.filter(institute_id=provider.migrate_institute):
        provider_accout = ProviderAccount.objects.filter(provider__migrate_institute=provider.migrate_institute,
                                                         account=institute_account.account).first()
        if provider_accout is None:
            print(' \_ Account: Create >> ', institute_account.account_id)
            provider_accout = ProviderAccount.objects.create(provider=provider,
                                                             account_id=institute_account.account_id,
                                                             is_superuser=institute_account.is_superuser)
            for institute_branch in provider_accout.branchs.all():
                provider_accout.branchs.add(institute_branch)
        else:
            print(' \_ Account: Found. >> ', institute_account.account_id)


def _push_tutor(tutor):
    for institute in tutor.institute.all():
        print(' \_ Institute: ', institute.id, ' >> ', institute.name)
        if tutor.providers.filter(migrate_institute=institute.id).exists():
            print('  |_ Found.')
        else:
            print('  |_ Add.')
            provider = Provider.objects.filter(migrate_institute=institute.id).first()
            tutor.providers.add(provider)

            
if __name__ == '__main__':
    print('Migrate: institute.Institute')
    for institute in Institute.objects.all():
        print(institute.id, ': ', institute.name)
        provider = _push_provider(institute)
        _push_branch(provider)
        _push_account(provider)

    print('Migrate: Tutor.institute -> Tutor.providers')
    for tutor in Tutor.objects.all():
        print(tutor.id, ': ', tutor.name)
        _push_tutor(tutor)
