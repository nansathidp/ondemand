import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings

from os import listdir
from os.path import isfile
from question.models import Section

if __name__ == '__main__':
    path = os.path.join(settings.BASE_DIR, 'upload')
    for file in listdir(path):
        if isfile(os.path.join(path,file)):
            print('-> ', file, end='\t\t')
            try:
                _ = file.split('.')[0]
                if Section.objects.filter(data=_).exists():
                    print(' <- Use')
                else:
                    print(' !! Delete')
                    os.remove(os.path.join(path,file))
            except:
                print(' <- error')
