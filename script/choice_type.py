import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "aisadmission.settings")
django.setup()

from quiz.models import Choice, ChoiceType

if __name__ == '__main__':
    for choice in Choice.objects.all():
        if choice.type > 0 and choice.choice_type is None:
            if ChoiceType.objects.filter(number=choice.type).count() == 0:
                choice_type = ChoiceType(number=choice.type,
                                         name=choice.get_type_display())
                choice_type.save()
            else:
                choice_type = ChoiceType.objects.get(number=choice.type)
            choice.choice_type = choice_type
            choice.save()
