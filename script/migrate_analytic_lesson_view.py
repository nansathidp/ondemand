import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

#from analytic.models import Log, LessonViewLog

from analytic.cached import cached_analytic_stat, cached_analytic_stat_update
import time

if __name__ == '__main__':
    print 'Not Use'
    exit()
    for lesson_view_log in LessonViewLog.objects.all():
        code = 'lesson_%s'%lesson_view_log.lesson_view.lesson_id
        print code, lesson_view_log.date
        stat_result = cached_analytic_stat(code)
        stat = stat_result['stat']
        stat.value += lesson_view_log.stat
        stat.save(update_fields=['value', 'last_update'])
        cached_analytic_stat_update(code, stat_result)
        Log.push(stat, lesson_view_log.date, lesson_view_log.stat)
        lesson_view_log.delete()
        #time.sleep(0.1)
