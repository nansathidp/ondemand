import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.utils import timezone

from institute.models import Institute

from conicle.script.cron.top_view import cal_top_view
import datetime

if __name__ == '__main__':
    #date = timezone.now().replace(year=2015, month=5, day=1, hour=0, minute=0, second=0, microsecond=0).date()
    date = timezone.now().replace(year=2015, month=11, day=1, hour=0, minute=0, second=0, microsecond=0).date()
    while date < timezone.now().date():
        print 'Cal : %s'%date
        cal_top_view(date)
        date += datetime.timedelta(days=1)
