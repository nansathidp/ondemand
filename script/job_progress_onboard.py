from program.models import Program
from django.conf import settings
from progress.models import Progress

def run():
    _program = settings.CONTENT_TYPE('program', 'program')
    _onboard = settings.CONTENT_TYPE('program', 'onboard')
    for progress in Progress.objects.filter(content_type=_program):
        program = Program.pull(progress.content)
        if program.type == 2:
            progress.content_type = _onboard
            progress.save(update_fields=['content_type'])
            item = progress.item
            item.content_type = _onboard
            item.save(update_fields=['content_type'])
