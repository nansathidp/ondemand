import os, sys, django , os.path
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from django.contrib.contenttypes.models import ContentType
from django.core.files import File
from course.models import Course,Price
from purchase.models import PriceMatrix,Apple
import shutil
import math
from django.conf import settings
import random
import Image
if __name__ == '__main__':
  content_type = ContentType.objects.get(app_label='course', model='course')
  dstsource = '/home/din/conicle/conicle'
  dstroot = '/home/din/datasheet'
  for course in Course.objects.filter(is_display=True):
    try:
      apple = Apple.objects.get(content_type=content_type, content=course.id)
      source = course.image.path
      course.image.name = 'course/image/'+'com.mimoTech.uacademy.course.'+str(course.id)
      target = settings.MEDIA_ROOT + course.image.name
      os.rename(source,target)
      course.save()
    except :
      print 'Fail'
