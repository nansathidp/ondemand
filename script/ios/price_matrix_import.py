import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()


from purchase.models import PriceMatrix

if __name__ == '__main__':
    BASE = os.path.dirname(os.path.abspath(__file__))
    f = open(BASE+'/pricing_matrix.csv')
    count = 0
    for line in f.readlines():
        p = line.split(',')
        tier = p[0].rstrip().strip()
        tier_list = PriceMatrix.objects.filter(tier=tier)
        if tier_list.count() == 0:
            matrix = PriceMatrix(tier=tier)
        else:
            matrix = tier_list[0]
        print('Import Tier : %s' % matrix.tier)
        matrix.price_usd = p[1].rstrip().strip()
        matrix.price_thb = p[2].rstrip().strip()
        try:
            matrix.save()
        except:
            raise
    print('-> Completed')
