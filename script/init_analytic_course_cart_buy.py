import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.utils import timezone

from institute.models import Course as InstituteCourse

from conicle.script.cron.analytic_course import cal
import datetime, time

if __name__ == '__main__':
    date_start = timezone.now().replace(year=2015, month=5, day=1, hour=0, minute=0, second=0, microsecond=0)
    #date_start = timezone.now().replace(year=2015, month=10, day=20, hour=0, minute=0, second=0, microsecond=0)
    while date_start.date() <= timezone.now().date() - datetime.timedelta(days=1):
        date_end  = date_start + datetime.timedelta(days=1)
        time.sleep(2)
        print 'Cal : %s'%date_start.date()
        for institute_course in InstituteCourse.objects.filter(course__timestamp__lte=date_start):
            #if 1444125742002197 != institute_course.course_id:
            #    continue
            #print institute_course.course.timestamp, date_start
            print ' >> %s'%institute_course.course_id
            cal(institute_course, date_start, date_end)
            time.sleep(0.2)
        date_start += datetime.timedelta(days=1)
