import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.contrib.contenttypes.models import ContentType
from course.models import Course
from stream.models import Stream,Config

if __name__ == '__main__':

    course_list = [1436509646002854,1436510381002964,1436510519002966,1441860055002839]
    # Eng
    # course_list += [1429266768002389, 1429266824002391, 1429266894002393, 1429268222002408, 1429268299002410, 1429272005002468, 1429272237002472, 1429272436002477, 1429272634002480, 1429281124002583, 1429282267002610, 1429282307002613, 1429282393002615, 1429282572002619, 1429282638002621, 1429282682002623, 1429282730002625, 1429282800002627, 1429282839002630, 1429282886002633, 1429282939002635, 1429283016002637, 1429283061002639]

    config = Config.objects.all()[0]
    content_type = ContentType.objects.get(app_label='course', model='course')
    for course_id in course_list:
        course = Course.objects.get(id=course_id)
        for video in course.video_set.filter(type=0, status=1):
            if len(video.path) > 0:
                if Stream.objects.filter(config=config, video=video).count() == 0:
                    if "_720.mp4" in video.path.lower():
                        stream = Stream(config=config, video=video, media=video.path.lower())
                    else:
                        stream = Stream(config=config, video=video, media=video.path.lower().replace(".mp4", "_720.mp4"))
                    stream.save()
                    print video.id


