__author__ = 'din'
import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from order.models import Order

if __name__ == '__main__':
    for order in Order.objects.all():
        if order.item_set.all().count() == 0:
            print order.id

