import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings

from order.models import Item
from course.models import Course
from package.models import Package

if __name__ == '__main__':
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_package = settings.CONTENT_TYPE('package', 'package')
    for item in Item.objects.all():
        if item.content_type_id == content_type_course.id:
            try:
                Course.objects.get(id=item.content)
            except:
                print 'Course not found : %s' % item.content
                item.delete()

        elif item.content_type_id == content_type_package.id:
            try:
                Package.objects.get(id=item.content)
            except:
                print 'Package not found : %s' % item.content
                item.delete()
