import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from course.models import Course
from lesson.models import Lesson
from subject.models import Subject
from tutor.models import Tutor
from account.models import Account
from highlight.models import WebBanner

import requests

#SITE = 'https://cocodemy.com'
#SITE = 'https://clickforclever.com'
SITE = 'https://dev.aisondemand.conicle.com'

def _load(image):
    try:
        path = image.path
    except:
        print(' -> No Image')
        return
    if os.path.isfile(path):
        print(' -> Found.')
        return
    else:
        print(' -> Not Found.')

    url = image.url
    folder = '/'.join(path.split('/')[:-1])
    if not os.path.isdir(folder):
        os.makedirs(folder)

    url_load = '%s/media/%s'%(SITE, image)
    print(url_load)
    r = requests.get(url_load, stream=True)
    if r.status_code == 200:
        with open(path, 'wb') as f:
            for chunk in r.iter_content():
                f.write(chunk)

if __name__ == '__main__':
    print('Load from: %s'%SITE)
    #print('Check Domain and commit this line :P')

    #for subject in Subject.objects.all():
    #    print('Subject: ', subject.id, end='')
    #    _load(subject.image)
    #    _load(subject.image_cover)

    #exit()
    for course in Course.objects.all():
        print('Course: ', course.id, end='')
        _load(course.image)
        
    #exit()
    #for lesson in Lesson.objects.all():
    #    print('Lesson: ', lesson.id, end='')
    #    _load(lesson.image)
    #    _load(lesson.image_cover)
    #    _load(lesson.account.image)

    #for tutor in Tutor.objects.all():
    #    print('Tutor: ', tutor.id, end='')
    #    _load(tutor.image)

    #for web_banner in WebBanner.objects.all():
    #    print('WebBanner: ', web_banner.id, end='')
    #    _load(web_banner.image)
