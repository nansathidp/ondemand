import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from account.models import Account
import pickle, requests, json, time

#domain = 'http://127.0.0.1:8000'
domain = 'https://clickforclever.com'
url = '%s/api/v4/load/user/'%domain
n = 4000
hit = 0

def load():
    r = requests.get(url)
    try:
        result = json.loads(r.text)
        account = pickle.loads(result['result'])
    except:
        print 'Error....'
        return
    if not Account.objects.filter(id=account.id).exists():
        if not Account.objects.filter(email=account.email).exists():
            account.save()
            print '%s/%s Save.... %s, %s'%(hit, n, account.id, account.email)
            return True
        else:
            print '%s/%s Found... (email)'%(hit, n)
            return False
    else:
        print '%s/%s Found.... (id)'%(hit, n)
        return False

if __name__ == '__main__':
    #print 'Check Domain and comment exit() next line :P'
    #exit()
    while(True):
        if load():
            hit += 1
            time.sleep(0.02)
        #else:
        #    time.sleep(0.5)
        if hit >= n:
            break    
            
