import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from course.models import Video

if __name__ == '__main__':
    BASE = os.path.dirname(os.path.abspath(__file__))
    f = open(BASE+'/ClickForClever/video_migrate.csv')
    count = 0
    for line in f.readlines():
        p = line.split(',')
        id = p[0].rstrip().strip()
        path = p[1].rstrip().strip()
        try:
            video = Video.objects.get(id=id)
            video.path = path
            video.type = 0
            video.type_video = 3
            video.status = 1
            video.is_display = True
            video.save(update_fields=['path', 'type', 'type_video', 'status', 'is_display'])
            print '%s : Pass'%(id)
        except:
            print '%s : Fail Fail'%(id)