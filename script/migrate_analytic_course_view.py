import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

#from analytic.models import Log, CourseViewLog

from analytic.cached import cached_analytic_stat, cached_analytic_stat_update
import time

if __name__ == '__main__':
    print 'Not Use'
    exit()
    for course_view_log in CourseViewLog.objects.all():
        code = 'course_%s'%course_view_log.course_view.course_id
        print code, course_view_log.date
        stat_result = cached_analytic_stat(code)
        stat = stat_result['stat']
        stat.value += course_view_log.stat
        stat.save(update_fields=['value', 'last_update'])
        cached_analytic_stat_update(code, stat_result)
        Log.push(stat, course_view_log.date, course_view_log.stat)
        course_view_log.delete()
        time.sleep(0.1)
