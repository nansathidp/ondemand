import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from institute.models import Log 
from dashboard.models import Log as _Log

if __name__ == '__main__':
    for log in Log.objects.all():
        print(log.id)
        _Log.objects.create(institute_id=log.institute_id,
                            admin_id=log.admin_id,
                            admin_email=log.admin_email,
                            admin_display=log.admin_display,
                            account_id=log.account_id,
                            account_email=log.account_email,
                            account_display=log.account_display,
                            type=log.type,
                            note=log.note,
                            timestamp=log.timestamp)
