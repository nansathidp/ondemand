import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings

from notification.models import Config

code_list = [
    ('New Assignment', 'new_assignment'),
    ('50% of Period before Expired', '50_percent_befor_expired'),
    ('25% of Period before Expired', '25_percent_befor_expired'),
    ('1 day left befor Expired', '1_day_befor_expired'),
    ('Expired', 'expired'),
    ('Completed', 'completed')
]

def gen(content_type, start):
    sort = start+1
    for code in code_list:
        print(content_type, code)
        config = Config.objects.filter(content_type=content_type,
                                       code=code[1]).first()
        if config is None:
            Config.objects.create(content_type=content_type,
                                  name=code[0],
                                  code=code[1],
                                  sort=sort)
        else:
            config.name = code[0]
            config.sort = sort
            config.save()
        sort += 1
    
if __name__ == '__main__':
    content_type = settings.CONTENT_TYPE('course', 'course')
    gen(content_type, 0)
    content_type = settings.CONTENT_TYPE('question', 'activity')
    gen(content_type, 10)
    content_type = settings.CONTENT_TYPE('program', 'program')
    gen(content_type, 20)
