import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings

from account.models import Account
from order.models import Item

from progress.cached import cached_progress_account_content

if __name__ == '__main__':
    #for account in Account.objects.filter():
    for account in Account.objects.all():
        print('+', account.email)
        print('* progress account Count')
        account_content = cached_progress_account_content(account.id, None)
        account_content.update_count()
