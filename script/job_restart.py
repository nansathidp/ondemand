import os

def rq():
    os.system('supervisorctl restart rqworker-learndi > /app/django/learndi/media/p.log')

def ps():
    os.system('ps auxf > /app/django/learndi/media/p.log')

def df():
    os.system('df -H > /app/django/learndi/media/p.log')

def kill():
    os.system('kill -9 9685')
