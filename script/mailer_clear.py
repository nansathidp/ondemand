import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from mailer.models import Send

if __name__ == '__main__':
    for send in Send.objects.all():
        if '@facebook.com.gen' in send.account_email or '@gen' in send.account_email:
            send.delete()
            print 'Remove : %s' % send.account_email
