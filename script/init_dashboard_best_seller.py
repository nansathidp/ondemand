import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.utils import timezone

from institute.models import Institute

from conicle.script.cron.best_seller import cal_best_seller
import datetime

if __name__ == '__main__':
    date_start = timezone.now().replace(year=2015, month=5, day=1, hour=0, minute=0, second=0, microsecond=0)
    #date_start = timezone.now().replace(year=2015, month=11, day=1, hour=0, minute=0, second=0, microsecond=0)
    while date_start.date() <= timezone.now().date() - datetime.timedelta(days=1):
        date_end  = date_start + datetime.timedelta(days=1)
        print 'Cal : %s'%date_start.date()
        cal_best_seller(date_start, date_end)
        date_start += datetime.timedelta(days=1)
