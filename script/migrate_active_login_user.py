import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from account.models import Account
from active.models import LoginLog, LoginDepartmentAmount

if __name__ == '__main__':
    accounts = Account.objects.filter(first_active__isnull=False).order_by('first_active')
    for account in accounts:
        first_active_list = LoginLog.objects.filter(account=account.id).first()
        if not first_active_list:
            print(account.email)
            LoginLog.objects.create(account=account,
                                    first_active=account.first_active,
                                    last_active=account.last_active)
