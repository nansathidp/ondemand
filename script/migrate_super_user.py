import os
import django
import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.contrib.auth.models import User, Group, Permission
from account.models import Account

if __name__ == '__main__':
    admin_list = [
                 {'email': 'demo@conicle.com', 'uid': 'demo', 'password': 'demoConicle'},
                 {'email': 'admin@conicle.com', 'uid': 'admin', 'password': 'adminpassword'}
               ]
    # Check and Create Default Admin
    for admin in admin_list:
        account = Account.objects.filter(email=admin['email']).first()
        if not account:
            from django.contrib.auth import get_user_model
            User = get_user_model()
            user = User.objects.create_superuser(password=admin['password'], email=admin['email'])
            user.uid = admin['uid']
            user.save()
            print('Superuser has been Created')
        else:
            # Fix for CIMB Only
            account.is_enable = True
            account.is_first = True
            account.uid = admin['uid']
            account.save()
            print('Superuser and E-Mail is already to create')

    # Create Group permission
    group, is_created = Group.objects.get_or_create(name='Super Admin')
    permission_list = Permission.objects.all()
    group.permissions.set(permission_list)

    # Push user
    user_list = Account.objects.filter(is_superuser=True)
    user_list.update(is_enable=True)
    for user in user_list:
        print('Grant permissions : %s' % user.email)
        user.groups.add(group)
