# -*- encoding: utf-8 -*-

import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings
from django.utils.timezone import utc
from django.utils import timezone
from django.db import connection
from django.db.models import Q

from app.models import App
from account.models import Account, App as AccountApp, Address
import random, string, datetime
import json

PROVINCE = {
    1: "กรุงเทพมหานคร",
    2: "สมุทรปราการ",
    3: "นนทบุรี",
    4: "ปทุมธานี",
    5: "พระนครศรีอยุธยา",
    6: "อ่างทอง",
    7: "ลพบุรี",
    8: "สิงห์บุรี",
    9: "ชัยนาท",
    10: "สระบุรี",
    11: "ชลบุรี",
    12: "ระยอง",
    13: "จันทบุรี",
    14: "ตราด",
    15: "ฉะเชิงเทรา",
    16: "ปราจีนบุรี",
    17: "นครนายก",
    18: "สระแก้ว",
    19: "นครราชสีมา",
    20: "บุรีรัมย์",
    21: "สุรินทร์",
    22: "ศรีสะเกษ",
    23: "อุบลราชธานี",
    24: "ยโสธร",
    25: "ชัยภูมิ",
    26: "อำนาจเจริญ",
    27: "หนองบัวลำภู",
    28: "ขอนแก่น",
    29: "อุดรธานี",
    30: "เลย",
    31: "หนองคาย",
    32: "มหาสารคาม",
    33: "ร้อยเอ็ด",
    34: "กาฬสินธุ์",
    35: "สกลนคร",
    36: "นครพนม",
    37: "มุกดาหาร",
    38: "เชียงใหม่",
    39: "ลำพูน",
    40: "ลำปาง",
    41: "อุตรดิตถ์",
    42: "แพร่",
    43: "น่าน",
    44: "พะเยา",
    45: "เชียงราย",
    46: "แม่ฮ่องสอน",
    47: "นครสวรรค์",
    48: "อุทัยธานี",
    49: "กำแพงเพชร",
    50: "ตาก",
    51: "สุโขทัย",
    52: "พิษณุโลก",
    53: "พิจิตร",
    54: "เพชรบูรณ์",
    55: "ราชบุรี",
    56: "กาญจนบุรี",
    57: "สุพรรณบุรี",
    58: "นครปฐม",
    59: "สมุทรสาคร",
    60: "สมุทรสงคราม",
    61: "เพชรบุรี",
    62: "ประจวบคีรีขันธ์",
    63: "นครศรีธรรมราช",
    64: "กระบี่",
    65: "พังงา",
    66: "ภูเก็ต",
    67: "สุราษฎร์ธานี",
    68: "ระนอง",
    69: "ชุมพร",
    70: "สงขลา",
    71: "สตูล",
    72: "ตรัง",
    73: "พัทลุง",
    74: "ปัตตานี",
    75: "ยะลา",
    76: "นราธิวาส",
    77: "บึงกาฬ",
}

def dictfetchall(cursor):
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

if __name__ == '__main__':
    print 'uncomment to run script.'
    exit()
    try:
        app = App.objects.get(id=3)
    except:
        print 'Insert App ClickForClever id=3 (Copy from server)'
        exit()
    today = timezone.now().date()
    cursor = connection.cursor()
    #cursor.execute("select * FROM tbl_users where user_id < 40000")
    #cursor.execute("select * FROM tbl_users")
    cursor.execute("select * FROM tbl_users where user_id in [47666, 28899, 50125, 50584, 46529, 47579, 33820, 54403, 40722, 54276, 56143, 38643, 49981, 56656, 61245, 61648, 56288, 91732, 71307, 31796, 78523, 1028951]")
    user_miss = 0
    user_hit = 0
    for row in dictfetchall(cursor):
        #if user_miss + user_hit > 200:
        #    break
        print '- %s %s %s'%(row['user_id'], row['email'], row['f_id'])
        print '%s [m%s / h%s] id: %s :'%(user_miss+user_hit, user_miss, user_hit, row['user_id']),
        continue
        email = row['email'].strip()
        #if len(email) == 0:
        #    print 'Continue email empty.'
        #    continue
        if len(row['f_id']) > 0:
            account_list = Account.objects.filter(Q(pair_id=row['user_id']) | Q(email=email) | Q(facebook_id=row['f_id']))
        else:
            account_list = Account.objects.filter(Q(pair_id=row['user_id']) | Q(email=email))
        is_update_info = True
        if account_list.count() == 0:
            print 'Miss.'
            user_miss += 1
            password = ''.join(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(12))
            if len(email) == 0:
                email = '%s@gen'%row['f_id']
            try:
                account = Account.objects.create_user(email, password)
            except:
                print '%s - %s << Continue'%(email, row['user_id'])
                #raise
                continue
            address = Address(account=account)
        else:
            user_hit += 1
            account = account_list[0]
            print 'Hit. -> %s (%s) VS. %s (%s)'%(account.email, account.pair_id, email, row['user_id'])
            try:
                address = account.address_set.all()[0]
            except:
                address = Address(account=account)
            #is_update_info = False
            is_update_info = True
        #Update Info
        if is_update_info:
            account.pair_id = row['user_id']
            account.pair_password = row['password']
            account.is_verify = True
            account.first_name = row['name']
            account.last_name = row['surname']
            account.birthday = row['birth']
            account.facebook_id = row['f_id']
            try:
                account.date_joined = row['date_regis'].date().replace(tzinfo=utc)
            except:
                account.date_joined = today
            prefix = int(row['prefix'])
            if prefix in [1, 3]:
                account.gender = 1
            elif prefix in [2, 4]:
                account.gender = 2
            else:
                account.gender = 0
            account.id_card = row['idcard']
            #account.info = json.dumps(d, indent=2)
            account.save()
            address.name = '%s %s'%(row['name'], row['surname'])
            address.address = row['address']
            address.subdistrict = ''
            address.district = row['district']
            try:
                address.province = PROVINCE[row['province']]
            except:
                address.province = ''
            address.zipcode = row['zip']
            address.tel = row['phone']
            address.save()
        else:
            if account.pair_id != row['user_id']:
                account.pair_id = row['user_id']
                account.pair_password = row['password']
                account.save(update_fields=['user_id', 'password'])
        if AccountApp.objects.filter(app=app, account=account).count() == 0:
            account_app = AccountApp(app=app,
                                     account=account)
            account_app.save()
    print 'Result Miss=%s, Hit=%s'%(user_miss, user_hit)

