import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from account.models import Account
from order.models import Order
from progress.models import Progress, Account as ProgressAccount

from order.cached import cached_order_item_delete
from progress.cached import cached_progress_delete

def _del_progress(item):
    for progress in item.progress_set.all():
        print('  - Progress ID:', progress.id)
        cached_progress_delete(progress)
        progress.delete()

def _del_order_item(order):
    for item in order.item_set.all():
        print(' - Order Item ID:', item.id)
        print(' \_', item.content_type, ' : ', item.content)
        _del_progress(item)
        cached_order_item_delete(item)
        item.delete()
        
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print ('Enter email.')
        exit()
    email = sys.argv[1]
    account = Account.objects.get(email=email)
    print(email)  
    for order in Order.objects.filter(account__email=email):
        print('Order ID:', order.id)
        _del_order_item(order)
        order.delete()
        
    ProgressAccount.update_progress(account.id)
