# -*- encoding: utf-8 -*-
__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
import os
from course.models import Course
if __name__ == '__main__':
    count = 0
    for course in Course.objects.all():
        if course.image:
            if os.path.exists(course.image.path):
                try:
                    filename, file_extension = os.path.splitext(course.image.path)
                    output = '%s/%s%s' % (course.image.path.rsplit('/', 1)[0], course.id, file_extension)
                    os.rename(course.image.path, output)
                    course.image = 'course/image/' + output.rsplit('course/image/', 1)[1]
                    course.save(update_fields=['image'])
                    print course.image.path
                except:
                    print course.id