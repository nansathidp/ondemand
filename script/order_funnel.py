from django.utils import timezone
import datetime

__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from order.models import Order

from analytic.models import OrderFunnel
if __name__ == '__main__':
    day = 3
    if len(sys.argv) > 1:
        day = int(sys.argv[1])
    for x in range(0, day):
        date_end = timezone.now().replace(hour=23, minute=59, second=59, microsecond=0) - datetime.timedelta(days=x)
        date_start = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0) - datetime.timedelta(days=x)
        order_paid_cart = Order.objects.filter(net__gt=0, timestamp__range=(date_start, date_end), status=1).count()
        order_paid_payment = Order.objects.filter(net__gt=0, timestamp__range=(date_start, date_end), status=2).count()
        order_paid_success = Order.objects.filter(net__gt=0, timestamp__range=(date_start, date_end), status=3).count()
        funnel, created = OrderFunnel.objects.get_or_create(date=date_start.date())
        funnel.cart = order_paid_cart
        funnel.payment = order_paid_payment
        funnel.success = order_paid_success
        funnel.save()
        print 'Execute : %s -- %s > %s > %s' % (funnel.date, order_paid_cart, order_paid_payment, order_paid_success)