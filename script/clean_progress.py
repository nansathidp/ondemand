import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

import getpass

from django.conf import settings

from account.models import Account
from order.models import Order
from progress.models import Progress, Account as ProgressAccount, Content as ProgressContent
from content.models import Location as ContentLocation

from course.models import Course
from question.models import Activity

from order.cached import cached_order_item_delete
from progress.cached import cached_progress_delete

def _progress(item):
    for progress in item.progress_set.all():
        print('  - Progress ID:', progress.id)
        cached_progress_delete(progress)
        progress.delete()

def _order_item(order):
    for item in order.item_set.all():
        print(' - Order Item ID:', item.id)
        print(' \_', item.content_type, ' : ', item.content)
        _progress(item)
        cached_order_item_delete(item)
        item.delete()

def _order(account):
    for order in Order.objects.filter(account=account):
        print('Order ID:', order.id)
        _order_item(order)
        order.delete()
    ProgressAccount.update_progress(account.id)


if __name__ == '__main__':
    password = getpass.getpass('Password:')
    if password != 'crm9uh':
        exit()

    for account in Account.objects.all():
        print('Clean: ', account.email)
        _order(account)
        
    print(' >> Update Course')
    content_type = settings.CONTENT_TYPE('course', 'course')
    for content in Course.objects.all():
        for content_location in ContentLocation.objects.filter(content_type=content_type,
                                                               content=content.id):
            print(' +', content.name, 'Location: ', content_location.get_parent()['name'])
            ProgressContent.update_progress(content_location.id, content_type.id, content.id)

    print(' >> Question')
    content_type = settings.CONTENT_TYPE('question', 'activity')
    for content in Activity.objects.all():
        for content_location in ContentLocation.objects.filter(content_type=content_type,
                                                               content=content.id):
            print(' +', content.name, 'Location: ', content_location.get_parent()['name'])
            ProgressContent.update_progress(content_location.id, content_type.id, content.id)
