import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from question.models import Activity, Section

if __name__ == '__main__':

    print('Update Section')
    for section in Section.objects.all():
        print(section.id)
        section.update_score()

    print('Update Activity')
    for activity in Activity.objects.all():
        print(activity.id)
        activity.update_max_score()
