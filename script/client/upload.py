# -*- coding: utf-8 -*-
import os
from boto.s3.connection import S3Connection
from boto.s3.key import Key

import requests, json, uuid

connection = S3Connection(
    host = 's3.byteark.com',
    is_secure = False,
    aws_access_key_id = 'yfko9VfPVL6kMFXkng5X',
    aws_secret_access_key = 'bMvZrcQbV7UGMPig6AMwI5QmpTzedNJLDtzvKtz1',
    debug = 2
)

bucket = connection.get_bucket('aisuacademy', validate = False)

#for key in bucket.list():
#    print key.name, key.generate_url(3600)

API_URL = 'http://127.0.0.1:8000'
KEY = 'mGilcYYHaKnehuvTzSWwF7TK4lLLiLOwUvzLe5V7M='
BASE = os.path.dirname(os.path.abspath(__file__))

def check_vdo_id(vdo_id):
    payload = {'key': KEY, 'id': vdo_id}
    r = requests.get('%s/api/v2/vdo/upload/check/'%API_URL, params=payload)
    result = json.loads(r.text)
    if result['status'] == 200:
        print 'Call : %s [%s]'%(result['result']['status'], result['result']['status_display'])
        return result['result']['status']
    else:
        print r.text

def upload(vdo_id, filename):
    path = '%s/vdo/%s'%(BASE, filename)
    path_uuid = 'vdo/%s.mp4'%str(uuid.uuid1())

    payload = {'key': KEY, 
               'id': vdo_id,
               'path': path_uuid,
               'status': 0}
    r = requests.get('%s/api/v2/vdo/upload/'%API_URL, params=payload)
    #print r.url
    result = json.loads(r.text)
    if result['status'] == 200:
        print '> Call Uploading... : %s [%s]'%(result['result']['status'], result['result']['status_display'])
    else:
        print r.text
        exit()
    print '> Upload to S3.'
    key = Key(bucket, path_uuid)
    key.set_contents_from_filename(path)

    payload = {'key': KEY, 
               'id': vdo_id,
               'path': path_uuid,
               'status': 1}
    r = requests.get('%s/api/v2/vdo/upload/'%API_URL, params=payload)
    result = json.loads(r.text)
    if result['status'] == 200:
        print '> Call Success : %s [%s]'%(result['result']['status'], result['result']['status_display'])
    else:
        print r.text
        exit()
    
if __name__ == '__main__':
    for line in open('%s/config.txt'%BASE):
        p = line.strip().split('::')
        if len(p) == 2:
            print '------ Process Vdo id: %s from Filename : %s ------'%(p[0], p[1])
            print '> Step 1 Check Vdo id.'
            status = check_vdo_id(p[0])
            if status == -1:
                exit()
            elif status == 0:
                print 'Override? [y/n]:',
                input = raw_input()
                if input.lower() == 'n':
                    continue
                elif input.lower() == 'y':
                    pass
                else:
                    print 'Please type y/n Only.'
                    exit()
            print '> Step 2 Upload.'
            upload(p[0], p[1])
