from department.models import Member

def check():
    account_id = None
    for member in Member.objects.order_by('account_id'):
        if account_id is None:
            account_id = member.account_id
        else:
            if account_id == member.account_id:
                member.delete()
            else:
                account_id = member.account_id
