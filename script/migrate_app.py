import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from app.models import App
from account.models import Account, App as AccountApp

if __name__ == '__main__':
    app = App.objects.get(id=1)
    for account in Account.objects.all():
        print account.email
        if AccountApp.objects.filter(app=app, account=account).count() == 0:
            account_app = AccountApp(app=app,
                                     account=account)
            account_app.save()
