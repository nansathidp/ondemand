# -*- encoding: utf-8 -*-
__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
import os
from PIL import Image
from highlight.models import WebBanner
if __name__ == '__main__':
    count = 0
    count_convert = 0
    count_except = 0
    size = 1150, 381
    count_all = WebBanner.objects.all().count()
    for highlight in WebBanner.objects.all():
        if highlight.image:
            if os.path.exists(highlight.image.path):
                path = highlight.image.path.rsplit('/image/', 1)
                output = path[0] + '/image/resize/' + path[1]
                try:
                    if not os.path.exists(os.path.dirname(output)):
                        os.makedirs(os.path.dirname(output))
                    img = Image.open(highlight.image.path)
                    img.thumbnail(size, Image.ANTIALIAS)
                    try:
                        img.save(output, "JPEG")
                        count += 1
                    except:
                        img.convert('RGB').save(output, "JPEG")
                        count_convert += 1
                except:
                    count_except += 1
                print output

    print 'Resize : %s' % count
    print 'Resize + Convert : %s' % count_convert
    print 'Default : %s' % count_except
    print 'Highlight web : %s of %s' % (count, count_all)
