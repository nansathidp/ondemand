# -*- encoding: utf-8 -*-
__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
import os
import shutil
from PIL import Image
from django.conf import settings
from tutor.models import Tutor
if __name__ == '__main__':
    count = 0
    count_convert = 0
    count_except = 0
    size = 150, 150
    profile_path = settings.STATICFILES_DIRS[0] + 'images/profile_default.jpg'
    count_all = Tutor.objects.all().count()
    for tutor in Tutor.objects.all():
        if tutor.image:
            if os.path.exists(tutor.image.path):
                path = tutor.image.path.rsplit('/tutor/', 1)
                output = path[0] + '/tutor/resize/' + path[1]
                try:
                    if not os.path.exists(os.path.dirname(output)):
                        os.makedirs(os.path.dirname(output))
                    img = Image.open(tutor.image.path)
                    img.thumbnail(size, Image.ANTIALIAS)
                    try:
                        img.save(output, "JPEG")
                        count += 1
                    except:
                        img.convert('RGB').save(output, "JPEG")
                        count_convert += 1
                except:
                    shutil.copy2(profile_path, output)
                    count_except += 1
                print output

    print 'Resize : %s' % count
    print 'Resize + Convert : %s' % count_convert
    print 'Default : %s' % count_except
    print 'Tutor : %s of %s' % (count, count_all)
