# -*- encoding: utf-8 -*-
__author__ = 'din'
import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
import os
from PIL import Image
from tutor.models import Feature
if __name__ == '__main__':
    count = 0
    count_convert = 0
    count_except = 0
    size = 418, 206
    count_all = Feature.objects.all().count()
    for tutor_feature in Feature.objects.all():
        if tutor_feature.image:
            if os.path.exists(tutor_feature.image.path):
                path = tutor_feature.image.path.rsplit('/feature-image/', 1)
                output = path[0] + '/feature-image/resize/' + path[1]
                try:
                    if not os.path.exists(os.path.dirname(output)):
                        os.makedirs(os.path.dirname(output))
                    img = Image.open(tutor_feature.image.path)
                    img.thumbnail(size, Image.ANTIALIAS)
                    try:
                        img.save(output, "JPEG")
                        count += 1
                    except:
                        img.convert('RGB').save(output, "JPEG")
                        count_convert += 1
                except:
                    count_except += 1
                print output

    print 'Resize : %s' % count
    print 'Resize + Convert : %s' % count_convert
    print 'Default : %s' % count_except
    print 'Tutor feature : %s of %s' % (count, count_all)
