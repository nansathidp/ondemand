# -*- encoding: utf-8 -*-
__author__ = 'din'
import os
import sys

import django

sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()
from PIL import Image
from lesson.models import Lesson
if __name__ == '__main__':
    count = 0
    count_convert = 0
    count_except = 0
    not_found = 0
    size = 428, 260
    for lesson in Lesson.objects.filter():
          if lesson.image:
            if os.path.exists(lesson.image.path):
                path = lesson.image.path.rsplit('/image/', 1)
                output = path[0] + '/image/resize/' + path[1]
                try:
                    if not os.path.exists(os.path.dirname(output)):
                        os.makedirs(os.path.dirname(output))
                    img = Image.open(lesson.image.path)
                    img.thumbnail(size, Image.ANTIALIAS)
                    try:
                        img.save(output, "JPEG")
                        count += 1
                    except:
                        img.convert('RGB').save(output, "JPEG")
                        count_convert += 1
                except:
                    count_except += 1
                print output
            else:
                not_found += 1
    print 'Not Found : %s' % not_found
    print 'Resize : %s' % count
    print 'Resize + Convert : %s' % count_convert
    print 'Default : %s' % count_except
    print 'Lesson : %s' % count

