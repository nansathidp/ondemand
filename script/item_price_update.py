import os
import sys
import django

sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings

from order.models import Item

if __name__ == '__main__':
    count = 0
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_package = settings.CONTENT_TYPE('package', 'package')
    for item in Item.objects.filter(price__gt=0, order__status=3):
        try:
            if item.price > 0:
                if item.discount > 0:
                    item.net = item.discount
                    # print 'CASE : RIGHT'
                elif item.discount == 0:
                    print 'CASE : DISCOUNT WRONG P: %s D: %s N: %s' % (item.price, item.discount, item.net)
                    item.net = item.price
                    item.discount = -1
                    item.save(update_fields=['net', 'discount'])
                elif item.discount == -1 and item.price == item.net:
                    item.discount == -1
                else:
                    print 'CASE : OTHER P: %s D: %s N: %s' % (item.price, item.discount, item.net)
                    item.net = item.price
                    item.save(update_fields=['net'])
        except:
            print 'Except : %s' % item.id
