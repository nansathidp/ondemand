import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from order.models import Item
from institute.models import Institute
from package.models import Item as PackageItem

import time

if __name__ == '__main__':
    count = 1
    for item in Item.objects.select_related('order').filter():
        print('%s >> %s at %s'%(count, item.id, item.timestamp))
        #Start
        #this magic number user in order.buy_success !!!
        if item.order.method in [1, 2, 3, 4, 5, 7, 8, 9, 10, 12, 16, 18, 19, 20]:
            item.fee = item.net * 0.04
        elif item.order.method in [6, 11]:
            item.fee = item.net * 0.3
        item.save(update_fields=['fee'])
        #End
        count += 1
        time.sleep(0.02)
        #if count > 20:
        #    break
