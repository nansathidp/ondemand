import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings

from course.models import Course,Price
from purchase.models import PriceMatrix, Apple

if __name__ == '__main__':
  content_type = settings.CONTENT_TYPE('course', 'course')
  tier = PriceMatrix.objects.get(tier=0)
  
  for course in Course.objects.all():
      price_web = Price(course=course,
                        price=course.price,
                        store=1)
      price_web.save()
      price_android = Price(course=course,
                            price=course.price,
                            store=4)
      price_android.save()
      if course.price == 0:
        price_apple = Apple(name=course.name,
                            content_type=content_type,
                            content=course.id,
                            type=1,
                            price_tier=tier)
        price_apple.save()

      print course.id
