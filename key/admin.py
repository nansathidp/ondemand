from django.contrib import admin
from key.models import Key

@admin.register(Key)
class KeyAdmin(admin.ModelAdmin):
    list_display = ('app', 'name', 'code', 'timestamp')
