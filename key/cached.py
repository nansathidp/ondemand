from django.conf import settings
from django.core.cache import cache

from key.models import Key

import hashlib

def cached_api_key(app, code, is_force=False):
    h = hashlib.sha1(code.encode('utf-8')).hexdigest()
    key = '%s_api_key_%s_%s'%(settings.CACHED_PREFIX, app.id, h)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Key.objects.filter(app=app, code=code).exists()
        cache.set(key, result, None)
    return result

