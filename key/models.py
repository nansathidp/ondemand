from django.db import models


class Key(models.Model):
    app = models.ForeignKey('app.App')
    name = models.CharField(max_length=24)
    code = models.CharField(max_length=64, blank=True, db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.code

    def save(self, *args, **kwargs):
        if len(self.code) == 0:
            self.code = Key.gen_code(self.app)
        super(self.__class__, self).save(*args, **kwargs)

    @staticmethod
    def gen_code(app):
        import uuid
        code = None
        while True:
            code = str(uuid.uuid4()).replace('-', '')
            if Key.objects.filter(app=app, code=code).count() == 0:
                break
        return code

