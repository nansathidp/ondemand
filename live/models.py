from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver


class Live(models.Model):
    import datetime

    STATUS_CHOICES = (
        (0, 'Offline'), #Check Start
        (1, 'Live'),
        (2, 'Live End'),
        (3, 'Convert'),
        (4, 'Playback'),
    )

    name = models.CharField(max_length=120)
    desc = models.TextField(blank=True)
    overview = models.TextField(blank=True)
    image = models.ImageField(upload_to='live/image/')
    tutor = models.ManyToManyField('tutor.Tutor', blank=True)

    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)
    credit = models.DurationField(default=datetime.timedelta(0))
    expired = models.DurationField(default=datetime.timedelta(0))

    stream_key = models.CharField(max_length=24, blank=True, db_index=True)

    is_display = models.BooleanField(db_index=True, default=False)
    status = models.IntegerField(choices=STATUS_CHOICES, default=0)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    #Wmspanel.com : Control -> WMSAuth paywall setup
    SIGN_KEY = 'atPyw8kplvtBsVVoPcrxu2QAos+VKhWeJvY2i3Dm+8k='
    VOD_KEY = '00OZS13p24xmiMvVV5zB/adZmBelHhyoaDaWuQZk9LE='

    class Meta:
        default_permissions = ('view', 'add', 'change', 'delete')

    def __str__(self):
        return self.name

    @property
    def is_live(self):
        from django.utils import timezone
        if self.start is not None and self.end is not None:
            return self.start <= timezone.now() <= self.end
        else:
            return False

    @property
    def is_past(self):
        from django.utils import timezone
        if self.end is not None:
            return self.end < timezone.now()
        else:
            return False

    @staticmethod
    def pull(id):
        from .cached import cached_live
        return cached_live(id)
    
    def gen_stream_key(self):
        import random
        c = 'abcdefghijklmnopqrstuvwxyz23456789'
        while True:
            code = '%s_%s' % (self.id, ''.join(random.choice(c) for _ in range(8)))
            if not Live.objects.filter(stream_key=code).exists():
                self.stream_key = code
                self.save(update_fields=['stream_key'])
                break

    def m3u8_url(self, ip):
        from django.conf import settings
        from stream.models import Stream
        sign = '?wmsAuthSign=%s' % Stream.nimble_sign(ip, Live.SIGN_KEY)
        return '%s/%s/playlist.m3u8%s' % (settings.NIMBLE_LIVE_URL, self.stream_key, sign)

    def dvr_url(self, ip):
        from stream.models import Stream
        sign = '?wmsAuthSign=%s'%Stream.nimble_sign(ip, Live.SIGN_KEY)
        return 'https://live-s1.cocodemy.com:8443/live/%s/playlist_dvr.m3u8%s'%(self.stream_key, sign)

    def vod_url(self, ip):
        from stream.models import Stream
        sign = '?wmsAuthSign=%s'%Stream.nimble_sign(ip, Live.VOD_KEY)
        return 'https://live-s1.cocodemy.com:8443/content/live/%s.mp4/playlist.m3u8%s'%(self.stream_key, sign)

    def stream_src(self, ip):
        if self.status in [0, 1, 2]:
            return self.m3u8_url(ip)
        elif self.status == 3:
            return self.dvr_url(ip)
        elif self.status == 4:
            return self.vod_url(ip)
        return None

    def api_display_title(self, store, install_version=0.0, store_version=0.0):
        from django.utils import timezone
        from price.models import Price
        from app.views import update_checkout_type

        tutor_list = []

        for tutor in self.tutor.all():
            tutor_list.append(tutor.api_display_title())
        if self.image:
            image = self.image.url
        else:
            image = None

        price = Price.pull(settings.CONTENT_TYPE('live', 'live'), self.id, store)
        price_set = price.api_display()

        result = {'id': self.id,
                  'name': self.name,
                  'start': timezone.get_current_timezone().normalize(self.start).strftime(settings.TIMESTAMP_FORMAT_),
                  'end': timezone.get_current_timezone().normalize(self.end).strftime(settings.TIMESTAMP_FORMAT_),
                  'price_set': price_set,
                  'tutor_list': tutor_list,
                  'image': image,
                  'status': self.status
                  }

        update_checkout_type(result, install_version, store_version)
        return result

    def api_display(self, store, install_version=0.0, store_version=0.0):
        d = self.api_display_title(store, install_version, store_version)
        d.update({'desc': self.desc})
        return d

    @property
    def is_live_now(self):
        from django.utils import timezone
        now = timezone.now()
        return now >= self.start and now <= self.end

    def manage_server_status(self):
        import requests
        url = 'http://103.246.16.183:8082/manage/server_status'
        r = requests.get(url=url)

    def manage_rtmp_status(self):
        import requests
        url = 'http://103.246.16.183:8082/manage/rtmp_status'
        r = requests.get(url=url)
        result = r.json()
        if len(result) == 0:
            return False
        else:
            result = result[0]
        for stream in result['streams']:
            if self.stream_key == stream['strm']:
                return True
        return False


@receiver(post_save, sender=Live)
def post_save(sender, **kwargs):
    from .cached import cached_live_delete
    cached_live_delete(kwargs['instance'].id)
