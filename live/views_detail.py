from django.http import Http404
from django.shortcuts import render
from django.conf import settings

from access.models import Access

from .cached import cached_live
from order.cached import cached_order_item_is_purchased

from api.views_ais_privilege import get_client_ip


def detail_view(request, live_id):
    live = cached_live(live_id)
    if live is None:
        raise Http404()
    content_type_live = settings.CONTENT_TYPE('live', 'live')
    content_permission, is_require_permission = Access.get_permission(live_id, content_type_live.id, request.user)

    if request.user.is_authenticated():
        order_item = cached_order_item_is_purchased(request.user, content_type_live, live.id)
    else:
        order_item = None
    ip = get_client_ip(request)
    
    return render(request,
                  'live/detail.html',
                  {'live': live,
                   'is_require_permission': is_require_permission,
                   'content_permission': content_permission,
                   'order_item': order_item,
                   'ROOT_PAGE': 'live',
                   'DISPLAY_SUBMENU': True,
                   'stream_src': live.stream_src(ip)})
