from django.conf.urls import url

from .views import home_view, previous_view
from .views_detail import detail_view
from .views_buy import checkout_view

app_name = 'live'
urlpatterns = [
    url(r'^$', home_view, name='home'),  # TODO: testing
    url(r'^previous/$', previous_view, name='previous'),  # TODO: testing
    url(r'^(\d+)/$', detail_view, name='detail'),  # TODO: testing
    url(r'^(\d+)/checkout/$', checkout_view, name='checkout'),  # TODO: testing
]
