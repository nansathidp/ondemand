from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import redirect

from order.models import Order
from content.models import Location as ContentLocation

from .cached import cached_live
from django.conf import settings


@login_required
def checkout_view(request, live_id):
    live = cached_live(live_id)
    if live is None:
        raise Http404()
    
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('live', 'live'),
                                                  live.id)
    order_item = Order.buy(content_location,
                           request.APP,
                           1,
                           settings.CONTENT_TYPE('live', 'live'),
                           live,
                           request.user)

    if ordder_item is not None:
        order = order_item.order

    if order_item.is_free:
        return redirect('live:detail', live_id)
    else:
        return redirect('order:checkout', order.id)
