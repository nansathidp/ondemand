from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Live
from ..job.stream_convert import _stream_convert

import django_rq

def stream_convert_view(request, lesson_id):
    if not request.user.has_perm('live.view_live',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    live = get_object_or_404(Live, id=lesson_id)
    if live.status in [0, 1, 2]:
        live.status = 3
        live.save(update_fields=['status'])
        django_rq.enqueue(_stream_convert, live.id)
    return redirect('dashboard:live-dashboard:stream', live.id)
