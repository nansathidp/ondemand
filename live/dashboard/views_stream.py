from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.core.urlresolvers import reverse
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from ..models import Live

from api.views_ais_privilege import get_client_ip

def stream_view(request, lesson_id):
    if not request.user.has_perm('live.view_live',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    live = get_object_or_404(Live, id=lesson_id)
    ip = get_client_ip(request)
    stream_src = live.stream_src(ip)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Live Management',
         'url': reverse('dashboard:live-dashboard:home')},
        {'is_active': False,
         'title': live.name,
         'url': reverse('dashboard:live-dashboard:information', args=[live.id])},
        {'is_active': True,
         'title': 'Stream'},
    ]
    return render(request, 'live/dashboard/stream.html',
                  {'SIDEBAR': 'live',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'stream',
                   'live': live,
                   'stream_src': stream_src,
                   'stream_url': settings.CONFIG(request.get_host(), 'LIVE_STREAM_URL')})


def get_stream_status(live_id):
    live = get_object_or_404(Live, id=live_id)
    stream_status = live.manage_rtmp_status()
    live.stream_status = stream_status
    if live.status == 0 and stream_status:
        live.status = 1
        live.save(update_fields=['status'])
    elif live.status == 2 and stream_status:
        live.status = 1
        live.save(update_fields=['status'])
    elif live.status == 1 and not stream_status:
        live.status = 2
        live.save(update_fields=['status'])
    return live


@csrf_exempt
def stream_status_view(request, live_id):
    from ..cached import cached_stream_status
    if not request.user.has_perm('live.view_live',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    live = cached_stream_status(live_id)
    source = live.stream_src(get_client_ip(request))
    return JsonResponse({'stream': live.stream_status,
                         'status': live.get_status_display(),
                         'source': source,
                         'live_status': live.status})
