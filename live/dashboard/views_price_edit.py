from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.conf import settings

from ..models import Live
from price.models import Price
from purchase.models import Apple, PriceMatrix

from ..cached import cached_live
import datetime

@csrf_exempt
def price_edit_view(request, live_id):
    if not request.user.has_perm('live.view_live',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    live = get_object_or_404(Live, id=live_id)
    content_type = settings.CONTENT_TYPE('live', 'live')
    
    if request.method == 'POST':
        price = get_object_or_404(Price, content_type=content_type, id=request.POST.get('id'))
        action = request.POST.get('action')
        if action == 'load':
            s = int(price.credit.total_seconds())
            h, r = divmod(s, 3600)
            m, s = divmod(r, 60)
            if price.store == 3:
                ios_price = price.ios.price_tier_id
                ios_discount = price.ios.discount_tier_id
            else:
                ios_price = -1
                ios_discount = -1
            return JsonResponse({'price': price.price,
                                 'discount': price.discount,
                                 'ios_price': ios_price,
                                 'ios_discount': ios_discount,
                                 'credit': '%.2d:%.2d:%.2d'%(h, m, s),
                                 'store': price.store})
        elif action == 'save':
            try:
                if price.store == 3:
                    apple = Apple.push(live.name,
                                       settings.CONTENT_TYPE('live', 'live'), live.id,
                                       get_object_or_404(PriceMatrix, id=request.POST.get('ios_price')),
                                       get_object_or_404(PriceMatrix, id=request.POST.get('ios_discount')))
                    price.ios = apple
                else:
                    price.price = float(request.POST.get('price'))
                    price.discount = float(request.POST.get('discount'))
                if price.store == 6:
                    s = request.POST.get('credit').split(':')
                    price.credit = datetime.timedelta(hours=int(s[0]), minutes=int(s[1]), seconds=int(s[2]))
                price.save(update_fields=['price', 'discount', 'ios', 'credit'])
                cached_live(live.id, is_force=True)
            except:
                raise
                pass
    price_list = Price.objects.filter(content_type=content_type,
                                      content=live.id)
    return render(request,
                  'live/dashboard/price_block.html',
                  {'price_list': price_list})

