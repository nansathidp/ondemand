# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from ..models import Live


class LiveForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(LiveForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({'type': 'file'})
        self.fields['start'].widget.attrs.update({'class': 'date-time-picker'})
        self.fields['end'].widget.attrs.update({'class': 'date-time-picker'})
        self.fields['start'].widget.format = '%m/%d/%Y %H:%M'
        self.fields['end'].widget.format = '%m/%d/%Y %H:%M'

    class Meta:
        model = Live
        fields = ['name', 'image', 'start', 'end', 'overview', 'desc', 'tutor', 'is_display']

        labels = {
            'tutor': 'Instructor',
        }

        help_texts = {
            'name': 'Limit at 60 characters',
            'image': settings.HELP_TEXT_IMAGE('live'),
            'overview': 'Limit at 400 characters'
        }
