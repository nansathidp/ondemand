from django.conf.urls import url

from .views import view
from .views_create import create_view

from .views_detail import information_view
from .views_stream import stream_view, stream_status_view
from .views_stream_convert import stream_convert_view

from .views_price import price_view
from .views_price_edit import price_edit_view
from .views_price_use import price_use_view

from .views_access import access_view

app_name = 'live-dashboard'
urlpatterns = [
    url(r'^$', view, name='home'), #TODO: testing
    url(r'^create/$', create_view, name='create'), #TODO: testing
    
    url(r'^(\d+)/information/$', information_view, name='information'), #TODO: testing
    url(r'^(\d+)/stream/$', stream_view, name='stream'), #TODO: testing
    url(r'^(\d+)/stream/status/$', stream_status_view, name='stream_status'), #TODO: testing
    url(r'^(\d+)/stream/convert/$', stream_convert_view, name='stream_convert'), #TODO: testing

    #Price
    url(r'^(\d+)/price/$', price_view, name='price'), #TODO: testing
    url(r'^(\d+)/price/edit/$', price_edit_view, name='price_edit'), #TODO: testing
    url(r'^(\d+)/price/use/$', price_use_view, name='price_use'), #TODO: testing

    url(r'^(\d+)/access/$', access_view, name='access'), #TODO: testing
]
