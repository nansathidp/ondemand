from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.conf import settings

from ..models import Live
from price.models import Price

@csrf_exempt
def price_use_view(request, live_id):
    if not request.user.has_perm('live.view_live',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    live = get_object_or_404(Live, id=live_id)
    content_type = settings.CONTENT_TYPE('live', 'live')
    
    if request.method == 'POST':
        price = get_object_or_404(Price,
                                  id=request.POST.get('id'),
                                  content_type=content_type,
                                  content=live.id)
        price.is_use = not price.is_use
        price.save(update_fields=['is_use'])
        
    price_list = Price.objects.filter(content_type=content_type,
                                      content=live.id)
    return render(request,
                  'live/dashboard/price_block.html',
                  {'price_list': price_list})
