from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.core.exceptions import PermissionDenied

from ..models import Live
from access.models import Access, Level

from access.cached import cached_access_level, cached_access_permission

def access_view(request, live_id):
    if not request.user.has_perm('live.view_live',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    live = get_object_or_404(Live, id=live_id)
    level_list = cached_access_level()
    content_type_live = settings.CONTENT_TYPE('live', 'live')
    content_permission = cached_access_permission(live_id, content_type_live.id)

    if request.method == 'POST':
        level_id = request.POST.get('level_id', None)
        if level_id is None or int(level_id) == -1:
            Access.objects.filter(content_type_id=content_type_live.id, content=live_id).delete()
            content_permission = cached_access_permission(live_id, content_type_live.id, is_force=True)
        else:
            level = Level.objects.filter(id=level_id).first()
            try:
                content_permission = Access.objects.get(content_type=content_type_live, content=live_id)
                content_permission.level = level
                content_permission.save(update_fields=['level'])
            except Access.DoesNotExist:
                content_permission = Access(content_type=content_type_live,
                                            content=live_id,
                                            level=level)
                content_permission.save()

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Live Management'},
    ]
    return render(request,
                  'live/dashboard/access.html',
                  {'SIDEBAR': 'live',
                   'live': live,
                   'TAB': 'access',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'level_list': level_list,
                   'content_permission': content_permission})
