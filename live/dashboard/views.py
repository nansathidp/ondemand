from django.shortcuts import render
from django.core.exceptions import PermissionDenied

from utils.paginator import paginator
from ..models import Live


def view(request):
    if not request.user.has_perm('live.view_live',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Live Management'}
    ]

    live_list = Live.objects.all().order_by('-timestamp')
    live_list = paginator(request, live_list)

    return render(request, 'live/dashboard/home.html',
                  {'SIDEBAR': 'live',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'live_list': live_list})
