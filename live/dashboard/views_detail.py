from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.core.exceptions import PermissionDenied

from ..models import Live

from .forms import LiveForm

def information_view(request, lesson_id):
    if not request.user.has_perm('live.view_live',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    live = get_object_or_404(Live, id=lesson_id)
    live.manage_rtmp_status()

    if request.method == 'POST':
        live_form = LiveForm(request.POST, request.FILES, instance=live)
        if live_form.is_valid():
            live = live_form.save()
            if len(live.stream_key) == 0:
                live.gen_stream_key()
            live_form = LiveForm(instance=live)
    else:
        live_form = LiveForm(instance=live)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Live Management',
         'url': reverse('dashboard:live-dashboard:home')},
        {'is_active': True,
         'title': live.name},
    ]
    return render(request, 'live/dashboard/information.html',
                  {'SIDEBAR': 'live',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'information',
                   'live_form': live_form,
                   'live': live})
