from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.conf import settings

from ..models import Live
from price.models import Price
from purchase.models import PriceMatrix


def price_view(request, live_id):
    if not request.user.has_perm('live.view_live',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    live = get_object_or_404(Live, id=live_id)
    price_list = Price.objects.filter(content_type=settings.CONTENT_TYPE('live', 'live'),
                                      content=live.id)
    price_matrix_list = PriceMatrix.objects.all()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Live Management',
         'url': reverse('dashboard:live-dashboard:home')},
        {'is_active': True,
         'title': 'Price: %s'%live.name},
    ]
    return render(request,
                  'live/dashboard/price.html',
                  {'SIDEBAR': 'live',
                   'TAB': 'price',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'live': live,
                   'price_list': price_list,
                   'price_matrix_list': price_matrix_list})
