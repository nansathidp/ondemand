from django.shortcuts import render, redirect
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.conf import settings

from price.models import Price

from .forms import LiveForm

def create_view(request):
    if not request.user.has_perm('live.add_live',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    if request.method == 'POST':
        live_form = LiveForm(request.POST, request.FILES)
        if live_form.is_valid():
            live = live_form.save()
            live.gen_stream_key()
            Price.init(settings.CONTENT_TYPE('live', 'live'), live.id)
            return redirect('dashboard:live-dashboard:home')
    else:
        live_form = LiveForm()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Live Management',
         'url': reverse('dashboard:live-dashboard:home')},
        {'is_active': True,
         'title': 'Create'},
    ]
    return render(request, 'live/dashboard/create.html',
                  {'SIDEBAR': 'live',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'live_form': live_form})
