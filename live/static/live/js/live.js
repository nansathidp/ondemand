function initFlowplayer(source){
    api = flowplayer("#hlsjslive", {
      splash: true,
      embed: false,
      ratio: 9/16,
      clip: {
        autoplay: true,
        live: true,

        sources: [
        { type: "application/x-mpegurl",
          src:  source }
      ]}
    });

    api.on("load", function () {
      console.log('on:load')
    }).on("ready", function () {
      console.log('on:ready')
    });

     api.on("load ready", function (e, api, video) {
      var log = $("<p/>").text(e.type + ": " + video.src + ", duration: " +
                               (video.duration || "not available"));
      console.log(log);
    });

    api.bind("load", function(){ api.play(); });
}
