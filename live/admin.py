from django.contrib import admin
from .models import Live


@admin.register(Live)
class LiveAdmin(admin.ModelAdmin):
    list_display = ('name', 'start', 'end', 'timestamp')
