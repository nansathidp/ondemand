import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings
from live.models import Live


def _stream_convert(live_id):
    live = Live.objects.get(id=live_id)
    path = os.path.join(settings.BASE_DIR, 'live', 'dvr')
    if not os.path.isdir(path):
        os.makedirs(path)

    cmd = '/usr/bin/ffmpeg -i %s -bsf:a aac_adtstoasc -codec copy %s/%s.mp4'%(live.dvr_url(settings.SERVER_IP),
                                                                              path,
                                                                              live.stream_key)
    os.system(cmd)

    folder = '/home/conicle/video/live'
    cmd = 'scp %s/%s.mp4 conicle@103.246.16.183:%s/%s.mp4'%(path, live.stream_key,
                                                            folder, live.stream_key)
    os.system(cmd)
    live.status = 4
    live.save(update_fields=['status'])
    os.remove('%s/%s.mp4' % (path, live.stream_key))
