from django.conf import settings

from order.models import Order
from content.models import Location as ContentLocation

from order.views_base import get_payment_url
from .views import check_key, check_require, check_auth
from .views import json_render
from ...cached import cached_live


def checkout_view(request, live_id):
    result = {
        'redirect_url': None
    }
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render(result, code)

    account, code = check_auth(request)
    if code != 200:
        return json_render({}, code)

    live = cached_live(live_id)
    if live is None:
        return json_render(result, 6000)

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('live', 'live'),
                                                  live.id)
    order_item = Order.buy(content_location,
                           request.APP,
                           store,
                           settings.CONTENT_TYPE('live', 'live'),
                           live,
                           account)

    if order_item is None:
        return json_render(result, 724)
    else:
        order = order_item.order

    result['order'] = order.api_display()
    result['is_free'] = order_item.is_free
    if order_item.is_free:
        order_item.order.buy_success(request.APP, request.get_host().split(':')[0])
        result['redirect_url'] = None
    else:
        result['redirect_url'] = get_payment_url(request, order, token, uuid, store)

    return json_render(result, code)
