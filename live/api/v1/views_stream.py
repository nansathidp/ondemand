from api.views_ip import get_client_ip
from .views import check_key, check_require
from .views import json_render
from ...cached import cached_stream_status


def status_view(request, live_id):
    result = {}
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render(result, code)

    live = cached_stream_status(live_id)
    source = live.stream_src(get_client_ip(request))
    result['stream'] = live.stream_status
    result['status'] = live.get_status_display()
    result['source'] = source
    result['live_status'] = live.status
    return json_render(result, code)

