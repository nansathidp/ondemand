from django.http import JsonResponse

from api.views_api import STATUS_MSG

_STATUS_MSG = {
    200: 'Success',
    201: 'Require POST method.',
    202: 'Require UUID.',
    204: 'Require token.',
    209: 'Require Store.',

    6000: 'Live not found',
}


def _json_render(result, status):
    try:
        status_msg = _STATUS_MSG[status]
    except:
        status_msg = STATUS_MSG[status]
    data = {'status': status,
            'status_msg': status_msg,
            'result': result}
    return JsonResponse(data, json_dumps_params={'indent': 2})

try:
    from api.views import json_render
    json_render = json_render
except:
    json_render = _json_render


def _group(live_list, store):
    live_group = {}
    for live in live_list:
        key = live.start.strftime("%b %y")
        if key in live_group:
            live_group[key]['live_list'].append(live.api_display_title(store=store))
        else:
            live_group[key] = {
                'month': key,
                'live_list': [live.api_display_title(store=store)]
            }
    return dict_to_list(live_group)


def dict_to_list(dict):
    list = []
    for key, value in dict.items():
        list.append(value)
    return list

from api.views import check_auth
from api.v5.views import check_key
from api.v5.views import check_require
