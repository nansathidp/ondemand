from django.conf import settings

from api.views_ip import get_client_ip
from order.cached import cached_order_item_is_purchased
from access.models import Access

from .views import check_key, check_require, check_auth
from .views import json_render
from ...cached import cached_live


def detail_view(request, live_id):
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render({}, code)

    # Check Acccount
    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)

    live = cached_live(live_id)
    if live is None:
        return json_render({}, 6000)

    # Check Content Permission
    install_version = request.GET.get('version', 0)
    result = live.api_display(store, install_version, request.APP.app_version)
    content_type_live = settings.CONTENT_TYPE('live', 'live')
    content_permission, is_require_permission = Access.get_permission(live_id, content_type_live.id, account)

    result.update({
        'content_permission': content_permission.api_display() if content_permission is not None else None,
        'is_require_permission': is_require_permission,
    })

    # Check Purchased
    result.update({
        'is_purchase': False,
        'order_item_id': None,
        'stream_src': None
    })

    if account is not None:
        order_item = cached_order_item_is_purchased(account, content_type_live, live.id)
        if order_item is not None:
            ip = get_client_ip(request)
            result.update({
                'is_purchase': True,
                'order_item_id': order_item.id,
                'stream_src': live.stream_src(ip)
            })

    return json_render(result, code)
