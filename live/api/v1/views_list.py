from django.utils import timezone

from .views import check_key, check_require
from .views import json_render

from ...models import Live
from .views import _group


def list_view(request):
    result = {}
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render(result, code)

    now = timezone.now()
    live_list = Live.objects.filter(end__gt=now, is_display=True).order_by('start')
    result['month_list'] = _group(live_list, store)

    return json_render(result, code)


def previous_list_view(request):
    result = {}
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render(result, code)

    now = timezone.now()
    live_list = Live.objects.filter(end__lte=now, is_display=True)
    result['month_list'] = _group(live_list, store)
    return json_render(result, code)



