from django.conf.urls import url

from .views_list import list_view, previous_list_view
from .views_detail import detail_view
from .views_stream import status_view
from .views_checkout import checkout_view

urlpatterns = [
    url(r'^$', list_view),  # TODO: testing
    url(r'^previous/$', previous_list_view),  # TODO: testing
    url(r'^(\d+)/$', detail_view),  # TODO: testing
    url(r'^(\d+)/status/$', status_view),  # TODO: testing
    url(r'^(\d+)/buy/$', checkout_view),  # TODO: testing
]
