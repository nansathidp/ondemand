from django.conf import settings
from django.core.cache import cache
from utils.cached.time_out import get_time_out_week, get_time_out

from .models import Live
from price.models import Price


def cached_live(live_id, store=1, is_force=False):
    key = '%s_live_%s' % (settings.CACHED_PREFIX, live_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Live.objects.prefetch_related('tutor').get(id=live_id)
            if result.tutor is not None:
                result.tutor_list = list(result.tutor.all())
            else:
                result.tutor_list = []
        except Live.DoesNotExist:
            result = -1
        cache.set(key, result, get_time_out_week())
    result.price = Price.pull(settings.CONTENT_TYPE('live', 'live'), live_id, store)
    return None if result == -1 else result


def cached_live_delete(live_id):
    key = '%s_live_%s' % (settings.CACHED_PREFIX, live_id)
    cache.delete(key)


def cached_stream_status(live_id, is_force=False):
    key = '%s_live_status_%s' % (settings.CACHED_PREFIX, live_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Live.objects.get(id=live_id)
            stream_status = result.manage_rtmp_status()
            result.stream_status = stream_status
            if result.status == 0 and stream_status:
                result.status = 1
                result.save(update_fields=['status'])
            elif result.status == 2 and stream_status:
                result.status = 1
                result.save(update_fields=['status'])
            elif result.status == 1 and not stream_status:
                result.status = 2
                result.save(update_fields=['status'])
        except Live.DoesNotExist:
            result = -1
        cache.set(key, result, 8)
    return None if result == -1 else result