from django.shortcuts import render
from django.utils import timezone

from .models import Live


def _group(live_id_list):
    from .cached import cached_live
    live_list = []
    for live_id in live_id_list:
        live = cached_live(live_id)
        if live is not None:
            live_list.append(live)

    live_group = {}
    for live in live_list:
        key = live.start.strftime("%b %y")
        if key in live_group:
            live_group[key]['live_list'].append(live)
        else:
            live_group[key] = {
                'month': key,
                'live_list': [live]
            }
    return live_group


def home_view(request):
    now = timezone.now()
    live_id_list = Live.objects.values_list('id', flat=True).filter(end__gt=now, is_display=True).order_by('-start')
    live_group = _group(live_id_list)
    return render(request, 'live/home.html', {
        'DISPLAY_SUBMENU': True,
        'ROOT_PAGE': 'live',
        'TAB': 'upcoming',
        'live_group': live_group,
    })


def previous_view(request):
    now = timezone.now()
    filter_start = request.GET.get('start', None)
    filter_end = request.GET.get('end', None)

    live_id_list = Live.objects.filter(end__lte=now, is_display=True).order_by('start')
    if filter_start is not None and filter_end is not None:
        live_id_list = live_id_list.filter(start__range=(filter_start, filter_end))
    elif filter_start is not None:
        live_id_list = live_id_list.filter(start__range=(filter_start, filter_end))
    elif filter_end is not None:
        live_id_list = live_id_list.filter(start__range=(filter_start, filter_end))
    live_group = _group(live_id_list.values_list('id', flat=True))
    return render(request, 'live/previous.html', {
        'ROOT_PAGE': 'live',
        'DISPLAY_SUBMENU': True,
        'TAB': 'previous',
        'live_group': live_group,
        'filter_start': filter_start,
        'filter_end': filter_end,
    })
