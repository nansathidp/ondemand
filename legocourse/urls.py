from django.conf.urls import include, url
from rest_framework import routers
from rest_framework.routers import Route

from .views import CourseViewSet


router = routers.DefaultRouter()
router.register(r'', CourseViewSet)
router.routes.extend([
    Route(url=r'^{prefix}/(?P<id>[0-9]+)/playlist/(?P<slot_id>[0-9]+)/$',
          name='{basename}-slot',
          mapping={
              'get': 'playlist',
          },
          initkwargs={}
    ),
    # Route(url=r'^{prefix}/(?P<id>[0-9]+)/program/(?P<program_id>[0-9]+)/$',
    #       name='{basename}-program-detail',
    #       mapping={
    #           'get': 'program',
    #       },
    #       initkwargs={}
    # ),
    # Route(url=r'^{prefix}/(?P<id>[0-9]+)/program/(?P<program_id>[0-9]+)/playlist/(?P<slot_id>[0-9]+)/$',
    #       name='{basename}-program-detail-playlist',
    #       mapping={
    #           'get': 'program_playlist',
    #       },
    #       initkwargs={}
    # ),
])

urlpatterns = [
    url(r'^', include(router.urls)),
]
