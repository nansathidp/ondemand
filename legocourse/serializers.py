from rest_framework import serializers
from django.conf import settings

from rating.models import Rating
from .models import Course, Section, Slot
from legoaudio.serializers import AudioSerializer, AudioFileSerializer
from legovideo.serializers import VideoSerializer, VideoFileSerializer
from legomaterial.serializers import WebLinkSerializer, DocumentSerializer, FileDownloadSerializer, WebLinkFileSerializer, DocumentFileSerializer, FileDownloadFileSerializer
from legoscorm.serializers import ScormSerializer, ScormFileSerializer
from rating.serializers import RatingSerializer


class SlotSerializer(serializers.ModelSerializer):
    class Meta:
        model = Slot
        fields = ('id', 'sort', 'content')

    def to_representation(self, instance):
        data = super(SlotSerializer, self).to_representation(instance)

        if instance.content_type == settings.CONTENT_TYPE('legoaudio', 'audio'):
            serializer = AudioSerializer(instance.content).data
        elif instance.content_type == settings.CONTENT_TYPE('legovideo', 'video'):
            serializer = VideoSerializer(instance.content).data
        elif instance.content_type == settings.CONTENT_TYPE('legomaterial', 'weblink'):
            serializer = WebLinkSerializer(instance.content).data
        elif instance.content_type == settings.CONTENT_TYPE('legomaterial', 'document'):
            serializer = DocumentSerializer(instance.content).data
        elif instance.content_type == settings.CONTENT_TYPE('legomaterial', 'filedownload'):
            serializer = FileDownloadSerializer(instance.content).data
        elif instance.content_type == settings.CONTENT_TYPE('legoscorm', 'scorm'):
            serializer = ScormSerializer(instance.content).data

        data.update({
            'content': serializer
        })

        return data


class SlotFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Slot
        fields = ()

    def to_representation(self, instance):
        data = super(SlotFileSerializer, self).to_representation(instance)

        if instance.content_type == settings.CONTENT_TYPE('legoaudio', 'audio'):
            serializer = AudioFileSerializer(instance.content).data
        elif instance.content_type == settings.CONTENT_TYPE('legovideo', 'video'):
            serializer = VideoFileSerializer(instance.content).data
        elif instance.content_type == settings.CONTENT_TYPE('legomaterial', 'weblink'):
            serializer = WebLinkFileSerializer(instance.content).data
        elif instance.content_type == settings.CONTENT_TYPE('legomaterial', 'document'):
            serializer = DocumentFileSerializer(instance.content).data
        elif instance.content_type == settings.CONTENT_TYPE('legomaterial', 'filedownload'):
            serializer = FileDownloadFileSerializer(instance.content).data
        elif instance.content_type == settings.CONTENT_TYPE('legoscorm', 'scorm'):
            serializer = ScormFileSerializer(instance.content).data

        data.update({
            'file_path': serializer
        })

        return data

class SlotNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Slot
        fields = ('id', 'sort')

    def to_representation(self, instance):
        data = super(SlotNameSerializer, self).to_representation(instance)
        data.update({
            'content_name': instance.get_content_name()
        })
        return data


class SectionSerializer(serializers.ModelSerializer):
    slot_list = serializers.SerializerMethodField()

    class Meta:
        model = Section
        fields = ('id', 'name', 'sort', 'slot_list')

    def get_slot_list(self, section):
        result = []
        for slot in section.slot_set.all():
            result.append(SlotSerializer(slot).data)
        return result


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('id', 'name', 'image', 'is_display', 'overview')

    def to_representation(self, instance):
        data = super(CourseSerializer, self).to_representation(instance)
        data.update({
            'rating': RatingSerializer(Rating.objects.filter(content=instance.id).first()).data
        })
        return data


class CourseDetailSerializer(serializers.ModelSerializer):
    section_list = serializers.SerializerMethodField()

    class Meta:
        model = Course
        fields = ('id', 'name', 'image', 'overview', 'desc', 'condition', 'is_display', 'section_list')


    def get_section_list(self, course):
        result = []
        for section in course.section_set.all():
            result.append(SectionSerializer(section).data)
        return result


class PlayListSerializer(serializers.ModelSerializer):
    slot_name_list = serializers.SerializerMethodField()

    class Meta:
        model = Section
        fields = ('id', 'name', 'slot_name_list')


    def get_slot_name_list(self, section):
        result = []
        for section in section.slot_set.all():
            result.append(SlotNameSerializer(section).data)
        return result
