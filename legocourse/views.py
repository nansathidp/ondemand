from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
<<<<<<< HEAD
from rest_framework.decorators import detail_route, list_route

from .models import Course, Section, Slot
from .serializers import CourseSerializer, CourseDetailSerializer, PlayListSerializer, SectionSerializer, SlotFileSerializer
=======
from rest_framework.decorators import detail_route

from .models import Course, Section
from .serializers import CourseSerializer, CourseDetailSerializer, PlayListSerializer, SectionSerializer
>>>>>>> e9f042a62a532f349d60f54d373de8b12820a735


class CourseViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Course.objects.filter(is_display=True)
    serializer_class = CourseSerializer

    def retrieve(self, request, pk=None):
        course = self.get_object()
        serializer = CourseDetailSerializer(course)
        return Response(serializer.data)

    def playlist(self, request, id, slot_id):
        course = Course.objects.get(id=id)
        serializer = PlayListSerializer(course)
        return Response(serializer.data)

    # def program(self, request, id, program_id):
    #     course = Course.objects.get(id=id)
    #     serializer = CourseDetailSerializer(course)
    #     return Response(serializer.data)

    # def program_playlist(self, request, id, program_id, slot_id):
    #     slot = Slot.objects.get(id=slot_id)
    #     serializer = SlotFileSerializer(slot)
    #     return Response(serializer.data)
