from django.db import models


class PriceMatrix(models.Model):
    tier = models.IntegerField(db_index=True, unique=True)
    price_usd = models.FloatField(default=0.0)
    price_thb = models.FloatField(default=0.0)

    class Meta:
        verbose_name = 'Apple price matrix'
        ordering = ['tier']

    def __str__(self):
        return str(self.tier)

    def api_display(self):
        return {'tier': self.tier,
                'price_usd': self.price_usd,
                'price_thb': self.price_thb}


class Apple(models.Model):
    TYPE_CHOICES = (
        (1, 'Consumables'),
        (2, 'Non-consumables'),
        (3, 'Subscriptions'),
        (4, 'Non-Renewing Subscription'),
    )

    name = models.CharField(max_length=120)
    content_type = models.ForeignKey('contenttypes.ContentType')
    content = models.BigIntegerField(db_index=True)
    
    type = models.IntegerField(choices=TYPE_CHOICES)
    price_tier = models.ForeignKey(PriceMatrix, null=True, blank=True)
    discount_tier = models.ForeignKey(PriceMatrix, null=True, blank=True, related_name='discount_set')
    
    is_public = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Apple in app purchase'

    def __str__(self):
        return self.name

    @property
    def net(self):
        if self.discount_tier is not None:
            return self.discount_tier.price_usd
        else:
            return self.price_tier.price_usd

    @property
    def net_tier(self):
        if self.discount_tier is not None:
            return self.discount_tier.api_display()
        elif self.price_tier is not None:
            return self.price_tier.api_display()
        else:
            return None

    @staticmethod
    def push(name, content_type, content, price_tier, discount_tier):
        apple = Apple.objects.filter(content_type=content_type, content=content).first()
        if apple is None:
            apple = Apple.objects.create(name=name,
                                         content_type=content_type,
                                         content=content,
                                         type=1,
                                         
                                         price_tier=price_tier,
                                         discount_tier=discount_tier,
                                         is_public=True)
        elif apple.price_tier_id != price_tier.id or apple.discount_tier != discount_tier:
            apple.price_tier = price_tier
            apple.discount_tier = discount_tier
            apple.save(update_fields=['price_tier', 'discount_tier'])
        return apple
    
    def api_display(self):
        from django.conf import settings
        d = {}
        d['id'] = self.id
        d['name'] = self.name
        d['content_type'] = settings.CONTENT_TYPE_ID(self.content_type_id).name
        d['content'] = self.content
        d['production_id'] = '%s.%s' % (d['content_type'], d['content'])
        d['price'] = self.price_tier.api_display()
        if self.discount_tier is not None:
            d['discount_tier'] = self.discount_tier.api_display()
        else:
            d['discount_tier'] = None
        return d

    def api_display_price(self):
        d = {
            'net': self.net
        }
        if self.price_tier is not None:
            d['price_tier'] = self.price_tier.api_display()
        else:
            d['price_tier'] = None
        if self.discount_tier is not None:
            d['discount_tier'] = self.discount_tier.api_display()
        else:
            d['discount_tier'] = None
        return d

    def api_display_price_tier(self):
        d = {
            'net_tier': self.net_tier
        }
        if self.price_tier is not None:
            d['price_tier'] = self.price_tier.api_display()
        else:
            d['price_tier'] = None
        if self.discount_tier is not None:
            d['discount_tier'] = self.discount_tier.api_display()
        else:
            d['discount_tier'] = None
        return d


class AppleResponse(models.Model):
    from django.conf import settings
    
    STATUS_CHOICES = (
        (-1, 'Fail'),
        (0, 'Wait'),
        (1, 'Success'),
        (2, 'Reject'),
    )

    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    apple = models.ForeignKey(Apple)
    client_response = models.TextField(blank=True)
    apple_response = models.TextField(blank=True)
    status = models.IntegerField(choices=STATUS_CHOICES)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']

    def receipt_validation(self, request, app=None, sandbox=False):
        from django.conf import settings
        import requests, base64, json, traceback
        payload = {'password': settings.PURCHASE_APPLE_PASSWORD,
                   'receipt-data': self.client_response}
        try:
            verify_url = settings.PURCHASE_APPLE_URL_SANDBOX if sandbox else settings.PURCHASE_APPLE_URL
            r = requests.post(verify_url, data=json.dumps(payload), verify=False)
            result = r.json()
            self.apple_response = json.dumps(result, indent=2)
            if result['status'] == 0:
                self.status = 1
                self.push_order(request, app)
            else:
                self.status = 2
        except:
            self.apple_response = traceback.format_exc()
            self.status = -1
        self.save(update_fields=['apple_response', 'status'])

    def push_order(self, request, app=None):
        from django.utils import timezone
        from order.models import Order, Item, Bill
        from mailer.models import Mailer
        from course.models import Course
        
        from course.cached import cached_course_video_sum_duration

        price = self.apple.price_tier.price_usd
        if self.apple.discount_tier is None:
            discount_price = 0
            net_price = self.apple.price_tier.price_usd
        else:
            discount_price = self.apple.discount_tier.price_usd
            net_price = discount_price

        order = Order(account=self.account,
                      price=price,
                      discount=discount_price,
                      net=net_price,
                      method=11,
                      status=3,
                      store=3,
                      app=app,
                      timecomplete=timezone.now())
        order.save()
        course = Course.pull(self.apple.content)
        item = Item.objects.create(order=order,
                                   account=self.account,
                                   content_type=self.apple.content_type,
                                   content=self.apple.content,
                                   amount=1,
                                   price=price,
                                   discount=discount_price,
                                   net=price-discount_price,
                                   is_free=False,
                                   status=2,
                                   total_duration=cached_course_video_sum_duration(course),
                                   credit=course.credit,
                                   expired=course.expired,
                                   is_display=True)
        # item.push()
        # bill = Bill.get_bill(order)
        # Mailer.send_billing_email(order, bill, request.get_host())
