from django.contrib import admin
from purchase.models import *


@admin.register(Apple)
class AppleAdmin(admin.ModelAdmin):
    list_display = ('name', 'content_type', 'content', 'type', 'price_tier', 'discount_tier', 'is_public')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'content_type':
            from utils.cached.content_type import cached_content_type_admin_item_inline
            kwargs["queryset"] = cached_content_type_admin_item_inline()
        return super(AppleAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_usd_price(self, obj):
        try:
            return obj.price_tier.price_usd
        except:
            return '-'
    get_usd_price.short_description = 'USD'

    def get_thb_price(self, obj):
        try:
            return obj.price_tier.price_thb
        except:
            return '-'
    get_thb_price.short_description = 'THB'


@admin.register(PriceMatrix)
class PriceMatrixAdmin(admin.ModelAdmin):
    list_display = ('tier', 'price_usd', 'price_thb')


@admin.register(AppleResponse)
class AppleResponseAdmin(admin.ModelAdmin):
    list_display = ('account', 'apple', 'status', 'timestamp')
    readonly_fields = ('account', 'apple')
