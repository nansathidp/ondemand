"""
from django.test import TestCase, Client, SimpleTestCase, LiveServerTestCase
from django.utils.html import strip_tags
from django.core.urlresolvers import reverse
from lesson.models import Lesson
from account.models import Account
from tag.models import Tag, Item
import json

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# Create your tests here.

class LessonPageTestCase ( TestCase ) :

    def setUp( self ):
        self.account_id = self.register( username = "kittitochp@gmail.com", password = "123456" )

    def login( self, username, password ):
        email = username
        password = password
        response = self.client.post('/account/login/', {
            'email' : email,
            'password' : password,
        })
        self.assertEquals( response.status_code == 200, True, "Can't login" )

    def verifyUser( self, account_id ):
        test_account = Account.objects.get( id = account_id )
        test_account.is_invited = True
        test_account.is_verify = True
        test_account.is_active = True
        test_account.is_admin = True
        test_account.save()

    def register( self, username, password ):
        self.email = username
        self.first_name = "Kittitoch"
        self.last_name = "Phuekphiphatmat"
        self.password = password
        self.birthday = "21/02/1992"
        self.gender = 1
        self.school_name = "saraburiwitthayakhon"
        self.school_id = "99"
        self.tel = "0869776857"
        self.response = self.client.post('/api/regis/', {
            'uuid' : "1111111111111",
            'email' : self.email,
            'first_name' : self.first_name,
            'last_name' : self.last_name,
            'password' : self.password,
            'birthday' : self.birthday,
            'gender' : self.gender,
            'school_name' : self.school_name,
            'school_id' : self.school_id,
            'tel' : self.tel
        })
        self.response_json = json.loads( self.response.content )
        self.assertEquals( self.response_json['status'] == 200, True, "Can't Regist" )
        self.assertEquals( self.response_json['status_msg'] == 'Success.', True, "Can't Regist" )
        return self.response_json['result']['profile'].get("id")

    def testEnterCreatePageWithVerify( self ):
        self.verifyUser( self.account_id )
        self.login( username = "kittitochp@gmail.com", password = "123456")
        response = self.client.get('/lesson/create/')
        self.assertEquals( response.status_code == 200, True, "Redirect to home" )
        self.assertIn( '<form action="/lesson/create/action" method="post" enctype="multipart/form-data" id="lesson">', response.content, "Wrong Page don't have form" )

    def testEnterCreatePageWithOutVerify( self ):
        self.login( username = "kittitochp@gmail.com", password = "123456")
        response = self.client.get('/lesson/create/')
        self.assertEquals( response.status_code == 302, True, "Not Redirect to home" )
        self.assertNotIn( '<form action="/lesson/create/action" method="post" enctype="multipart/form-data" id="lesson">', response.content, "It's have from!! Should redirect" )

    def testCreateNewLessonYoutube( self ):
        self.verifyUser( self.account_id )
        self.login( username = "kittitochp@gmail.com", password = "123456")
        #self.client = Client( enforce_csrf_checks = True )
        response = self.client.post( '/lesson/create/action', {
            "topic" : "test topic",
            "type" : 0,
            "level" : 0,
            "desc" : "test Description",
            "credit" : "test Credit",
            "video" : "https://www.youtube.com/watch?v=ihxeaCgYcpo",
            "addtag" : "",
            "deltag" : "",
        })
        self.assertEquals( response.status_code == 302, True, "Not Redirect" )
        latest_lesson = Lesson.objects.latest('pk')
        self.assertEquals( latest_lesson.name, "test topic" )
        self.assertEquals( latest_lesson.desc, "test Description" )
        self.assertEquals( latest_lesson.credit, "test Credit" )
        self.assertEquals( latest_lesson.video_type, 2 )
        self.assertEquals( latest_lesson.video, "ihxeaCgYcpo" )
        self.assertEquals( latest_lesson.lesson_type, 0 )
        self.assertEquals( latest_lesson.lesson_level, 0 )

    def testCreateNewLessonVimeo( self ):
        self.verifyUser( self.account_id )
        self.login( username = "kittitochp@gmail.com", password = "123456")
        response = self.client.post( '/lesson/create/action', {
            "topic" : "test topic vimeo",
            "type" : 0,
            "level" : 0,
            "desc" : "test Description vimeo",
            "credit" : "test Credit vimeo",
            "video" : "https://vimeo.com/channels/staffpicks/59859181",
            "addtag" : "",
            "deltag" : "",
        })
        self.assertEquals( response.status_code == 302, True, "Not Redirect" )
        latest_lesson = Lesson.objects.latest('pk')
        self.assertEquals( latest_lesson.name, "test topic vimeo" )
        self.assertEquals( latest_lesson.desc, "test Description vimeo" )
        self.assertEquals( latest_lesson.credit, "test Credit vimeo" )
        self.assertEquals( latest_lesson.video_type, 1 )
        self.assertEquals( latest_lesson.video, "59859181" )
        self.assertEquals( latest_lesson.lesson_type, 0 )
        self.assertEquals( latest_lesson.lesson_level, 0 )

    def testCreateNewLessonWithOutVideo( self ):
        self.verifyUser( self.account_id )
        self.login( username = "kittitochp@gmail.com", password = "123456")
        response = self.client.post( '/lesson/create/action', {
            "topic" : "test topic",
            "type" : 0,
            "level" : 0,
            "desc" : "test Description",
            "credit" : "test Credit",
            "video" : "",
            "addtag" : "",
            "deltag" : "",
        })
        self.assertEquals( response.status_code == 302, True, "Not Redirect" )
        latest_lesson = Lesson.objects.latest('pk')
        self.assertEquals( latest_lesson.name, "test topic" )
        self.assertEquals( latest_lesson.desc, "test Description" )
        self.assertEquals( latest_lesson.credit, "test Credit" )
        self.assertEquals( latest_lesson.video_type, 0 )
        self.assertEquals( latest_lesson.video, "" )
        self.assertEquals( latest_lesson.lesson_type, 0 )
        self.assertEquals( latest_lesson.lesson_level, 0 )

    def testCreateNewLessonWithOutVerify( self ):
        self.login( username = "kittitochp@gmail.com", password = "123456")
        response = self.client.post( '/lesson/create/action', {
            "topic" : "test topic vimeo",
            "type" : 0,
            "level" : 0,
            "desc" : "test Description vimeo",
            "credit" : "test Credit vimeo",
            "video" : "https://vimeo.com/channels/staffpicks/59859181",
            "addtag" : "",
            "deltag" : "",
        })
        self.assertEquals( response.status_code == 302, True, "Authenticated" )
        self.assertNotIn( '<form action="/lesson/create/action" method="post" enctype="multipart/form-data" id="lesson">', response.content, "It's have from!! Should redirect" )

    def testCreateLessonSetupWithOutLessonId( self ):
        response = self.client.get( '/lesson/create/setup' )
        response_json = json.loads( response.content )
        self.assertEquals( response_json['status'] == 1200, True, "Wrong Status Code" )
        self.assertEquals( response_json['status_msg'] == 'Lesson not Found', True, "Wrong Status Code" )
        level = [{"name": data[1], "val": data[0]} for data in Lesson.LEVEL]
        lesson_type = [{"name": data[1], "val": data[0]} for data in Lesson.LESSON_TYPE]
        self.assertEquals( response_json['result']['level'] == level, True, "Wrong level dict response" )
        self.assertEquals( response_json['result']['type'] == lesson_type, True, "Wrong type dict response" )

        from lesson.models import Category
        category = Category.objects.all()
        category_dict = {}
        for c in category:
            key = str(c.parent_id)
            if key in result["category"]:
                category_dict[key].append( { "name": c.name , "val":c.id } )
            else:
               category_dict[key] = [{ "name": c.name , "val":c.id } ]
               
        self.assertEquals( response_json['result']['category'] == category_dict, True, "Wrong Categroy Dict" )

    def testCreateLessonSetupWithLessonId( self ):
        lessonId = self.createSimpleLesson()
        response = self.client.post( '/lesson/create/setup', {
                "lesson_id" : lessonId
        })
        response_json = json.loads( response.content )
        self.assertEquals( response_json['status'] == 200, True, "Wrong Status Code" )
        self.assertEquals( response_json['status_msg'] == 'Success.', True, "Wrong Status Message" )
        level = [{"name": data[1], "val": data[0]} for data in Lesson.LEVEL]
        lesson_type = [{"name": data[1], "val": data[0]} for data in Lesson.LESSON_TYPE]
        self.assertEquals( response_json['result']['level'] == level, True, "Wrong level dict response" )
        self.assertEquals( response_json['result']['type'] == lesson_type, True, "Wrong type dict response" )

        from lesson.models import Category
        category = Category.objects.all()
        category_dict = {}
        for c in category:
            key = str(c.parent_id)
            if key in result["category"]:
                category_dict[key].append( { "name": c.name , "val":c.id } )
            else:
               category_dict[key] = [{ "name": c.name , "val":c.id } ]
               
        self.assertEquals( response_json['result']['category'] == category_dict, True, "Wrong Categroy Dict" )

        from lesson.models import Subscribe
        latest_subscribe = Subscribe.objects.latest('pk')
        self.assertEquals( latest_subscribe.account.id == self.account_id, True, "Wrong Accoutn Id on subscribe lesson" )

    def createSimpleLesson( self, addtag = None, deltag = None):
        self.verifyUser( self.account_id )
        self.login( username = "kittitochp@gmail.com", password = "123456")
        #self.client = Client( enforce_csrf_checks = True )
        response = self.client.post( '/lesson/create/action', {
            "topic" : "test topic",
            "type" : 0,
            "level" : 0,
            "desc" : "test Description",
            "credit" : "test Credit",
            "video" : "https://www.youtube.com/watch?v=ihxeaCgYcpo",
            "addtag" : addtag,
            "deltag" : deltag,
        })
        self.assertEquals( response.status_code == 302, True, "Not Redirect" )
        latest_lesson = Lesson.objects.latest('pk')
        latest_lesson.is_display = True
        latest_lesson.save()
        self.assertEquals( latest_lesson.name, "test topic" )
        self.assertEquals( latest_lesson.desc, "test Description" )
        self.assertEquals( latest_lesson.credit, "test Credit" )
        self.assertEquals( latest_lesson.video_type, 2 )
        self.assertEquals( latest_lesson.video, "ihxeaCgYcpo" )
        self.assertEquals( latest_lesson.lesson_type, 0 )
        self.assertEquals( latest_lesson.lesson_level, 0 )
        return latest_lesson.id

    def get_tagItems_and_tagObjects( self, lesson_id ):
        all_tag_item_of_lesson = Item.objects.filter( item_id = lesson_id )
        all_tag_name = list()
        for tag_item in all_tag_item_of_lesson:
            tag_obj = Tag.objects.get( id = tag_item.tag_id )
            all_tag_name.append( str(tag_obj) )
        return all_tag_item_of_lesson, all_tag_name

    def union( self, list_a, list_b ):
        return list( set(list_a) | set(list_b) )

    def testGetAddTagOfLesson( self ):
        all_addtag = "TAG1,TAG2"
        all_addtag_list = ["TAG1", "TAG2"]
        lessonId = self.createSimpleLesson( addtag = all_addtag )
        all_tag_item_of_lesson, all_tag_name = self.get_tagItems_and_tagObjects( lessonId )
        self.assertEquals( len(all_tag_item_of_lesson) == len(all_addtag_list), True, "Wrong Count Tag of lesson" )
        self.assertEquals( all_tag_name == all_addtag_list, True, "Tag list not match in lesson" )

        all_tag_objects = Tag.objects.all()         
        self.assertEquals( len(all_tag_objects) == 2, True, "All Tag not match in lesson" )

    def testGetAddTagOfLessonWithDelTag( self ):
        all_addtag = "TAG1,TAG2,TAG3"
        all_deltag = "TAG3"
        left_tag_list = ["TAG1", "TAG2"]
        lessonId = self.createSimpleLesson( addtag = all_addtag, deltag = all_deltag )
        all_tag_item_of_lesson, all_tag_name = self.get_tagItems_and_tagObjects( lessonId )
        self.assertEquals( len(all_tag_item_of_lesson) == len(left_tag_list), True, "Wrong Count Tag of lesson with deltag" )
        self.assertEquals( all_tag_name == left_tag_list, True, "Tag list not match in lesson with deltag" )

        all_tag_objects = Tag.objects.all()         
        self.assertEquals( len(all_tag_objects) == 3, True, "All Tag not match in lesson with deltag" )

    def testCreateLessonWithSomeSameTag( self ):
        all_addtag_firstlesson = "TAG1,TAG2,TAG3"
        left_tag_list_firstlesson = ["TAG1", "TAG2", "TAG3"]
        first_lessonId = self.createSimpleLesson( addtag = all_addtag_firstlesson )
        all_tag_item_of_firstlesson, all_tag_name_of_firstlesson = self.get_tagItems_and_tagObjects( first_lessonId )
        self.assertEquals( len(all_tag_item_of_firstlesson) == len(left_tag_list_firstlesson), True, "Wrong Count TagItem of firstlesson same tag" )
        self.assertEquals( all_tag_name_of_firstlesson == left_tag_list_firstlesson, True, "Tag list not match in firstlesson same tag" )

        all_addtag_secondlesson = "TAG2,TAG3,TAG4"
        left_tag_list_secondlesson = ["TAG2", "TAG3", "TAG4"]
        second_lessonId = self.createSimpleLesson( addtag = all_addtag_secondlesson ) 
        all_tag_item_of_secondlesson, all_tag_name_secondlesson = self.get_tagItems_and_tagObjects( second_lessonId )
        self.assertEquals( len(all_tag_item_of_secondlesson) == len(left_tag_list_secondlesson), True, "Wrong Count TagItem of secondlesson same tag" )
        self.assertEquals( all_tag_name_secondlesson == left_tag_list_secondlesson, True, "Tag list not match in secondlesson same tag" )

        all_tag_object = Tag.objects.all()
        tag_should_have = self.union( left_tag_list_firstlesson, left_tag_list_secondlesson )
        self.assertEquals( len(all_tag_object) == len(tag_should_have), True, "All Tag not match in lesson same tag" )

    def testCreateLessonWithSomeSameTagAndDelTag( self ):
        all_addtag_firstlesson = "TAG1,TAG2,TAG3,TAG4"
        all_deltag_firstlesson = "TAG4"
        left_tag_list_firstlesson = ["TAG1", "TAG2", "TAG3"]
        first_lessonId = self.createSimpleLesson( addtag = all_addtag_firstlesson, deltag = all_deltag_firstlesson )
        all_tag_item_of_firstlesson, all_tag_name_firstlesson = self.get_tagItems_and_tagObjects( first_lessonId )
        self.assertEquals( len(all_tag_item_of_firstlesson) == len(left_tag_list_firstlesson), True, "Wrong Count TagItem of firstlesson same tag del" )
        self.assertEquals( all_tag_name_firstlesson == left_tag_list_firstlesson, True, "Tag list not match in firstlesson same tag del" )

        all_addtag_secondlesson = "TAG3,TAG4,TAG5"
        all_deltag_secondlesson = "TAG3,TAG4"
        left_tag_list_secondlesson = ["TAG5"]
        second_lessonId = self.createSimpleLesson( addtag = all_addtag_secondlesson, deltag = all_deltag_secondlesson )
        all_tag_item_of_secondlesson, all_tag_name_secondlesson = self.get_tagItems_and_tagObjects( second_lessonId )
        self.assertEquals( len(all_tag_item_of_secondlesson) == len(left_tag_list_secondlesson), True, "Wrong Count TagItem of secondlesson same tag del" )
        self.assertEquals( all_tag_name_secondlesson == left_tag_list_secondlesson, True, "Tag list not match in secondlesson same tag del" )

        all_tag_objects = Tag.objects.all()
        tag_should_have = self.union( left_tag_list_firstlesson, left_tag_list_secondlesson )
        tag_should_have.append("TAG4")
        self.assertEquals( len(all_tag_objects) == len(tag_should_have), True, "All Tag not mtach in lesson same tag del" )

    def testEditLesson( self ):
        lessonId = self.createSimpleLesson()
        lesson_object = Lesson.objects.get( id = lessonId )
        response = self.client.get( '/lesson/'+ str(lessonId) +'/edit/' )
        self.assertEquals( response.status_code == 200, True, "Can't Enter Edit Page" )
        self.assertIn( 'value="' + str(lesson_object.name) + '" id="topic"', response.content, "Fill Topic Wrong" )
        self.assertIn( 'id="video" placeholder="Video URL" value="https://www.youtube.com/watch?v=' + str(lesson_object.video) + '"', response.content, "Wrong Page don't have form" )
        self.assertIn( str(lesson_object.desc), response.content, "Fill Topic Wrong" )
        self.assertIn( 'value="' + str(lesson_object.credit) + '"', response.content, "Fill Topic Wrong" )

    def testEditLessonAction( self ):
        lessonId = self.createSimpleLesson()
        response = self.client.post( '/lesson/'+ str(lessonId) +'/edit/action/', {
            "topic" : "Test Edit Action",
            "type" : 1,
            "level" : 1,
            "desc" : "Test Desciption Edit Action",
            "credit" : "Test Edit Action Credit",
            "video" : "https://www.youtube.com/watch?v=KsAHfrBr_4c",
            "addtag" : "",
            "deltag" : "",
        })
        lesson_object = Lesson.objects.get( id = lessonId )
        self.assertEquals( lesson_object.name, "Test Edit Action" )
        self.assertEquals( lesson_object.desc, "Test Desciption Edit Action" )
        self.assertEquals( lesson_object.credit, "Test Edit Action Credit" )
        self.assertEquals( lesson_object.video_type, 2 )
        self.assertEquals( lesson_object.video, "KsAHfrBr_4c" )
        self.assertEquals( lesson_object.lesson_type, 1 )
        self.assertEquals( lesson_object.lesson_level, 1 )

    def testEditLessonActionWithOutVerify( self ):
        lessonId = self.createSimpleLesson()
        self.account_id = self.register( username = "kittitochph@gmail.com", password = "123456" )
        self.login( username = "kittitochph@gmail.com", password = "123456")
        self.client = Client()
        response = self.client.post( '/lesson/'+ str(lessonId) +'/edit/action/', {
            "topic" : "Test Edit Action non verify",
            "type" : 1,
            "level" : 1,
            "desc" : "Test Desciption Edit Action",
            "credit" : "Test Edit Action Credit",
            "video" : "https://www.youtube.com/watch?v=KsAHfrBr_4c",
            "addtag" : "",
            "deltag" : "",
        })
        self.assertEquals( response.status_code == 302, True, "Authenticated" )
        lesson_object = Lesson.objects.get( id = lessonId )
        self.assertEquals( lesson_object.name != "Test Edit Action non verify", True, "It's Edit" )
        self.assertNotIn( '<form action="/lesson/create/action" method="post" enctype="multipart/form-data" id="lesson">', response.content, "It's have from!! Should redirect" )

    def testDeleteLesson( self ):
        lessonId = self.createSimpleLesson()
        response = self.client.get( '/lesson/' + str(lessonId) + '/delete/' )
        lesson_object = Lesson.objects.get( id = lessonId )
        self.assertEquals( response.status_code == 302, True, "Don't redirect to HTTPREFER or Home" )
        self.assertEquals( lesson_object.is_display == False, True, "It's Still display" )

    def testDeleteLessonWithOutVerify( self ):
        lessonId = self.createSimpleLesson()
        self.account_id = self.register( username = "kittitochph@gmail.com", password = "123456" )
        self.login( username = "kittitochph@gmail.com", password = "123456")
        self.client = Client()
        response = self.client.get( '/lesson/' + str(lessonId) + '/delete/' )
        lesson_object = Lesson.objects.get( id = lessonId )
        self.assertEquals( response.status_code == 302, True, "Don't redirect to Home" )
        self.assertEquals( lesson_object.is_display == True, True, "It don't display" )

    def testSubscribeWithLessonId( self ):
        lessonId = self.createSimpleLesson()
        response = self.client.post( '/lesson/subscribe/', {
            "lesson_id" : lessonId,
        })
        response_json = json.loads( response.content )
        self.assertEquals( response_json['status'] == 200, True, "Wrong Status Subscribe" )
        self.assertEquals( response_json['status_msg'] == 'Success.', True, "Wrong Status Subscribe" )
        lesson_object = Lesson.objects.get( id = lessonId )
        self.assertEquals( lesson_object.subscribe_set.all().count() == response_json['result']['subscribe_count'], True, "Wrong Subscribe Count" )

    def testUnsubscribeWithLessonId( self ):
        lessonId = self.createSimpleLesson()
        response = self.client.post( '/lesson/subscribe/', {
            "lesson_id" : lessonId,
        })
        response_json = json.loads( response.content )
        self.assertEquals( response_json['status'] == 200, True, "Wrong Status Subscribe" )
        self.assertEquals( response_json['status_msg'] == 'Success.', True, "Wrong Status Subscribe" )
        lesson_object = Lesson.objects.get( id = lessonId )
        self.assertEquals( lesson_object.subscribe_set.all().count() == response_json['result']['subscribe_count'], True, "Wrong Subscribe Count" )

        response_unsubscript = self.client.post( '/lesson/unsubscribe/', {
            "lesson_id" : lessonId,
        })
        response_json_unsub = json.loads( response_unsubscript.content )
        self.assertEquals( response_json_unsub['status'] == 200, True, "Wrong Status Unsubscribe" )
        self.assertEquals( response_json_unsub['status_msg'] == 'Success.', True, "Wrong Status Unsubscribe" )
        account = Account.objects.get( id = self.account_id )
        lesson_after_unsub = Lesson.objects.get( id = lessonId )
        try:
            lesson.subscribe_set.filter( account = account )
            self.assertRaises(ValidationError)
        except:
            pass

    def testCommentLesson( self ):
        lessonId = self.createSimpleLesson()
        response = self.client.post( '/lesson/'+ str(lessonId) +'/comment/add/', {
            'user_id' : self.account_id,
            'DESC': "test description"
        })
        response_json = json.loads( response.content )
        self.assertEquals( response_json['status'] == 200, True, "Wrong Status Comment" )
        self.assertEquals( response_json['status_msg'] == 'Success.', True, "Wrong Status Message Comment" )
        self.assertEquals( response_json['result'].has_key('template'), True, "Don't have result template key" )
        self.assertIn( 'test description', response_json['result']['template'], "Don't have description" )

    def testCommentLessonWithAccountNotAuthen( self ):
        lessonId = self.createSimpleLesson()
        self.client = Client()
        response = self.client.post( '/lesson/'+ str(lessonId) +'/comment/add/', {
            'user_id' : self.account_id,
            'DESC': "test description"
        })
        response_json = json.loads( response.content )
        self.assertEquals( response_json['status'] == 460, True, "Wrong Status Comment" )

    def testCommentLessonWithWrongLessonID( self ):
        lessonId = self.createSimpleLesson()
        lessonId = 0000000000
        response = self.client.post( '/lesson/'+ str(lessonId) +'/comment/add/', {
            'user_id' : self.account_id,
            'DESC': "test description"
        })
        response_json = json.loads( response.content )
        self.assertEquals( response_json['status'] == 700, True, "Wrong Status Comment" )

    def makeNewCategory( self, name, desc ):
        from lesson.models import Category
        newCategory = Category()
        newCategory.name = name
        newCategory.desc = desc
        newCategory.save()
        newest_cat = Category.objects.latest('pk')
        return newest_cat

    def testCategoryLesson( self ):
        lessonId = self.createSimpleLesson()
        first_cat = self.makeNewCategory( name="cat1", desc="desc1" )
        second_cat = self.makeNewCategory( name="cat2", desc="desc2" )
        third_cat = self.makeNewCategory( name="cat3", desc="desc3" )
        first_cat.parent = second_cat
        first_cat.save()
        third_cat.parent = second_cat
        third_cat.save()
        response = self.client.get( '/lesson/category/'+ str(second_cat.id) +'/' )
        response_json = json.loads( response.content )
        self.assertEquals( response_json['status'] == 200, True, "Wrong Status Category" )
        self.assertEquals( response_json['status_msg'] == 'Success.', True, "Wrong Status Message Category" )
"""
