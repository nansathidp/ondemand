from django.shortcuts import render, get_object_or_404

from lesson.models import Lesson
from .cached import cached_lesson


def home_view(request):
    highlight_list = CategoryHighlight.objects.filter(is_display=True).order_by('sort')[:10]
    for highlight in highlight_list:
        highlight.lesson_list = []
        for lesson_id in Lesson.objects.values_list('id', flat=True).filter(category=highlight.category, is_display=True)[:4]:
            lesson = cached_lesson(lesson_id)
            if lesson is not None:
                highlight.lesson_list.append(lesson)
                
    return render(request,
                  'lesson/home.html',
                  {'DISPLAY_SUBMENU': True,
                   'DISPLAY_CHAT': True,
                   'ROOT_PAGE': 'home_lesson',
                   'TITLE': 'Your Daily Learning Community',
                   'highlight_list': highlight_list})


def featured_view(request):
    highlight_menu_list = CategoryHighlight.objects.filter(is_display=True).order_by('sort')[:10]
    lesson_list = []
    for feature in Feature.objects.values('lesson_id').filter(lesson__is_display=True, lesson__status=2).order_by('sort')[:10]:
        lesson = cached_lesson(feature['lesson_id'])
        if lesson is not None:
            lesson_list.append(lesson)
    return render(request,
                  'lesson/category.html',
                  {'DISPLAY_SUBMENU': True ,
                   'DISPLAY_CHAT': True,
                   'ROOT_PAGE': 'home_lesson',
                   'TITLE': 'Your Daily Learning Community',
                   'highlight_menu_list': highlight_menu_list,
                   'lesson_list': lesson_list})


def category_view(request, highlight_id):
    highlight_menu_list = CategoryHighlight.objects.filter(is_display=True).order_by('sort')[:10]
    lesson_list = []
    highlight = get_object_or_404(CategoryHighlight, id=highlight_id)
    for d in Lesson.objects.values('id').filter(is_display=True, category_id=highlight.category_id, status=2).order_by('sort')[:100]:
        lesson = cached_lesson(d['id'])
        if lesson is not None:
            lesson_list.append(lesson)

    return render(request,
                  'lesson/category.html',
                  {'ROOT_PAGE': 'home_lesson',
                   'DISPLAY_SUBMENU': True,
                   'highlight_menu_list': highlight_menu_list,
                   'highlight': highlight,
                   'lesson_list': lesson_list})
