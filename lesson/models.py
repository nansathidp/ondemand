from django.db import models
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save


class Lesson(models.Model):
    LESSON_TYPE = (
        (0, 'Discuss'),
        (1, 'Workshop'),
        (2, 'Lecture & Workshop'),
        (3, 'Lecture'),
    )

    LEVEL = (
        (0, 'Beginner'),
        (1, 'Beginner, Intermediate'),
        (2, 'Intermediate'),
        (3, 'Intermediate , Expert'),
        (4, 'Expert'),
        (5, 'General'),
    )

    VIDEO_TYPE = (
        (0, 'None'),
        (1, 'Vimeo'),
        (2, 'Youtube'),
        (3, 'Upload'),
    )

    LESSON_STATUS = (
        (0, 'Waiting'),
        (1, 'Reject'),
        (2, 'Approved'),
    )

    name = models.CharField(max_length=120)
    image = models.ImageField(upload_to='lesson/image/')
    image_cover = models.ImageField(upload_to='lesson/image/cover/', null=True, blank=True)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='lesson_account')
    tutor = models.ForeignKey('tutor.Tutor', related_name='lesson_tutor', null=True, blank=True)
    category = models.ForeignKey('lesson.Category', related_name='lesson_category')
    desc = models.TextField(blank=True)
    sort = models.IntegerField(db_index=True, default=0)
    is_display = models.BooleanField(default=True, db_index=True)
    status = models.IntegerField(choices=LESSON_STATUS, default=0)

    lesson_type = models.IntegerField(choices=LESSON_TYPE, default=0)
    video_type = models.IntegerField(choices=VIDEO_TYPE, default=0)
    video = models.CharField(max_length=120, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    timeupdate = models.DateTimeField(auto_now=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')
        ordering = ['sort']

    def __str__(self):
        return self.name

    @staticmethod
    def pull(id):
        from .cached import cached_lesson
        return cached_lesson(id)
    
    def save(self, *args, **kwargs):
        from utils.models_image_delete import image_delete_pre_save
        from .cached import cached_lesson_update
        image_delete_pre_save(self)
        super(self.__class__, self).save(*args, **kwargs)
        cached_lesson_update(self)

    def delete(self, *args, **kwargs):
        from utils.models_image_delete import image_delete_pre_delete
        image_delete_pre_delete(self)
        super(self.__class__, self).delete(*args, **kwargs)

    def stat_view(self):
        from analytic.cached import cached_analytic_stat
        stat = cached_analytic_stat('lesson_%s' % self.id)
        try:
            return stat['stat'].value
        except:
            return 0

    def api_display_title(self, show_desc=False):
        d = {}
        d['id'] = self.id
        d['name'] = self.name
        d['desc'] = self.desc
        d['status'] = self.status
        d['stat_view'] = self.stat_view()
        d['tag_list'] = []
        d['link'] = self.get_absolute_url()
        d['desc'] = ''
        if self.tutor is None:
            d['account'] = self.account.api_display_title()
        else:
            d['account'] = {}
            d['account']['first_name'] = self.tutor.name
            d['account']['last_name'] = ''
            if bool(self.tutor.image):
                d['account']['image'] = self.tutor.image.url
            else:
                d['account']['image'] = '%s/static/images/profile_default.jpg' % settings.SITE_URL
            d['account']['email'] = self.tutor.name
            d['account']['id'] = self.tutor.id
            d['account']['title'] = ''
        if self.image:
            d['image'] = self.image.url
        else:
            d['image'] = ''
        return d

    def api_display(self, account=None):
        d = {}
        d['id'] = self.id
        d['name'] = self.name
        d['desc'] = self.desc
        d['video'] = self.video
        d['video_type'] = self.video_type
        d['video_type_display'] = self.get_video_type_display()
        d['status'] = self.status
        try:
            d['account'] = self.account.api_display_title()
        except:
            d['account'] = '%s/static/images/profile_default.jpg' % settings.SITE_URL
        try:
            d['tutor'] = self.tutor.api_display_title(is_display_provider=True)
        except:
            d['tutor'] = '%s/static/images/profile_default.jpg' % settings.SITE_URL
        try:
            d['category'] = self.category.api_display()
        except:
            d['category'] = None
        try:
            d['image'] = '%s%s' % (settings.SITE_URL, self.image.url)
        except:
            d['image'] = ''
        try:
            d['image_cover'] = '%s%s' % (settings.SITE_URL, self.image_cover.url)
        except:
            d['image_cover'] = ''

        d['comment_list'] = list()
        for comment in self.comment_set.select_related('account').filter(is_display=True):
            d['comment_list'].append(comment.api_display())

        d['image_list'] = []

        if account is not None:
            d['is_owner'] = self.account.id == account.id
            if self.subscribe_set.filter(account=account).count() == 0:
                d['is_subscribe'] = False
            else:
                d['is_subscribe'] = True
        else:
            d['is_subscribe'] = None
            d['is_owner'] = False

        d['subscribe_count'] = self.subscribe_set.all().count()
        d['comment_count'] = self.comment_set.all().count()
        d['timestamp'] = self.timestamp.strftime(settings.TIME_FORMAT)
        d['timeupdate'] = self.timeupdate.strftime(settings.TIME_FORMAT)
        d['stat_view'] = self.stat_view()
        d['tag_list'] = []
        return d

    def api_display_video_url(self):
        d = {}
        d['video_type_display'] = self.get_video_type_display()
        if self.video_type == 0:
            d['video_url'] = ''
        elif self.video_type == 1:
            d['video_url'] = '%s%s' % ('https://vimeo.com/', self.video)
        elif self.video_type == 2:
            d['video_url'] = '%s%s' % ('https://www.youtube.com/watch?v=', self.video)
        elif self.video_type == 3:
            d['video_url'] = self.video
        return d

    def is_subscribe(self, account):
        is_subscribe = False
        if account is not None:
            if self.subscribe_set.filter(account=account).count() > 0:
                is_subscribe = True
        return is_subscribe

    def api_related_by_timestamp(self, current_lesson_timestamp, count_related=4):
        lesson_related_timestamp_lt = Lesson.objects.filter(is_display=True).filter(
            timestamp__lt=current_lesson_timestamp)
        count_lesson_related_lt = lesson_related_timestamp_lt.count()
        if count_lesson_related_lt >= count_related:
            return lesson_related_timestamp_lt[:4]

        count_lesson_related_left = count_related - count_lesson_related_lt
        lesson_related_timestamp_gt = Lesson.objects.filter(is_display=True).filter(
            timestamp__gt=current_lesson_timestamp)
        if lesson_related_timestamp_gt >= count_lesson_related_left:
            count_lesson_related_timestamp_gt = lesson_related_timestamp_gt.count()
            wanted_lesson = lesson_related_timestamp_gt.reverse()[count_lesson_related_timestamp_gt - count_lesson_related_left:]
            lesson_related = lesson_related_timestamp_lt.reverse()
            return lesson_related

    def api_next_video(self, current_lesson_timestamp):
        return Lesson.objects.filter(is_display=True).filter(timestamp__lte=current_lesson_timestamp)[:1]

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('lesson:detail', args=[self.id])


class Category(models.Model):
    name = models.CharField(max_length=120)
    parent = models.ForeignKey('Category', null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    class meta:
        ordering = ['parent']

    def __str__(self):
        return self.name

    def api_display(self):
        return {
            'id': self.id,
            'name': self.name
        }

    def api_lesson_list(self, n=4):
        result = []
        for lesson in Lesson.objects.filter(category=self, is_display=True, status=2).order_by('sort')[:n]:
            result.append(lesson.api_display_title())
        return result

    def api_category_path(self):
        category_list = list()
        category_dict = {}
        for category in Category.objects.all():
            category_dict[category.id] = category
        next_object = self
        while True:
            category_list.append(next_object.api_display())
            next_object = next_object.parent
            if next_object is None:
                break
        category_list.reverse()
        return category_list


@receiver(pre_save, sender=Lesson)
def lesson_pre_save(sender, **kwargs):
    try:
        if kwargs['institute'].is_cached_mark:
            try:
                lesson = Lesson.objects.select_related('account', 'category', 'tutor').get(id=kwargs['instance'].id)
            except:
                lesson = None
    except:
        lesson = None
    try:
        sender.check_account_id = lesson.account.id
    except:
        sender.check_account_id = None

    try:
        sender.check_tutor_id = lesson.tutor.id
    except:
        sender.check_tutor_id = None

    try:
        sender.check_category_id = lesson.category.id
    except:
        sender.check_category_id = None


@receiver(post_save, sender=Lesson)
def lesson_post_save(sender, **kwargs):
    d = {}
    try:
        if kwargs['instance'].is_cached_mark:
            if sender.check_account_id != kwargs['instance'].account.id:
                d['cached_mark_account'] = True
    except:
        if sender.check_account_id is not None:
            d['cached_mark_account'] = True

    try:
        if kwargs['instance'].is_cached_mark:
            if sender.check_category_id != kwargs['instance'].category.id:
                d['cached_mark_category'] = True
    except:
        if sender.check_category_id is not None:
            d['cached_mark_category'] = True

    if d != {}:
        kwargs['instance'].__class__.objects.update(**d)
