from django.contrib import admin

from lesson.models import Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'timestamp')
    search_fields = ['name']

