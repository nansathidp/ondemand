from django.conf import settings
from django.core.cache import cache
from django.core.urlresolvers import reverse

from lesson.models import Lesson

from utils.cached.time_out import get_time_out
from ondemand.views_mobile import _redirect_url


def cached_lesson(lesson_id, is_force=False):
    key = '%s_lesson_%s' % (settings.CACHED_PREFIX, lesson_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Lesson.objects.select_related('account', 'tutor').get(id=lesson_id)
        except:
            result = -1
        cache.set(key, result, get_time_out())
    return None if result == -1 else result


def cached_lesson_update(lesson):
    key = '%s_lesson_%s' % (settings.CACHED_PREFIX, lesson.id)
    cache.set(key, lesson, get_time_out())


def cached_lesson_update_stat_view(lesson_id, stat_view):
    key = '%s_lesson_%s' % (settings.CACHED_PREFIX, lesson_id)
    result = cache.get(key)
    if result is None:
        cached_lesson(lesson_id, is_force=True)
    else:
        result.cached_stat_view = stat_view
        cache.set(key, result, get_time_out())

        
def cached_lesson_home(is_force=False):
    key = '%s_lesson_home_id' % (settings.CACHED_PREFIX)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Feature.objects.values_list('lesson_id', flat=True).filter(lesson__is_display=True, lesson__status=2).order_by('sort')[:10]
        cache.set(key, result, get_time_out())
    lesson_list = []
    for lesson_id in result:
        lesson = cached_lesson(lesson_id)
        if lesson is not None:
            lesson_list.append(lesson)
    return lesson_list


def cached_lesson_relate(lesson, is_force=False):
    key = '%s_lesson_relate_%s' % (settings.CACHED_PREFIX, lesson.id)
    result = None if is_force else cache.get(key)
    if result is None:
        if lesson.tutor is not None:
            result = Lesson.objects.values_list('id', flat=True).filter(is_display=True,
                                                                        tutor=lesson.tutor).exclude(id=lesson.id).order_by('?')[:5]
        else:
            result = Lesson.objects.values_list('id', flat=True).filter(is_display=True,
                                                                        category=lesson.category).exclude(id=lesson.id).order_by('?')[:5]
        cache.set(key, result, get_time_out())
    lesson_list = []
    for lesson_id in result:
        lesson = cached_lesson(lesson_id)
        if lesson is not None:
            lesson_list.append(lesson)
    return lesson_list


def cached_api_lesson(request, app, is_force=False):
    key = '%s_api_lesson_%s' % (settings.CACHED_PREFIX, app.id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Lesson.objects.values('id').filter(is_display=True, status=2, app__app=app).order_by('-timestamp')[:30]
        cache.set(key, result, get_time_out())
    lesson_list = []
    for d in result:
        lesson = cached_lesson(d['id'])
        if lesson is not None:
            lesson_display = lesson.api_display_title()
            lesson_display['url_detail'] = _redirect_url(request, reverse('lesson:detail', args=[lesson.id]))
            lesson_list.append(lesson_display)
    return lesson_list


def cached_api_v2_lesson(request, is_force=False):
    key = '%s_api_v2_lesson_%s' % (settings.CACHED_PREFIX, request.get_host())
    result = None if is_force else cache.get(key)
    if result is None:
        result = []
        for lesson in Lesson.objects.select_related('account').filter(is_display=True, status=2).order_by('timestamp')[:30]:
            lesson_display = lesson.api_display_title()
            lesson_display['url_detail'] = _redirect_url(request, reverse('lesson:detail', args=[lesson.id]))
            result.append(lesson_display)
        cache.set(key, result, get_time_out())
    return result
