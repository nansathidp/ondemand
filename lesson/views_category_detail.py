from django.shortcuts import render, redirect, get_object_or_404

from lesson.models import Category, Lesson

from .cached import cached_lesson


def category_detail_view(request, slug):
    category = Category.objects.filter(slug=slug).first()
    if category is None:
        return redirect('home')
    
    lesson_list = []
    for lesson_id in Lesson.objects.values_list('id', flat=True).filter(category=category):
        lesson = cached_lesson(lesson_id)
        if lesson is not None:
            lesson_list.append(lesson)
    
    return render(request,
                  'lesson/category_detail.html',
                  {'category': category,
                   'lesson_list': lesson_list,
                   'DISPLAY_SUBMENU': True,
                   'ROOT_PAGE': '%s' % category.slug})

