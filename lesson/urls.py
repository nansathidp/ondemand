from django.conf.urls import url

from .views import home_view, featured_view, category_view
from .views_category_detail import category_detail_view

from .views_detail import detail_view

from .views_manage import create_view, create_action_view, create_setup_view, delete_view, edit_view, edit_action_view

from .views_subscribe import subscribe_view, unsubscribe_view
from .views_comment import comment_view

app_name = 'lesson'
urlpatterns = [
    url(r'^$', home_view, name='home'), #TODO: testing
    url(r'^category/featured/$', featured_view, name='category_featured'), #TODO: testing
    url(r'^category/(\d+)/$', category_view, name='category'), #TODO: testing
    
    url(r'^(\d+)/$', detail_view, name='detail'), #TODO: testing
    url(r'^category/(?P<slug>[-\w\d]+)/$', category_detail_view, name='category_detail'), #TODO: testing

    url(r'^create/$', create_view, name='create'), #TODO: testing
    url(r'^create/action$', create_action_view, name='create_action'), #TODO: testing
    url(r'^create/setup$', create_setup_view, name='create_setup'), #TODO: testing
    url(r'^(\d+)/delete/$', delete_view, name='delete'), #TODO: testing
    url(r'^(\d+)/edit/$', edit_view, name='edit'), #TODO: testing
    url(r'^(\d+)/edit/action/$', edit_action_view, name='edit_action'), #TODO: testing

    # JS
    url(r'^subscribe/$', subscribe_view), #TODO: testing
    url(r'^unsubscribe/$', unsubscribe_view), #TODO: testing
    url(r'^(\d+)/comment/add/$', comment_view, name='comment_add'), #TODO: testing

]
