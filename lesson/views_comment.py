from django.template.loader import render_to_string
from django.views.decorators.http import require_POST

from api.views_api import json_render
from lesson.models import Lesson


@require_POST
def comment_view(request, lesson_id):
    result = {}
    code = 200
    if request.user.is_authenticated():
        desc = request.POST.get('desc', None)
        if request.user is None:
            return json_render(result, 400)
        try:
            lesson = Lesson.objects.get(id=int(lesson_id))
            comment = lesson.api_comment(request.user, desc)
            result['template'] = render_to_string('main/fragment/item_comment.html', {'comment': comment.api_display()})
        except Lesson.DoesNotExist:
            code = 700
    else:
        code = 460
    return json_render(result, code)
