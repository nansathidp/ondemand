from django.contrib.auth.decorators import permission_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect

from .forms import LessonForm

from utils.crop import crop_image

@permission_required('lesson.add_lesson', raise_exception=True)
def create_view(request):

    if request.method == 'POST':
        lesson_form = LessonForm(request.POST, request.FILES,
                                 initial={'account': request.user})
        if lesson_form.is_valid():
            lesson_form.cleaned_data['account'] = request.user
            lesson = lesson_form.save(commit=False)
            lesson.account = request.user
            lesson.save()
            crop_image(lesson, request)
            return redirect('dashboard:lesson-dashboard:home')
    else:
        lesson_form = LessonForm()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Blog & News Management',
         'url': reverse('dashboard:lesson-dashboard:home')},
        {'is_active': True,
         'title': 'Create'},
    ]
    return render(request, 'lesson/dashboard/create.html',
                  {'SIDEBAR': 'lesson',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'lesson_form': lesson_form})
