from django.core.exceptions import PermissionDenied
from django.shortcuts import render

from utils.paginator import paginator
from ..models import Lesson


def view(request):
    if not request.user.has_perm('lesson.view_lesson'):
        raise PermissionDenied

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Blog & News Management'}
    ]

    lesson_list = Lesson.objects.all().order_by('-timestamp')
    lesson_list = paginator(request, lesson_list)

    return render(request, 'lesson/dashboard/home.html',
                  {'SIDEBAR': 'lesson',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'lesson_list': lesson_list})