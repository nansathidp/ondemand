from django.contrib.auth.decorators import permission_required
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404

from ..models import Lesson


@permission_required('lesson.add_lesson', raise_exception=True)
def content_view(request, lesson_id):
    if not request.user.has_perm('course.view_course') and not request.user.has_perm('course.view_own_course'):
        raise PermissionDenied
    lesson = get_object_or_404(Lesson, id=lesson_id)

    if request.method == 'POST':
        lesson.desc = request.POST.get('desc', '')
        lesson.save(update_fields=['desc'])

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Blog & News Management',
         'url': reverse('dashboard:lesson-dashboard:home')
         },
        {'is_active': True,
         'title': 'Content'},
    ]

    return render(request, 'lesson/dashboard/content.html',
                  {'SIDEBAR': 'lesson',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'content',
                   'lesson': lesson,
                   })