# -*- coding: utf-8 -*-
from django import forms

from ..models import Lesson


class LessonForm(forms.ModelForm):

    class Meta:
        model = Lesson
        fields = ['name', 'image', 'image_cover', 'tutor', 'category', 'status']

        labels = {
            'tutor': 'Publisher',
        }

        help_texts = {
            'name': 'Limit at 60 characters',
            'image': 'Limit file size at 500 kb.',
            'image_cover': 'Limit file size at 500 kb.'
        }

    def __init__(self, *args, **kwargs):
        super(LessonForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({'type': 'file'})
