from django.conf.urls import url

from .views import view
from .views_create import create_view
from .views_information import information_view
from .views_content import content_view
from .views_report import report_view

app_name = 'lesson-dashboard'
urlpatterns = [
    url(r'^$', view, name='home'), #TODO: testing
    url(r'^create/$', create_view, name='create'), #TODO: testing

    url(r'^(\d+)/$', information_view, name='information'), #TODO: testing
    url(r'^(\d+)/content/$', content_view, name='content'), #TODO: testing
    url(r'^(\d+)/report/$', report_view, name='report'), #TODO: testing
]
