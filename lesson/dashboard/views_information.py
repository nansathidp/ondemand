from django.contrib.auth.decorators import permission_required
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404

from .forms import LessonForm
from ..models import Lesson
from ..cached import cached_lesson_update

from utils.crop import crop_image

@permission_required('lesson.add_lesson', raise_exception=True)
def information_view(request, lesson_id):
    if not request.user.has_perm('course.view_course') and not request.user.has_perm('course.view_own_course'):
        raise PermissionDenied
    lesson = get_object_or_404(Lesson, id=lesson_id)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Blog & News Management',
         'url': reverse('dashboard:lesson-dashboard:home')
         },
        {'is_active': True,
         'title': 'Information'},
    ]

    if request.method == 'POST':
        lesson_form = LessonForm(request.POST, request.FILES, instance=lesson)
        if lesson_form.is_valid():
            lesson = lesson_form.save()
            crop_image(lesson, request)
            cached_lesson_update(lesson)
    else:
        lesson_form = LessonForm(instance=lesson)

    return render(request, 'lesson/dashboard/information.html',
                  {'SIDEBAR': 'lesson',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'information',
                   'lesson_form': lesson_form,
                   'lesson': lesson,
                   })
