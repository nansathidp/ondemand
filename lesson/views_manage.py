from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.safestring import mark_safe
from django.views.decorators.http import require_POST

from api.views_api import json_render
from lesson.models import Lesson, Category
from .cached import cached_lesson_update


def lesson_update(request, lesson, is_create=False):
    if is_create:
        lesson.account_id = request.user.id
        lesson.status = 0

    lesson.name = request.POST.get('topic',None)
    lesson.category_id = request.POST.get('subcategory',None)
    lesson.lesson_language = request.POST.get('language', None)
    lesson.desc = mark_safe(request.POST.get('desc',None))
    lesson.credit = request.POST.get('credit',None)
    hours = request.POST.get('hours', 0)
    minutes = request.POST.get('minutes', 0)
    lesson.time = int(hours)*60 + int(minutes)
    video_url = request.POST.get('video',None)
    if video_url is not None:
        if  "youtube" in video_url:
            try:
                lesson.video = video_url.rsplit('v=',1)[1]
                lesson.video_type = 2
            except:
                lesson.video = ''
                lesson.video_type = 0

        elif "vimeo" in video_url:
            try:
                lesson.video = video_url.rsplit('/',1)[1]
                lesson.video_type = 1
            except:
                lesson.video = ''
                lesson.video_type = 0
        else:
            lesson.video = ''
            lesson.video_type = 0

    banner = request.FILES.get('banner',None)
    if banner is not None:
        lesson.image = banner
        lesson.image_cover = banner
    lesson.save()
    cached_lesson_update(lesson)

    addtag = request.POST.get("addtag",None).upper().split(",")
    deltag = request.POST.get("deltag",None).upper().split(",")


    slide = request.FILES.getlist('slide[]',None)
    if slide is not None:
        for s in slide:
            slideimg = SlideImage()
            slideimg.lesson_id = lesson.id
            slideimg.image = s
            slideimg.sort = 0
            slideimg.save()


@login_required
def create_view(request):
    return render(request,
                  'lesson/create.html',
                  {'lesson': None,
                   'category': None})

@login_required
def edit_view(request, lesson_id):
    lesson = get_object_or_404(Lesson,
                               id=lesson_id,
                               account=request.user)
    video_url = lesson.api_display_video_url()['video_url']
    return render(request,
                  'lesson/create.html',
                  {'lesson': lesson,
                   'category': lesson.category,
                   'video_url': video_url})


@require_POST
@login_required
def edit_action_view(request, lesson_id):
    lesson = get_object_or_404(Lesson,
                               id=lesson_id,
                               account=request.user)

    lesson_update(request, lesson)

    is_redirect = request.POST.get('redirect', None)
    if is_redirect == "0":
        return redirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('profile_lesson_owner')

@login_required
def delete_view(request,lesson_id):
    lesson = get_object_or_404(Lesson,
                               id=lesson_id,
                               account=request.user)
    lesson.is_display = 0
    lesson.save(update_fields=['is_display'])

    try:
        return redirect(request.META.get('HTTP_REFERER'))
    except:
        return redirect('profile_lesson_owner')

@require_POST
@login_required
def create_action_view(request):
    lesson = Lesson()
    lesson_update(request, lesson, True)

    is_redirect = request.POST.get('redirect', None)
    if is_redirect == "0":
        return redirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('profile_lesson_owner')


#TODO: check permission?
def create_setup_view(request):

    result = {
        #"level" : [{"name": e[1],"val": e[0]} for e in Lesson.LEVEL],
        #"type" : [{"name": e[1],"val": e[0]} for e in Lesson.LESSON_TYPE],
        "language" : [{"name": e[1],"val": e[0]} for e in Lesson.LANGUAGE],
        "category" : {}
    }

    category = Category.objects.all()
    for c in category:
        key = str(c.parent_id)
        if key in result["category"]:
            result["category"][key].append( { "name": c.name , "val":c.id } )
        else:
            result["category"][key] = [{ "name": c.name , "val":c.id } ]

    code = 200
    lesson_id = request.POST.get('lesson_id', None)
    if lesson_id is not None:
        lesson = Lesson.objects.get(id=lesson_id)
    else:
        code = 1200
    return json_render(result, code)
