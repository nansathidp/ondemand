from api.views_api import json_render
from lesson.models import Lesson


def unsubscribe_view(request):
    result = {}
    code = 200
    lesson_id = request.POST.get('lesson_id', None)
    if not request.user.is_authenticated():
        return json_render(result, 460)
    if lesson_id is not None:
        lesson = Lesson.objects.get(id=lesson_id)
        lesson.subscribe_set.filter(account=result.user).delete()
    else:
        code = 1200
    return json_render(result, code)


def subscribe_view(request):
    result = {}
    code = 200
    lesson_id = request.POST.get('lesson_id', None)
    if not request.user.is_authenticated():
        return json_render(result, 460)
    if lesson_id is not None:
        lesson = Lesson.objects.get(id=lesson_id)
        lesson.api_subscribe(request.user)
        result = {}
    else:
        code = 1200
    return json_render(result, code)


