from django.core.urlresolvers import reverse

from ondemand.views_mobile import _redirect_url
from .views import check_key, check_require
from .views import json_render

from ...models import Lesson


def list_view(request):
    result = {
        'lesson_list': []
    }
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render(result, code)

    catgory_id = request.GET.get('category', None)
    lesson_list = Lesson.objects.filter(is_display=True)
    if catgory_id is not None:
        lesson_list = lesson_list.filter(category_id=catgory_id)

    for lesson in lesson_list:
        _lesson = lesson.api_display_title()
        _lesson['url_detail'] = _redirect_url(request, reverse('lesson:detail', args=[lesson.id]))
        result['lesson_list'].append(_lesson)
    return json_render(result, code)
