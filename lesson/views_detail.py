from django.http import Http404
from django.shortcuts import render, redirect
from django.templatetags.static import static
from django.conf import settings
from django.core.urlresolvers import reverse

from analytic.models import Stat
from course.cached import cached_get_course_relate_rand
from ads.cached import cached_ads_list


from .cached import cached_lesson, cached_lesson_relate


def detail_view(request, lesson_id):
    unet = None
    if settings.PROJECT == 'unilever':
        unet = request.GET.get('unet', None)
        if request.user.is_authenticated():
            if unet is None:
                return redirect('%s?unet=%s' % (reverse('lesson:detail', args=[lesson_id]), request.user.info))

    lesson = cached_lesson(lesson_id)
    if lesson is None:
        raise Http404("Lesson does not exist")

    Stat.push('lesson_%s' % lesson.id, request.META.get('HTTP_REFERER', '-'), request.META.get('HTTP_USER_AGENT', ''))

    is_subscribe = False
    source = request.GET.get('source', None)

    relate_lesson_list = cached_lesson_relate(lesson)
    if lesson.tutor:
        relate_course_list = cached_get_course_relate_rand(3, lesson.tutor)
    else:
        relate_course_list = cached_get_course_relate_rand(3)

    if source is not None and source == "ais":
        logo_path = static('images/logo/logo-aisu.png')
    else:
        logo_path = None

    if settings.PROJECT == 'unilever':
        ROOT_PAGE = '%s' % lesson.category.slug
        relate_ads_list = cached_ads_list()
    else:
        ROOT_PAGE = 'lesson'
        relate_ads_list = []

    return render(request,
                  'lesson/detail.html',
                  {'TITLE': lesson.name,
                   'IMAGE_URL': lesson.image.url if bool(lesson.image) else None,
                   'DISPLAY_SUBMENU': True,
                   'DISPLAY_EVENT_HEADER': True,
                   'DESC': lesson.desc,
                   'ROOT_PAGE': ROOT_PAGE,
                   'lesson': lesson,
                   'comment_list': [],
                   'relate_lesson_list': relate_lesson_list,
                   'relate_course_list': relate_course_list,
                   'relate_ads_list': relate_ads_list,
                   'logo_path': logo_path,
                   'unet_account': unet,
                   'is_subscribe': is_subscribe})
