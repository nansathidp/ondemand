from django.db import models


class Parent(models.Model):
    """
    Keep One Level
    """
    from django.conf import settings

    parent = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='org_parent_set')
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='org_parent_account_set')


class Child(models.Model):
    """
    Keep All Level
    """
    from django.conf import settings

    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='org_child_account_set')
    child = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='org_child_set')

    @staticmethod
    def _push(account, child):
        if Child.objects.filter(
            account=child,
            child=account
        ).exists():
            return False
        _ = Child.objects.filter(
            account=account,
            child=child
        ).first()
        if _ is None:
            Child.objects.create(
                account=account,
                child=child
            )
        return True
            
    @staticmethod
    def push(account, child_list, child=None):
        from django.conf import settings

        if account == child:
            return False
        if child is None:
            parent_list = Parent.objects.select_related(
                'account'
            ).filter(parent=account)
            count = 0
            for _ in parent_list:
                count += 1
                is_found = False
                for child in child_list:
                    if _.account_id == child.child_id:
                        is_found = True
                        child_list.remove(child)
                        break
                if not is_found:
                    check = Child._push(account, _.account)
                    if check is False:
                        return False
                Child.push(account, child_list, _.account)
        else:
            parent_list = Parent.objects.select_related(
                'account'
            ).filter(parent=child)
            for _ in parent_list:
                is_found = False
                for child in child_list:
                    if _.account_id == child.child_id:
                        is_found = True
                        child_list.remove(child)
                        break
                if not is_found:
                    check = Child._push(account, _.account)
                    if check is False:
                        return False
                Child.push(account, child_list, _.account)
        return True
