from django.contrib import admin

from .models import Parent, Child

@admin.register(Parent)
class ParentAdmin(admin.ModelAdmin):
    list_display = ('parent', 'account')
    readonly_fields = ('parent', 'account')
    
@admin.register(Child)
class ChildAdmin(admin.ModelAdmin):
    list_display = ('account', 'child')
    readonly_fields = ('account', 'child')
