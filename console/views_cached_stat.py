from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404

from memcached_stats import MemcachedStats

@login_required
def cached_stat_view(request):
    if not request.user.is_admin:
        raise Http404
    mem = MemcachedStats()
    stats = mem.stats()
    return render(request,
                  'console/cached_dashboard.html',
                  {'MENU': 'CACHED',
                   'stats': stats})
