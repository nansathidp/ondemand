from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404

from account.models import Account

@login_required
def account_move_view(request):
    if not request.user.is_admin:
        raise Http404
    
    msg = None
    account_from = None
    account_to = None
    if request.method == 'POST':
        try:
            account_from = Account.objects.get(id=request.POST.get('from', -1))
        except:
            msg = 'Account From Not Found.'
        try:
            account_to = Account.objects.get(id=request.POST.get('to', -1))
        except:
            msg = 'Account To Not Found.'
        if msg is None:
            if 'move' in request.POST:
                links = [rel.get_accessor_name() for rel in account_from._meta.get_all_related_objects()]
                for link in links:
                    #print '-------------- \n %s'%link
                    try:
                        objects = getattr(account_from, link).all()
                        for object in objects:
                            object.account = account_to
                            object.save(update_fields=['account'])
                    except:
                        pass
                        #try:
                        #    object = getattr(account_from, link)
                        #except:
                        #    pass
                msg = -1
            else:
                msg = -2
    return render(request,
                  'console/account_move.html',
                  {'msg': msg,
                   'account_from': account_from,
                   'account_to': account_to})
