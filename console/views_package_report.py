from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.conf import settings

#from package.models import Package
from order.models import Item
from course.models import Course
# from progress.models import Course as ProgressCourse

import datetime

@login_required
def package_report_view(request, package_id):
    if not request.user.is_admin:
        raise Http404
    
    package = get_object_or_404(Package, id=package_id)
    content_type_package = settings.CONTENT_TYPE('package', 'package')
    item_list = Item.objects.select_related('order').filter(content_type=content_type_package,
                                                            content=package.id,
                                                            order__status=3)
    
    course_list = {}
    #for progress_course in ProgressCourse.objects.filter(item__in=item_list):
    #    if progress_course.content_id in course_list:
    #        d = course_list[progress_course.content_id]
    #        d['item_count'] += 1
    #        d['credit_sum'] += progress_course.credit
    #    else:
    #        d = {}
    #        d['item'] = Course.pull(progress_course.content_id)
    #        d['item_count'] = 1
    #        d['credit_sum'] = progress_course.credit
    #    course_list[progress_course.content_id] = d

    tutor_dict = {}
    for key, course in course_list.items():
        cached = course['item']
        for tutor in cached.tutor.all():
            if tutor.id in tutor_dict:
                tutor_dict[tutor.id]['course_list'].append(course['item'].id)
            else:
                tutor_dict[tutor.id] = {'name': tutor.name,
                                        'sum': 0.0,
                                        'course_list': [course['item'].id]}
    total_sell = 0.0
    total_sell_start = 0.0
    total_credit = datetime.timedelta(0)
    for item in item_list:
        total_credit += item.use_credit
        total_sell += item.get_net_price()
        if item.start is not None:
            total_sell_start += item.get_net_price()
    try:
      avg_time = datetime.timedelta(seconds=total_credit.total_seconds() / len(item_list))
    except:
      avg_time = datetime.timedelta(0)

    for item in item_list:
        progress_course_cal_list = item.course_set.all()
        use_credit = datetime.timedelta(0)
        for progress_course in progress_course_cal_list:
            use_credit += progress_course.credit
        for progress_course in progress_course_cal_list:
            try:
                percent = (progress_course.credit.total_seconds()/use_credit.total_seconds()) * 100.0
            except:
                percent = 0
            
            for tutor_id, tutor in tutor_dict.items():
                for tutor_course in tutor['course_list']:
                    if tutor_course == progress_course.content_id:
                        d = {'id': item.order.id,
                             'course': Course.pull(progress_course.content_id),
                             'credit': progress_course.credit,
                             'percent': percent,
                             'bath': item.get_net_price()*percent/100.0}
                        tutor['sum'] += d['bath']
                        if total_sell_start is 0:
                            tutor['percent'] = (tutor['sum'] / total_sell_start) * 100.0
                        else:
                            tutor['percent'] = 0

                        if not 'order_list' in tutor:
                            tutor['order_list'] = []
                        tutor['order_list'].append(d)
    return render(request,
                  'console/package_report.html',
                  {'package': package,
                   'total_sell': total_sell,
                   'total_sell_start': total_sell_start,
                   'total_credit': total_credit,
                   'avg_time': avg_time,
                   'item_list': item_list,
                   'course_list': course_list,
                   'tutor_dict': tutor_dict})
