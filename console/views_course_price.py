from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404

from course.models import Course
from purchase.models import Apple


@login_required
def price_ios_view(request):
    if not request.user.is_admin:
        raise Http404
    course_id = [apple.content for apple in Apple.objects.filter(price_tier__tier__gt=0)]
    course_list = Course.objects.filter(is_display=True, id__in=course_id)
    if request.method == 'POST':
        for course in course_list:
            if course.reference_name != request.POST['course-%s' % course.id]:
                course.reference_name = request.POST['course-%s' % course.id]
                course.is_cached_mark = False
                course.save(update_fields=['reference_name'])
    return render(request, 'console/course_price_ios.html', {'course_list': course_list})
