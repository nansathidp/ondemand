from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404

from course.models import Course


@login_required
def document_view(request):
    if not request.user.is_admin:
        raise Http404
    course_list = Course.objects.prefetch_related('material_set').all()
    return render(request,
                  'console/document.html',
                  {'MENU': 'video',
                   'course_list': course_list})
