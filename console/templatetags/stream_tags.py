from django import template

register = template.Library()

@register.inclusion_tag('console/templatetags/stream.html')
def stream_tag(material, stream_list):
    result = []
    for stream in stream_list:
        if stream.material_id == material.id:
            result.append(stream)
    return {'stream_list': result}
