from django import template
from django.conf import settings

register = template.Library()

@register.inclusion_tag('console/cached_update_tag.html')
def cached_time_tag(app, cached_key, key_list):
    keys = cached_key.get_key(app)
    #print '>> %s'%keys
    html_list = []
    for key in keys:
        t = -1
        for k in key_list:
            if k['key'] == key:
                t = k['time']
                break
        if t == -1:
            html = 'Not Found.'
        elif t < 0:
            html = 'timeout.'
        else:
            html = ''
            h, m = divmod(t, 60*60)
            if h > 0:
                html += str('%s h'%h).rjust(4)
                t = m
            m, s = divmod(t, 60)
            if m > 0 or len(html) > 0:
                html += str(' %s m'%m).rjust(5)
                t = s
            html += str('%s s.'%s).rjust(6)
        html_list.append(html)
    return {'html_list' : html_list}
