from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.db.models import Sum

# from progress.models import Video, Log
from order.models import Item

from order.cached import cached_order_item_update

@login_required
def progress_view(request):
    if not request.user.is_admin:
        raise Http404

    material_list = []
    video_list = []
    item_id = ''
    item = None
    if request.method == 'POST':
        try:
            item_id = int(request.POST.get('item', None))
            try:
                item = Item.objects.get(id=item_id)
            except:
                item = None

            if 'cal' in request.POST:
                #item.use_credit = Log.objects.filter(item_id=item.id).aggregate(Sum('credit_use'))['credit_use__sum']
                item.is_active(True)
                item.save(update_fields=['use_credit'])
                cached_order_item_update(item)
            if item_id != -1:
                pass
                #material_list = Video.objects.filter(item_id=item_id)
                #for material in material_list:
                #    material.credit_use = Log.objects.filter(item_id=item_id, material=material.material).aggregate(Sum('credit_use'))['credit_use__sum']
        except:
            pass
    return render(request,
                  'console/progress.html',
                  {'material_list': material_list,
                   'item_id': item_id,
                   'item': item})

@login_required
def progress_log_view(request, item_id, material_id):
    if not request.user.is_admin:
        raise Http404

    log_list = []
    # log_list = Log.objects.filter(item_id=item_id, material_id=material_id)
    return render(request,
                  'console/progress_log.html',
                  {'log_list': log_list})

@login_required
def progress_delete_log_view(request, item_id, video_id, log_id):
    if not request.user.is_admin:
        raise Http404

    try:
        pass
        #Log.objects.get(id=log_id).delete()
    except:
        pass
    return redirect('console:progress_log', int(item_id), int(video_id))
