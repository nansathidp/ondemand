from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.http import Http404

from course.models import Course, Material
from stream.models import Stream

import datetime


@login_required
def video_view(request):
    if not request.user.is_admin:
        raise Http404
    course_list = Course.objects.prefetch_related('material_set').all()
    stream_list = Stream.objects.select_related('config').all()
    return render(request,
                  'console/video.html',
                  {'MENU': 'video',
                   'course_list': course_list,
                   'stream_list': stream_list})


@login_required
def video_preview_view(request, material_id):
    if not request.user.is_admin:
        raise Http404
    type = request.GET.get('type', None)
    material = get_object_or_404(Material, id=material_id)
    # url = signed_url('/'.join(material.data.split('.')[:-1]))
    url = '/'.join(material.data.split('.')[:-1])
    if material.video_type == 2:
        template = 'console/video_preview.html'
    else:
        template = 'console/video_preview_jw.html'
    return render(request,
                  template,
                  {'MENU': 'video',
                   'video': material,
                   'type': type,
                   'url': url})


@csrf_exempt
@login_required
def video_duration_view(request, material_id):
    if not request.user.is_admin:
        raise Http404

    material = get_object_or_404(Material, id=material_id)
    if request.method == 'POST':
        duration = request.POST.get('duration')
        try:
            material.video_duration = datetime.timedelta(seconds=int(float(duration)), milliseconds=int((float(duration)*1000.0)%1000))
            material.save(update_fields=['video_duration'])
        except:
            pass
    return HttpResponse('ok.')


@csrf_exempt
@login_required
def video_verify_view(request, material_id):
    if not request.user.is_admin:
        raise Http404

    material = get_object_or_404(Material, id=material_id)
    if request.method == 'POST':
        try:
            material.is_verify_pass = True
            material.save(update_fields=['video_is_verify'])
        except:
            pass
    return HttpResponse('Ok.')
