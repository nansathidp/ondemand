from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404

@login_required
def home_view(request):
    if not request.user.is_admin:
        raise Http404
    return render(request,
                  'console/dashboard.html',
                  {})
