from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404

import django_rq

@login_required
def rq_view(request):
    if not request.user.is_superuser:
        raise Http404

    if request.method == 'POST':
        django_rq.enqueue(request.POST.get('job'))

    return render(request,
                  'console/rq.html',
                  {})
