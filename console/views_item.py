from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404

from order.models import Item


@login_required
def item_expired_view(request):
    if not request.user.is_admin:
        raise Http404
    item_list = Item.objects.filter(is_free=False, status=3).order_by('-timestamp')[:50]
    return render(request,
                  'console/item_list.html',
                  {'MENU': 'ITEM',
                   'item_list': item_list,
                   })


@login_required
def item_will_expired_view(request):
    if not request.user.is_admin:
        raise Http404
    item_list = Item.objects.filter(is_free=False, status=2).order_by('-use_credit')[:50]
    return render(request,
                  'console/item_list.html',
                  {'MENU': 'ITEM',
                   'item_list': item_list,
                   })


@login_required
def item_notstart_view(request):
    if not request.user.is_admin:
        raise Http404
    item_list = Item.objects.filter(is_free=False, status=2,start=None).order_by('-order__timecomplete')[:50]
    return render(request,
                  'console/item_list_with_user.html',
                  {'MENU': 'ITEM',
                   'item_list': item_list,
                   })
