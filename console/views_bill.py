from operator import attrgetter
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.conf import settings

from order.models import Order, Bill, BankTranferRecord
from price.models import Price

import datetime


@login_required
def bill_table_view(request):
    if not request.user.is_admin:
        raise Http404
    year = None
    month = None
    bill_list_all_display = None
    sum_all_price = 0
    sum_all_vat = 0
    sum_all_net = 0
    bill_list_apple_display = None
    sum_apple_price = 0
    sum_apple_vat = 0
    sum_apple_net = 0
    bill_list_mpay_display = None
    sum_mpay_price = 0
    sum_mpay_vat = 0
    sum_mpay_net = 0
    year_range = range(2015, (datetime.datetime.now().year + 1))

    if request.method == 'POST':
        month = int(request.POST.get("month"))
        year = int(request.POST.get("year"))
        # ALL
        bill_list_all = _get_bill_list(month, year, None, [6, 11])
        bill_list_all_display = _get_bill_list_display_table(bill_list_all)
        sum_all_price, sum_all_vat, sum_all_net = _get_sum_of_value(bill_list_all_display)

        # APPLE
        bill_list_apple = _get_bill_list(month, year, [11], None)
        bill_list_apple_display = _get_bill_list_display_table(bill_list_apple)
        sum_apple_price, sum_apple_vat, sum_apple_net = _get_sum_of_value(bill_list_apple_display)

        # MPay
        bill_list_mpay = _get_bill_list(month, year, [6], None)
        bill_list_mpay_display = _get_bill_list_display_table(bill_list_mpay)
        sum_mpay_price, sum_mpay_vat, sum_mpay_net = _get_sum_of_value(bill_list_mpay_display)
    return render(request, 'console/report/report_bill_table.html',
                  {
                      'MENU': 'REPORT',
                      'year_range': year_range,
                      'bill_year': year,
                      'bill_month': month,

                      'bill_all_list': bill_list_all_display,
                      'sum_all_price': sum_all_price,
                      'sum_all_vat': sum_all_vat,
                      'sum_all_net': sum_all_net,

                      'bill_apple_list': bill_list_apple_display,
                      'sum_apple_price': sum_apple_price,
                      'sum_apple_vat': sum_apple_vat,
                      'sum_apple_net': sum_apple_net,

                      'bill_mpay_list': bill_list_mpay_display,
                      'sum_mpay_price': sum_mpay_price,
                      'sum_mpay_vat': sum_mpay_vat,
                      'sum_mpay_net': sum_mpay_net
                  })


@login_required
def bill_view(request):
    if not request.user.is_admin:
        raise Http404
    year = None
    month = None
    order_list_display = None
    year_range = range(2015, (datetime.datetime.now().year + 1))
    now = datetime.datetime.now()
    start = request.GET.get("start", 0)
    today = "%s %s %s" % (now.day, _get_text_month(now.month), now.year + 543)
    if request.method == 'GET':
        month = request.GET.get("month", 1)
        year = request.GET.get("year", 2015)
        month = int(month)
        year = int(year)
        order_list, dic_billing = _get_order_list(month, year)
        order_list_display = _get_order_list_display(order_list, dic_billing)
    return render(request,
                  'console/report/report_per_bill.html',
                  {'MENU': 'REPORT',
                   'year_range': year_range,
                   'order_year': year,
                   'order_month': month,
                   'today': today,
                   'start': int(start),
                   'order_list': order_list_display})


def _get_order_list(month, year):
    date_gte, date_lt = _get_filter_date(month, year)
    order_id_list = []
    dic_billing = {}
    for bill in Bill.objects.filter(timestamp__gte=date_gte, timestamp__lt=date_lt).order_by('no'):
        order_id_list.append(bill.order.id)
        dic_billing[bill.order.id] = bill
    order_list = Order.objects.filter(id__in=order_id_list).order_by('timecomplete')
    return order_list, dic_billing


def _get_order_list_display(order_list, dic_billing):
    order_list_display = []
    order_list_sort = []
    count = 0
    for order in order_list:
        if order.method == 11:
            order.net = order.net * Price.EXCHANGE_RATE
        order.vat = round(order.net * 7 / 107, 2)
        order.price = order.net - order.vat
        order.time = "%s %s %s" % (order.timecomplete.day, _get_text_month(order.timecomplete.month), order.timecomplete.year + 543)
        order.address = order.account.address_set.first()
        order.information_list = _get_order_information(order)
        order.no = dic_billing[order.id].no
        order.complete = dic_billing[order.id].timestamp
        order_list_display.append(order)
    for order in sorted(order_list_display, key=attrgetter('no')):
        order.page_break = True if count % 2 == 0 or len(order.information_list) > 6 else False
        order_list_sort.append(order)
        count += 1
    return order_list_sort


def _get_order_information(order):
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_package = settings.CONTENT_TYPE('package', 'package')
    content_type_coin = settings.CONTENT_TYPE('coin', 'coin')
    order_item_list = order.item_set.all()
    order_information_list = []
    for order_item in order_item_list:
        order_information_list.append({'name': order_item.content_name,
                                       'tutor_name': '',
                                       'price': _get_order_item_price(order_item),
                                       'item': order_item})
    return order_information_list


def _get_order_item_price(order_item):
    if order_item.discount == -1.0:
        return order_item.price - round(order_item.price * 7 / 107, 2)
    else:
        return order_item.discount - round(order_item.discount * 7 / 107, 2)


def _get_bill_list(month, year, include_method, exclude_method):
    date_gte, date_lt = _get_filter_date(month, year)
    if include_method is not None:
        bill_list = Bill.objects.filter(timestamp__gte=date_gte, timestamp__lt=date_lt,
                                        order__method__in=include_method).order_by('timestamp')
    elif exclude_method is not None:
        bill_list = Bill.objects.filter(timestamp__gte=date_gte, timestamp__lt=date_lt).exclude(
            order__method__in=exclude_method).order_by('timestamp')
    else:
        bill_list = Bill.objects.filter(timestamp__gte=date_gte, timestamp__lt=date_lt).order_by('timestamp')
    return bill_list


def _get_bill_list_display_table(bill_list):
    bill_list_display = []
    for bill in bill_list:
        bill.no = str(bill.no).rjust(11, '0')
        bill.vat = bill.get_vat()
        bill.price = bill.get_price()
        bill.net = bill.get_net()
        bill.method = bill.order.method
        bill.method_display = bill.order.get_method_display()
        if bill.order.method == 4:
            try:
                bill.record = BankTranferRecord.objects.filter(order__id=str(bill.order.id))[0]
            except:
                pass
        bill_list_display.append(bill)
    return bill_list_display


def _get_sum_of_value(bill_list_display):
    sum_price = 0
    sum_vat = 0
    sum_net = 0
    for bill in bill_list_display:
        sum_price += bill.get_price()
        sum_vat += bill.get_vat()
        sum_net += bill.get_net()
    return sum_price, sum_vat, sum_net


def _get_filter_date(month, year):
    gte_month = month
    if month < 10:
        gte_month = '0' + str(month)
    date_gte = "%s-%s-01" % (year, gte_month)
    lt_month = month + 1
    if lt_month > 12:
        lt_month = '01'
        year = year + 1
    if lt_month < 10:
        lt_month = '0' + str(lt_month)
    date_lt = "%s-%s-01" % (year, lt_month)
    return date_gte, date_lt


def _get_text_month(month):
    month_list = {
        1: "ม.ค.",
        2: "ก.พ.",
        3: "มี.ค.",
        4: "เ.ม.ย",
        5: "พ.ค.",
        6: "มิ.ย.",
        7: "ก.ค.",
        8: "ส.ค.",
        9: "ก.ย.",
        10: "ต.ค.",
        11: "พ.ย.",
        12: "ธ.ค.",
    }
    return month_list[month]
