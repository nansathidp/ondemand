from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import Http404

from order.models import Order
# from progress.models import Course as ProgressCourse

@login_required
def order_progress_view(request, order_id):
    if not request.user.is_admin:
        raise Http404

    order = get_object_or_404(Order, id=order_id)
    progress_course_list = []
    # progress_course_list = ProgressCourse.objects.select_related('content').prefetch_related('video_set').filter(item__order=order)
    return render(request,
                  'console/order_progress.html',
                  {'MENU': 'ORDER',
                   'progress_course_list': progress_course_list})
