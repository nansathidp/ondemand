from django.shortcuts import render, get_object_or_404
from api.views_ais_privilege import get_client_ip
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from stream.models import Stream


@login_required
def video_stream_preview_wowza_view(request, stream_id):
    if not request.user.is_admin:
        raise Http404

    type = request.GET.get('type', None)
    stream = get_object_or_404(Stream, id=stream_id)
    stream_url = stream.get_url(get_client_ip(request))
    return render(request,
                  'console/video_preview_stream_hls.html',
                  {'stream': stream,
                   'stream_url': stream_url,
                   'type': type})


@csrf_exempt
@login_required
def video_stream_verify_view(request, stream_id):
    if not request.user.is_admin:
        raise Http404

    stream = get_object_or_404(Stream, id=stream_id)
    if request.method == 'POST':
        try:
            stream.is_verify_pass = True
            stream.save(update_fields=['is_verify_pass'])
        except:
            pass
    return HttpResponse('Ok.')
