from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.core.cache import cache

@login_required
def cached_detail_view(request):
    if not request.user.is_admin:
        raise Http404

    key = request.GET.get('key', None)
    result = cache.get(key)
    return render(request,
                  'console/cached_detail.html',
                  {'MENU': 'CACHED',
                   'result': result})
