from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404

from order.models import Order
import datetime


@login_required
def order_view(request):
    if not request.user.is_admin:
        raise Http404
    price = 0
    duration = datetime.timedelta(0)
    order_list = Order.objects.filter(net__gt=0.0,
                                      method__in=[1, 2, 3, 4, 5, 6, 7, 8, 9],
                                      status=3).order_by('-timecomplete')[:200]
    for order in order_list:
        price += order.net
        for item in order.item_set.all():
            duration += item.use_credit
    total_duration = datetime.timedelta(seconds=duration.total_seconds())
    try:
        time_per_order = datetime.timedelta(seconds=(duration.total_seconds() / len(order_list)))
    except:
        time_per_order = 0
    return render(request,
                  'console/order_dashboard.html',
                  {'MENU': 'ORDER',
                   'order_list': order_list,
                   'price': price,
                   'total_duration': total_duration,
                   'time_per_order': time_per_order})
