from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404

from django.contrib.contenttypes.models import ContentType
from order.models import Order
from django.db.models import Count

import datetime

@login_required
def order_access_view(request):
    if not request.user.is_admin:
        raise Http404
    price = 0
    content = 0
    duration = datetime.timedelta(0)
    content = {}
    content_type = ContentType.objects.get(app_label='course', model='course')
    order_id_list = [ order.id for order in Order.objects.filter(net__gt=0.0,method__in=[1,2,3,4,5,6,7,8,9], status=3)[:100]]
  	
    item_all_count = order.item_set.filter(content_type=content_type,order__id__in=order_id_list).count()
    item_list = [ item for item in order.item_set.filter(content_type=content_type,order__id__in=order_id_list).annotate(Count('content')).order_by(Count('content'))]

    
    total_duration = datetime.timedelta(seconds=duration.total_seconds())
    time_per_order = datetime.timedelta(seconds=(duration.total_seconds()/len(order_id_list)))
    return render(request,
                  'console/order_content_access.html',
                  {'MENU': 'ORDER',
                   'item_list': item_list,
                   'item_all_count': item_all_count,
                   'price': price,
                   'total_duration': total_duration,
                   'time_per_order': time_per_order})
