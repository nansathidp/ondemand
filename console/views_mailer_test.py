from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404

from mailer.models import Template, Send
from account.models import Account

import uuid

@login_required
def mailer_test_view(request):
    if not request.user.is_admin:
        raise Http404

    msg = None
    template_list = Template.objects.filter(status=1)
    if request.method == 'POST':
        try:
            account = Account.objects.get(email=request.POST.get('email', ''))
        except:
            msg = 'Email not Found.'
        try:
            template = Template.objects.get(id=request.POST.get('template', -1))
        except:
            msg = 'Mailer.Template Not Found.'

        if msg is None:
            send = Send.objects.create(template=template,
                                       account_id=request.user.id,
                                       account_email=request.user.email,
                                       code=str(uuid.uuid4()).replace('-', ''))
            #send.send_mandrill()
            send.send_appengine()
            send.delete()
            msg = -1
    return render(request,
                  'console/mailer_test.html',
                  {'template_list': template_list,
                   'msg': msg})
