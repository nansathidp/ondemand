from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.contrib.auth import authenticate, login
from django.http import Http404

from account.models import Account

@login_required
def loginas_view(request):
    if not request.user.is_admin:
        raise Http404

    account_list = []
    if request.method == 'POST':
        q = request.POST.get('q', '-1')
        try:
            q = int(q)
            account_list = Account.objects.filter(id=q)
        except:
            account_list = Account.objects.filter(email__icontains=q)[:20]
    return render(request,
                  'console/loginas.html',
                  {'MENU': 'LOGINAS',
                   'account_list': account_list})

@login_required
def loginas_go_view(request, account_id):
    if not request.user.is_admin:
        raise Http404
    is_dashboard = request.GET.get('dashboard', '-1')
    account = get_object_or_404(Account, id=account_id)
    user = authenticate(email=account.email, login_as=True)
    login(request, user)
    if int(is_dashboard) == 1:
        return redirect('institute:dashboard')
    return redirect('home')
