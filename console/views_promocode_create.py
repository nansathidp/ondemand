from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import Http404

from api.views_api import json_render
from promote.models import Promote, Code

@login_required
def promocode_create_view(request):
    if not request.user.is_admin:
        raise Http404

    promotion_id = request.GET.get('promotion_id', None)
    create = request.GET.get('create', 0)
    if promotion_id is None:
        return json_render({'error':'promotion_id'}, 200)

    promotion = Promote.objects.get(id=promotion_id)

    for x in range(0, int(create)):
        code = Code(promote=promotion)
        code.gen()
        code.save()

    code_list = promotion.code_set.all()
    return render(request,
                  'console/promocode_create.html',
                  {'MENU': 'Promocode',
                   'code_list': code_list})
