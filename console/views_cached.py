import time

from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import render

from memcached_stats import MemcachedStats


@login_required
def cached_view(request):
    if not request.user.is_admin:
        raise Http404

    mem = MemcachedStats()
    key_list = []
    t = int(time.time())
    for cached in mem.key_details():
        d = {}
        d['key'] = ':'.join(cached[0].split(':')[2:])
        d['size'] = cached[1]
        d['time'] = int(cached[2].split(' ')[0]) - t
        key_list.append(d)
    return render(request,
                  'console/cached.html',
                  {'MENU': 'CACHED',
                   'key_list': key_list})
