from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.conf import settings

from memcached_stats import MemcachedStats
import time


@login_required
def cached_list_view(request):
    if not request.user.is_admin:
        raise Http404
    mem = MemcachedStats()
    key_list = []
    t = int(time.time())
    for cached in mem.key_details():
        d = {}
        d['key'] = ':'.join(cached[0].split(':')[2:])
        if d['key'].find(settings.CACHED_PREFIX) == -1:
            continue
        d['size'] = cached[1]
        d['time'] = int(cached[2].split(' ')[0]) - t
        key_list.append(d)
    return render(request,
                  'console/cached_list.html',
                  {'MENU': 'CACHED',
                   'key_list': key_list})
