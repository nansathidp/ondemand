from django.contrib.auth.decorators import login_required
from django.http import Http404
from io import BytesIO
from django.http import HttpResponse
from django.template import Template, Context

import os
#from rlextra.rml2pdf import rml2pdf
#import cStringIO
#
#from reportlab.pdfgen import canvas
#from reportlab.pdfbase import pdfmetrics
#from reportlab.pdfbase.cidfonts import UnicodeCIDFont
#
#from reportlab.pdfbase.ttfonts import TTFont

@login_required
def pdf_view(request):
    if not request.user.is_admin:
        raise Http404
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'

    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response)

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    pdfmetrics.registerFont(TTFont('THSarabunNew', os.path.join(PROJECT_ROOT, 'fonts/THSarabunNew.ttf')))
    p.setFont("THSarabunNew", 30)
    count_y = 0
    y = 30
    while (count_y < 26):
        count_x = 0
        x = 30
        while (count_x < 7):
            p.drawString(x, y, "สวัสดี")
            x += 82
            count_x += 1
        y += 30
        count_y += 1


    # Close the PDF object cleanly, and we're done.
    p.showPage()
    p.save()
    return response

@login_required
def pdf_rml_view(request):
    if not request.user.is_admin:
        raise Http404
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
    t = Template(open(os.path.join(PROJECT_ROOT, 'rml/sample.rml')).read())
    c = Context({"name": "Anapat Vimolprapaporn"})
    rml = t.render(c)
    buf = cStringIO.StringIO()
    rml2pdf.go(rml, outputFileName=buf)
    pdfData = buf.read()
    response = HttpResponse(mimetype='application/pdf')
    response.write(pdfData)
    response['Content-Disposition'] = 'attachment; filename=output.pdf'
    return response
