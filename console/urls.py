from django.conf.urls import url

from .views import home_view
from .views_account_move import account_move_view
from .views_bill import bill_table_view, bill_view
from .views_cached import cached_view
from .views_cached_delete import cached_delete_view, cached_delete_key_view
from .views_cached_detail import cached_detail_view
from .views_cached_stat import cached_stat_view
from .views_course_price import price_ios_view
from .views_document import document_view
from .views_item import item_expired_view, item_will_expired_view, item_notstart_view
from .views_loginas import loginas_view, loginas_go_view
from .views_mailer_test import mailer_test_view
from .views_order import order_view
from .views_order_access import order_access_view
from .views_order_progress import order_progress_view
from .views_order_unsuccess import order_unsuccess_view
from .views_package_report import package_report_view
from .views_pdf import pdf_view, pdf_rml_view
from .views_progress import progress_view, progress_log_view, progress_delete_log_view
from .views_promocode_create import promocode_create_view
from .views_rq import rq_view
from .views_stat import stat_keymetric_view
from .views_video import video_view, video_preview_view, video_duration_view, video_verify_view
from .views_video_stream import video_stream_preview_wowza_view, video_stream_verify_view

app_name = 'console'
urlpatterns = [
    url(r'^$', home_view, name='dashboard'), #TODO: testing
    url(r'^loginas/$', loginas_view, name='loginas'), #TODO: testing
    url(r'^loginas/(\d+)/go/$', loginas_go_view, name='loginas_go'), #TODO: testing

    url(r'^course/price/ios/$', price_ios_view, name='course_price_ios'), #TODO: testing
    url(r'^rq/$', rq_view),

    #Cached
    url(r'^cached/$', cached_view, name='cached'),
    url(r'^cached/stat/$', cached_stat_view, name='cached_stat'),
    url(r'^cached/detail/$', cached_detail_view, name='cached_detail'),
    url(r'^cached/delete/$', cached_delete_view, name='cached_delete'),
    url(r'^cached/delete/key/$', cached_delete_key_view, name='cached_delete_key'),
    
    url(r'^order/$', order_view, name='order'), #TODO: testing
    url(r'^order/access/$', order_access_view, name='access'), #TODO: testing
    url(r'^order/(\d+)/progress/$', order_progress_view, name='order_progress'), #TODO: testing
    url(r'^order/unsuccess/$', order_unsuccess_view, name='order_unsuccess'), #TODO: testing
    
    url(r'^item/expired/$', item_expired_view, name='item_expired'), #TODO: testing
    url(r'^item/notstart/$', item_notstart_view, name='item_notstart'), #TODO: testing
    url(r'^item/$', item_will_expired_view, name='item_will_expired'), #TODO: testing

    url(r'^progress/$', progress_view, name='progress'), #TODO: testing
    url(r'^progress/log/(\d+)/(\d+)/$', progress_log_view, name='progress_log'), #TODO: testing
    url(r'^progress/log/(\d+)/(\d+)/(\d+)/delete/$', progress_delete_log_view, name='progress_log_delete'), #TODO: testing

    url(r'^report/table/$', bill_table_view, name='report_table'), #TODO: testing
    url(r'^report/bill/$', bill_view, name='report_bill'), #TODO: testing
    
    # url(r'^statistic/user/$', stat_user_view, name='stat_user'), #TODO: testing
    url(r'^statistic/keymetric/$', stat_keymetric_view, name='stat_keymetric'), #TODO: testing
    
    url(r'^pdf/$', pdf_view, name='pdf'), #TODO: testing
    url(r'^pdf/rml/$', pdf_rml_view), #TODO: testing
    url(r'^package/(\d+)/report/$', package_report_view, name='package_report'), #TODO: testing
    url(r'^document/$', document_view, name='document'), #TODO: testing
    
    url(r'^promocode/create/$', promocode_create_view, name='promocode_create'), #TODO: testing

    url(r'^video/$', video_view, name='video'), #TODO: testing
    url(r'^video/(\d+)/$', video_preview_view, name='video_preview'), #TODO: testing
    url(r'^video/(\d+)/duration/$', video_duration_view, name='video_duration'), #TODO: testing
    url(r'^video/(\d+)/verify/$', video_verify_view, name='video_verify'), #TODO: testing
    url(r'^video/(\d+)/stream/wowza/$', video_stream_preview_wowza_view, name='video_preview_stream_wowza'), #TODO: testing
    url(r'^video/(\d+)/stream/wowza/verify/$', video_stream_verify_view, name='video_preview_stream_wowza_verify'), #TODO: testing
    url(r'^mailer/test/$', mailer_test_view, name='mailer_test'), #TODO: testing
    url(r'^account/move/$', account_move_view, name='account_move'), #TODO: testing
]
