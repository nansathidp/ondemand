from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.db.models import Sum, Count
from django.utils import timezone
from django.conf import settings

from account.models import Account
from django.conf import settings
from order.models import Order, Bill, Item
# from package.models import Package
# from progress.models import Log as ProgressLog, Course as ProgressCourse
from price.models import Price

from console.views_bill import _get_text_month, _get_filter_date
import datetime

@login_required
def stat_user_view(request):
    if not request.user.is_admin:
        raise Http404
    year = None
    month = None
    count_user = 0
    count_past_user = 0
    count_month_user = 0
    count_month_valid_user = 0
    year_range = range(2014, (datetime.datetime.now().year + 1))
    now = datetime.datetime.now()
    today = "%s %s %s" % (now.day, _get_text_month(now.month), now.year + 543)
    if request.method == 'POST':
        month = request.POST.get("month")
        year = request.POST.get("year")
        month = int(month)
        year = int(year)
        date_gte, date_lt = _get_filter_date(month, year)
        count_month_user = Account.objects.filter(date_joined__gte=date_gte, date_joined__lt=date_lt).count()
        count_month_valid_user = Account.objects.filter(date_joined__gte=date_gte, date_joined__lt=date_lt,
                                                        pair_id=-1).count()
        count_past_user = Account.objects.filter(date_joined__lt=date_lt).count()
        count_user = Account.objects.all().count()

    return render(request,
                  'console/stat_user.html',
                  {'MENU': 'stat',
                   'year_range': year_range,
                   'year': year,
                   'month': month,
                   'today': today,
                   'count_month_user': count_month_user,
                   'count_month_valid_user': count_month_valid_user,
                   'count_past_user': count_past_user,
                   'count_user': count_user})


@login_required
def stat_keymetric_view(request):
    if not request.user.is_admin:
        raise Http404

    start = None
    end = None
    new_register_user = 0
    new_register_user_organic = 0
    paid_user = 0
    paid_user_new_register = 0
    order_total = 0
    order_total_organic = 0
    order_free = 0
    order_free_organic = 0
    order_paid = 0
    total_revenue = 0

    course_revenue = 0
    package_revenue = 0
    other_revenue = 0
    course_usd_revenue = 0
    package_usd_revenue = 0
    other_usd_revenue = 0

    course_view = 0
    course_free_view = 0
    course_paid_view = 0
    free_item_total = 0
    free_item_total_organic = 0
    free_item_no_learn = 0
    free_item_vdo_played = 0
    free_item_learn_duration = datetime.timedelta(0)
    free_item_learn_duration_min = datetime.timedelta(0)

    paid_item_total = 0
    paid_item_total_organic = 0
    paid_item_no_learn = 0
    paid_item_vdo_played = 0
    paid_item_learn_duration = datetime.timedelta(0)
    paid_item_learn_duration_min = datetime.timedelta(0)

    revenue_android = 0
    revenue_ios = 0
    revenue_web = 0
    revenue_other = 0

    revenue_method_apple = 0
    revenue_method_mpay = 0
    revenue_method_other = 0

    order_paid_cart = 0
    order_paid_payment = 0
    order_paid_success = 0
    order_paid_all = order_paid_cart + order_paid_payment + order_paid_success
    content_free = []
    content_paid = []
    order_id_list = []
    paid_order_id = []
    if request.method == 'POST':
        course_type = settings.CONTENT_TYPE('course', 'course')
        package_type = settings.CONTENT_TYPE('package', 'package')
        datestart = request.POST.get('date-start', None).split('-')
        dateend = request.POST.get('date-end', None).split('-')
        start = timezone.make_aware(datetime.datetime(int(datestart[0]), int(datestart[1]), int(datestart[2]), 0, 0, 0))
        end = timezone.make_aware(datetime.datetime(int(dateend[0]), int(dateend[1]), int(dateend[2]), 23, 59, 59))
        new_register_user = Account.objects.filter(date_joined__range=(start, end)).count()
        new_register_user_organic = Account.objects.filter(date_joined__range=(start, end), pair_id=-1).count()
        paid_user = Account.objects.filter(order_account__timecomplete__range=(start, end),
                                           order_account__net__gt=0.0).annotate(Count('order_account')).count()
        paid_user_new_register = Account.objects.filter(date_joined__range=(start, end),
                                                        order_account__timecomplete__range=(start, end),
                                                        order_account__net__gt=0.0).annotate(
            Count('order_account')).count()
        # order_total = Order.objects.filter(order__timecomplete__range=(start, end), status=3).count()
        order_starter = Order.objects.filter(net=0, timecomplete__range=(start, end), status=3, store=0).count()
        order_free = Order.objects.filter(net=0, timecomplete__range=(start, end), status=3).count()
        bill_query = Bill.objects.filter(order__net__gt=0, timestamp__range=(start, end))
        order_id_list = [bill.order.id for bill in bill_query]
        order_paid = bill_query.count()
        order_free_organic = order_free - order_starter
        order_total = order_free + order_paid
        order_total_organic = (order_free - order_starter) + order_paid
        # total_revenue = bill_query.aggregate(Sum('order__net'))['order__net__sum']

        # Funnel
        order_paid_cart = Order.objects.filter(net__gt=0, timestamp__range=(start, end), status=1).count()
        order_paid_payment = Order.objects.filter(net__gt=0, timestamp__range=(start, end), status=2).count()
        order_paid_success = Order.objects.filter(net__gt=0, timecomplete__range=(start, end), status=3).count()
        order_paid_all = order_paid_cart + order_paid_payment + order_paid_success

        for order in Order.objects.filter(id__in=order_id_list):
            if order.method == 11:
                revenue_method_apple += order.net
            elif order.method == 6:
                revenue_method_mpay += order.net
            else:
                revenue_method_other += order.net

        total_revenue = revenue_method_other + revenue_method_mpay
        item_list = Item.objects.filter(order__in=order_id_list)
        for item in item_list:
            if item.content_type == course_type:
                if item.order.method == 11:
                    course_usd_revenue += item.get_net_price()
                else:
                    course_revenue += item.get_net_price()

            elif item.content_type == package_type:
                if item.order.method == 11:
                    package_usd_revenue += item.get_net_pdrice()
                else:
                    package_revenue += item.get_net_price()
            else:
                if item.order.method == 11:
                    other_usd_revenue += item.get_net_price()
                else:
                    other_revenue += item.get_net_price()

        for price in Price.objects.filter(store=1):
            if price.net == 0:
                content_free.append(price.course_id)
            else:
                content_paid.append(price.course_id)

        for package in Package.objects.all():
            if package.price == 0:
                content_free.append(package.id)
            else:
                content_paid.append(package.id)

        #Mark Use Stat
        course_free_view = 0
        course_paid_view = 0
        #course_free_view = CourseViewLog.objects.filter(date__range=(start, end),
        #                                                course_view__course__id__in=content_free).aggregate(Sum('stat'))['stat__sum']
        #course_paid_view = CourseViewLog.objects.filter(date__range=(start, end),
        #                                                course_view__course__id__in=content_paid).aggregate(Sum('stat'))['stat__sum']
        
        # course_free_view = Course.objects.filter(id__in=course_free).aggregate(Sum('stat_view'))['stat_view__sum']
        # course_paid_view = Course.objects.filter(id__in=course_paid).aggregate(Sum('stat_view'))['stat_view__sum']

        free_item_total = Item.objects.filter(order__timecomplete__range=(start, end), order__status=3,
                                              content__in=content_free).count()
        free_item_total_organic = Item.objects.filter(order__timecomplete__range=(start, end), order__status=3,
                                                      content__in=content_free).exclude(order__store=0).count()
        free_item_no_learn = 0
        free_item_vdo_played = 0
        free_item_learn_duration = 0
        #free_item_no_learn = ProgressCourse.objects.filter(log__item__is_free=True, log__timestamp__range=(start, end),
        #                                                   log__action=1).annotate(Count('log')).count()
        #free_item_vdo_played = ProgressLog.objects.filter(item__is_free=True,
        #                                                  timestamp__range=(start, end),
        #                                                  action=1).count()
        #free_item_learn_duration = ProgressLog.objects.filter(item__is_free=True,
        #                                                      timestamp__range=(start, end)) \
        #                                              .aggregate(Sum('credit_use'))['credit_use__sum']
        try:
            free_item_learn_duration_min = free_item_learn_duration.total_seconds() / 60
        except:
            free_item_learn_duration_min = 0

        paid_item_total = Item.objects.filter(order__timecomplete__range=(start, end), order__status=3,
                                              content__in=content_paid).count()

        for item in Item.objects.filter(order__timecomplete__range=(start, end), order__status=3,
                                        content__in=content_paid):
            paid_order_id.append(item.order.id)
        paid_item_total_organic = Item.objects.filter(order__timecomplete__range=(start, end), order__status=3,
                                                      content__in=content_paid).exclude(order__store=0).count()
        paid_item_no_learn = ProgressCourse.objects.filter(log__item__is_free=True, log__timestamp__range=(start, end),
                                                           log__action=1).annotate(Count('log')).count()
        paid_item_vdo_played = ProgressLog.objects.filter(item__is_free=False, timestamp__range=(start, end),
                                                          action=1).count()
        paid_item_learn_duration = \
        ProgressLog.objects.filter(item__is_free=False, timestamp__range=(start, end)).aggregate(Sum('credit_use'))[
            'credit_use__sum']
        try:
          paid_item_learn_duration_min = paid_item_learn_duration.total_seconds() / 60
        except:
          paid_item_learn_duration_min = 0
    return render(request,
                  'console/stat_keymetric.html',
                  {'MENU': 'stat',
                   'start': start,
                   'end': end,
                   'new_register_user': new_register_user,
                   'new_register_user_organic': new_register_user_organic,
                   'paid_user': paid_user,
                   'paid_user_new_register': paid_user_new_register,
                   'order_total': order_total,
                   'order_total_organic': order_total_organic,
                   'order_free': order_free,
                   'order_free_organic': order_free_organic,
                   'order_paid': order_paid,
                   'revenue_android': revenue_android,
                   'revenue_ios': revenue_ios,
                   'revenue_web': revenue_web,
                   'revenue_other': revenue_other,
                   'revenue_method_apple': revenue_method_apple,
                   'revenue_method_mpay': revenue_method_mpay,
                   'revenue_method_other': revenue_method_other,
                   'total_revenue': total_revenue,
                   'course_revenue': course_revenue,
                   'package_revenue': package_revenue,
                   'other_revenue': other_revenue,
                   'course_usd_revenue': course_usd_revenue,
                   'package_usd_revenue': package_usd_revenue,
                   'other_usd_revenue': other_usd_revenue,
                   'count_course_free': len(content_free),
                   'count_course_paid': len(content_paid),
                   'course_free_view': course_free_view,
                   'course_paid_view': course_paid_view,
                   'free_item_total': free_item_total,
                   'free_item_total_organic': free_item_total_organic,
                   'free_item_no_learn': free_item_no_learn,
                   'free_item_vdo_played': free_item_vdo_played,
                   'free_item_learn_duration': free_item_learn_duration,
                   'free_item_learn_duration_min': free_item_learn_duration_min,
                   'order_id_list': order_id_list,
                   'order_paid_all': order_paid_all,
                   'order_paid_cart': order_paid_cart,
                   'order_paid_payment': order_paid_payment,
                   'order_paid_success': order_paid_success,
                   'paid_item_total': paid_item_total,
                   'paid_order_id': paid_order_id,
                   'paid_item_total_organic': paid_item_total_organic,
                   'paid_item_no_learn': paid_item_no_learn,
                   'paid_item_vdo_played': paid_item_vdo_played,
                   'paid_item_learn_duration': paid_item_learn_duration,
                   'paid_item_learn_duration_min': paid_item_learn_duration_min,
                   })
