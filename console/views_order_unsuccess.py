from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import Http404

from order.models import Order, Item


@login_required
def order_unsuccess_view(request):
    if not request.user.is_admin:
        raise Http404
    content_list = [1436509646002854, 1436510381002964, 1436510519002966, 1436510519002966, 1441195136002802]
    account_list = []
    order_list = []
    order_account_list = []
    for item in Item.objects.filter(content__in=content_list, order__status=3):
        if item.order.account not in account_list:
            account_list.append(item.order.account)

    for item in Item.objects.filter(content__in=content_list, order__status=2).order_by('-order__timestamp')[:100]:
        if item.order.account not in account_list and item.order.account not in order_account_list:
            order_account_list.append(item.order.account)
            order_list.append(item.order)

    return render(request,
                  'console/order_unsuccess.html', {
                      'order_list': order_list,
                  })
