from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse, JsonResponse
from django.core.cache import cache

from memcached_stats import MemcachedStats


@login_required
def cached_delete_view(request):
    if not request.user.is_admin:
        raise Http404

    key = request.GET.get('key', None)
    if key is not None:
        cache.delete(key)
    return redirect('console:cached')


@login_required
def cached_delete_key_view(request):
    if not request.user.is_admin:
        raise Http404
    response_data = {'remove': [], 'count': 0}
    key = request.GET.get('key', None)
    if key is not None:
        mem = MemcachedStats()
        for cached in mem.key_details():
            cached_key = ':'.join(cached[0].split(':')[2:])
            if key in cached_key:
                response_data['remove'].append(cached_key)
                cache.delete(cached_key)
        response_data['count'] = len(response_data['remove'])
    return JsonResponse(response_data)

