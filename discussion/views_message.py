from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt


from .models import Topic, Message, Role
from .views import check_provider_permission
from utils.content import get_content
import bleach


@csrf_exempt
@login_required
def create_view(request):
    code = 200
    if request.method == 'POST':
        topic_id = request.POST.get('topic_id', None)
        body = request.POST.get('body', None)
        try:
            body = bleach.clean(body,
                                tags=settings.BLEACH_ALLOW_TAG)
        except:
            body = ''
        type = request.POST.get('type', 1)
        topic = get_object_or_404(Topic, id=topic_id)
        content = get_content(topic.content_type_id, topic.content)
        if content is None:
            raise Http404

        provider, sub_provider = check_provider_permission(request, content.provider)
        role = Role.pull(request.user, provider, sub_provider, type)
        message = Message.objects.create(topic=topic, body=body, role=role)

    else:
        return HttpResponse(status=405)
    return JsonResponse({'status': code, 'message': message.api_display()})


@csrf_exempt
@login_required
def edit_view(request):
    code = 200
    # is_webview = _mobile_auth(request)
    if request.method == 'POST':
        message_id = request.POST.get('message_id', None)
        body = request.POST.get('body', None)
        try:
            body = bleach.clean(body,
                                tags=settings.BLEACH_ALLOW_TAG)
        except:
            body = ''
        type = request.POST.get('type', 1)
        message = get_object_or_404(Message, id=message_id)
        provider, sub_provider = check_provider_permission(request, message.topic.role.provider)
        role = Role.pull(request.user, provider, sub_provider, type)
        message.body = body
        # message.role = role
        message.is_edited = True
        # message.edit_by = role.get_owner()['name']
        message.edit_by = request.user.get_full_name()
        message.save(update_fields=['body', 'role', 'is_edited', 'edit_by'])
    else:
        return HttpResponse(status=405)
    return JsonResponse({'status': code, 'message': message.api_display()})


@csrf_exempt
def delete_view(request):
    code = 200
    if request.method == 'POST':
        message_id = request.POST.get('message_id', None)
        message = get_object_or_404(Message, id=message_id)
        message.is_display = False
        message.save(update_fields=['is_display'])
    else:
        return HttpResponse(status=405)
    return JsonResponse({'status': code})
