from django.conf import settings
from django.db import models


class Role(models.Model):

    TYPE_CHOICES = (
        (1, 'User'),
        (2, 'Learning Center'),
        (3, 'Sub Learning Center'),
    )

    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    provider = models.ForeignKey('provider.Provider', null=True, blank=True, on_delete=models.SET_NULL)

    # Keep Sub Learning Center
    content = models.BigIntegerField(db_index=True, default=-1)
    content_type = models.ForeignKey('contenttypes.ContentType', null=True, related_name='+')

    type = models.IntegerField(choices=TYPE_CHOICES, default=1)

    @staticmethod
    def pull(account, provider, subprovider, type):
        subprovider_content_type = None
        content = -1
        if subprovider is not None and settings.IS_SUB_PROVIDER:
            subprovider_content_type = settings.CONTENT_TYPE('subprovider', 'subprovider')
            content = subprovider.id

        role = Role.objects.filter(account=account,
                                   provider=provider,
                                   content_type=subprovider_content_type,
                                   content=content,
                                   type=type).first()
        if role is None:
            role = Role.objects.create(account=account,
                                       provider=provider,
                                       content_type=subprovider_content_type,
                                       content=content,
                                       type=type)
        return role

    # def is_owner(self):

    def get_owner(self, is_api=False):

        def _account(role):
            if role.account.image:
                image = role.account.image.url if is_api else role.account.image.url
            else:
                image = '%s/static/images/profile_default.jpg' % settings.SITE_URL
            return {'name': role.account.get_display(),
                    'image': image}

        if self.type == 1:
            return _account(self)
        if self.type == 2 and self.provider:
            image = None
            if self.provider.image:
                image = self.provider.image.url if is_api else self.provider.image.url
            return {'name': self.provider.name,
                    'image': image}
        elif settings.IS_SUB_PROVIDER and self.type == 3:
            from subprovider.models import SubProvider
            sub_provider = SubProvider.pull(self.content)
            if sub_provider is None:
                return _account(self)
            else:
                image = None
                if sub_provider.image:
                    image = sub_provider.image.url if is_api else sub_provider.image.url
                return {'name': sub_provider.name,
                        'image': image}
        else:
            return _account(self)


class Topic(models.Model):
    role = models.ForeignKey(Role)

    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    content = models.BigIntegerField(db_index=True)

    topic = models.CharField(max_length=120, blank=True)
    body = models.TextField(blank=True)

    is_edited = models.BooleanField(default=False)
    edit_by = models.CharField(max_length=120, blank=True)

    is_display = models.BooleanField(db_index=True, default=True)

    timeupdate = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')

    def count_message(self):
        return Message.objects.filter(topic_id=self.id, is_display=True).count()

    def api_display(self):
        return {'id': self.id,
                'owner': self.role.get_owner(is_api=True),
                'topic': self.topic,
                'body': self.body,
                'is_edited': self.is_edited,
                'edit_by': self.edit_by,
                'timestamp': self.timestamp}


class Message(models.Model):

    topic = models.ForeignKey(Topic)
    role = models.ForeignKey(Role)

    body = models.TextField(blank=True)

    is_edited = models.BooleanField(default=False)
    edit_by = models.CharField(max_length=120, blank=True)

    is_display = models.BooleanField(db_index=True, default=True)

    timeupdate = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')

    def api_display(self):
        return {'id': self.id,
                'owner': self.role.get_owner(is_api=True),
                'body': self.body,
                'is_edited': self.is_edited,
                'edit_by': self.edit_by,
                'timestamp': self.timestamp}
