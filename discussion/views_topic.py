from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

from discussion.views import check_provider_permission
from .models import Topic, Role


@login_required
def edit_view(request):
    code = 200
    # is_webview = _mobile_auth(request)
    if request.method == 'POST':
        topic_id = request.POST.get('topic_id', None)
        topic = get_object_or_404(Topic, id=topic_id)
        type = request.POST.get('type', 1)
        body = request.POST.get('body', None)

        # provider, sub_provider = check_provider_permission(request, topic.role.provider)
        # role = Role.pull(request.user, provider, sub_provider, type)

        topic.body = body
        # topic.role = role
        topic.is_edited = True
        # topic.edit_by = role.get_owner()['name']
        topic.edit_by = request.user.get_full_name()
        topic.save(update_fields=['body', 'is_edited', 'edit_by'])
    else:
        return HttpResponse(status=405)
    return JsonResponse({'status': code, 'topic': topic.api_display()})


@login_required
def delete_view(request):
    code = 200
    if request.method == 'POST':
        topic_id = request.POST.get('topic_id', None)
        topic = get_object_or_404(Topic, id=topic_id)
        topic.is_display = False
        topic.save(update_fields=['is_display'])
    else:
        return HttpResponse(status=405)
    return JsonResponse({'status': code, 'topic': topic.api_display()})

