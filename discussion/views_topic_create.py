from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse

from .models import Topic, Role

from ondemand.views_decorator import check_project

from .views import check_provider_permission
from utils.content import get_content
import bleach


@login_required
@check_project
def create_view(request, content_type, content_id, order_item_id=None):
    is_webview = request.GET.get('is_webview', 'f')
    if content_type == 'course':
        _content_type = settings.CONTENT_TYPE('course', 'course')
    elif content_type == 'program':
        _content_type = settings.CONTENT_TYPE('program', 'program')
    else:
        raise Http404

    content = get_content(_content_type.id, content_id)
    if content is None:
        raise Http404

    type = request.POST.get('type', 1)
    topic = request.POST.get('topic', None)
    body = request.POST.get('body', None)
    if topic is None:
        raise Http404
    try:
        body = bleach.clean(body, tags=settings.BLEACH_ALLOW_TAG)
    except:
        body = ''
    provider, sub_provider = check_provider_permission(request, content.provider)
    try:
        role = Role.pull(request.user, provider, sub_provider, type)
    except:
        raise Http404
    topic = Topic.objects.create(content=content_id,
                                 content_type=_content_type,
                                 topic=topic,
                                 body=body,
                                 role=role)
    if topic is None:
        raise Http404
    if order_item_id:
        redirect_url = '%s?is_webview=%s' % (reverse('discussion:detail', args=[topic.id, order_item_id]), is_webview)
    else:
        redirect_url = '%s?is_webview=%s' % (reverse('discussion:detail', args=[topic.id]), is_webview)
    return redirect(redirect_url)
