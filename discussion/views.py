from django.conf import settings

from provider.cached import cached_provider_account

if settings.IS_SUB_PROVIDER:
    from subprovider.cached import cached_sub_provider_account_provider


def get_topic_content_type(topic):
    content_type = None
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_program = settings.CONTENT_TYPE('program', 'program')
    if topic.content_type == content_type_course:
        content_type = 'course'
    elif topic.content_type == content_type_program:
        content_type = 'program'
    return content_type


def check_provider_permission(request, content_provider):
    provider = None
    sub_provider = None
    if content_provider is not None:
        provider_id_list = cached_provider_account(request.user.id)
        if content_provider in provider_id_list:
            provider = content_provider
            if settings.IS_SUB_PROVIDER:
                sub_provider = cached_sub_provider_account_provider(request.user.id, provider.id)
    return provider, sub_provider
