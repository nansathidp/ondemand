from django.conf import settings
from django.contrib.auth import authenticate, login
from django.core.paginator import PageNotAnInteger, Paginator, EmptyPage
from django.http import Http404
from django.shortcuts import render
from django.urls import reverse

from ondemand.views_decorator import param_filter
from .models import Topic
from course.models import Course
from program.models import Program

from account.cached import cached_api_account_auth
from discussion.views import check_provider_permission


def _mobile_auth(request):
    is_webview = request.GET.get('is_webview', 'f') == 't'
    if not request.user.is_authenticated and is_webview:
        if is_webview:
            token = request.GET.get('token', None)
            if token:
                account = cached_api_account_auth(token, 'autologin', is_force=True)
                if account:
                    if settings.PROJECT == 'cimb':
                        user = authenticate(email=account.uid, password=None)
                    else:
                        user = authenticate(email=account.email)
                    if user is None:
                        raise Http404
                    login(request, user)
                else:
                    raise Http404
            else:
                raise Http404
        else:
            raise Http404
    return is_webview


@param_filter
def home_view(request, content_type, content_id, order_item_id=None):

    # Mobile Auth
    is_webview = _mobile_auth(request)
    if content_type == 'course':
        content = Course.pull(content_id)
        _content_type = settings.CONTENT_TYPE('course', 'course')
        if order_item_id:
            parent_url = reverse('course:detail', args=[content_id, order_item_id])
        else:
            parent_url = reverse('course:detail', args=[content_id])
    elif content_type == 'program':
        content = Program.pull(content_id)
        _content_type = settings.CONTENT_TYPE('program', 'program')
        if order_item_id:
            parent_url = reverse('program:detail', args=[content_id, order_item_id])
        else:
            parent_url = reverse('program:detail', args=[content_id])
    else:
        raise Http404

    if content is None:
        raise Http404

    topic_list = Topic.objects.filter(content=content_id, content_type=_content_type, is_display=True).order_by('-timeupdate')[:10]
    paginator = Paginator(topic_list, 10)
    try:
        pager = paginator.page(request.page)
    except PageNotAnInteger:
        pager = paginator.page(1)
    except EmptyPage:
        pager = paginator.page(paginator.num_pages)

    provider, sub_provider = check_provider_permission(request, content.provider)

    for topic in topic_list:
        if provider is not None or sub_provider is not None:
            topic.has_perm = True
        elif topic.role.account == request.user:
            topic.has_perm = True
        else:
            topic.has_perm = False

    return render(request, 'discussion/topic.html',
                  {'DISPLAY_SUBMENU': True,
                   'ROOT_PAGE': content_type,
                   'topic_list': pager.object_list,
                   'content_type': content_type,
                   'content_id': content_id,
                   'content': content,
                   'provider': provider,
                   'pager': pager,
                   'sub_provider': sub_provider,
                   'order_item_id': order_item_id,
                   'IS_SUB_PROVIDER': settings.IS_SUB_PROVIDER,
                   'PARENT_URL': parent_url,
                   'IS_WEBVIEW': is_webview
                   })
