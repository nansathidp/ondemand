from django.conf import settings
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from discussion.views import check_provider_permission, get_topic_content_type
from ondemand.views_decorator import param_filter
from utils.content import get_content
from .models import Topic, Message


@param_filter
def detail_view(request, topic_id, order_item_id=None):
    is_webview = request.GET.get('is_webview', 'f') == 't'
    topic = get_object_or_404(Topic, id=topic_id)
    if topic.content_type == settings.CONTENT_TYPE('course', 'course'):
        _content_type = settings.CONTENT_TYPE('course', 'course')
    elif topic.content_type == settings.CONTENT_TYPE('program', 'program'):
        _content_type = settings.CONTENT_TYPE('program', 'program')
    else:
        raise Http404

    # FIXME: PAGINATION
    message_list = Message.objects.filter(topic=topic, is_display=True)[:100]
    content = get_content(topic.content_type_id, topic.content)
    if content is None:
        raise Http404

    provider, sub_provider = check_provider_permission(request, content.provider)
    if order_item_id:
        parent_url = reverse('discussion:topic', args=[get_topic_content_type(topic), topic.content, order_item_id])
    else:
        parent_url = reverse('discussion:topic', args=[get_topic_content_type(topic), topic.content])
    return render(request, 'discussion/detail.html',
                  {'DISPLAY_SUBMENU': True,
                   'ROOT_PAGE': 'discussion',
                   'topic': topic,
                   'message_list': message_list,
                   'content': content,
                   'content_type': _content_type,
                   'provider': provider,
                   'sub_provider': sub_provider,
                   'order_item_id': order_item_id,
                   'IS_WEBVIEW': is_webview,
                   'IS_SUB_PROVIDER': settings.IS_SUB_PROVIDER,
                   'PARENT_URL': parent_url,
                   })
