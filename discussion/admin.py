from django.contrib import admin
from .models import Topic, Message


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ('content', 'content_type', 'topic', 'role')


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('topic', 'body')