from django.conf.urls import url

from .views_home import home_view
from .views_topic_create import create_view
from .views_detail import detail_view

from . import views_topic
from . import views_message

app_name = 'discussion'
urlpatterns = [

    # API
    url(r'^topic/edit/$', views_topic.edit_view, name='topic_edit'),  # TODO: testing
    url(r'^topic/delete/$', views_topic.delete_view, name='topic_delete'),  # TODO: testing
    url(r'^message/create/$', views_message.create_view, name='message_create'),  # TODO: testing
    url(r'^message/edit/$', views_message.edit_view, name='message_edit'),  # TODO: testing
    url(r'^message/delete/$', views_message.delete_view, name='message_delete'),  # TODO: testing

    # Web
    url(r'^(?P<content_type>[\w-]+)/(?P<content_id>\w+)/$', home_view, name='topic'),  # TODO: testing
    url(r'^(?P<content_type>[\w-]+)/(?P<content_id>\w+)/item/(?P<order_item_id>\w+)/$', home_view, name='topic'),  # TODO: testing

    url(r'^(\w+)/(\d+)/create/$', create_view, name='create'),
    url(r'^(\w+)/(\d+)/create/item/(\d+)/$', create_view, name='create'),

    url(r'^(\d+)/$', detail_view, name='detail'),  # TODO: testing
    url(r'^(\d+)/item/(\d+)/$', detail_view, name='detail'),  # TODO: testing
]
