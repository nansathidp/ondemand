from django import template
from django.conf import settings
from discussion.views import check_provider_permission

register = template.Library()


@register.simple_tag
def is_owner(request, provider, object):
    provider, sub_provider = check_provider_permission(request, provider)
    if request.user == object.role.account or provider or sub_provider:
        return True
    elif request.user.is_authenticated and request.user.is_superuser:
        return True
    elif request.user.is_authenticated and settings.ROLE_SUPER_ADMIN_ID != -1 and request.user.groups.filter(id=settings.ROLE_SUPER_ADMIN_ID).exists():
        return True
    else:
        return False

