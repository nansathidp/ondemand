import datetime

from dateutil import tz
from django.db import models
from django.db.models import Count

from department.models import Member


# Create your models here.
class LoginLog(models.Model):
    account = models.ForeignKey('account.Account')
    first_active = models.DateTimeField(blank=True, null=True)
    last_active = models.DateTimeField(blank=True, null=True)

    @staticmethod
    def push(account):
        LoginLog.objects.create(account=account, first_active=account.first_active, last_active=account.last_active)


class LoginAmount(models.Model):
    sum = models.IntegerField(default=0)
    date = models.DateField(null=True)

    @staticmethod
    def push(date):
        active_user = LoginAmount.objects.filter(date=date).first()
        if active_user:
            return active_user.update()
        else:
            date_min = datetime.datetime.combine(date, datetime.time.min).replace(tzinfo=tz.tzlocal())
            date_max = datetime.datetime.combine(date, datetime.time.max).replace(tzinfo=tz.tzlocal())
            count_active_user = LoginLog.objects.filter(datetime__range=(date_min, date_max)).values(
                'account').distinct().count()
            return LoginAmount.objects.create(sum=count_active_user, date=date)

    def update(self):
        date_min = datetime.datetime.combine(self.date, datetime.time.min).replace(tzinfo=tz.tzlocal())
        date_max = datetime.datetime.combine(self.date, datetime.time.max).replace(tzinfo=tz.tzlocal())
        self.sum = LoginLog.objects.filter(datetime__range=(date_min, date_max)).values('account').distinct().count()
        self.save(update_fields=['sum'])


class LoginDepartmentAmount(models.Model):
    department = models.ForeignKey('department.Department')
    sum = models.IntegerField(default=0)
    date = models.DateField(null=True)

    @staticmethod
    def push(date):
        date_min = datetime.datetime.combine(date, datetime.time.min).replace(tzinfo=tz.tzlocal())
        date_max = datetime.datetime.combine(date, datetime.time.max).replace(tzinfo=tz.tzlocal())
        active_user = LoginLog.objects.filter(first_active__range=(date_min, date_max)).values_list('account',
                                                                                                    flat=True).distinct()
        department_sum = Member.objects.filter(account__in=active_user).values('department').annotate(
            sum=Count('department'))
        for department in department_sum:
            department['date'] = date
            LoginDepartmentAmount.update(department)

    def update(self):
        department_log = LoginDepartmentAmount.objects.filter(department=self['department'], date=self['date']).first()
        if department_log:
            department_log.sum = self['sum']
            department_log.save(update_fields=['sum'])
        else:
            LoginDepartmentAmount.objects.create(
                department_id=self['department'],
                sum=self['sum'],
                date=self['date']
            )
