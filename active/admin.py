from django.contrib import admin
from active.models import LoginLog, LoginAmount, LoginDepartmentAmount


# Register your models here.
@admin.register(LoginLog)
class LoginLogAdmin(admin.ModelAdmin):
    list_display = ('account', 'first_active', 'last_active')


@admin.register(LoginAmount)
class LoginAmountAdmin(admin.ModelAdmin):
    list_display = ('sum', 'date')


@admin.register(LoginDepartmentAmount)
class LoginDepartmentAmount(admin.ModelAdmin):
    list_display = ('department', 'sum', 'date')
