from django.test import TestCase
from active.models import LoginAmount, LoginLog, LoginDepartmentAmount
# Create your tests here.

class LoginTestCase(TestCase):

    def setUp(self):
        LoginAmount.objects.create(sum=20, date='2017-07-01')

    def login_amount_can_show(self):
        get_count = LoginAmount.objects.filter(sum__gte=20)
        self.assertEqual(get_count.speak(), 'more than 20')
