import datetime

import xlwt
from django.http import HttpResponse


def to_xls(columns, rows, user_amount):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=login_report.xls'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()

    date_format = xlwt.XFStyle()
    date_format.num_format_str = 'dd/mm/yyyy'

    total = 0
    for row in rows:
        row_num += 1
        total = row[2] + total
        percent = '%.2f' % ((total / user_amount) * 100)
        row = (row[0], row[1], total, user_amount, percent, row[2])
        for col_num in range(len(row)):
            if isinstance(row[col_num], datetime.date):
                ws.write(row_num, col_num, row[col_num], date_format)
            else:
                ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response
