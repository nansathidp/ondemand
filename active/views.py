import datetime

import xlwt
from dateutil import tz
from django.http import HttpResponse
from django.utils import timezone

from active.dashboard.views_content import content_login_view
from active.models import LoginLog, LoginDepartmentAmount
from account.models import Account
from department.models import Member
from django.db.models import Sum, Count


def login_report(request):
    LoginLog.push(request.user)
    if request.user.first_active is None:
        LoginDepartmentAmount.push(timezone.now().date())
    else:
        LoginDepartmentAmount.push(request.user.first_active.date())


def export_department(request):
    from department.cached import cached_department
    department = None
    columns = ['Date', 'Total of login', 'Total of users', '%', 'Number of First Login']
    q_department = request.POST.get('q_department', None)
    date_from = request.POST.get('datetime1', None)
    try:
        if date_from is not None:
            date_from = datetime.datetime.strptime(date_from, '%d/%m/%Y').date()
            d_from = date_from
    except:
        return content_login_view(request)

    date_to = request.POST.get('datetime2', None)
    try:
        if date_to is not None:
            date_to = datetime.datetime.strptime(date_to, '%d/%m/%Y').date()
            d_to = date_to
    except:
        return content_login_view(request)

    day = datetime.timedelta(days=1)

    if date_from and date_to and q_department:

        if int(q_department) != -1:
            department = cached_department(q_department)
        if department is not None:
            filter_department_list = department.get_all_child()
            rows = LoginDepartmentAmount.objects.filter(date__range=(date_from, date_to), department__in=filter_department_list) \
                .values('date').distinct().annotate(total=Sum('sum')).values_list('date', 'total')
            user_amount = Member.objects.filter(department__in=filter_department_list).count()
        else:
            rows = Account.objects.extra(select={'first_active':'DATE(first_active)'}).filter(first_active__range=(date_from, date_to)) \
                   .values('first_active').annotate(total=Count('first_active')).distinct().values_list('first_active', 'total').order_by('first_active')
            user_amount = Account.objects.all().count()

        result = []
        while date_from <= date_to:
            date_compare = [row for row in rows if date_from in row]
            if not date_compare:
                result.append((date_from, 0))
            else:
                result.append(date_compare[0])
            date_from += day
        return to_xls(columns, result, user_amount, department, d_from, d_to)

    return content_login_view(request)


def to_xls(columns, rows, user_amount=0, department=None, date_from=None, date_to=None):
    response = HttpResponse(content_type='application/ms-excel')
    if department is None:
        department = 'ALL'
    response['Content-Disposition'] = 'attachment; filename=Login_Report_%s_%s_To_%s.xls' % (str(department), str(date_from), str(date_to))

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users')

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()

    date_format = xlwt.XFStyle()
    date_format.num_format_str = 'dd/mm/yyyy'

    total = 0
    for row in rows:
        row_num += 1
        total = row[1] + total
        try:
            percent = '%.2f' % ((total / user_amount) * 100)
        except:
            percent = '%.2f' % 0
        row = (row[0], total, user_amount, percent, row[1])
        for col_num in range(len(row)):
            if isinstance(row[col_num], datetime.date):
                ws.write(row_num, col_num, row[col_num], date_format)
            else:
                ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response
