from department.cached import cached_department
from utils.filter import get_q
from django.shortcuts import render


def content_login_view(request):
    from department.models import Department
    q_department_list = Department.objects.filter(parents__isnull=True)
    q_department = get_q(request, 'q_department')

    return render(request, 'active/dashboard/login_report.html',
                  {'q_department': q_department,
                   'q_department_list': q_department_list})