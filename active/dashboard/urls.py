from django.conf.urls import url

from active.views import export_department, login_report
from active.dashboard.views_content import content_login_view

app_name = 'active'
urlpatterns = [
    url(r'^$', content_login_view, name='report'),
    url(r'^department-login/report$', export_department, name='content_department_report'),
]
