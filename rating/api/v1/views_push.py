from django.conf import settings

from api.views import check_auth
from .views import json_render, check_key, check_require

from ...models import Rating

from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def push_view(request):
    result = {}

    if request.method != 'POST':
        return json_render({}, 201)

    content_id = request.POST.get('content', -1)
    content_type = request.POST.get('content_type', None)
    weight = int(request.POST.get('rating', -1))
    message = request.POST.get('message', None)

    # Check Require
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request, require_token=False)
    if code != 200:
        return json_render(result, code)

    # Check Authentication
    account, code = check_auth(request, is_allow_anonymous=False)
    if code != 200:
        return json_render({}, code)

    _content_type = None
    if content_type == 'course':
        _content_type = settings.CONTENT_TYPE('course', 'course')
    elif content_type == 'program':
        _content_type = settings.CONTENT_TYPE('program', 'program')
    elif content_type == 'question':
        _content_type = settings.CONTENT_TYPE('question', 'activity')

    # Validate
    if content_id == -1:
        return json_render({}, 213)
    if _content_type is None:
        return json_render({}, 210)
    if weight not in [1, 2, 3, 4, 5]:
        return json_render({}, 9002)

    rating, log = Rating.push(_content_type,
                              content_id,
                              account,
                              message,
                              weight)
    if rating is None:
        return json_render({}, 9000)
    else:
        result['rating'] = rating.api_display()
    return json_render(result, code)
