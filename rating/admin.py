from django.contrib import admin
from .models import Rating, Log

@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'content', 'weight', 'count', 'timeupdate')
    search_fields = ['content']

@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    list_display = ('rating', 'account', 'weight', 'timestamp')
