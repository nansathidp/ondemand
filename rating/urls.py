from django.conf.urls import url

from .views_push import push_view

app_name = 'rating'
urlpatterns = [
    url(r'^push/$', push_view, name='push'), #TODO: testing
]
