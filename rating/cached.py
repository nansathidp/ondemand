from django.conf import settings
from django.core.cache import cache

from .models import Rating, Log

from utils.cached.time_out import get_time_out_week


def cached_rating_first(content_type, content_id):
    key = '%s_rating_first_%s_%s' % (settings.CACHED_PREFIX, content_type.id, int(content_id))
    result = cache.get(key)
    if result is None:
        result = Rating.objects.filter(content_type=content_type,
                                       content=content_id).first()
        if result is None:
            result = Rating.objects.create(content_type=content_type,
                                           content=content_id)
        cache.set(key, result, get_time_out_week())
    return result


def cached_rating_update(rating):
    key = '%s_rating_first_%s_%s' % (settings.CACHED_PREFIX, rating.content_type_id, rating.content)
    cache.set(key, rating, get_time_out_week())


def cached_rating_log(rating_id, account_id):
    key = '%s_rating_log_%s_%s' % (settings.CACHED_PREFIX, rating_id, account_id)
    result = cache.get(key)
    if result is None:
        result = Log.objects.filter(rating_id=rating_id,
                                    account_id=account_id).first()
        if result is None:
            cache.set(key, -1, get_time_out_week())
        else:
            cache.set(key, result, get_time_out_week())
    return None if result == -1 else result


def cached_rating_log_update(log):
    key = '%s_rating_log_%s_%s' % (settings.CACHED_PREFIX, log.rating_id, log.account_id)
    cache.set(key, log, get_time_out_week())


def cached_rating_log_list(rating_id):
    key = '%s_rating_log_list_%s' % (settings.CACHED_PREFIX, rating_id)
    result = cache.get(key)
    if result is None:
        result = Log.objects.filter(rating_id=rating_id)[:100]
        cache.set(key, result, get_time_out_week())
    return result


def cached_rating_log_list_delete(rating_id):
    key = '%s_rating_log_list_%s' % (settings.CACHED_PREFIX, rating_id)
    cache.delete(key)


def cached_rating_popular_list(content_type, is_force=False):
    key = '%s_rating_popular_%s' % (settings.CACHED_PREFIX, content_type.id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Rating.objects.values_list('content', flat=True).filter(content_type=content_type).order_by('-weight')[
                 :8]  # Max
        result = list(result)
        cache.set(key, result, get_time_out_week())
    return result


def cached_rating_popular_list_delete(content_type):
    key = '%s_rating_popular_%s' % (settings.CACHED_PREFIX, content_type.id)
    cache.delete(key)
