from django.db import models


class Rating(models.Model):
    content_type = models.ForeignKey('contenttypes.ContentType')
    content = models.BigIntegerField(db_index=True)

    weight = models.FloatField(default=0.0, db_index=True)
    count = models.IntegerField(default=0, db_index=True)
    timeupdate = models.DateTimeField(auto_now=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')

    @staticmethod
    def pull_first(content_type, content_id):
        from .cached import cached_rating_first
        return cached_rating_first(content_type, content_id)
    
    @staticmethod
    def push(content_type, content_id, account, message, weight):
        from .cached import cached_rating_update, \
            cached_rating_log, cached_rating_log_update, cached_rating_log_list_delete, \
            cached_rating_popular_list_delete
        
        rating = Rating.pull_first(content_type, content_id)
        log = cached_rating_log(rating.id, account.id)
        if log is None:
            log = Log.objects.create(rating=rating,
                                     account=account,
                                     message=message,
                                     weight=weight)
            cached_rating_log_update(log)
            rating.weight = ((rating.weight * rating.count) + weight) / (rating.count + 1)
            rating.count += 1
            rating.save(update_fields=['weight', 'count'])
            cached_rating_popular_list_delete(content_type)
            cached_rating_update(rating)
            cached_rating_log_list_delete(rating.id)
            return rating, log
        else:
            return None, None
        
    @staticmethod
    def popular_list(content_type, limit):
        from django.conf import settings
        from .cached import cached_rating_popular_list
        from course.cached import pack_course
        from program.cached import pack_program

        if content_type == settings.CONTENT_TYPE('course', 'course'):
            return pack_course(cached_rating_popular_list(content_type)[:limit])
        elif content_type == settings.CONTENT_TYPE('program', 'program'):
            return pack_program(cached_rating_popular_list(content_type)[:limit])
        else:
            return []

    @staticmethod
    def is_rated(content_type_id, content_id, account):
        return Log.objects.filter(
            rating__content_type_id=content_type_id,
            rating__content=content_id,
            account=account
        ).exists()
    
    def pull_log(self, account):
        from .cached import cached_rating_log
        
        if account.is_authenticated:
            log = cached_rating_log(self.id, account.id)
            if log is None:
                return False
            else:
                return True
        else:
            return False

    def pull_log_list(self):
        from .cached import cached_rating_log_list
        return cached_rating_log_list(self.id)

    # Double Save (Touch DB)
    # def update(self):
    #     from django.db.models import Avg
    #     weight = self.log_set.aggregate(Avg('weight'))['rating__avg']
    #     self.weight = weight if weight is None else 0
    #     self.count = self.log_set.count()
    #     self.save(update_fields=['rating', 'count'])
    def get_log_list(self):
        return Log.objects.filter(rating=self)
    def api_display(self):
        d = {
                'weight': self.weight,
                'count': self.count,

            }
        return d
    
class Log(models.Model):
    from django.conf import settings
    
    rating = models.ForeignKey(Rating)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='+')
    message = models.TextField(blank=True)
    weight = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-timestamp']
        
    def api_display(self):
        from django.conf import settings
        return {
            'id': self.id,
            'account': self.account.api_display_title(),
            'message': self.message,
            'weight': self.weight,
            'timestamp': self.timestamp.strftime(settings.TIME_FORMAT)
        }


