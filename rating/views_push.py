from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from .models import Rating


@csrf_exempt
@require_POST
@login_required
def push_view(request):
    result = {}
    code = 200
    content_id = request.POST.get('content_id', None)
    content_type = request.POST.get('content_type', None)
    weight = request.POST.get('rating', None)
    message = request.POST.get('message', None)

    if content_id is None or weight is None:
        return JsonResponse({'status': code,
                             'status_msg': code,
                             'result': result})

    if content_type == 'course':
        _content_type = settings.CONTENT_TYPE('course', 'course')
    elif content_type == 'program':
        _content_type = settings.CONTENT_TYPE('program', 'program')
    else:
        _content_type = settings.CONTENT_TYPE('program', 'onboard')

    rating, log = Rating.push(_content_type,
                              content_id,
                              request.user,
                              message,
                              int(weight))
    result['template'] = render_to_string('rating/log_block.html',
                                          {'rating_log': log})
    return JsonResponse({'status': code,
                         'status_msg': code,
                         'result': result})
