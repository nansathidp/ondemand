from django.db import models


class Dependency(models.Model):
    content_type = models.ForeignKey('contenttypes.ContentType')  # For Program keep program.Item
    content = models.BigIntegerField(db_index=True)

    def __str__(self):
        return '[%s] %s: %s'%(self.id, self.content_type, self.content)

    @staticmethod
    def pull(content_type, content):
        from .cached import cached_dependency_v2
        return cached_dependency_v2(content_type, content)

    @staticmethod
    def pull_parent(content_type, content):
        from .cached import cached_dependency_parent_v2
        parent_list = cached_dependency_parent_v2(content_type, content, is_force=True)
        for parent in parent_list:
            parent.get_content_cached()
        return parent_list

    @staticmethod
    def get_content_list(type):
        from django.conf import settings
        from course.models import Course
        from question.models import Activity
        from program.models import Program

        if type == 'course':
            return settings.CONTENT_TYPE('course', 'course'), Course.objects.all()
        elif type == 'question':
            return settings.CONTENT_TYPE('question', 'activity'), Activity.objects.all()
        elif type == 'program':
            return settings.CONTENT_TYPE('program', 'program'), Program.objects.filter(type=1)
        elif type == 'onboard':
            return settings.CONTENT_TYPE('program', 'program'), Program.objects.filter(type=2)
        else:
            return None, None

    @staticmethod
    def check_content2(content_type, content_id, account):
        from django.conf import settings
        
        from progress.models import Progress
        from content.models import Location as ContentLocation
        
        from .cached import cached_dependency_v2, cached_dependency_parent_v2

        dependency = cached_dependency_v2(content_type, content_id)
        parent_list = cached_dependency_parent_v2(content_type, content_id)
        
        for parent in parent_list:
            content_type = settings.CONTENT_TYPE_ID(parent.content_type_id)
            parent.group = int(parent.sort)
            if Progress.objects.filter(account=account,
                                       content_type_id=parent.content_type_id,
                                       content=parent.content,
                                       status=2).exists():
                parent.is_complete = True
            else:
                parent.is_complete = False
        return Dependency.check_dependency(parent_list), parent_list
    
    @staticmethod
    def check_course_material(order_item, content_location, content_type, content_id): # ContentLocation
        DEBUG = False
        from django.conf import settings
        
        from progress.models import Progress
        from content.models import Location as ContentLocation
        from course.models import Course, Material
        
        from .cached import cached_dependency_v2, cached_dependency_parent_v2

        dependency = cached_dependency_v2(content_type, content_id)
        parent_list = cached_dependency_parent_v2(content_type, content_id)

        if DEBUG:
            print('-----------------')
            print('check_course_material: ', content_type, content_id)
        
        for parent in parent_list:
            # Material in Course
            if content_type == settings.CONTENT_TYPE('course', 'material'):
                _material = Material.pull(content_id)
                __material = Material.pull(parent.content)
                             
                def _parent_get_content():
                    if __material and __material.type == 3:
                        return settings.CONTENT_TYPE('question', 'activity'), __material.question_id
                    elif __material:
                        return settings.CONTENT_TYPE('course', 'material'), __material.id
                    else:
                        return None, None

                # _course = Course.pull(_material.course_id)
                if __material.type == 3:
                    _content_type, _content_id = _parent_get_content()
                    if order_item.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
                        _content_location = ContentLocation.pull_first(_content_type, _content_id,
                                                                       parent1_content_type=settings.CONTENT_TYPE('program', 'onboard'),
                                                                       parent1_content=order_item.content,
                                                                       parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                                       parent2_content=_material.course_id,
                                                                       parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                                       parent3_content=__material.id)
                    elif order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
                        _content_location = ContentLocation.pull_first(_content_type, _content_id,
                                                                       parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                                       parent1_content=order_item.content,
                                                                       parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                                       parent2_content=_material.course_id,
                                                                       parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                                       parent3_content=__material.id)
                    else:
                        _content_location = ContentLocation.pull_first(_content_type, _content_id,
                                                                       parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                                       parent1_content=_material.course_id,
                                                                       parent2_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                                       parent2_content=__material.id)
                else:
                    _content_type, _content_id = _parent_get_content()
                    if DEBUG:
                        print('_content_type', _content_type, ' _content_id : ', _content_id)
                    if order_item.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
                        _content_location = ContentLocation.pull_first(_content_type, _content_id,
                                                                       parent1_content_type=settings.CONTENT_TYPE('program', 'onboard'),
                                                                       parent1_content=order_item.content,
                                                                       parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                                       parent2_content=__material.course_id)
                    elif order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
                        _content_location = ContentLocation.pull_first(_content_type, _content_id,
                                                                       parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                                       parent1_content=order_item.content,
                                                                       parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                                       parent2_content=__material.course_id)
                    else:
                        _content_location = ContentLocation.pull_first(_content_type, _content_id,
                                                                       parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                                       parent1_content=__material.course_id)
                if DEBUG:
                    print('Debug _content_location', _content_location.id)
            else:
                _content_location = content_location
                _content_type = content_type
                _content_id = content_id
                
            progress = Progress.pull(order_item,
                                     _content_location,
                                     _content_type,
                                     _content_id)
            if DEBUG:
                print('Debug progress: ', progress.id, ' Status: ', progress.get_status_display())
            parent.group = int(parent.sort)
            if progress and progress.status == 2:
                parent.is_complete = True
            else:
                parent.is_complete = False
        return Dependency.check_dependency(parent_list), parent_list
    
    @staticmethod
    def check_dependency(parent_list):
        group = 0
        group_count = 0
        group_befor_is_dependency = False
        group_befor_condition = 1
        befor_is_dependency = False
        for parent in parent_list:
            if group == 0:
                if parent.group == group:
                    if group_count == 0:
                        befor_is_dependency = not parent.is_complete
                    else:
                        if parent.condition == 1:
                            if parent.is_complete and not befor_is_dependency:
                                befor_is_dependency = False
                            else:
                                befor_is_dependency = True
                        else:
                            if parent.is_complete or not befor_is_dependency:
                                befor_is_dependency = False
                            else:
                                befor_is_dependency = True
                    group_count += 1
                else:
                    group = parent.group
                    group_count = 0
                    group_befor_is_dependency = befor_is_dependency
                    befor_is_dependency = False
            # Not Use
            """
            else: 
                if parent.group == group:
                    if group_count == 0:
                        befor_is_dependency = parent.is_complete
                        group_befor_condition = parent.condition
                    else:
                        if parent.condition == 1:
                            if parent.is_complete and befor_is_dependency:
                                befor_is_dependency = False
                            else:
                                befor_is_dependency = True
                        else:
                            if parent.is_complete or befor_is_dependency:
                                befor_is_dependency = False
                            else:
                                befor_is_dependency = True
                    group_count += 1
                else:
                    group = parent.group
                    group_count = 0
                    befor_is_dependency = False
                    if group_befor_condition == 1:
                        if group_befor_is_dependency and befor_is_dependency:
                            group_befor_is_dependency = False
                        else:
                            group_befor_is_dependency = True
                    else:
                        if group_befor_is_dependency or befor_is_dependency:
                            group_befor_is_dependency = False
                        else:
                            group_befor_is_dependency = True
            """
        if group == 0:
            is_dependency = befor_is_dependency
        return is_dependency

    # use for program only!!
    @staticmethod
    def check_dependency_item(order_item, parent_list):
        from django.conf import settings
        from progress.models import Progress
        from program.cached import cached_program_item
        from content.models import Location as ContentLocation

        content_type_program_item = settings.CONTENT_TYPE('program', 'item')
        is_dependency = True
        for parent in parent_list:
            parent.group = int(parent.sort)
            content_type = settings.CONTENT_TYPE_ID(parent.content_type_id)
            content = parent.content
            if content_type == content_type_program_item:
                program_item = cached_program_item(parent.content)
                content_type = settings.CONTENT_TYPE_ID(program_item.content_type_id)
                content = program_item.content
            _content_type = settings.CONTENT_TYPE_ID(order_item.content_type_id)
            content_location_parent = ContentLocation.pull_first(content_type, content,
                                                                 parent1_content_type=_content_type,
                                                                 parent1_content=order_item.content)
                                                                 # parent2_content_type=settings.CONTENT_TYPE_ID(content_location.parent1_content_type_id),
                                                                 # parent2_content=content_location.parent1_content,
                                                                 # parent3_content_type=settings.CONTENT_TYPE_ID(content_location.parent2_content_type_id),
                                                                 # parent3_content=content_location.parent2_content)
            progress = Progress.pull(order_item,
                                     content_location_parent,
                                     content_type,
                                     content)
            if progress is not None:
                parent.is_complete = True if progress.status == 2 else False
            else:
                parent.is_complete = False
        return Dependency.check_dependency(parent_list)

    def add_parent(self, content_type, content, condition):
        from django.conf import settings
        from .cached import cached_dependency_parent_v2, cached_dependency_parent_delete_v2

        IS_DEBUG = False

        def find_parent(content_type, content, parent_list):
            _parent_list = cached_dependency_parent_v2(content_type, content)
            for _parent in _parent_list:
                parent_list.append([_parent.content_type_id, _parent.content])
                _content_type = settings.CONTENT_TYPE_ID(_parent.content_type_id)
                find_parent(_content_type, _parent.content, parent_list)
            return parent_list

        if self.content_type_id == content_type.id and self.content == content:
            # Add self
            return False, 'Add self content'

        _content_type = settings.CONTENT_TYPE_ID(self.content_type_id)

        # Parent of Content
        parent_list = find_parent(_content_type, self.content, [])
        for _parent in parent_list:
            if _parent[0] == content_type.id and _parent[1] == content:
                if IS_DEBUG:
                    print('add_parent: False -> Parent of Content')
                    # Loop
                return False, 'Dependency exception.'

        # Parent of Parent to add
        parent_list = find_parent(content_type, content, [])
        for _parent in parent_list:
            if _parent[0] == _content_type.id and _parent[1] == self.content:
                if IS_DEBUG:
                    print('add_parent: False -> Parent of Parent to add')
                # Duplicate
                return False, 'Duplicated content.'

        if IS_DEBUG:
            print('add_parent: Complete')
            print(content_type, content)
            print('dependency: ', self.content_type, self.content)
        parent = Parent.objects.create(dependency=self,
                                       content_type=content_type,
                                       content=content,
                                       condition=condition,
                                       sort=0.0)
        cached_dependency_parent_delete_v2(self.content_type_id, self.content)

        return parent

    def delete_parent(self, parent_id):
        from .cached import cached_dependency_parent_delete_v2
        parent = Parent.objects.filter(id=parent_id).first()
        if parent is None:
            return None, None
        else:
            parent.delete()
            cached_dependency_parent_delete_v2(self.content_type_id, self.content)
            return parent, None

    @staticmethod
    def delete_material(material_id):
        from .cached import cached_dependency_parent_delete_v2
        from django.conf import settings
        dependency = Dependency.objects.filter(content_type=settings.CONTENT_TYPE('course', 'material'),
                                               content=material_id) \
                                       .first()

        for parent in Parent.objects.select_related('dependency') \
                                    .filter(content_type=settings.CONTENT_TYPE('course', 'material'),
                                            content=material_id):
            parent.delete()
            cached_dependency_parent_delete_v2(parent.dependency.content_type_id,
                                               parent.dependency.content)
            
    
    # TODO: migrate to add_parent
    def push_parent(self, code_type, content, condition, sort):
        from django.conf import settings
        from .cached import cached_dependency_parent, cached_dependency_parent_delete

        def find_parent(content_type, content, parent_list):
            _parent_list = cached_dependency_parent('', content, content_type=content_type)
            for _parent in _parent_list:
                parent_list.append([_parent.content_type_id, _parent.content])
                _content_type = settings.CONTENT_TYPE_ID(_parent.content_type_id)
                find_parent(_content_type, _parent.content, parent_list)
            return parent_list

        if code_type == 'program':
            content_type = settings.CONTENT_TYPE('program', 'item')
        elif code_type == 'course':
            content_type = settings.CONTENT_TYPE('course', 'course')
        else:
            return None

        _content_type = settings.CONTENT_TYPE_ID(self.content_type_id)
        if _content_type == content_type and self.content == content:
            is_found = True
        else:
            is_found = False
            # Parent of Content
            parent_list = find_parent(_content_type, self.content, [])
            for _parent in parent_list:
                if _parent[0] == content_type.id and _parent[1] == content:
                    is_found = True
                    break

        if not is_found:
            # Parent of Parent to add
            parent_list = find_parent(content_type, content, [])
            for _parent in parent_list:
                if _parent[0] == _content_type.id and _parent[1] == self.content:
                    is_found = True
                    break

        if not is_found:
            parent = Parent.objects.create(dependency=self,
                                           content_type=content_type,
                                           content=content,
                                           condition=condition,
                                           sort=sort)
            cached_dependency_parent_delete(content_type.id, self.content)


class Parent(models.Model):
    CONDITION_CHOICES = (
        (1, 'AND'),
        (2, 'OR'),
    )

    dependency = models.ForeignKey(Dependency)
    content_type = models.ForeignKey('contenttypes.ContentType')
    content = models.BigIntegerField(db_index=True)
    condition = models.IntegerField(choices=CONDITION_CHOICES)
    sort = models.FloatField(default=0.0)  # Group Condition

    class Meta:
        ordering = ['sort']

    def api_display(self):
        from utils.api_image import api_image_url
        self.get_content_cached()
        content_display = None
        if self.content_cached:
            content_display = {
                'content': self.content,
                'content_type': self.content_type.name,
                'name': self.content_cached.name,
                'image': api_image_url(self.content_cached.image),
            }
        return content_display

    def get_content_cached(self):
        from django.conf import settings
        from program.cached import cached_program_item
        from utils.content_cached import _get_content_cached

        content_type_program_item = settings.CONTENT_TYPE('program', 'item')

        if self.content_type_id == content_type_program_item.id:
            self.content_type_cached = content_type_program_item
            item = cached_program_item(self.content)
            if item is not None:
                item.get_content_cached()
                self.content_cached = item.content_cached
        else:
            _get_content_cached(self)

    def get_status_html(self):
        return '<i class="zmdi zmdi-plus-circle-o zmdi-hc-fw add-content" content_type="%s" content="%s" style="cursor: pointer"></i>' % (self.content_type_id,
                                                                                                                                          self.content)
