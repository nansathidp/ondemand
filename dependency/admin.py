from django.contrib import admin

from .models import Dependency, Parent

@admin.register(Dependency)
class DependencyAdmin(admin.ModelAdmin):
    list_display = ('id', 'content_type', 'content')

@admin.register(Parent)
class ParentAdmin(admin.ModelAdmin):
    list_display = ('id', 'dependency', 'content_type', 'content', 'condition', 'sort')
