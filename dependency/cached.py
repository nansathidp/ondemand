from django.conf import settings
from django.core.cache import cache

from .models import Dependency, Parent

from utils.cached.time_out import get_time_out_day, get_time_out


def cached_dependency_v2(content_type, content):
    key = '%s_dependency_v2_%s_%s'%(settings.CACHED_PREFIX, content_type.id, content)
    result = cache.get(key)
    if result is None:
        result = Dependency.objects.filter(content_type=content_type,
                                           content=content).first()
        if result is None:
            result = Dependency.objects.create(content_type=content_type,
                                               content=content)
        cache.set(key, result, get_time_out_day())
    return None if result == -1 else result

def cached_dependency_v2_delete(content_type_id, content):
    key = '%s_dependency_v2_%s_%s'%(settings.CACHED_PREFIX, content_type_id, content)
    cache.delete(key)

def cached_dependency_parent_v2(content_type, content, is_force=False):
    key = '%s_dependency_parent_v2_%s_%s' % (settings.CACHED_PREFIX, content_type.id, content)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Parent.objects.filter(dependency__content_type=content_type,
                                       dependency__content=content)
        cache.set(key, result, get_time_out_day())
    return None if result == -1 else result

def cached_dependency_parent_delete_v2(content_type_id, content):
    key = '%s_dependency_parent_v2_%s_%s' % (settings.CACHED_PREFIX, content_type_id, content)
    cache.delete(key)

#TODO: remove
def cached_dependency(type, content, content_type=None, is_force=False):
    if content_type is None:
        if type == 'program':
            content_type = settings.CONTENT_TYPE('program', 'item')
        elif type == 'course':
            content_type = settings.CONTENT_TYPE('course', 'course')
        else:
            return None

    key = '%s_dependency_%s_%s'%(settings.CACHED_PREFIX, content_type.id, content)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Dependency.objects.filter(content_type=content_type,
                                           content=content).first()
        if result is None:
            result = Dependency.objects.create(content_type=content_type,
                                               content=content)
        cache.set(key, result, get_time_out_day())
    return None if result == -1 else result


#TODO: remove
def cached_dependency_parent(type, content, content_type=None, is_force=False):
    if content_type is None:
        if type == 'program':
            content_type = settings.CONTENT_TYPE('program', 'item')
        elif type == 'course':
            content_type = settings.CONTENT_TYPE('course', 'course')
        elif type == 'question':
            content_type = settings.CONTENT_TYPE('question', 'activity')
        elif type == 'material':
            content_type = settings.CONTENT_TYPE('course', 'material')
        else:
            return []

    key = '%s_dependency_parent_%s_%s' % (settings.CACHED_PREFIX, content_type.id, content)
    result = None if is_force else cache.get(key)
    if result is None:
        dependency = cached_dependency(type, content, content_type=content_type)
        if dependency is None:
            return []
        result = dependency.parent_set.all()
        cache.set(key, result, get_time_out_day())
    return None if result == -1 else result


def cached_dependency_parent_delete(content_type_id, content):
    key = '%s_dependency_parent_%s_%s' % (settings.CACHED_PREFIX, content_type_id, content)
    cache.delete(key)
