from django import template
from django.conf import settings

from dependency.models import Dependency
register = template.Library()


@register.assignment_tag
def check_dependency_tag(order_item, content_location, content_type, content_id, account):
    is_dependency, parent_list = Dependency.check_content2(content_type,
                                                           content_id,
                                                           account)
    return is_dependency

@register.assignment_tag
def check_dependency_course_material_tag(order_item, content_location, content_type, content_id):
    if order_item:
        is_dependency, parent_list = Dependency.check_course_material(order_item,
                                                                      content_location,
                                                                      content_type,
                                                                      content_id)
        return is_dependency
    else:
        return False
