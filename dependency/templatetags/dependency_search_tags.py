from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

register = template.Library()

@register.simple_tag
def parent_check_tag(parent_list, content_type, content_id):
    parent = None
    for _parent in parent_list:
        if _parent.content_type_id == content_type.id and _parent.content == content_id:
            parent = _parent
            break

    if parent is None:
        result = '<i class="zmdi zmdi-plus-circle-o zmdi-hc-fw add-content" content_type="%s" content="%s" style="cursor: pointer"></i>'%(content_type.id, content_id)
        condition = None
    else:
        result = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green search-delete" parent="%s" style="cursor: pointer"></i>'%parent.id
        condition = parent.condition
    return {'condition': condition,
            'result': result}
