# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-16 10:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('provider', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='App',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120)),
                ('is_master', models.BooleanField(default=False)),
                ('is_provider', models.BooleanField(default=False)),
                ('code', models.CharField(blank=True, db_index=True, max_length=32)),
                ('app_version', models.FloatField(default=0.0)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('provider', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='provider.Provider')),
            ],
        ),
        migrations.CreateModel(
            name='Content',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.BigIntegerField(db_index=True)),
                ('sort', models.IntegerField(db_index=True, default=0)),
                ('app', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.App')),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='contenttypes.ContentType')),
            ],
        ),
        migrations.CreateModel(
            name='Host',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('host', models.CharField(db_index=True, max_length=120)),
                ('app', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.App')),
            ],
        ),
    ]
