from django.contrib import admin
from app.models import App, Host, AppleAppsConfig, ApplePathConfig, AppleIdConfig


@admin.register(App)
class AppAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'is_master', 'provider', 'code','app_version', 'timestamp')


@admin.register(Host)
class HostAdmin(admin.ModelAdmin):
    list_display = ('app', 'host')


@admin.register(AppleAppsConfig)
class AppleAppsConfigAdmin(admin.ModelAdmin):
    list_display = ('id','app_name')

@admin.register(AppleIdConfig)
class AppleIdConfigAdmin(admin.ModelAdmin):
    list_display = ('id','app_id', 'apps')

@admin.register(ApplePathConfig)
class ApplePathConfigAdmin(admin.ModelAdmin):
    list_display = ('id','path', 'app_id')