from django.conf import settings
from django.core.cache import cache

from utils.cached.time_out import get_time_out_hour

from app.models import App
import hashlib


def cached_app(host, is_force=False):
    if host.find(':') != -1:
        host = host.split(':')[0]
    key = '%s_app_%s' % (settings.CACHED_PREFIX, host)
    result = None if is_force else cache.get(key)
    if result == -1:
        result = None
    if result is None:
        try:
            result = App.objects.select_related('institute').get(host__host=host)
        except:
            result = App.objects.first()
            if result is None:
                result = App.objects.create(name='gen', is_master=True)
        cache.set(key, result, get_time_out_hour())
    return None if result == -1 else result


def cached_api_app(code, is_force=False):
    h = hashlib.sha1(code.encode('utf-8')).hexdigest()
    key = '%s_api_app_%s' % (settings.CACHED_PREFIX, h)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = App.objects.get(code=code)
        except:
            result = -1
        cache.set(key, result, get_time_out_hour())
    return None if result == -1 else result

