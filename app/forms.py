from django import forms
from app.models import *

class AppForm(forms.Form):
    app = forms.ModelChoiceField(queryset=App.objects.all())