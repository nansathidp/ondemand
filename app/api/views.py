
import json
from  django.http import HttpResponse, Http404, JsonResponse
from app.models import AppleAppsConfig, ApplePathConfig, AppleIdConfig


def views_apple(request):
    if request.method == 'GET':
        app_list = AppleAppsConfig.objects.all()
        result = {
            "activitycontinuation": {
                "apps": []
            },
            "applinks": {
                "apps": [],
                "details": []
            }
        }

        if app_list is not None:
            for app_item in app_list:
                result["activitycontinuation"]["apps"].append(app_item.app_name)
                appId_list = AppleIdConfig.objects.filter(apps=app_item)
                for appId_item in appId_list:
                    app_path_list = ApplePathConfig.objects.filter(app_id=appId_item)
                    app_path = []
                    for app_path_item in app_path_list:
                        app_path.append(app_path_item.path)
                    result["applinks"]["details"].append({'appID': appId_item.app_id, 'paths': app_path})

        return JsonResponse(result)
    else:
        raise Http404
