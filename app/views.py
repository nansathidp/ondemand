def is_review(install_version, store_version):
    return float(install_version) > store_version


def update_checkout_type(result, install_version, store_version):
    try:
        install_version = float(install_version)
        store_version = float(store_version)
    except:
        install_version = 0.0
        store_version = 0.0

    if install_version > store_version:
        checkout_type = 1
        checkout_type_display = 'In-App Purchase'
    else:
        checkout_type = 2
        checkout_type_display = 'Local Payment Gateway'
    result.update({'checkout_type': checkout_type,
                   'checkout_type_display': checkout_type_display})

