from django.db import models


class App(models.Model):
    name = models.CharField(max_length=120)
    is_master = models.BooleanField(default=False)
    is_provider = models.BooleanField(default=False)
    provider = models.ForeignKey('provider.Provider', related_name='+', null=True, blank=True)
    code = models.CharField(max_length=32, blank=True, db_index=True)
    app_version = models.FloatField(default=0.0)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if len(self.code) == 0:
            self.code = App.gen_code()
        super(self.__class__, self).save(*args, **kwargs)

    @staticmethod
    def gen_code():
        import uuid
        code = None
        while True:
            code = str(uuid.uuid4()).replace('-', '')
            if App.objects.filter(code=code).count() == 0:
                break
        return code


class Host(models.Model):
    app = models.ForeignKey(App)
    host = models.CharField(max_length=120, db_index=True)

    def __str__(self):
        return self.app.name

# Migrate Status
# Course.App
# Institute.App (not)
# Lesson.App
# Subject.App -> Category (not)
# Provider.App
# Account.App
# Tutot.App


class Content(models.Model):
    app = models.ForeignKey(App)
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    content = models.BigIntegerField(db_index=True)
    sort = models.IntegerField(db_index=True, default=0)

class ApplePathConfig(models.Model):
    path = models.CharField(max_length=100, default=False)
    app_id = models.ForeignKey("app.AppleIdConfig", related_name='+')

    def __str__(self):
        return self.app_id.app_id


class AppleIdConfig(models.Model):
    app_id = models.CharField(max_length=100, default=False)
    apps = models.ForeignKey("app.AppleAppsConfig", related_name='+')

    def __str__(self):
        return self.apps.app_name


class AppleAppsConfig(models.Model):
    app_name = models.CharField(max_length=100, default=False)







