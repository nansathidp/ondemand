from django.conf import settings
from django.core.cache import cache

from .models import Iptable

from utils.cached.time_out import get_time_out_week

def cached_iptable(content_type, content):
    key = '%s_iptable_%s_%s'%(settings.CACHED_PREFIX, content_type.id, content)
    result = cache.get(key)
    if result is None:
        result = Iptable.objects.filter(content_type=content_type,
                                        content=content) \
                                .first()
        if result is None:
            result = Iptable.objects.create(content_type=content_type,
                                            content=content)
        cache.set(key, result, get_time_out_week())
    return result

def cached_iptable_delete(content_type_id, content):
    key = '%s_iptable_%s_%s'%(settings.CACHED_PREFIX, content_type_id, content)
    cache.delete(key)

