from django.db import models


class Iptable(models.Model):
    TYPE_CHOICES = (
        (1, 'Intranet/Internet'),
        (2, 'Intranet only'),
    )
    
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    content = models.BigIntegerField(db_index=True)
    type = models.IntegerField(choices=TYPE_CHOICES, default=1)
    
    @staticmethod
    def pull(content_type, content):
        from .cached import cached_iptable
        return cached_iptable(content_type, content)

    @staticmethod
    def check_ip(content_type, content, ip):
        from django.conf import settings
        import ipaddress
        
        iptable = Iptable.pull(content_type, content)
        if iptable.type == 1:
            return True
        elif iptable.type == 2:
            for allow in settings.IPTABLE__ALLOW:
                if ipaddress.ip_address(ip) in ipaddress.ip_network(allow):
                    return True
            return False
        else:
            return True
            
    def push(self):
        from .cached import cached_iptable_delete
        cached_iptable_delete(self.content_type_id, self.content)
