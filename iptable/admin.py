from django.contrib import admin
from .models import Iptable


@admin.register(Iptable)
class IptableAdmin(admin.ModelAdmin):
    pass
