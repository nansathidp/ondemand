from django.conf.urls import url

#TODO: migrate url format to from .views_xxx import xxx_view :)
from . import views

app_name = 'manual'
urlpatterns = [
    url(r'^preface/$', views.views_preface, name="preface"), #TODO: testing
    url(r'^style/$', views.views_style, name="style"), #TODO: testing
    url(r'^why/$', views.views_why, name="why"), #TODO: testing
    url(r'^order/$', views.views_order, name="order"), #TODO: testing
    url(r'^payment/$', views.views_payment, name="payment"), #TODO: testing
    url(r'^learn/$', views.views_learn, name="learn"), #TODO: testing
    url(r'^faq/$', views.views_faq, name="faq"), #TODO: testing
]
