from django.shortcuts import render


def views_preface(request):
    return render(request, 'manual/preface.html')


def views_style(request):
    return render(request, 'manual/style.html')


def views_why(request):
    return render(request, 'manual/why.html')


def views_order(request):
    return render(request, 'manual/howorder.html')


def views_learn(request):
    return render(request, 'manual/howlearn.html')


def views_payment(request):
    return render(request, 'manual/payment.html',
                  {'ROOT_PAGE': 'payment_info',
                   'DISPLAY_SUBMENU': True
                   })


def views_faq(request):
    return render(request, 'manual/faq.html',
                  {'ROOT_PAGE': 'contact',
                   'DISPLAY_SUBMENU': True,
                   })
