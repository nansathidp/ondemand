from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from ..models import SubProvider, Item
from provider.models import Provider

@csrf_exempt
def load_view(request):
    sub_provider = None
    if request.method == 'POST':
        try:
            provider_id = int(request.POST.get('provider_id', -1))
        except:
            provider_id = None
            sub_provider_list = []
        if provider_id is not None:
            provider = get_object_or_404(Provider, id=provider_id)
            sub_provider_list = SubProvider.objects.filter(provider=provider)
            app_label = request.POST.get('app_label', '')
            model = request.POST.get('model', '')
            if app_label in ['program'] and model in ['program']:
                try:
                    content = int(request.POST.get('content', -1))
                    sub_provider = SubProvider.objects.filter(item__content_type=settings.CONTENT_TYPE(app_label, model),
                                                              item__content=content).first()
                except:
                    pass
    else:
        sub_provider_list = []
    return render(request,
                  'subprovider/dashboard/load.html',
                  {'sub_provider_list': sub_provider_list,
                   'sub_provider': sub_provider})
