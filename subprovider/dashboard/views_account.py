from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ..models import SubProvider
from provider.models import Provider

from utils.paginator import paginator

def account_view(request, provider_id, sub_provider_id):
    provider = get_object_or_404(Provider, id=provider_id)
    if not provider.check_access(request):
        raise PermissionDenied

    sub_provider = get_object_or_404(SubProvider, id=sub_provider_id)
    if not sub_provider.check_access(request):
        raise PermissionDenied
    
    account_list = sub_provider.account_set.prefetch_related('account').all()
    account_list = paginator(request, account_list)
    
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Provider Management',
         'url': reverse('dashboard:provider:home')},
        {'is_active': False,
         'title': 'Sub Provider Management: %s' % provider.name,
         'url': reverse('dashboard:subprovider-dashboard:home', args=[provider_id])},
        {'is_active': True,
         'title': 'Admin: %s'%sub_provider.name}
    ]
    return render(request,
                  'subprovider/dashboard/account.html',
                  {'SIDEBAR': 'provider',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'provider': provider,
                   'sub_provider': sub_provider,
                   'account_list': account_list})
