from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404

from provider.models import Provider
from .forms import SubProviderForm
from ..models import SubProvider


def create_view(request, provider_id):
    if not request.user.has_perm('subprovider.add_subprovider',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    provider = get_object_or_404(Provider, id=provider_id)
    if not provider.check_access(request):
        raise PermissionDenied
    
    if request.method == 'POST':
        sub_provider_form = SubProviderForm(request.POST, request.FILES, instance=SubProvider(provider=provider))

        if sub_provider_form.is_valid():
            sub_provider = sub_provider_form.save()
            return redirect('dashboard:subprovider-dashboard:home', provider.id)
    else:
        sub_provider_form = SubProviderForm()
        
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Learning Center Management',
         'url': reverse('dashboard:provider:home')},
        {'is_active': False,
         'title': 'Sub Provider Management: %s' % provider.name,
         'url': reverse('dashboard:subprovider-dashboard:home', args=[provider.id])},
        {'is_active': True,
         'title': 'Sub Learning Center Create'}
    ]
    return render(request,
                  'subprovider/dashboard/create.html',
                  {'SIDEBAR': 'provider',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'provider': provider,
                   'sub_provider_form': sub_provider_form})
