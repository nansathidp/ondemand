from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ..models import SubProvider
from course.models import Course

from .forms import SubProviderForm

def detail_view(request, provider_id, sub_provider_id):
    sub_provider = get_object_or_404(SubProvider, id=sub_provider_id)

    if request.method == 'POST':
        if not request.user.has_perm('subprovider.change_subprovider',
                                     group=request.DASHBOARD_GROUP):
            raise PermissionDenied

        sub_provider_form = SubProviderForm(request.POST, request.FILES, instance=sub_provider)

        if sub_provider_form.is_valid():
            sub_provider = sub_provider_form.save()
    sub_provider_form = SubProviderForm(instance=sub_provider)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Learning Center Management',
         'url': reverse('dashboard:provider:home')},
        {'is_active': False,
         'title': 'Sub Provider Management',
         'url': reverse('dashboard:subprovider-dashboard:home', args=[provider_id])},
        {'is_active': True,
         'title': 'Learning Center Create'}
    ]
    return render(request,
                  'subprovider/dashboard/detail.html',
                  {'SIDEBAR': 'provider',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'sub_provider_form': sub_provider_form})
