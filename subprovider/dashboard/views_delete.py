from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import SubProvider
from provider.models import Provider

def delete_view(request, provider_id, sub_provider_id):
    if not request.user.has_perm('subprovider.delete_subprovider',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    provider = get_object_or_404(Provider, id=provider_id)
    if not provider.check_access(request):
        raise PermissionDenied

    sub_provider = get_object_or_404(SubProvider, provider=provider, id=sub_provider_id)
    if not sub_provider.check_access(request):
        raise PermissionDenied

    sub_provider.delete()
    return redirect('dashboard:subprovider-dashboard:home', provider.id)
