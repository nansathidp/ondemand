from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ..models import SubProvider
from provider.models import Provider
from course.models import Course

from utils.paginator import paginator

def home_view(request, provider_id):
    provider = get_object_or_404(Provider, id=provider_id)
    
    if not request.user.has_perm('provider.view_provider', group=request.DASHBOARD_GROUP) and \
       not request.user.has_perm('provider.view_own_provider', group=request.DASHBOARD_GROUP) and \
       not request.user.has_perm('subprovider.view_subprovider', group=request.DASHBOARD_GROUP) and \
       not request.user.has_perm('subprovider.view_own_subprovider', group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    sub_provider_list = SubProvider.objects.filter(provider=provider)
    sub_provider_list = paginator(request, sub_provider_list)
    #for provider in provider_list:
    #    provider.course_count = Course.objects.filter(tutor__providers=provider).distinct().count()
    #    provider.test_count = 0
    #    provider.account_count = provider.account_set.count()
        
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Learning Center Management',
         'url': reverse('dashboard:provider:home')},
        {'is_active': True,
         'title': 'Sub Provider Management: %s' % provider.name}
    ]
    return render(request,
                  'subprovider/dashboard/home.html',
                  {'SIDEBAR': 'provider',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'provider': provider,
                   'sub_provider_list': sub_provider_list})
