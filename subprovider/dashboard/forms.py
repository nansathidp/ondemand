from django import forms

from ..models import SubProvider


class SubProviderForm(forms.ModelForm):
    class Meta:
        model = SubProvider
        fields = ['name',
                  'image',
                  'is_display']
        labels = {
            'name': 'ชื่อ<br/> Learning Center Name',
            'image': 'รูปประกอบ<br/> Image',
            'is_display': 'Display',
        }
        help_texts = {
            'name': '(60)',
            'image': '* Size ( 250 * 250 )',
        }
