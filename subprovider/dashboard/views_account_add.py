from django.shortcuts import render, get_object_or_404, redirect
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from subprovider.cached import cached_sub_provider_account_delete
from ..models import SubProvider, Account as SubProviderAccount
from provider.models import Provider
from account.models import Account


def account_add_view(request, provider_id, sub_provider_id):
    if not request.user.has_perm('subprovider.change_subprovider',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    provider = get_object_or_404(Provider, id=provider_id)
    if not provider.check_access(request):
        raise PermissionDenied

    sub_provider = get_object_or_404(SubProvider, id=sub_provider_id, provider=provider)
    if not sub_provider.check_access(request):
        raise PermissionDenied
    
    if request.method == 'POST':
        email = request.POST.get('email', '')
        if len(email) > 0:
            account = Account.objects.filter(email__icontains=email).first()
            if account is not None:
                SubProviderAccount.push(sub_provider, account)
                cached_sub_provider_account_delete(account.id)
                return redirect('dashboard:subprovider-dashboard:account', provider.id, sub_provider.id)
                
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Provider Management',
         'url': reverse('dashboard:provider:home')},
        {'is_active': False,
         'title': 'Sub Provider Management: %s' % provider.name,
         'url': reverse('dashboard:subprovider-dashboard:home', args=[provider_id])},
        {'is_active': False,
         'title': 'Admin: %s' % sub_provider.name,
         'url': reverse('dashboard:subprovider-dashboard:account', args=[provider.id, sub_provider.id])},
        {'is_active': True,
         'title': 'Add Admin'}
    ]
    return render(request,
                  'provider/dashboard/account_add.html',
                  {'SIDEBAR': 'provider',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'provider': provider,
                   'sub_provider': sub_provider})
