from django.conf.urls import url

from .views import home_view
from .views_create import create_view
from .views_detail import detail_view
from .views_delete import delete_view

from .views_account import account_view
from .views_account_add import account_add_view
from .views_account_delete import account_delete_view

from .views_load import load_view

app_name = 'subprovider-dashboard'
urlpatterns = [
    url(r'^(\d+)/$', home_view, name='home'), #TODO: testing
    url(r'^(\d+)/create/$', create_view, name='create'), #TODO: testing
    url(r'^(\d+)/(\d+)/$', detail_view, name='detail'), #TODO: testing
    url(r'^(\d+)/(\d+)/delete/$', delete_view, name='delete'), #TODO: testing
    
    url(r'^(\d+)/(\d+)/admin/$', account_view, name='account'), #TODO: testing
    url(r'^(\d+)/(\d+)/admin/add/$', account_add_view, name='account_add'), #TODO: testing
    url(r'^(\d+)/(\d+)/admin/(\d+)/delete/$', account_delete_view, name='account_delete'), #TODO: testing
    
    url(r'^load/$', load_view, name='load'), #TODO: testing
]
