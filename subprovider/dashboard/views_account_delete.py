from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import SubProvider, Account
from provider.models import Provider

from ..cached import cached_sub_provider_account_delete


def account_delete_view(request, provider_id, sub_provider_id, account_id):
    if not request.user.has_perm('subprovider.change_subprovider',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    provider = get_object_or_404(Provider, id=provider_id)
    if not provider.check_access(request):
        raise PermissionDenied

    sub_provider = get_object_or_404(SubProvider, id=sub_provider_id, provider=provider)
    if not sub_provider.check_access(request):
        raise PermissionDenied

    account = get_object_or_404(Account, sub_provider_id=sub_provider.id, id=account_id)
    account_id = account.account_id
    account.delete()
    cached_sub_provider_account_delete(account_id)
    if request.DASHBOARD_SUB_PROVIDER is not None and request.DASHBOARD_SUB_PROVIDER.id == sub_provider_id:
        request.DASHBOARD_SUB_PROVIDER = None
    return redirect('dashboard:subprovider-dashboard:account', provider.id, sub_provider.id)
