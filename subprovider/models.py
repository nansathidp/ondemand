from django.db import models


class SubProvider(models.Model):
    provider = models.ForeignKey('provider.Provider', on_delete=models.CASCADE)
    name = models.CharField(max_length=120)
    image = models.ImageField(upload_to='subprovider/')
    
    is_display = models.BooleanField(default=True, db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')

    def __str__(self):
        return self.name

    @staticmethod
    def pull(id):
        #TODO: cached
        return SubProvider.objects.filter(id=id).first()
    
    def check_access(self, request):
        if request.user.has_perm('subprovider.view_subprovider',
                                 group=request.DASHBOARD_GROUP):
            return True
        elif request.user.has_perm('subprovider.view_own_subprovider',
                                   group=request.DASHBOARD_GROUP):
            return self.account_set.filter(account=request.user).exists()
        else:
            return False
        
    def push_item(self, content_type, content):
        item = Item.objects.filter(content_type=content_type,
                                   content=content).first()
        if item is None:
            Item.objects.create(sub_provider=self,
                                content_type=content_type,
                                content=content)
        elif item.sub_provider_id != self.id:
            item.sub_provider = self
            item.save(update_fields=['sub_provider'])


# TODO: migrate to app Owner
class Item(models.Model):
    sub_provider = models.ForeignKey(SubProvider)
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='sub_provider_item_set')
    content = models.BigIntegerField(db_index=True)
    sort = models.FloatField(default=0.0, db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True)


class Account(models.Model):
    from django.conf import settings
    
    sub_provider = models.ForeignKey(SubProvider)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='sub_provider_account_set')
    
    @staticmethod
    def push(sub_provider, account):
        from django.contrib.auth.models import Group
        from .cached import cached_sub_provider_account_delete
        if not Account.objects.filter(sub_provider=sub_provider, account=account).exists():
            Account.objects.create(sub_provider=sub_provider,
                                   account=account)
            cached_sub_provider_account_delete(account.id)
        #TODO: user dashboard.Config to keep group_id
        #try:
        #    if not account.groups.filter(id=7).exists():
        #        account.groups.add(7)
        #except:
        #    pass


