from django.apps import AppConfig


class SubproviderConfig(AppConfig):
    name = 'subprovider'
