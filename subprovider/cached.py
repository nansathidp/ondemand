from django.conf import settings
from django.core.cache import cache

from .models import SubProvider

from utils.cached.time_out import get_time_out_day


def _sub_provider_list(sub_provider_list):
    result = []
    for _ in sub_provider_list:
        sub_provider = cached_sub_provider(_)
        if sub_provider is not None:
            result.append(sub_provider)
    return result


def cached_sub_provider(sub_provider_id, is_force=False):
    key = '%s_sub_provider_%s' % (settings.CACHED_PREFIX, sub_provider_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = SubProvider.objects.get(id=sub_provider_id)
        except:
            result = -1
        cache.set(key, result, None)
    return None if result == -1 else result


def cached_sub_provider_account(account_id):
    key = '%s_sub_provider_account_%s' % (settings.CACHED_PREFIX, account_id)
    result = cache.get(key)
    if result is None:
        result = SubProvider.objects.values_list('id', flat=True).filter(account__account_id=account_id)
        cache.set(key, result, get_time_out_day())
    return _sub_provider_list(result)


def cached_sub_provider_account_provider(account_id, provider_id):
    key = '%s_sub_provider_account_provider_%s' % (settings.CACHED_PREFIX, account_id)
    result = cache.get(key)
    if result is None:
        result = SubProvider.objects.filter(account__account_id=account_id, provider_id=provider_id).first()
        cache.set(key, result, get_time_out_day())
    return result


def cached_sub_provider_account_delete(account_id):
    key = '%s_sub_provider_account_%s' % (settings.CACHED_PREFIX, account_id)
    cache.delete(key)
