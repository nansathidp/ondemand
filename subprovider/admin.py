from django.contrib import admin

from .models import SubProvider, Item, Account

@admin.register(SubProvider)
class SubProviderAdmin(admin.ModelAdmin):
    list_display = ('provider', 'name')

@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('sub_provider', 'get_content_type', 'content', 'sort', 'timestamp')

    def get_content_type(self, obj):
        return '%s.%s'%(obj.content_type.model, obj.content_type.name)

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('sub_provider', 'account')
