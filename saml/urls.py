from django.conf.urls import include, url

from .views import index, attrs

app_name = 'saml'
urlpatterns = [
    url(r'^$', index, name='home'),
    url(r'^attrs/$', attrs, name='attrs'),
]

