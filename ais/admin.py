from django.contrib import admin

from .models import Login, AccountInfo, PNSPush, PNSSubscriber, PNSResult


@admin.register(Login)
class LoginAdmin(admin.ModelAdmin):
    list_display = ('status_code', 'timestamp')


@admin.register(AccountInfo)
class AccountInfoAdmin(admin.ModelAdmin):
    list_display = ('account', 'status', 'employee', 'username',
                    'job_code', 'hire_date', 'primary_position',
                    'phone', 'address1', 'address2', 'city', 'province',
                    'login_method', 'timeupdate')
    list_filter = ('status', )

@admin.register(PNSSubscriber)
class PNSSubscriberAdmin(admin.ModelAdmin):
    list_display = ('request', 'response', 'timestamp')


@admin.register(PNSPush)
class PNSPushAdmin(admin.ModelAdmin):
    list_display = ('request', 'method', 'ref_id', 'response', 'status', 'timestamp')



@admin.register(PNSResult)
class PNSResultAdmin(admin.ModelAdmin):
    list_display = ('push', 'response', 'user_id', 'status')

