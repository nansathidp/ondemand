import django
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings

if settings.PROJECT != 'ais':
    print('This script run on PROJECT "aisondemand" only. Plesase check settings.py')
    exit()

from account.models import Account
from ais.models import AccountInfo
from organization.models import Parent
from department.models import Department, Member

import datetime

import logging

logger = logging.getLogger('ais-om')
hdlr = logging.FileHandler('ais-om-%s.log' % datetime.datetime.today().strftime("%Y-%m-%d"))
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

ORGANIZATION_LIST = []

ACCOUNT_INFO = {}
ACCOUNT = {}
DEPARTMENT = {}
DEPARTMENT_MEMBER = {}


def _get_file():
    os.chdir(settings.OM_PATH)
    files = filter(os.path.isfile, os.listdir(settings.OM_PATH))
    files = [os.path.join(settings.OM_PATH, f) for f in files]
    files.sort(key=lambda x: os.path.getmtime(x), reverse=True)
    if len(files) > 0:
        return files[0]
    else:
        return None


def _push_account(data, account):
    def _gender(gender):
        if gender == 'M':
            return 1
        elif gender == 'F':
            return 2
        else:
            return 0

    is_insert = False
    if account is None:
        email = data[6].lower().strip()
        if email in ACCOUNT:
            account = ACCOUNT[email]
        else:
            account = None
        if account is None:
            is_insert = True
            account = Account.objects.create(
                email=email,
                first_name=data[3],
                last_name=data[4],
                gender=_gender(data[5])
            )
    if not is_insert:
        if account.first_name != data[3] or account.last_name != data[4] or account.gender != _gender(data[5]):
            account.first_name = data[3]
            account.last_name = data[4]
            account.gender = _gender(data[5])
            account.save()
    return account


def _push_info(data):
    def _status(status):
        if status == 'Active':
            return 1
        else:
            return -1

    def _hire_date(date):
        p = date.split('/')
        try:
            return datetime.date(int(p[2]), int(p[0]), int(p[1]))
        except:
            return None

    def _is_change(_):
        if _.status != _status(data[0]):
            return True
        if _.username != data[2]:
            return True
        if _.company != data[9][:3]:
            return True
        if _.job_code != data[17]:
            return True
        if _.hire_date != _hire_date(data[19]):
            return True
        if _.primary_position != data[20]:
            return True
        if _.phone != data[21][:12]:
            return True
        if _.address1 != data[22]:
            return True
        if _.address2 != data[23]:
            return True
        if _.city != data[24]:
            return True
        if _.province != data[25]:
            return True
        if _.buh != buh:
            return True
        if _.bu != bu:
            return True
        if _.section != section:
            return True
        if _.login_method != data[31]:
            return True
        return False

    global ORGANIZATION_LIST, ACCOUNT_INFO
    employee = int(data[1])
    if employee in ACCOUNT_INFO:
        account_info = ACCOUNT_INFO[employee]
    else:
        account_info = None
    buh = data[13][:255]
    bu = data[14][:255]
    section = data[15][:255]
    if account_info is None:
        account = _push_account(data, None)
        account_info = AccountInfo.objects.create(
            account=account,
            status=_status(data[0]),
            employee=employee,
            username=data[2],
            company=data[9][:120],
            job_code=data[17],
            hire_date=_hire_date(data[19]),
            primary_position=data[20],
            phone=data[21][:120],
            address1=data[22],
            address2=data[23],
            city=data[24],
            province=data[25],
            buh=buh,
            bu=bu,
            section=section,
            login_method=data[31]
        )
    else:
        account = _push_account(data, account_info.account)
        if _is_change(account_info):
            account_info.status = _status(data[0])
            account_info.username = data[2]
            account_info.company = data[9][:120]  # Fix 2017-08-01
            account_info.job_code = data[17]
            account_info.hire_date = _hire_date(data[19])
            account_info.primary_position = data[20]

            account_info.phone = data[21][:120]  # Fix 2017-08-01
            account_info.address1 = data[22]
            account_info.address2 = data[23]
            account_info.city = data[24]
            account_info.province = data[25]

            account_info.buh = buh
            account_info.bu = bu
            account_info.section = section

            account_info.login_method = data[31]
            account_info.save()

    if account_info.status == -1:
        try:
            if data[32].lower().strip() == 'resigned':
                account.deactivate()
        except:
            pass

    try:
        ORGANIZATION_LIST.append([account.id, int(data[7])])
    except:
        pass
    _push_department(account, data[10], data[20])


def _push_department(account, name, position):
    global DEPARTMENT, DEPARTMENT_MEMBER
    if name in DEPARTMENT:
        department = DEPARTMENT[name]
    else:
        department = Department.objects.create(name=name)

    if account.id in DEPARTMENT_MEMBER:
        member = DEPARTMENT_MEMBER[account.id]
        if member.department_id != department.id or member.position != position:
            member.department = department
            member.position = position[:255]
            member.save()
    else:
        Member.objects.create(
            department=department,
            account=account,
            position=position
        )


def _push_organization():
    global ORGANIZATION_LIST

    account_parent_dict = {}
    for parent in Parent.objects.all():
        if parent.account_id in account_parent_dict:
            parent.delete()
        else:
            account_parent_dict[parent.account_id] = parent

    _pull_account_info()
    _parent_create_list = []
    for account_id, employee_id in ORGANIZATION_LIST:
        if account_id == employee_id or employee_id == 0:
            continue
        if employee_id in ACCOUNT_INFO:
            account_info = ACCOUNT_INFO[employee_id]
        else:
            if account_id in account_parent_dict:
                account_parent_dict[account_id].delete()
            continue
        if account_id in account_parent_dict:
            parent = account_parent_dict[account_id]
            if parent.parent_id != account_info.account_id:
                parent.parent_id = account_info.account_id
                parent.save()
        else:
            _parent_create_list.append(
                Parent(
                    parent_id=account_info.account_id,
                    account_id=account_id
                )
            )

    # Organization v.2
    parent_dict = {}
    child_dict = {}
    for parent in Parent.objects.all():
        if parent.parent_id in parent_dict:
            parent_dict[parent.parent_id].append(parent.account_id)
        else:
            parent_dict[parent.parent_id] = [parent.account_id]

    def _check_child(key, child):
        return child in child_dict[key]

    def _add_child(key, child_list):
        for child in child_list:
            if not _check_child(key, child):
                child_dict[key].append(child)
            else:
                return None
            if child in parent_dict:
                _add_child(key, parent_dict[child])

    for key, value in parent_dict.items():
        child_dict[key] = []
        _add_child(key, value)

    account_id_dict = {}
    for account in Account.objects.all():
        account_id_dict[account.id] = account

    _child_delete_id_list = []
    for child in Child.objects.all():
        if child.account_id in child_dict:
            if child.child_id in child_dict[child.account_id]:  # Exists.
                child_dict[child.account_id].remove(child.child_id)
            else:  # Delete (with Child)
                _child_delete_id_list.append(child.id)
        else:  # Delete (Account)
            _child_delete_id_list.append(child.id)
    if len(_child_delete_id_list) > 0:
        Child.objects.filter(id__in=_child_delete_id_list).delete()

    _child_create_list = []
    for account_id, value in child_dict.items():
        for child_id in value:
            if account_id in account_id_dict and child_id in account_id_dict:
                _child_create_list.append(
                    Child(
                        account=account_id_dict[account_id],
                        child=account_id_dict[child_id]
                    )
                )
    if len(_child_create_list) > 0:
        Child.objects.bulk_create(_child_create_list)


def _pull_account():
    global ACCOUNT
    for account in Account.objects.all():
        ACCOUNT[account.email] = account


def _pull_account_info():
    global ACCOUNT_INFO
    for account_info in AccountInfo.objects.select_related('account').all():
        ACCOUNT_INFO[account_info.employee] = account_info


def _pull_department():
    global DEPARTMENT
    for department in Department.objects.all():
        DEPARTMENT[department.name] = department


def _pull_department_member():
    global DEPARTMENT_MEMBER
    for member in Member.objects.all():
        if member.account_id in DEPARTMENT_MEMBER:
            member.delete()
        else:
            DEPARTMENT_MEMBER[member.account_id] = member


if __name__ == '__main__':
    path = _get_file()
    if path is None:
        print('file Not Found.')
        exit()

    _pull_account()
    _pull_account_info()
    _pull_department()
    _pull_department_member()
    count = 0
    with open(path) as f:
        for line in f:
            count += 1
            if count <= 2:
                continue
            try:
                line = line.split('|')
                _push_info(line)
            except:
                logger.error('line : %s' % count)
                logger.error('data : %s' % line)
                # if count > 100:
                #    break
    logger.info('count : %s' % count)
    try:
        _push_organization()
    except:
        logger.error('organization fail.')
    logger.info('count department : %s' % len(DEPARTMENT_DICT))
    logger.info('count organization : %s' % len(ORGANIZATION_LIST))
