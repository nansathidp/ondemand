import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings
if settings.PROJECT != 'aisondemand':
    print('This script run on PROJECT "aisondemand" only. Plesase check settings.py')
    exit()

from account.models import Account
from ais.models import AccountInfo
from organization.models import Parent


import datetime

ORGANIZATION_LIST = []
DEPARTMANR_RAND = ['BSI', 'SOLUTION', 'TKM', 'ACC', 'CMD', 'CSM', 'HR']
DEPARTMANR_DICT = {}


def _get_file():
    return os.path.join(settings.BASE_DIR, 'ais', 'script', 'data', 'mimotechcoT1_EMP_20160802.csv')


def _push_account(data, account):
    def _gender(gender):
        if gender == 'M':
            return 1
        elif gender == 'F':
            return 2
        else:
            return 0
    is_insert = False
    if account is None:
        account = Account.objects.filter(email=data[6]).first()
        if account is None:
            is_insert = True
            account = Account(email=data[6],
                              first_name=data[3],
                              last_name=data[4],
                              gender=_gender(data[5]))
    if not is_insert:
        account.first_name = data[3]
        account.last_name = data[4]
        account.gender = _gender(data[5])
    account.save()
    return account


def _push_info(data):
    def _status(status):
        if status == 'Active':
            return 1
        else:
            return -1
        
    def _hire_date(date):
        p = date.split('/')
        try:
            return datetime.date(int(p[2]), int(p[0]), int(p[1]))
        except:
            return None

    global ORGANIZATION_LIST
    employee = int(data[1])
    account_info = AccountInfo.objects.select_related('account').filter(employee=employee).first()
    if account_info is None:
        account = _push_account(data, None)
        account_info = AccountInfo.objects.create(account=account,
                                                  status=_status(data[0]),
                                                  employee=employee,
                                                  username=data[2],
                                                  company=data[9],
                                                  job_code=data[17],
                                                  hire_date=_hire_date(data[19]),
                                                  primary_position=data[20],
                                                  phone=data[21],
                                                  address1=data[22],
                                                  address2=data[23],
                                                  city=data[24],
                                                  province=data[25],
                                                  login_method=data[31])

    else:
        account = _push_account(data, account_info.account)
        account_info.status = _status(data[0])
        account_info.username = data[2]
        account_info.company = data[9]
        account_info.job_code = data[17]
        account_info.hire_date = _hire_date(data[19])
        account_info.primary_position = data[20]

        account_info.phone = data[21]
        account_info.address1 = data[22]
        account_info.address2 = data[23]
        account_info.city = data[24]
        account_info.province = data[25]

        account_info.login_method = data[31]
        account_info.save()
    try:
        ORGANIZATION_LIST.append([account.id, int(data[7])])
    except:
        pass
    try:
        import random
        _push_department(account, DEPARTMANR_RAND[random.randrange(7)], data[20])
    except:
        pass


def _push_department(account, dept_name, position):
    global DEPARTMANR_DICT
    from department.models import Department, Member
    if not Member.objects.filter(account=account).exists():
        if dept_name not in DEPARTMANR_DICT:
            department, created = Department.objects.get_or_create(
                name=dept_name
            )
            DEPARTMANR_DICT[dept_name] = department
        else:
            department = DEPARTMANR_DICT[dept_name]

            member, created = Member.objects.get_or_create(
                account=account,
                department=department
            )
            if created:
                if member.position != position:
                    member.position = position
                    member.save(update_fields=['position'])
            else:
                member.position = position
                member.save(update_fields=['position'])


def _push_organization():
    global ORGANIZATION_LIST
    for account_id, employee_id in ORGANIZATION_LIST:
        #print(account_id, employee_id)
        account_info = AccountInfo.objects.filter(employee=employee_id).first()
        if account_info is not None:
            parent = Parent.objects.filter(account_id=account_id).first()
            if parent is not None and parent.parent_id != account_info.account_id:
                parent.parent_id = account_info.account_id
                parent.save(update_fields=['parent'])
            else:
                Parent.objects.create(parent_id=account_info.account_id,
                                      account_id=account_id)
            
    
if __name__ == '__main__':
    path = _get_file()
    count = 0
    
    with open(path) as f:
        for line in f:
            count += 1
            if count <= 2:
                continue
            line = line.split('|')
            _push_info(line)
            if count > 100:
               break
    _push_organization()
