import os, sys, django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from account.models import Account
from django.contrib.auth.models import Group
from provider.models import Provider, Account as ProviderAccount

if __name__ == '__main__':
    p = [
        "somphopp", "ACC", 3, 5,
        "arunraty", "ACC", 3, 5,
        "suttimaw", "ACC", 3, 5,
        "wichitk", "ACC", 3, 5,
        "jantimap", "ACC", 3, 5,
        "korakrit", "ACC", 3, 5,
        "nantanit", "ACC", 3, 5,
        "athiwits", "TKM", 3, 6,
        "veerasan", "TKM", 3, 6,
        "yosawadb", "TKM", 3, 6,
        "aujzharw", "TKM", 3, 6,
        "kornkans", "TKM", 3, 6,
        "sukanyat", "TKM", 3, 6,
        "supawaru", "TKM", 3, 6,
        "jutharuk", "TKM", 3, 6,
        "chutimpr", "TKM", 3, 6,
        "suvimoka", "TKM", 3, 6,
        "tassirib", "TKM", 3, 6,
        "nanthiyn", "TKM", 3, 6,
        "eakapolc", "TKM", 3, 6,
        "pongtepc", "TKM", 3, 6,
        "suleepoc", "TKM", 3, 6,
        "woravits", "TKM", 3, 6,
        "ketsarit", "TKM", 3, 6,
        "chanansp", "CSM", 3, 3,
        "sunantau", "CSM", 3, 3,
        "kamonrak", "CSM", 3, 3,
        "kanlayak", "CSM", 3, 3,
        "nathamon", "CSM", 3, 3,
        "sujitrad", "CSM", 3, 3,
        "surisabo", "CSM", 3, 3,
        "aroonral", "CSM", 3, 3,
        "pamonpom", "CSM", 3, 3,
        "chonlast", "CSM", 3, 3,
        "sukumalt", "CSM", 3, 3,
        "nopanans", "CSM", 3, 3,
        "songkiau", "ACM", 1, 0,
        "wirachab", "ACM", 1, 0,
        "thanapas", "HRIS", 5, 0,
        "kittichm", "HRIS", 5, 0,
        "wanitchw", "ACM", 1, 0,
        "umapornh", "ECC", 1, 0,
        "kamonwan", "ECC", 1, 0,
        "kraisorj", "ACM", 1, 0,
        "watchame", "S", 1, 0,
        "thunyals", "S", 1, 0,
        "porramec", "S", 1, 0,
        "pornchti", "S", 1, 0]

    c = 0
    for _ in p:
        c += 1
        if c == 5:
            c = 1
            print(u, g, gi, pi)
            email = '%s@carbon.super' % u
            account = Account.objects.filter(email=email).first()
            if account is None:
                account = Account.objects.create_user(email, '1234567')
                account.save()
            group = Group.objects.filter(id=gi).first()
            account.groups.set([group])
            if pi > 0:
                provider = Provider.objects.filter(id=pi).first()
                if provider is not None:
                    ProviderAccount.push(provider, account)

        if c == 1:
            u = _
        elif c == 2:
            g = _
        elif c == 3:
            gi = _
        elif c == 4:
            pi = _
