from django.conf import settings
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from course.models import Course
from featured.models import Highlight
from ondemand.views_decorator import check_project
from rating.models import Rating


@csrf_exempt
@check_project
def home_view(request):
    announcement = Highlight.pull_list('announcement').first()
    banner_list = Highlight.pull_list('banner')

    course_list = Course.pull_latest_list(request.APP, 8)
    content_type = settings.CONTENT_TYPE('course', 'course')
    for course in course_list:
        course.tutor_list = course.get_tutor_list()
        course.rating = Rating.pull_first(content_type, course.id)

    course_popular_list = Rating.popular_list(settings.CONTENT_TYPE('course', 'course'), 8)
    for course in course_popular_list:
        course.tutor_list = course.get_tutor_list()
        course.rating = Rating.pull_first(content_type, course.id)
    return render(request,
                  'home.html',
                  {'ROOT_PAGE': 'home',
                   'DISPLAY_SUBMENU': True,
                   'DISPLAY_CHAT': True,
                   'announcement': announcement,
                   'banner_list': banner_list,
                   'course_list': course_list,
                   'course_popular_list': course_popular_list})

