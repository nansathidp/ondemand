import urllib

import requests
from account.models import Account
from api.v4.views import json_render, check_key
from django.conf import settings
from django.contrib.auth import authenticate
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from term.models import Term


@csrf_exempt
def login_view(request):
    result = {}
    code = 200
    response = check_key(request)
    if response is not None:
        return response
    if request.method == 'POST':
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        uuid = request.POST.get('uuid', None)
    else:
        email = request.GET.get('email', None)
        password = request.GET.get('password', None)
        uuid = request.GET.get('uuid', None)

    if email is not None:
        email = email.lower().strip()

    if password is not None:
        password = password.strip()

    if uuid is not None and len(uuid) > 0:
        pass
    else:
        return json_render({}, 202)

    if email is None or len(email) < 4 or password is None or len(password) < 4:
        return json_render({}, 300)

    email = urllib.parse.unquote(email)
    password = urllib.parse.unquote(password)
    if email == 'kidsdev@gmail.com' or settings.OAUTH_URL is None:
        if email is None or len(email) < 4 or password is None or len(password) < 4:
            return json_render({}, 300)
        else:
            email = urllib.parse.unquote(email)
            password = urllib.parse.unquote(password)
            account = authenticate(email=email, password=password)

            if account is not None:
                result['token'] = account.login_token()
                if account.first_active is None:
                    account.first_active = timezone.now()
                account.last_active = timezone.now()
                account.update_uuid(uuid, is_login_facebook=None)
                account.device_log(uuid, 1)  # Basic Login
                account.save()

                result['profile'] = account.api_profile()
                result['is_require_information'] = False

                term = Term.pull_content(settings.CONTENT_TYPE('app', 'app').id,
                                         request.APP.id)
                if term:
                    result['terms'] = term.api_display()
                    result['is_require_terms'] = Term.is_require_term(settings.CONTENT_TYPE('app', 'app').id,
                                                                      request.APP.id,
                                                                      account.id)
                else:
                    result['terms'] = None
                    result['is_require_terms'] = False
            else:
                return json_render({}, 300)
    else:
        if email is None or len(email) < 4 or password is None or len(password) < 4:
            return json_render({}, 300)
        else:
            r = requests.post(settings.OAUTH_URL,
                              data={'client_id': settings.OAUTH_CLIENT_ID,
                                    'client_secret': settings.OAUTH_CLIENT_SECRET,
                                    'grant_type': 'password',
                                    'username': email,
                                    'password': password,
                                    'scope': 'openid'})
            try:
                data = r.json()
            except:
                return json_render({}, 300)

            if 'refresh_token' in data:
                email = urllib.parse.unquote(email)
                if email.find('@ais.co.th') == -1:
                    email = '%s@ais.co.th' % email

                account = Account.objects.filter(email=email).first()
                if account is not None:
                    result['token'] = account.login_token()
                    if account.first_active is None:
                        account.first_active = timezone.now()
                    account.last_active = timezone.now()
                    account.update_uuid(uuid, is_login_facebook=None)
                    account.device_log(uuid, 1)  # Basic Login
                    account.save()
                else:
                    account = Account.objects.create_user(email, 'bIPQTt/nl6QySrH3SUObDT1dx4iusVMIKkJJLIpgtoQ=')
                    account.save()

                result['profile'] = account.api_profile()
                result['is_require_information'] = False

                term = Term.pull_content(settings.CONTENT_TYPE('app', 'app').id,
                                         request.APP.id)
                if term:
                    result['terms'] = term.api_display()
                    result['is_require_terms'] = Term.is_require_term(settings.CONTENT_TYPE('app', 'app').id,
                                                                      request.APP.id,
                                                                      account.id)
                else:
                    result['terms'] = None
                    result['is_require_terms'] = False
            else:
                return json_render({}, 300)
    return json_render(result, code)
