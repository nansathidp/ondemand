from django.http import JsonResponse

_STATUS_MSG = {
    200: 'Success',
    201: 'Require POST method.',
    202: 'Require UUID.',
    204: 'Require token.',
    209: 'Require Store.',

    300: 'The username or password were incorrect.',  # The username or password were incorrect.
    301: 'Login Facebook Exception [301]',  # Facebook Login : access token cannot be null
    302: 'Facebook Login : Please Signup.',  # Facebook Login : Please Signup.
    303: 'Login Facebook Exception [303]',  # Facebook Login : access token fail.
    304: 'Login Facebook Exception [304]',  # Facebook Connect : access token cannot be null.
    305: 'Login Facebook Exception [305]',  # Facebook Connect : access token fail.
    306: 'Login Exception [306]',  # The username or password were incorrect.
    307: 'Duplicate Facebook Account',  # The username or password were incorrect.
    308: 'Require Access token',  # The username or password were incorrect.
    309: 'UUID Account not found.',
    310: 'Duplicate Email Address',
    311: 'Exception',
    312: 'Facebook Exception , Please register with email.',
}

try:
    from .views import STATUS_MSG
except:
    STATUS_MSG = _STATUS_MSG


def _json_render(result, status):
    data = {'status': status,
            'status_msg': STATUS_MSG[status],
            'result': result}
    return JsonResponse(data, json_dumps_params={'indent': 2})

try:
    from api.views import json_render
    json_render = json_render
except:
    json_render = _json_render
