from django.shortcuts import render
from django.conf import settings

from course.models import Course
from rating.models import Rating
from featured.models import Highlight

from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render, redirect

from course.models import Course
from program.models import Program
from access.models import Access
from analytic.models import Stat
from dependency.models import Dependency
from discussion.models import Topic
from term.models import Term
from rating.models import Rating
from progress.models import Progress
from content.models import Location as ContentLocation
from order.models import Item as OrderItem

from ondemand.views_decorator import check_project
from order.cached import cached_order_item, cached_order_item_is_purchased

from course.views_detail import _detail

def ais_loadtest_home_view(request):
    announcement = Highlight.pull_list('announcement').first()
    banner_list = Highlight.pull_list('banner')

    course_list = Course.pull_latest_list(request.APP, 8)
    content_type = settings.CONTENT_TYPE('course', 'course')
    for course in course_list:
        course.tutor_list = course.get_tutor_list()
        course.rating = Rating.pull_first(content_type, course.id)

    course_popular_list = Rating.popular_list(settings.CONTENT_TYPE('course', 'course'), 8)
    for course in course_popular_list:
        course.tutor_list = course.get_tutor_list()
        course.rating = Rating.pull_first(content_type, course.id)

    return render(request,
                  'home.html',
                  {'ROOT_PAGE': 'home',
                   'DISPLAY_SUBMENU': True,
                   'DISPLAY_CHAT': True,
                   'announcement': announcement,
                   'banner_list': banner_list,
                   'course_list': course_list,
                   'course_popular_list': course_popular_list})

def ais_loadtest_course_view(request, course_id):
    course = Course.pull(course_id)
    if course is None:
        raise Http404
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                  course.id)
    if request.user.is_authenticated():
        if order_item_id is not None:
            order_item = OrderItem.pull(order_item_id)
            if order_item is None or order_item.account_id != request.user.id:
                return redirect('course:detail', course.id)
        else:
            order_item = OrderItem.pull_purchased(content_location,
                                                  request.user,
                                                  settings.CONTENT_TYPE('course', 'course'),
                                                  course.id)
            if order_item is not None:
                return redirect('course:detail', course.id, order_item.id)
    return _detail(request, content_location, course, None)
