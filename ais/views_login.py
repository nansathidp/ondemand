import urllib

from api.views_api import json_render
from django.contrib.auth import authenticate, login
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST


@csrf_exempt
@require_POST
def login_view(request):
    result = {}
    code = 200
    try:
        email = request.POST.get('email', '').lower().strip()
        password = request.POST.get('password', '').strip()
        ref = request.POST.get('ref', None)

        email = urllib.parse.unquote(email)
        password = urllib.parse.unquote(password)

        if ref is None:
            result['link'] = request.META.get('HTTP_REFERER')
        else:
            result['link'] = ref

        if email and password is not None:
            # account = Login.send(email, password) #LDAP
            account = None
            if account is not None:
                account = authenticate(email=account.email, password=None)
            if account is None:
                account = authenticate(email=email, password=password)
            if account is not None:
                if account.is_active:
                    if account.first_active is None:
                        account.first_active = timezone.now()
                    account.last_active = timezone.now()
                    account.save(update_fields=['last_active', 'first_active'])
                    login(request, account)
                    request.session["is_skip"] = False

                    # Fix case /login/
                    if request.method == 'POST':
                        next = request.POST.get('next', None)
                        if next is not None:
                            result['link'] = next
                else:
                    code = 313
            else:
                code = 300
        else:
            code = 300
    except:
        skip = request.POST.get('skip', None)
        if skip == "1":
            request.session["is_skip"] = True
        else:
            code = 306

    return json_render(result, code)
