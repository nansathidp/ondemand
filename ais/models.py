from django.db import models
import json


class Login(models.Model):
    text = models.TextField()
    status_code = models.IntegerField()
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    URL = 'ldap://110.49.174.128'

    # 10.252.64.41
    # 1389

    class Meta:
        ordering = ['-timestamp']

    @staticmethod
    def send(user, pw):
        import ldap
        import traceback
        import uuid
        from account.models import Account

        # dn = 'uid=%s,ou=users,dc=example,dc=com'%(user)
        dn = 'ou=people,o=wireless.shinawatra.com,o=isp'
        l = ldap.initialize(Login.URL)
        l.set_option(ldap.OPT_NETWORK_TIMEOUT, 5.0)
        try:
            l.simple_bind_s(dn, pw)
            text = ''
            status_code = 200
        except:
            text = traceback.format_exc()
            status_code = 500
        Login.objects.create(text=text,
                             status_code=status_code)
        if status_code == 200:
            account = Account.objects.filter(email=user).first()
            if account is None:
                account = Account.objects.filter(username=user).first()
            if account is None:
                password = str(uuid.uuid4())
                if user.find('@') != -1:
                    account = Account.objects.create_user(user, password)
                else:
                    account = Account.objects.create_user('%s@gen'%user, password)
                account.save()
            return account
        else:
            return None


class AccountInfo(models.Model):
    """
    Keep Information for AIS Account
    """
    from django.conf import settings

    STATUS_CHOICES = (
        (-1, 'Inactive'),
        (1, 'Active'),
    )

    account = models.ForeignKey(settings.AUTH_USER_MODEL)

    status = models.IntegerField(choices=STATUS_CHOICES)
    employee = models.IntegerField(db_index=True)
    username = models.CharField(max_length=120)

    company = models.CharField(max_length=120)
    job_code = models.CharField(max_length=2)
    hire_date = models.DateField(null=True)
    primary_position = models.CharField(max_length=120)

    phone = models.CharField(max_length=120, blank=True)
    address1 = models.CharField(max_length=120, blank=True)
    address2 = models.CharField(max_length=120, blank=True)
    city = models.CharField(max_length=24, blank=True)
    province = models.CharField(max_length=24, blank=True)

    buh = models.CharField(max_length=255, blank=True)
    bu = models.CharField(max_length=255, blank=True)
    section = models.CharField(max_length=255, blank=True)
    
    login_method = models.CharField(max_length=8, blank=True)
    timeupdate = models.DateTimeField(auto_now=True, db_index=True)


class OM(models.Model):
    insert = models.IntegerField(default=0)
    update = models.IntegerField(default=0)
    row = models.IntegerField(default=0)

    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']


class OMDiff(models.Model):
    om = models.ForeignKey(OM)
    account_info = models.ForeignKey(AccountInfo)
    raw = models.CharField(max_length=512)
    diff = models.TextField()  # JSON
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)


def _push_header():
    from django.utils import timezone
    from random import randint
    now = timezone.now()
    timestamp = now.strftime('%Y%m%d%H%M%S')
    millisecond = int(int(now.strftime('%f')) / 1000)
    randnum = randint(1000, 9999)
    order_reference = '%s%s%s' % (timestamp, millisecond, randnum)
    return {
        'content-type': 'application/json',
        'accept': 'application/json',
        'x-orderRef': order_reference,
    }


class PNSSubscriber(models.Model):
    request = models.TextField()
    response = models.TextField(blank=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    @staticmethod
    def push(android=None, iphone=None):
        import traceback
        import requests
        from django.conf import settings

        # Check deploy other env AIS
        if settings.PNS_URL is None:
            return
        device_token = None

        if android is not None:
            device_token = android.token
            account = android.account
            # device = android.device
            # version = android.version
            platform = 'Android'
        elif iphone is not None:
            device_token = iphone.token
            account = iphone.account
            # device = iphone.device
            # version = iphone.version
            platform = 'iOS'

        device = 'Unknown'
        version = '0.0.0'

        request_url = '%s%s' % (settings.PNS_URL, 'subscribers')
        headers = _push_header()
        payload = {
            'userId': account.email,
            'deviceModel': device,
            'deviceToken': device_token,
            'appId': settings.PNS_APP_ID,
            'appName': settings.PNS_APP_NAME,
            'appVersion': settings.PNS_APP_VERSION,
            'platform': platform,
            'platformVersion': version,
            'eventName': settings.EVENT_NAME,
            'sender': settings.PNS_APP_SENDER
        }

        # For Production
        try:
            response = requests.post(request_url, data=json.dumps(payload), headers=headers, timeout=5)
            response = response.text
        except:
            response = '\n------\n%s' % traceback.format_exc()
        PNSSubscriber.objects.create(request=payload, response=response)


class PNSPush(models.Model):
    METHOD_CHOICE = (
        (-1, '-'),
        (0, 'BY DEVICES TOKEN'),
        (1, 'BY USERID'),
    )

    STATUS_CHOICE = (
        (0, 'DRAFT'),
        (1, 'MESSAGE SENT'),
        (2, 'SUCCESS')
    )

    request = models.TextField()
    method = models.IntegerField(choices=METHOD_CHOICE, default=-1)
    ref_id = models.CharField(max_length=64, default='-')
    response = models.TextField(blank=True)
    status = models.IntegerField(choices=STATUS_CHOICE, default=0)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    # TODO:POST
    # TODO:GET

    @staticmethod
    def push_by_token(message, token_list, platform):
        import requests
        from django.conf import settings
        request_url = '%s%s' % (settings.PNS_URL, 'notifications/deviceTokens')
        headers = _push_header()
        payload = {
            'deviceTokenList': token_list,
            'appId': settings.PNS_APP_ID,
            'appName': settings.PNS_APP_NAME,
            'appVersion': settings.PNS_APP_VERSION,
            'platform': platform,
            'platformVersion': '0.0.0',
            'message': message.message,
            'eventName': settings.EVENT_NAME,
            'deviceModel': 'Unknown',
            'sender': settings.PNS_APP_SENDER
        }

        response = requests.post(request_url, data=json.dumps(payload), headers=headers, timeout=5, method=0)
        PNSPush.objects.create(request=payload, response=response.text)

    @staticmethod
    def push_by_email(message, account_list, platform):
        import requests
        import traceback
        from django.conf import settings
        request_url = '%s%s' % (settings.PNS_URL, 'notifications/userIds')
        headers = _push_header()
        email_list = []
        for account in account_list:
            email_list.append(account.email)
        payload = {
            'dataList': email_list,
            'appId': settings.PNS_APP_ID,
            'appName': settings.PNS_APP_NAME,
            'appVersion': settings.PNS_APP_VERSION,
            'platform': platform,
            'platformVersion': '0.0.0',
            'message': message,
            'eventName': settings.EVENT_NAME,
            'deviceModel': 'Unknown',
            'sender': settings.PNS_APP_SENDER
        }
        try:
            ref_id = '-'
            response = requests.post(request_url, data=json.dumps(payload), headers=headers, timeout=5)
            json_response = json.loads(response.text)
            ref_id = json_response['pushRefId']
            response_str = response.text
        except:
            response_str = '\n------\n%s' % traceback.format_exc()
        PNSPush.objects.create(request=payload, response=response_str, ref_id=ref_id, method=1)

    def check_status(self):
        import requests
        import json
        from django.conf import settings
        request_url = '%s%s%s?sender=%s' % (settings.PNS_URL, 'notifications/pushRefIds/', self.ref_id, settings.PNS_APP_SENDER)
        headers = _push_header()
        response = requests.get(request_url, headers=headers, timeout=5)
        result = json.loads(response.text)
        result_list = []
        for _result in result.results:
            status = 2 if int(_result.statusCode) == 0 else 1
            PNSResult(push=self, response=_result, user_id=_result.userId, status=status)
        PNSResult.objects.bulk_create(result_list)


class PNSResult(models.Model):
    push = models.ForeignKey(PNSPush)
    response = models.TextField(blank=True)
    user_id = models.CharField(max_length=16, default=None, blank=True, null=True)
    status = models.IntegerField(choices=PNSPush.STATUS_CHOICE, default=0)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

# END PUSH NOTIFICATION #
