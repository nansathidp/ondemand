# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-31 03:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ais', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountinfo',
            name='company',
            field=models.CharField(max_length=120),
        ),
        migrations.AlterField(
            model_name='accountinfo',
            name='phone',
            field=models.CharField(blank=True, max_length=120),
        ),
    ]
