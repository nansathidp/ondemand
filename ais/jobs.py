from .models import PNSPush


def push_with_email(message, account_list, platform):
    PNSPush.push_by_email(message, account_list, platform)
