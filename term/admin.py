from django.contrib import admin
from .models import Term, Accept
from account.models import Account
from utils.cached.content_type import cached_content_type_admin_item_inline


@admin.register(Term)
class ContentAdmin(admin.ModelAdmin):
    list_display = ('content', 'content_type', 'body')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'content_type':
            kwargs["queryset"] = cached_content_type_admin_item_inline()
        return super(ContentAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Accept)
class AcceptAdmin(admin.ModelAdmin):
    list_display = ('term', 'account', 'timestamp')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "account":
            kwargs["queryset"] = Account.objects.filter(id=request.user.id)
        return super(AcceptAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
