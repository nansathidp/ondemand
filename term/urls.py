from django.conf.urls import url

from .views import view
from .views_accept import accept_view

app_name = 'term'
urlpatterns = [

    # USER
    url(r'^$', view, name='home'),  # TODO: testing

    # JS
    url(r'^(\d+)/accept/$', accept_view, name='accept'),  # TODO: testing
]
