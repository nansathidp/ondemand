from django.shortcuts import render, redirect
from django.conf import settings

from term.models import Term

from ondemand.views_decorator import check_project


@check_project
def view(request):
    term = Term.pull_content(settings.CONTENT_TYPE('app', 'app').id, request.APP.id)
    if term is None:
        return render(request,
                      'static/terms.html',
                      {
                          'ROOT_PAGE': 'term'
                      })

    if request.user.is_authenticated():
        is_require_accept = Term.is_require_term(settings.CONTENT_TYPE('app', 'app').id,
                                                 request.APP.id,
                                                 request.user.id)
    else:
        is_require_accept = False

    return render(request,
                #   'term/home.html',
                  'static/terms.html',
                  {
                      'ROOT_PAGE': 'term',
                      'term': term,
                      'is_require_accept': is_require_accept
                  })
