from django.db import models


class Term(models.Model):
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    content = models.BigIntegerField(db_index=True)

    body = models.TextField(blank=True)

    class Meta:
        default_permissions = ('view', 'add', 'change', 'delete')  # not view_own

    def __str__(self):
        return str(self.content)

    @staticmethod
    def pull(id):
        from .cached import cached_term
        return cached_term(id)

    @staticmethod
    def pull_content(content_type_id, content_id):
        # TODO cached
        return Term.objects.filter(content_type_id=content_type_id,
                                   content=content_id) \
            .first()

    @staticmethod
    def is_require_term(content_type_id, content_id, account_id):
        # TODO cached
        return not Accept.objects.filter(term__content_type_id=content_type_id,
                                         term__content=content_id,
                                         account_id=account_id) \
            .exists()

    def push_accept(self, account):
        accept = Accept.objects.filter(term=self, account=account).first()
        if accept:
            return accept
        return Accept.objects.create(term=self, account=account)

    def api_display(self):
        return {
            'id': self.id,
            'term': self.body
        }


class Accept(models.Model):
    from django.conf import settings

    term = models.ForeignKey(Term)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='+')
    timestamp = models.DateTimeField(auto_now_add=True)
