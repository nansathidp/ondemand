from django.conf import settings
from django.core.cache import cache
from .models import Term

from utils.cached.time_out import get_time_out_week


def cached_term(term_id, is_force=False):
    key = '%s_terms_%s' % (settings.CACHED_PREFIX, term_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Term.objects.get(id=term_id)
        except Term.DoesNotExist:
            result = -1
        cache.set(key, result, get_time_out_week())
    return None if result == -1 else result


def cached_term_content(content_id, content_type_id, is_force=False):
    key = '%s_terms_%s_%s' % (settings.CACHED_PREFIX, content_id, content_type_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Term.objects.get(content=content_id, content_type_id=content_type_id)
        except Term.DoesNotExist:
            result = -1
        cache.set(key, result, get_time_out_week())
    return None if result == -1 else result


def cached_term_content_delete(content_id, content_type_id):
    key = '%s_terms_%s_%s' % (settings.CACHED_PREFIX, content_id, content_type_id)
    cache.delete(key)

