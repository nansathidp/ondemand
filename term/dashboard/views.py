from django.shortcuts import render
from django.core.exceptions import PermissionDenied

from term.cached import cached_term
from ..models import Term
from .forms import TermContentForm

from django.conf import settings


def home_view(request):
    if not request.user.has_perm('term.view_content',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    content_type_app = settings.CONTENT_TYPE('app', 'app')
    term = Term.pull_content(content_type_app, request.APP.id)

    if request.method == 'POST':
        if not request.user.has_perm('term.change_content',
                                     group=request.DASHBOARD_GROUP):
            raise PermissionDenied

        term_content_form = TermContentForm(request.POST, instance=term)
        if term_content_form.is_valid():
            term = term_content_form.save()
            cached_term(term_id=term.id, is_force=True)

    else:
        term_content_form = TermContentForm(instance=term)

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Terms of Service Management'}
    ]
    return render(request,
                  'term/dashboard/home.html',
                  {'SIDEBAR': 'term',
                   'term': term,
                   'term_content_form': term_content_form,
                   'BREADCRUMB_LIST': BREADCRUMB_LIST})
