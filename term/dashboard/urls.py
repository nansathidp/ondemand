from django.conf.urls import url

from .views import home_view

app_name = 'term-dashboard'
urlpatterns = [
    url(r'^$', home_view, name='home'), #TODO: testing
]
