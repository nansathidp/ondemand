from django import forms

from ..models import Term


class TermContentForm(forms.ModelForm):

    class Meta:
        model = Term
        fields = ['body']

        labels = {
            'term': 'Terms & Conitions',
        }

    def __init__(self, *args, **kwargs):
        super(TermContentForm, self).__init__(*args, **kwargs)
