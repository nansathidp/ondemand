from django.conf.urls import url

from .views_accept import accept_view

urlpatterns = [
    url(r'^(\d+)/accept/$', accept_view), #TODO: testing
]
