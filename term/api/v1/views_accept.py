from account.cached import cached_api_account_auth
from term.cached import cached_term
from .views import check_key, check_require
from .views import json_render

from ...models import Accept


def accept_view(request, term_id):
    result = {}
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render(result, code)

    # Account
    account = cached_api_account_auth(token, uuid)
    if account is None:
        return json_render(result, 400)

    # FIXME @kidsdev
    term = cached_term(term_id)
    if term is None:
        return json_render(result, 4200)

    # Push
    Accept.push(term, account)
    return json_render(result, code)
