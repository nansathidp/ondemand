from .api.v1.views import json_render

from .models import Term


from ondemand.views_decorator import check_project


@check_project
def accept_view(request, term_id):
    result = {}
    code = 200

    if not request.user.is_authenticated():
        return json_render(result, 400)

    term = Term.pull(term_id)
    if term is None:
        return json_render(result, 4200)

    # Push
    term.push_accept(request.user)
    return json_render(result, code)
