from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

from .models import Course
from order.models import Order
from dependency.models import Dependency
from content.models import Location as ContentLocation

@login_required
def buy_view(request, course_id):
    """
    call by course detail Stand alone Only 
    * not call in course detail in Program!!
    """
    course = Course.pull(course_id)
    if course is None:
        return redirect('home')
    
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                  course.id)
    is_dependency, dependency_list = Dependency.check_content2(settings.CONTENT_TYPE('course', 'course'),
                                                               course.id,
                                                               request.user)
    if is_dependency:
        return redirect('course:detail', course.id)

    order_item = Order.buy(content_location,
                           request.APP,
                           1,
                           settings.CONTENT_TYPE('course', 'course'),
                           course,
                           request.user)
                           
    if order_item.is_free:
        order_item.order.buy_success(request.APP, request.get_host().split(':')[0])
        return redirect('course:detail', course_id, order_item.id)
    else:
        return redirect('order:cart', order_item.id)


@login_required
def checkout_view(request, course_id):
    course = Course.pull(course_id)
    if course is None:
        return redirect('home')
    
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                  course.id)
    is_dependency, dependency_list = Dependency.check_content2(settings.CONTENT_TYPE('course', 'course'),
                                                               course.id,
                                                               request.user)
    if is_dependency:
        return redirect('course:detail', course.id)

    order_item = Order.buy(content_location,
                           request.APP,
                           1,
                           settings.CONTENT_TYPE('course', 'course'),
                           course,
                           request.user,
                           is_checkout=True)
    
    if order_item is not None:
        order = order_item.order

    if order.is_free:
        return redirect('course:detail', course_id, order_item.id)
    else:
        return redirect('order:cart', order.id)
