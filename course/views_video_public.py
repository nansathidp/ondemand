from django.shortcuts import render, redirect, get_object_or_404
from api.views_ais_privilege import get_client_ip

from .models import Course, Material
from stream.models import Stream


def vider_public_view(request, course_id, material_id):
    video_previous = None
    video_play = None
    video_next = None
    stream = None
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    is_public = False

    if course is None or material is None:
        return redirect('home')

    # Check Public content
    if course.permission == 2 or material.is_public is True:
        is_public = True

    if not is_public:
        return redirect('course:detail', course.id)

    for outline in course.outline_set.all():
        if video_play is not None and video_next is not None:
            break
        for video_item in outline.get_public_video():
            if video_item.type in [0, 3]:
                if video_item.id == material.id:
                    video_play = material.id
                elif video_play is None:
                    video_previous = video_item.id
                elif video_play is not None:
                    video_next = video_item.id
                    break

    ip = get_client_ip(request)
    if material.type == 0:
        material_url = material.get_url(ip=ip)
        if material.video_type == 2:
            Stream.pull_material(material)
            stream = Stream.get_url_mobile_web_quality(material, ip)
    return render(request,
                  'course/playlist.html',
                  {
                      'course': course,
                      'video': material, #Migrate
                      'material': material,
                      'video_url': material_url,
                      'stream': stream,
                      'video_previous': video_previous,
                      'video_next': video_next,
                      'is_public': True,
                   })
