from django.conf import settings

from rating.models import Rating
from .models import Course
from price.models import Price

from app.views import update_checkout_type
from utils.api_image import api_image_url


def _api_display_title(course, store=1, install_version=0.0, store_version=0.0):
    is_cached = getattr(course, 'is_cached', False)
    content_type = settings.CONTENT_TYPE('course', 'course')
    if not is_cached:
        cached = Course.pull(course.id)
        if cached is None:
            return {}
        course = cached

    d = {
        'id': course.id,
        'name': course.name,
        'image': api_image_url(course.image),
        'overview': course.overview[:75],
        'desc': course.desc[:75],
        'status': course.status,
        'rating': Rating.pull_first(content_type, course.id).api_display(),
        'tutor_list': [],
        'status_display': course.get_status_display(),
        'category': {
            'id': course.category_id,
            'name': course.category.name
        }
    }

    # Mark remove
    # course.set_api_price(d, store)
    
    if int(store) == 1:
        d['price'] = getattr(course, 'price_web', None)
        d['price_set'] = getattr(course, 'price_web_set', {})
    else:
        price = getattr(course, 'price_%s' % store, None)
        discount = getattr(course, 'price_%s_discount' % store, None)
        if discount == -1.0 or discount is None:
            d['price'] = price
        else:
            d['price'] = discount
        d['price_set'] = getattr(course, 'price_%s_set' % store, None)

    # Price
    price = Price.pull(settings.CONTENT_TYPE('course', 'course'), course.id, store)
    price_set = price.api_display()
    d.update({
        'price_set': price_set
    })

    # Checkout Type
    update_checkout_type(d, install_version, store_version)

    # Tutor
    for tutor in course.tutor.all():
        d['tutor_list'].append({
            'id': tutor.id,
            'name': tutor.name,
            'image': api_image_url(tutor.image),
            'image_featured': api_image_url(tutor.image_featured),
            'institute_list': []
        })
    return d
