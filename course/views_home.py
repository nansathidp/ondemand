from django.conf import settings
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from ondemand.views_base import content_sort
from ondemand.views_decorator import check_project, param_filter
from .cached import pack_course

from .models import Course
from category.models import Category
from provider.models import Provider
from rating.models import Rating


@check_project
@param_filter
def new_home_view(request):
    course_id_list = Course.objects.values_list('id', flat=True) \
                                   .filter(is_display=True) \
                                   .order_by('sort', '-timestamp') # Fix 2017-06-02

    if request.category is not None:
        course_id_list = course_id_list.filter(category=request.category)

    if request.provider is not None:
        course_id_list = course_id_list.filter(provider=request.provider)

    content_type = settings.CONTENT_TYPE('course', 'course')
    course_id_list = content_sort(request, course_id_list, content_type)
    paginator = Paginator(course_id_list, 24)
    try:
        pager = paginator.page(request.page)
    except PageNotAnInteger:
        pager = paginator.page(1)
    except EmptyPage:
        pager = paginator.page(paginator.num_pages)

    course_list = pack_course(pager.object_list)

    for _ in course_list:
        _.tutor_list = _.get_tutor_list()
        _.rating = Rating.pull_first(content_type, _.id)
    return render(request,
                  'course/home.html',
                  {
                      'DISPLAY_SUBMENU': True,
                      'ROOT_PAGE': 'course',
                      'course_list': course_list,
                      'category': request.category,
                      'provider': request.provider,
                      'sort': request.sort,
                      'pager': pager,
                      'content_type_str': 'course',
                      'provider_list': Provider.pull_list(),
                      'category_list': Category.pull_list()
                  })
