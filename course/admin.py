from django.contrib import admin

from django.core.urlresolvers import reverse
from django.utils.html import format_html
from django.utils.functional import curry
from django.forms.models import BaseInlineFormSet
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
from django.shortcuts import render

from .models import Course, Outline, Material, Upload
from stream.models import Stream, Config

from .cached import cached_course_delete


class MaterialInlineFormset(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        self.initial = [{'label': 'first name', }]
        super(MaterialInlineFormset, self).__init__(*args, **kwargs)


class MaterialInline(admin.TabularInline):
    model = Material
    formset = MaterialInlineFormset
    extra = 3

    @staticmethod
    def edit_material(self, instance):
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label,
                                              instance._meta.model_name),
                      args=(instance.id,))
        if instance.id is None:
            return '-'
        else:
            return format_html(u'<a href="{}">Edit</a>', url)

    def get_extra(self, request, obj=None, **kwargs):
        return 0

    def get_formset(self, request, obj=None, **kwargs):
        initial = []
        if request.method == "GET":
            initial.append({
                'label': 'first name',
            })
        formset = super(MaterialInline, self).get_formset(request, obj, **kwargs)
        formset.__init__ = curry(formset.__init__, initial=initial)
        return formset

    # readonly_fields = ('edit_material',)
    # exclude = ('path', 'status')


class OutlineInline(admin.TabularInline):
    model = Outline

    def edit_outline(self, instance):
        url = reverse('admin:%s_%s_change' % (instance._meta.app_label,
                                              instance._meta.model_name),
                      args=(instance.id,))
        if instance.id is None:
            return '-'
        else:
            return format_html(u'<a href="{}">Edit</a>', url)

    readonly_fields = ('edit_outline',)


class MaterialPathFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('Material Path')
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'material_path'

    def lookups(self, request, model_admin):
        return (
            ('No Path', _('No Path')),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value() == 'No Path':
            return queryset.filter(path="")


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ('name',
                    'category',
                    'duration',
                    'duration', 'credit', 'expired', 'status', 'is_display',
                    'timestamp', 'sort')
    sortable = 'sort'
    list_per_page = 15
    search_fields = ['id', 'name']
    inlines = [
        OutlineInline,
    ]

    actions = ['clear_cache',
               'cal_total_time',
               'update_material_count',
               'select_application',
               'change_category',
               'add_stream',
               'remove_stream',
               'broadcast_image']

    #list_filter = ('category', 'tutor__institute', 'is_featured', 'group')
    list_filter = ('category',)

    #def get_levels(self, obj):
    #    return ', '.join([p.name for p in cached_course_level_by_course(obj)])
    #get_levels.short_description = "Levels"

    def change_category(self, request, queryset):
        from category.forms import CategoryForm

        if 'do_action' in request.POST:
            form = CategoryForm(request.POST)
            if form.is_valid():
                category = form.cleaned_data['category']
                updated = queryset.update(category=category)
                messages.success(request, '{0} item were updated'.format(updated))
        else:
            form = CategoryForm()
        return render(request, 'category/admin/change.html',
                      {'title': u'Choose Category',
                       'objects': queryset,
                       'form': form})

    @staticmethod
    def broadcast_image(self, request, queryset):
        course = queryset.first()
        Course.objects.all().update(image=course.image)

    def add_stream(self, request, queryset):
        config = Config.objects.filter(is_default=True).first()
        for course in queryset:
            for material in course.material_set.filter(type=0, video_type=2, video_status=2, is_display=True):
                if len(material.data) > 0:
                    if not Stream.objects.filter(material=material).exists():
                        stream = Stream(config=config, material=material, media=material.data)
                        stream.save()
                    else:
                        Stream.objects.filter(material=material).update(media=material.data)

    add_stream.short_description = "Add stream"

    def remove_stream(self, request, queryset):
        for course in queryset:
            for material in course.material_set.filter(type=0, video_status=1):
                Stream.objects.filter(material=material).delete()
    remove_stream.short_description = "Remove stream"

    def clear_cache(self, request, queryset):
        for course in queryset:
            cached_course_delete(course.id)
    clear_cache.short_description = "Update Cache"

    def cal_total_time(self, request, queryset):
        for course in queryset:
            course.update_duration()
    cal_total_time.short_description = "Update : Total time"

    def update_material_count(self, request, queryset):
        for course in queryset:
            course.count_material()
    update_material_count.short_description = "Update : Material Count"


"""
@admin.register(Feature)
class FeatureAdmin(admin.ModelAdmin):
    list_display = ('app', 'course', 'is_display', 'sort')
    sortable = 'sort'
    actions = ['make_published', 'make_unpublished', 'select_application']

    def make_published(self, request, queryset):
        queryset.update(is_display=True)

    make_published.short_description = "Mark selected content as published"

    def make_unpublished(self, request, queryset):
        queryset.update(is_display=False)

    make_unpublished.short_description = "Mark selected content as unpublished"

    def select_application(self, request, queryset):
        if 'do_action' in request.POST:
            form = AppForm(request.POST)
            if form.is_valid():
                app = form.cleaned_data['app']
                updated = queryset.update(app=app)
                messages.success(request, '{0} item were updated'.format(updated))
        else:
            form = AppForm()
        return render(request, 'admin/app/action_select_app_inline.html',
                      {'title': u'Choose App',
                       'objects': queryset,
                       'form': form})
"""

"""
@admin.register(CourseLevel)
class CourseLevelAdmin(admin.ModelAdmin):
    list_display = ('app', 'name', 'is_menu_display', 'sort')
    sortable = 'sort'
    actions = ['make_published', 'make_unpublished']

    def make_published(self, request, queryset):
        queryset.update(is_menu_display=True)

    make_published.short_description = "Mark selected content as published"

    def make_unpublished(self, request, queryset):
        queryset.update(is_menu_display=False)

    make_unpublished.short_description = "Mark selected content as unpublished"
"""

@admin.register(Outline)
class OutlineAdmin(admin.ModelAdmin):
    list_display = ('course', 'section')

    inlines = [
        MaterialInline
    ]


@admin.register(Material)
class MaterialAdmin(admin.ModelAdmin):
    list_display = (
        'course', 'outline', 'name', 'type', 'video_type', 'video_duration', 'video_status', 'video_is_verify', 'question', 'data',
        'sort', 'is_public', 'is_display')
    sortable = 'sort'
    search_fields = ['name', 'course__name',]

"""
@admin.register(Subscribe)
class SubscribeAdmin(admin.ModelAdmin):
    list_display = ('course', 'account', 'timestamp')
    list_filter = ('account__is_admin', )
"""


"""
@admin.register(Cut)
class CutAdmin(admin.ModelAdmin):
    list_display = ('course', 'start', 'end', 'status', 'timestamp')
"""

"""
@admin.register(Quality)
class QualityAdmin(admin.ModelAdmin):
    list_display = ('course', 'type', 'status', 'timestamp')
"""

@admin.register(Upload)
class UploadAdmin(admin.ModelAdmin):
    list_display = ('account', 'filename', 'uuid', 'material', 'progress', 'status', 'timestamp')
    readonly_fields = ('account',)
