from django.conf import settings

from dependency.models import Dependency
from content.models import Location as ContentLocation
from utils.duration import duration_display


def api_display(material, account=None, args={}, reverse=False, set_index=True, order_item=None):
    d = {
        'id': material.id,
        'name': material.name,
        'desc': material.desc,
        'type': material.type,
        'type_display': material.get_type_display(),
        'type_video_display': material.get_video_type_display(),
        'is_public': material.is_public,
        'duration': '00:00:00',
        'pdf': None,
        'link': None
    }

    if material.type == 0:
        if material.video_type == 1:
            d['path'] = material.data
        if material.video_duration.total_seconds() > 0:
            d['duration'] = duration_display(material.video_duration)
    elif material.type == 1:
        if "http://" in material.data or 'https://' in material.data:
            d['link'] = material.data
        else:
            d['link'] = '%s%s' % ('http://', material.data)
    elif material.type == 2:
        if material.document:
            d['pdf'] = material.document.url
    elif material.type == 3:
        if material.type == 3:
            d['content_id'] = material.question_id
        else:
            d['content_id'] = -1
    elif material.type == 4:
        if material.video_duration.total_seconds() > 0:
            d['duration'] = duration_display(material.video_duration)
    elif material.type == 5:
        d['scorm_url'] = material.get_scorm_url()
            
    if material.video_type == 1:
        d['type_video'] = 2
    else:
        d['type_video'] = 0

    if material.image:
        d['image'] = material.image.url
    else:
        d['image'] = args['default_video_thumb']

    args['index'] += 1
    d['index'] = int(args['index'])

    args['index_video'] += 1
    d['index_video'] = int(args['index_video'])

    if material.type in args['content_count']:
        args['content_count'][material.type] += 1
    else:
        args['content_count'][material.type] = 1

    if order_item:
        if order_item.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
            content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'material'),
                                                          material.id,
                                                          parent1_content_type=settings.CONTENT_TYPE('program', 'onboard'),
                                                          parent1_content=order_item.content,
                                                          parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                          parent2_content=material.course_id)
        elif order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
            content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'material'),
                                                          material.id,
                                                          parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                          parent1_content=order_item.content,
                                                          parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                          parent2_content=material.course_id)
        else:
            content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'material'),
                                                          material.id,
                                                          parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                          parent1_content=material.course_id)
        
        is_dependency, parent_list = Dependency.check_course_material(order_item,
                                                                      content_location,
                                                                      settings.CONTENT_TYPE('course', 'material'),
                                                                      material.id)
        
        d['is_dependency'] = is_dependency
    else:
        d['is_dependency'] = False
    return d
