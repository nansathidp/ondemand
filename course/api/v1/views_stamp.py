import datetime

from django.conf import settings
from django.utils import timezone

from api.v5.views import json_render
from api.views import check_auth
from content.models import Location as ContentLocation
from course.models import Course, Material
from order.models import Item as OrderItem
from program.models import Program
from progress.models import Progress


def _stamp(request, content_location_material, content_location_course, course, material, order_item):
    result = {}
    account, code = check_auth(request)
    if code != 200:
        return json_render({}, code)

    duration = request.GET.get('duration')
    if duration is None:
        return json_render(result, 740)
    else:
        try:
            # 02:22:22
            p = duration.split(':')
            duration = datetime.timedelta(hours=int(p[0]), minutes=int(p[1]), seconds=int(p[2]))
        except:
            return json_render(result, 741)

    action = request.GET.get('action', 0)
    try:
        action = int(action)
    except:
        action = 0
        
    progress_material = Progress.pull(
        order_item,
        content_location_material,
        settings.CONTENT_TYPE('course', 'material'),
        material.id
    )
    progress_course = Progress.pull(
        order_item,
        content_location_course,
        settings.CONTENT_TYPE('course', 'course'),
        course.id
    )
    progress_material.push_material(progress_course, order_item, account, material, action, duration)

    result['progress'] = progress_material.api_display()
    result['percent'] = progress_course.percent
    result['is_active'] = order_item.is_active()
    try:
        result['start'] = order_item.start.strftime(settings.TIME_FORMAT)
    except:
        result['start'] = '--'

    result['progress_percent'] = progress_course.percent
    if order_item.credit > datetime.timedelta(0):
        s = (order_item.credit - order_item.use_credit).total_seconds()
        h, r = divmod(s, 3600)
        m, s = divmod(r, 60)
        result['remaining'] = '%02d:%02d' % (h, m)
        result['is_remaining'] = True
    else:
        result['remaining'] = '--'
        result['is_remaining'] = False
    if result['is_active'] is False:
        result['is_remaining'] = False

    if order_item.expired > datetime.timedelta(0):
        if order_item.start is None:
            result['expired'] = '%s days.' % (int(order_item.expired.total_seconds() / (60 * 60 * 24)))
        else:
            expired = (order_item.start + order_item.expired) - timezone.now()
            result['expired'] = '%s days.' % (int(expired.total_seconds() / (60 * 60 * 24)))
    else:
        result['expired'] = '--'

    return json_render(result, code)
    

def stamp_view(request, course_id, material_id, order_item_id):
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    order_item = OrderItem.pull(order_item_id)
    if course is None or material is None or order_item is None:
        return json_render({}, 700)

    content_location_material = ContentLocation.pull_first(
        settings.CONTENT_TYPE('course', 'material'),
        material.id,
        parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
        parent1_content=course.id
    )
    content_location_course = ContentLocation.pull_first(
        settings.CONTENT_TYPE('course', 'course'),
        course.id
    )
    return _stamp(
        request,
        content_location_material,
        content_location_course,
        course, material, order_item
    )


def stamp_program_view(request, course_id, program_id, material_id, order_item_id):
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    program = Program.pull(program_id)
    order_item = OrderItem.pull(order_item_id)
    if course is None or material is None or program is None or order_item is None:
        return json_render({}, 700)

    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    content_location_material = ContentLocation.pull_first(
        settings.CONTENT_TYPE('course', 'material'),
        material.id,
        parent1_content_type=content_type,
        parent1_content=program.id,
        parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
        parent2_content=course.id
    )
    content_location_course = ContentLocation.pull_first(
        settings.CONTENT_TYPE('course', 'course'),
        course.id,
        parent1_content_type=content_type,
        parent1_content=program.id,
    )
    return _stamp(
        request,
        content_location_material,
        content_location_course,
        course, material, order_item
    )
