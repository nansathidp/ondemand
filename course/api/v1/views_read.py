import datetime

from django.conf import settings
from django.utils import timezone

from api.v5.views import json_render
from api.views import check_auth
from content.models import Location as ContentLocation
from course.models import Course, Material
from order.models import Item as OrderItem
from program.models import Program
from progress.models import Progress


def _read(request, course, material, order_item, program=None):
    result = {}
    account, code = check_auth(request)
    if code != 200:
        return json_render({}, code)

    progress = material.get_progress(order_item, program)
    progress.check_complete_material(order_item, material)
    result['progress'] = progress.api_display()
    return json_render(result, code)
    

def read_view(request, course_id, material_id, order_item_id):
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    order_item = OrderItem.pull(order_item_id)
    if course is None or material is None or order_item is None:
        return json_render({}, 700)
    elif not material.type in [1, 2]:
        return json_render({}, 700)

    return _read(request, course, material, order_item)


def read_program_view(request, course_id, program_id, material_id, order_item_id):
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    program = Program.pull(program_id)
    order_item = OrderItem.pull(order_item_id)
    if course is None or material is None or program is None or order_item is None:
        return json_render({}, 700)
    elif not material.type in [1, 2]:
        return json_render({}, 700)
    return _read(request, course, material, order_item, program)
