from .views import json_render, check_key, check_auth
from django.conf import settings

from ...models import Course, Material
from program.models import Program
from order.models import Item as OrderItem
from progress.models import Progress
from content.models import Location as ContentLocation

from api.views_ip import get_client_ip

def _playlist(request, content_location, course, order_item, account, program=None):
    result = {}
    
    response = check_key(request)
    if response is not None:
        return response

    if account is None and course.permission != 2:
        return json_render({}, 204)

    if course.permission != 2:
        if order_item is None:
            return json_render({}, 725)

    result['outline_list'] = []
    args = {'index': 0,
            'index_video': 0,
            'default_video_thumb': course.image.url if bool(course.image) else None,
            'content_count': {}}
    for outline in course.outline_list():
        d = outline.api_display(account=account, args=args, is_video_list=True, order_item=order_item)
        if len(d['video_list']) > 0:
            result['outline_list'].append(d)
            
    # Fix Audio Url
    ip = get_client_ip(request)
    for outline in result['outline_list']:
        for material in outline['video_list']:
            if material['type'] == 4:
                _material = Material.pull(material['id'])
                material['link'] = _material.get_url(ip=ip, is_api=True)
    # End Fix


    # Fix is_video_progress = True
    if account is not None and order_item is not None:
        for outline in result['outline_list']:
            for material in outline['video_list']:
                material['progress'] = {}
                material_object = Material.pull(material['id'])
                if material_object:
                    progress = material_object.get_progress(order_item, program)
                    if progress is not None:
                        material['progress'] = progress.api_display()
                        
    return json_render(result, 200)


def playlist_view(request, course_id, order_item_id=None):
    course = Course.pull(course_id)
    if course is None:
        return json_render({}, 700)
    
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                  course.id)

    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)
    
    order_item, code = OrderItem.api_pull(content_location, order_item_id,
                                          settings.CONTENT_TYPE('course', 'course'),
                                          course.id,
                                          account)
    if code != 200:
        return json_render({}, code)

    return _playlist(request, content_location, course, order_item, account)


def playlist_program_view(request, course_id, program_id, order_item_id=None):
    course = Course.pull(course_id)
    program = Program.pull(program_id)
    if course is None or program is None:
        return json_render({}, 700)

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                  course.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                  parent1_content=program.id)
    
    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)

    _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('program', 'program'),
                                                   program.id)
    order_item, code = OrderItem.api_pull(_content_location,
                                          order_item_id,
                                          settings.CONTENT_TYPE('program', 'program'),
                                          program.id,
                                          account)
    if code != 200:
        return json_render({}, code)

    return _playlist(request, content_location, course, order_item, account,
                     program=program)
