from api.v5.views import json_render, check_key
from course.models import Course


def filter_view(request):
    result = {}
    code = 200
    response = check_key(request)
    if response is not None:
        return response
    category = request.GET.get('category', None)
    store = request.GET.get('store', -1)
    if store == -1:
        return json_render(result, 209)
    try:
        if category is not None:
            course_list = Course.objects.filter(category_id=category, is_display=True)
            result['course_list'] = [course.api_display_title(store=store) for course in course_list]
        else:
            code = 702
    except:
        code = 702
    return json_render(result, code)
