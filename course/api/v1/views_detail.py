from .views import json_render, check_key, check_auth, check_require
from django.conf import settings
from django.core.urlresolvers import reverse
from django.conf import settings
from ...models import Course, Material
from program.models import Program
from purchase.models import Apple
from order.models import Item as OrderItem
from analytic.models import Stat
from term.models import Term
from access.models import Access as AccessContent
from progress.models import Progress
from content.models import Location as ContentLocation
from dependency.models import Dependency
from rating.models import Rating

from api.views_ip import get_client_ip

if settings.IS_IPTABLE:
    from iptable.models import Iptable
    


def _detail(request, content_location, course, account, order_item, program=None):
    result = {}
    code, token, uuid, store, install_version = check_require(request)
    if code != 200:
        return json_render({}, code)

    if order_item is not None:
        is_video_progress = True
        result['order_item_id'] = order_item.id

        result['item'] = order_item.api_progress_display()
        progress = Progress.pull(order_item,
                                 content_location,
                                 settings.CONTENT_TYPE('course', 'course'),
                                 course.id)
        result['accessibility'] = OrderItem.accessibility(account, course, order_item)
        result['is_rated'] = Rating.is_rated(
            settings.CONTENT_TYPE('course', 'course').id,
            course.id,
            account
        )
        if progress is not None:
            result['progress'] = progress.api_display()
        else:
            result['progress'] = None
    else:
        is_video_progress = False
        result['order_item_id'] = None
        result['item'] = None
        result['progress'] = None
        result['is_rated'] = True # Fix For Android, IOS check other object
        # result['is_rated'] = False

    result['dependency_list'] = []
    is_dependency, dependency_list = Dependency.check_content2(settings.CONTENT_TYPE('course', 'course'),
                                                               course.id,
                                                               account)
    for parent in dependency_list:
        result['dependency_list'].append(parent.api_display())
    result['is_dependency'] = is_dependency

    is_purchase_apple = request.GET.get('is_purchase_apple', None)
    if is_purchase_apple is not None and is_purchase_apple.lower() == 't':
        result['purchase'] = {}
        content_type = settings.CONTENT_TYPE('course', 'course')
        try:
            apple = Apple.objects.get(content_type=content_type, content=course.id)
            result['purchase']['apple'] = apple.api_display()
        except:
            result['purchase']['apple'] = 404

    result.update(course.api_display(install_version=install_version,
                                     store_version=request.APP.app_version,
                                     account=account,
                                     is_video_progress=is_video_progress,
                                     store=store,
                                     order_item=order_item))
    # Fix Audio Url
    ip = get_client_ip(request)
    for outline in result['outline_list']:
        for material in outline['video_list']:
            if material['type'] == 4:
                _material = Material.pull(material['id'])
                material['link'] = _material.get_url(ip=ip, is_api=True)

    # End Fix
    # term = Term.pull_content(settings.CONTENT_TYPE('course', 'course').id, course.id)
    # if term is not None:
    #     result.update({
    #         'term': term.api_display(),
    #         'is_require_terms': Term.is_require_term(settings.CONTENT_TYPE('course', 'course').id,
    #                                                  course.id,
    #                                                  account.id)
    #     })

    content_permission, is_require_permission = AccessContent.get_permission(course.id,
                                                                             settings.CONTENT_TYPE('course', 'course').id,
                                                                             account)

    if settings.IS_IPTABLE:
        is_iptable = Iptable.check_ip(settings.CONTENT_TYPE('course', 'course'),
                                      course.id,
                                      get_client_ip(request))
    else:
        is_iptable = True

    result.update({
        'content_permission': content_permission.api_display() if content_permission is not None else None,
        'is_require_permission': is_require_permission,
        'url': '%s%s' % (settings.BASE_URL(request), course.get_absolute_url()),
        'is_iptable': is_iptable
    })
    result['link_share'] = settings.SITE_URL+reverse('course:detail', args=[course.id])+'?id='+str(course.id)
    Stat.push('course_%s' % course.id, 'API_V5', request.META.get('HTTP_USER_AGENT', ''))
    return json_render(result, code)


def detail_view(request, course_id, order_item_id=None):
    course = Course.pull(course_id)

    if course is None:
        return json_render({}, 700)
    
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                  course.id)

    response = check_key(request)

    if response is not None:
        return response

    account, code = check_auth(request, is_allow_anonymous=True)

    if code != 200:
        return json_render({}, code)

    if order_item_id is not None:
        order_item = OrderItem.pull(order_item_id)
        if order_item is None:
            return json_render({}, 700)
        if not order_item.is_active():
            return json_render({}, 700)
        elif account is None:
            return json_render({}, 700)
        elif account.id != order_item.account_id:
            return json_render({}, 700)
    elif account is not None:
        order_item = OrderItem.pull_purchased(content_location,
                                              account,
                                              settings.CONTENT_TYPE('course', 'course'),
                                              course.id)
    else:
        order_item = None
    return _detail(request, content_location, course, account, order_item)


def detail_program_view(request, course_id, program_id, order_item_id=None):
    course = Course.pull(course_id)
    program = Program.pull(program_id)
    if course is None or program is None:
        return json_render({}, 700)

    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                  course.id,
                                                  parent1_content_type=content_type,
                                                  parent1_content=program.id)

    response = check_key(request)
    if response is not None:
        return response

    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)

    if order_item_id is not None:
        order_item = OrderItem.pull(order_item_id)
        if order_item is None:
            return json_render({}, 700)
        if not order_item.is_active():
            return json_render({}, 700)
        elif account is None:
            return json_render({}, 700)
        elif account.id != order_item.account_id:
            return json_render({}, 700)
    elif account is not None:
        order_item = OrderItem.pull_purchased(content_location,
                                              account,
                                              settings.CONTENT_TYPE('course', 'course'),
                                              course.id)
    else:
        order_item = None
    return _detail(request, content_location, course, account, order_item, program=program)
