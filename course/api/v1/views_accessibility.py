import datetime
from django.conf import settings
from django.utils import timezone

from ...models import Course, Material
from progress.models import Progress
from order.models import Item as OrderItem
from content.models import Location as ContentLocation

from .views import json_render, check_auth


def accessibility_view(request, material_id, order_item_id):
    result = {}
    code = 200

    account, code = check_auth(request)
    if code != 200:
        return json_render({}, code)

    material = Material.pull(material_id)
    if material is None:
        return json_render({}, 700)
    
    order_item = OrderItem.pull(order_item_id)
    
    if order_item.status == 1:
        return json_render({}, 721)
    elif order_item.status == 3:
        return json_render({}, 723)

    result['progress'] = order_item.percent
    result['device_list'] = []
    for device in account.device_set.select_related('uuid').all():
        result['device_list'].append(device.api_display())
    return json_render(result, code)


def accessibility_stamp_view(request, material_id, order_item_id):
    result = {}
    account, code = check_auth(request)
    if code != 200:
        return json_render({}, code)

    duration = request.GET.get('duration')
    if duration is None:
        return json_render(result, 740)
    else:
        try:
            p = duration.split(':')
            duration = datetime.timedelta(hours=int(p[0]), minutes=int(p[1]), seconds=int(p[2]))
        except:
            return json_render(result, 741)

    action = request.GET.get('action', 0)
    try:
        action = int(action)
    except:
        action = 0
    # fix Case seek Duration = 1s.
    if action == 4 and duration == datetime.timedelta(seconds=1):
        result['is_active'] = True
        result['is_remaining'] = True
        result['progress_percent'] = 0
        result['percent'] = 0
        return json_render(result, 200)

    material = Material.pull(material_id)
    if material is None:
        return json_render({}, 700)

    order_item = OrderItem.pull(order_item_id)
    if order_item is None:
        return json_render({}, 700)

    course = Course.pull(material.course_id)
    # FIXME : content_location fail split url to defind location
    if order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
        content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'material'),
                                                      material.id,
                                                      parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                      parent1_content=order_item.content,
                                                      parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                      parent2_content=material.course_id)
    else:
        content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'material'),
                                                      material.id,
                                                      parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                      parent1_content=material.course_id)
        
    progress_material = Progress.pull(order_item,
                                      content_location,
                                      settings.CONTENT_TYPE('course', 'material'),
                                      material.id)
    progress_course = Progress.pull(order_item,
                                    content_location,
                                    settings.CONTENT_TYPE('course', 'course'),
                                    course.id)
    progress_material.push_material(progress_course, order_item, account, material, action, duration)
    
    result['percent'] = progress_course.percent
    result['is_active'] = order_item.is_active()
    try:
        result['start'] = order_item.start.strftime(settings.TIME_FORMAT)
    except:
        result['start'] = '--'

    result['progress_percent'] = progress_course.percent
    if order_item.credit > datetime.timedelta(0):
        s = (order_item.credit - order_item.use_credit).total_seconds()
        h, r = divmod(s, 3600)
        m, s = divmod(r, 60)
        result['remaining'] = '%02d:%02d' % (h, m)
        result['is_remaining'] = True
    else:
        result['remaining'] = '--'
        result['is_remaining'] = False
    if result['is_active'] is False:
        result['is_remaining'] = False

    if order_item.expired > datetime.timedelta(0):
        if order_item.start is None:
            result['expired'] = '%s days.' % (int(order_item.expired.total_seconds() / (60 * 60 * 24)))
        else:
            expired = (order_item.start + order_item.expired) - timezone.now()
            result['expired'] = '%s days.' % (int(expired.total_seconds() / (60 * 60 * 24)))
    else:
        result['expired'] = '--'

    return json_render(result, code)
