from django.conf.urls import url

from .views import home_view
from .views_buy import buy_view
from .views_review import course_review_view

from .views_detail import detail_view, detail_program_view
from .views_playlist import playlist_view, playlist_program_view
from .views_material import material_view, material_program_view
from .views_stamp import stamp_view, stamp_program_view
from .views_read import read_view, read_program_view

from .views_relate import relate_view
from .views_material_read import material_read_view
from .views_filter import filter_view

from .views_accessibility import accessibility_view, accessibility_stamp_view

urlpatterns = [
    url(r'^$', home_view),
    url(r'^(\d+)/buy/$', buy_view),
    url(r'^(\d+)/$', detail_view),
    url(r'^(\d+)/(\d+)/$', detail_view),
    url(r'^(\d+)/playlist/$', playlist_view),
    url(r'^(\d+)/playlist/(\d+)/$', playlist_view),

    url(r'^(\d+)/review/$', course_review_view),

    url(r'^(\d+)/material/(\d+)/$', material_view),
    url(r'^(\d+)/material/(\d+)/(\d+)/$', material_view),

    url(r'^(\d+)/material/(\d+)/(\d+)/stamp/$', stamp_view),
    url(r'^(\d+)/material/(\d+)/(\d+)/read/$', read_view),
    
    # In Program
    url(r'^(\d+)/program/(\d+)/$', detail_program_view),
    url(r'^(\d+)/program/(\d+)/(\d+)/$', detail_program_view),
    url(r'^(\d+)/program/(\d+)/playlist/$', playlist_program_view),
    url(r'^(\d+)/program/(\d+)/playlist/(\d+)/$', playlist_program_view),

    url(r'^(\d+)/program/(\d+)/material/(\d+)/$', material_program_view),
    url(r'^(\d+)/program/(\d+)/material/(\d+)/(\d+)/$', material_program_view),
    url(r'^(\d+)/program/(\d+)/material/(\d+)/(\d+)/stamp/$', stamp_program_view),
    url(r'^(\d+)/program/(\d+)/material/(\d+)/(\d+)/read/$', read_program_view),
    
    url(r'^(\d+)/relate/$', relate_view),
    url(r'^(\d+)/material/(\d+)/read/(\d+)/$', material_read_view),
    url(r'^filter/$', filter_view),

    url(r'^accessibility/(\d+)/(\d+)/$', accessibility_view), # TODO : Remove
    url(r'^accessibility/(\d+)/stamp/(\d+)/$', accessibility_stamp_view), # TODO : Remove
]
