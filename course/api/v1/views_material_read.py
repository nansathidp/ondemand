from .views import json_render, check_key, check_auth
from django.conf import settings

from progress.models import Progress
from content.models import Location as ContentLocation
from course.models import Course, Material
from order.models import Item as OrderItem


def material_read_view(request, course_id, material_id, order_item_id):
    result = {}
    response = check_key(request)
    if response is not None:
        return response
    
    course = Course.pull(course_id)
    if course is None:
        return json_render({}, 1300)

    account, code = check_auth(request)
    if code != 200:
        return json_render({}, code)
    
    order_item = OrderItem.pull(order_item_id)
    if order_item is None:
        return json_render({}, 700)

    if account.id != order_item.account_id:
        return json_render({}, 700)
    
    material = Material.pull(material_id)
    if material is None:
        return json_render({}, 700)

    # FIXME : content_location fail
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'material'),
                                                  material.id)
    progress = Progress.pull(order_item,
                             content_location,
                             settings.CONTENT_TYPE('course', 'material'),
                             material.id)
    progress.read_material()
    return json_render(result, code)
