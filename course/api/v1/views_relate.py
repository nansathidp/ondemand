from api.v4.views import json_render, check_key

from course.cached import cached_api_course_relate


def relate_view(request, course_id):
    result = {}
    code = 200
    response = check_key(request)
    if response is not None:
        return response
    store = int(request.GET.get('store', -1))
    if store == -1:
        return json_render(result, 209)
    if not store in [-1, 0, 1, 2, 3, 4]:
        return json_render(result, 209)

    result['relate_course'] = cached_api_course_relate(course_id, request.APP, store)
    return json_render(result, code)
