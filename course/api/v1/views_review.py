from .views import json_render, check_key, check_auth, check_require
from django.db.models import Count, Avg, Max
from django.conf import settings
from ...models import Course
from program.models import Program
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from content.models import Location as ContentLocation
from rating.models import Rating, Log


def _review(request, content_location, course, account, page_number=1, program=None):
    code, token, uuid, store, install_version = check_require(request)
    if code != 200:
        return json_render({}, code)
    if program is None:

        rating = Rating.objects.filter(content_type=settings.CONTENT_TYPE('course', 'course').id,
                                       content=course.id).first()
    else:
        rating = Rating.objects.filter(content_type=settings.CONTENT_TYPE('program', 'program').id,
                                       content=program.id).first()

    weight_list = Log.objects.filter(rating=rating).order_by().values('weight').annotate(
        count=Count('weight')).distinct()

    result = rating.api_display()
    review_temp_list = []
    wieght_temp = []
    total_content = result['count']
    review_list = rating.get_log_list()
    paginator = Paginator(review_list, 20)
    result['total'] = paginator.num_pages
    try:
        review = paginator.page(page_number)
        result['next'] = review.next_page_number()
        result['previous'] = review.previous_page_number()
        result['page_item'] = 20


    except PageNotAnInteger:
        review = paginator.page(1)
        result['next'] = 1
        result['previous'] = 1


    except EmptyPage:
        result['next'] = 1
        result['previous'] = 1
        review = paginator.page(paginator.num_pages)

    for review_item in review.object_list:
        review_temp_list.append(review_item.api_display())
    wieght = [1, 2, 3, 4, 5]

    for weight_item in weight_list:
        weight_item['percen'] = (weight_item['count'] / total_content) * 100
        wieght.pop(wieght.index(weight_item['weight']))

    result['weight_list'] = list(weight_list)
    for wieght_item in wieght:
        result['weight_list'].append({'weight': wieght_item, 'count': 0, 'percen': 0})

    result['review_list'] = review_temp_list

    return json_render(result, code)


def course_review_view(request, course_id, order_item_id=None):
    page_number = request.GET.get('page')
    course = Course.pull(course_id)

    if course is None:
        return json_render({}, 700)

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                  course.id)

    response = check_key(request)

    if response is not None:
        return response

    account, code = check_auth(request, is_allow_anonymous=True)

    if code != 200:
        return json_render({}, code)

    return _review(request, content_location, course, account, page_number)


def prgram_review_view(request, course_id, program_id, order_item_id=None):
    course = Course.pull(course_id)
    program = Program.pull(program_id)
    page_number = request.GET.get('page')

    if course is None or program is None:
        return json_render({}, 700)

    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                  course.id,
                                                  parent1_content_type=content_type,
                                                  parent1_content=program.id)

    response = check_key(request)
    if response is not None:
        return response

    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)

    return _review(request, content_location, course, account, page_number, program=program)
