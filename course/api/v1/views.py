from django.conf import settings
from django.http import JsonResponse

from api.views_api import STATUS_MSG

_STATUS_MSG = {
}


def json_render(result, status):
    if status in _STATUS_MSG:
        status_msg = _STATUS_MSG[status]
    else:
        status_msg = STATUS_MSG[status]
    data = {'status': status,
            'status_msg': status_msg,
            'result': result}
    return JsonResponse(data, json_dumps_params={'indent': 2})

from api.views import check_auth
from api.v5.views import check_key
from api.v5.views import check_require

from ...models import Course
from ...cached import cached_course_category #TODO cleanup
from api.v5.views import get_filter_list
from api.v5.views_decorator import param_filter
from ondemand.views_base import content_sort
from category.models import Category


@param_filter
def home_view(request):
    result = {}
    response = check_key(request)
    if response is not None:
        return response
    
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render(result, code)

    get_filter_list(result, Category.pull_list(),
                    is_display_provider=request.is_display_provider == 't',
                    is_display_category=request.is_display_category == 't',
                    is_display_popular=True)

    course_id_list = Course.objects.values_list('id', flat=True).filter(is_display=True)
    try:
        # Filter Category
        if request.category_id != 'all':
            course_id_list = course_id_list.filter(category_id=request.category_id)

        # Filter Provider
        if request.provider_id != 'all':
            course_id_list = course_id_list.filter(provider_id=request.provider_id)
    except:
        course_id_list = course_id_list.all()

    # Sort
    content_type = settings.CONTENT_TYPE('course', 'course')
    course_id_list = content_sort(request, course_id_list, content_type)

    result['course_list'] = [Course.pull(course_id).api_display_title() for course_id in course_id_list]
    return json_render(result, code)
