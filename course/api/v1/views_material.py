from order.cached import cached_order_item, cached_order_item_is_purchased
from .views import json_render, check_key, check_auth
from django.conf import settings

from ...models import Course, Material
from program.models import Program
from order.models import Item as OrderItem
from content.models import Location as ContentLocation

from api.views_ip import get_client_ip

if settings.IS_IPTABLE:
    from iptable.models import Iptable


def _material(request, content_location, course, material, order_item, account, program=None):
    result = {}
    response = check_key(request)
    if response is not None:
        return response
    
    if account is None and course.permission != 2:
        return json_render({}, 204)

    # Check Public course
    if course.permission != 2 and order_item is None:
        return json_render({}, 725)

    if account is not None:
        if order_item is not None:
            result['order_item_id'] = order_item.id
            accessibility = OrderItem.accessibility(account, course, order_item)
            if accessibility['code'] == 200:
                get_material_url(request, result, material, account)
            else:
                code = accessibility['code']
        else:
            get_material_url(request, result, material, account)
    else:
        get_material_url(request, result, material, account)

    if settings.IS_IPTABLE:
        is_iptable = Iptable.check_ip(settings.CONTENT_TYPE('course', 'course'),
                                      course.id,
                                      get_client_ip(request))
    else:
        is_iptable = True
    result['is_iptable'] = is_iptable
    return json_render(result, 200)


def get_material_url(request, result, material, account):
    result['type'] = material.type
    ip = get_client_ip(request)
    if material.type == 0:
        result['m_t'] = material.type
        result['m_vt'] = material.video_type
        if material.video_type == 1:
            # Client play youtube
            result['type_video'] = 2
        else:
            # Client play with path
            result['type_video'] = 0
    result['url'] = material.get_url(ip=ip, is_api=True)


def material_view(request, course_id, material_id, order_item_id=None):
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    if course is None or material is None:
        return json_render({}, 1300)

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'material'),
                                                  material.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent1_content=course.id)

    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)

    order_item, code = OrderItem.api_pull(content_location, order_item_id,
                                          settings.CONTENT_TYPE('course', 'course'),
                                          course.id,
                                          account)
    if code != 200:
        return json_render({}, code)

    return _material(request, content_location, course, material, order_item, account)


def material_program_view(request, course_id, program_id, material_id, order_item_id=None):
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    program = Program.pull(program_id)
    if course is None or material is None or program is None:
        return json_render({}, 700)

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'material'),
                                                  material.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                  parent1_content=program.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent2_content=course.id)

    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)

    _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('program', 'program'),
                                                   program.id)
    order_item, code = OrderItem.api_pull(_content_location, order_item_id,
                                          settings.CONTENT_TYPE('program', 'program'),
                                          program.id,
                                          account)
    if code != 200:
        return json_render({}, code)

    return _material(request, content_location, course, material, order_item, account,
                     program=program)
