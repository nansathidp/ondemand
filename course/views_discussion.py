from django.shortcuts import render, get_object_or_404

from ondemand.views_decorator import check_project


@check_project
def discussion_view(request, course_id):
    return render(request, 'course/discussion.html',
                  {'DISPLAY_SUBMENU': True,
                   'ROOT_PAGE': 'course'})
