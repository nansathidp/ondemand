from django.test import TestCase
from django.urls import reverse
from django.conf import settings


from course.models import Course
from category.models import Category
from price.models import Price
from account.models import Account
from django.contrib.auth.models import Group, Permission

from .cached import cached_course_delete
from category.tests import create_category

from ondemand.tests import PROJECT_LIST

import random
import string


def _random():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(8))


def create_course():
    return Course.objects.create(name=_random(),
                                 category=create_category(),
                                 is_display=True)


def create_paid_course():
    course = create_course()
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    Price.objects.create(content_type=content_type_course,
                         content=course.id,
                         price=100,
                         store=1)
    cached_course_delete(course.id)
    return course


def dashboard_user(permission_codename):        
    user = Account.objects.create_user('%s@test.com'%_random(),
                                       '123456')
    group = Group.objects.create(name=_random())
    if permission_codename is not None:
        permission = Permission.objects.get(content_type=settings.CONTENT_TYPE('course', 'course'),
                                            codename=permission_codename)
        group.permissions.add(permission)
    user.groups.add(group)
    return user


def dashboard_course(account):
    return Course.objects.create(name=_random(), category=Category.objects.create(name=_random()))


class InitTests(TestCase):

    def test_create_course(self):
        course = create_course()
        self.assertIsNotNone(course)

    def test_create_paid_course(self):
        course = create_paid_course()
        content_type_course = settings.CONTENT_TYPE('course', 'course')
        price = Price.pull(content_type_course, course.id, 1)
        self.assertIsNotNone(course)
        self.assertIsNotNone(price)
        self.assertEqual(price.price, 100)


class AnonymousTests(TestCase):
    def test_home(self):
        response = self.client.get(reverse('course:home'))
        if settings.PROJECT in PROJECT_LIST:
            self.assertEqual(response.status_code, 302)
        else:
            self.assertEqual(response.status_code, 200)

    def test_detail(self):
        course = create_course()
        response = self.client.get(reverse('course:detail', args=[course.id]))
        if settings.PROJECT in PROJECT_LIST:
            self.assertEqual(response.status_code, 302)
        else:
            self.assertEqual(response.status_code, 200)

    """
    def test_detail_slug(self):
        course = create_course()
        response = self.client.get(reverse('course:detail', args=[course.id, course.slug]))
        if settings.PROJECT in PROJECT_LIST:
            self.assertEqual(response.status_code, 302)
        else:
            self.assertEqual(response.status_code, 200)
    """
    
    def test_buy(self):
        course = create_course()
        response = self.client.get(reverse('course:buy', args=[course.id]))
        self.assertEqual(response.status_code, 302)

    def test_checkout(self):
        course = create_course()
        response = self.client.get(reverse('course:checkout', args=[course.id]))
        self.assertEqual(response.status_code, 302)
