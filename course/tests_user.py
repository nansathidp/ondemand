from django.test import TestCase
from django.urls import reverse
from django.core.cache import cache

from account.models import Account
from order.models import Order, Item as OrderItem

from .tests import _random, create_course, create_paid_course


class UserTests(TestCase):

    def setUp(self):
        cache._cache.flush_all()
        self.user = Account.objects.create_user('%s@test.com' % _random(), '123456')
        self.client.force_login(self.user)

    def test_user_login(self):
        self.assertEqual(self.user.is_authenticated(), True)
    
    def test_home(self):
        response = self.client.get(reverse('course:home'))
        self.assertEqual(response.status_code, 200)

    def test_detail(self):
        course = create_course()
        response = self.client.get(reverse('course:detail', args=[course.id]))
        self.assertEqual(response.status_code, 200)

    #def test_detail_slug(self):
    #    course = create_course()
    #    response = self.client.get(reverse('course:detail', args=[course.id, course.slug]))
    #    self.assertEqual(response.status_code, 200)

    def test_buy_free(self):
        course = create_course()
        response = self.client.get(reverse('course:buy', args=[course.id]))
        self.assertEquals(Order.objects.filter(account=self.user, status=3).count(), 1)
        self.assertEquals(response.status_code, 302)

    def test_buy_paid(self):
        course = create_paid_course()
        response = self.client.get(reverse('course:buy', args=[course.id]))
        order = Order.objects.filter(account=self.user, status=1).first()
        self.assertEquals(Order.objects.filter(account=self.user, status=1).count(), 1)
        self.assertRedirects(response, reverse('order:cart', args=[order.id]))

    def test_checkout_free(self):
        course = create_course()
        response = self.client.get(reverse('course:checkout', args=[course.id]))
        self.assertEquals(Order.objects.filter(account=self.user, status=3).count(), 1)
        order_item = OrderItem.objects.filter(account=self.user,
                                              order__status=3).first()
        self.assertRedirects(response, reverse('course:detail', args=[course.id, order_item.id]))

    def test_checkout_paid(self):
        course = create_paid_course()
        response = self.client.get(reverse('course:checkout', args=[course.id]))
        order = Order.objects.filter(account=self.user, status=1).first()
        self.assertEquals(Order.objects.filter(account=self.user, status=1).count(), 1)
        self.assertRedirects(response, reverse('order:cart', args=[order.id]))
