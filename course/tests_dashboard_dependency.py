from django.test import TestCase
from django.urls import reverse

from .tests import dashboard_course

class AnonymousTests(TestCase):
    def setUp(self):
        self.course = dashboard_course(None)

    def test_dependency(self):
        response = self.client.get(reverse('dashboard:course:dependency', args=[self.course.id]))
        self.assertRedirects(response, reverse('dashboard:login'))

    def test_dependency_search(self):
        response = self.client.get(reverse('dashboard:course:dependency_search', args=[self.course.id]))
        self.assertRedirects(response, reverse('dashboard:login'))

    def test_dependency_search_add(self):
        response = self.client.get(reverse('dashboard:course:dependency_search_add', args=[self.course.id]))
        self.assertRedirects(response, reverse('dashboard:login'))

    def test_dependency_search_delete(self):
        response = self.client.get(reverse('dashboard:course:dependency_search_delete', args=[self.course.id]))
        self.assertRedirects(response, reverse('dashboard:login'))

    def test_dependency_delete(self):
        response = self.client.get(reverse('dashboard:course:dependency_delete', args=[self.course.id]))
        self.assertRedirects(response, reverse('dashboard:login'))
