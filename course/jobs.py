import codecs
import os
import shutil
import zipfile

from django.apps import apps
from django.conf import settings

from course.cached import cached_course_delete, cached_course_material_update
from course.models import Course, Upload, Material
from order.models import Item as OrderItem
from progress.models import Progress, Scorm, ScormLog
from xml.etree import ElementTree as ET

def _delete_course(course_id):
    content_type = settings.CONTENT_TYPE('course', 'course')
    model_list = ['price.Price',
                  'term.Term',
                  'iptable.iptable',
                  'dependency.Dependency',
                  'log.Log',
                  'featured.Featured',
                  'featured.Item',
                  'discussion.Topic',
                  'order.Item',
                  'order.Summary',
                  'assignment.Item',
                  'rating.Rating',
                  # 'slug.Slug',
                  'owner.Owner',
                  'pool.Pool',
                  'content.Location',
                  'access.Access',
                  'program.Item',
                  'inbox.Inbox',
                  # Progress      
    ]
    
    for model in model_list:
        Model = apps.get_model(model)
        for _ in Model.objects.filter(content_type=content_type,
                                      content=course_id):
            _.delete()


def upload_scorm(upload_id):
    try:
        upload = Upload.objects.get(id=upload_id)
    except:
        return None
    path = os.path.join(settings.BASE_DIR, 'media', 'scorm')
    if not os.path.isdir(path):
        os.makedirs(path)
    src = os.path.join(settings.BASE_DIR, 'upload', '%s.zip'%upload.uuid)
    dst = os.path.join(settings.BASE_DIR, 'media', 'scorm', '%s.zip'%upload.uuid)
    shutil.move(src, dst)
    
    z = zipfile.ZipFile(dst, 'r')
    _ = os.path.join(settings.BASE_DIR, 'media', 'scorm', '%s'%upload.uuid)
    z.extractall(_)
    z.close()

    _ = os.path.join(settings.BASE_DIR, 'media', 'scorm', '%s'%upload.uuid, 'imsmanifest.xml')
    link = None
    try:
        tree = ET.parse(_)
        namespace = tree.getroot().tag[1:].split("}")[0]
        link = tree.find(".//{%s}resource" % namespace).get('href')
    except:
        pass
    # with codecs.open(_, "r", encoding='utf-8', errors='ignore') as f:
    #     for line in f:
    #         # print('>', line, '<')
    #         if line.find('<resource') != -1 and line.find('type="webcontent"') != -1:
    #             try:
    #                 link = line.split('href')[1].split('"')[1]
    #             except:
    #                 link = None
    #             break
    if link is not None:
        material = upload.material
        material.data = 'scorm/%s/%s'%(upload.uuid, link)
        material.save()
        cached_course_material_update(material)
        Upload.objects.filter(material=material).update(status=4)
    cached_course_delete(upload.material.course_id)


def progress_scorm(order_item_id, scorm_id):
    try:
        order_item = OrderItem.objects.get(id=order_item_id)
        scorm = Scorm.objects.get(id=scorm_id)
    except:
        return

    for progress in Progress.objects.filter(item=order_item,
                                            content_type=settings.CONTENT_TYPE('course', 'material'),
                                            content=scorm.material_id):
        log = ScormLog.objects.filter(scorm=scorm,
                                      status=3) \
                              .first()
        if log is not None:
            _ = log.data.replace('NaN', '0').strip()
            count = len(_)
            n = _.count('1')
            if n >= count:
                percent = 100.0
            else:
                try:
                    percent = float(n)/float(count)*100.0
                except:
                    percent = 0.0
            # print(_, n, count, percent)
            progress.check_complete_scorm(order_item, scorm.material, percent)
        elif ScormLog.objects.filter(scorm=scorm, status=2).count() > 0:
            progress.check_complete_scorm(order_item, scorm.material, 100.0)
        
    # for progress in Progress.objects.filter(item=order_item,
    #                                         content_type=settings.CONTENT_TYPE('course', 'material'),
    #                                         content=scorm.material_id):
    #     count = ScormLesson.objects.filter(scorm=scorm,
    #                                        scormlog__status=2) \
    #                                .distinct() \
    #                                .count()
    #     if count >= scorm.total_lesson:
    #         percent = 100.0
    #     else:
    #         try:
    #             percent = float(count)/float(scorm.total_lesson)*100.0
    #         except:
    #             percent = 0.0
    #     progress.check_complete_scorm(order_item, scorm.material, percent)

    
def _upload_video(upload_id):
    upload = Upload.objects.get(id=upload_id)
    upload.status = 3
    upload.save(update_fields=['status'])

    material = upload.material
    path = getattr(settings, 'NIMBLE_SERVER_PATH', None)
    if path is None:
        path = '/home/conicle/video'
    _path = '_'
    if settings.DEBUG:
        _path += 'debug_'
    if settings.PROJECT is not None:
        _path += settings.PROJECT
    folder = '%s/%s/%s' % (path, _path, material.course_id)
    server_type = getattr(settings, 'NIMBLE_SERVER_TYPE', None)
    if server_type is None:
        server_type = 'scp'
    if server_type == 'scp':
        server_ssh = getattr(settings, 'NIMBLE_SSH', 'conicle@103.246.16.183')
        cmd = 'ssh %s "mkdir -p %s"' % (server_ssh, folder)
        os.system(cmd)
        cmd = 'scp %s/upload/%s %s:%s/%s/%s' % (settings.BASE_DIR,
                                                upload.get_filename(),
                                                server_ssh,
                                                path,
                                                _path,
                                                upload.get_media())
        os.system(cmd)
    else:
        cmd = 'mkdir -p %s' % folder
        os.system(cmd)
        cmd = 'cp %s/upload/%s %s/%s/%s'%(settings.BASE_DIR,
                                          upload.get_filename(),
                                          path,
                                          _path,
                                          upload.get_media())
        os.system(cmd)
        
    upload.status = 4
    upload.save(update_fields=['status'])
    material.video_status = 2
    material.data = '%s/%s'%(_path, upload.get_media())
    material.save(update_fields=['video_status', 'data'])
    cached_course_material_update(material)
    upload.push_stream()
    upload.get_duration_ffmpeg()
    os.remove('%s/upload/%s'%(settings.BASE_DIR, upload.get_filename()))

    
def _material_delete(course_id, type, data):
    if type in [0, 4]:
        is_rm = True
    else:
        is_rm = False
    if is_rm and settings.DEBUG:
        if data.find('_debug') != 0:
            is_rm = False
    if is_rm:
        if Material.objects.filter(data=data).exists():
            is_rm = False

    if is_rm and settings.IS_ALLOW_DELETE_CONTENT:
        path = '/home/conicle/video'
        cmd = 'ssh conicle@103.246.16.183 "rm %s/%s"' % (path, data)
        os.system(cmd)

    course = Course.objects.get(id=course_id)
    content_type = settings.CONTENT_TYPE('course', 'course')
    for progress in Progress.objects.filter(content_type=content_type,
                                            is_root=True,
                                            content=course.id):
        progress.content_update()
