from django.shortcuts import render, redirect

from .models import Course
from category.models import Category

from ondemand.views_decorator import check_project
from provider.cached import cached_provider_list


@check_project
def home_view(request):
    category = Category.pull(request.GET.get('category'))
    type = request.GET.get('type', None)
    menu = None
    title = None
    is_see_all = False
    content_list = []

    if type is None and category is None:
        type = 'featured'

    if type is not None:
        if "featured" in type:
            d = {
                'title': 'Featured Courses',
                'course_list': []
            }
        elif "all" in type:
            d = {
                'title': 'All Courses',
                'course_list': []
            }
            for course_id in Course.objects.values_list('id', flat=True).filter(is_display=True).order_by('sort')[:32]:
                course = Course.pull(course_id)
                if course is not None:
                    d['course_list'].append(course)
            content_list.append(d)
        elif "new" in type:
            d = {
                'title': 'New Courses',
                'course_list': []
            }
            for course_id in Course.objects.values_list('id', flat=True).filter(is_display=True).order_by('-timestamp')[:32]:
                course = Course.pull(course_id)
                if course is not None:
                    d['course_list'].append(course)
            content_list.append(d)
        elif "free" in type:
            root_page = 'course_free'
            d = {
                'title': 'คอร์สฟรี',
                'course_list': []
            }
            for course_id in Course.objects.values_list('id', flat=True).filter(is_display=True, group=1).order_by('-timestamp')[:32]:
                course = Course.pull(course_id)
                if course is not None:
                    d['course_list'].append(course)
            content_list.append(d)
        elif "tricks" in type:
            d = {
                'title': 'คณิตคิดเลขเร็ว',
                'course_list': []
            }
            for course_id in Course.objects.values_list('id', flat=True).filter(is_display=True, group=4).order_by('sort'):
                course = Course.pull(course_id)
                if course is not None:
                    d['course_list'].append(course)
            content_list.append(d)
        else:
            return redirect('course:home')
        title = d['title']
    return render(request,
                  'course/home.html',
                  {
                      'DISPLAY_SUBMENU': True,
                      'DISPLAY_CHAT': True,
                      'ROOT_PAGE': 'course',
                      'menu': menu,
                      'type': type,
                      'content_list': content_list,
                      'is_see_all': is_see_all,
                      'title': title,
                      'category': category,
                      'provider_list': cached_provider_list()
                  })
