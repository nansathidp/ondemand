from django import template

register = template.Library()

@register.inclusion_tag('course/dashboard/material_video_tag.html')
def video_upload_tag(material, upload_list):
    upload = None
    for item in upload_list:
        if item.material_id == material.id:
            upload = item
            break
    return {'upload': upload}