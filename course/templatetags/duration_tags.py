from django import template
from utils.duration import duration_display
import datetime

register = template.Library()


@register.filter
def duration_tag(duration):
    return duration_display(duration)

    # Fix 2017-10-27
    result = '-'
    if isinstance(duration, datetime.timedelta):
        s = duration.total_seconds()
        if s == 0:
            return '0 min'
        d, r = divmod(s, 60*60*24)
        h, r = divmod(r, 3600)
        m, s = divmod(r, 60)
        if d > 0:
            return '%d Day %d:%02d:%02d' % (d, h, m, s)
        if h > 0:
            return '%d:%02d:%02d' % (h, m, s)
        else:
            return '%d:%02d' % (m, s)
        # Fix CIMB 2017-10-12
        # if h > 0:
        #     return '%d:%02d h' % (h, m)
        # elif m > 0:
        #     return '%d min' % m
        # elif s > 0:
        #     return '%d sec' % s
    return result


@register.filter(name='duration')
def duration(duration):
    try:
        s = duration.total_seconds()
    except:
        s = 0
    if s == 0:
        return '00:00:00'
    h, r = divmod(s, 3600)
    m, s = divmod(r, 60)
    return '%02d:%02d:%02d' % (h, m, s)


@register.filter()
def secondtomin(second):
    return int(second/60)

@register.filter()
def check_unlimited(time):
    return time if time>0 else 'Unlimited'
