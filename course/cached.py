from django.conf import settings
from django.core.cache import cache
from django.db.models import Sum

from course.models import Course, Material  # Feature as CourseFeature, CourseLevel, App as CourseApp # FIXME @kidsdev
from rating.cached import cached_rating_popular_list_delete
from utils.cached.time_out import get_time_out, get_time_out_day, get_time_out_week, get_time_out_hour

from rating.models import Rating

import datetime
import random


def pack_course(id_list):
    result = []
    for _ in id_list:
        course = cached_course(_)
        if course is not None and course.is_display:
            result.append(course)
    return result


def cached_course(course_id, is_force=False):
    key = '%s_course_%s' % (settings.CACHED_PREFIX, course_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Course.objects.select_related('category').prefetch_related('tutor', 'outline_set').get(
                id=course_id)
            cache.set(key, result, get_time_out_day())
        except:
            result = -1
    return None if result == -1 else result


def cached_course_delete(course_id):
    key = '%s_course_%s' % (settings.CACHED_PREFIX, course_id)
    cache.delete(key)


def cached_course_material(material_id, is_force=False):
    key = '%s_course_material_%s' % (settings.CACHED_PREFIX, material_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Material.objects.get(id=material_id)
        except:
            return None
        cache.set(key, result, get_time_out_day())
    return result


def cached_course_material_update(material):
    key = '%s_course_material_%s' % (settings.CACHED_PREFIX, material.id)
    cache.set(key, material, get_time_out_day())


def cached_course_video_delete(video):
    key = '%s_course_video_%s' % (settings.CACHED_PREFIX, video.id)
    cache.delete(key)


def cached_home_course_feature(app, is_force=False):
    key = '%s_home_course_feature_v2_app_%s' % (settings.CACHED_PREFIX, app.id)
    result = None if is_force else cache.get(key)
    result = []
    # if result is None:
    #     result = CourseFeature.objects.values_list('course_id', flat=True).filter(app=app,
    #                                                                               is_display=True).order_by('sort')[:15]
    #     cache.set(key, result, get_time_out())
    return pack_course(result)


def cached_home_course_feature_delete(app):
    key = '%s_home_course_feature_v2_app_%s' % (settings.CACHED_PREFIX, app.id)
    cache.delete(key)


def cached_course_latest(app, limit, is_force=False):
    key = '%s_course_latest_%s' % (settings.CACHED_PREFIX, app.id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Course.objects.values_list('id', flat=True) \
                     .filter(is_display=True) \
                     .order_by('-timestamp')[:10]  # Fix 2017-06-02
        cache.set(key, result, get_time_out())
    return pack_course(result[:limit])


def cached_course_latest_delete(app):
    key = '%s_course_latest_%s' % (settings.CACHED_PREFIX, app.id)
    cache.delete(key)


def cached_course_popular(app, limit, is_force=False):
    key = '%s_course_popular_app_%s_%s' % (settings.CACHED_PREFIX, app.id, limit)
    result = None if is_force else cache.get(key)
    if result is None:
        content_type_course = settings.CONTENT_TYPE('course', 'course')
        result = Rating.objects.values_list('content', flat=True).filter(content_type=content_type_course, ).order_by(
            '-weight')[:limit]
        cache.set(key, result, get_time_out())
    return pack_course(result)


# TODO: @Din
def cached_api_course_feature(app, store, install_version, store_version, is_force=False):
    key = '%s_api_course_feature_%s_%s_%s_%s' % (settings.CACHED_PREFIX, app.id, store, install_version, store_version)
    result = None if is_force else cache.get(key)
    if result is None:
        result = []
    return result


def cached_api_course_feature_delete(app):
    """ Fix Migration """
    if app is None:
        return
    for store in [3, 4]:
        key = '%s_api_course_feature_%s_%s' % (settings.CACHED_PREFIX, app.id, store)
        cache.delete(key)


def cached_course_level_delete(level_id):
    key = '%s_course_level_%s' % (settings.CACHED_PREFIX, level_id)
    cache.delete(key)


def cached_course_level_list_delete(app):
    key = '%s_course_level_list_%s' % (settings.CACHED_PREFIX, app.id)
    cache.delete(key)


def cached_api_v3_course_level_delete():
    key = '%s_api_v3_course_level' % settings.CACHED_PREFIX
    cache.delete(key)


def cached_api_course_by_level(app, store, level, install_version, store_version, is_force=False):
    key = '%s_api_course_by_level_%s_%s_%s_%s_%s' % (
    settings.CACHED_PREFIX, app.id, store, level.id, install_version, store_version)
    result = None if is_force else cache.get(key)
    if result is None:
        result = []
        for course in Course.objects.filter(app__app=app, is_display=True, level=level).order_by('-timestamp')[:5]:
            result.append(course.api_display_title(store, install_version, store_version))
        cache.set(key, result, get_time_out())
    return result


def cached_api_course_by_level_delete_by_level(level, is_force=False):
    for store in [3, 4]:
        key = '%s_api_course_by_level_%s_%s_%s' % (settings.CACHED_PREFIX, level.app.id, store, level.id)
        cache.delete(key)


def cached_api_course_by_subject(app, store, subject, install_version, store_version, is_force=False):
    key = '%s_api_course_by_subject_%s_%s_%s_%s_%s' % (
    settings.CACHED_PREFIX, app.id, store, subject.id, install_version, store_version)
    result = None if is_force else cache.get(key)
    if result is None:
        result = []
        for course in Course.objects.filter(app__app=app, subject=subject).order_by('-timestamp')[:5]:
            result.append(course.api_display_title(store, install_version, store_version))
        cache.set(key, result, get_time_out())
    return result


def cached_api_course_relate(course_id, app, store, is_force=False):
    key = '%s_api_course_relate_%s_%s_%s' % (settings.CACHED_PREFIX, course_id, app.id, store)
    result = None if is_force else cache.get(key)
    if result is None:
        result = []
        try:
            course = Course.objects.get(id=course_id)
            for course_item in Course.objects.filter(is_display=True, subject=course.subject, app__app=app).exclude(
                    id=course.id)[:6]:
                result.append(course_item.api_display_title(store=store))
            cache.set(key, result, get_time_out())
        except:
            pass
    return result


def cached_course_level_by_course(course, is_force=False):
    key = '%s_course_level_by_course_%s' % (settings.CACHED_PREFIX, course.id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = course.level.all()
        cache.set(key, result, get_time_out())
    return result


def cached_course_video_sum_duration(course, is_force=False):
    key = '%s_course_video_sum_duration_%s' % (settings.CACHED_PREFIX, course.id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Material.objects.filter(course=course).aggregate(Sum('video_duration'))['video_duration__sum']
            if result is None:
                result = datetime.timedelta(0)
        except:
            result = datetime.timedelta(0)
        cache.set(key, result, get_time_out())
    return result


def cached_course_id_list(is_force=False):
    key = '%s_course_id_list' % settings.CACHED_PREFIX
    result = None if is_force else cache.get(key)
    if result is None:
        result = [course['id'] for course in Course.objects.filter(is_display=True).values('id')]
        cache.set(key, result, get_time_out())
    return result


def cached_course_tutor_id_list(tutor, is_force=False):
    key = '%s_course_tutor_id_list' % settings.CACHED_PREFIX
    result = None if is_force else cache.get(key)
    if result is None:
        result = [course['id'] for course in Course.objects.filter(is_display=True, tutor=tutor).values('id')]
        cache.set(key, result, get_time_out())
    return result


def cached_course_app_id_list(app, is_force=False):
    key = '%s_course_id_list_app_%s' % (settings.CACHED_PREFIX, app.id)
    result = None if is_force else cache.get(key)
    if result is None:
        if app.is_master:
            result = [course['id'] for course in Course.objects.all().values('id')]
        else:
            result = [course['id'] for course in Course.objects.filter(app__app=app).values('id')]
        cache.set(key, result, get_time_out())
    return result


def cached_course_relate_same_institute(course, is_force=False):
    key = '%s_course_relate_same_institute_%s' % (settings.CACHED_PREFIX, course.id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Course.objects.values_list('id', flat=True).filter(tutor__institute__tutor__course__id=course.id,
                                                                    is_display=True)
        cache.set(key, result, get_time_out())
    return result


def cached_get_course_relate(course, n, is_force=False):
    result = []
    course_id_list = cached_course_relate_same_institute(course)
    for course_id in random.sample(list(course_id_list), n) if len(course_id_list) >= n else course_id_list:
        course = cached_course(course_id)
        if course is not None:
            result.append(course)
    return result


def cached_get_course_relate_rand(n, tutor=None, is_force=False):
    course_relate_list = []
    if tutor is not None:
        course_id_list = cached_course_tutor_id_list(tutor, True)
    else:
        course_id_list = cached_course_id_list()

    for course_id in random.sample(course_id_list, n) if len(course_id_list) >= n else course_id_list:
        course = cached_course(course_id)
        if course is not None:
            course_relate_list.append(course)
    return course_relate_list


def cached_course_review(course, is_force=False):
    key = '%s_course_review_%s' % (settings.CACHED_PREFIX, course.id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = course.review_set.select_related('account') \
                     .order_by('-timestamp')[:3]
        cache.set(key, result, get_time_out_hour())
    return result


# TODO fix query
def cached_course_category(is_force=False):
    from category.cached import cached_category
    key = '%s_course_category' % settings.CACHED_PREFIX
    result = None if is_force else cache.get(key)
    category_list = []
    if result is None:
        result = Course.objects.values_list('category_id', flat=True) \
            .filter(is_display=True) \
            .order_by('category_id') \
            .distinct()
        for category_id in result:
            category_list.append(cached_category(category_id))
        cache.set(key, category_list, get_time_out_day())
    return category_list


def cached_course_delete_all(request, course):
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    # Course
    cached_course_delete(course.id)
    cached_course_latest_delete(request.APP)
    # Rating
    cached_rating_popular_list_delete(content_type_course)
    # Provider
    from provider.cached import cached_course_provider_delete_all
    if course.provider:
        cached_course_provider_delete_all(course.provider)

