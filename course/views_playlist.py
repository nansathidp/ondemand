from django.shortcuts import render, redirect
from django.utils import timezone
from django.http import Http404
from django.conf import settings

from .models import Course, Material
from program.models import Program
from stream.models import Stream
from order.models import Item as OrderItem
from question.models import Score, ScoreSection
from content.models import Location as ContentLocation
from dependency.models import Dependency

from api.views_ais_privilege import get_client_ip
from order.cached import cached_order_item_update
from progress.models import Progress
from ondemand.views_decorator import check_project

import itertools

if settings.IS_IPTABLE:
    from iptable.models import Iptable
    from api.views_ip import get_client_ip

class TemplateIterator(itertools.count):
    def next(self):
        return next(self)
    
def _playlist(request, content_location, content_location_course, course, material, order_item,
              progress, program=None):
    if not request.user.is_authenticated():
        return redirect('course:detail', course.id, course.slug)

    if material.type in [1, 2]:
        if 'http' not in material.data and material.type == 1:
            material.data = 'http://'+material.data
        progress.check_complete_material(order_item, material)
        
    progress_course = Progress.pull(order_item,
                                    content_location_course,
                                    settings.CONTENT_TYPE('course', 'course'),
                                    course.id)
    is_dependency, dependency_list = Dependency.check_course_material(order_item,
                                                                      content_location,
                                                                      settings.CONTENT_TYPE('course', 'material'),
                                                                      material.id)
    video_previous = None
    video_play = None
    video_next = None
    video_url = None
    stream = None
    score_item = None
    score_list = []
    score_section_list = None
    question = None
    is_submit = False
    action = request.GET.get('action', None)
    for outline in course.outline_set.all():
        if video_play is not None and video_next is not None:
            break
        for video_item in outline.get_material_set():
            if video_item.type in [0, 1, 2, 3, 4, 5]:
                if video_item.id == material.id:
                    video_play = material.id
                elif video_play is None:
                    video_previous = video_item.id
                elif video_play is not None:
                    video_next = video_item.id
                    break
    ip = get_client_ip(request)
    if material.type in [0, 4]:
        if material.video_type in [2, 3]:
            video_url = material.stream_src(ip)
        elif material.video_type == 2:
            stream = Stream.pull_material(material)
            video_url = stream.get_url(get_client_ip(request))
            stream = Stream.get_url_mobile_web_quality(material, ip)
        else:
            video_url = material.get_url(ip=ip)
    elif material.type == 3:
        question = material.question
        is_submit = question.is_submit(order_item, content_location)
        score_item = Score.pull_latest(content_location, request.user, order_item)
        score_list = Score.pull_list(content_location, request.user, order_item)
        score_section_list = ScoreSection.objects.filter(score_item=score_item)
    elif material.type in [1, 2, 5]:
        pass
    else:
        raise Http404()
    if settings.IS_IPTABLE:
        is_iptable = Iptable.check_ip(settings.CONTENT_TYPE('course', 'course'),
                                      course.id,
                                      get_client_ip(request))
    else:
        is_iptable = True
    return render(request,
                  'course/playlist.html',
                  {
                      'course': course,
                      'program': program,
                      'material': material,
                      'activity': question,
                      'question': question,
                      'order_item': order_item,
                      'content_location': content_location,
                      'stream': stream,
                      'action': action,
                      'score_item': score_item,
                      'score_list': score_list,
                      'score_section_list': score_section_list,
                      'ip': ip,
                      'is_public': False,
                      'is_submit': is_submit,
                      'progress': progress,
                      'progress_course': progress_course,
                      'video_url': video_url,
                      'video_previous': video_previous,
                      'video_next': video_next,
                      'is_iptable': is_iptable,
                      'counter': TemplateIterator(1),
                      'is_dependency': is_dependency,
                      'dependency_list': dependency_list,
                  })


@check_project
def playlist_view(request, course_id, material_id, order_item_id):
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    order_item = OrderItem.pull(order_item_id)

    if course is None or material is None:
        raise Http404
    if order_item is None:
        return redirect('profile')
    elif order_item.account_id != request.user.id:
        return redirect('profile')
    elif order_item.start is None:
        order_item.start = timezone.now()
        order_item.save(update_fields=['start'])
        cached_order_item_update(order_item)

    if material.type == 3:
        content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                      material.question_id,
                                                      parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                      parent1_content=course.id,
                                                      parent2_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                      parent2_content=material.id)
        progress = Progress.pull(order_item,
                                 content_location,
                                 settings.CONTENT_TYPE('question', 'activity'),
                                 material.question_id)
    else:
        content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'material'),
                                                      material.id,
                                                      parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                      parent1_content=course.id)
        progress = Progress.pull(order_item,
                                 content_location,
                                 settings.CONTENT_TYPE('course', 'material'),
                                 material.id)
        
    content_location_course = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                         course.id)
    return _playlist(request, content_location, content_location_course, course, material, order_item,
                     progress)


@check_project
def playlist_program_view(request, program_id, course_id, material_id, order_item_id):
    program = Program.pull(program_id)
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    order_item = OrderItem.pull(order_item_id)
    
    if program is None or course is None or material is None:
        raise Http404
    if order_item is None:
        return redirect('profile')
    elif order_item.account_id != request.user.id:
        return redirect('profile')
    elif order_item.start is None:
        order_item.start = timezone.now()
        order_item.save(update_fields=['start'])
        cached_order_item_update(order_item)

    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    if material.type == 3:
        content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                      material.question_id,
                                                      parent1_content_type=content_type,
                                                      parent1_content=program.id,
                                                      parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                      parent2_content=course.id,
                                                      parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                      parent3_content=material.id)
        progress = Progress.pull(order_item,
                                 content_location,
                                 settings.CONTENT_TYPE('question', 'activity'),
                                 material.question_id)

    else:
        content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'material'),
                                                      material.id,
                                                      parent1_content_type=content_type,
                                                      parent1_content=program.id,
                                                      parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                      parent2_content=course.id)
        progress = Progress.pull(order_item,
                                 content_location,
                                 settings.CONTENT_TYPE('course', 'material'),
                                 material.id)
        
    content_location_course = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                         course.id,
                                                         parent1_content_type=content_type,
                                                         parent1_content=program.id)
    return _playlist(request, content_location, content_location_course, course, material, order_item,
                     progress,
                     program=program)
