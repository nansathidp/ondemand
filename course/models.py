from django.db import models


class Course(models.Model):
    import datetime

    PERMISSION_CHOICES = (
        (0, 'General'),
        (1, 'AIS Privilege'),
        (2, 'Public'),
    )

    STATUS_CHOICES = (
        (0, 'Normal'),
        (1, 'Feature'),
        (2, 'New'),
        (3, 'Coming Soon'),
        (4, 'Sale'),
        (5, 'Pre-Sale'),
        (6, 'Privilege'),
        (7, 'Starter'),
    )

    PREVIEW_VIDEO_TYPE_CHOICES = (
        (-1, 'None'),
        (1, 'Full Path'),
        (2, 'Youtube'),
    )

    CHECKOUT_TYPE_CHOICES = (
        (1, 'Apple in-app purchase'),
        (2, 'Local Gateway'),
    )

    name = models.CharField(max_length=120)
    provider = models.ForeignKey('provider.Provider', null=True, blank=True, on_delete=models.SET_NULL)

    image = models.ImageField(upload_to='course/image/', null=True, blank=True)
    image_cover = models.ImageField(upload_to='course/image/cover', blank=True)

    # tutos (s) for ManyToManyField
    tutor = models.ManyToManyField('tutor.Tutor', blank=True)

    category = models.ForeignKey('category.Category')
    # level = models.ManyToManyField('content.Level', blank=True, related_name='+')

    overview = models.TextField(blank=True)
    desc = models.TextField(blank=True)
    info = models.TextField(blank=True, null=True)
    condition = models.TextField(blank=True, null=True)
    access = models.TextField(blank=True, null=True, default='24/7 on demand lifetime access')

    duration = models.DurationField(default=datetime.timedelta(0))
    credit = models.DurationField(default=datetime.timedelta(0))
    expired = models.DurationField(default=datetime.timedelta(0))

    # TODO: Mark Update
    preview_video_type = models.IntegerField(choices=PREVIEW_VIDEO_TYPE_CHOICES, default=-1)
    preview_video_image = models.ImageField(upload_to='course/video/image/', null=True, blank=True)
    preview_video_path = models.CharField(max_length=120, blank=True, default='')

    sort = models.IntegerField(db_index=True, default=0)
    status = models.IntegerField(choices=STATUS_CHOICES, default=0)

    # TODO: Mark Remove
    permission = models.IntegerField(choices=PERMISSION_CHOICES, default=0)

    is_display = models.BooleanField(db_index=True, default=False)
    extra = models.TextField(blank=True)

    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    timeupdate = models.DateTimeField(auto_now=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')
        ordering = ['timestamp']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        from utils.models_image_delete import image_delete_pre_save
        image_delete_pre_save(self)
        super(self.__class__, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        from utils.models_image_delete import image_delete_pre_delete
        image_delete_pre_delete(self)
        super(self.__class__, self).delete(*args, **kwargs)

    @staticmethod
    def pull(id):
        from .cached import cached_course
        return cached_course(id)

    @staticmethod
    def pull_latest_list(app, limit):
        from .cached import cached_course_latest
        return cached_course_latest(app, limit)

    @staticmethod
    def pull_category(category_id, provider):
        if not provider:
            return Course.objects.values_list('id', flat=True).filter(category_id=category_id,
                                                                      is_display=True)

        return Course.objects.values_list('id', flat=True).filter(category_id=category_id,
                                                                  is_display=True,
                                                                  provider=provider)

    @staticmethod
    def access_list(request):
        from django.conf import settings
        from provider.models import Provider
        from category.models import Category
        from django.core.exceptions import PermissionDenied

        from utils.filter import get_q_dashboard

        request.q_provider_list = Provider.pull_list()
        request.q_category_list = Category.pull_list()
        request.q_provider = get_q_dashboard(request, 'provider', 'int')

        if request.user.has_perm('course.view_course',
                                 group=request.DASHBOARD_GROUP):
            content_list = Course.objects.all()
            if request.q_provider:
                content_list = content_list.filter(provider_id=request.q_provider)
        elif request.user.has_perm('course.view_own_course',
                                   group=request.DASHBOARD_GROUP):
            request.q_provider_list = None
            if request.DASHBOARD_PROVIDER is not None:
                content_list = Course.objects.filter(provider=request.DASHBOARD_PROVIDER)
            elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                from subprovider.models import Item
                content_list = Course.objects.filter(id__in=Item.objects
                                                     .values_list('content', flat=True)
                                                     .filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                                             content_type=settings.CONTENT_TYPE('course', 'course')))
            else:
                content_list = Course.objects.none()
        else:
            # content_list = Course.objects.none()
            raise PermissionDenied

        request.q_name = " ".join(get_q_dashboard(request, 'name', 'str').split())
        if request.q_name != '':
            content_list = content_list.filter(name__icontains=request.q_name)
        request.q_category = get_q_dashboard(request, 'category', 'int')
        if request.q_category:
            content_list = content_list.filter(category=request.q_category)
        return content_list

    def check_access(self, request):
        from django.conf import settings
        if request.user.has_perm('course.view_course',
                                 group=request.DASHBOARD_GROUP):
            return True
        elif request.user.has_perm('course.view_own_course',
                                   group=request.DASHBOARD_GROUP):
            if self.provider is None:
                return False
            elif self.provider.account_set.filter(account=request.user).exists():
                return True
            if settings.IS_SUB_PROVIDER:
                from subprovider.models import Item
                if request.DASHBOARD_SUB_PROVIDER is None:
                    return False
                else:
                    return Item.objects.filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                               content_type=settings.CONTENT_TYPE('course', 'course'),
                                               content=self.id).exists()
            else:
                return False
        else:
            return False

    def check_change(self, request):
        from django.conf import settings
        if request.user.has_perm('course.change_course',
                                 group=request.DASHBOARD_GROUP):
            if request.user.has_perm('course.view_course',
                                     group=request.DASHBOARD_GROUP):
                return True
            elif request.user.has_perm('course.view_own_course',
                                       group=request.DASHBOARD_GROUP):
                if self.provider.account_set.filter(account=request.user).exists():
                    return True
                if settings.IS_SUB_PROVIDER:
                    from subprovider.models import Item
                    if request.DASHBOARD_SUB_PROVIDER is None:
                        return False
                    else:
                        return Item.objects.filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                                   content_type=settings.CONTENT_TYPE('course', 'course'),
                                                   content=self.id).exists()
                else:
                    return False
            else:
                return False
        else:
            return False

    def stat_view(self):
        from analytic.cached import cached_analytic_stat
        stat = cached_analytic_stat('course_%s' % self.id)
        try:
            return stat['stat'].value
        except:
            return 0

    def outline_list(self):
        return self.outline_set.all()

    # TODO cleanup
    def cached_price(self):
        from django.conf import settings
        from price.models import Price

        from purchase.models import Apple

        for price in Price.objects.filter(content_type=settings.CONTENT_TYPE('course', 'course'),
                                          content=self.id,
                                          store__in=[1, 3, 4]):
            if price.store == 1:
                setattr(self, 'price_web', price.price)
                setattr(self, 'price_web_discount', price.discount)
                setattr(self, 'price_web_net', price.net)
                setattr(self, 'price_web_set', {'id': price.id,
                                                'price': price.price,
                                                'net': price.net,
                                                'discount': price.discount})
            else:
                setattr(self, 'price_%s' % price.store, price.price)
                setattr(self, 'price_%s_discount' % price.store, price.discount)
                setattr(self, 'price_%s_net' % price.store, price.net)
                setattr(self, 'price_%s_set' % price.store, {'id': price.id,
                                                             'price': price.price,
                                                             'net': price.net,
                                                             'discount': price.discount})
        try:
            apple = Apple.objects.select_related('price_tier', 'discount_tier').filter(content=self.id).first()
            setattr(self, 'price_3', apple.price_tier.price_usd)
            try:
                setattr(self, 'price_3_discount', apple.discount_tier.price_usd)
            except:
                setattr(self, 'price_3_discount', None)
            setattr(self, 'price_3_net', apple.store_price())
            setattr(self, 'price_3_set', apple.api_display_price())
        except:
            setattr(self, 'price_3', None)
            setattr(self, 'price_3_discount', None)
            setattr(self, 'price_3_net', None)
            setattr(self, 'price_3_set', None)

    def api_display_title(self, store=1, install_version=0.0, store_version=0.0):
        from .method_api_display_title import _api_display_title
        return _api_display_title(self, store, install_version, store_version)

    def api_display(self, install_version=0.0, store_version=0.0, account=None, is_video_progress=False, store=1, order_item=None):
        from .method_api_display import _api_display
        return _api_display(self, install_version, store_version, account, is_video_progress, store, order_item)

    def api_relate_for_lesson(self, n=4):
        return Course.objects.select_related('category').filter(is_display=True).exclude(id=self.id).order_by('?')[:n]

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('course:detail', args=[self.id])

    def get_checkout_url(self):
        from django.core.urlresolvers import reverse
        return reverse('course:checkout', args=[self.id])

    def get_price(self, content_type):
        from price.models import Price
        _dict_price = {}
        price_list = Price.objects.filter(content_type=content_type, content=self.id)
        for price in price_list:
            _dict_price[price.store] = price
        return _dict_price

    def set_api_price(self, d, store, is_review=False):
        from django.conf import settings
        from price.models import Price
        store_android = 4
        store_ios = 3
        content_type_course = settings.CONTENT_TYPE('course', 'course')
        price_set = self.get_price(content_type_course)

        # Android Price
        try:
            price_thb = price_set[store_android]
        except:
            price_thb = Price.pull(content_type_course, self.id, store_android)

        d['price_thb'] = price_thb.net
        d['price_thb_set'] = {'price': price_thb.price,
                              'discount': price_thb.discount,
                              'net': price_thb.net}

        # iOS Price
        try:
            price_usd = price_set[store_ios]
        except:
            price_usd = Price.pull(content_type_course, self.id, store_ios)

        if price_usd.ios is not None:
            ios_price = price_usd.ios
            d['price_usd'] = ios_price.discount_tier.price_usd if ios_price.discount_tier else ios_price.price_tier.price_usd
            d['price_usd_set'] = {'price': ios_price.price_tier.price_usd,
                                  'discount': ios_price.discount_tier.price_usd if ios_price.discount_tier else None,
                                  'net': ios_price.discount_tier.price_usd if ios_price.discount_tier else ios_price.price_tier.price_usd}
        else:
            d['price_usd'] = None
            d['price_usd_set'] = {'price': None,
                                  'discount': None,
                                  'net': None}

        d['price'] = price_usd.net if is_review else price_thb.net
        d['price_set'] = price_usd.api_display() if is_review else price_thb.api_display()

        # Price List
        # d['price_list'] = []
        # for price in Price.objects.filter(content_type=content_type, content=course.id):
        #     d['price_list'].append(price.api_display())

    def is_free(self):
        is_cached = getattr(self, 'is_cached', False)
        if not is_cached:
            self.cached_price()
            self.is_cached = True
        from purchase.models import Apple
        for price in self.price_list_cached:
            if price.price != 0.0:
                return False
        price = Apple.objects.filter(content=self.id).first()
        if price is None:
            return True
        else:
            if price.price_tier is None:
                return True
            elif price.price_tier.price_usd == 0.0:
                return True
            else:
                return False
        return True

    def get_extra(self):
        import json
        try:
            extra = json.loads(self.extra)
        except ValueError:
            extra = {}
        return extra

    def get_tutor(self):
        return self.tutor.all().first()

    def get_tutor_list(self):
        return self.tutor.all()

    def get_credit_unlimited(self):
        if int(self.credit.total_seconds()) == 0:
            return True
        else:
            return False

    def get_credit_hour(self):
        s = self.credit.total_seconds()
        d, s = divmod(s, 60*60*24)
        return int(s/(60*60))

    def get_credit_minute(self):
        s = self.credit.total_seconds()
        h, s = divmod(s, 60*60)
        return int(s/60)

    def get_credit_second(self):
        s = self.credit.total_seconds()
        h, s = divmod(s, 60*60)
        m, s = divmod(s, 60)
        return int(s)

    def get_expired_unlimited(self):
        if int(self.expired.total_seconds()) == 0:
            return True
        else:
            return False

    def get_expired_hour(self):
        s = self.expired.total_seconds()
        d, s = divmod(s, 60*60*24)
        return int(s/(60*60))

    def get_expired_minute(self):
        s = self.expired.total_seconds()
        h, s = divmod(s, 60*60)
        return int(s/60)

    def get_expired_second(self):
        s = self.expired.total_seconds()
        h, s = divmod(s, 60*60)
        m, s = divmod(s, 60)
        return int(s)

    def update_duration(self, is_save=True):
        from django.db.models import Sum

        from .cached import cached_course_delete
        import datetime
        try:
            self.duration = Material.objects.filter(course=self,
                                                    type__in=[0, 4],
                                                    is_display=True) \
                                            .aggregate(Sum('video_duration'))['video_duration__sum']
            if self.duration is None:
                self.duration = datetime.timedelta(0)
        except:
            self.duration = datetime.timedelta(0)
        # print('debug duration: ', self.duration)

        # n = datetime.timedelta(0)
        # for material in Material.objects.filter(course=self,
        #                                         type__in=[0, 4],
        #                                         is_display=True):
        #     n += material.video_duration
        # self.duration = datetime.timedelta(seconds=n.total_seconds())
        if is_save:
            self.save(update_fields=['duration'])
            cached_course_delete(self.id)
        return self.duration

    def material_list(self):
        return self.material_set.filter(type=0, is_display=True)

    def get_material_set(self): #Console
        return self.material_set.filter(type=0, is_display=True)

    def progress_update(self):
        from django.conf import settings
        from progress.models import Progress

        Progress.objects.filter(content_type=settings.CONTENT_TYPE('course', 'course'),
                                content=self.id,
                                status__in=[1, 3, 5]) \
                        .update(is_update=False)

    def progress_count(self):
        result = {'content_count': 1,
                  'section_count': self.outline_set.count(),
                  'material_count': self.material_set.filter(is_display=True,
                                                             type__in=[0, 1, 2, 3, 4, 5]).count()}
        return result

    def count_material(self, type=None):
        from .cached import cached_course_delete
        import json
        try:
            extra = json.loads(self.extra)
        except ValueError:
            extra = {}

        extra['count_material'] = self.material_set.filter(is_display=True).count()
        if type is None:
            extra['count_video'] = self.material_set.filter(is_display=True, type=0).count()
            extra['count_link'] = self.material_set.filter(is_display=True, type=1).count()
            extra['count_document'] = self.material_set.filter(is_display=True, type=2).count()
            extra['count_question'] = self.material_set.filter(is_display=True, type=3).count()
            extra['count_audio'] = self.material_set.filter(is_display=True, type=4).count()
        elif type == 0:
            extra['count_video'] = self.material_set.filter(is_display=True, type=0).count()
        elif type == 1:
            extra['count_link'] = self.material_set.filter(is_display=True, type=1).count()
        elif type == 2:
            extra['count_document'] = self.material_set.filter(is_display=True, type=2).count()
        elif type == 3:
            extra['count_question'] = self.material_set.filter(is_display=True, type=3).count()
        elif type == 4:
            extra['count_audio'] = self.material_set.filter(is_display=True, type=4).count()
        else:
            return

        self.extra = json.dumps(extra)
        self.save(update_fields=['extra'])
        cached_course_delete(self.id)


class Outline(models.Model):
    course = models.ForeignKey(Course)
    section = models.CharField(max_length=120)
    desc = models.TextField(blank=True)
    sort = models.IntegerField(db_index=True, default=1)

    # TODO : add is_display
    # จะได้ไม่ต้องเช็ค outline.material_set.count() > 0 ใน template!!

    class Meta:
        ordering = ['sort']

    def __str__(self):
        return self.section

    def pull_material(self):
        material_list = self.material_set.all()
        for material in material_list:
            material.get_content_cached()
        return material_list

    def api_display(self, account=None, args={}, reverse=False, is_video_list=False, order_item=None):
        d = {
            'id': self.id,
            'section': self.section,
            'desc': self.desc,
            'video_count': 0,
            'video_list': []
        }
        for material in self.material_set.filter(is_display=True):
            if material.type == 0:
                d['video_count'] += 1
            d['video_list'].append(material.api_display(account, args, reverse, order_item=order_item))
        return d

    def material_list(self):
        return self.material_set.filter(is_display=True)

    # TODO: Migrate to material_list
    def get_material_set(self):
        return self.material_set.filter(is_display=True)

    def get_material_count(self):
        return self.get_material_set().count()

    def get_public_video(self):
        if self.course.permission == 2:
            return self.material_set.filter(is_display=True).exclude(video_type=-1)
        else:
            return self.material_set.filter(is_public=True, is_display=True).exclude(video_type=-1)

    def has_public_video(self):
        if self.course.permission == 2:
            return self.material_set.filter(is_display=True).exclude(video_type=-1).exists()
        else:
            return self.material_set.filter(is_public=True, is_display=True).exclude(video_type=-1).exists()


class Material(models.Model):
    import datetime
    TYPE_CHOICES = (
        (0, 'Video'),
        (1, 'Web Link'),
        (2, 'Document'),
        (3, 'Test'), #Question
        (4, 'Audio'),  #HLS audio-only (.mp3)
        (5, 'SCORM'), # ห้ามเปลี่ยน
    )

    VIDEO_TYPE_CHOICES = (
        #Dashboard Course ordering
        #(-1, 'Unknown'),
        #(0, 'Full Path'),
        (3, 'Nimble'),
        (1, 'Youtube'), #Not imperment
        #(2, 'WowZa'),
    )

    VIDEO_STATUS_CHOICES = (
        (-1, 'Unknown'),
        (0, 'Uploading.'),
        (1, 'Waiting'),
        (2, 'Success.'),
    )

    course = models.ForeignKey(Course)
    outline = models.ForeignKey(Outline)

    image = models.ImageField(upload_to='course/material/image/%Y/%m/', null=True, blank=True)
    name = models.CharField(max_length=120)
    desc = models.TextField(blank=True)

    type = models.IntegerField(choices=TYPE_CHOICES, default=0)

    video_type = models.IntegerField(choices=VIDEO_TYPE_CHOICES, default=3)
    video_duration = models.DurationField(default=datetime.timedelta(0))
    video_status = models.IntegerField(choices=VIDEO_STATUS_CHOICES, default=-1)
    video_is_verify = models.BooleanField(default=False)
    video_allow_mark_complete = models.BooleanField(default=True)  # allow user mark complete (skip to end of video or send action finish)

    document = models.FileField(upload_to='course/material/document/%Y/%m/', null=True, blank=True)
    question = models.ForeignKey('question.Activity', null=True, blank=True)

    data = models.CharField(max_length=256, blank=True)  # Migrate From: path (video s3), link, pdf
    sort = models.IntegerField(db_index=True, default=1)

    is_public = models.BooleanField(default=False)
    is_display = models.BooleanField(default=False, db_index=True)  # Don't change default (depend on create and progress)

    class Meta:
        ordering = ['sort']

    def __str__(self):
        return str(self.id)

    @staticmethod
    def pull(id):
        from .cached import cached_course_material
        return cached_course_material(id)

    def api_display_title(self):
        return {'id': self.id,
                'name': self.name}

    def api_display(self, account=None, args={}, reverse=False, set_index=True, order_item=None):
        from .api.material import api_display
        return api_display(self, account, args, reverse, set_index, order_item)

    def get_url(self, ip=None, is_api=False, is_secure=True):
        if self.type == 0 or self.type == 4:
            url = self.stream_src(ip, is_secure)
        elif self.type == 1:
            url = self.data
        elif self.type == 2:
            url = self.document.url if bool(self.document) else None
        else:
            url = self.data
        return url

    def get_scorm_url(self):
        from django.conf import settings
        return '%s%s' % (settings.MEDIA_URL, self.data)

    def stream_src(self, ip, is_secure=True):
        from django.conf import settings
        from stream.models import Stream
        from live.models import Live
        from .cached import cached_course_material_update

        if self.type == 0:  # Video
            if self.video_type == 1:  # Youtube
                return self.data
            elif self.video_type in [2, 3]:  # WowZa #Nimble
                if self.video_type == 2:
                    self.video_type = 3
                    self.save(update_fields=['video_type'])
                    cached_course_material_update(self)

                prefix = settings.NIMBLE_URL if is_secure else settings.NIMBLE_HTTP_URL
                if getattr(settings, 'NIMBLE_SIGN', True):  
                    sign = '?wmsAuthSign=%s' % Stream.nimble_sign(ip, Live.VOD_KEY)
                    url = '%s/%s/playlist.m3u8%s' % (prefix, self.data, sign)
                else:
                    url = '%s/%s/playlist.m3u8' % (prefix, self.data)
                return url
            else:
                return None
        elif self.type == 4:  # Audio
            prefix = settings.NIMBLE_URL if is_secure else settings.NIMBLE_HTTP_URL
            if getattr(settings, 'NIMBLE_SIGN', True):
                sign = '?wmsAuthSign=%s' % Stream.nimble_sign(ip, Live.VOD_KEY)
                # url = '%s/%s/playlist.m3u8%s' % (prefix, self.data, sign) # For IOS Only
                url = '%s/%s%s' % (prefix, self.data, sign)
            else:
                # url = '%s/%s/playlist.m3u8' % (prefix, self.data)
                url = '%s/%s' % (prefix, self.data) # Debug
            return url
        else:
            return None

    # TODO: remove (use utils.content get_content)
    def get_content_cached(self):
        from django.conf import settings
        from question.models import Activity

        if self.type == 3:  # Question
            self.content_cached = Activity.pull(self.question_id)
            self.content_type_cached = settings.CONTENT_TYPE('question', 'activity')
        else:
            self.content_cached = self
            self.content_type_cached = settings.CONTENT_TYPE('course', 'material')

    def progress_count(self):
        return {
            'content_count': 0,
            'section_count': 0,
            'material_count': 1
        }

    def dashboard_video_class(self):
        if self.type == 0:  # Video
            if self.video_is_verify:
                return ''
            if self.video_status == 2:
                return 'warning'
            else:
                return 'info'
        elif self.type == 4:  # Audio
            if self.video_is_verify:
                return ''
            if self.video_status == 2:
                return 'warning'
            else:
                return 'info'
        else:
            return ''

    def get_dependency_parent(self):
        from django.conf import settings
        from dependency.models import Dependency
        return Dependency.pull_parent(settings.CONTENT_TYPE('course', 'material'), self.id)

    def get_progress(self, order_item, program):
        from django.conf import settings

        from content.models import Location as ContentLocation
        from progress.models import Progress

        if self.type == 3:
            _content_type = settings.CONTENT_TYPE('question', 'activity')
            _content_id = self.question_id
            if program and program.type == 2:
                _content_location = ContentLocation.pull_first(
                    _content_type,
                    _content_id,
                    parent1_content_type=settings.CONTENT_TYPE('program', 'onboard'),
                    parent1_content=order_item.content,
                    parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                    parent2_content=self.course_id,
                    parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
                    parent3_content=self.id
                )
            elif program:
                _content_location = ContentLocation.pull_first(
                    _content_type,
                    _content_id,
                    parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                    parent1_content=order_item.content,
                    parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                    parent2_content=self.course_id,
                    parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
                    parent3_content=self.id
                )
            else:
                _content_location = ContentLocation.pull_first(
                    _content_type,
                    _content_id,
                    parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                    parent1_content=self.course_id,
                    parent2_content_type=settings.CONTENT_TYPE('course', 'material'),
                    parent2_content=self.id
                )
        else:
            _content_type = settings.CONTENT_TYPE('course', 'material')
            _content_id = self.id
            if program and program.type == 2:
                _content_location = ContentLocation.pull_first(
                    _content_type,
                    _content_id,
                    parent1_content_type=settings.CONTENT_TYPE('program', 'onboard'),
                    parent1_content=order_item.content,
                    parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                    parent2_content=self.course_id
                )
            elif program:
                _content_location = ContentLocation.pull_first(
                    _content_type,
                    _content_id,
                    parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                    parent1_content=order_item.content,
                    parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                    parent2_content=self.course_id
                )
            else:
                _content_location = ContentLocation.pull_first(
                    _content_type,
                    _content_id,
                    parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                    parent1_content=self.course_id
                )
        progress = Progress.pull(
            order_item,
            _content_location,
            _content_type,
            _content_id
        )
        return progress


# TODO: Check
class Upload(models.Model):
    from django.conf import settings

    STATUS_CHOICES = (
        (-5, 'Cancel'),
        (-4, 'FFmpeg not Found.'),
        (-3, 'Copy Fail'),
        (-1, 'Upload Fail'),
        (1, 'Uploading'),
        (2, 'Upload Complete'),
        (3, 'Copy to Stream Server'),
        (4, 'Success'),
    )

    material = models.ForeignKey(Material)
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    filename = models.CharField(max_length=120, db_index=True)
    uuid = models.CharField(max_length=120)
    progress = models.IntegerField()
    status = models.IntegerField(choices=STATUS_CHOICES, db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']

    @staticmethod
    def pull(filename, account, material=None):
        from django.conf import settings
        from django.utils import timezone
        import os, uuid
        if material is None:
            upload = Upload.objects.filter(filename=filename).first()

        else:
            upload = Upload.objects.filter(material=material,
                                           filename=filename).first()
        if upload is None:
            Upload.objects.filter(
                material=material,
                status=1
            ).update(status=-5)
            now = timezone.now()
            uuid = '%s-%s-%s'%(now.year, now.month, uuid.uuid4())
            upload = Upload.objects.create(account=account,
                                           material=material,
                                           filename=filename,
                                           uuid=uuid,
                                           progress=0,
                                           status=1)
            path = os.path.join(settings.BASE_DIR, 'upload', upload.uuid)
            if not os.path.isdir(path):
                os.makedirs(path)
        else:
            path = os.path.join(settings.BASE_DIR, 'upload', upload.uuid)
        return upload, path

    def get_filename(self):
        if self.material.type == 0:
            return '%s.mp4' % self.uuid
        elif self.material.type == 4:
            return '%s.mp3' % self.uuid
        elif self.material.type == 5:
            return '%s.zip' % self.uuid
        else:
            return None

    def get_media(self):
        if self.material.type == 0:
            return '%s/%s.mp4' % (self.material.course_id, self.material_id)
        elif self.material.type == 4:
            return '%s/%s.mp3' % (self.material.course_id, self.material_id)

    def push_stream(self):
        from stream.models import Config, Stream
        count = 0
        for stream in Stream.objects.filter(material_id=self.material_id):
            count += 1
            stream.media = '%s/%s' % (self.material.course_id, self.get_filename())
            stream.save(update_fields=['media'])
        if count == 0:
            config = Config.objects.filter(is_default=True).first()
            if config is not None:
                Stream.objects.create(config=config,
                                      material_id=self.material_id,
                                      media=self.get_media(),
                                      is_public=True)

    def get_duration_ffmpeg(self):
        from django.conf import settings

        from .cached import cached_course_material_update

        import subprocess
        import datetime
        _path = '%s/upload/%s'%(settings.BASE_DIR, self.get_filename())
        result = subprocess.Popen('ffprobe -i %s -show_entries format=duration -v quiet -of csv="p=0"' % _path,
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.STDOUT,
                                  shell=True)
        output = result.communicate()
        try:
            duration = float(output[0])
            self.material.video_duration = datetime.timedelta(seconds=int(duration))
            self.material.video_status = 2
            self.material.video_is_verify = True
            self.material.save(update_fields=['video_duration', 'video_status', 'video_is_verify'])
            cached_course_material_update(self.material)
            self.material.course.update_duration()
        except:
            raise
            duration = 0.0
            self.status = -4
            self.save()
