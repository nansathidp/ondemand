from django.shortcuts import render, get_object_or_404

from course.models import Course
from category.models import Category


def group_view(request, group_name):
    desc = ''
    if group_name == 'free':
        group = 1
        desc = 'มอบโอกาสให้น้องๆ นักเรียนทั่วประเทศ ซึ่งอาจเป็นคนที่มีโอกาสได้เรียนพิเศษหรือคนที่ไม่มีโอกาสได้เรียนพิเศษ ก็สามารถเข้าถึงได้โดย ไม่เสียค่าใช้จ่ายใดๆ ทั้งสิ้น ...บรรจุเนื้อหาที่ผ่านการสรุปในประเด็นสำคัญที่ไม่ควรมองข้าม ยกตัวอย่างในแต่ละเรื่อง เพื่อให้เห็นภาพของข้อสอบที่อาจจะเกิดขึ้นในทุกสนามสอบให้เข้าใจ ง่าย'
    elif group_name == 'intensive':
        group = 2
        desc = 'เมื่อชีวิตการสอบเข้ามหาวิทยาลัยขึ้นอยู่กับเกรดปลายภาค คัมภีร์สรุปย่อย 7 วิชาสามัญอย่างละเอียดจึงเกิดขึ้น ทุกเนื้อหา ทุกบท ทุกวิชาที่เพิ่มเติมจากในห้องเรียนถูกรวมมาไว้ในคอร์สนี้ พร้อมเทคนิคการจำที่ช่วยให้เกิดความเข้าใจมากขึ้น ผนวกกับตัวอย่างข้อสอบที่น้องๆ สามารถแก้โจทย์ขั้นเทพได้ด้วยตนเอง'
    elif group_name == 'admission':
        group = 3
    elif group_name == 'trick':
        return course_group_4( request )
    else:
        group = 0
    featured_course_list = _get_featured_course_list_by_group(group=group)
    content_list = _get_course_list_by_group(group=group)
    return render(request,
                  'course/list.html',
                  {'ROOT_PAGE': 'course',
                   'featured_course_list': featured_course_list,
                   'content_list': content_list,
                   'desc': desc,
                   'group': group})


# Group to 1 View
# Click For Trick
def course_group_4( request ):
    course_id_list = Course.objects.values_list('id', flat=True).filter(is_display=True, group=4)[:5]
    course_list_display = _get_course_display(course_id_list)
    return render( request,
                   'course/grid.html',
                   {'ROOT_PAGE': 'course',
                    'course_list' : course_list_display,
                    'header' : 'Click For Tricks'})


def grid_view(request):
    category_name = request.GET.get('category')
    group = request.GET.get('g')
    if category_name:
        category = get_object_or_404(Category, is_display=True, name=category_name)
        course_id_list = Course.objects.values_list('id', flat=True).filter(is_display=True, category=category, group=group)
        course_list_display = _get_course_display(course_id_list)
    else:
        course_list_display = []
    return render(request,
                  'course/grid.html',
                   {'ROOT_PAGE': 'course',
                    'header': category_name,
                    'course_list': course_list_display,
                    'group': group})


def by_subject_view(request, subject_name):
    if subject_name == 'O-Net' or subject_name == 'o-net':
        header = 'O-Net'
        category_id = ['1434447014002505']
    elif subject_name == 'GAT-PAT' or subject_name == 'gat-pat':
        header = 'GAT/PAT'
        category_id = ['1434447280002508', '1434447203002506']
    elif subject_name == '7-Subject' or subject_name == '7-subject':
        header = '7 Subject'
        category_id = ['1434447324002509']
    elif subject_name == 'Quota' or subject_name == 'quota':
        header = 'Quota'
        category_id = ['1434617069002642', '1434617121002643']
    else:
        category_id = []
    category_list = Category.objects.filter(id__in=category_id)
    course_id_list = Course.objects.values_list('id', flat=True).filter(is_display=True, category__in=category_list)
    course_list_display = _get_course_display(course_id_list)
    return render( request,
                   'course/grid.html',
                   {'ROOT_PAGE': 'course',
                    'header': header,
                    'course_list': course_list_display})


def _get_course_display(course_list):
    course_list_display = []
    for course_id in course_list:
        course = Course.pull(course_id)
        if course is not None:
            course_list_display.append(course)
    return course_list_display


def _get_featured_course_list_by_group(group):
    featured_course_list = []
    for course_id in Course.objects.values_list('id', flat=True).filter(is_display=True, is_featured=True, group=group).order_by('-timestamp')[:8]:
        course = Course.pull(course_id)
        if course is not None:
            featured_course_list.append(course)
    return featured_course_list


def _get_course_list_by_group(group):
    result = []
    category_list = Category.objects.filter(is_display=True).order_by('sort')
    for category in category_list:
        d = {
            'category': category.name,
            'course_list': []
        }
        for course_id in Course.objects.values_list('id', flat=True) \
                                       .filter(category=category, group=group, is_display=True) \
                                       .order_by('sort')[:5]:
            course = Course.pull(course_id)
            if course is not None:
                d['course_list'].append(course)
        if len(d['course_list']) > 0:
            result.append(d)
    return result


