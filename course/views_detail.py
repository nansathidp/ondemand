from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render, redirect

from .models import Course
from program.models import Program
from access.models import Access
from analytic.models import Stat
from dependency.models import Dependency
from discussion.models import Topic
from term.models import Term
from rating.models import Rating
from progress.models import Progress
from content.models import Location as ContentLocation
from order.models import Item as OrderItem

from ondemand.views_decorator import check_project
from .views_playlist import TemplateIterator

if settings.IS_IPTABLE:
    from iptable.models import Iptable
    from api.views_ip import get_client_ip

    
def _detail(request, content_location, course, order_item, program=None):
    progress = None
    parent_url = None
    user_not_login = True
    is_user_verify = True
    is_email_valid = False
    is_require_term = False
    count_discussion = 0
    root_page = 'course'

    is_dependency, dependency_list = Dependency.check_content2(settings.CONTENT_TYPE('course', 'course'),
                                                               course.id,
                                                               request.user)
    for parent in dependency_list:
        parent.get_content_cached()

    # Access permission
    content_permission, is_require_permission = Access.get_permission(course.id, settings.CONTENT_TYPE('course', 'course').id, request.user)

    is_display_info = True
    material_id = None
    if request.user.is_authenticated():
        if order_item is not None and order_item.account_id != request.user.id:
            raise Http404

        is_email_valid = request.user.is_valid_email()
        user_not_login = False
        is_user_verify = request.user.is_verify

        if order_item is not None:
            if order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
                parent_url = reverse('program:detail', args=[order_item.content, order_item.id])

            for material in course.material_set.filter(is_display=True).order_by('outline__sort', 'sort'):
                progress = material.get_progress(order_item, program)
                if progress.status == 1:
                    material_id = material.id
                    break

            is_require_term = False
            # is_require_term = Term.is_require_term(settings.CONTENT_TYPE('course', 'course').id,
            #                                        course.id,
            #                                        request.user.id)

            template = 'course/detail_library.html'
            progress = Progress.pull(order_item,
                                     content_location,
                                     settings.CONTENT_TYPE('course', 'course'),
                                     course.id)
            count_discussion = Topic.objects.filter(
                content=course.id,
                content_type=settings.CONTENT_TYPE('course', 'course'),
                is_display=True
            ).count()
        else:
            template = 'course/detail.html'
    else:
        template = 'course/detail.html'

    # relate_course_list = cached_get_course_relate(course, 4)

    if settings.PROJECT == 'unilever':
        if course.subject:
            root_page = course.subject.slug

    rating = Rating.pull_first(settings.CONTENT_TYPE('course', 'course'),
                               course.id)
    tutor_list = course.get_tutor_list()
    Stat.push('course_%s' % course.id, request.META.get('HTTP_REFERER', '-'), request.META.get('HTTP_USER_AGENT', ''))

    if settings.IS_IPTABLE:
        is_iptable = Iptable.check_ip(settings.CONTENT_TYPE('course', 'course'),
                                      course.id,
                                      get_client_ip(request))
    else:
        is_iptable = True
    return render(request,
                  template,
                  {
                      'TITLE': course.name,
                      'IMAGE_URL': course.image.url if bool(course.image) else None,
                      'DESC': course.desc,
                      'DISPLAY_SUBMENU': True,
                      'ROOT_PAGE': root_page,
                      'PARENT_URL': parent_url,
                      'course': course,
                      'program': program,
                      'content_location': content_location,
                      'content_type': settings.CONTENT_TYPE('course', 'course'),
                      'category': course.category,
                      'provider': course.provider if course.provider is not None else None,
                      'tutor_list': tutor_list,
                      'material_id': material_id,
                      'order_item': order_item,
                      'count_discussion': count_discussion,
                      'rating': rating,
                      'rating_log': rating.pull_log(request.user),
                      'rating_log_list': rating.pull_log_list(),
                      'progress': progress,
                      # 'relate_course_list': relate_course_list,
                      'user_not_login': user_not_login,
                      'content_permission': content_permission,
                      'is_require_permission': is_require_permission,
                      'is_email_valid': is_email_valid,
                      'is_user_verify': is_user_verify,
                      'is_display_info': is_display_info,
                      'is_require_term': is_require_term,
                      'is_dependency': is_dependency,
                      'dependency_list': dependency_list,
                      'is_iptable': is_iptable,
                      'counter': TemplateIterator(1)
                  })


@check_project
def detail_view(request, course_id, order_item_id=None):
    order_item = None
    course = Course.pull(course_id)
    if course is None:
        raise Http404
    
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'), course.id)
    if request.user.is_authenticated:
        if order_item_id is not None:
            order_item = OrderItem.pull(order_item_id)

            if order_item is None or order_item.account_id != request.user.id:
                return redirect('course:detail', course.id)
        else:
            order_item = OrderItem.pull_purchased(content_location,
                                                  request.user,
                                                  settings.CONTENT_TYPE('course', 'course'),
                                                  course.id)
            if order_item is not None:
                return redirect('course:detail', course.id, order_item.id)
    elif order_item_id is not None:
        return redirect('course:detail', course.id)

    if not course.is_display:
        preview = request.GET.get('preview', None)
        if preview and preview.lower() == 'true' and request.user.is_authenticated and course.check_access(request):
            pass
        elif order_item:
            pass
        else:
            raise Http404
    return _detail(request, content_location, course, order_item)


@check_project
def detail_program_view(request, program_id, course_id, order_item_id=None):
    program = Program.pull(program_id)
    course = Course.pull(course_id)

    if program is None or course is None:
        raise Http404

    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                  course.id,
                                                  parent1_content_type=content_type,
                                                  parent1_content=program.id)
    if request.user.is_authenticated:
        if order_item_id is not None:
            order_item = OrderItem.pull(order_item_id)
            if order_item is None or order_item.account_id != request.user.id:
                return redirect('course:detail_program', course.id, program.id)
        else:
            order_item = OrderItem.pull_purchased(content_location,
                                                  request.user,
                                                  settings.CONTENT_TYPE('course', 'course'),
                                                  course.id)
            if order_item is not None:
                return redirect('course:detail_program', course.id, program.id, order_item.id)
    elif order_item_id is not None:
        return redirect('course:detail_program', course.id)
    return _detail(request, content_location, course, order_item, program=program)

# @check_project
# def detail_slug_view(request, course_id, slug, order_item_id=None):
#     course = Course.pull(course_id)
#     if course is None:
#         raise Http404
#     content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
#                                                   course.id)
#     if request.user.is_authenticated():
#         if order_item_id is not None:
#             order_item = OrderItem.pull(order_item_id)
#         else:
#             order_item = OrderItem.pull_purchased(content_location,
#                                                   request.user,
#                                                   settings.CONTENT_TYPE('course', 'course'),
#                                                   course.id)
#             if order_item is not None:
#                 return redirect('course:detail', course.id, order_item.id)
#     elif order_item_id is not None:
#         return redirect('course:detail', course.id)
#     return _detail(request, content_location, course, order_item)
