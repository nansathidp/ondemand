from django.test import TestCase
from django.urls import reverse

from .tests import dashboard_user, dashboard_course

class PermissionTests(TestCase):
    def setUp(self):
        self.client.force_login(dashboard_user(None))
        self.course = dashboard_course(None)
        
    def test_dependency(self):
        response = self.client.get(reverse('dashboard:course:dependency', args=[self.course.id]))
        self.assertEqual(response.status_code, 403)

    def test_dependency_search(self):
        response = self.client.get(reverse('dashboard:course:dependency_search', args=[self.course.id]))
        self.assertEqual(response.status_code, 403)

    def test_dependency_search_add(self):
        response = self.client.get(reverse('dashboard:course:dependency_search_add', args=[self.course.id]))
        self.assertEqual(response.status_code, 403)

    def test_dependency_search_delete(self):
        response = self.client.get(reverse('dashboard:course:dependency_search_delete', args=[self.course.id]))
        self.assertEqual(response.status_code, 403)

    def test_dependency_delete(self):
        response = self.client.get(reverse('dashboard:course:dependency_delete', args=[self.course.id]))
        self.assertEqual(response.status_code, 403)
