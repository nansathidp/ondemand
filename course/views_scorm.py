from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404

from order.models import Item as OrderItem
from course.models import Material
from progress.models import ScormLog

@csrf_exempt
def scorm_view(request):
    if request.method == 'POST':
        ref = request.META.get('HTTP_REFERER', None)
        if ref is not None:
            order_item_id = ref.split('/')[-2]
            order_item = get_object_or_404(OrderItem, id=order_item_id)
            material_id = ref.split('/')[-3]
            material = Material.pull(material_id)
            element = request.POST.get('element', None)
            value = request.POST.get('value', None)
            ScormLog.push(order_item, request.user, material, element, value)
    return JsonResponse({})
