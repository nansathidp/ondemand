import datetime
import json

from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt

from api.views_ais_privilege import get_client_ip
from course.models import Course, Material

from ..cached import cached_course_material_update
from .views import check_change


@csrf_exempt
@check_change
def material_verify_view(request, course_id):
    course = request.course

    outline = None
    if request.method == 'POST':
        material = get_object_or_404(Material, course=course, id=request.POST.get('material'))
        action = request.POST.get('action')
        if action == 'pull':
            ip = get_client_ip(request)
            d = {'url': material.stream_src(ip),
                 'type': material.type}
            return HttpResponse(json.dumps(d, indent=2),
                                content_type="application/json")
        elif action == 'duration':
            try:
                duration = int(float(request.POST.get('duration')))
            except:
                duration = 0
            material.video_duration = datetime.timedelta(seconds=duration)
            material.video_status = 2
            material.video_is_verify = True
            material.save(update_fields=['video_duration', 'video_status', 'video_is_verify'])
            cached_course_material_update(material)
            
            # Update Course Duration
            course.update_duration()
            return render(request,
                          'course/dashboard/material_block.html',
                          {'outline': material.outline})
    return render(request,
                  'course/dashboard/material_block.html',
                  {'outline': outline})
