from django.conf import settings
from django.shortcuts import render

from progress.models import Progress, Content as ProgressContent
from order.models import Item as OrderItem
from utils.paginator import paginator
from content.models import Location as ContentLocation

from .views import check_access, pull_breadcrumb


@check_access
def progress_view(request, course_id):
    course = request.course

    content_type = settings.CONTENT_TYPE('course', 'course')

    progress_content_sum = ProgressContent.pull(ContentLocation.pull_first(content_type,
                                                                           course.id).id,
                                                content_type.id,
                                                course.id)
    progress_content_list = ProgressContent.objects.filter(location__isnull=False,
                                                           content_type=content_type,
                                                           content=course.id)
    
    progress_list = Progress.objects.filter(content_type=content_type, content=course.id)
    progress_list = paginator(request, progress_list)
    for progress in progress_list:
        progress.item_cached = OrderItem.pull(progress.item_id)
        progress.get_account_cached()
    
    return render(request,
                  'course/dashboard/progress.html',
                  {'SIDEBAR': 'course',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'course': course,
                   'progress_content_sum': progress_content_sum,
                   'progress_content_list': progress_content_list,
                   'progress_list': progress_list})
