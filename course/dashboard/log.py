from django.conf import settings
from django.core import serializers

from log.models import Log


def _provider(request):
    if request.DASHBOARD_PROVIDER is not None:
        return request.DASHBOARD_PROVIDER.id
    else:
        return -1


def _sub_provider(request):
    if settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
        return request.DASHBOARD_SUB_PROVIDER.id
    else:
        return -1


def _log(code, request, old, new, id):
    if old is None:
        old = ''
    elif type(old) == type([]):
        old = serializers.serialize('json', old)
    else:
        old = serializers.serialize('json', [old])
    if new is None:
        new = ''
    elif type(new) == type([]):
        new = serializers.serialize('json', new)
    else:
        new = serializers.serialize('json', [new])
    Log.push(code,
             _provider(request),
             _sub_provider(request),
             request.user,
             None,
             '',
             settings.CONTENT_TYPE('course', 'course'),
             id,
             old,
             new)


def log_create(request, course):
    _log('create', request, None, course, course.id)


def log_edit(request, course_old, course):
    _log('edit', request, course_old, course, course.id)


def log_outline_add(request, outline):
    _log('outline_add', request, None, outline, outline.course_id)


def log_outline_edit(request, outline_old, outline):
    _log('outline_edit', request, outline_old, outline, outline.course_id)


def log_outline_sort(request, outline_old, outline):
    _log('outline_sort', request, outline_old, outline, outline.course_id)


def log_outline_delete(request, outline_old):
    _log('outline_delete', request, outline_old, None, outline_old.course_id)


def log_material_add(request, material):
    _log('material_add', request, None, material, material.course_id)


def log_material_edit(request, material_old, material):
    _log('material_edit', request, material_old, material, material.course_id)


def log_material_sort(request, material_old, material):
    _log('material_sort', request, material_old, material, material.course_id)


def log_material_display(request, material_old, material):
    _log('material_display', request, material_old, material, material.course_id)


def log_material_delete(request, material_old):
    _log('material_delete', request, material_old, None, material_old.course_id)


def log_material_dependency_add(request, material, dependency_parent):
    _log('material_dependency_add', request, None, [material, dependency_parent], material.course_id)


def log_material_dependency_delete(request, material, dependency_parent_old):
    _log('material_dependency_delete', request, None, [material, dependency_parent_old], material.course_id)

