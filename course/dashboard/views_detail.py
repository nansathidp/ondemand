import copy

import django_rq
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect, render

from category.job.count_content import _count_content as category_count_content
from tutor.job.count_content import _count_content as tutor_count_content
from utils.crop import crop_image
from .forms import InformationForm
from .log import log_edit
from .views import check_access, pull_breadcrumb
from ..cached import cached_course_delete_all


@check_access
def detail_view(request, course_id):
    course = request.course
    course_old = copy.copy(course)

    is_change = course.check_change(request)

    old_category = course.category_id
    old_tutor = [tutor.id for tutor in course.tutor.all()]
    if request.method == 'POST':
        if not is_change:
            raise PermissionDenied

        if request.user.has_perm('course.view_course', group=request.DASHBOARD_GROUP):
            information_form = InformationForm(None, request.POST, request.FILES, instance=course)
        elif request.user.has_perm('course.view_own_course', group=request.DASHBOARD_GROUP):
            information_form = InformationForm(request, request.POST, request.FILES, instance=course)
        else:
            raise PermissionDenied

        if information_form.is_valid():
            course = information_form.save()
            cached_course_delete_all(request, course)
            crop_image(course, request)
            log_edit(request, course_old, course)
            if course.category_id != old_category:
                django_rq.enqueue(category_count_content, old_category)
                django_rq.enqueue(category_count_content, course.category_id)
            tutor_found = []
            for tutor in course.tutor.all():
                is_found = False
                for _ in old_tutor:
                    if tutor.id == _:
                        is_found = True
                        tutor_found.append(_)
                        break
                if is_found:
                    tutor_found.append(tutor.id)
                else:
                    django_rq.enqueue(tutor_count_content, tutor.id)
            for _ in old_tutor:
                is_found = False
                for __ in tutor_found:
                    if _ == __:
                        is_found = True
                        break
                if not is_found:
                    django_rq.enqueue(tutor_count_content, _)

            if 'save-next' in request.POST:
                return redirect('dashboard:course:information', course.id)

    if request.user.has_perm('course.view_course', group=request.DASHBOARD_GROUP):
        information_form = InformationForm(None, instance=course)
    elif request.user.has_perm('course.view_own_course', group=request.DASHBOARD_GROUP):
        information_form = InformationForm(request, instance=course)
    else:
        raise PermissionDenied

    return render(request, 'course/dashboard/information.html',
                  {'SIDEBAR': 'course',
                   'TAB': 'information',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'course': course,
                   'information_form': information_form,
                   'is_change': is_change})
