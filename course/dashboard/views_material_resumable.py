import os
import shutil

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from course.models import Course, Upload
from utils.resumable import Resumable
from question.dashboard.views_section_detail import check_ip
from .views import check_change
from course.jobs import upload_scorm, _upload_video

import django_rq


def _complete(resumable, upload, path):
    storage = FileSystemStorage(location=os.path.join(settings.BASE_DIR, 'upload'))
    filename = upload.get_filename()
    if os.path.isfile('%s/%s' % (os.path.join(settings.BASE_DIR, 'upload'), filename)):
        os.remove('%s/%s' % (os.path.join(settings.BASE_DIR, 'upload'), filename))
    storage.save(filename, resumable)
    resumable.delete_chunks()
    shutil.rmtree(path)
    upload.status = 2
    upload.save(update_fields=['progress', 'status'])
    material = upload.material
    material.video_status = 1
    material.save(update_fields=['video_status'])

    if upload.material.type == 5:
        django_rq.enqueue(upload_scorm, upload.id)
    else:
        django_rq.enqueue(_upload_video, upload.id)


@csrf_exempt
@check_change
def material_resumable_view(request, course_id):
    course = request.course

    if request.method == 'POST' and check_ip(request) is True:

        chunk = request.FILES.get('file')
        upload, path = Upload.pull(request.POST.get('resumableFilename'), request.user)
        resumable = Resumable(path, upload.get_filename(), request.POST)
        if resumable.chunk_exists:
            return HttpResponse('chunk already exists')
        resumable.process_chunk(chunk)
        if resumable.is_complete:
            _complete(resumable, upload, path)
        else:
            try:
                n = float(request.POST.get('resumableChunkNumber'))
                t = float(request.POST.get('resumableTotalChunks'))
                p = int((n / t) * 100.0)
                if p >= upload.progress + 5:
                    upload.progress = p
                    upload.save(update_fields=['progress'])
            except:
                pass
        return HttpResponse()
    else:
        upload, path = Upload.pull(request.GET.get('resumableFilename'), request.user)
        resumable = Resumable(path, upload.get_filename(), request.GET)
        if not (resumable.chunk_exists or resumable.is_complete):
            return HttpResponse('chunk not found', status=404)
        else:
            if resumable.is_complete:
                _complete(resumable, upload, path)
            return HttpResponse('chunk already exists')



