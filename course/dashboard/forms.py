from django import forms
from django.conf import settings
from provider.models import Provider

from ..models import Course, Material  # CourseLevel


class InformationForm(forms.ModelForm):
    IPTABLE_CHOICES = (
        (1, 'Intranet and Internet'),
        (2, 'Intranet only'),
    )

    iptable = forms.ChoiceField(choices=IPTABLE_CHOICES, label='การจำกัดสิทธิการเข้าถึง',
                                help_text='Intranet คือ การเชื่อมต่ออินเตอร์เน็ตภายในองค์กร')

    class Meta:
        model = Course
        fields = ['name',
                  'image',
                  'provider',
                  'tutor',
                  'category',
                  'overview', 'condition', 'desc', 'is_display']

        labels = {'name': 'ชื่อคอร์ส<br /> Course Name',
                  'image': 'รูปหน้าปกคอร์ส<br /> Course Cover',
                  'provider': 'Learning Center',
                  'tutor': 'ผู้บรรยาย/ผู้สอน<br />Instructor',
                  'category': 'หมวดหมู่<br />Category',
                  'overview': 'คำแนะนำคอร์ส<br />Course Overview',
                  'info': 'คอร์สเรียนประกอบด้วย<br /> What\'s inside',
                  'is_display': 'แสดงบนหน้าเว็บ?<br />Display',
                  'desc': 'รายละเอียดคอร์ส<br/>Course Description',
                  'condition': 'เงื่อนไข<br/>Course condition'}

        help_texts = {'name': 'Limit at 120 characters',
                      'image': settings.HELP_TEXT_IMAGE('course'),
                      'tutor': 'กด Ctrl ค้าง และคลิกที่ชื่อเพื่อเลือกผู้บรรยายหลายคน',
                      'overview': 'Limit at 250 characters'}

    def __init__(self, request=None, *args, **kwargs):
        super(InformationForm, self).__init__(*args, **kwargs)
        self.fields['provider'].required = True
        if request is not None:
            self.fields['provider'].queryset = Provider.access(request)
            self.fields['tutor'].queryset = self.fields['tutor'].queryset.filter(providers__in=Provider.access(request))
        if settings.IS_IPTABLE:
            from iptable.models import Iptable
            if self.instance.id:
                self._iptable = Iptable.pull(settings.CONTENT_TYPE('course', 'course'),
                                             content=self.instance.id)
                self.fields['iptable'].initial = self._iptable.type
        else:
            del self.fields['iptable']

    def save(self, commit=True):
        instance = super().save(commit=commit)
        if settings.IS_IPTABLE:
            from iptable.models import Iptable
            iptable = self.cleaned_data['iptable']
            self._iptable = Iptable.pull(settings.CONTENT_TYPE('course', 'course'),
                                         content=instance.id)
            self._iptable.type = iptable
            self._iptable.save(update_fields=['type'])
            self._iptable.push()
        return instance


class MaterialForm(forms.ModelForm):
    class Meta:
        model = Material
        fields = ['name', 'type',
                  # 'video_type',
                  'image', 'data', 'document', 'question']
        labels = {
            'data': 'Url',
            'question': 'Test',
        }

        help_texts = {
            'data': 'กรุณาระบุ Url ของเว็บ เช่น https://www.google.co.th',
            'document': 'อัพโหลด Document เฉพาะไฟล์นามสกุล .pdf',
            'question': 'โปรดเลือก Test',
            'image': settings.HELP_TEXT_IMAGE('course'),
            'tutor': 'กด Ctrl ค้าง และคลิกที่ชื่อเพื่อเลือกผู้บรรยายหลายคน'
        }

    def __init__(self, *args, **kwargs):
        super(MaterialForm, self).__init__(*args, **kwargs)
        if not settings.COURSE__MATERIAL_IS_SCORM:
            _new = []
            for _ in self.fields['type'].choices:
                if _[0] == 5:
                    # print('delete')
                    # self.fields['type'].choices.remove(_)
                    pass
                else:
                    _new.append(_)
            self.fields['type'].choices = _new


class MaterialEditForm(forms.ModelForm):
    class Meta:
        model = Material
        fields = ['name', 'type', 'video_type', 'image', 'data', 'document', 'question']
        labels = {
            'data': 'Url',
            'question': 'Test',
        }

        help_texts = {'name': 'Limit at 60 characters',
                      'image': settings.HELP_TEXT_IMAGE('course'),
                      'tutor': 'กด Ctrl ค้าง และคลิกที่ชื่อเพื่อเลือกผู้บรรยายหลายคน',
                      'document': 'Limit at 250 characters'
                      }

    def __init__(self, *args, **kwargs):
        super(MaterialEditForm, self).__init__(*args, **kwargs)
        self.fields['type'].disabled = True

    def clean_type(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.type
        else:
            return self.cleaned_data['type']
