from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect, render

from content.models import Location as ContentLocation
from price.models import Price
from utils.crop import crop_image
from .forms import InformationForm
from .log import log_create
from .views import pull_breadcrumb
from ..cached import cached_course_delete_all


def create_view(request):
    if not request.user.has_perm('course.add_course', group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    if request.method == 'POST':
        if request.user.has_perm('course.view_course', group=request.DASHBOARD_GROUP):
            information_form = InformationForm(None, request.POST, request.FILES)
        elif request.user.has_perm('course.view_own_course', group=request.DASHBOARD_GROUP):
            information_form = InformationForm(request, request.POST, request.FILES)
        else:
            raise PermissionDenied

        if information_form.is_valid():
            course = information_form.save()
            crop_image(course, request)
            Price.init(settings.CONTENT_TYPE('course', 'course'), course.id)
            ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                       course.id)
            if settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                request.DASHBOARD_SUB_PROVIDER.push_item(settings.CONTENT_TYPE('course', 'course'), course.id)
            log_create(request, course)
            cached_course_delete_all(request, course)
            return redirect('dashboard:course:detail', course.id)
    else:
        if request.user.has_perm('course.view_course', group=request.DASHBOARD_GROUP):
            information_form = InformationForm(None)
        elif request.user.has_perm('course.view_own_course', group=request.DASHBOARD_GROUP):
            information_form = InformationForm(request)
        else:
            raise PermissionDenied

    return render(request,
                  'course/dashboard/create.html',
                  {'SIDEBAR': 'course',
                   'TAB': 'information',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'information_form': information_form})
