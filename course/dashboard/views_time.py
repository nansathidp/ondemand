import datetime

from django.shortcuts import render

from .views import check_access, pull_breadcrumb
from ..cached import cached_course_delete


@check_access
def time_view(request, course_id):
    course = request.course
    
    if request.method == 'POST':
        try:
            if request.POST['credit'] == 'unlimited':
                course.credit = datetime.timedelta(0)
            else:
                credit_day = int(request.POST.get('credit-day'))
                credit_hour = int(request.POST.get('credit-hour'))
                credit_minute = int(request.POST.get('credit-minute'))
                credit_second = 0 #int(request.POST.get('credit-second'))
                course.credit = datetime.timedelta(days=credit_day, hours=credit_hour, minutes=credit_minute, seconds=credit_second)

            if request.POST['expired'] == 'unlimited':
                course.expired = datetime.timedelta(0)
            else:
                expired_day = int(request.POST.get('expired-day'))
                expired_hour = int(request.POST.get('expired-hour'))
                expired_minute = int(request.POST.get('expired-minute'))
                expired_second = 0 #int(request.POST.get('expired-second'))
                course.expired = datetime.timedelta(days=expired_day, hours=expired_hour, minutes=expired_minute, seconds=expired_second)
            course.save(update_fields=['credit', 'expired'])
            cached_course_delete(course.id)
        except:
            #raise
            pass
        
    return render(request,
                  'course/dashboard/time.html',
                  {'SIDEBAR': 'course',
                   'TAB': 'time',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'course': course})
