import copy

from django.shortcuts import get_object_or_404, redirect, render
from django.views.decorators.csrf import csrf_exempt

from course.models import Course, Material, Outline
from stream.models import Config, Stream

from ..cached import cached_course_material_update
from .forms import MaterialEditForm
from .views import check_change
from .log import log_material_edit


@csrf_exempt
@check_change
def material_detail_view(request, course_id):
    course = request.course
    if request.method == 'POST':
        material = get_object_or_404(Material,
                                     course=course,
                                     id=request.POST.get('material'))
        material_old = copy.copy(material)
        
        action = request.POST.get('action')
        if action == 'save':
            outline = get_object_or_404(Outline,
                                        course=course,
                                        id=request.POST.get('outline'))

            old_data = material.data
            material_form = MaterialEditForm(request.POST, request.FILES, instance=material)
            if material_form.is_valid():
                material = material_form.save()
                log_material_edit(request, material_old, material)
                if material.type == 0:
                    if material.video_type == -1:
                        material.video_type = 2
                        material.save(update_fields=['video_type'])
                    if material.data != old_data:
                        material.stream_set.update(media=material.data)
                        if material.stream_set.count() == 0:
                            config = Config.objects.filter(is_default=True).first()
                            Stream.objects.create(config=config,
                                                  material=material,
                                                  media=material.data)
                cached_course_material_update(material)
            return redirect('dashboard:course:outline', course.id)
        else:
            material_form = MaterialEditForm(instance=material)
    else:
        material_form = MaterialEditForm()
    return render(request,
                  'course/dashboard/material_edit.html',
                  {'course': course,
                   'material': material,
                   'material_form': material_form})
