from django.conf import settings
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from dashboard.views import paginator
from dependency.models import Dependency
from dependency.models import Parent as DependencyParent
from provider.models import Provider
from .views import check_access


@csrf_exempt
@check_access
def dependency_search_view(request, course_id):
    course = request.course
    q_name = None
    type = None
    content_type = settings.CONTENT_TYPE('course', 'course')
    dependency = Dependency.pull(content_type, course.id)
    parent_list = Dependency.pull_parent(content_type, course.id)
    provider_list = Provider.pull_list()
    if request.method == 'POST':

        type = request.POST.get('type')
        q_name = request.POST.get('q_name', '')
        content_id = request.POST.get('content_id', '')
        content_type, content_list = Dependency.get_content_list(type)
        if content_list is None:
            content_list = []
        else:
            if q_name:
                content_list = content_list.filter(name__contains=q_name)
            content_list = paginator(request, content_list)

    else:
        content_list = []
    return render(request,
                  'course/dashboard/dependency_search.html',
                  {'content_list': content_list,
                   'condition_choices': DependencyParent.CONDITION_CHOICES,
                   'course': course,
                   'q_name': q_name,
                   'type': type,
                   'parent_list': parent_list,
                   'content_type': content_type})
