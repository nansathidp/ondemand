from django.conf.urls import url

from .views import home_view
from .views_access import access_view
from .views_alert import alert_view
from .views_analytic import analytic_view
from .views_create import create_view
from .views_delete import delete_view
from .views_dependency import dependency_view
from .views_dependency_delete import dependency_delete_view
from .views_dependency_search import dependency_search_view
from .views_dependency_search_add import dependency_search_add_view
from .views_dependency_search_delete import dependency_search_delete_view
from .views_detail import detail_view
from .views_level import level_view
from .views_level_create import level_create_view
from .views_level_delete import level_delete_view
from .views_level_edit import level_edit_view
from .views_material_add import material_add_view
from .views_material_delete import material_delete_view
from .views_material_dependency import material_dependency_view
from .views_material_dependency_add import material_dependency_add_view
from .views_material_dependency_delete import material_dependency_delete_view
from .views_material_detail import material_detail_view
from .views_material_display import material_display_view
from .views_material_resumable import material_resumable_view
from .views_material_sort import material_sort_view
from .views_material_verify import material_verify_view
from .views_material_video import material_video_view
from .views_outline import outline_view
from .views_outline_delete import outline_delete_view
from .views_outline_manage import outline_manage_view
from .views_outline_sort import outline_sort_view
from .views_price import price_view
from .views_price_edit import price_edit_view
from .views_price_use import price_use_view
from .views_progress import progress_view
from .views_term import term_view
from .views_time import time_view
from .views_update import update_view

app_name = 'course'
urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^create/$', create_view, name='create'),
    url(r'^(\d+)/$', detail_view, name='detail'),
    url(r'^(\d+)/delete/$', delete_view, name='delete'),

    # Curriculum (Outline)
    url(r'^(\d+)/curriculum/$', outline_view, name='outline'),  # TODO: testing
    url(r'^(\d+)/curriculum/outline/manage/$', outline_manage_view, name='outline_manage'),  # TODO: testing
    url(r'^(\d+)/curriculum/outline/sort/$', outline_sort_view, name='outline_sort'),  # TODO: testing
    url(r'^(\d+)/curriculum/outline/delete/$', outline_delete_view, name='outline_delete'),  # TODO: testing

    url(r'^(\d+)/curriculum/material/add/$', material_add_view, name='material_add'),  # TODO: testing
    url(r'^(\d+)/curriculum/material/detail/$', material_detail_view, name='material_detail'),  # TODO: testing
    url(r'^(\d+)/curriculum/material/sort/$', material_sort_view, name='material_sort'),  # TODO: testing
    url(r'^(\d+)/curriculum/material/display/$', material_display_view, name='material_display'),  # TODO: testing
    url(r'^(\d+)/curriculum/material/resumable/$', material_resumable_view, name='material_resumable'),

    url(r'^(\d+)/curriculum/material/video/$', material_video_view, name='material_video'),
    url(r'^(\d+)/curriculum/material/verify/$', material_verify_view, name='material_verify'),
    url(r'^(\d+)/curriculum/material/delete/$', material_delete_view, name='material_delete'),

    url(r'^(\d+)/material/dependency/$', material_dependency_view, name='material_dependency'),
    url(r'^(\d+)/material/dependency/add/$', material_dependency_add_view, name='material_dependency_add'),
    url(r'^(\d+)/material/dependency/delete/$', material_dependency_delete_view, name='material_dependency_delete'),

    # Price
    url(r'^(\d+)/price/$', price_view, name='price'),  # TODO: testing
    url(r'^(\d+)/price/edit/$', price_edit_view, name='price_edit'),  # TODO: testing
    url(r'^(\d+)/price/use/$', price_use_view, name='price_use'),  # TODO: testing

    # Period & Credit
    url(r'^(\d+)/time/$', time_view, name='time'),  # TODO: testing

    # Dependency
    url(r'^(\d+)/dependency/$', dependency_view, name='dependency'),
    url(r'^(\d+)/dependency/search/$', dependency_search_view, name='dependency_search'),
    url(r'^(\d+)/dependency/search/add/$', dependency_search_add_view, name='dependency_search_add'),
    url(r'^(\d+)/dependency/search/delete/$', dependency_search_delete_view, name='dependency_search_delete'),
    url(r'^(\d+)/dependency/delete/$', dependency_delete_view, name='dependency_delete'),

    # Progress
    url(r'^(\d+)/progress/$', progress_view, name='progress'),  # TODO: testing
    # Analytic
    url(r'^(\d+)/analytic/$', analytic_view, name='analytic'),  # TODO: testing

    url(r'^level/$', level_view, name='level'),  # TODO: testing
    url(r'^level/create/$', level_create_view, name='level_create'),  # TODO: testing
    url(r'^level/(\d+)/edit/$', level_edit_view, name='level_edit'),  # TODO: testing
    url(r'^level/(\d+)/delete/$', level_delete_view, name='level_delete'),  # TODO: testing

    url(r'^(\d+)/alert/$', alert_view, name='alert'),  # TODO: testing
    url(r'^(\d+)/update/$', update_view, name='update'),  # TODO: testing

    url(r'^(\d+)/access/$', access_view, name='access'),  # TODO: testing
    url(r'^(\d+)/term/$', term_view, name='term'),  # TODO: testing
]
