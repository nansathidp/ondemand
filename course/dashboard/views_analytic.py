import datetime

from django.shortcuts import render
from django.utils import timezone

from analytic.models import Log, Refer

from .views import check_access, pull_breadcrumb


@check_access
def analytic_view(request, course_id):
    course = request.course

    now = timezone.now()

    start = now.date() - datetime.timedelta(days=30)
    end = now.date()
    code_view = 'course_%s' % course.id
    code_cart = 'course_%s_cart' % course.id
    code_buy = 'course_%s_buy' % course.id
    code_libary = 'course_%s_libary' % course.id

    if True:
        chart_result = Log.pull([code_view, code_libary], start, end)
        chart_view_list = chart_result[code_view]
        chart_libary_list = chart_result[code_libary]
        chart_cart_list = []
        chart_buy_list = []
    else:
        chart_result = Log.pull([code_view, code_cart, code_buy], start, end)
        chart_view_list = chart_result[code_view]
        chart_cart_list = chart_result[code_cart]
        chart_buy_list = chart_result[code_buy]
        chart_libary_list = []
    
    refer_list = Refer.pull(code_view, start, end)

    return render(request,
                  'course/dashboard/analytic.html',
                  {'SIDEBAR': 'course',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'course': course,
                   'is_free': True,
                   'chart_view_list': chart_view_list,
                   'chart_cart_list': chart_cart_list,
                   'chart_buy_list': chart_buy_list,
                   'chart_libary_list': chart_libary_list,
                   'refer_list': refer_list})
