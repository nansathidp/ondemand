import copy

from django.conf import settings
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt

from course.models import Course, Material, Outline
from dependency.models import Dependency

from course.jobs import _material_delete
from .views import check_change
from .log import log_material_delete

import django_rq

@csrf_exempt
@check_change
def material_delete_view(request, course_id):
    course = request.course

    outline = None
    if request.method == 'POST':
        outline = get_object_or_404(Outline,
                                    course=course,
                                    id=request.POST.get('outline'))
        material = get_object_or_404(Material,
                                     outline=outline,
                                     id=request.POST.get('material'))
        Dependency.delete_material(material.id)
        material_old = copy.copy(material)
        django_rq.enqueue(_material_delete, course.id, material.type,  material.data)
        material.delete()
        log_material_delete(request, material_old)
        course.count_material(material.type)
        course.progress_update()
    return render(request,
                  'course/dashboard/material_block.html',
                  {'outline': outline})
