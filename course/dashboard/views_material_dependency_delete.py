import copy

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt

from dependency.models import Parent as DependencyParent
from dependency.models import Dependency

from ..models import Course, Material, Outline
from .views import check_access
from .log import log_material_dependency_delete

@csrf_exempt
@check_access
def material_dependency_delete_view(request, course_id):
    course = request.course

    outline = None
    if request.method == 'POST':
        try:
            outline_id = int(request.POST.get('outline'))
            material_id = int(request.POST.get('material'))
            parent_id = int(request.POST.get('parent'))

            outline = Outline.objects.get(course=course,
                                          id=outline_id)
            material = Material.objects.get(course=course,
                                            outline_id=outline_id,
                                            id=material_id)
            dependency = Dependency.pull(settings.CONTENT_TYPE('course', 'material'),
                                         material.id)
            parent = dependency.delete_parent(parent_id)
            if parent is not None:
                log_material_dependency_delete(request,
                                               material,
                                               parent)
        except:
            pass
    
    return render(request,
                  'course/dashboard/material_block.html',
                  {'outline': outline})
