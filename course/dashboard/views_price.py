from django.conf import settings
from django.shortcuts import render

from price.models import Price
from purchase.models import PriceMatrix

from .views import check_access, pull_breadcrumb


@check_access
def price_view(request, course_id):
    course = request.course

    price_list = Price.objects.filter(content_type=settings.CONTENT_TYPE('course', 'course'),
                                      content=course.id)
    price_matrix_list = PriceMatrix.objects.all()

    return render(request,
                  'course/dashboard/price.html',
                  {'SIDEBAR': 'course',
                   'TAB': 'price',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'course': course,
                   'price_list': price_list,
                   'price_matrix_list': price_matrix_list})
