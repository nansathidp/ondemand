from django.shortcuts import get_object_or_404, render, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from course.models import Material, Upload
from question.dashboard.views_section_detail import check_ip
from .views import check_change


@csrf_exempt
@check_change
def material_video_view(request, course_id):
    course = request.course
    if request.method == 'POST' and check_ip(request) is True:
        material = get_object_or_404(Material, course=course, id=request.POST.get('material'))
        material.video_status = 0
        material.video_is_verify = False
        material.save(update_fields=['video_status', 'video_is_verify'])
        filename = request.POST.get('filename')
        upload, path = Upload.pull(filename, request.user, material)
        return render(request,
                      'course/dashboard/outline_block.html',
                      {'outline_list': course.outline_set.all()})
    else:
        return HttpResponse(status=403, content='Can not upload')


