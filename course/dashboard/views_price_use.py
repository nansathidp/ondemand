from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt

from price.models import Price

from ..models import Course


@csrf_exempt
def price_use_view(request, course_id):
    if not request.user.has_perm('course.view_course') and not request.user.has_perm('course.view_own_course'):
        raise PermissionDenied
    
    course = get_object_or_404(Course, id=course_id)
    content_type = settings.CONTENT_TYPE('course', 'course')
    
    if request.method == 'POST':
        price = get_object_or_404(Price,
                                  id=request.POST.get('id'),
                                  content_type=content_type,
                                  content=course.id)
        price.is_use = not price.is_use
        price.save(update_fields=['is_use'])
        
    price_list = Price.objects.filter(content_type=content_type,
                                      content=course.id)
    return render(request,
                  'course/dashboard/price_block.html',
                  {'price_list': price_list})
