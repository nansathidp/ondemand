import copy

from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt

from course.cached import cached_course_delete
from course.models import Course, Outline
from .views import check_change
from .log import log_outline_sort

@csrf_exempt
@check_change
def outline_sort_view(request, course_id):
    course = request.course

    if request.method == 'POST':
        outline = get_object_or_404(Outline,
                                    course=course,
                                    id=request.POST.get('outline'))
        outline_old = copy.copy(outline)
        action = request.POST.get('action')
        if action == 'up':
            outlint_swap = Outline.objects.filter(course=course,
                                                  sort__lt=outline.sort) \
                                          .exclude(id=outline.id) \
                                          .order_by('-sort') \
                                          .first()
            if outlint_swap is not None:
                outlint_swap.sort = outline.sort
                outlint_swap.save(update_fields=['sort'])
            outline.sort -= 1
            outline.save(update_fields=['sort'])
            log_outline_sort(request, outline_old, outline)
        elif action == 'down':
            outlint_swap = Outline.objects.filter(course=course,
                                                  sort__gt=outline.sort) \
                                          .exclude(id=outline.id) \
                                          .order_by('sort') \
                                          .first()
            if outlint_swap is not None:
                outlint_swap.sort = outline.sort
                outlint_swap.save(update_fields=['sort'])
            outline.sort += 1
            outline.save(update_fields=['sort'])
            log_outline_sort(request, outline_old, outline)
        cached_course_delete(course.id)
    return render(request,
                  'course/dashboard/outline_block.html',
                  {'outline_list': course.outline_set.all()})
