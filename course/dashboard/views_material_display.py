import copy

from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt

from course.models import Material, Outline

from .views import check_change
from .log import log_material_display


@csrf_exempt
@check_change
def material_display_view(request, course_id):
    course = request.course

    outline = None
    if request.method == 'POST':
        outline = get_object_or_404(Outline, course=course, id=request.POST.get('outline'))
        material = get_object_or_404(Material, outline=outline, id=request.POST.get('material'))
        material_old = copy.copy(material)
        material.is_display = not material.is_display
        material.save(update_fields=['is_display'])
        log_material_display(request, material_old, material)
        course.count_material(material.type)
        course.progress_update()
        if material.type in [0, 4]:
            course.update_duration()
    return render(request,
                  'course/dashboard/material_block.html',
                  {'outline': outline})
