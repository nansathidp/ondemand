from django.shortcuts import redirect, get_object_or_404

from course.models import Course

from course.jobs import _delete_course
import django_rq


def delete_view(request, course_id):
    course = get_object_or_404(Course, id=course_id)
    course.delete()
    django_rq.enqueue(_delete_course, course.id)
    return redirect('dashboard:course:home')
