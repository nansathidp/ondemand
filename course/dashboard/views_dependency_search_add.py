from django.conf import settings
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from dependency.models import Dependency
from utils.content import get_content
from .views import check_change


@csrf_exempt
@check_change
def dependency_search_add_view(request, course_id):
    course = request.course
    error = None
    content_type = settings.CONTENT_TYPE('course', 'course')
    dependency = Dependency.pull(content_type, course.id)
    if request.method == 'POST':
        try:
            content_type_id = int(request.POST.get('content_type'))
            content_id = int(request.POST.get('content'))
            condition = int(request.POST.get('condition'))

            content = get_content(content_type_id, content_id)
            if condition in [1, 2] and content is not None:
                is_success, error = dependency.add_parent(settings.CONTENT_TYPE_ID(content_type_id),
                                                          content.id,
                                                          condition)
            else:
                error = 'Condition Not found.'
        except:
            error = None

    parent_list = Dependency.pull_parent(content_type, course.id)
    return render(request,
                  'course/dashboard/dependency_block.html',
                  {'course': course,
                   'error': error,
                   'parent_list': parent_list})
