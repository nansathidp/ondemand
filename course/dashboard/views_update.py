from django.conf import settings
from django.shortcuts import redirect

from progress.models import Progress

from .views import check_access


@check_access
def update_view(request, course_id):
    course = request.course

    for progress in Progress.objects.filter(is_update=False,
                                            content_type=settings.CONTENT_TYPE('course', 'course'),
                                            content=course.id):
        progress.content_update()
    
    ref = request.META.get('HTTP_REFERER', None)
    if ref is not None:
        return redirect(ref)
    return redirect('dashboard:course:outline', course.id)
