import inspect
from functools import wraps

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render

from analytic.cached import cached_analytic_stat
from dashboard.views import condition_view
from utils.paginator import paginator
from ..models import Course


def check_access(view_func):
    def _decorator(request, *args, **kwargs):
        course_id = args[0]
        request.course = get_object_or_404(Course, id=course_id)
        if not request.course.check_access(request):
            raise PermissionDenied
        response = view_func(request, *args, **kwargs)
        return response

    return wraps(view_func)(_decorator)


def check_ip(view_func):
    def _decorator(request, *args, **kwargs):
        from django.conf import settings
        from api.views_ais_privilege import get_client_ip
        import ipaddress
        is_upload = False
        ip = get_client_ip(request)
        for allow in settings.IPTABLE_UPLOAD_ALLOW:
            if ipaddress.ip_address(ip) in ipaddress.ip_network(allow):
                is_upload = True
        if is_upload is False:
            raise PermissionDenied
        response = view_func(request, *args, **kwargs)
        return response

    return wraps(view_func)(_decorator)


def check_change(view_func):
    def _decorator(request, *args, **kwargs):
        course_id = args[0]
        request.course = get_object_or_404(Course, id=course_id)
        if not request.course.check_change(request):
            raise PermissionDenied
        response = view_func(request, *args, **kwargs)
        return response

    return wraps(view_func)(_decorator)


def home_view(request):
    course_list = Course.access_list(request)
    if course_list is None:
        raise PermissionDenied
    elif course_list == []:
        return condition_view(request)

    course_list = paginator(request, course_list)
    for course in course_list:
        course.is_change = course.check_change(request)
        course.analytic_view = cached_analytic_stat('course_%s' % course.id)['stat_now']
        course.analytic_libary = cached_analytic_stat('course_%s_library' % course.id)['stat_now']
        course.analytic_assign = cached_analytic_stat('course_%s_assign' % course.id)['stat_now']

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Course Management'},
    ]
    return render(request,
                  'course/dashboard/home.html',
                  {'SIDEBAR': 'course',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'course_list': course_list,
                   'content_type': settings.CONTENT_TYPE('course', 'course'),
                   'q_name': request.q_name,
                   'q_provider': request.q_provider,
                   'q_provider_list': request.q_provider_list,
                   'q_category': request.q_category,
                   'q_category_list': request.q_category_list})


def pull_breadcrumb(course=None):
    breadcrumb_list = [{'is_active': False,
                        'title': 'Course Management',
                        'url': reverse('dashboard:course:home')}]
    caller = inspect.stack()[1][3]
    if caller == 'detail_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Information'})
    elif caller == 'create_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Create'})
    elif caller == 'outline_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Curriculum'})
    elif caller == 'dependency_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Dependency'})
    elif caller == 'term_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Terms & Conditions'})
    elif caller == 'access_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Accessibility'})
    elif caller == 'time_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Period & Credit'})
    elif caller == 'price_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Price'})
    elif caller == 'analytic_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Statistics'})
    elif caller == 'progress_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Progress'})
    return breadcrumb_list
