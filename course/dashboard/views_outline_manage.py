import copy

from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt

from course.cached import cached_course_delete
from course.models import Course, Outline

from .views import check_change
from .log import log_outline_add, log_outline_edit

@csrf_exempt
@check_change
def outline_manage_view(request, course_id):
    course = request.course

    if request.method == 'POST':
        action = request.POST.get('action')
        if action == 'add':
            name = request.POST.get('name')
            outline = Outline.objects.create(course=course,
                                             section=name,
                                             sort=course.outline_set.count()+1)
            log_outline_add(request, outline)
            cached_course_delete(course.id)
        elif action == 'edit':
            name = request.POST.get('name')
            outline = get_object_or_404(Outline, course=course, id=request.POST.get('id'))
            outline_old = copy.copy(outline)
            outline.section = name
            outline.save(update_fields=['section'])
            log_outline_edit(request, outline_old, outline)
            cached_course_delete(course.id)
    return render(request,
                  'course/dashboard/outline_block.html',
                  {
                      'outline_list': course.outline_set.all()
                  })
