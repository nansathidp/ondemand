from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect, render

#from ..models import CourseLevel
#from .forms import CourseLevelForm


def level_edit_view(request, course_level_id):
    if not request.user.has_perm('course.change_courselevel',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    course_level = get_object_or_404(CourseLevel, id=course_level_id)

    if request.method == 'POST':
        course_level_form = CourseLevelForm(request.POST, instance=course_level)
        if course_level_form.is_valid():
            course_level = course_level_form.save()
            return redirect('dashboard:course:level')
    else:
        course_level_form = CourseLevelForm(instance=course_level)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Content Level Management',
         'url': reverse('dashboard:course:level')},
        {'is_active': True,
         'title': 'Edit'}
    ]
    return render(request,
                  'course/dashboard/level_create.html',
                  {'SIDEBAR': 'level_content',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'course_level_form': course_level_form})
