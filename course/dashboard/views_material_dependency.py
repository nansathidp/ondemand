from django.conf import settings
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from dependency.models import Dependency
from dependency.models import Parent as DependencyParent
from .views import check_access
from ..models import Material


@csrf_exempt
@check_access
def material_dependency_view(request, course_id):
    course = request.course

    material_list = course.material_set.filter(is_display=True).order_by('outline__sort', 'sort')
    if request.method == 'POST':
        try:
            material_id = int(request.POST.get('material', -1))
            material = Material.objects.get(course=course, id=material_id)
            material_list = material_list.exclude(id=material.id)
            dependency = Dependency.pull(settings.CONTENT_TYPE('course', 'material'),
                                         material.id)
            is_first = not dependency.parent_set.exists()
        except:
            material = None
            is_first = True
    else:
        material = None
        is_first = True

    for material in material_list:
        material.get_content_cached()

    return render(request,
                  'course/dashboard/material_dependency.html',
                  {'material_list': material_list,
                   'condition_choices': DependencyParent.CONDITION_CHOICES,
                   'material': material,
                   'is_first': is_first})
