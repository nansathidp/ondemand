import datetime

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt

from price.models import Price
from purchase.models import Apple, PriceMatrix

from ..cached import cached_course_delete
from ..models import Course
from .views import check_change

@csrf_exempt
@check_change
def price_edit_view(request, course_id):
    course = request.course

    content_type = settings.CONTENT_TYPE('course', 'course')
    
    if request.method == 'POST':
        price = get_object_or_404(Price, content_type=content_type, id=request.POST.get('id'))
        action = request.POST.get('action')
        if action == 'load':
            s = int(price.credit.total_seconds())
            h, r = divmod(s, 3600)
            m, s = divmod(r, 60)
            if price.store == 3:
                ios_price = price.ios.price_tier_id
                if price.ios.discount_tier is None:
                    ios_discount = -1
                else:
                    ios_discount = price.ios.discount_tier_id
            else:
                ios_price = -1
                ios_discount = -1
            return JsonResponse({'price': price.price,
                                 'discount': price.discount,
                                 'ios_price': ios_price,
                                 'ios_discount': ios_discount,
                                 'credit': '%.2d:%.2d:%.2d'%(h, m, s),
                                 'store': price.store})
        elif action == 'save':
            try:
                if price.store == 3:
                    try:
                        discount_tier = PriceMatrix.objects.get(id=request.POST.get('ios_discount'))
                    except:
                        discount_tier = None
                    apple = Apple.push(course.name,
                                       content_type, course.id,
                                       get_object_or_404(PriceMatrix, id=request.POST.get('ios_price')),
                                       discount_tier)
                    price.ios = apple
                elif price.store == 4:
                    price.price = float(request.POST.get('price'))
                    price.discount = float(request.POST.get('discount'))

                    price_ios = Price.pull(content_type, course.id, 3)
                    price_ios.price = float(request.POST.get('price'))
                    price_ios.discount = float(request.POST.get('discount'))
                    price_ios.save(update_fields=['price', 'discount'])
                else:
                    price.price = float(request.POST.get('price'))
                    price.discount = float(request.POST.get('discount'))
                if price.store == 6:
                    s = request.POST.get('credit').split(':')
                    price.credit = datetime.timedelta(hours=int(s[0]), minutes=int(s[1]), seconds=int(s[2]))
                price.save(update_fields=['price', 'discount', 'ios', 'credit'])
                cached_course_delete(course.id)
            except:
                raise
                pass
    price_list = Price.objects.filter(content_type=content_type,
                                      content=course.id)
    return render(request,
                  'course/dashboard/price_block.html',
                  {'price_list': price_list})
