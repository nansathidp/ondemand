from django.conf import settings
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from dependency.models import Parent as DependencyParent
from dependency.models import Dependency

from ..models import Course, Material
from .views import check_access
from .log import log_material_dependency_add


@csrf_exempt
@check_access
def material_dependency_add_view(request, course_id):
    course = request.course

    outline = None
    if request.method == 'POST':
        try:
            material_id = int(request.POST.get('material', -1))
            material = Material.objects.get(course=course,
                                            id=material_id)
            outline = material.outline
            dependency = Dependency.pull(settings.CONTENT_TYPE('course', 'material'),
                                         material.id)

            material_add_id = int(request.POST.get('material_add', -1))
            material_add = Material.objects.get(course=course,
                                                id=material_add_id)
            condition = int(request.POST.get('condition', -1))
            if condition in [1, 2]:
                parent = dependency.add_parent(settings.CONTENT_TYPE('course', 'material'),
                                               material_add.id,
                                               condition)
                if parent is not False:
                    log_material_dependency_add(request, material, parent)
        except:
            pass
    
    return render(request,
                  'course/dashboard/material_block.html',
                  {'outline': outline})
