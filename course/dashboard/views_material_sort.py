import copy

from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt

from course.cached import cached_course_delete
from course.models import Material, Outline
from .log import log_material_sort
from .views import check_change


@csrf_exempt
@check_change
def material_sort_view(request, course_id):
    course = request.course

    outline = None
    if request.method == 'POST':
        outline = get_object_or_404(Outline,
                                    course=course,
                                    id=request.POST.get('outline'))
        material = get_object_or_404(Material,
                                     outline=outline,
                                     id=request.POST.get('material'))
        material_old = copy.copy(material)
        action = request.POST.get('action')
        if action == 'up':
            material_swap = Material.objects.filter(outline=outline,
                                                    sort__lt=material.sort) \
                                            .exclude(id=material.id) \
                                            .order_by('-sort') \
                                            .first()
            if material_swap is not None:
                material_swap.sort = material.sort
                material_swap.is_cached_mark = False
                material_swap.save(update_fields=['sort'])
            material.sort -= 1
            material.is_cached_mark = False
            material.save(update_fields=['sort'])
            log_material_sort(request, material_old, material)
        elif action == 'down':
            material_swap = Material.objects.filter(outline=outline,
                                                    sort__gt=material.sort) \
                                            .exclude(id=material.id) \
                                            .order_by('sort') \
                                            .first()
            if material_swap is not None:
                material_swap.sort = material.sort
                material_swap.is_cached_mark = False
                material_swap.save(update_fields=['sort'])
            material.sort += 1
            material.is_cached_mark = False
            material.save(update_fields=['sort'])
            log_material_sort(request, material_old, material)
    cached_course_delete(course.id)
    return render(request,
                  'course/dashboard/material_block.html',
                  {'outline': outline})
