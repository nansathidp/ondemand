from django.shortcuts import get_object_or_404, redirect, render
from django.views.decorators.csrf import csrf_exempt

from course.models import Course, Material, Outline

from .forms import MaterialForm
from .views import check_change
from .log import log_material_add


@csrf_exempt
@check_change
def material_add_view(request, course_id):
    course = request.course
    outline = None
    
    if request.method == 'POST':
        outline = get_object_or_404(Outline,
                                    course=course,
                                    id=request.POST.get('outline'))

        action = request.POST.get('action')
        if action == 'add':
            material_last = Material.objects.filter(outline=outline).order_by('-sort').first()
            if material_last is None:
                sort_last = 0
            else:
                sort_last = material_last.sort + 1
            material_form = MaterialForm(request.POST,
                                         request.FILES,
                                         instance=Material(course=course,
                                                           outline=outline,
                                                           sort=sort_last))
            if material_form.is_valid():
                material = material_form.save()
                log_material_add(request, material)

                #course.count_material(material.type)
                #course.progress_update() #is_diaplay = False
            # else:
            #     print(material_form.errors)
            return redirect('dashboard:course:outline', course.id)
            #return render(request,
            #              'course/dashboard/material_block.html',
            #              {'outline': outline})
        else:
            material_form = MaterialForm()

    else:
        material_form = MaterialForm()
    return render(request,
                  'course/dashboard/material_create.html',
                  {'course': course,
                   'outline': outline,
                   'material_form': material_form})



