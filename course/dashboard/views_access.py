from django.conf import settings
from django.shortcuts import render

from access.cached import cached_access_level, cached_access_permission
from access.models import Access, Level

from .views import check_access, pull_breadcrumb


@check_access
def access_view(request, course_id):
    course = request.course

    level_list = cached_access_level()
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_permission = cached_access_permission(course_id, content_type_course.id)

    if request.method == 'POST':
        level_id = request.POST.get('level_id', None)
        if level_id is None or int(level_id) == -1:
            Access.objects.filter(content_type_id=content_type_course.id, content=course_id).delete()
            content_permission = cached_access_permission(course_id, content_type_course.id, is_force=True)
        else:
            level = Level.objects.filter(id=level_id).first()
            try:
                content_permission = Access.objects.get(content_type=content_type_course, content=course_id)
                content_permission.level = level
                content_permission.save(update_fields=['level'])
            except Access.DoesNotExist:
                content_permission = Access(content_type=content_type_course,
                                            content=course_id,
                                            level=level)
                content_permission.save()
                
    return render(request,
                  'course/dashboard/access.html',
                  {'SIDEBAR': 'course',
                   'course': course,
                   'TAB': 'access',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'level_list': level_list,
                   'content_permission': content_permission})
