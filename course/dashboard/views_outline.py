from django.shortcuts import render

from utils.paginator import paginator
from question.dashboard.views_section_detail import check_ip
from ..models import Course, Upload

from .views import check_access, pull_breadcrumb
from ..models import Upload


@check_access
def outline_view(request, course_id):
    course = request.course
    is_upload = check_ip(request)
    outline_list = course.outline_set.all()
    upload_list = Upload.objects.filter(material__course_id=course.id, status__in=[1, 2, 3])

    return render(request,
                  'course/dashboard/outline.html',
                  {'SIDEBAR': 'course',
                   'TAB': 'outline',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'course': course,
                   'outline_list': outline_list,
                   'upload_list': upload_list,
                   'is_upload': is_upload})
