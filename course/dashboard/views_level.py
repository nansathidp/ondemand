from django.core.exceptions import PermissionDenied
from django.shortcuts import render

#from ..models import CourseLevel


def level_view(request):
    if not request.user.has_perm('course.view_courselevel',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    course_level_list = CourseLevel.objects.all()
    
    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Content Level Management'}
    ]
    return render(request,
                  'course/dashboard/level.html',
                  {'SIDEBAR': 'level_content',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'course_level_list': course_level_list})
