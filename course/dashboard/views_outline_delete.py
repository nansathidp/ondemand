import copy

from django.conf import settings
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt

from course.models import Course, Outline

from course.jobs import _material_delete
from .views import check_change
from .log import log_outline_delete

import django_rq

@csrf_exempt
@check_change
def outline_delete_view(request, course_id):
    course = request.course

    if request.method == 'POST':
        outline = get_object_or_404(Outline,
                                    course=course,
                                    id=request.POST.get('outline'))
        outline_old = copy.copy(outline)
        if outline is not None:
            for material in outline.material_set.all():
                django_rq.enqueue(_material_delete, course.id, material.type, material.data)
                material.delete()
            outline.delete()
            log_outline_delete(request, outline_old)
    return render(request,
                  'course/dashboard/outline_block.html',
                  {'outline_list': course.outline_set.all()})
