from django.conf import settings
from django.shortcuts import render

from term.models import Term

from term.cached import cached_term_content, cached_term_content_delete
from .views import check_access, pull_breadcrumb


@check_access
def term_view(request, course_id):
    course = request.course

    content_type_course = settings.CONTENT_TYPE('course', 'course')
    term_content = cached_term_content(course_id, content_type_course.id)

    if request.method == 'POST':
        term_info = request.POST.get('term-info', None)
        if term_info is None or not term_info:
            Term.objects.filter(content_type_id=content_type_course.id, content=course_id).delete()
            term_content = cached_term_content(course_id, content_type_course.id, is_force=True)
        else:
            try:
                term_content = Term.objects.get(content_type=content_type_course, content=course_id)
                term_content.body = term_info
                term_content.save(update_fields=['body'])
                cached_term_content_delete(course_id, content_type_course.id)
            except Term.DoesNotExist:
                term_content = Term.objects.create(content_type=content_type_course,
                                                   content=course_id,
                                                   body=term_info)
                cached_term_content_delete(course_id, content_type_course.id)
    return render(request,
                  'course/dashboard/term.html',
                  {
                      'SIDEBAR': 'course',
                      'course': course,
                      'TAB': 'term',
                      'BREADCRUMB_LIST': pull_breadcrumb(),
                      'term_content': term_content
                  })
