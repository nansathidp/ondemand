from django.conf import settings
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.template.loader import render_to_string

from progress.models import Progress

from ..models import Upload
from .views import check_access


@csrf_exempt
@check_access
def alert_view(request, course_id):
    course = request.course

    upload_list = Upload.objects.filter(material__course_id=course.id, status__in=[1, 2, 3])
    
    is_progress_update = Progress.objects.filter(is_update=False,
                                                 content_type=settings.CONTENT_TYPE('course', 'course'),
                                                 content=course.id).exists()
    if upload_list.count() > 0 or is_progress_update:
        status = 1
    else:
        status = 0
    return JsonResponse({'status': status,
                         'html': render_to_string('course/dashboard/alert.html',
                                                  {'course': course,
                                                   'upload_list': upload_list,
                                                   'is_progress_update': is_progress_update})})
