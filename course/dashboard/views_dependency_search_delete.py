from django.conf import settings
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from dependency.models import Dependency

from .views import check_change


@csrf_exempt
@check_change
def dependency_search_delete_view(request, course_id):
    course = request.course
    
    content_type = settings.CONTENT_TYPE('course', 'course')
    dependency = Dependency.pull(content_type,
                                 course.id)

    html = ''
    if request.method == 'POST':
        try:
            parent_id = int(request.POST.get('parent'))

            parent = dependency.delete_parent(parent_id)
            if parent is None:
                html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-red"></i>'
            else:
                html = parent.get_status_html()
        except:
            raise
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-red"></i>'   
        
    return JsonResponse({'html': html})
