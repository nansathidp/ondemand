from django.conf import settings
from django.shortcuts import render

from dependency.models import Dependency

from .views import check_access, pull_breadcrumb


@check_access
def dependency_view(request, course_id):
    course = request.course
    parent_list = Dependency.pull_parent(settings.CONTENT_TYPE('course', 'course'), course.id)
    return render(request,
                  'course/dashboard/dependency.html',
                  {'SIDEBAR': 'course',
                   'TAB': 'dependency',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'course': course,
                   'parent_list': parent_list})
