from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, redirect

#from ..models import CourseLevel


def level_delete_view(request, course_level_id):
    if not request.user.has_perm('course.delete_courselevel',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    course_level = get_object_or_404(CourseLevel, id=course_level_id)
    course_level.delete()
    return redirect('dashboard:course:level')
