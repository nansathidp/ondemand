from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt

from dependency.models import Dependency

from ..models import Course
from .views import check_change

@csrf_exempt
@check_change
def dependency_delete_view(request, course_id):
    course = request.course

    content_type = settings.CONTENT_TYPE('course', 'course')
    dependency = Dependency.pull(content_type,
                                 course.id)
    if request.method == 'POST':
        try:
            parent_id = int(request.POST.get('parent'))
            dependency.delete_parent(parent_id)
        except:
            pass

    parent_list = Dependency.pull_parent(content_type, course.id)
    return render(request,
                  'course/dashboard/dependency_block.html',
                  {'course': course,
                   'parent_list': parent_list})
