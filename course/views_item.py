from django.http import JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template.loader import render_to_string
from django.conf import settings

from course.models import Course
from category.models import Category


def item_view(request):
    result = {}
    code = 200
    page = request.GET.get('page', 1)
    category_id = request.GET.get('category', None)
    category = None
    q = request.GET.get('q', None)
    result['content_list'] = []
    platform_id = settings.CONFIG(request.get_host(), 'PLATFORM_ID')

    if category_id is not None:
        category = Category.pull(category_id)

    course_list = Course.objects.filter(is_display=True).order_by('-timestamp')

    if category is not None:
        course_list = course_list.filter(category=category)

    if q is not None and len(q) > 1:
        course_list = course_list.filter(name__icontains=q)

    paginator = Paginator(course_list, 12)
    try:
        pager = paginator.page(page)
    except PageNotAnInteger:
        pager = paginator.page(1)
    except EmptyPage:
        pager = paginator.page(paginator.num_pages)

    result['has_next'] = pager.has_next()
    for course in pager.object_list:
        course = Course.pull(course.id)
        if course is not None:
            result['content_list'].append(render_to_string('course/item_cached_new.html',
                                                           {
                                                               'course': course,
                                                               'PLATFORM_ID': platform_id
                                                           }))
    return JsonResponse({
        'status': code,
        'status_msg': code,
        'result': result
    })
