from django.conf import settings

from .models import Course, Material
from order.models import Item as OrderItem
from rating.models import Rating
from price.models import Price
from progress.models import Progress
from content.models import Location as ContentLocation
from program.models import Program

from app.views import update_checkout_type
from utils.api_image import api_image_url
from utils.duration import duration_display


def _api_display(course, install_version=0.0, store_version=0.0, account=None, is_video_progress=False, store=1,
                 order_item=None):
    content_type = settings.CONTENT_TYPE('course', 'course')
    is_cached = getattr(course, 'is_cached', False)
    if not is_cached:
        cached = Course.pull(course.id)
        if cached is None:
            return {}
        course = cached

    d = {
        'id': course.id,
        'name': course.name,
        'status': course.status,
        'overview': course.overview,
        'status_display': course.get_status_display(),
        'category': course.category.api_display_title(),
        'desc': course.desc,
        'info': course.info,
        'condition': course.condition,
        'access': course.access,
        'provider': course.provider.api_display_title() if course.provider else None,
        'preview_video_type': course.preview_video_type,
        'preview_video_path': course.preview_video_path,
        'image': course.image.url if bool(course.image) else None,
        'image_cover': course.image_cover.url if bool(course.image_cover) else None,
        'preview_video_image': course.preview_video_image.url if bool(course.preview_video_image) else None,
        'count_material': course.material_set.filter(is_display=True).count(),
        'rating': Rating.pull_first(content_type, course.id).api_display(),
        'term': None,
        'is_require_terms': None,
        'level': None,
        'level_display': None,
        'tutor_list': [],
        'outline_list': [],
        'level_list': [],
        'faq_list': []
    }

    price = Price.pull(content_type, course.id, store)
    price_set = price.api_display()
    d.update({
        'price_set': price_set
    })

    # Checkout Type
    update_checkout_type(d, install_version, store_version)

    # Condition
    d['duration'] = duration_display(course.duration)

    if int(course.expired.total_seconds()) == 0:
        d['expired_display'] = "Unlimited"
    else:
        d['expired_display'] = duration_display(course.expired)

    if int(course.credit.total_seconds()) == 0:
        d['credit_display'] = "Unlimited"
    else:
        d['credit_display'] = duration_display(course.credit)

    # Fix CIMB 2017-10-12
    # if course.expired.days == 0:
    #     d['expired_display'] = "No Expiry"
    # else:
    #     d['expired_display'] = "%s days" % course.expired.days
    #
    # if int(course.credit.total_seconds()) == 0:
    #     d['credit_display'] = "Unlimited"
    # else:
    #     d['credit_display'] = "%s hours" % int(course.credit.total_seconds() / 3600)

    for tutor in course.tutor.all():
        d['tutor_list'].append({
            'id': tutor.id,
            'name': tutor.name,
            'image': api_image_url(tutor.image),
            'image_featured': api_image_url(tutor.image_featured)
        })

    # Check Purchased
    if account is not None and order_item is None:
        order_item = OrderItem.pull_purchased(None, account, content_type, course.id) # FIXME : kidsdev (content_location)
        if order_item is not None:
            d['is_purchased'] = True
        else:
            d['is_purchased'] = False
    elif order_item is not None:
        d['is_purchased'] = True
    else:
        d['is_purchased'] = False

    # Args
    args = {
        'index': 0,
        'index_video': 0,
        'default_video_thumb': d['image'],
        'content_count': {}
    }

    for outline in course.outline_set.all():
        d['outline_list'].append(
            outline.api_display(account=account, args=args, is_video_list=False, order_item=order_item))

    # Course Condition
    d.update({
        # 'expired': course.expired.days,
        # 'credit': int(course.credit.total_seconds() / 3600),
        'expired': duration_display(course.expired),
        'credit': duration_display(course.credit),
        'permission': course.permission,
        'permission_display': course.get_permission_display(),
        'content_count': args['content_count'],
    })

    # Mark Remove
    d.update({
        'time_total': d['duration'],  # Mark Remove
        'time_expired': d['expired'],  # Mark Remove
        'time_credit': d['credit'],  # Mark Remove
    })

    # FIXME : Split content_location
    # Progress # Fail!! 
    if is_video_progress:
        if account is not None and order_item is not None:
            for outline in d['outline_list']:
                for material in outline['video_list']:
                    material['progress'] = {}
                    material_object = Material.pull(material['id'])
                    if material_object:
                        # FIXME : unknown content_location & move out of the block to parent level to call
                        if order_item.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
                            program = Program.pull(order_item.content)
                        elif order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
                            program = Program.pull(order_item.content)
                        else:
                            program = None
                        progress = material_object.get_progress(order_item, program)
                        if progress is not None:
                            material['progress'] = progress.api_display()
    return d
