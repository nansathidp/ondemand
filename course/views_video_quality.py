from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

from course.models import Material
from stream.models import Stream

from api.views_ais_privilege import get_client_ip

@csrf_exempt
def quality_view(request):
    result = {'code': 404}
    if request.method == 'POST':
        material_id = request.POST.get('video')
        quality = request.POST.get('quality')
        material = Material.objects.get(id=material_id)
        ip = get_client_ip(request)
        if quality == '720p':
            quality = 0
        elif quality == '360p':
            quality = 1
        else:
            quality = 1
        stream = Stream.get_mobile_quality(material, ip, quality)
        result['code'] = 200
        result['url'] = stream['hls']
    return JsonResponse(result)
