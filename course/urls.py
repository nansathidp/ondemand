from django.conf.urls import url

from .views_buy import buy_view, checkout_view
from .views_detail import detail_view, detail_program_view  # , detail_slug_view
from .views_home import new_home_view
from .views_playlist import playlist_view, playlist_program_view
from .views_scorm import scorm_view
from .views_stamp import stamp_view, stamp_program_view
from .views_video_public import vider_public_view

# from .views_group import group_view, grid_view, by_subject_view
# from .views_item import item_view

app_name = 'course'
urlpatterns = [
    url(r'^$', new_home_view, name='home'),
    url(r'^(\d+)/buy/$', buy_view, name='buy'),
    url(r'^(\d+)/checkout/$', checkout_view, name='checkout'),

    url(r'^(\d+)/$', detail_view, name='detail'),
    url(r'^(\d+)/item/(\d+)/$', detail_view, name='detail'),
    url(r'^(\d+)/playlist/(\d+)/(\d+)/$', playlist_view, name='playlist'),
    url(r'^(\d+)/playlist/(\d+)/(\d+)/stamp/$', stamp_view, name='stamp'),

    # In Program
    url(r'^(\d+)/program/(\d+)/$', detail_program_view, name='detail_program'),
    url(r'^(\d+)/program/(\d+)/item/(\d+)/$', detail_program_view, name='detail_program'),
    url(r'^(\d+)/program/(\d+)/playlist/(\d+)/(\d+)/$', playlist_program_view, name='playlist_program'),
    url(r'^(\d+)/program/(\d+)/playlist/(\d+)/(\d+)/stamp/$', stamp_program_view, name='stamp_program'),

    # # TODO: CHECK
    # url(r'^type/(?P<group_name>[\w-]+)/$', group_view, name='group'),  # TODO: testing
    # url(r'^all/$', grid_view, name='course_grid'),  # TODO: testing
    # url(r'^all/(?P<subject_name>[\w-]+)/$', by_subject_view, name='course_subject'),  # TODO: testing
    # url(r'^item/$', item_view, name='item'),  # TODO: testing

    url(r'^(\d+)/material/(\d+)/public/$', vider_public_view, name='video_public'),  # TODO : remove ?

    # Detail Slug
    # url(r'^(\d+)/(.*)/(\d+)/$', detail_slug_view, name='detail'),
    # url(r'^(\d+)/(.*)/$', detail_slug_view, name='detail'),

    url(r'^scorm/$', scorm_view, name='scorm'),
]
