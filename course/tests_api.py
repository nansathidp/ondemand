import random
import string

from django.test import TestCase

URL_PERFIX = '/api/v5/course/'

from app.models import App
from key.models import Key


def _random():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(8))


def create_app():
    app = App.objects.create(name=_random(),
                             code=App.gen_code())
    return app


def create_key(app):
    key = Key.objects.create(app=app,
                             name=_random(),
                             code=Key.gen_code(app))
    return key


def pull_code_key():
    app = create_app()
    key = create_key(app)
    return app.code, key.code


class AuthTests(TestCase):
    def test_home(self):
        response = self.client.get(URL_PERFIX)
        self.assertEqual(response.status_code, 401)


class AnonymousTests(TestCase):
    def setUp(self):
        code, key = pull_code_key()
        self.payload = {'code': code,
                        'key': key,
                        'uuid': '1234',
                        'store': 3,
                        'version': 10}

    def test_home(self):
        response = self.client.get(URL_PERFIX, self.payload)
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(data['status'], 200)
