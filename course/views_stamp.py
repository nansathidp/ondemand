import datetime
import json
import random

from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse
from django.utils import timezone
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from program.models import Program
from .models import Course, Material
from progress.models import Progress
from order.models import Item as OrderItem
from content.models import Location as ContentLocation


def _stamp(request, content_location, progress_course, course, material, order_item, program=None):
    result = {
        'code': 200,
        'msg': 'OK'
    }

    if request.method == 'POST':
        try:
            w = int(request.POST.get('width', 120))
            if w < 120:
                w = 120
            elif w > 360:
                w -= 360
        except:
            w = 120
        try:
            h = int(request.POST.get('height', 120))
            if h < 120:
                h = 120
        except:
            h = 120
        dot = "{0:#b}".format(order_item.id)
        result['dot'] = dot[2:].replace('0', '_').replace('1', '.')
        result['dot_top'] = random.randrange(0, h)
        result['dot_left'] = random.randrange(0, w)
        result['course_id'] = course.id
        result['item_id'] = order_item.id
        action = request.POST.get('action', '')
        now = request.POST.get('now', None)
        time = request.POST.get('time', 0)
        now = datetime.datetime.fromtimestamp(float(now) / 1000.0)
        try:
            action = int(action)
            s = int(float(time))
            m = int((float(time) * 1000) % 1000)
            duration = datetime.timedelta(seconds=s, milliseconds=m)
            
            progress_material = Progress.pull(order_item,
                                              content_location,
                                              settings.CONTENT_TYPE('course', 'material'),
                                              material.id)

            progress_material.push_material(progress_course, order_item, request.user, material, action, duration)
            result['is_active'] = progress_course.is_active(order_item)
            try:
                result['start'] = order_item.start.strftime(settings.TIME_FORMAT)
            except:
                result['start'] = '--'
            result['progress_percent'] = round(progress_course.percent)
            if order_item.credit > datetime.timedelta(0):
                s = (order_item.credit - order_item.use_credit).total_seconds()
                h, r = divmod(s, 3600)
                m, s = divmod(r, 60)
                result['remaining'] = '%02d:%02d' % (h, m)
            else:
                result['remaining'] = '--'
            if order_item.expired > datetime.timedelta(0):
                if order_item.start is None:
                    result['expired'] = '%s days.' % (int(order_item.expired.total_seconds() / (60 * 60 * 24)))
                else:
                    expired = (order_item.start + order_item.expired) - timezone.now()
                    result['expired'] = '%s days.' % (int(expired.total_seconds() / (60 * 60 * 24)))
            else:
                result['expired'] = '--'
        except:
            raise
            pass

    return HttpResponse(json.dumps(result,
                                   indent=2),
                        content_type="application/json")


@csrf_exempt
@login_required
def stamp_view(request, course_id, material_id, order_item_id):
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    order_item = OrderItem.pull(order_item_id)
    if course is None or material is None or order_item is None:
        raise Http404

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'material'),
                                                  material.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent1_content=course.id)
    _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                   course.id)
    progress_course = Progress.pull(order_item,
                                    _content_location,
                                    settings.CONTENT_TYPE('course', 'course'),
                                    course.id)
    return _stamp(request, content_location, progress_course, course, material, order_item)


@csrf_exempt
@login_required
def stamp_program_view(request, program_id, course_id, material_id, order_item_id):
    program = Program.pull(program_id)
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    order_item = OrderItem.pull(order_item_id)
    if program is None or course is None or material is None or order_item is None:
        raise Http404

    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'material'),
                                                  material.id,
                                                  parent1_content_type=content_type,
                                                  parent1_content=program.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent2_content=course.id)
    _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                   course.id,
                                                   parent1_content_type=content_type,
                                                   parent1_content=program.id)
    progress_course = Progress.pull(order_item,
                                    _content_location,
                                    settings.CONTENT_TYPE('course', 'course'),
                                    course.id)    
    return _stamp(request, content_location, progress_course, course, material, order_item, program=program)
