from django.test import TestCase
from django.urls import reverse

from .tests import dashboard_user, dashboard_course

class PermissionOwnTests(TestCase):
    def setUp(self):
        self.course = dashboard_course(None)
        self.user = dashboard_user('view_own_course')
        self.client.force_login(self.user)

    def test_dependency(self):
        response = self.client.get(reverse('dashboard:course:dependency', args=[self.course.id]))
        self.assertEqual(response.status_code, 403)

    def test_dependency_search(self):
        response = self.client.get(reverse('dashboard:course:dependency_search', args=[self.course.id]))
        self.assertEqual(response.status_code, 403)

    def test_dependency_search_add(self): #Permiaaion change_course
        response = self.client.get(reverse('dashboard:course:dependency_search_add', args=[self.course.id]))
        self.assertEqual(response.status_code, 403)

    def test_dependency_search_delete(self): #Permiaaion change_course
        response = self.client.get(reverse('dashboard:course:dependency_search_delete', args=[self.course.id]))
        self.assertEqual(response.status_code, 403)

    def test_dependency_delete(self): #Permiaaion change_course
        response = self.client.get(reverse('dashboard:course:dependency_delete', args=[self.course.id]))
        self.assertEqual(response.status_code, 403)
