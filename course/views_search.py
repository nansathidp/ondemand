from django.conf import settings
from django.shortcuts import render

from course.models import Course
from ondemand.views_decorator import check_project
from rating.models import Rating


@check_project
def search_view(request):
    q = " ".join(request.GET.get('q', None).split())
    content_type = settings.CONTENT_TYPE('course', 'course')
    course_list = []
    if q is not None:
        course_id_lit = Course.objects.values_list('id', flat=True) \
                                      .filter(is_display=True,
                                              name__icontains=q).order_by('sort')[:50]
        for course_id in course_id_lit:
            course = Course.pull(course_id)
            if course is not None:
                course.tutor_list = course.get_tutor_list()
                course.rating = Rating.pull_first(content_type, course.id)
                course_list.append(course)

    return render(request,
                  'course/search.html',
                  {'DISPLAY_SUBMENU': True,
                   'ROOT_PAGE': 'course',
                   'course_list': course_list,
                   'keyword': q})
