from django.conf.urls import url

#TODO: migrate url format to from .views_xxx import xxx_view :)
from . import views_resend_verify, views_read

app_name = 'mailer'
urlpatterns = [
    url(r'^resend_verify_email/$', views_resend_verify.resend_view), #.js] #TODO: testing
    url(r'^resend_verify_email_with_specific_email/$', views_resend_verify.specific_email_view), #.js] #TODO: testing
    url(r'^read/(.*)', views_read.read_view), #TODO: testing
]
