from django.db import models
from django.conf import settings
from django.urls import reverse


class Mailer(models.Model):

    STATUS_CHOICES = (
        (0, 'FAILED'),
        (1, 'WAITING'),
        (2, 'SUCCESS'),
    )

    TYPE_CHOICES = (
        (0, 'Verify'),
        (1, 'Reset Password'),
        (2, 'Order'),
        (3, 'Order Success'),
        (4, 'Notify'),
        (5, 'Contact'),
        (6, 'Direct Message'),
        (7, 'Assignment'),
        (8, 'Progress Update'),
        (9, 'Export Progress'),
    )

    SERVICE_CHOICES = (
        (1, 'Mandrill'),
        (2, 'Google App Engine'),
    )

    from_email = models.EmailField(max_length=254)
    to = models.EmailField(max_length=254, db_index=True)
    subject = models.CharField(max_length=120)
    body = models.TextField()
    traceback = models.TextField()
    status = models.IntegerField(choices=STATUS_CHOICES, default=1)
    type = models.IntegerField(choices=TYPE_CHOICES, default=0)
    service = models.IntegerField(choices=SERVICE_CHOICES, default=1)
    order = models.ForeignKey('order.Order', null=True, blank=True)
    send_time = models.DateTimeField(blank=True, null=True, db_index=True)

    class Meta:
        ordering = ['-send_time']

    def send(self):
        import traceback
        from django.conf import settings
        from django.utils import timezone
        import mandrill
        import requests
        import smtplib
        
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText

        from_name = 'System Admin'
        if settings.MAILER_SERVICE == 'SMTP':
            # AIS
            # server = smtplib.SMTP(host='10.252.160.41', port=25)
            # sender_email = "aiseventdev@corp.ais900dev.org"

            sender_email = settings.MAILER_SENDER
            server = smtplib.SMTP(host=settings.MAILER_HOST, port=settings.MAILER_PORT)
            server.set_debuglevel(1)
            
            msg = MIMEMultipart('alternative')
            msg['Subject'] = self.subject
            msg['From'] = sender_email
            msg['To'] = self.to

            # part1 = MIMEText(text, 'plain')
            part2 = MIMEText(self.body, 'html')

            # msg.attach(part1)
            msg.attach(part2)
            try:
                server.sendmail(sender_email,
                                self.to,
                                msg.as_string())
                server.quit()
            except:
                pass
            self.status = 2
        elif settings.MAILER_SERVICE == 'MANDRILL':
            mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
            message = {'from_email': self.from_email,
                       'from_name': from_name,
                       'to': [{'type': 'to',
                               'email': self.to,
                               'name': ''}],
                       'subject': self.subject,
                       'html': self.body}
            response = mandrill_client.messages.send(message=message, async=False)
            if response[0]['status'] == 'sent':
                self.status = 2
            else:
                self.status = 1
        elif settings.MAILER_SERVICE == 'DJANGO_SMTP':
            from django.core.mail import send_mail
            try:
                send_mail(self.subject, '', self.from_email, self.to.split(','), html_message=self.body, fail_silently=False)
                self.status = 2
                self.save(update_fields=['status'])
            except:
                self.traceback = traceback.format_exc()
                self.save(update_fields=['traceback'])
        elif settings.MAILER_SERVICE == 'GOOGLE_APP':
            payload = {
                'key': 'tQFc9+9HRqD4ZOc95iOByMRm84tfTNX/Oho9KAZoRS0=',
                'sender': '%s < %s >' % (from_name, self.from_email),
                'subject': self.subject,
                'to': self.to,
                'html': self.body
            }
            r = requests.post('https://cocodemy-mailer.appspot.com/mail/send/', data=payload)
            self.status = 2 if r.status_code == 200 else 0

        # End
        self.send_time = timezone.now()
        self.save(update_fields=['status', 'send_time'])

    @staticmethod
    def get_sender(domain):
        return settings.CONFIG(domain, 'CONTACT_EMAIL')

    @staticmethod
    def push(from_email, to_email, subject, body, type, service=1, order=None):
        return Mailer.objects.create(
            from_email=from_email,
            to=to_email,
            subject=subject,
            body=body,
            type=type,
            service=service,
            order=order
        )

    @staticmethod
    def push_send(from_email, to_email, subject, body, type, service=1, order=None):
        mailer = Mailer.objects.create(
            from_email=from_email,
            to=to_email,
            subject=subject,
            body=body,
            type=type,
            service=service,
            order=order
        )
        try:
            mailer.send()
        except:
            # TODO : Log to sysadmin
            pass

    @staticmethod
    def send_create_account(account, from_email, token, status):
        from django.template.loader import render_to_string
        link = '%s%s?token=%s&status=%s' % (settings.SITE_URL, reverse('reset_password'), token, status)
        subject = '%sCreate your Password on LMS system : %s' % (settings.EMAIL_SUBJECT_PREFIX, account.email)
        body = render_to_string('mailer/create_account.html',
                                {'email': account.email,
                                 'link': link})
        # Mailer.push_send(settings.EMAIL_HOST_USER, account.email, subject, body, 1)
        Mailer.push(settings.EMAIL_HOST_USER, account.email, subject, body, 1)

    @staticmethod
    def send_forget_password_web(account, from_email, token, status):
        from django.template.loader import render_to_string
        link = '%s%s?token=%s&status=%s' % (settings.SITE_URL, reverse('reset_password'), token, status)
        subject = '%sCreate your Password on LMS system : %s' % (settings.EMAIL_SUBJECT_PREFIX, account.email)
        body = render_to_string('mailer/forgot.html',
                                {'email': account.email,
                                 'link': link})
        Mailer.push_send(settings.EMAIL_HOST_USER, account.email, subject, body, 1)

    @staticmethod
    def send_order_email(order, from_email, site_url):
        from django.template.loader import render_to_string
        if order.status == 3:
            return
        elif Mailer.objects.filter(to=order.account.email,
                                   order=order).exists():
            return

        email_body = render_to_string('mailer/order/order_payment.html',
                                      {'order': order,
                                       'site_url': site_url})
        Mailer.push_send(from_email, order.account.email, 'Order : %s' % order.id, email_body, 2, order=order)

    @staticmethod
    def send_billing_email(order, bill, domain):
        from django.template.loader import render_to_string
        from account.models import Address
        domain = domain.split(':')[0].replace('www.', '')
        email_body = render_to_string('mailer/order/order_billing.html',
                                      {'order': order,
                                       'bill': bill,
                                       'address': Address.objects.filter(account_id=order.account_id).first(),
                                       'site_url': domain})
        from_email = Mailer.get_sender(domain)
        Mailer.push_send(from_email, order.account.email, 'Billing for Order : %s' % order.id, email_body, 3, order=order)

    @staticmethod
    def send_notify_email(order, bank_transfer_record, from_email, site_url):
        from django.template.loader import render_to_string
        from account.models import Address
        email_body = render_to_string('mailer/order/notify_to_admin.html',
                                      {'order': order,
                                       'address': Address.objects.filter(account_id=order.account_id).first(),
                                       'bank_transfer_record': bank_transfer_record,
                                       'site_url': site_url})
        Mailer.push_send(from_email, from_email, 'Notify Banktransfer Approve for Order : %s' % order.id, email_body, 4, order=order)

    @staticmethod
    def send_contact_email(contact_data, from_email, site_url):
        from django.template.loader import render_to_string
        email_body = render_to_string('mailer/contact_email.html',
                                      {'name': contact_data.get('name', ''),
                                       'email': contact_data.get('email', ''),
                                       'tel': contact_data.get('tel', ''),
                                       'message': contact_data.get('message', ''),
                                       'site_url': site_url})
        Mailer.push_send(contact_data.get('email', from_email),
                         from_email,
                         'Contact Email : %s' % contact_data.get('email', ''),
                         email_body,
                         5)

    @staticmethod
    def send_login_alert(request, account):
        from django.template.loader import render_to_string
        from api.views_ip import get_client_ip
        from django.utils import timezone
        from_email = settings.MAILER_SENDER

        to_email = settings.SYSTEM_ADMIN_EMAIL
        to_email.append(account.email)
        to = ",".join(to_email)
        ip = get_client_ip(request)
        subject = 'Login Alert form ip : %s' % ip
        body = render_to_string('mailer/login_alert.html', {
            'ip': ip,
            'timestamp': timezone.now(),
            'account': account
        })
        Mailer.push_send(from_email,
                         to,
                         subject,
                         body,
                         5)

    @staticmethod
    def send_progress(progress):
        from django.template.loader import render_to_string
        subject = 'Progress Update : Content ?'
        body = render_to_string('mailer/progress.html',
                                {'progress': progress
                                 })
        Mailer.push_send(settings.MAILER_SENDER, progress.account.email, subject, body, 8)

    @staticmethod
    def send_inbox(inbox, account, domain):
        from django.template.loader import render_to_string
        subject = 'Direct Message : %s' % inbox.title
        body = render_to_string('mailer/message.html',
                                {'inbox': inbox,
                                 'account': account
                                 })
        Mailer.push_send(settings.MAILER_SENDER, account.email, subject, body, 6)

    @staticmethod
    def send_assignment(assignment, content_list):
        from django.template.loader import render_to_string
        for member in assignment.member_set.all():
            subject = 'Assignment: %s assign content to you' % member.account.email
            body = render_to_string('mailer/assignment.html',
                                    {'member': member,
                                     'content_list': content_list,
                                     'assignment': assignment})
            Mailer.push_send(settings.MAILER_SENDER, member.account.email, subject, body, 7)


class Template(models.Model):
    STATUS_CHOICES = (
        (1, 'Draft'),
        (2, 'Wait Send.'),
        (3, 'Sending: Create Pause'),
        (4, 'Sending: Create'),
        (5, 'Sending: Send'),
        (6, 'Finish'),
    )

    subject = models.CharField(max_length=120)
    html = models.TextField()
    domain = models.CharField(max_length=120)
    count_send = models.IntegerField(default=0)
    count_read = models.IntegerField(default=0)
    create_limit_start = models.IntegerField(default=0)
    status = models.IntegerField(choices=STATUS_CHOICES, db_index=True)


class Send(models.Model):
    template = models.ForeignKey(Template)
    account_id = models.BigIntegerField()
    account_email = models.CharField(max_length=512)
    code = models.CharField(max_length=32, db_index=True)
    is_read = models.BooleanField(default=False, db_index=True)
    is_send = models.BooleanField(default=False, db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']

    def send_appengine(self):
        import requests
        from django_facebook.views import unicode
        tag_read = '<img src="%s/mailer/read/%s-%s.png">' % (settings.SITE_URL, self.id, self.code)
        html = self.template.html.replace(unicode('[[read]]'), unicode(tag_read))
        payload = {'key': 'tQFc9+9HRqD4ZOc95iOByMRm84tfTNX/Oho9KAZoRS0=',
                   'sender': settings.CONFIG(self.template.domain, 'CONTACT_EMAIL'),
                   'subject': self.template.subject,
                   'to': self.account_email,
                   'html': html}
        r = requests.post('https://cocodemy-mailer.appspot.com/mail/send/', data=payload)
        self.is_send = True
        self.save(update_fields=['is_send'])
        return True if r.status_code == 200 else False

    def send_mandrill(self):
        import mandrill
        mandrill_client = mandrill.Mandrill(settings.MANDRILL_API_KEY)
        message = {'from_email': settings.CONFIG(self.template.domain, 'CONTACT_EMAIL'),
                   'to': [{'type': 'to',
                           'email': self.account_email,
                           'name': ''}],
                   'subject': self.template.subject,
                   'html': self.template.html}
        response = mandrill_client.messages.send(message=message, async=False)
        self.is_send = True
        self.save(update_fields=['is_send'])
