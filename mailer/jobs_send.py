import time

from django.conf import settings

from mailer.models import Mailer


def sernd_delay_job():
    for mailer in Mailer.objects.filter(status=1):
        mailer.send()
        time.sleep(settings.MAILER_DELAY)
