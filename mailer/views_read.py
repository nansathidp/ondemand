from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse

from mailer.models import Send
from PIL import Image


def read_view(request, token):
    try:
        p = token.split('.')[0].split('-')
        send = get_object_or_404(Send, id=p[0])
        if send.code == p[1] and send.is_read is False:
            send.is_read = True
            send.save(update_fields=['is_read'])
            template = send.template
            template.count_read += 1
            template.save(update_fields=['count_read'])
    except:
        pass
    red = Image.new('RGBA', (1, 1), (255,0,0,0))
    response = HttpResponse(content_type="image/jpeg")
    red.save(response, "PNG")
    return response
