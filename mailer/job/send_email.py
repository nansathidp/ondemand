import django
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from inbox.models import Inbox

from django.conf import settings
from django.template.loader import render_to_string
from mailer.models import Mailer
from account.models import Account


def _send_inbox(inbox_id, account_id):
    inbox = Inbox.objects.get(id=inbox_id)
    account = Account.objects.get(id=account_id)
    subject = 'Direct Message : %s' % inbox.title
    body = render_to_string('mailer/message.html',
                            {'inbox': inbox,
                             'account': account
                             })
    Mailer.push_send(settings.MAILER_SENDER, account.email, subject, body, 6)


def _send_assignment_email(assignment, item_content_list):
    Mailer.send_assignment(assignment, item_content_list)
