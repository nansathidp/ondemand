from django.contrib import admin
from .models import Mailer, Template, Send


@admin.register(Mailer)
class MailerAdmin(admin.ModelAdmin):
    list_display = ('id', 'from_email', 'to', 'subject', 'status', 'type', 'send_time')
    sortable = 'status'
    search_fields = ['from_email']
    readonly_fields = ['order']


@admin.register(Template)
class TemplateAdmin(admin.ModelAdmin):
    list_display = ('subject', 'count_send', 'count_read', 'status', 'create_limit_start')


@admin.register(Send)
class SendAdmin(admin.ModelAdmin):
    list_display = ('code', 'is_read', 'is_send', 'timestamp')
