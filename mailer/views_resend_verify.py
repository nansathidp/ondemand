import urllib

from django.conf import settings
from django.http import HttpResponse
from django.views.decorators.http import require_POST

from api.views_api import json_render
from mailer.models import Mailer


def resend_view(request):
    if not request.user.is_authenticated:
        return HttpResponse(status=403)

    if len(request.GET) > 0:
        return HttpResponse(status=403)
    # VA Scan

    result = {}
    code = 200 
    link_to_redirect = request.META.get('HTTP_REFERER')
    if request.user.is_verify:
        return json_render(result, code)
    contact_email = settings.CONFIG(request.get_host(), 'CONTACT_EMAIL')
    site_url = request.get_host()
    Mailer.send_verify_email(request.user, contact_email, site_url, link_to_redirect)
    return json_render(result, code)

@require_POST
def specific_email_view(request):
    result = {}
    code = 200
    link_to_redirect = request.META.get('HTTP_REFERER')

    email = request.POST.get('email', None)
    email = urllib.parse.unquote(email)

    if email is not None:
        account = request.user
        account.email = email
        account.save(update_fields=['email'])
        contact_email = settings.CONFIG(request.get_host(), 'CONTACT_EMAIL')
        site_url = request.get_host()
        Mailer.send_verify_email(account, contact_email, site_url, link_to_redirect)
    else: 
        code = 300
    return json_render(result, code)
