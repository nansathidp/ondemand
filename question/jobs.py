from django.conf import settings

from alert.models import Alert
from question.models import Activity, Section, Question, Choice

import os
from xlrd import open_workbook, XL_CELL_TEXT


def _section(activity, name, type, sort):
    try:
        section = activity.section_set.all()[sort - 1]
        if section.type != type or section.name != name or section.sort != sort:
            section.name = name[:120]
            section.type = type
            section.save()
        return section
    except:
        return Section.objects.create(activity=activity,
                                      name=name[:120],
                                      type=type,
                                      sort=sort)


def _question(section, text, type, sort, score):
    try:
        score = int(score)
        if score < 0:
            score = 1
    except:
        score = 0
    try:
        question = section.question_set.all()[sort - 1]
        if question.type != type or question.text != text or question.score != score or question.sort != sort:
            question.text = text
            question.type = type
            question.sort = sort
            question.score = score
            question.save()
        return question
    except:
        return Question.objects.create(section=section,
                                       text=text,
                                       type=type,
                                       sort=sort,
                                       score=score)


def _choice(question, text, sort, is_answer):
    if is_answer == '':
        is_answer = False
    else:
        is_answer = True

    if is_answer:
        question.choice_set.filter(is_answer=True).update(is_answer=False)

    try:
        choice = question.choice_set.all()[sort - 1]
        choice.text = text
        choice.sort = sort
        choice.is_answer = is_answer

        choice.save()
        return choice
    except:
        return Choice.objects.create(question=question,
                                     text=text,
                                     sort=sort,
                                     is_answer=is_answer)


def _import(alert_id):
    try:
        alert = Alert.objects.get(id=alert_id)
    except:
        return None

    try:
        activity = Activity.objects.get(id=alert.content)
    except:
        return None

    path = os.path.join(settings.BASE_DIR, 'alert', 'upload', alert.file_input)
    book = open_workbook(path)

    sheet_name_list = book.sheet_names()

    section_sort = 1
    for sheet_name in sheet_name_list:
        sheet = book.sheet_by_name(sheet_name)

        if sheet.cell(0, 1).value.lower().find('choice') != -1:
            type = 2
        else:
            type = 4
        section = _section(activity, sheet_name, type, section_sort)
        section_sort += 1
        if type == 2:
            question = None
            question_sort = 1
            choice_sort = 1

            for i in range(sheet.nrows):
                if i == 0:
                    continue
                _question_text = sheet.cell(i, 0).value
                if _question_text != '':
                    if choice_sort != 1:
                        for _ in question.choice_set.all()[choice_sort - 1:]:
                            _.delete()
                    question = _question(section, _question_text, 2, question_sort, sheet.cell(i, 3).value)

                    question_sort += 1
                    choice_sort = 1
                    if sheet.cell(i, 1).value == '':
                        continue
                _choice(question, sheet.cell(i, 1).value, choice_sort, sheet.cell(i, 2).value)
                choice_sort += 1

            if question is not None:
                for _ in question.choice_set.all()[choice_sort - 1:]:
                    _.delete()

            if question_sort == 1:
                for _ in section.question_set.all():
                    _.delete()
            for _ in section.question_set.all()[question_sort - 1:]:
                _.delete()

            section.update_score()

        elif type == 4:
            question_sort = 1

            # if sheet.nrows < 2:
            #     continue

            for i in range(sheet.nrows):
                if i == 0:
                    continue
                _question_text = sheet.cell(i, 0).value
                question = _question(section, _question_text, 4, question_sort, sheet.cell(i, 2).value)
                question_sort += 1
                if (i == sheet.nrows - 1):
                    for _ in section.question_set.all()[question_sort - 1:]:
                        _.delete()
            if question_sort == 1:
                for _ in section.question_set.all():
                    _.delete()

            section.update_score()

    for _ in activity.section_set.all()[section_sort - 1:]:
        _.delete()

    activity.update_max_score()
    alert.status = 2
    alert.save(update_fields=['status'])
