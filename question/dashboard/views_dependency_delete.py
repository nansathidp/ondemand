from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from ..models import Activity
from dependency.models import Dependency

@csrf_exempt
def dependency_delete_view(request, activity_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_change(request):
        raise PermissionDenied

    content_type = settings.CONTENT_TYPE('question', 'activity')
    dependency = Dependency.pull(content_type,
                                 activity.id)
    if request.method == 'POST':
        try:
            parent_id = int(request.POST.get('parent'))
            dependency.delete_parent(parent_id)
        except:
            pass

    parent_list = Dependency.pull_parent(content_type, activity.id)
    return render(request,
                  'question/dashboard/dependency_block.html',
                  {'activity': activity,
                   'parent_list': parent_list})
