from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.forms import inlineformset_factory
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string

from .forms import QuestionForm, QuestionScaleForm, QuestionMutipleSelectForm, QuestionFillDescriptionForm, ChoiceForm, \
    ChoiceMutipleChoiceForm
from ..models import Activity, Section, Question, Choice


def question_edit_view(request, activity_id, section_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied

    section = get_object_or_404(Section, activity=activity, id=section_id)
    if section.type == 2:
        ChoiceFormSet = inlineformset_factory(Question, Choice,
                                              form=ChoiceMutipleChoiceForm,
                                              extra=4,
                                              can_delete=True)
    else:
        ChoiceFormSet = inlineformset_factory(Question, Choice,
                                              form=ChoiceForm,
                                              extra=4,
                                              can_delete=True)
    if request.method == 'POST':
        question_id = request.GET.get('question', -1)
        question = get_object_or_404(Question, section=section, id=question_id)

        if section.type == 3:
            question_form = QuestionMutipleSelectForm(request.POST, instance=question)
        elif section.type == 4:
            question_form = QuestionFillDescriptionForm(request.POST, instance=question)
        else:
            question_form = QuestionForm(request.POST, instance=question)

        if question_form.is_valid():
            question = question_form.save()
            section.update_score()
            section.save(update_fields=['score'])
            if section.type == 1 or section.type == 4:
                return JsonResponse({'status': 302,
                                     'url': ''})
            else:
                choice_form = ChoiceFormSet(request.POST, instance=question)
                if choice_form.is_valid():
                    choice_form.save()
                    return JsonResponse({'status': 302,
                                         'url': ''})
        else:
            choice_form = ChoiceFormSet()
    else:
        question_id = request.GET.get('question', -1)
        question = get_object_or_404(Question, section=section, id=question_id)

        if section.type == 1:
            question_form = QuestionScaleForm(instance=question)
        elif section.type == 3:
            question_form = QuestionMutipleSelectForm(instance=question)
        elif section.type == 4:
            question_form = QuestionFillDescriptionForm(instance=question)
        else:
            question_form = QuestionForm(instance=question)
        choice_form = ChoiceFormSet(instance=question)
    return JsonResponse({'status': 200,
                         'html': render_to_string('question/dashboard/question_edit.html',
                                                  request=request,
                                                  context={'activity': activity,
                                                           'section': section,
                                                           'question_form': question_form,
                                                           'choice_form': choice_form})})


def question_edit2_view(request, activity_id, section_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied

    section = get_object_or_404(Section, activity=activity, id=section_id)

    if request.method == 'POST':
        question_id = request.GET.get('question', -1)
        question = get_object_or_404(Question, section=section, id=question_id)
        try:
            score = int(request.POST.get('score'))
        except:
            score = 0
        question.text = request.POST.get('text', '')
        question.score = score
        question.save(update_fields=['text', 'score'])

        question.choice_set.update(is_answer=False)
        ans = request.POST.get('ans', None)

        c = 0

        # Check Delete
        for choice in question.choice_set.all():
            if not 'choice-%s' % choice.id in request.POST:
                choice.delete()
            else:
                c += 1
                choice.text = request.POST.get('choice-%s' % choice.id, '')
                if ans == str(choice.id):
                    choice.is_answer = True
                    choice.sort = c
                choice.save(update_fields=['text', 'is_answer', 'sort'])

        # Check Add
        is_new_answer = False
        for n in range(0, settings.TEST__MAX_CHOICE):
            if 'choice-new-%s' % n in request.POST:
                c += 1
                choice = request.POST.get('choice-new-%s' % n, '')
                if len(choice) == 0:
                    continue
                if ans and ans == 'new-%s' % n:
                    is_new_answer = True
                    is_answer = True
                else:
                    is_answer = False
                Choice.objects.create(question=question,
                                      text=choice,
                                      sort=c,
                                      is_answer=is_answer)

        choice_new_list = []
        section.update_score()
        return JsonResponse({'status': 302,
                             'url': ''})
    else:
        question_id = request.GET.get('question', -1)
        question = get_object_or_404(Question, section=section, id=question_id)
        c = question.choice_set.count()
        if c < 4:
            choice_new_list = range(0, 4 - c)
        else:
            choice_new_list = []
    section.update_score()
    return JsonResponse({'status': 200,
                         'html': render_to_string('question/dashboard/question_create2.html',
                                                  request=request,
                                                  context={'activity': activity,
                                                           'section': section,
                                                           'question': question,
                                                           'choice_new_list': choice_new_list})})
