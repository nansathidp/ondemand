from django.shortcuts import render, get_object_or_404
from django.conf import settings

from account.models import Account
from ..models import Score
from order.models import Item as OrderItem
from content.models import Location as ContentLocation

from .views import check_access
from utils.breadcrumb import get_breadcrumb

@check_access
def score_account_view(request, activity_id, account_id, content_location_id=None):
    activity = request.activity
    account = get_object_or_404(Account, id=account_id)
    if content_location_id is not None:
        request.content_location = get_object_or_404(ContentLocation,
                                                     id=content_location_id)
    else:
        request.content_location = None
    
    content_location_list = ContentLocation.objects.filter(score__account=account,
                                                           score__activity=activity) \
                                                   .distinct()
    if request.content_location is not None:
        content_location_list = [request.content_location]
    score_list = Score.objects.filter(activity=activity,
                                      account=account) \
                              .order_by('submit_number')
    return render(request,
                  'question/dashboard/score_account.html',
                  {
                      'SIDEBAR': 'question',
                      'TAB': 'score',
                      'BREADCRUMB_LIST': [], #get_breadcrumb_old(request),
                      'activity': activity,
                      'content_type_name': 'test',
                      'account': account,
                      'content_location_list': content_location_list,
                      'score_list': score_list
                  })
