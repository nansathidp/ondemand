from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Activity

#Not Use
def sort_view(request, activity_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied

    move = request.GET.get('move', None)
    if move == 'up':
        activity_swap = Activity.objects.filter(sort__lt=activity.sort).exclude(id=activity.id).order_by('-sort').first()
        if activity_swap is not None:
            activity_swap.sort = activity.sort
            activity_swap.save(update_fields=['sort'])
        activity.sort -= 1
        activity.save(update_fields=['sort'])
    elif move == 'down':
        activity_swap = Activity.objects.filter(sort__gt=activity.sort).exclude(id=activity.id).order_by('sort').first()
        if activity_swap is not None:
            activity_swap.sort = activity.sort
            activity_swap.save(update_fields=['sort'])
        activity.sort += 1
        activity.save(update_fields=['sort'])
    return redirect('dashboard:question:home')
