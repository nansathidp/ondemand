from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt

from ..models import Activity

@csrf_exempt
def alert_view(request, activity_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied

    is_progress_update = False
    return render(request,
                  'question/dashboard/alert.html',
                  {'activity': activity,
                   'is_progress_update': is_progress_update})
