from django.conf.urls import url

from .views import home_view
from .views_alert import alert_view
from .views_create import create_view
from .views_dependency import dependency_view
from .views_dependency_delete import dependency_delete_view
from .views_dependency_search import dependency_search_view
from .views_dependency_search_add import dependency_search_add_view
from .views_dependency_search_delete import dependency_search_delete_view
from .views_detail import detail_view
from .views_progress import progress_view
from .views_question_create import question_create_view, question_create2_view
from .views_question_delete import question_delete_view
from .views_question_edit import question_edit_view, question_edit2_view
from .views_question_sort import question_sort_view
from .views_report import report_view
from .views_score import score_view
from .views_score_account import score_account_view
from .views_score_detail import score_detail_view
from .views_score_progress import score_progress_view
from .views_score_submit import score_submit_view
from .views_section import section_view
from .views_section_create import section_create_view
from .views_section_delete import section_delete_view
from .views_section_detail import section_detail_view
from .views_section_sort import section_sort_view
from .views_section_upload import section_upload_view
from .views_shuffle import section_shuffle_view, section_shuffle_choice_view
from .views_time import time_view
from .views_update import update_view

# from .views_sort import sort_view

app_name = 'question'
urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^create/$', create_view, name='create'),
    url(r'^(\d+)/$', detail_view, name='detail'),

    # url(r'^(\d+)/sort/$', sort_view, name='sort'),

    url(r'^(\d+)/section/$', section_view, name='section'),
    url(r'^(\d+)/section/create/$', section_create_view, name='section_create'),

    url(r'^(\d+)/section/shuffer/$', section_shuffle_view, name='section_shuffer'),
    url(r'^(\d+)/section/shuffer/choice/$', section_shuffle_choice_view, name='section_shuffer_choice'),

    url(r'^(\d+)/section/(\d+)/sort/$', section_sort_view, name='section_sort'),
    url(r'^(\d+)/section/(\d+)/upload/$', section_upload_view, name='section_upload'),
    url(r'^(\d+)/section/(\d+)/detail/$', section_detail_view, name='section_detail'),
    url(r'^(\d+)/section/(\d+)/delete/$', section_delete_view, name='section_delete'),

    url(r'^(\d+)/section/(\d+)/question/create/$', question_create_view, name='question_create'),
    url(r'^(\d+)/section/(\d+)/question/edit/$', question_edit_view, name='question_edit'),
    url(r'^(\d+)/section/(\d+)/question/create2/$', question_create2_view, name='question_create2'),
    url(r'^(\d+)/section/(\d+)/question/edit2/$', question_edit2_view, name='question_edit2'),

    url(r'^(\d+)/section/(\d+)/question/(\d+)/sort/$', question_sort_view, name='question_sort'),
    url(r'^(\d+)/section/(\d+)/question/(\d+)/delete/$', question_delete_view, name='question_delete'),

    # PERIOD & CREDIT
    url(r'^(\d+)/time/$', time_view, name='time'),

    # Dependency
    url(r'^(\d+)/dependency/$', dependency_view, name='dependency'),
    url(r'^(\d+)/dependency/search/$', dependency_search_view, name='dependency_search'),
    url(r'^(\d+)/dependency/search/add/$', dependency_search_add_view, name='dependency_search_add'),
    url(r'^(\d+)/dependency/search/delete/$', dependency_search_delete_view, name='dependency_search_delete'),
    url(r'^(\d+)/dependency/delete/$', dependency_delete_view, name='dependency_delete'),

    # Score
    url(r'^(\d+)/score/$', score_view, name='score'),
    url(r'^(\d+)/score/(\d+)/$', score_detail_view, name='score_detail'),
    url(r'^(\d+)/score/submit/$', score_submit_view, name='score_submit'),
    url(r'^(\d+)/score/(\d+)/progress/$', score_progress_view, name='score_progress'),

    url(r'^(\d+)/score/account/(\d+)/$', score_account_view, name='score_account'),
    url(r'^(\d+)/score/account/(\d+)/(\d+)/$', score_account_view, name='score_account'),
    url(r'^(\d+)/report/(\d+)/$', report_view, name='report'),
    url(r'^(\d+)/progress/$', progress_view, name='progress'),

    url(r'^(\d+)/alert/$', alert_view, name='alert'),
    url(r'^(\d+)/update/$', update_view, name='update'),
]
