from django.conf import settings
from django.shortcuts import render, get_object_or_404

from .forms import SectionForm, SectionInfoForm
from .views import check_access, pull_breadcrumb
from ..models import Section


@check_access
def section_detail_view(request, activity_id, section_id):
    activity = request.activity
    section = get_object_or_404(Section, activity=activity, id=section_id)

    if request.method == 'POST':
        if section.type == 0:
            section_form = SectionInfoForm(request.POST, request.FILES, instance=section)
        else:
            section_form = SectionForm(request.POST, instance=section)
        if section_form.is_valid():
            section = section_form.save()

    if section.type == 0:
        section_form = SectionInfoForm(instance=section)
    else:
        section_form = SectionForm(instance=section)
    is_upload =  check_ip(request)
    return render(request,
                  'question/dashboard/section_detail.html',
                  {'BREADCRUMB_LIST': pull_breadcrumb(activity, section),
                   'TAB': 'section',
                   'SERVER_UPLOAD_URL': settings.SERVER_UPLOAD_URL,
                   'activity': activity,
                   'section': section,
                   'max_choice': settings.TEST__MAX_CHOICE,
                   'section_form': section_form,
                   'is_upload':is_upload})


def check_ip(request):
    from django.conf import settings
    if settings.IS_IP_UPLOAD:
        from api.views_ais_privilege import get_client_ip
        import ipaddress
        request.is_upload = False
        ip = get_client_ip(request)
        for allow in settings.IPTABLE_UPLOAD_ALLOW:
            if ipaddress.ip_address(ip) in ipaddress.ip_network(allow):
                return True
        return False
    else:
        return True
