import copy

from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt
from question.models import Activity, Section
import json
from django.http import JsonResponse

@csrf_exempt
def section_shuffle_view(request, activity_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if request.method == 'POST':
        section = get_object_or_404(Section, id=request.POST.get('id', -1), activity=activity)
        section.is_shuffle = not section.is_shuffle
        section.save(update_fields=['is_shuffle'])
    return JsonResponse({})


@csrf_exempt
def section_shuffle_choice_view(
        request, activity_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if request.method == 'POST':
        section = get_object_or_404(Section, id=request.POST.get('id', -1), activity=activity)
        section.is_shuffle_choice = not section.is_shuffle_choice
        section.save(update_fields=['is_shuffle_choice'])
    return JsonResponse({})
