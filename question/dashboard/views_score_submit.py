from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.conf import settings
from django.db.models import Sum
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.response import Response

from ..models import Activity, Question, Answer, Score, ScoreSection
from progress.models import Content as ProgressContent


@csrf_exempt
def score_submit_view(request, activity_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_change(request):
        raise PermissionDenied

    result = {
        'is_error': False,
        'is_change': False
    }
    if request.method == 'POST':
        try:
            name = request.POST.get('name').split('-')
            question_id = name[1]
            answer_id = name[2]
            score = int(request.POST.get('score'))
            score_item_id = int(request.POST.get('score_item'))
            url = request.POST.get('url', None)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        question = get_object_or_404(Question,
                                     section__activity=activity,
                                     id=question_id)
        score_item = get_object_or_404(Score,
                                       activity=activity,
                                       id=score_item_id)
        answer = get_object_or_404(Answer,
                                   question=question,
                                   id=answer_id)
        if question.type == 4 and answer.score != score:
            if 0 <= score <= question.score:
                answer.score = score
                answer.save(update_fields=['score'])
            else:
                result['is_error'] = True
                result['msg'] = 'Score > Max Score.'
                result['score'] = answer.score
                
        if not result['is_error']:
            score = Answer.objects.filter(score_item=score_item,
                                          question__section_id=question.section_id) \
                                  .aggregate(Sum('score'))['score__sum']
            try:
                score = int(score)
            except:
                score = 0

            ScoreSection.objects.filter(score_item=score_item,
                                        section_id=answer.question.section_id) \
                                .update(status=2,
                                        score=score)

        if not result['is_error'] and not Answer.objects.filter(score_item=score_item,
                                                                score=-1).exists():
            score = score_item.sum_score()
            
            result['is_change'] = score != score_item.score
            result['score'] = score
            result['max_score'] = activity.max_score
            result['pass_score'] = activity.pass_score
            if activity.pass_score == 0:
                result['status'] = 2
                result['status_display'] = 'Completed'
            elif score > activity.pass_score:
                result['status'] = 2
                result['status_display'] = 'Completed'
            else:
                result['status'] = 2
                result['status_display'] = 'Failed'

            result['old_score'] = score_item.score
            result['old_status'] = score_item.status
            result['old_status_display'] = score_item.get_status_display()
            
            score_item.check_complete(score, False)
            score_item.push_progress()
            ProgressContent.update_progress(score_item.location_id,
                                            settings.CONTENT_TYPE('question', 'activity').id,
                                            activity.id)
            if url is None:
                # result['url'] = reverse('dashboard:question:score_progress', args=[activity.id, score_item.id])
                result['url'] = reverse('dashboard:question:score_account', args=[activity.id, score_item.account_id])
            else:
                result['url'] = url
    return JsonResponse(result)
