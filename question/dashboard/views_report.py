from django.shortcuts import render, get_object_or_404

from account.models import Account
from utils.paginator import paginator
from .views import check_access, pull_breadcrumb
from ..models import Section, Answer


@check_access
def report_view(request, activity_id, section_id):
    activity = request.activity
    
    section = get_object_or_404(Section, activity=activity, id=section_id)
    if section.type == 2:
        account_list = Account.objects.filter(score__activity=activity).distinct()
        account_list = paginator(request, account_list)
        answer_list = list(Answer.objects.filter(question__section=section, account__in=list(account_list.object_list)))
        question = None
    elif section.type == 4:
        account_list = Account.objects.filter(score__activity=activity).distinct()
        account_list = paginator(request, account_list)
        answer_list = list(Answer.objects.filter(question__section=section, account__in=list(account_list.object_list)))
        question = None
    elif section.type == 5:
        account_list = Account.objects.filter(score__activity=activity).distinct()
        account_list = paginator(request, account_list)
        question_id = request.GET.get('question', None)
        if question_id is not None:
            try:
                question = section.question_set.get(id=question_id)
            except:
                question = None
        else:
            question = section.question_set.first()
        if question is not None:
            answer_list = Answer.objects.filter(question=question, account__in=list(account_list.object_list))
        else:
            answer_list = None
    else:
        account_list = None
        answer_list = None
        question = None
    print(account_list)
    return render(request,
                  'question/dashboard/report.html',
                  {'SIDEBAR': 'question',
                   'BREADCRUMB_LIST': pull_breadcrumb(activity, section),
                   'activity': activity,
                   'section': section,
                   'account_list': account_list,
                   'answer_list': answer_list,
                   'question': question})
