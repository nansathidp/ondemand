from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect, get_object_or_404

from ..models import Activity, Section


def section_sort_view(request, activity_id, section_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied
    
    section = get_object_or_404(Section, activity=activity, id=section_id)
    move = request.GET.get('move', None)
    if move == 'up':
        section_swap = Section.objects.filter(activity=activity, sort__lt=section.sort).exclude(id=section.id).order_by('-sort').first()
        if section_swap is not None:
            section_swap.sort = section.sort
            section_swap.save(update_fields=['sort'])
        section.sort -= 1
        section.save(update_fields=['sort'])
    elif move == 'down':
        section_swap = Section.objects.filter(activity=activity, sort__gt=section.sort).exclude(id=section.id).order_by('sort').first()
        if section_swap is not None:
            section_swap.sort = section.sort
            section_swap.save(update_fields=['sort'])
        section.sort += 1
        section.save(update_fields=['sort'])
    return redirect('dashboard:question:section', activity.id)
