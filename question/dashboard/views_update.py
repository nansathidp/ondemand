from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Activity

def update_view(request, activity_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied

    ref = request.META.get('HTTP_REFERER', None)
    if ref is not None:
        return redirect(ref)
    return redirect('dashboard:question:section', activity.id)
