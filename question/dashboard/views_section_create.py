from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Activity, Section

from .log import log_section_create


def section_create_view(request, activity_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied

    if request.method == 'POST':
        try:
            type = int(request.POST.get('type', None))
        except:
            type = None
        is_found = False
        for type_choice in Section.TYPE_CHOICES:
            if type == type_choice[0]:
                is_found = True
                break
        if not is_found:
            return redirect('dashboard:question:section', activity.id)
        else:
            section = Section.objects.create(activity=activity,
                                             name='',
                                             type=type,
                                             sort=activity.section_set.count() + 1)
            log_section_create(request, section)
            return redirect('dashboard:question:section_detail', activity.id, section.id)
    else:
        return redirect('dashboard:question:section', activity.id)
