from django.conf import settings
from django.shortcuts import get_object_or_404
from django.forms import inlineformset_factory
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt

from .forms import QuestionForm, QuestionScaleForm, QuestionMutipleSelectForm, QuestionFillDescriptionForm, ChoiceForm, ChoiceMutipleChoiceForm
from ..models import Activity, Section, Question, Choice


def question_create_view(request, activity_id, section_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied
    section = get_object_or_404(Section, activity=activity, id=section_id)

    if section.type == 2:
        ChoiceFormSet = inlineformset_factory(Question, Choice,
                                              form=ChoiceMutipleChoiceForm,
                                              extra=4)
    else:
        ChoiceFormSet = inlineformset_factory(Question, Choice,
                                              form=ChoiceForm,
                                              extra=4)
    if request.method == 'POST':
        if section.type == 3:
            question_form = QuestionMutipleSelectForm(request.POST, instance=Question(section=section,
                                                                                      type=section.type,
                                                                                      sort=section.question_set.count()+1))
        elif section.type == 4:
            question_form = QuestionFillDescriptionForm(request.POST, instance=Question(section=section,
                                                                                        type=section.type,
                                                                                        sort=section.question_set.count()+1))
        else:
            question_form = QuestionForm(request.POST, instance=Question(section=section,
                                                                         type=section.type,
                                                                         sort=section.question_set.count()+1))
        if question_form.is_valid():
            question = question_form.save()
            section.update_score()
            if section.type == 1 or section.type == 4:
                return JsonResponse({'status': 302,
                                     'url': ''})
            else:
                choice_form = ChoiceFormSet(request.POST, instance=question)
                if choice_form.is_valid():
                    choice_form.save()
                    return JsonResponse({'status': 302,
                                         'url': ''})
        else:
            choice_form = ChoiceFormSet()
    else:
        if section.type == 1:
            question_form = QuestionScaleForm()
        elif section.type == 3:
            question_form = QuestionMutipleSelectForm()
        elif section.type == 4:
            question_form = QuestionFillDescriptionForm(instance=Question(score=0))
        else:
            question_form = QuestionForm()
        choice_form = ChoiceFormSet()
    return JsonResponse({'status': 200,
                         'html': render_to_string('question/dashboard/question_create.html',
                                                  request=request,
                                                  context={'SIDEBAR': 'question',
                                                           'activity': activity,
                                                           'section': section,
                                                           'question_form': question_form,
                                                           'choice_form': choice_form})})


@csrf_exempt
def question_create2_view(request, activity_id, section_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied
    section = get_object_or_404(Section, activity=activity, id=section_id)

    if request.method == 'POST':
        try:
            score = int(request.POST.get('score'))
        except:
            score = 0
        question = Question.objects.create(section=section,
                                           text=request.POST.get('text', ''),
                                           score=score,
                                           type=section.type,
                                           sort=section.question_set.count()+1)
        try:
            ans = request.POST.get('ans', None)
            ans = int(ans.replace('new-', ''))
        except:
            ans = -1
        c = 0

        for n in range(0, settings.TEST__MAX_CHOICE):
            if 'choice-new-%s' % n in request.POST:
                c += 1
                choice = request.POST.get('choice-new-%s' % n, '')
                if len(choice) == 0:
                    continue

                if ans is not None and ans == n:
                    is_answer = True
                else:
                    is_answer = False

                Choice.objects.create(question=question,
                                      text=choice,
                                      sort=c,
                                      is_answer=is_answer)
            section.update_score()
        return JsonResponse({'status': 302,
                             'url': ''})
    return JsonResponse({'status': 200,
                         'html': render_to_string('question/dashboard/question_create2.html',
                                                  request=request,
                                                  context={'SIDEBAR': 'question',
                                                           'activity': activity,
                                                           'section': section,
                                                           'choice_new_list': range(0, 4)})})
