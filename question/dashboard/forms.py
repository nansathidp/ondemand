from django import forms
from django.conf import settings
from ..models import Activity, Section, Question, Choice
from provider.models import Provider

activity_labels = {
    'name': 'ชื่อข้อสอบ<br/>Test Name',
    'image': 'รูปประกอบข้อสอบ<br/>Test Image',
    'type': 'รูปแบบข้อสอบ<br/>Test Type',
    'provider': 'Learning Center',
    'overview': 'คำแนะนำข้อสอบ<br/>Test Overview',
    'desc': 'รายละเอียดข้อสอบ<br/>Test Description',
    'condition': 'เงื่อนไข<br/>Test condition',
    'is_display': 'Display',
    'is_show_answer': 'Show Answer',
    'data': 'Web Link'
}

activity_help_texts = {
    'name': 'Limit at 120 characters',
    'image': settings.HELP_TEXT_IMAGE('question'),
    'overview': 'Limit at 250 characters',
    'is_show_answer': 'Show Answer after user submit.'
}


class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ['name',  'image', 'type', 'category',
                  'provider',
                  'overview', 'desc', 'condition',
                  'is_show_answer','is_display' ]
        labels = activity_labels
        help_texts = activity_help_texts

    def __init__(self, request=None, *args, **kwargs):
        super(ActivityForm, self).__init__(*args, **kwargs)
        if request is not None:
            self.fields['provider'].queryset = Provider.access(request)


class ActivityEditForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ['name',  'image', 'type', 'category', 'provider',
                  'overview', 'desc', 'condition',
                  'is_show_answer','is_display',  'data']
        labels = activity_labels
        help_texts = activity_help_texts

    def __init__(self, request=None, *args, **kwargs):
        super(ActivityEditForm, self).__init__(*args, **kwargs)
        self.fields['type'].disabled = True
        if request is not None:
            self.fields['provider'].queryset = Provider.access(request)

    def clean_type(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.type
        else:
            return self.cleaned_data['type']


# class TimerForm(forms.ModelForm):
#     class Meta:
#         model = Timer
#         fields = ['type', 'time']
#         #fields = ['type', 'time', 'duration', 'schedule', 'related']
#         #labels = {
#         #    'related': 'Activity Related',
#         #}
#         #help_texts = {
#         #    'duration': 'hh:mm:ss'
#         #}
#
#     def __init__(self, *args, **kwargs):
#         #workshop = kwargs.pop('workshop', None)
#         super(TimerForm, self).__init__(*args, **kwargs)
#         self.fields['time'].widget.attrs.update({'class' : 'date-time-picker text-center'})
#         self.fields['time'].widget.format = '%m/%d/%Y %H:%M'
#
#         #self.fields['schedule'].queryset = self.fields['schedule'].queryset.filter(workshop=workshop)
#         #self.fields['related'].queryset = self.fields['related'].queryset.filter(workshop=workshop)


class SectionForm(forms.ModelForm):
    class Meta:
        model = Section
        fields = ['name']

    def __init__(self, *args, **kwargs):
        super(SectionForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class' : ''})


class SectionInfoForm(forms.ModelForm):
    class Meta:
        model = Section
        fields = ['name', 'content', 'data_type', 'image']
        labels = {
            'data_type': 'Type',
        }
        help_texts = {
            'content': 'สามารถใส่ [image] หรือ [video] ระหว่างข้อความได้.'
        }


class QuestionScaleForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['text']
        labels = {
            'text': 'Question',
        }


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['text', 'score']
        labels = {
            'text': 'Question',
        }


class QuestionMutipleSelectForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['text', 'max_select']
        labels = {
            'text': 'Question',
        }


class QuestionFillDescriptionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['text', 'score']
        labels = {
            'text': 'Question',
        }


class ChoiceForm(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ['text']
        labels = {
            'text': 'Choice',
        }


class ChoiceMutipleChoiceForm(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ['text', 'is_answer']
        labels = {
            'text': 'Choice',
        }
