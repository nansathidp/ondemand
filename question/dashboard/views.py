import inspect
from functools import wraps

from django.shortcuts import render, get_object_or_404, redirect
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.conf import settings

from dashboard.views_decorator import filter_param
from ..models import Activity
from progress.models import Content as ProgressContent
from provider.models import Provider
from category.models import Category

from utils.paginator import paginator
from analytic.cached import cached_analytic_stat
from dashboard.views import condition_view

from utils.filter import get_q
from utils.content_filter import filter_all

def check_access(view_func):
    def _decorator(request, *args, **kwargs):
        activity_id = args[0]
        request.activity = get_object_or_404(Activity, id=activity_id)
        if not request.activity.check_access(request):
            raise PermissionDenied
        response = view_func(request, *args, **kwargs)
        return response
    return wraps(view_func)(_decorator)


@filter_param
def home_view(request):
    activity_list = Activity.access_list(request)
    if activity_list is None:
        raise PermissionDenied
    elif activity_list == []:
        return condition_view(request)

    activity_list = paginator(request, activity_list)
        
    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Test Management'}
    ]
    return render(request,
                  'question/dashboard/home.html',
                  {'SIDEBAR': 'question',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'activity_list': activity_list,
                   'q_name': request.q_name,
                   'q_provider': request.q_provider,
                   'q_provider_list': request.q_provider_list,
                   'q_category': request.q_category,
                   'q_category_list': request.q_category_list})


def pull_breadcrumb(activity=None, section=None):
    breadcrumb_list = [{'is_active': False,
                        'title': 'Test Management',
                        'url': reverse('dashboard:question:home')}]
    caller = inspect.stack()[1][3]
    if caller == 'detail_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Information'})
    elif caller == 'create_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Create'})
    elif caller == 'section_detail_view':
        breadcrumb_list.append({'is_active': False,
                                'title': 'Information',
                                'url': reverse('dashboard:question:detail', args=[activity.id])})
        breadcrumb_list.append({'is_active': True,
                                'title': 'Section %s: %s'%(section.sort, section.name)})
    elif caller == 'report_view':
        breadcrumb_list.append({'is_active': False,
                                'title': 'Information',
                                'url': reverse('dashboard:question:detail', args=[activity.id])})
        breadcrumb_list.append({'is_active': True,
                                'title': 'Section %s: %s'%(section.sort, section.name)})
    elif caller == 'dependency_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Dependency'})
    elif caller == 'time_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Period & Quota'})
    elif caller == 'progress_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Progress'})
    elif caller == 'score_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Verifying Test Score'})
    elif caller == 'score_detail_view':
        breadcrumb_list.append({'is_active': False,
                                'title': 'Verifying Test Score',
                                'url': reverse('dashboard:question:score', args=[activity.id])})
        breadcrumb_list.append({'is_active': True,
                                'title': 'Fill-in Score'})
    return breadcrumb_list
