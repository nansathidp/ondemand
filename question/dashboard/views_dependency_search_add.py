from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from dependency.models import Dependency
from ..models import Activity

from utils.content import get_content

@csrf_exempt
def dependency_search_add_view(request, activity_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_change(request):
        raise PermissionDenied

    content_type = settings.CONTENT_TYPE('question', 'activity')
    dependency = Dependency.pull(content_type,
                                 activity.id)
    if request.method == 'POST':
        try:
            content_type_id = int(request.POST.get('content_type'))
            content_id = int(request.POST.get('content'))
            condition = int(request.POST.get('condition'))

            content = get_content(content_type_id, content_id)
            if condition in [1, 2] and content is not None:
                dependency.add_parent(settings.CONTENT_TYPE_ID(content_type_id),
                                      content.id,
                                      condition)
        except:
            pass

    parent_list = Dependency.pull_parent(content_type, activity.id)
    return render(request,
                  'question/dashboard/dependency_block.html',
                  {'activity': activity,
                   'parent_list': parent_list})
