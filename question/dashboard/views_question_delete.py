from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Activity, Section, Question


def question_delete_view(request, activity_id, section_id, question_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied

    section = get_object_or_404(Section, activity=activity, id=section_id)
    question = get_object_or_404(Question, section=section, id=question_id)
    question.delete()
    section.update_score()
    return redirect('dashboard:question:section_detail', activity.id, section.id)
