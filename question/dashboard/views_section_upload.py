import os
import shutil
import uuid

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from utils.resumable import Resumable
from ..models import Activity, Section


def _complete(resumable, section, path):
    storage = FileSystemStorage(location=os.path.join(settings.BASE_DIR, 'upload'))
    filename = '%s.mp4' % section.data
    if os.path.isfile('%s/%s' % (os.path.join(settings.BASE_DIR, 'upload'), filename)):
        os.remove('%s/%s' % (os.path.join(settings.BASE_DIR, 'upload'), filename))
    storage.save(filename, resumable)
    resumable.delete_chunks()
    shutil.rmtree(path)
    section.data_status = 1
    section.save(update_fields=['data_status'])


@csrf_exempt
def section_upload_view(request, activity_id, section_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied

    section = get_object_or_404(Section, activity=activity, id=section_id)
    if len(section.data) == 0:
        section.data = str(uuid.uuid4())
        section.save(update_fields=['data'])
        
    if request.method == 'POST':
        chunk = request.FILES.get('file')
        path = os.path.join(settings.BASE_DIR, 'upload', section.data)
        if not os.path.isdir(path):
            os.makedirs(path)
        resumable = Resumable(path, request.POST.get('resumableFilename'), request.POST)
        if resumable.chunk_exists:
            return HttpResponse('chunk already exists')
        resumable.process_chunk(chunk)
        if resumable.is_complete:
            _complete(resumable, section, path)
            if settings.QUESTION_INFORMATION_UPLOAD_TYPE is None:
                section.data_status = 2
                section.save(update_fields=['data_status'])
        else:
            if section.data_status != 0:
                section.data_status = 0
                section.save(update_fields=['data_status'])
        return HttpResponse()
    else:
        path = os.path.join(settings.BASE_DIR, 'upload', section.data)
        resumable = Resumable(path, request.GET.get('resumableFilename'), request.GET)
        if not (resumable.chunk_exists or resumable.is_complete):
            return HttpResponse('chunk not found', status=404)
        else:
            if resumable.is_complete:
                _complete(resumable, section, path)
            return HttpResponse('chunk already exists')
