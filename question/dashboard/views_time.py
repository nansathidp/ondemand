from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse

from question.cached import cached_question_update
from ..models import Activity
from .views import check_access, pull_breadcrumb

import datetime
from utils.duration import duration

@check_access
def time_view(request, activity_id):
    activity = request.activity

    if request.method == 'POST':
        if 'expired' in request.POST:
            if request.POST['expired'] == 'unlimited':
                activity.expired = datetime.timedelta(0)
            else:
                activity.expired = duration(request,
                                            'expired-day',
                                            'expired-hour',
                                            'expired-minute',
                                            None) #'expired-second')

        if 'limit' in request.POST:
            if request.POST['limit'] == 'unlimited':
                activity.time_limit = datetime.timedelta(0)
            else:
                activity.time_limit = duration(request,
                                               'limit-day',
                                               'limit-hour',
                                               'limit-minute',
                                               None) #'limit-second')
            
        try:
            _ = int(request.POST.get('max-submit'))
            activity.max_submit = 1 if _ < 0 else _
        except:
            activity.max_submit = 1
        activity.save(update_fields=['expired', 'time_limit', 'max_submit'])
        cached_question_update(activity)
        
    return render(request,
                  'question/dashboard/time.html',
                  {'SIDEBAR': 'question',
                   'TAB': 'time',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'activity': activity})
