from django.shortcuts import render, redirect
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.conf import settings

from content.models import Location as ContentLocation

from .forms import ActivityForm

from .views import pull_breadcrumb
from utils.crop import crop_image


def create_view(request):
    if not request.user.has_perm('question.add_activity',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    if request.method == 'POST':
        if request.user.has_perm('question.view_activity',
                                 group=request.DASHBOARD_GROUP):
            activity_form = ActivityForm(None, request.POST, request.FILES)
        elif request.user.has_perm('question.view_own_activity',
                                   group=request.DASHBOARD_GROUP):
            activity_form = ActivityForm(request, request.POST, request.FILES)
        else:
            raise PermissionDenied

        if activity_form.is_valid():
            activity = activity_form.save()
            crop_image(activity, request)
            content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                          activity.id)
            if settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                request.DASHBOARD_SUB_PROVIDER.push_item(settings.CONTENT_TYPE('question', 'activity'),
                                                         activity.id)
            return redirect('dashboard:question:section', activity.id)
    else:
        if request.user.has_perm('question.view_activity',
                                 group=request.DASHBOARD_GROUP):
            activity_form = ActivityForm(None)
        elif request.user.has_perm('question.view_own_activity',
                                   group=request.DASHBOARD_GROUP):
            activity_form = ActivityForm(request)
        else:
            raise PermissionDenied

    return render(request,
                  'question/dashboard/create.html',
                  {'SIDEBAR': 'question',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'activity_form': activity_form})
