from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils import timezone

from ..models import Activity, Section
from .forms import ActivityEditForm
from ..cached import cached_question_update
from .views import check_access, pull_breadcrumb
from category.job.count_content import _count_content
import os, uuid

from utils.crop import crop_image

import django_rq

@check_access
def detail_view(request, activity_id):
    activity = request.activity
    
    is_change = activity.check_change(request)

    old_category = activity.category_id
    if request.method == 'POST':
        if not is_change:
            raise PermissionDenied

        if request.user.has_perm('question.view_activity', group=request.DASHBOARD_GROUP):
            activity_form = ActivityEditForm(None, request.POST, request.FILES, instance=activity)
        elif request.user.has_perm('question.view_own_activity', group=request.DASHBOARD_GROUP):
            activity_form = ActivityEditForm(request, request.POST, request.FILES, instance=activity)
        else:
            raise PermissionDenied

        if activity_form.is_valid():
            activity = activity_form.save()
            crop_image(activity, request)
            # if activity.category_id != old_category:
            #     django_rq.enqueue(_count_content, old_category)
            #     django_rq.enqueue(_count_content, activity.category_id)
            if 'document' in request.FILES:
                try:
                    t = request.FILES['document'].name.split('.')[-1]
                    now = timezone.now()
                    p = 'activity/document/%s-%02d' % (now.year, now.month)
                    path = os.path.join(settings.MEDIA_ROOT, 'activity', 'document', '%s-%02d'%(now.year, now.month))
                    if not os.path.isdir(path):
                        os.makedirs(path)
                    f = '%s.%s' % (uuid.uuid4(), t)
                    filename = '%s/%s' % (path, f)
                    with open(filename, 'wb+') as destination:
                        for chunk in request.FILES['document'].chunks():
                            destination.write(chunk)
                    activity.data = '%s/%s' % (p, f)
                    activity.data_status = 2
                    activity.save(update_fields=['data', 'data_status'])
                except:
                    pass
            cached_question_update(activity)

    if request.user.has_perm('question.view_activity',
                             group=request.DASHBOARD_GROUP):
        activity_form = ActivityEditForm(None, instance=activity)
    elif request.user.has_perm('question.view_own_activity',
                               group=request.DASHBOARD_GROUP):
        activity_form = ActivityEditForm(request, instance=activity)
    else:
        raise PermissionDenied

    return render(request,
                  'question/dashboard/information.html',
                  {'SIDEBAR': 'question',
                   'TAB': 'detail',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'SERVER_UPLOAD_URL': settings.SERVER_UPLOAD_URL,
                   'activity': activity,
                   'activity_form': activity_form,
                   'is_change': is_change,
                   'section_type_choices': Section.TYPE_CHOICES})
