from django.conf import settings
from django.core import serializers

from log.models import Log


def _provider(request):
    if request.DASHBOARD_PROVIDER is not None:
        return request.DASHBOARD_PROVIDER.id
    else:
        return -1


def _sub_provider(request):
    if settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
        return request.DASHBOARD_SUB_PROVIDER.id
    else:
        return -1


def log_section_create(request, section):
    Log.push('section_create',
             _provider(request),
             _sub_provider(request),
             request.user,
             None,
             '',
             settings.CONTENT_TYPE('question', 'activity'),
             section.activity_id,
             '',
             serializers.serialize('json', [section]))
