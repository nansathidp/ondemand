from django.shortcuts import render
from django.conf import settings

from dependency.models import Dependency
from .views import check_access, pull_breadcrumb

@check_access
def dependency_view(request, activity_id):
    activity = request.activity
    parent_list = Dependency.pull_parent(settings.CONTENT_TYPE('question', 'activity'),
                                         activity.id)
    return render(request,
                  'question/dashboard/dependency.html',
                  {'SIDEBAR': 'question',
                   'TAB': 'dependency',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'activity': activity,
                   'parent_list': parent_list})
