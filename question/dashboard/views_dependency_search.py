from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from dashboard.views import paginator
from dependency.models import Dependency, Parent as DependencyParent
from ..models import Activity


@csrf_exempt
def dependency_search_view(request, activity_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied

    q_name = None
    type = None
    content_type = settings.CONTENT_TYPE('question', 'activity')
    dependency = Dependency.pull(content_type, activity.id)
    parent_list = Dependency.pull_parent(content_type,
                                         activity.id)
    if request.method == 'POST':
        type = request.POST.get('type')
        q_name = request.POST.get('q_name', '')
        content_id = request.POST.get('content_id', '')
        content_type, content_list = Dependency.get_content_list(type)
        if content_list is None:
            content_list = []
        else:
            if q_name:
                content_list = content_list.filter(name__contains=q_name)
            content_list = paginator(request, content_list)
    else:
        content_list = []
    return render(request,
                  'question/dashboard/dependency_search.html',
                  {'content_list': content_list,
                   'condition_choices': DependencyParent.CONDITION_CHOICES,
                   'activity': activity,
                   'q_name': q_name,
                   'type': type,
                   'parent_list': parent_list,
                   'content_type': content_type})
