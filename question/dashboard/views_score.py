from django.shortcuts import render
from django.conf import settings

from ..models import Score
from .views import check_access, pull_breadcrumb

@check_access
def score_view(request, activity_id):
    activity = request.activity
    score_list = Score.objects.filter(
        activity=activity,
        status=5
    )
    if settings.ACCOUNT__HIDE_INACTIVE:
        score_list = score_list.exclude(account__is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        score_list = score_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)
    return render(request,
                  'question/dashboard/score.html',
                  {'SIDEBAR': 'question',
                   'TAB': 'score',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'activity': activity,
                   'score_list': score_list})
