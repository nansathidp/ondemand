from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from question.cached import cached_question_update
from ..models import Activity, Section


def section_delete_view(request, activity_id, section_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied

    section = get_object_or_404(Section, activity=activity, id=section_id)
    section.delete()
    activity.update_max_score()
    cached_question_update(activity)
    return redirect('dashboard:question:section', activity.id)
