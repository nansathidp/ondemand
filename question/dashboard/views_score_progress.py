from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.conf import settings

from ..models import Activity, Question, Answer, Score, ScoreSection
from progress.models import Content as ProgressContent

@csrf_exempt
def score_progress_view(request, activity_id, score_item_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_change(request):
        raise PermissionDenied

    score_item = get_object_or_404(Score,
                                   activity=activity,
                                   id=score_item_id)
    score = score_item.sum_score()

    score_item.check_complete(score, False)
    score_item.push_progress()
    ProgressContent.update_progress(score_item.location_id,
                                    settings.CONTENT_TYPE('question', 'activity').id,
                                    activity.id)

    return redirect('dashboard:question:score_account', activity.id, score_item.account_id)
    #return redirect('dashboard:question:score_detail', activity.id, score_item.id)
