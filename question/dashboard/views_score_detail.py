from django.shortcuts import render, get_object_or_404

from account.models import Account
from .views import check_access, pull_breadcrumb
from ..models import Score, Answer, ScoreSection


@check_access
def score_detail_view(request, activity_id, score_id):
    activity = request.activity

    score = get_object_or_404(
        Score,
        activity=activity,
        id=score_id
    )
    score_section_list = ScoreSection.objects.filter(
        score_item=score,
        activity=activity,
        account_id=score.account_id
    )
    answer_list = Answer.objects.filter(
        score_item=score,
        question__section__activity_id=activity.id,
        account_id=score.account_id
    )
    return render(
        request,
        'question/dashboard/score_detail.html',
        {
            'SIDEBAR': 'question',
            'TAB': 'score',
            'BREADCRUMB_LIST': pull_breadcrumb(activity),
            'activity': activity,
            'account': Account.pull(score.account_id),
            'content_type_name': 'test',
            'score': score,
            'score_section_list': score_section_list,
            'answer_list': answer_list
        }
    )
