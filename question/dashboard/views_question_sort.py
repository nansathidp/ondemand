from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.forms import inlineformset_factory

from ..models import Activity, Section, Question


def question_sort_view(request, activity_id, section_id, question_id):
    activity = get_object_or_404(Activity, id=activity_id)
    if not activity.check_access(request):
        raise PermissionDenied
    
    section = get_object_or_404(Section, activity=activity, id=section_id)
    question = get_object_or_404(Question, section=section, id=question_id)
    
    move = request.GET.get('move', None)
    if move == 'up':
        question_swap = Question.objects.filter(section=section, sort__lt=question.sort).exclude(id=question.id).order_by('-sort').first()
        if question_swap is not None:
            question_swap.sort = question.sort
            question_swap.save(update_fields=['sort'])
        question.sort -= 1
        question.save(update_fields=['sort'])
    elif move == 'down':
        question_swap = Question.objects.filter(section=section, sort__gt=question.sort).exclude(id=question.id).order_by('sort').first()
        if question_swap is not None:
            question_swap.sort = question.sort
            question_swap.save(update_fields=['sort'])
        question.sort += 1
        question.save(update_fields=['sort'])
    return redirect('dashboard:question:section_detail', activity.id, section.id)
