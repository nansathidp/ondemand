from question.models import Activity, Question, Choice, Answer

def report_job(activity_id):
    try:
        activity = Activity.objects.get(id=activity_id)
    except:
        return None

    for question in Question.objects.filter(
            section__activity=activity,
            section__type=2
    ):
        choice_list = Choice.objects.filter(
            question=question
        )
        sum = 0
        answer_count = 0
        for choice in choice_list:
            choice.count =  Answer.objects.filter(choice=choice).count()
            sum += choice.count
            if choice.is_answer:
                answer_count = choice.count
        for choice in choice_list:
            try:
                choice.percent = float(choice.count) / float(sum) * 100.0
            except:
                choice.percent = 0
            choice.save()
        try:
            question.answer_score = float(answer_count) / float(sum) * 100.0
        except:
            question.answer_score = 0
        question.save()
