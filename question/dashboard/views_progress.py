from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.conf import settings

from ..models import Activity, Score
from order.models import Item as OrderItem
from progress.models import Progress

from .views import check_access, pull_breadcrumb
from utils.paginator import paginator

@check_access
def progress_view(request, activity_id):
    activity = request.activity

    content_type = settings.CONTENT_TYPE('question', 'activity')
    progress_content = None
    
    progress_list = Progress.objects.filter(content_type=content_type, content=activity.id, is_root=True)
    progress_list = paginator(request, progress_list)
    for progress in progress_list:
        #progress.get_content_cached()
        progress.item_cached = OrderItem.pull(progress.item_id)
        progress.get_account_cached()
    return render(request,
                  'question/dashboard/progress.html',
                  {'SIDEBAR': 'question',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'activity': activity,
                   'progress_content': progress_content,
                   'progress_list': progress_list})
