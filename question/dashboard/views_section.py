from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from question.cached import cached_question_update
from ..models import Activity, Section
from .views import check_access, pull_breadcrumb

@check_access
def section_view(request, activity_id):
    activity = request.activity
    
    if request.method == 'POST':
        try:
            activity.pass_score = int(request.POST.get('pass_score', 0))
            activity.save(update_fields=['pass_score'])
            cached_question_update(activity)
        except:
            pass

    return render(request,
                  'question/dashboard/section.html',
                  {'SIDEBAR': 'question',
                   'TAB': 'section',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'activity': activity,
                   'section_type_choices': Section.TYPE_CHOICES})
