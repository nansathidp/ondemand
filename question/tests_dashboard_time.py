from django.test import TestCase, RequestFactory
from django.urls import reverse
from django.core.exceptions import PermissionDenied

from .tests import dashboard_user, dashboard_activity, dashboard_provider

import datetime

class AnonymousTests(TestCase):
    def test_time(self):
        activity = dashboard_activity()
        response = self.client.get(reverse('dashboard:question:time', args=[activity.id]))
        self.assertRedirects(response, reverse('dashboard:login'))

class PermissionTests(TestCase):
    def setUp(self):
        self.client.force_login(dashboard_user(None))
        
    def test_permission_denind(self):
        activity = dashboard_activity()
        response = self.client.get(reverse('dashboard:question:time', args=[activity.id]))
        self.assertEqual(response.status_code, 403)

class ViewPermissionTests(TestCase):
    def setUp(self):
        self.client.force_login(dashboard_user('view_activity'))

    def test_access(self):
        activity = dashboard_activity()
        response = self.client.get(reverse('dashboard:question:time', args=[activity.id]))
        self.assertEqual(response.status_code, 200)
        
class OwnPermissionTests(TestCase):
    def setUp(self):
        self.user = dashboard_user('view_own_activity')
        self.client.force_login(self.user)

    def test_access(self):
        activity = dashboard_activity()
        
        response = self.client.get(reverse('dashboard:question:time', args=[activity.id]))
        self.assertEqual(response.status_code, 403)

        dashboard_provider(activity, None)
        response = self.client.get(reverse('dashboard:question:time', args=[activity.id]))
        self.assertEqual(response.status_code, 403)
        
        dashboard_provider(activity, self.user)
        response = self.client.get(reverse('dashboard:question:time', args=[activity.id]))
        self.assertEqual(response.status_code, 200)


class PostTests(TestCase):
    def setUp(self):
        self.client.force_login(dashboard_user('view_activity'))

    def test_expired(self):
        activity = dashboard_activity()
        response = self.client.post(reverse('dashboard:question:time', args=[activity.id]),
                                    {'expired': 'unlimited'})
        self.assertEqual(activity.expired, datetime.timedelta(0))

        response = self.client.post(reverse('dashboard:question:time', args=[activity.id]),
                                    {'expired': 'limited',
                                     'expired-day': 1})
        activity.refresh_from_db()
        self.assertEqual(activity.expired, datetime.timedelta(days=1))

        response = self.client.post(reverse('dashboard:question:time', args=[activity.id]),
                                    {'expired': 'limited',
                                     'expired-hour': 1})
        activity.refresh_from_db()
        self.assertEqual(activity.expired, datetime.timedelta(hours=1))

        response = self.client.post(reverse('dashboard:question:time', args=[activity.id]),
                                    {'expired': 'limited',
                                     'expired-minute': 1})
        activity.refresh_from_db()
        self.assertEqual(activity.expired, datetime.timedelta(minutes=1))

        response = self.client.post(reverse('dashboard:question:time', args=[activity.id]),
                                    {'expired': 'limited',
                                     'expired-day': -1})
        activity.refresh_from_db()
        self.assertEqual(activity.expired, datetime.timedelta(0))

        response = self.client.post(reverse('dashboard:question:time', args=[activity.id]),
                                    {'expired': 'limited',
                                     'expired-hour': -1})
        activity.refresh_from_db()
        self.assertEqual(activity.expired, datetime.timedelta(0))

        response = self.client.post(reverse('dashboard:question:time', args=[activity.id]),
                                    {'expired': 'limited',
                                     'expired-minute': -1})
        activity.refresh_from_db()
        self.assertEqual(activity.expired, datetime.timedelta(0))
