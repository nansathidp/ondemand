from django.contrib import admin

from .models import Activity, Score, Section, Question, Choice, Answer, ScoreSection

@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'is_public', 'provider', 'is_display', 'sort', 'timestamp', 'publish')

    actions = ['broadcast_image']

    @staticmethod
    def broadcast_image(self, request, queryset):
        activity = queryset.first()
        Activity.objects.all().update(image=activity.image)


@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):
    list_display = ('activity', 'name', 'score', 'sort', 'timestamp')


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('section', 'type', 'sort', 'timestamp')


@admin.register(Choice)
class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('question', 'sort', 'is_answer', 'timestamp', 'timeupdate')


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ('score_item', 'item', 'question', 'account', 'choice', 'data', 'score', 'timestamp', 'timeupdate')
    readonly_fields = ('account', 'item')


@admin.register(Score)
class ScoreAdmin(admin.ModelAdmin):
    list_display = ('id', 'item', 'activity', 'location', 'account', 'material', 'submit_number', 'score', 'status', 'timestamp', 'timeupdate')
    readonly_fields = ('account', 'item')


@admin.register(ScoreSection)
class ScoreSectionAdmin(admin.ModelAdmin):
    list_display = ('id', 'score_item', 'activity', 'section', 'account', 'score', 'status', 'timestamp', 'timeupdate')
    readonly_fields = ('account', 'item')
