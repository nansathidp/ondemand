from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist

from .models import Activity

from utils.cached.time_out import get_time_out


def cached_question(activity_id, is_force=False):
    key = '%s_question_%s' % (settings.CACHED_PREFIX, activity_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Activity.objects.select_related('category', 'provider').get(id=activity_id)
            cache.set(key, result, get_time_out())
        except ObjectDoesNotExist:
            result = -1
    return None if result == -1 else result


def cached_question_update(activity):
    key = '%s_question_%s' % (settings.CACHED_PREFIX, activity.id)
    cache.set(key, activity, get_time_out())


def cached_api_question_activity_list(is_force=False):
    key = '%s_api_question_activity_list' % settings.CACHED_PREFIX
    result = None if is_force else cache.get(key)
    if result is None:
        result = []
        for activity in Activity.objects.filter():
            result.append(activity.api_display_title())
        cache.set(key, result, get_time_out())
    return result
