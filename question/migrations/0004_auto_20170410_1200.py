# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-10 05:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('question', '0003_auto_20170207_1142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='section',
            name='data_type',
            field=models.IntegerField(choices=[(0, '---'), (1, 'Image'), (2, 'Video & Audio')], default=0),
        ),
    ]
