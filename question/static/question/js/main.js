;(function($){
    $(function(){
      $(window).on('scroll', function(){
        if ($(window).width() < 768 && $(window).scrollTop() > 100) {
          $('.content-area').addClass('is-sticky');
        }else{
          $('.content-area').removeClass('is-sticky');
        }
      });
        /**
         Skel Breakpoint
        **/
        skel.breakpoints({
            xlarge: '(min-width: 1200px)',
            large:  '(min-width: 992px)',
            medium: '(min-width: 768px)',
            small:  '(min-width: 480px)',
            xsmall: '(min-width: 300px)'
        });

        /**
        Variable
        **/
        var $time_count     = $('.site-header .time-count');
            $clock          = $('#clock');
            $sec_lv_title   = $('.footer-action .action-box .section-level');
            $form           = $('#myForm');
            $next_btn       = $('.footer-action .action-box .btn.next, .step-action .btn.next');
            $prev_btn       = $('.footer-action .action-box .btn.prev, .step-action .btn.prev');

        /**
         Match Height Fucntion
        **/
        $('.matchHeight').matchHeight();

        /**
         Countdown
        **/
    if(timeLeft > 0){
        swal({
            title: '<i class="fa fa-clock-o" style="color:#b2d234;font-size:100px"></i> <h2>เริ่มการจับเวลา!</h2>',
            text: "คุณมีเวลาในการทำแบบทดสอบ "+ timeLeft +" นาที",
            customClass: "",
            confirmButtonColor: "#b2d234",
            confirmButtonText: "ตกลง",
            closeOnConfirm: true,
            html: true},
            function(isConfirm){
                if (isConfirm) { countDownTime(); }
        });
    }

    function countDownTime(){
        $('.time-count').show();
        $count = new Date($.now() + (1000 * 60 * timeLeft));
        var tickingIntro = $("#ticking-intro");
        var tickingNoise = $("#ticking-noise");
        var ringing = $("#ringing-clock");
        function soundPause(sound,duration) {
            var duration = duration || 1000;
            sound.animate({volume: 0}, duration, 'swing', function() {
                sound[0].pause();
            });
        };

        tickingIntro[0].play();
        soundPause(tickingIntro,4000);
        $clock.countdown($count, function(event) {
            $(this).html(event.strftime(' %H : %M : %S'));
            //console.log(JSON.stringify(event));
            //console.log($count.getTime());
            if(event.timeStamp > $count.getTime()-10000){
                tickingNoise[0].play();
            }else if(event.timeStamp > $count.getTime()-58000){
                soundPause(tickingIntro,3000);
            }else if(event.timeStamp > $count.getTime()-60000){
                tickingIntro.animate({volume: 1}, 1000, 'swing', function() {
                    tickingIntro[0].play();
                });
                $('#clock').css({'color':'red'});
            }
            if(event.type=="finish"){
                soundPause(tickingNoise);
                ringing[0].play();
                setTimeout(function(){
                    soundPause(ringing);
                    swal({
                        title: "หมดเวลา!",
                        text: "คุณจำเป็นต้องส่งคำตอบ",
                        type: "warning",
                        customClass: "",
                        confirmButtonColor: "#b2d234",
                        confirmButtonText: "ส่งคำตอบ",
                        showLoaderOnConfirm: true},
                        function(isConfirm){
                            if (isConfirm) { $form.trigger('submit'); }
                        });
                },3000);
            }
        });
    };

        /**
         Back Top Button
        **/
        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 800) {
                    $('#back-top').addClass('active');
                } else {
                    $('#back-top').removeClass('active');
                }
            });

            // scroll body to 0px on click
            $('#back-top').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 900);
                return false;
            });
        });

        /**
         Lightbox
        **/
        $('.entry-content').find('a[href*=".jpg"], a[href*=".jpeg"], a[href*=".png"], a[href*=".gif"]').each(function(){
            $(this).fluidbox({
                stackIndex: 1005,
            });
        });


        /**
         iCheck
        **/
        $('input:not(.radio-btn)').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
            increaseArea: '20%'
        });

        /**
         Button Group Active Class
        **/
        $(".btn-group").find(':input:checked').parent('.btn').addClass('active');

        /**
         Form Validator Option
        **/
        var $top_space = $('.site-header').outerHeight() + 70;
        var $validator = $form.validate({
            errorElement: "p",
            errorClass: "form-error",
            errorPlacement: function(error, element) {
                error.appendTo(element.parents('.form-group:not(".inputs-group")'));
            },
            highlight: function(element, errorClass, validClass) {
                $(element).parents('.form-group').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.form-group').removeClass(errorClass).addClass(validClass);
            },
            focusInvalid: false,
            invalidHandler: function(form, validator) {
                if (!validator.numberOfInvalids())
                    return;
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top -$top_space
                }, 500);
            }
        });

        /**
         Form Validator Thai Language
        **/
        $.extend( $.validator.messages, {
            required: "โปรดระบุฟิลด์นี้ด้วย",
            remote: "โปรดแก้ไขให้ถูกต้อง",
            email: "โปรดระบุที่อยู่อีเมล์ที่ถูกต้อง",
            url: "โปรดระบุ URL ที่ถูกต้อง",
            date: "โปรดระบุวันที่ ที่ถูกต้อง",
            dateISO: "โปรดระบุวันที่ ที่ถูกต้อง (ระบบ ISO).",
            number: "โปรดระบุทศนิยมที่ถูกต้อง",
            digits: "โปรดระบุจำนวนเต็มที่ถูกต้อง",
            creditcard: "โปรดระบุรหัสบัตรเครดิตที่ถูกต้อง",
            equalTo: "โปรดระบุค่าเดิมอีกครั้ง",
            extension: "โปรดระบุค่าที่มีส่วนขยายที่ถูกต้อง",
            maxlength: $.validator.format( "โปรดอย่าระบุค่าที่ยาวกว่า {0} อักขระ" ),
            minlength: $.validator.format( "โปรดอย่าระบุค่าที่สั้นกว่า {0} อักขระ" ),
            rangelength: $.validator.format( "โปรดอย่าระบุค่าความยาวระหว่าง {0} ถึง {1} อักขระ" ),
            range: $.validator.format( "โปรดระบุค่าระหว่าง {0} และ {1}" ),
            max: $.validator.format( "โปรดระบุค่าน้อยกว่าหรือเท่ากับ {0}" ),
            min: $.validator.format( "โปรดระบุค่ามากกว่าหรือเท่ากับ {0}" )
        } );


        /**
         Bootstrap 3 Wizard
        **/
        $form.bootstrapWizard({
            'nextSelector': $next_btn,
            'previousSelector': $prev_btn,
            'withVisible': false,
            'onInit': function(tab, navigation, index) {
                var $total   = navigation.find('li').length;
                $('.entry-header > .step-action > h4 > .total, .footer-action .action-box .section-level > .total').html($total);

                $('.site-main').removeClass('loading');
            },
            'onTabShow': function(tab, navigation, index) {
                var $current = index + 1;
                var $total   = navigation.find('li').length;
                $('.entry-header > .step-action > h4 > .current, .footer-action .action-box .section-level > .current').html($current);
                if($current >= $total) {
                    $form.find($next_btn).hide();
                    $form.find('.footer-action .action-box .btn.btn-finish, .entry-header > .step-action .btn.btn-finish').show();
                    $form.find('.footer-action .action-box .btn.btn-finish, .entry-header > .step-action .btn.btn-finish').removeClass('disabled');
                } else {
                    $form.find($next_btn).show();
                    $form.find('.footer-action .action-box .btn.btn-finish, .entry-header > .step-action .btn.btn-finish').hide();
                }

                //Move the scrollbar back to the beginning(top of the page)
                $('body').animate({ scrollTop: 0 }, 0);

                /**
                 Skel
                **/
                skel.on("+medium", function() {
                    $('.sticky').unstick();
                    $(".sticky").each(function(){
                        $(this).sticky({ topSpacing: true });
                    });
                    $('.sticky').sticky('update');
                });
            },
            'onNext': function(tab, navigation, index, nextSelector) {
                if(!$form.valid()) {
                    return false;
                }
            },
        });

        /**
         Finish Button Popup
        **/
        $form.find('.btn.btn-finish').click(function() {
            if($form.valid()) {
                swal({
                    title: "คุณต้องการที่จะส่งคำตอบ ?",
                    text: "หลังจากส่งคำตอบแล้ว คุณจะไม่สามารถกลับมาแก้ไขได้อีก",
                    type: "warning",
                    customClass: "",
                    showCancelButton: true,
                    confirmButtonColor: "#b2d234",
                    confirmButtonText: "ส่งคำตอบ",
                    cancelButtonText: "ยกเลิก",
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    showLoaderOnConfirm: true},
                    function(isConfirm){
                        if (isConfirm) { $form.trigger('submit'); }
                });
            }
        });

        /**
         Simple Ajax Call
        **/
        $form.on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function (data) {
                    setTimeout(function(){
                        if(data.result.is_success){
                            topic = "เรียบร้อย"
                            messages = "<h4>คุณสามารถดูผลการทดสอบได้ที่หน้าทดสอบ</h4> <p>"+ data.result.account +"</p> <p>ทดสอบเมื่อ : "+ data.result.date +"</p>"
                            type = "success"
                        }else{
                            topic = "เกิดข้อผิดพลาด"
                            messages = "<h4>ไม่สามารถส่งแบบทดสอบได้</h4>"
                            type = "error"
                        }
                        swal({
                            title: topic,
                            text: messages,
                            type: type,
                            html: true,
                            customClass: "",
                            confirmButtonColor: "#b2d234",
                            confirmButtonText: "OK",
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true},
                            function(isConfirm){
                            if (isConfirm) {
                                window.onbeforeunload = function () {};
                                setTimeout(function(){
                                    location.href = data.result.redirect_url
                                }, 1000);
                            }
                        });
                    }, 500);
                },
                error: function() {
                    setTimeout(function(){
                        swal({
                            title: "ผิดพลาด",
                            customClass: "",
                            text: "ไม่สามารถส่งฟอร์มได้ กรุณาลองใหม่",
                            type: "error"
                        });
                    }, 500);
                }
            });
        });

        /**
         Sticky Header
        **/
        var $h_height;
        $(".site-header").sticky({
            topSpacing:0,
            wrapperClassName: 'sticky-wrapper header-wrapper',
            className: 'is-sticky header-is-sticky'
        });
        $('.site-header').on('sticky-start', function() {
            $h_height = $(this).outerHeight();
            $(this).parent().removeClass('header-wrapper');
        });
        $('.site-header').on('sticky-end', function() {
          $(this).parent().css('height', 'auto');
          $(this).parent().addClass('header-wrapper');
        });


        /**
         Skel
        **/
        skel
        .on("ready", function() {
            /* do DOM ready stuff */

            if (skel.vars.touch) {
                /* enable feature for devices with a touchscreen */
            }

            if (skel.vars.IEVersion < 9) {
                /* apply workaround for IE<9 */
            }

        }).on("+medium", function() {
            $time_count.detach().insertAfter(".site-header .entry-header");
            $sec_lv_title.detach().insertAfter(".footer-action .action-box .btn-link");
            setTimeout(function(){
                $('.sticky').unstick();
                $(".sticky").each(function(){
                    $(this).sticky({ topSpacing: true });
                });
                $('.sticky').sticky('update');
            }, 100);
        });

    });
})(jQuery);
