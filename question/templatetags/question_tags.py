from django import template

from ..models import Score

register = template.Library()

@register.simple_tag
def answer_tag(account, question, answer_list):
    for answer in answer_list:
        if answer.account_id == account.id and answer.question_id == question.id:
            return answer.data
    return '-'    

@register.simple_tag
def answer_choice_tag(account, choice, answer_list):
    for answer in answer_list:
        if answer.account_id == account.id and answer.choice_id == choice.id:
            return answer.data
    return '-'

@register.simple_tag
def score_section_tag(section, score_section_list):
    for score_section in score_section_list:
        if section.id == score_section.section_id:
            return '%s/%s'%(score_section.score, score_section.max_score)
    return 0

#use as in teplates
@register.simple_tag
def score_answer_tag(question, answer_list):
    for answer in answer_list:
        if answer.question_id == question.id:
            return answer
    return None

@register.simple_tag
def score_latest_tag(content_location, score_list):
    for score in score_list:
        if score.location_id == content_location.id:
            return score
    return None

@register.simple_tag
def score_tag(content_location, submit_number, score_list):
    for score in score_list:
        if score.location_id == content_location.id and score.submit_number == submit_number:
            return score
    return None

@register.simple_tag
def score_section_tag2(section, score_section_list):
    for score_section in score_section_list:
        if section.id == score_section.section_id:
            return score_section
    return None

@register.simple_tag
def score_location_tag(order_item, activity, account, content_location):
    return Score.objects.filter(item=order_item,
                                activity=activity,
                                account=account,
                                location=content_location) \
                        .order_by('submit_number')
