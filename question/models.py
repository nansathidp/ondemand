from django.db import models


class Activity(models.Model):
    import datetime

    TYPE_CHOICES = (
        (1, 'Question'),
        # (2, 'Video'),
        # (3, 'Document'),
        # (4, 'Web Link'),
    )

    DATA_STATUS_CHOICES = (
        (0, '---'),
        (1, 'Wait'),
        (2, 'Complete'),
    )

    name = models.CharField(max_length=120)
    image = models.ImageField(upload_to='activity/image/', null=True, blank=True)
    type = models.IntegerField(choices=TYPE_CHOICES)
    category = models.ForeignKey('category.Category')

    provider = models.ForeignKey('provider.Provider', null=True, on_delete=models.SET_NULL)

    desc = models.TextField(blank=True)
    overview = models.TextField(blank=True)
    content = models.TextField(blank=True)
    instruction = models.TextField(blank=True)
    condition = models.TextField(blank=True)
    data = models.CharField(max_length=120, blank=True)  # For Type [2, 3, 4]
    data_status = models.IntegerField(choices=DATA_STATUS_CHOICES, default=0, db_index=True)

    time_limit = models.DurationField(default=datetime.timedelta(0))
    expired = models.DurationField(default=datetime.timedelta(0))

    is_public = models.BooleanField(default=True, db_index=True)
    is_show_answer = models.BooleanField(default=False)
    is_display = models.BooleanField(default=True, db_index=True)

    sort = models.IntegerField(db_index=True, default=0)
    publish = models.DateTimeField(null=True, blank=True, db_index=True)

    max_score = models.IntegerField(default=0)
    max_submit = models.IntegerField(default=1)
    pass_score = models.IntegerField(default=0)

    extra = models.TextField(blank=True)

    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    timeupdate = models.DateTimeField(auto_now=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')
        ordering = ['sort']

    def __str__(self):
        return self.name

    @staticmethod
    def pull(id):
        from .cached import cached_question
        return cached_question(id)

    @staticmethod
    def access_list(request):
        from django.conf import settings
        from provider.models import Provider
        from category.models import Category
        from django.core.exceptions import PermissionDenied

        from utils.filter import get_q_dashboard

        request.q_provider_list = Provider.pull_list()
        request.q_category_list = Category.pull_list()
        request.q_provider = get_q_dashboard(request, 'provider', 'int')

        if request.user.has_perm('question.view_activity', group=request.DASHBOARD_GROUP):
            content_list = Activity.objects.all()
            if request.q_provider:
                content_list = content_list.filter(provider_id=request.q_provider)
        elif request.user.has_perm('question.view_own_activity', group=request.DASHBOARD_GROUP):
            request.q_provider_list = None
            if request.DASHBOARD_PROVIDER is not None:
                content_list = Activity.objects.filter(provider=request.DASHBOARD_PROVIDER)
            elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                from subprovider.models import Item
                content_list = Activity.objects.filter(id__in=Item.objects
                                                       .values_list('content', flat=True)
                                                       .filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                                               content_type=settings.CONTENT_TYPE('question',
                                                                                                  'activity')))
            else:
                content_list = Activity.objects.none()
        else:
            # content_list = Activity.objects.none()
            raise PermissionDenied

        request.q_name = " ".join(get_q_dashboard(request, 'name', 'str').split())
        if request.q_name != '':
            content_list = content_list.filter(name__icontains=request.q_name)
        request.q_category = get_q_dashboard(request, 'category', 'int')
        if request.q_category:
            content_list = content_list.filter(category=request.q_category)
        return content_list

    def check_access(self, request):
        from django.conf import settings
        if not request.user.is_authenticated:
            return False
        if request.user.has_perm('question.view_activity',
                                 group=request.DASHBOARD_GROUP):
            return True
        elif request.user.has_perm('question.view_own_activity',
                                   group=request.DASHBOARD_GROUP):
            if self.provider is None:
                return False
            if self.provider.account_set.filter(account=request.user).exists():
                return True
            if settings.IS_SUB_PROVIDER:
                from subprovider.models import Item
                if request.DASHBOARD_SUB_PROVIDER is None:
                    return False
                else:
                    return Item.objects.filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                               content_type=settings.CONTENT_TYPE('question', 'activity'),
                                               content=self.id).exists()
            else:
                return False
        else:
            return False

    def check_change(self, request):
        from django.conf import settings
        if request.user.has_perm('question.change_activity',
                                 group=request.DASHBOARD_GROUP):
            if request.user.has_perm('question.view_activity',
                                     group=request.DASHBOARD_GROUP):
                return True
            elif request.user.has_perm('question.view_own_activity',
                                       group=request.DASHBOARD_GROUP):
                if self.provider.account_set.filter(account=request.user).exists():
                    return True
                if settings.IS_SUB_PROVIDER:
                    from subprovider.models import Item
                    if request.DASHBOARD_SUB_PROVIDER is None:
                        return False
                    else:
                        return Item.objects.filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                                   content_type=settings.CONTENT_TYPE('question', 'activity'),
                                                   content=self.id).exists()
                else:
                    return False
            else:
                return False
        else:
            return False

    def api_display_title(self, store=1, install_version=0.0, store_version=0.0):
        from django.conf import settings

        result = {
            'id': self.id,
            'name': self.name,
            'image': self.image.url if bool(self.image) else None,
            'content': self.content,
            'type': self.type,
            'type_display': self.get_type_display(),
            'pass_score': self.pass_score,
            'percent': 0,
            'category': {
                'id': self.category_id,
                'name': self.category.name
            }
        }

        if self.type == 2:  # Video
            if self.data_status == 2:
                result['data'] = '%s%s.mp4' % (settings.SERVER_UPLOAD_URL, self.data)
            else:
                result['data'] = None
        elif self.type == 3:  # Document
            if self.data_status == 2:
                result['data'] = '%s%s' % (settings.MEDIA_URL, self.data)
            else:
                result['data'] = None
        elif self.type == 4:  # WebLink
            result['data'] = self.data
        return result

    def api_detail_display(self, store=1, install_version=0.0, store_version=0.0):
        from django.conf import settings
        from price.models import Price
        from app.views import update_checkout_type

        price = Price.pull(settings.CONTENT_TYPE('question', 'activity'), self.id, store)
        price_set = price.api_display()

        result = {
            'id': self.id,
            'name': self.name,
            'image': self.image.url if bool(self.image) else None,
            'overview': self.overview,
            'content': self.content,
            'desc': self.desc,
            'condition': self.condition,
            'instruction': self.instruction,
            'type': self.type,
            'type_display': self.get_type_display(),
            'percent': 0,
            'category': None if self.category is None else self.category.api_display_title(),
            'is_show_answer': self.is_show_answer,
            'max_score': self.max_score,
            'max_submit': self.max_submit,
            'pass_score': '-' if self.pass_score == 0 else self.pass_score,
            'expired': self.expired.days,
            'expired_display': self.get_expired_display(),
            'price_set': price_set,
            'time_limit': self.time_limit.total_seconds(),
            'section_list': []
        }
        update_checkout_type(result, install_version, store_version)
        for section in self.section_set.all():
            result['section_list'].append(section.api_display_title())
        return result

    def api_display(self, is_show_answer=False):
        result = {'name': self.name,
                  'image': self.image.url if bool(self.image) else None,
                  'type': self.type,
                  'type_display': self.get_type_display(),
                  'section_list': []}
        for section in self.section_set.all():
            result['section_list'].append(section.api_display(is_show_answer=is_show_answer))
        return result

    def is_submit(self, order_item, content_location):
        count = self.score_set.filter(item=order_item,
                                      location=content_location,
                                      status__in=[2, 3, 5]).count()

        if count < self.max_submit or self.max_submit is 0:
            return True
        else:
            return False

    def get_time_limit(self):
        return self.time_limit.total_seconds() / 60

    def is_publish(self):
        from django.utils import timezone
        if not self.is_display:
            return False
        elif self.publish is None:
            return False
        elif self.publish <= timezone.now():
            return True
        else:
            return False

    def calculation_scale(self, section):
        score = 0
        for question in section.question_set.filter(type=1):
            question.answer_count = 0
            question.answer_count_passive = 0
            question.answer_count_detractor = 0
            for answer in Answer.objects.filter(question=question):
                try:
                    ans = int(answer.data)
                    if 9 <= ans <= 10:
                        question.answer_count += 1
                    elif 7 <= ans <= 8:
                        question.answer_count_passive += 1
                    elif 0 <= ans <= 6:
                        question.answer_count_detractor += 1
                except:
                    pass
            m = float(question.answer_count + question.answer_count_passive + question.answer_count_detractor)
            try:
                question.answer_score = (float(question.answer_count) / m * 100.00) - (
                float(question.answer_count_detractor) / m * 100)
            except:
                question.answer_score = 0.0
            question.save(
                update_fields=['answer_score', 'answer_count', 'answer_count_passive', 'answer_count_detractor'])
        return score

    def cal_t2_mutiple_choice(self, score_item, section):
        choice_list = Choice.objects.select_related('question') \
            .filter(question__section=section,
                    is_answer=True)
        answer_list = Answer.objects.filter(score_item=score_item,
                                            question__section=section)

        score = 0
        for choice in choice_list:
            for answer in answer_list:
                if choice.id == answer.choice_id:
                    score += choice.question.score
        ScoreSection.push(score_item, section, score, 2)
        return score

    # Remove Count stat in Question, Choice
    """
    def calculation_mutiple_choice(self, section, account, order_item, score_item):
        from account.models import Account
        score = 0
        for question in section.question_set.filter(type=2):
            choice_list = list(question.choice_set.all())
            answer_list = list(question.answer_set.filter(item=order_item))
            for choice in choice_list:
                choice.count = 0
            n = 0
            for choice in choice_list:
                for answer in answer_list:
                    if choice.id == answer.choice_id:
                        choice.count += 1
                        if choice.is_answer:
                            n += 1
                            if answer.account_id == account.id:
                                score += question.score
            m = Account.objects.filter(answer__question=question).distinct().count()
            for choice in choice_list:
                try:
                    choice.percent = float(choice.count)/float(m)*100.0
                except:
                    choice.percent = 0.0
                choice.save(update_fields=['count', 'percent'])
            if question.type == 2:
                try:
                    question.answer_score = float(n)/float(m)*100.0
                    question.save(update_fields=['answer_score'])
                except:
                    pass
        ScoreSection.push(score_item, self, section, account, order_item, score, status=2)
        return score
    """

    def calculation_mutiple_select(self, section, account, order_item):
        from account.models import Account
        score = 0
        for question in section.question_set.filter(type=2):
            choice_list = list(question.choice_set.all())
            answer_list = list(question.answer_set.all())
            for choice in choice_list:
                choice.count = 0
            n = 0
            for choice in choice_list:
                for answer in answer_list:
                    if choice.id == answer.choice_id:
                        choice.count += 1
                        if choice.is_answer:
                            n += 1
            m = Account.objects.filter(answer__question=question).distinct().count()
            for choice in choice_list:
                try:
                    choice.percent = float(choice.count) / float(m) * 100.0
                except:
                    choice.percent = 0.0
                choice.save(update_fields=['count', 'percent'])
            if question.type == 2:
                try:
                    question.answer_score = float(n) / float(m) * 100.0
                    question.save(update_fields=['answer_score'])
                except:
                    pass
        return score

    def calculation_score(self, score_item):
        import django_rq
        from question.dashboard.jobs import report_job

        score = 0
        is_verifying = False
        for section in self.section_set.all():
            if section.type == 1:  # Scale
                self.calculation_scale(section)
            elif section.type == 2:  # Mutiple Choice
                score += self.cal_t2_mutiple_choice(score_item, section)
                # score += self.calculation_mutiple_choice(section, account, order_item, score_item)

            elif section.type == 3:  # Mutiple Select
                pass
                # FIXME : FIX CAL SOCRE
                # score += self.calculation_mutiple_select(section, account, order_item)
            elif section.type == 4:  # Fill Description
                if section.score > 0:
                    is_verifying = True
                    status = 5
                    score = 0
                else:
                    status = 2
                    score = 0
                ScoreSection.push(score_item,
                                  section,
                                  score,
                                  status)
        django_rq.enqueue(report_job, self.id)

        score_item.check_complete(score, is_verifying)
        score_item.push_progress()
        return score_item

    def progress_count(self):
        result = {
            'content_count': 1,
            'section_count': self.section_set.count(),
            'material_count': Question.objects.filter(section__activity=self).count()
        }
        return result

    def update_max_score(self):
        from django.db.models import Sum
        from .cached import cached_question_update

        try:
            self.max_score = int(self.section_set.aggregate(Sum('score'))['score__sum'])
        except:
            self.max_score = 0
        self.save(update_fields=['max_score'])
        cached_question_update(self)

    def get_expired_hour(self):
        s = self.expired.total_seconds()
        d, s = divmod(s, 60 * 60 * 24)
        return int(s / (60 * 60))

    def get_expired_minute(self):
        s = self.expired.total_seconds()
        h, s = divmod(s, 60 * 60)
        return int(s / 60)

    def get_time_limit_hour(self):
        s = self.time_limit.total_seconds()
        d, s = divmod(s, 60 * 60 * 24)
        return int(s / (60 * 60))

    def get_time_limit_minute(self):
        s = self.time_limit.total_seconds()
        h, s = divmod(s, 60 * 60)
        return int(s / 60)

    def get_submit_list(self):
        return range(1, self.max_submit + 1)

    def get_expired_display(self):
        # Fix CIMB 2017-10-24
        from utils.duration import duration_display
        if self.expired.total_seconds() > 0:
            return duration_display(self.expired)
        else:
            return 'Unlimited'

#
# class Location(models.Model):
#     from django.conf import settings
#
#     activity = models.ForeignKey(Activity)
#     item = models.ForeignKey('order.Item', related_name='+')
#     account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='+')
#     program = models.ForeignKey('program.Program', null=True)
#     course = models.ForeignKey('course.Course', null=True)
#
#     @staticmethod
#     def pull(activity, order_item, material):
#         from django.conf import settings
#         from program.models import Program
#         from course.models import Course
#         location = Location.objects.filter(activity=activity,
#                                            item=order_item) \
#                                    .first()
#         if location is None:
#             if order_item.content_type_id == settings.CONTENT_TYPE('program', 'program'):
#                 program = Program.pull(order_item.content)
#             else:
#                 program = None
#             if order_item.content_type_id == settings.CONTENT_TYPE('course', 'course'):
#                 course = Course.pull(order_item.content)
#             elif material is not None:
#                 course = Course.pull(material.course_id)
#             else:
#                 course = None
#             location = Location.objects.create(activity=activity,
#                                                item=order_item,
#                                                account_id=order_item.account_id,
#                                                program=program,
#                                                course=course)
#         return location
#
#     def get_parent(self):
#         if self.program is not None and self.course is None:
#             parent = self.program
#             name = 'In Program: %s'%self.program.name
#         elif self.program is not None and self.course is not None:
#             parent = self.program
#             name = 'In Program: %s -> In Course: %s' % (self.program.name, self.course.name)
#         elif self.program is None and self.course is not None:
#             parent = self.course
#             name = 'In Course: %s'%self.course.name
#         else:
#             parent = self.activity
#             name = 'Stand alone'
#         return {'item': parent,
#                 'name': name}
#
#     def get_score(self):
#         return self.score_set.order_by('-timeupdate').first()

# class Timer(models.Model):
#     TYPE_CHOICES = (
#         (-1, 'Unpublish'),
#         #(0, 'Same Workshop'),
#         (1, 'At Time'),
#         #(10, 'Befor Schedule'),
#         #(11, 'After Schedule'),
#         #(20, 'After Related Activity Submit'),
#         #(21, 'After Related Activity Publish'),
#     )
#     activity = models.OneToOneField(Activity)
#     type = models.IntegerField(choices=TYPE_CHOICES, default=-1)
#     time = models.DateTimeField(null=True, blank=True)
#     #duration = models.DurationField(default=datetime.timedelta(0))
#     #schedule = models.ForeignKey('workshop.Schedule', null=True, blank=True)
#     #related = models.ForeignKey(Activity, null=True, blank=True, related_name='timer_related_set')
#
#     def display(self):
#         from django.conf import settings
#         from django.utils import timezone
#
#         if self.type == 0:
#             return self.get_type_display()
#         elif self.type == -1:
#             return 'Unpublish'
#         elif self.type == 1:
#             try:
#                 return 'At: %s'%timezone.get_default_timezone().normalize(self.time).strftime(settings.DATETIME_FORMAT)
#             except:
#                 return 'At: - (Please Insert Time)'
#         elif self.type == 10:
#             try:
#                 return 'At: %s Befor Schedule (%s) %s'%(self.schedule.start-self.duration, self.schedule, self.duration)
#             except:
#                 return 'At: - (Please Insert Schedule/Duration)'
#         elif self.type == 11:
#             try:
#                 return 'At: %s After Schedule (%s) %s'%(self.schedule.end+self.duration, self.schedule, self.duration)
#             except:
#                 return 'At: - (Please Insert Schedule/Duration)'
#         elif self.type == 20:
#             try:
#                 return 'After Activity(%s) Submit %s'%(self.related, self.duration)
#             except:
#                 return 'Please Insert Activity/Duration'
#         elif self.type == 21:
#             try:
#                 return 'After Activity(%s) Publish %s'%(self.related, self.duration)
#             except:
#                 return 'Please Insert Activity/Duration'


class Section(models.Model):
    TYPE_CHOICES = (
        (0, 'Information'),
        # (1, 'Scale'),
        (2, 'Mutiple Choice'),
        # (3, 'Mutiple Select'),
        (4, 'Fill Description'),  # Fill Description or Fill in the blank
        # (5, 'Fill Text'),
        # (6, 'Friend'),
    )
    DATA_TYPE_CHOICES = (
        (0, '---'),
        (1, 'Image'),
        (2, 'Video & Audio'),
    )
    DATA_STATUS_CHOICES = (
        (0, '---'),
        (1, 'Wait'),
        (2, 'Complete'),
    )

    activity = models.ForeignKey(Activity)
    name = models.CharField(max_length=120)
    type = models.IntegerField(choices=TYPE_CHOICES)
    content = models.TextField(blank=True)
    data_type = models.IntegerField(choices=DATA_TYPE_CHOICES, default=0)
    image = models.ImageField(upload_to='workshop/images/', null=True, blank=True)
    data = models.CharField(max_length=120, blank=True)
    data_status = models.IntegerField(choices=DATA_STATUS_CHOICES, default=0, db_index=True)

    score = models.IntegerField(default=0)
    answer_count = models.IntegerField(default=0)
    answer_score = models.FloatField(default=0.0)

    is_shuffle = models.BooleanField(default=False)
    is_shuffle_choice = models.BooleanField(default=False)
    sort = models.IntegerField(db_index=True, default=0)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['sort']

    def __str__(self):
        return self.name

    def api_display_title(self):
        question_count = self.question_set.all().count()
        result = {'id': self.id,
                  'name': self.name,
                  'type': self.type,
                  'type_display': self.get_type_display(),
                  'question_count': question_count,
                  'max_score': self.score,  # TODO: Mark remove @din
                  'score': self.score,
                  'is_score_available': self.score > 0}
        return result

    def api_display(self, is_show_answer=False):
        from django.conf import settings
        from django.templatetags.static import static

        result = {'id': self.id,
                  'name': self.name,
                  'type': self.type,
                  'type_display': self.get_type_display()}
        if self.type == 0:
            result['data_type'] = self.data_type
            result['data_type_display'] = self.get_data_type_display()
            if self.content.find('[image]') != -1:
                content = self.content.split('[image]')
                result['content_1'] = content[0]
                result['content_2'] = content[1]
            elif self.content.find('[video]') != -1:
                content = self.content.split('[video]')
                result['content_1'] = content[0]
                result['content_2'] = content[1]
            else:
                result['content_1'] = self.content
                result['content_2'] = None
            if self.data_type == 1:
                if bool(self.image):
                    result['image'] = self.image.url
                else:
                    result['image'] = static('workshop/images/default_image.png')
            elif self.data_type == 2:
                if self.data_status == 2 or self.data_status == 1:
                    result['video_thumb'] = static('question/images/default-video-image.png')
                    result['video'] = '%s%s.mp4' % (settings.SERVER_UPLOAD_URL, self.data)
                else:
                    result['video'] = None

        result['question_list'] = []
        for question in self.get_question_list(is_show_answer=is_show_answer):
            result['question_list'].append(question.api_display(is_show_answer=is_show_answer))
        return result

    def calculation(self):
        from django.db.models import Sum
        from account.models import Account
        self.answer_count = Account.objects.filter(answer__question__section=self).distinct().count()
        if self.type == 1:
            question_result = Question.objects.filter(section=self).aggregate(Sum('answer_count'),
                                                                              Sum('answer_count_passive'),
                                                                              Sum('answer_count_detractor'))
            answer_count = question_result['answer_count__sum']
            answer_count_passive = question_result['answer_count_passive__sum']
            answer_count_detractor = question_result['answer_count_detractor__sum']
            try:
                m = float(answer_count + answer_count_passive + answer_count_detractor)
                self.answer_score = (float(answer_count) / m * 100.0) - (float(answer_count_detractor) / m * 100.0)
            except:
                self.answer_score = 0.0
        elif self.type == 2:
            m = float(self.question_set.count() * self.answer_count)
            n = float(Answer.objects.filter(
                question__section=self,
                choice__is_answer=True,
                score_item__is_last=True
            ).count())
            try:
                self.answer_score = n / m * 100.0
            except:
                self.answer_score = 0.0
        self.save(update_fields=['answer_count', 'answer_score'])

    def update_score(self):
        from django.db.models import Sum
        try:
            self.score = int(self.question_set.all().aggregate(Sum('score'))['score__sum'])
        except:
            self.score = 0
        self.save(update_fields=['score'])
        self.activity.update_max_score()

    def get_question_list(self, is_show_answer=False):
        if is_show_answer:
            return self.question_set.all()
        elif self.is_shuffle:
            return self.question_set.all().order_by('?')
        else:
            return self.question_set.all()

    def get_question_list_shuffle_none(self):
        return self.question_set.all()


class Question(models.Model):
    section = models.ForeignKey(Section)
    text = models.TextField()
    type = models.IntegerField(choices=Section.TYPE_CHOICES)
    score = models.IntegerField(default=1)
    answer_score = models.FloatField(default=0.0)
    answer_count = models.IntegerField(default=0)  # NPS Promoter
    answer_count_passive = models.IntegerField(default=0)  # NPS
    answer_count_detractor = models.IntegerField(default=0)  # NPS

    max_select = models.IntegerField(default=1)  # For type 3 (Mutiple Select)
    sort = models.IntegerField(db_index=True, default=0)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['sort']

    def __str__(self):
        return '%s: %s' % (self.section, self.id)

    def api_display(self, is_show_answer=False):
        result = {}
        result['id'] = self.id
        result['text'] = self.text
        result['type'] = self.type
        result['type_display'] = self.get_type_display()
        result['sort'] = self.sort
        if self.type == 3:
            result['max_select'] = self.max_select
        if self.type in [2, 3, 5]:
            result['choice_list'] = []
            for choice in self.get_choice_list(is_show_answer=is_show_answer):
                result['choice_list'].append(choice.api_display())
        return result

    def nps_progress(self):
        import math
        d = [0, 0, 0]
        if self.answer_score >= 0.0:
            d[0] = 50
            d[2] = self.answer_score * 0.5
        else:
            d[0] = 50 - (math.fabs(self.answer_score) * 0.5)
            d[1] = math.fabs(self.answer_score) * 0.5
        return d

    def get_choice_list(self, is_show_answer=False):
        if is_show_answer:
            return self.choice_set.all()
        elif self.section.is_shuffle_choice:
            return self.choice_set.all().order_by('?')
        else:
            return self.choice_set.all()


class Choice(models.Model):
    # TODO: add section for fast query in cal_*
    question = models.ForeignKey(Question)
    text = models.TextField()
    sort = models.IntegerField(default=0, db_index=True)
    count = models.IntegerField(default=0)
    percent = models.FloatField(default=0.0)
    is_answer = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    timeupdate = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['sort']

    def api_display(self):
        return {
            'id': self.id,
            'text': self.text
        }


class Score(models.Model):
    from django.conf import settings
    from progress.models import Progress

    item = models.ForeignKey('order.Item')
    activity = models.ForeignKey(Activity)
    location = models.ForeignKey('content.Location')
    account = models.ForeignKey(settings.AUTH_USER_MODEL) # not change (related_name) use in report
    material = models.ForeignKey('course.Material', null=True)

    submit_number = models.IntegerField(default=1)
    is_last = models.BooleanField(default=False) # use in report (max submit > 1)
    score = models.IntegerField(default=0)  # -1 (wait grading)
    max_score = models.IntegerField(default=0)
    max_submit = models.IntegerField(default=1)
    pass_score = models.IntegerField(default=0)

    progress = models.IntegerField(default=0)  # TODO: check use?
    status = models.IntegerField(choices=Progress.STATUS_CHOICES, default=1)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    timeupdate = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        ordering = ['-timeupdate']  # Don't Change affect in api.

    def __str__(self):
        return str(self.id)

    @staticmethod
    def pull(activity, order_item, content_location, account, material):
        submit_count = Score.objects.filter(item=order_item,
                                            activity=activity,
                                            location=content_location,
                                            status__in=[2, 3]).count() + 1
        if submit_count <= activity.max_submit or activity.max_submit is 0:
            Score.objects.filter(
                item=order_item,
                activity=activity,
                location=content_location,
            ).update(is_last=False)
            return Score.objects.create(item=order_item,
                                        activity=activity,
                                        location=content_location,
                                        account=account,
                                        material=material,
                                        submit_number=submit_count,
                                        is_last=True,
                                        max_score=activity.max_score,
                                        max_submit=activity.max_submit,
                                        pass_score=activity.pass_score)
        else:
            return None

    @staticmethod
    def pull_latest(content_location, account, order_item):
        return Score.objects.filter(item=order_item,
                                    location=content_location,
                                    account=account,
                                    status__in=[2, 3, 5]) \
            .order_by('-timestamp') \
            .first()

    @staticmethod
    def pull_list(content_location, account, order_item):
        return Score.objects.filter(item=order_item,
                                    location=content_location,
                                    account=account,
                                    status__in=[2, 3, 5]).order_by('timestamp')

    def check_complete(self, score, is_verifying):
        self.score = score

        if is_verifying:
            self.status = 5
        elif self.activity.pass_score == 0:
            self.status = 2
        elif self.score >= self.activity.pass_score:
            self.status = 2
        else:
            self.status = 3
        self.save(update_fields=['score', 'status'])

    def sum_score(self):
        from django.db.models import Sum
        score = ScoreSection.objects.filter(score_item=self) \
            .aggregate(Sum('score'))['score__sum']
        try:
            score = int(score)
        except:
            score = 0
        return score

    def push_progress(self):
        from django.conf import settings
        from progress.models import Progress

        progress = Progress.pull(self.item,
                                 self.location,
                                 settings.CONTENT_TYPE('question', 'activity'),
                                 self.activity_id)
        if progress is not None:
            progress.score = self.score
            progress.check_complete_question(self.item,
                                             self.activity,
                                             self.location,
                                             self.material,
                                             self.status)

    def api_display(self):
        from django.conf import settings
        return {'id': self.id,
                'score': self.score,
                'max_score': self.max_score,
                'max_submit': self.max_submit,
                'pass_score': self.pass_score,
                'submit_number': self.submit_number,
                'status': self.status,
                'status_display': self.get_status_display(),
                'timeupdate': self.timeupdate.strftime(settings.TIME_SHORT_FORMAT),
                'timeupdate_ISO': self.timeupdate.isoformat(),
                'timeupdate_iso': self.timeupdate.isoformat(),
                }

    def get_show_score(self):
        if self.status == 5:
            return 'Waiting for verify'
        else:
            return '%s/%s Point' % (self.score, self.max_score)


class Answer(models.Model):
    from django.conf import settings

    score_item = models.ForeignKey(Score, null=True)
    item = models.ForeignKey('order.Item')
    # TODO: add section for fast query in cal_*
    question = models.ForeignKey(Question)
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    choice = models.ForeignKey(Choice, null=True, blank=True)
    data = models.TextField(blank=True)
    score = models.IntegerField(default=0)

    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    timeupdate = models.DateTimeField(auto_now=True)

    @staticmethod
    def question_answer(question):
        answer = Answer.objects.filter(question=question)
        return answer

    @staticmethod
    def submit_mutiple_choice(score_item, question, ans):
        if ans is None:
            return
        try:
            choice = Choice.objects.get(id=int(ans))
        except:
            choice = None

        Answer.objects.filter(score_item=score_item,
                              question=question).delete()
        Answer.objects.create(score_item=score_item,
                              item_id=score_item.item_id,
                              question=question,
                              account_id=score_item.account_id,
                              choice=choice)

    @staticmethod
    def submit_mutiple_select(score_item, question, ans_list):
        Answer.objects.filter(score_item=score_item,
                              question=question).delete()
        for ans in ans_list:
            try:
                choice = Choice.objects.get(id=int(ans))
            except:
                continue
            Answer.objects.create(score_item=score_item,
                                  item_id=score_item.item_id,
                                  question=question,
                                  account_id=score_item.account_id,
                                  choice=choice,
                                  data='')

    @staticmethod
    def submit_fill_description(score_item, question, ans):
        if ans is None:
            ans = ''
            # return
        Answer.objects.filter(
            score_item=score_item,
            question=question
        ).delete()
        Answer.objects.create(
            score_item=score_item,
            item_id=score_item.item_id,
            question=question,
            account_id=score_item.account_id,
            data=ans,
            score=-1
        )

    @staticmethod
    def submit_fill_text(score_item, question, request):
        Answer.objects.filter(score_item=score_item,
                              question=question).delete()
        for choice in question.choice_set.all():
            if '%s-%s' % (question.id, choice.id) in request.POST:
                ans = request.POST.get('%s-%s' % (question.id, choice.id), None)
                Answer.objects.create(score_item=score_item,
                                      item_id=score_item.item_id,
                                      question=question,
                                      account_id=score_item.account_id,
                                      choice=choice,
                                      data=ans)


class ScoreSection(models.Model):
    from django.conf import settings
    from progress.models import Progress

    score_item = models.ForeignKey(Score)
    item = models.ForeignKey('order.Item')
    activity = models.ForeignKey(Activity)
    section = models.ForeignKey(Section)
    account = models.ForeignKey(settings.AUTH_USER_MODEL)

    score = models.IntegerField(default=0)  # -1 (wait grading)
    max_score = models.IntegerField(default=0)

    status = models.IntegerField(choices=Progress.STATUS_CHOICES, default=1)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    timeupdate = models.DateTimeField(auto_now=True)

    @staticmethod
    def push(score_item, section, score, status):
        score_section = ScoreSection.objects.filter(score_item=score_item,
                                                    section=section).first()
        if score_section is None:
            score_section = ScoreSection.objects.create(score_item=score_item,
                                                        item_id=score_item.item_id,
                                                        activity_id=score_item.activity_id,
                                                        section=section,
                                                        account_id=score_item.account_id,
                                                        score=score,
                                                        max_score=section.score,
                                                        status=status)
        else:
            score_section.score = score
            score_section.status = status
            score_section.save(update_fields=['score', 'status', 'timeupdate'])

    def api_display(self):
        return {'id': self.id,
                'section_id': self.section_id,
                'score': self.score,
                'max_score': self.max_score}
