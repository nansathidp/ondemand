from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.views.decorators.http import require_POST

from .models import Activity, Question, Score, Answer
from program.models import Program
from course.models import Course, Material
from order.models import Item as OrderItem
from content.models import Location as ContentLocation
from progress.models import Content as ProgressContent

from .api.v1.views import json_render


def _submit(request, content_location, activity, order_item, redirect_url, program=None, course=None, material=None):
    result = {
        'redirect_url': redirect_url
    }
    code = 200
    score_item = Score.pull(activity,
                            order_item,
                            content_location,
                            request.user,
                            material)
    if score_item is None:
        result['is_success'] = False
        result['status_msg'] = 'Exception'
        return json_render(result, code)

    for question in Question.objects.filter(section__activity=activity):
        if question.type == 2:
            ans = request.POST.get('%s' % question.id, None)
            Answer.submit_mutiple_choice(score_item,
                                         question,
                                         ans)
        elif question.type == 3:
            ans = request.POST.getlist('%s' % question.id)
            Answer.submit_mutiple_select(score_item,
                                         question,
                                         ans)
        elif question.type == 4:
            ans = request.POST.get('%s' % question.id, None)
            Answer.submit_fill_description(score_item,
                                           question,
                                           ans)
        elif question.type == 5:
            Answer.submit_fill_text(score_item,
                                    question,
                                    request)
        else:
            return json_render({}, 3005)

    activity.calculation_score(score_item)
    ProgressContent.update_progress(content_location.id,
                                    settings.CONTENT_TYPE('question', 'activity').id,
                                    activity.id)
    
    for section in activity.section_set.all():
        section.calculation()
        
    result['score'] = score_item.score
    result['is_success'] = True
    
    result['account'] = request.user.get_display()
    submit_time = timezone.get_current_timezone().normalize(score_item.timestamp)
    result['date'] = submit_time.strftime(settings.TIME_FULL_FORMAT)
    return json_render(result, code)


@login_required
@require_POST
def submit_view(request, activity_id, order_item_id):
    activity = Activity.pull(activity_id)
    if activity is None:
        raise Http404
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    order_item = OrderItem.pull(order_item_id)
    redirect_url = reverse('question:detail', args=[activity.id, order_item.id])
    return _submit(request, content_location, activity, order_item, redirect_url)
            
@login_required
@require_POST
def submit_program_view(request, activity_id, program_id, order_item_id):
    program = Program.pull(program_id)
    activity = Activity.pull(activity_id)
    if program is None or activity is None:
        raise Http404
    
    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=content_type,
                                                  parent1_content=program.id)
    order_item = OrderItem.pull(order_item_id)
    redirect_url = reverse('question:detail_program', args=[activity.id, program.id, order_item.id])
    return _submit(request, content_location, activity, order_item, redirect_url,
                   program=program)
    
@login_required
@require_POST
def submit_course_view(request, activity_id, course_id, material_id, order_item_id):
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    activity = Activity.pull(activity_id)
    if course is None or material is None or activity is None:
        raise Http404
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent1_content=course.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                  parent2_content=material.id)
    order_item = OrderItem.pull(order_item_id)
    redirect_url = reverse('course:playlist', args=[course.id, material.id, order_item.id])
    return _submit(request, content_location, activity, order_item, redirect_url, redirect_url,
                   course=course, material=material)
    
@login_required
@require_POST
def submit_program_course_view(request, activity_id, program_id, course_id, material_id, order_item_id):
    program = Program.pull(program_id)
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    activity = Activity.pull(activity_id)
    if program is None or material is None or course is None or activity is None:
        raise Http404

    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=content_type,
                                                  parent1_content=program.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent2_content=course.id,
                                                  parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                  parent3_content=material.id)
    order_item = OrderItem.pull(order_item_id)
    redirect_url = reverse('course:playlist_program', args=[program.id, course.id, material.id, order_item.id])
    return _submit(request, content_location, activity, order_item, redirect_url,
                   program=program, course=course, material=material)
    
