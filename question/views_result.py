from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import Http404

from .models import Activity, Score, ScoreSection
from program.models import Program
from course.models import Course, Material
from order.models import Item as OrderItem
from content.models import Location as ContentLocation


def _result(request, content_location, activity, order_item, program=None, course=None):
    material_id = request.GET.get('material_id', -1)
    material = Material.pull(material_id)
    
    score_item = Score.pull_latest(content_location, request.user)
    score_section_list = ScoreSection.objects.filter(score_item=score_item, account=request.user)
    return render(request,
                  'question/result.html',
                  {
                      'question': activity,
                      'score_item': score_item,
                      'score_section_list': score_section_list
                  })

@login_required
def result_view(request, activity_id, order_item_id):
    activity = Activity.pull(activity_id)
    order_item = OrderItem.pull(order_item_id)
    if activity is None or order_item is None:
        raise Http404
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    return _result(request, content_location, activity, order_item)

@login_required
def result_program_view(request, activity_id, program_id, order_item_id):
    program = Program.pull(program_id)
    activity = Activity.pull(activity_id)
    order_item = OrderItem.pull(order_item_id)
    if program is None or activity is None or order_item is None:
        raise Http404
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                  parent1_content=program.id)
    return _result(request, content_location, activity, order_item, program=program)

@login_required
def result_course_view(request, activity_id, course_id, order_item_id):
    course = Course.pull(course_id)
    activity = Activity.pull(activity_id)
    order_item = OrderItem.pull(order_item_id)
    if course is None or activity is None or order_item is None:
        raise Http404
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent1_content=course.id)
    return _result(request, content_location, activity, order_item, course=course)

@login_required
def result_program_course_view(request, activity_id, program_id, course_id, order_item_id):
    program = Program.pull(program_id)
    course = Course.pull(course_id)
    activity = Activity.pull(activity_id)
    order_item = OrderItem.pull(order_item_id)
    if program is None or course is None or activity is None or order_item is None:
        raise Http404
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                  parent1_content=program.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent2_content=course.id)
    return _result(request, content_location, activity, order_item, program=program, course=course)
