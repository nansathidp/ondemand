from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth.decorators import login_required

from content.models import Location as ContentLocation
from question.models import Activity, Answer, Score
from order.models import Item as OrderItem
from analytic.models import Stat

def _show_answer(request, content_location, activity, score,
                 previous_url, ROOT_PAGE, parent_url,
             program=None, course=None, material=None):

    view = request.GET.get('view', None)  # FIXME : use?

    section_list = activity.section_set.all().order_by('sort')
    answer_list = Answer.objects.filter(score_item=score)

    Stat.push('activity_%s' % activity.id, request.META.get('HTTP_REFERER', '-'),
              request.META.get('HTTP_USER_AGENT', ''))
    return render(request,
                  'question/show_answer.html',
                  {
                      'TITLE': activity.name,
                      'IMAGE_URL': activity.image.url if activity.image else None,
                      'DISPLAY_SUBMENU': True,
                      'ROOT_PAGE': ROOT_PAGE,
                      'PARENT_URL': parent_url,
                      'previous_url': previous_url,
                      'activity': activity,
                      'answer_list':answer_list,
                      'program': program,
                      'course': course,
                      'material': material,
                      'view': view,
                      'section_list': section_list
                  })

@login_required
def show_answer(request,  activity_id, score_item_id):

    activity = Activity.pull(activity_id)
    score = get_object_or_404(Score, id=score_item_id)



    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    previous_url = reverse('question:detail', args=[activity.id])
    parent_url = reverse('question:answer', args=[activity.id, score_item_id])


    return _show_answer(request, content_location, activity, score,
                        previous_url
                        ,'question', parent_url)


@login_required
def show_answer_couse(request,  activity_id,course_id , material_id, score_item_id):

    activity = Activity.pull(activity_id)
    score = get_object_or_404(Score, id=score_item_id)
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    prevoid_url = reverse('course:playlist', args=[course_id, material_id, score.item_id])
    parent_url = reverse('question:answer_course', args=[activity.id, course_id, material_id, score_item_id])

    return _show_answer(request, content_location, activity, score,
                        prevoid_url, 'question', parent_url)


@login_required
def ahow_answer_program(request,  activity_id, program_id, score_item_id):
    activity = Activity.pull(activity_id)
    score = get_object_or_404(Score, id=score_item_id)

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    prevoid_url = reverse('question:detail_program', args=[activity_id, program_id, score.item_id])
    parent_url = reverse('question:answer_program', args=[activity.id, program_id, score_item_id])

    return _show_answer(request, content_location, activity, score,
                        prevoid_url, 'question', parent_url)

@login_required
def show_answer_program_couse(request,  activity_id, program_id, course_id, material_id, score_item_id):

    activity = Activity.pull(activity_id)
    score = get_object_or_404(Score, id=score_item_id)
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    previous_url = reverse('course:playlist_program', args=[ program_id, course_id, material_id, score.item_id])
    parent_url= reverse('question:answer_program_course', args=[activity_id, program_id, course_id, material_id, score_item_id])
    #previous_url= reverse('question:section_program_course', args=[activity_id, program_id, course_id, material_id, score.item_id])

    return _show_answer(request, content_location, activity, score,
                        previous_url, 'question', parent_url)
