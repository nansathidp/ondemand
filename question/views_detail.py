from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render, redirect


from .models import Activity, Score, ScoreSection
from program.models import Program
from course.models import Course, Material

from analytic.models import Stat
from dependency.models import Dependency
from progress.models import Progress
from order.models import Item as OrderItem
from access.models import Access
from content.models import Location as ContentLocation

from ondemand.views_decorator import check_project


def _detail(request, content_location, activity, order_item,
            section_url, ROOT_PAGE, parent_url,
            program=None, course=None, material=None):

    content_permission, is_require_permission = Access.get_permission(activity.id,
                                                                      settings.CONTENT_TYPE('question', 'activity').id,
                                                                      request.user)

    is_dependency, dependency_list = Dependency.check_content2(settings.CONTENT_TYPE('question', 'activity'),
                                                               activity.id,
                                                               request.user)
    for parent in dependency_list:
        parent.get_content_cached()

    if order_item is not None:
        progress = Progress.pull(order_item,
                                 content_location,
                                 settings.CONTENT_TYPE('question', 'activity'),
                                 activity.id)

        score_item = Score.pull_latest(content_location, request.user, order_item)
        score_list = Score.pull_list(content_location, request.user, order_item)
        score_section_list = ScoreSection.objects.filter(score_item=score_item)
        is_submit = activity.is_submit(order_item, content_location)
    else:
        progress = None
        score_list = []
        score_section_list = []
        is_submit = None
        score_item = None

    Stat.push('activity_%s' % activity.id, request.META.get('HTTP_REFERER', '-'), request.META.get('HTTP_USER_AGENT', ''))
    return render(request,
                  'question/detail.html',
                  {
                      'TITLE': activity.name,
                      'IMAGE_URL': activity.image.url if activity.image else None,
                      'DESC': activity.desc,
                      'DISPLAY_SUBMENU': True,
                      'ROOT_PAGE': ROOT_PAGE,
                      'PARENT_URL': parent_url,
                      'activity': activity,
                      'program': program,
                      'course': course,
                      'order_item': order_item,
                      'section_url': section_url,
                      'progress': progress,
                      'is_dependency': is_dependency,
                      'dependency_list': dependency_list,
                      'content_permission': content_permission,
                      'is_require_permission': is_require_permission,
                      'score_section_list': score_section_list,
                      'score_list': score_list,
                      'score_item': score_item,
                      'is_submit': is_submit
                  })


@check_project
def detail_view(request, activity_id, order_item_id=None):
    activity = Activity.pull(activity_id)
    if activity is None:
        raise Http404
    
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    order_item = None
    if request.user.is_authenticated:
        if order_item_id is not None:
            order_item = OrderItem.pull(order_item_id)
            if order_item is None:
                return redirect('question:detail', activity.id)
        else:
            order_item = OrderItem.pull_purchased(content_location,
                                                  request.user,
                                                  settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
            if order_item is not None:
                return redirect('question:detail', activity.id, order_item.id)
    elif order_item_id is not None:
        return redirect('question:detail', activity.id)

    if order_item is None:
        section_url = None
    else:
        section_url = reverse('question:section', args=[activity.id, order_item.id])

    if not activity.is_display:
        preview = request.GET.get('preview', None)
        if preview and preview.lower() == 'true' and request.user.is_authenticated and activity.check_access(request):
            pass
        elif order_item:
            pass
        else:
            raise Http404
    return _detail(request, content_location, activity, order_item,
                   section_url, 'question', None)

@check_project
def detail_program_view(request, activity_id, program_id, order_item_id=None):
    program = Program.pull(program_id)
    activity = Activity.pull(activity_id)
    if program is None or activity is None:
        raise Http404

    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=content_type,
                                                  parent1_content=program.id)
    if request.user.is_authenticated():
        if order_item_id is not None:
            order_item = OrderItem.pull(order_item_id)
        else:
            order_item = OrderItem.pull_purchased(content_location,
                                                  request.user,
                                                  content_type,
                                                  program.id)
            if order_item is not None:
                return redirect('question:detail', activity.id, order_item.id)
    elif order_item_id is not None:
        return redirect('question:detail', activity.id)
    if order_item is None:
        parent_url = reverse('program:detail', args=[program.id])
        section_url = None
    else:
        parent_url = reverse('program:detail', args=[program.id, order_item.id])
        section_url = reverse('question:section_program', args=[activity.id, program.id, order_item.id])
    return _detail(request, content_location, activity, order_item,
                   section_url, 'program', parent_url,
                   program=program)


@check_project
def detail_course_view(request, activity_id, course_id, material_id, order_item_id=None):
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    activity = Activity.pull(activity_id)
    if course is None or material is None or activity is None:
        raise Http404
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent1_content=course.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                  parent2_content=material.id)
    if request.user.is_authenticated():
        if order_item_id is not None:
            order_item = OrderItem.pull(order_item_id)
        else:
            _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                           course.id)
            order_item = OrderItem.pull_purchased(_content_location,
                                                  request.user,
                                                  settings.CONTENT_TYPE('course', 'course'),
                                                  course.id)
            if order_item is not None:
                return redirect('question:detail', activity.id, order_item.id)
    elif order_item_id is not None:
        return redirect('question:detail', activity.id)
    if order_item is None:
        parent_url = reverse('course:detail', args=[course.id])
        section_url = None
    else:
        parent_url = reverse('course:detail', args=[course.id, order_item.id])
        section_url = reverse('question:section_course', args=[activity.id, course.id, material.id, order_item.id])
    return _detail(request, content_location, activity, order_item,
                   section_url, 'course', parent_url,
                   course=course, material=material)

@check_project
def detail_program_course_view(request, activity_id, program_id, course_id, material_id, order_item_id=None):
    program = Program.pull(program_id)
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    activity = Activity.pull(activity_id)
    if program is None or course is None or activity is None:
        raise Http404

    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=content_type,
                                                  parent1_content=program.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent2_content=course.id,
                                                  parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                  parent3_content=material.id)
    if request.user.is_authenticated():
        if order_item_id is not None:
            order_item = OrderItem.pull(order_item_id)
        else:
            _content_location = ContentLocation.pull_first(content_type,
                                                           program.id)
            order_item = OrderItem.pull_purchased(_content_location,
                                                  request.user,
                                                  content_type,
                                                  program.id)
            if order_item is not None:
                return redirect('question:detail', activity.id, order_item.id)
    elif order_item_id is not None:
        return redirect('question:detail', activity.id)
    if order_item is None:
        parent_url = reverse('course:detail', args=[course.id])
        section_url = None
    else:
        parent_url = reverse('program:detail', args=[course.id, order_item.id])
        section_url = reverse('question:section_program_course', args=[activity.id, program.id, course.id, material.id, order_item.id])
    return _detail(request, content_location, activity, order_item,
                   section_url, 'program', parent_url,
                   program=program, course=course, material=material)
