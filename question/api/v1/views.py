from django.http import JsonResponse

from api.views_api import STATUS_MSG

_STATUS_MSG = {
    200: 'Success',
    201: 'Require POST method.',
    202: 'Require UUID.',
    204: 'Require token.',
    209: 'Require Store.',

    3001: 'Required type.',
    3002: 'Type: format Fail.',
    3003: 'Type: not Allow.',
    3004: 'Activity not found.',
    3005: 'Required Question.',
    3006: 'Choice not found.',
    3007: 'Question and Choice not found.',
    3008: 'Activity not found.',
    3009: 'Dupicate item.',
    3010: 'Maximum Submit',
}


def json_render(result, status):
    try:
        status_msg = _STATUS_MSG[status]
    except:
        status_msg = STATUS_MSG[status]
    data = {'status': status,
            'status_msg': status_msg,
            'result': result}
    return JsonResponse(data, json_dumps_params={'indent': 2})

from api.views import check_auth
from api.v5.views import check_key
from api.v5.views import check_require
