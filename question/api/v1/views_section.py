from .views import json_render, check_key, check_auth, check_require
from django.conf import settings

from ...models import Activity
from course.models import Course, Material
from program.models import Program
from content.models import Location as ContentLocation
from order.models import Item as OrderItem


def _section(request, content_location, activity, order_item, account, program=None, course=None, material=None):
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render({}, code)
    
    result = activity.api_display()

    if order_item is not None:
        result['order_item_id'] = order_item.id
    else:
        result['order_item_id'] = -1
    return json_render(result, code)


def section_view(request, activity_id, order_item_id=None):
    activity = Activity.pull(activity_id)
    if activity is None or activity.is_display is False:
        return json_render({}, 700)
    
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)
    order_item, code = OrderItem.api_pull(content_location,
                                          order_item_id,
                                          settings.CONTENT_TYPE('question', 'activity'),
                                          activity.id,
                                          account)
    if code != 200:
        return json_render({}, code)
    return _section(request, content_location, activity, order_item, account)
                    
    
def section_program_view(request, activity_id, program_id, order_item_id=None):
    activity = Activity.pull(activity_id)
    program = Program.pull(program_id)
    if activity is None or activity.is_display is False or program is None:
        return json_render({}, 700)

    if program.type == 2:
        _content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        _content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=_content_type,
                                                  parent1_content=program.id)
    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)
    _content_location = ContentLocation.pull_first(_content_type,
                                                   program.id)
    order_item, code = OrderItem.api_pull(_content_location,
                                          order_item_id,
                                          _content_type,
                                          program.id,
                                          account)
    if code != 200:
        return json_render({}, code)
    return _section(request, content_location, activity, order_item, account,
                    program=program)


def section_course_view(request, activity_id, course_id, material_id, order_item_id=None):
    activity = Activity.pull(activity_id)
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    if activity is None or activity.is_display is False or course is None or material is None:
        return json_render({}, 700)

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent1_content=course.id)
    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)
    _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                   course.id)
    order_item, code = OrderItem.api_pull(_content_location,
                                          order_item_id,
                                          settings.CONTENT_TYPE('course', 'course'),
                                          course.id,
                                          account)
    if code != 200:
        return json_render({}, code)
    return _section(request, content_location, activity, order_item, account,
                    course=course, material=material)


def section_program_course_view(request, activity_id, program_id, course_id, material_id, order_item_id=None):    
    activity = Activity.pull(activity_id)
    program = Program.pull(program_id)
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    if activity is None or activity.is_display is False or program is None or course is None or material is None:
        return json_render({}, 700)

    if program.type == 2:
        _content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        _content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=_content_type,
                                                  parent1_content=program.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent2_content=course.id)
    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)
    _content_location = ContentLocation.pull_first(_content_type,
                                                   program.id)
    order_item, code = OrderItem.api_pull(_content_location,
                                          order_item_id,
                                          _content_type,
                                          program.id,
                                          account)
    if code != 200:
        return json_render({}, code)
    return _section(request, content_location, activity, order_item, account,
                    program=program, course=course, material=material)
