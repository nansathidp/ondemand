from django.conf.urls import url

from .views_list import list_view
from .views_buy import buy_view

from .views_detail import detail_view, detail_program_view, detail_course_view, detail_program_course_view
from .views_section import section_view, section_program_view, section_course_view, section_program_course_view
from .views_submit import submit_view, submit_program_view, submit_course_view, submit_program_course_view
from .views_answer import answer_view, answer_program_view, answer_course_view, answer_program_course_view

urlpatterns = [
    url(r'^$', list_view),
    url(r'^(\d+)/buy/$', buy_view),
    
    url(r'^(\d+)/detail/$', detail_view),
    url(r'^(\d+)/detail/(\d+)/$', detail_view),
    url(r'^(\d+)/section-list/$', section_view),
    url(r'^(\d+)/section-list/(\d+)/$', section_view),
    url(r'^(\d+)/submit/(\d+)/$', submit_view),
    url(r'^(\d+)/answer/(\d+)/$', answer_view),
    # In Program
    url(r'^(\d+)/program/(\d+)/detail/$', detail_program_view),
    url(r'^(\d+)/program/(\d+)/detail/(\d+)/$', detail_program_view),
    url(r'^(\d+)/program/(\d+)/answer/(\d+)/$', answer_program_view),
    url(r'^(\d+)/program/(\d+)/section-list/$', section_program_view),
    url(r'^(\d+)/program/(\d+)/section-list/(\d+)/$', section_program_view),
    url(r'^(\d+)/program/(\d+)/submit/(\d+)/$', submit_program_view),

    # In Course
    url(r'^(\d+)/course/(\d+)/material/(\d+)/detail/$', detail_course_view),
    url(r'^(\d+)/course/(\d+)/material/(\d+)/detail/(\d+)/$', detail_course_view),
    url(r'^(\d+)/course/(\d+)/material/(\d+)/answer/(\d+)/$', answer_course_view),
    url(r'^(\d+)/course/(\d+)/material/(\d+)/section-list/$', section_course_view),
    url(r'^(\d+)/course/(\d+)/material/(\d+)/section-list/(\d+)/$', section_course_view),
    url(r'^(\d+)/course/(\d+)/material/(\d+)/submit/(\d+)/$', submit_course_view),

    # In Program In  Course
    url(r'^(\d+)/program/(\d+)/course/(\d+)/material/(\d+)/detail/$', detail_program_course_view),
    url(r'^(\d+)/program/(\d+)/course/(\d+)/material/(\d+)/detail/(\d+)/$', detail_program_course_view),
    url(r'^(\d+)/program/(\d+)/course/(\d+)/material/(\d+)/answer/(\d+)/$', answer_program_course_view),
    url(r'^(\d+)/program/(\d+)/course/(\d+)/material/(\d+)/section-list/$', section_program_course_view),
    url(r'^(\d+)/program/(\d+)/course/(\d+)/material/(\d+)/section-list/(\d+)/$', section_program_course_view),
    url(r'^(\d+)/program/(\d+)/course/(\d+)/material/(\d+)/submit/(\d+)/$', submit_program_course_view),
]
