from django.conf import settings

from .views import json_render, check_key, check_auth
from django.views.decorators.csrf import csrf_exempt

from ...models import Activity, Question, Score, Answer
from program.models import Program
from course.models import Course, Material
from order.models import Item as OrderItem
from content.models import Location as ContentLocation
from progress.models import Content as ProgressContent


def _submit(request, content_location, activity, order_item,
            program=None, course=None, material=None):
    result = {}
    if request.method != 'POST':
        return json_render({}, 201)

    response = check_key(request)
    if response is not None:
        return response

    account, code = check_auth(request, is_post=True)
    if code != 200:
        return json_render({}, code)

    if order_item.account_id != account.id:
        return json_render({}, 720)

    score_item = Score.pull(activity,
                            order_item,
                            content_location,
                            account,
                            material)
    if score_item is None:
        return json_render({}, 3010)

    for question in Question.objects.filter(section__activity=activity):
        if question.type == 2:
            ans = request.POST.get('%s' % question.id, None)
            Answer.submit_mutiple_choice(score_item,
                                         question,
                                         ans)
        elif question.type == 3:
            ans = request.POST.get('%s' % question.id, None)
            if ans is None:
                continue
            Answer.submit_mutiple_select(score_item,
                                         question,
                                         ans.split(','))
        elif question.type == 4:
            ans = request.POST.get('%s' % question.id, None)
            Answer.submit_fill_description(score_item,
                                           question,
                                           ans)
        elif question.type == 5:
            Answer.submit_fill_text(score_item,
                                    question,
                                    request)
        else:
            return json_render({}, 3005)

    activity.calculation_score(score_item)
    ProgressContent.update_progress(content_location.id,
                                    settings.CONTENT_TYPE('question', 'activity').id,
                                    activity.id)

    for section in activity.section_set.all():
        section.calculation()
        
    result['score'] = score_item.api_display()
    return json_render(result, code)


@csrf_exempt
def submit_view(request, activity_id, order_item_id):
    activity = Activity.pull(activity_id)
    order_item = OrderItem.pull(order_item_id)
    if activity is None or activity.is_display is False or order_item is None:
        return json_render({}, 700)
    
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    return _submit(request, content_location, activity, order_item)


@csrf_exempt
def submit_program_view(request, activity_id, program_id, order_item_id):
    activity = Activity.pull(activity_id)
    program = Program.pull(program_id)
    order_item = OrderItem.pull(order_item_id)
    if activity is None or activity.is_display is False or program is None or order_item is None:
        return json_render({}, 700)

    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=content_type,
                                                  parent1_content=program.id)
    return _submit(request, content_location, activity, order_item,
                   program=program)


@csrf_exempt
def submit_course_view(request, activity_id, course_id, material_id, order_item_id):
    activity = Activity.pull(activity_id)
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    order_item = OrderItem.pull(order_item_id)
    if activity is None or activity.is_display is False or course is None or material is None or order_item is None:
        return json_render({}, 700)

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent1_content=course.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                  parent2_content=material.id)

    return _submit(request, content_location, activity, order_item,
                   course=course, material=material)


@csrf_exempt
def submit_program_course_view(request, activity_id, program_id, course_id, material_id, order_item_id):
    activity = Activity.pull(activity_id)
    program = Program.pull(program_id)
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    order_item = OrderItem.pull(order_item_id)
    if activity is None or activity.is_display is False or program is None or course is None or material is None or order_item is None:
        return json_render({}, 700)

    if program.type == 2:
        content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=content_type,
                                                  parent1_content=program.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent2_content=course.id,
                                                  parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                  parent3_content=material.id)
    return _submit(request, content_location, activity, order_item,
                   program=program, course=course, material=material)
    

