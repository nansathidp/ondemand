from django.conf import settings

from ...models import Activity, Score, ScoreSection
from course.models import Course, Material
from program.models import Program
from dependency.models import Dependency
from price.models import Price
from progress.models import Progress
from content.models import Location as ContentLocation
from order.models import Item as OrderItem

from app.views import update_checkout_type

from .views import json_render, check_key, check_auth, check_require


def _detail(request, content_location, activity, order_item, account, program=None, course=None, material=None):
    response = check_key(request)
    if response is not None:
        return response

    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render({}, code)

    price = Price.pull(settings.CONTENT_TYPE('question', 'activity'),
                       activity.id,
                       store)

    result = {
        'progress': None,
        'order_item_id': None,
        'item': None,
        'score': None,
        'score_list': [],
        'score_section_list': [],
        'dependency_list': [],
        'submit_count': 0,  # Mark Remove
        'is_submit': False,
        'is_purchased': False,
        'price_set': price.api_display()
    }

    result.update(activity.api_detail_display())

    result['provider'] = activity.provider.api_display() if activity.provider is not None else None

    is_dependency, dependency_list = Dependency.check_content2(settings.CONTENT_TYPE('question', 'activity'),
                                                               activity.id,
                                                               account)
    for parent in dependency_list:
        result['dependency_list'].append(parent.api_display())
    result['is_dependency'] = is_dependency

    install_version = request.GET.get('version', 0)
    update_checkout_type(result, install_version, request.APP.app_version)

    if account is not None and order_item is not None:
        result['order_item_id'] = order_item.id
        result['is_purchased'] = True
        result['item'] = order_item.api_progress_display()
        result['is_submit'] = activity.is_submit(order_item, content_location)
        score = Score.pull_latest(content_location, account, order_item)
        score_list = Score.pull_list(content_location, account, order_item)
        if score:
            result['score'] = score.api_display()

        result['submit_count'] = score_list.count()  # Mark Remove
        score_section_list = ScoreSection.objects.filter(score_item=score)
        for score_section in score_section_list:
            result['score_section_list'].append(score_section.api_display())

        progress = Progress.pull(order_item,
                                 content_location,
                                 settings.CONTENT_TYPE('question', 'activity'),
                                 activity.id)
        
        if progress is not None:
            result['progress'] = progress.api_display()

        for score in score_list:
            result['score_list'].append(score.api_display())
    return json_render(result, code)


def detail_view(request, activity_id, order_item_id=None):
    activity = Activity.pull(activity_id)
    if activity is None or activity.is_display is False:
        return json_render({}, 700)
    
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)
    order_item, code = OrderItem.api_pull(content_location,
                                          order_item_id,
                                          settings.CONTENT_TYPE('question', 'activity'),
                                          activity.id,
                                          account)
    if code != 200:
        return json_render({}, code)
    return _detail(request, content_location, activity, order_item, account)


def detail_program_view(request, activity_id, program_id, order_item_id=None):
    activity = Activity.pull(activity_id)
    program = Program.pull(program_id)
    if activity is None or activity.is_display is False or program is None:
        return json_render({}, 700)

    if program.type == 2:
        _content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        _content_type = settings.CONTENT_TYPE('program', 'program')
        
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=_content_type,
                                                  parent1_content=program.id)
    
    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)
    _content_location = ContentLocation.pull_first(_content_type,
                                                   program.id)
    order_item, code = OrderItem.api_pull(_content_location,
                                          order_item_id,
                                          _content_type,
                                          program.id,
                                          account)
    if code != 200:
        return json_render({}, code)
    return _detail(request, content_location, activity, order_item, account,
                   program=program)


def detail_course_view(request, activity_id, course_id, material_id, order_item_id=None):
    activity = Activity.pull(activity_id)
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    if activity is None or activity.is_display is False or course is None or material is None:
        return json_render({}, 700)

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent1_content=course.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                  parent2_content=material.id)
    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)
    _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                   course.id)
    order_item, code = OrderItem.api_pull(_content_location,
                                          order_item_id,
                                          settings.CONTENT_TYPE('course', 'course'),
                                          course.id,
                                          account)
    if code != 200:
        return json_render({}, code)
    return _detail(request, content_location, activity, order_item, account,
                   course=course, material=material)


def detail_program_course_view(request, activity_id, program_id, course_id, material_id, order_item_id=None):
    activity = Activity.pull(activity_id)
    program = Program.pull(program_id)
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    if activity is None or activity.is_display is False or program is None or course is None or material is None:
        return json_render({}, 700)

    if program.type == 2:
        _content_type = settings.CONTENT_TYPE('program', 'onboard')
    else:
        _content_type = settings.CONTENT_TYPE('program', 'program')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=_content_type,
                                                  parent1_content=program.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent2_content=course.id,
                                                  parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                  parent3_content=material.id)
    account, code = check_auth(request, is_allow_anonymous=True)
    if code != 200:
        return json_render({}, code)
    _content_location = ContentLocation.pull_first(_content_type,
                                                   program.id)
    order_item, code = OrderItem.api_pull(_content_location,
                                          order_item_id,
                                          _content_type,
                                          program.id,
                                          account)
    if code != 200:
        return json_render({}, code)
    return _detail(request, content_location, activity, order_item, account,
                   program=program, course=course, material=material)
