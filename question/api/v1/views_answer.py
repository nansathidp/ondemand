
from .views import json_render, check_key, check_auth, check_require
from ...models import Activity, Score, Answer,Choice

def _answer_view(request,  activity,score, account):
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render({}, code)

    result = activity.api_display(is_show_answer=True)

    choice_list = Choice.objects.filter(question__section__activity=activity)
    answer_list = Answer.objects.filter(account=account, score_item=score, question__section__activity=activity)
    if answer_list.count() == 0:
        return json_render({},3011)
    if activity.is_show_answer:
        for section_item in result['section_list']:
            for question_item in section_item['question_list']:
                for answer in answer_list:
                    if question_item['type'] == 2:
                        if answer.question_id == question_item['id']:
                            for choice_item in question_item['choice_list']:
                                choice_item['is_user_answer'] = answer.choice_id == choice_item['id']
                                choice_item['is_answer'] =  False
                                for _choice in choice_list:
                                    if _choice.id == choice_item['id'] and _choice.is_answer:
                                        choice_item['is_answer'] = True
                    elif question_item['type'] == 4:
                        if answer.question_id == question_item['id']:
                            question_item['answer'] = answer.data

    #if activity.is_show_answer:
    #   for section_item in result['section_list']:
    #        for question_item in section_item['question_list']:
    #            for choice_item in question_item['choice_list']:
    #                choice_item['is_user_answer'] = False
    #                choice_item['is_answer'] = False

    #            for answer in answer_list:
    #                if answer.question_id == question_item['id']:
    #                    for choice_item in question_item['choice_list']:
    #                        if choice_item['id'] == answer.choice_id:
    #                            choice_item['is_user_answer'] = True
    #                            for _choice in choice_list:
    #                                if _choice.id == choice_item['id'] and _choice.is_answer:
    #                                    choice_item['is_answer'] = True

    result['order_item_id'] = score.item_id

    return json_render(result, code)

def answer_view(request, activity_id, score_item_id):
    score = Score.objects.filter(id=score_item_id).first()
    if score is None:
        return json_render({}, 700)
    account, code = check_auth(request, is_allow_anonymous=False)
    print(code)
    if code != 200:
        return json_render({}, code)

    activity = Activity.pull(activity_id)
    if activity is None or activity.is_display is False:
        return json_render({}, 700)

    return _answer_view(request,  activity, score, account)


def answer_program_view(request, activity_id, program_id, score_item_id):
    score = Score.objects.filter(id=score_item_id).first()
    if score is None:
        return json_render({}, 700)
    account, code = check_auth(request, is_allow_anonymous=False)
    if code != 200:
        return json_render({}, code)

    activity = Activity.pull(activity_id)
    if activity is None or activity.is_display is False:
        return json_render({}, 700)

    return _answer_view(request, activity, score, account)


def answer_course_view(request, activity_id,  course_id, material_id, score_item_id):
    score = Score.objects.filter(id=score_item_id).first()
    if score is None:
        return json_render({}, 700)
    account, code = check_auth(request, is_allow_anonymous=False)
    if code != 200:
        return json_render({}, code)

    activity = Activity.pull(activity_id)
    if activity is None or activity.is_display is False:
        return json_render({}, 700)

    return _answer_view(request, activity, score, account)


def answer_program_course_view(request, activity_id, program_id, course_id, material_id, score_item_id):
    score = Score.objects.filter(id=score_item_id).first()
    if score is None:
        return json_render({}, 700)
    account, code = check_auth(request, is_allow_anonymous=False)
    if code != 200:
        return json_render({}, code)

    activity = Activity.pull(activity_id)
    if activity is None or activity.is_display is False:
        return json_render({}, 700)

    return _answer_view(request, activity, score, account)



