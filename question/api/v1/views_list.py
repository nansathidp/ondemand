from .views import check_key, check_require
from .views import json_render

from ...models import Activity


def list_view(request):
    result = {}
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render(result, code)

    result['activity_list'] = []

    for activity in Activity.objects.filter(is_public=True, is_display=True).order_by('-publish'):
        d = activity.api_display_title()
        result['activity_list'].append(d)
    return json_render(result, code)
