from django.conf import settings

from .views import json_render, check_key, check_require

from ...models import Activity
from order.models import Order
from content.models import Location as ContentLocation

from account.cached import cached_api_account_auth
from order.views_base import get_payment_url


def buy_view(request, activity_id):
    result = {}

    # Mobile Data Require
    response = check_key(request)
    if response is not None:
        return response

    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render(result, code)

    account = cached_api_account_auth(token, uuid)
    if account is None:
        return json_render({}, 400)

    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return json_render({}, 703)
    except:
        return json_render({}, 209)

    activity = Activity.pull(activity_id)
    if activity is None:
        return json_render({}, 3008)

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    order_item = Order.buy(content_location,
                           request.APP,
                           store,
                           settings.CONTENT_TYPE('question', 'activity'),
                           activity,
                           account,
                           is_checkout=True)

    if order_item is None:
        return json_render(result, 724)
    else:
        order = order_item.order

    result['order'] = order.api_display()
    result['is_free'] = order_item.is_free
    result['order_id'] = order.id

    # if not order.is_free:
    #     result['redirect_url'] = get_payment_url(request, order, token, uuid, store)
    # else:
    #     order_item.buy_success(request.APP, request.get_host().split(':')[0])
    #     result['redirect_url'] = None
    return json_render(result, code)
