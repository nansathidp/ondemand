from functools import wraps

from course.models import Material


def get_material(view_func):
    def _decorator(request, *args, **kwargs):
        material_id = request.GET.get('material', None)
        request.material = Material.pull(material_id)
        request.material_id = material_id
        response = view_func(request, *args, **kwargs)
        return response
    return wraps(view_func)(_decorator)
