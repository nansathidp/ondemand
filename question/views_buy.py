from django.conf import settings
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

from .models import Activity
from dependency.models import Dependency
from content.models import Location as ContentLocation
from order.models import Order


@login_required
def buy_view(request, activity_id):
    activity = Activity.pull(activity_id)
    if activity is None:
        return redirect('home')

    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    is_dependency, dependency_list = Dependency.check_content2(settings.CONTENT_TYPE('question', 'activity'),
                                                               activity.id,
                                                               request.user)
    if is_dependency:
        return redirect('question:detail', activity.id)

    order_item = Order.buy(content_location,
                           request.APP,
                           1,
                           settings.CONTENT_TYPE('question', 'activity'),
                           activity,
                           request.user)

    if order_item.is_free:
        order_item.order.buy_success(request.APP, request.get_host().split(':')[0])
        return redirect('question:detail', activity_id, order_item.id)
    else:
        return redirect('order:checkout', order_item.order_id)
