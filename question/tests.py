import random
import string
from django.test import TestCase
import json
from django.conf import settings
from rest_framework.test import APITestCase
from rest_framework import status
from .models import Activity, Score
from provider.models import Provider, Account as ProviderAccount
from account.models import Account
from app.models import App
from django.contrib.auth.models import Group, Permission

from category.tests import create_category

def _random():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(8))

def create_app():
    return App.objects.create(name=_random())

def create_user():        
    return Account.objects.create_user('%s@test.com'%_random(),
                                       '123456')

def create_activity(max_submit=1):
    return Activity.objects.create(name=_random(),
                                   type=1,
                                   max_submit=max_submit)

def dashboard_user(permission_codename):        
    user = Account.objects.create_user('dashboard@test.com',
                                       '123456')
    group = Group.objects.create(name='Dashboard')
    if permission_codename is not None:
        permission = Permission.objects.get(content_type=settings.CONTENT_TYPE('question', 'activity'),
                                            codename=permission_codename)
        group.permissions.add(permission)
    user.groups.add(group)
    return user

def dashboard_activity():
    activity = Activity.objects.create(name='Testing',
                                       category=create_category(),
                                       type=1)
    return activity

def dashboard_provider(activity, account):
    provider = Provider.objects.create(name='Provider #1')
    if account is not None:
        ProviderAccount.objects.create(provider=provider,
                                       account=account)
    activity.provider = provider
    activity.save(update_fields=['provider'])





def get_pre_data_db(activity_id, score_id):
    activity = Activity.objects.filter(id=activity_id)
    if activity is None:
        return False
    elif activity is []:
        return False
    else:
        score = Score.objects.filter(activity=activity, id=score_id).count()
        if score > 0:
            return True
        return False


def get_data_json(data_json):
    if data_json['result'] == {}:
       return False
    else:
        return True

def get_activity_random(number=None):
    if number is None:
        score = Score.objects.order_by('?').values('activity').first()
        return score.activity_id
    else:
        score = Score.objects.filter(id=number).values('activity').first()
        return score.activity_id

def get_score_random():
    score = Score.objects.order_by('?').first()
    # print(score)
    # return score.id

BASH_URL_API = 'http://localhost:8000/api/v5/question/'
BASH_URL_API_USER = "/?code=efd7ac9fa4d94d7e8a1c0d9d95898669&key=096c025cf39847909f8248621b24c7b2&uuid=1234&store=4&version=1&token=99de18769980f4f00e8819f8df29c0bf"
class TestcaseAPI(APITestCase):

    def setUp(self):
        self.program = 0
        self.course = 0
        self.activity = 0
        self.score = 0
        self.material = 0
        self.url = ''
        self.str_program = '/program/'
        self.str_course = '/course/'
        self.str_material = '/material/'
        self.str_answer = '/answer/'

    def test_api_question_random(self):
        self.activity = random.randint(0, 20)
        self.score = random.randint(0, 50)
        self.url = BASH_URL_API+str(self.activity)+self.str_answer+str(self.score)+BASH_URL_API_USER
        response = self.client.get(self.url, format="json")
        data = json.loads(response.content)
        response_json = get_data_json(data)
        data_json = get_pre_data_db(self.activity, self.score)
        self.assertEqual(response_json, data_json)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_pass(self):
        get_score_random()
        #self.activity = get_score_random(self.score)
        #self.url = BASH_URL_API + str(self.activity) + self.str_answer + str(self.score) + BASH_URL_API_USER
        #response = self.client.get(self.url, format="json")
        #data = json.loads(response.content)
        #response_json = get_data_json(data)
        #self.assertEqual(response.status_code, status.HTTP_200_OK)
        #self.assertEqual(data['status'], status.HTTP_200_OK)
        #self.assertEqual(response_json, True)
