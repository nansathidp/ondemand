from django.conf.urls import url

from .views import home_view

from .views_buy import buy_view

from .views_detail import detail_view, detail_program_view, detail_course_view, detail_program_course_view
from .views_result import result_view, result_program_view, result_course_view, result_program_course_view
from .views_section import section_view, section_program_view, section_course_view, section_program_course_view
from .views_submit import submit_view
from .views_submit import submit_program_view, submit_course_view, submit_program_course_view
from .views_show_answer import show_answer, show_answer_couse, ahow_answer_program, show_answer_program_couse
app_name = 'question'
urlpatterns = [
    url(r'^$', home_view, name='home'),

    url(r'^(\d+)/buy/$', buy_view, name='buy'),
    
    url(r'^(\d+)/$', detail_view, name='detail'),
    url(r'^(\d+)/item/(\d+)/$', detail_view, name='detail'),
    url(r'^(\d+)/answer/(\d+)/$', show_answer, name='answer'),
    url(r'^(\d+)/item/(\d+)/result/$', result_view, name='result'),
    url(r'^(\d+)/section/(\d+)/$', section_view, name='section'),
    url(r'^(\d+)/section/(\d+)/submit/$', submit_view, name='submit'),

    # In Program
    url(r'^(\d+)/program/(\d+)/$', detail_program_view, name='detail_program'),
    url(r'^(\d+)/program/(\d+)/item/(\d+)/$', detail_program_view, name='detail_program'),
    url(r'^(\d+)/program/(\d+)/answer/(\d+)/$', ahow_answer_program, name='answer_program'),
    url(r'^(\d+)/program/(\d+)/item/(\d+)/result/$', result_program_view, name='result_program'),
    url(r'^(\d+)/program/(\d+)/section/(\d+)/$', section_program_view, name='section_program'),
    url(r'^(\d+)/program/(\d+)/section/(\d+)/submit/$', submit_program_view, name='submit_program'),

    # In Course
    url(r'^(\d+)/course/(\d+)/material/(\d+)/$', detail_course_view, name='detail_course'),
    url(r'^(\d+)/course/(\d+)/material/(\d+)/item/(\d+)/$', detail_course_view, name='detail_course'),
    url(r'^(\d+)/course/(\d+)/material/(\d+)/answer/(\d+)/$', show_answer_couse, name='answer_course'),
    url(r'^(\d+)/course/(\d+)/material/(\d+)/item/(\d+)/result/$', result_course_view, name='result_course'),
    url(r'^(\d+)/course/(\d+)/material/(\d+)/section/(\d+)/$', section_course_view, name='section_course'),
    url(r'^(\d+)/course/(\d+)/material/(\d+)/section/(\d+)/submit/$', submit_course_view, name='submit_course'),

    # In Program In Course
    url(r'^(\d+)/program/(\d+)/course/(\d+)/material/(\d+)/$', detail_program_course_view, name='detail_program_course'),
    url(r'^(\d+)/program/(\d+)/course/(\d+)/material/(\d+)/item/(\d+)/$', detail_program_course_view, name='detail_program_course'),
    url(r'^(\d+)/program/(\d+)/course/(\d+)/material/(\d+)/answer/(\d+)/$', show_answer_program_couse, name='answer_program_course'),
    url(r'^(\d+)/program/(\d+)/course/(\d+)/material/(\d+)/item/(\d+)/result/$', result_program_course_view, name='result_program_lcourse'),
    url(r'^(\d+)/program/(\d+)/course/(\d+)/material/(\d+)/section/(\d+)/$', section_program_course_view, name='section_program_course'),
    url(r'^(\d+)/program/(\d+)/course/(\d+)/material/(\d+)/section/(\d+)/submit/$', submit_program_course_view, name='submit_program_course'),
]
