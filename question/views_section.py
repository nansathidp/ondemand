from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render, redirect

from .models import Activity
from program.models import Program
from course.models import Course, Material
from analytic.models import Stat
from content.models import Location as ContentLocation
from order.models import Item as OrderItem


def _section(request, content_location, activity, order_item,
             submit_url, ROOT_PAGE, parent_url,
             program=None, course=None, material=None):
    if not activity.is_submit(order_item, content_location):
        return redirect(parent_url)

    view = request.GET.get('view', None) # FIXME : use?
    
    section_list = activity.section_set.all().order_by('sort')

    Stat.push('activity_%s' % activity.id, request.META.get('HTTP_REFERER', '-'), request.META.get('HTTP_USER_AGENT', ''))
    return render(request,
                  'question/section.html',
                  {
                      'TITLE': activity.name,
                      'IMAGE_URL': activity.image.url if activity.image else None,
                      'DISPLAY_SUBMENU': True,
                      'ROOT_PAGE': ROOT_PAGE,
                      'PARENT_URL': parent_url,
                      'submit_url': submit_url,
                      'activity': activity,
                      'program': program,
                      'course': course,
                      'material': material,
                      'order_item': order_item,
                      'view': view,
                      'section_list': section_list
                  })


@login_required
def section_view(request, activity_id, order_item_id):
    activity = Activity.pull(activity_id)
    order_item = OrderItem.pull(order_item_id)
    if activity is None or order_item is None:
        return Http404
    if request.user.id != order_item.account_id and order_item.content != activity.id:
        return Http404
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id)
    submit_url = reverse('question:submit', args=[activity.id, order_item.id])
    parent_url = reverse('question:detail', args=[activity.id, order_item.id])
    return _section(request, content_location, activity, order_item,
                    submit_url, 'question', parent_url)


@login_required
def section_program_view(request, activity_id, program_id, order_item_id):
    program = Program.pull(program_id)
    activity = Activity.pull(activity_id)
    order_item = OrderItem.pull(order_item_id)
    if program is None or activity is None or order_item is None:
        return Http404
    if request.user.id != order_item.account_id and not program.item_set.filter(content_type=settings.CONTENT_TYPE('question', 'activity'),
                                                                                content=activity.id).exists():
        return Http404
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                  parent1_content=program.id)
    submit_url = reverse('question:submit_program', args=[activity.id, program.id, order_item.id])
    parent_url = reverse('question:detail_program', args=[activity.id, program.id, order_item.id])
    return _section(request, content_location, activity, order_item,
                    submit_url, 'program', parent_url)


@login_required
def section_course_view(request, activity_id, course_id, material_id, order_item_id):
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    activity = Activity.pull(activity_id)
    order_item = OrderItem.pull(order_item_id)
    if course is None or material is None or activity is None or order_item is None:
        return Http404
    if request.user.id != order_item.account_id and not course.material_set.filter(type=3,
                                                                                   question_id=activity.id).exists():
        return Http404
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent1_content=course.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                  parent2_content=material.id)
    submit_url = reverse('question:submit_course', args=[activity.id, course.id, material.id, order_item.id])
    parent_url = reverse('question:detail_course', args=[activity.id, course.id, material.id, order_item.id])
    return _section(request, content_location, activity, order_item,
                    submit_url, 'course', parent_url)


@login_required
def section_program_course_view(request, activity_id, program_id, course_id, material_id, order_item_id):
    program = Program.pull(program_id)
    course = Course.pull(course_id)
    material = Material.pull(material_id)
    activity = Activity.pull(activity_id)
    order_item = OrderItem.pull(order_item_id)
    if program is None or course is None or activity is None or order_item is None:
        return Http404
    if request.user.id != order_item.account_id and not program.item_set.filter(content_type=settings.CONTENT_TYPE('course', 'course'),
                                                                                content=course.id).exists() \
                                                and not course.material_set.filter(type=3,
                                                                                   question_id=activity.id).exists():
        return Http404
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('question', 'activity'),
                                                  activity.id,
                                                  parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                  parent1_content=program.id,
                                                  parent2_content_type=settings.CONTENT_TYPE('course', 'course'),
                                                  parent2_content=course.id,
                                                  parent3_content_type=settings.CONTENT_TYPE('course', 'material'),
                                                  parent3_content=material.id)
    submit_url = reverse('question:submit_program_course', args=[activity.id, program.id, course.id, material.id, order_item.id])
    parent_url = reverse('question:detail_program_course', args=[activity.id, program.id, course.id, material.id, order_item.id])
    return _section(request, content_location, activity, order_item,
                    submit_url, 'program', parent_url)
