from django.test import TestCase
from django.conf import settings

from .models import Activity, Score
from order.models import Order

from .tests import create_app, create_user, create_activity

class ActivityTests(TestCase):
    pass
"""
    def test_calculation_score_fill_description_max_submit(self):
        app = create_app()
        account = create_user()
        activity = create_activity(max_submit=0)
        order = Order.pull_create(app=app,
                                  account=account)
        order_item = order.buy_content(content_type=settings.CONTENT_TYPE('question', 'activity'),
                                       content_id=activity.id,
                                       store=1,
                                       account=account,
                                       app=app)
                                  
        score = activity.calculation_score(account, order_item, None)
        self.assertEqual(score, None)

    def test_calculation_score_fill_description(self):
        app = create_app()
        account = create_user()
        activity = create_activity(max_submit=1)
        order = Order.pull_create(app=app,
                                  account=account)
        order_item = order.buy_content(content_type=settings.CONTENT_TYPE('question', 'activity'),
                                       content_id=activity.id,
                                       store=1,
                                       account=account,
                                       app=app)
                                  
        score = activity.calculation_score(account, order_item, None)
        self.assertEqual(type(score), Score)
        self.assertEqual(type(score.location), Location) 
"""

class LocationTests(TestCase):
    pass
"""
    def test_pull(self):
        app = create_app()
        account = create_user()
        activity = create_activity()
        order_item = Order.buy(app,
                               1,
                               settings.CONTENT_TYPE('question', 'activity'),
                               activity,
                               account)
        location = Location.pull(activity, order_item, None)
        
        self.assertEqual(type(location), Location)
        self.assertIsNone(location.program)
        self.assertIsNone(location.course) 
"""
