from django.shortcuts import render
from question.models import Activity
from ondemand.views_decorator import check_project

@check_project
def home_view(request):
    question_list = Activity.objects.all()
    return render(request,
                  'question/home.html',
                  {'ROOT_PAGE': 'question',
                   'DISPLAY_SUBMENU': True,
                   'question_list': question_list,})
