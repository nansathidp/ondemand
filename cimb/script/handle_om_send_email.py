import django
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

SPECIAL_EMAIL_LIST = ['wimonrat.s@cimbthai.com',
                      'prachya.n@cimbthai.com',
                      'donnapa.m@cimbthai.com',
                      'phimphimon.t@cimbthai.com',
                      'bubpa.w@cimbthai.com',
                      'sunanta.s@cimbthai.com']

from account.models import Account
from mailer.models import Mailer
from django.conf import settings


if __name__ == '__main__':

    forgot_status = 5
    sender_email = settings.EMAIL_HOST_USER

    for email in SPECIAL_EMAIL_LIST:
        account = Account.objects.filter(email=email).first()
        if account:
            token = account.forgot_password(forgot_status)
            Mailer.send_forget_password_web(account, sender_email, token, forgot_status)
            print('Send email :', email)
        else:
            print('Not found :', email)
