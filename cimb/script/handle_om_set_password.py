import django
import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from account.models import Account

SPECIAL_EMAIL_LIST = [
    'intara@conicle.com',
    'anapat@conicle.com',
    'demo@conicle.com',
    'admin@conicle.com',
    'ninnutda@conicle.com',
    'cimb01@conicle.com',
    'cimb02@conicle.com',
    'cimb03@conicle.com',
    'cimb04@conicle.com',
    'cimb05@conicle.com',
    'cimb05@conicle.com',
    'cimb06@conicle.com',
    'cimb07@conicle.com',
    'cimb08@conicle.com',
    'cimb09@conicle.com',
    'cimb10@conicle.com',
    'cimb11@conicle.com',
    'cimb12@conicle.com',
    'wimonrat.s@cimbthai.com',
    'prachya.n@cimbthai.com',
    'KANJANA.J@CIMBTHAI.COM',
    'PONGSATHORN.J@CIMBTHAI.COM',
    'donnapa.m@cimbthai.com',
    'phimphimon.t@cimbthai.com',
]


if __name__ == '__main__':

    for account in Account.objects.exclude(email__in=SPECIAL_EMAIL_LIST):
        print('Update password :', account.email)
        account.is_enable = True
        account.is_first = True
        account.is_force_reset_password = True
        account.set_password('pwd#123*')
        account.save()
