from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from ..models import Assignment, Result


@csrf_exempt
def preview_change_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)
    if assignment.status != 4:
        html = '<i class="zmdi zmdi-close-circle text-red"></i>'
        return JsonResponse({'html': html})

    html = ''
    if request.method == 'POST':
        try:
            result_id = int(request.POST.get('result', -1))
            result = Result.objects.get(id=result_id)
        except:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
            return JsonResponse({'html': html})

        try:
            status = int(request.POST.get('status', -1))
        except:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
            return JsonResponse({'html': html})

        if status not in [11, 12, 13]:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
            return JsonResponse({'html': html})

        result.status = status
        result.save(update_fields=['status'])
        if result.status == 10:
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
        elif result.status == 11:
            html = '<select class="status" result="%s"><option value="11" selected>Cancel</option><option value="12">Replace</option><option value="13">Expand</option></select>' % (result.id)
        elif result.status == 12:
            html = '<select class="status" result="%s"><option value="11">Cancel</option><option value="12" selected>Replace</option><option value="13">Expand</option></select>' % (result.id)
        elif result.status == 13:
            html = '<select class="status" result="%s"><option value="11">Cancel</option><option value="12">Replace</option><option value="13" selected>Expand</option></select>' % (result.id)
        else:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
    return JsonResponse({'html': html})
