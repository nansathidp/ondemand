from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from account.models import Account
from ..models import Assignment, Member


@csrf_exempt
def account_search_add_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)
    if assignment.status == 3:
        html = '<i class="zmdi zmdi-close-circle text-red"></i>'
        return JsonResponse({'html': html})

    if request.method == 'POST':
        try:
            account_id = int(request.POST.get('account', -1))
            account = Account.objects.get(id=account_id)
        except:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
            return JsonResponse({'html': html})
        
        if request.user.has_perm('assignment.view_assignment', group=request.DASHBOARD_GROUP) or \
           request.user.has_perm('assignment.view_own_assignment', group=request.DASHBOARD_GROUP):
            pass
        elif request.user.has_perm('assignment.view_org_assignment', group=request.DASHBOARD_GROUP):
            if not account.org_parent_account_set.filter(parent=request.user).exists():
                raise PermissionDenied
        else:
            raise PermissionDenied

        member = Member.objects.filter(assignment=assignment,
                                       account=account).first()
        if member is not None:
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green search-delete-member" member="%s" style="cursor: pointer"></i>' % member.id
        else:
            member = Member.objects.create(assignment=assignment,
                                           account=account)
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green search-delete-member" member="%s" style="cursor: pointer"></i>' % member.id
    return JsonResponse({'html': html})
