from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt

from ..models import Assignment, Member


@csrf_exempt
def account_search_delete_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)
    
    html = ''
    if request.method == 'POST':
        try:
            member = Member.objects.filter(assignment=assignment,
                                           id=int(request.POST.get('member', -1))).first()
        except:
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
            return JsonResponse({'html': html})

        html = '<i class="zmdi zmdi-plus-circle-o zmdi-hc-fw insert-account" account="%s" style="cursor: pointer"></i>'%(member.account_id)
        member.delete()
    return JsonResponse({'html': html})
