from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import render

from utils.paginator import paginator
from ..models import Assignment


def home_view(request):
    assignment_list = Assignment.access_list(request)
    if assignment_list is None:
        raise PermissionDenied

    if settings.ACCOUNT__HIDE_INACTIVE:
        assignment_list = assignment_list.exclude(account__is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        assignment_list = assignment_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)
    assignment_list = paginator(request, assignment_list)
    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Assignment Management'}
    ]
    return render(request, 'assignment/dashboard/home.html',
                  {'SIDEBAR': 'assignment',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'assignment_list': assignment_list})
