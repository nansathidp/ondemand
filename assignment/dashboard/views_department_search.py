from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from department.models import Department
from ..models import Assignment


@csrf_exempt
def department_search_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)

    if request.user.has_perm('assignment.view_assignment', group=request.DASHBOARD_GROUP):
        q_department_list = Department.objects.filter(parents__isnull=True)
    elif request.user.has_perm('assignment.view_own_assignment', group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    elif request.user.has_perm('assignment.view_org_assignment', group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    department_list = []
    for department in q_department_list:
        department_list.append(department.display(department.get_child()))

    return JsonResponse({
        'code': 200,
        'department_list': department_list
    })
