from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Assignment


def create_view(request):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    assignment = Assignment.objects.create(account=request.user,
                                           status=1)
    return redirect('dashboard:assignment:content', assignment.id)
