from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Assignment
from ..job.assignment_delete import _assignment_delete

import django_rq


def delete_view(request, assignment_id):
    if not request.user.has_perm('assignment.delete_assignment', group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    assignment = get_object_or_404(Assignment, id=assignment_id)
    assignment.status *= -1
    assignment.save()
    django_rq.enqueue(_assignment_delete, assignment.id)
    return redirect('dashboard:assignment:home')
