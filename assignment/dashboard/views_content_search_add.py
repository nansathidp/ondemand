from django.shortcuts import get_object_or_404
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from course.models import Course
from program.models import Program
from question.models import Activity
from ..models import Assignment, Item as AssignmentItem


@csrf_exempt
def content_search_add_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)
    if assignment.status == 3:
        html = '<i class="zmdi zmdi-close-circle text-red"></i>'
        return JsonResponse({'html': html})

    if request.method == 'POST':
        content_type = settings.CONTENT_TYPE_ID(int(request.POST.get('type', -1)))
        try:
            content = int(request.POST.get('content', -1))
            if content_type.id == settings.CONTENT_TYPE('course', 'course').id:
                content = Course.objects.get(id=content)
            elif content_type.id == settings.CONTENT_TYPE('question', 'activity').id:
                content = Activity.objects.get(id=content)
            elif content_type.id == settings.CONTENT_TYPE('program', 'program').id:
                content = Program.objects.get(id=content)
            elif content_type.id == settings.CONTENT_TYPE('program', 'onboard').id:
                content = Program.objects.get(id=content)
            else:
                content = None
        except:
            content = None

        if content is not None:
            if request.user.has_perm('assignment.view_assignment', group=request.DASHBOARD_GROUP) or \
               request.user.has_perm('assignment.view_org_assignment', group=request.DASHBOARD_GROUP):
                pass
            elif request.user.has_perm('assignment.view_own_assignment', group=request.DASHBOARD_GROUP):
                if request.DASHBOARD_PROVIDER is not None:
                    if content.provider != request.DASHBOARD_PROVIDER:
                        raise PermissionDenied
                elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                    if content.provider_id != request.DASHBOARD_SUB_PROVIDER.provider_id:
                        raise PermissionDenied
                else:
                    pass
            else:
                raise PermissionDenied

        if content_type is None or content is None:
            html = '<i class="zmdi zmdi-close-circle text-red"></i>'
            return JsonResponse({'html': html})

        item = AssignmentItem.objects.filter(assignment=assignment,
                                             content_type=content_type,
                                             content=content.id).first()
        if item is not None:
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green search-delete-item" item="%s" style="cursor: pointer"></i>' %item.id
        else:
            item = AssignmentItem.objects.create(assignment=assignment,
                                                 content_type=content_type,
                                                 content=content.id,
                                                 sort=assignment.item_set.count()+1)
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green search-delete-item" item="%s" style="cursor: pointer"></i>' % item.id
    return JsonResponse({'html': html})
