from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Assignment, Member
from ..job.assignment_delete import _assignment_summary_delete

import django_rq


def summary_delete_view(request, assignment_id, member_id):
    if not request.user.has_perm('assignment.delete_assignment', group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    assignment = get_object_or_404(Assignment, id=assignment_id)
    member = get_object_or_404(Member, id=member_id)
    member.status = -2
    member.save(update_fields=['status'])
    django_rq.enqueue(_assignment_summary_delete, member.id)
    return redirect('dashboard:assignment:summary', assignment.id)
