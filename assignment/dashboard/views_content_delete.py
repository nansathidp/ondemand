from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from ..models import Assignment, Item
from progress.models import Progress

@csrf_exempt
def content_delete_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)

    if request.method == 'POST':
        try:
            Item.objects.filter(assignment=assignment,
                                id=int(request.POST.get('item', -1))).delete()
            #program.progress_update()
        except:
            pass

    item_list = assignment.item_set.all()
    return render(request,
                  'assignment/dashboard/content_block.html',
                  {'assignment': assignment,
                   'item_list': item_list})
