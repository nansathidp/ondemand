from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt

from ..models import Assignment, Item
from progress.models import Progress

@csrf_exempt
def content_search_delete_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)

    html = ''
    if request.method == 'POST':
        try:
            item = Item.objects.filter(assignment=assignment,
                                       id=int(request.POST.get('item', -1))).first()
        except:
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-red"></i>'
            return JsonResponse({'html': html})

        html = '<i class="zmdi zmdi-plus-circle-o zmdi-hc-fw insert-content" type="%s" content="%s" style="cursor: pointer"></i>'%(item.content_type_id, item.content)
        item.delete()
    return JsonResponse({'html': html})
