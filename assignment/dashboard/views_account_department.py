from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from department.models import Department
from ..models import Assignment


@csrf_exempt
def account_department_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)

    if request.user.has_perm('assignment.view_assignment', group=request.DASHBOARD_GROUP):
        department_list = Department.objects.filter(parents__isnull=True)
    elif request.user.has_perm('assignment.view_own_assignment', group=request.DASHBOARD_GROUP):
        department_list = None
    elif request.user.has_perm('assignment.view_org_assignment', group=request.DASHBOARD_GROUP):
        department_list = None

    return render(request,
                  'assignment/dashboard/department.html',
                  {'assignment': assignment,
                   'type': type,
                   'department_list': department_list})
