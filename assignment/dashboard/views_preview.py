import datetime

import django_rq
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone

from ..jobs import _push, _push_notification
from ..models import Assignment


def preview_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)
    if assignment.status != 4:
        if assignment.status <= 2:
            return redirect('dashboard:assignment:account', assignment.id)
        else:
            return redirect('dashboard:assignment:summary', assignment.id)

    if request.method == 'POST':
        try:
            if request.POST['expired'] == 'content':
                assignment.expired = datetime.timedelta(0)
                assignment.expired_date = None
            elif request.POST['expired'] == 'date':
                assignment.expired = datetime.timedelta(0)
                _ = request.POST.get('expired_date').split('/')
                assignment.expired_date = timezone.make_aware(
                    datetime.datetime(int(_[2]), int(_[1]), int(_[0]), 0, 0, 0)).date()
            elif request.POST['expired'] == 'duration':
                assignment.expired_date = None
                day = int(request.POST.get('duration-day'))
                hour = int(request.POST.get('duration-hour'))
                minute = int(request.POST.get('duration-minute'))
                second = 0
                assignment.expired = datetime.timedelta(days=day, hours=hour, minutes=minute, seconds=second)
            assignment.save()
        except:
            pass

        if 'submit' in request.POST:
            # assignment.push(request.APP, request.get_host(), False)
            host = request.get_host()
            queue = django_rq.get_queue('default', autocommit=True, async=True, default_timeout=3600)
            queue.enqueue(_push, assignment, request.APP, host)
            queue.enqueue(_push_notification, assignment)
            # assignment.preview_submit_notification()
            return redirect('dashboard:assignment:process', assignment.id)

    member_list = assignment.member_set.select_related('account').all()
    item_list = assignment.item_set.all()
    result_list = assignment.result_set.all()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Assignment Management',
         'url': reverse('dashboard:assignment:home')},
        {'is_active': True,
         'title': 'Assignment Preview.'}
    ]
    return render(request,
                  'assignment/dashboard/preview.html',
                  {'SIDEBAR': 'assignment',
                   'TAB': 'preview',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'assignment': assignment,
                   'member_list': member_list,
                   'item_list': item_list,
                   'result_list': result_list})
