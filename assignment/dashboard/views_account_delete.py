from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from ..models import Assignment, Member


@csrf_exempt
def account_delete_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)

    if request.method == 'POST':
        try:
            Member.objects.filter(assignment=assignment,
                                  id=int(request.POST.get('member', -1))).delete()
        except:
            pass

    member_list = assignment.member_set.all()
    return render(request,
                  'assignment/dashboard/account_block.html',
                  {'assignment': assignment,
                   'member_list': member_list})
