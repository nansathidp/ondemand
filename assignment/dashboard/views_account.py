import xlrd
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404

from account.jobs import _email
from account.models import Account
from dashboard.views import config_view
from ..models import Assignment, Member
from alert.models import Alert

def _push(assignment, email, count):
    email = email.strip()
    if len(email) == 0:
        return

    account = Account.objects.filter(email=email).first()
    if account is None:
        account = Account.objects.filter(id=email).first()
    if account is None:
        status = -1
    else:
        status = 1
    Member.objects.create(
        assignment=assignment,
        account=account,
        display=email,
        status=status,
        sort=count
    )


def _excel(assignment, book, count, allow_account_list):
    sh = book.sheet_by_index(0)
    for rx in range(sh.nrows):
        email = _email(sh.cell(rx, 0).value, sh.cell(rx, 0).ctype)
        count += 1
        if email in allow_account_list:
            _push(assignment, email, count)


def account_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)
    if not assignment.check_access(request):
        raise PermissionDenied

    if assignment.status == 3:
        return redirect('dashboard:assignment:summary', assignment.id)

    error = False

    if request.method == 'POST':
        data = request.FILES['import']
        count = assignment.member_set.count()
        try:
            book = xlrd.open_workbook(file_contents=data.read())
            allow_account_list = []
            if request.user.has_perm('assignment.view_assignment', group=request.DASHBOARD_GROUP) or \
                    request.user.has_perm('assignment.view_own_assignment', group=request.DASHBOARD_GROUP):
                allow_account_list = Account.objects.values_list('email', flat=True).all()
            elif request.user.has_perm('assignment.view_org_assignment', group=request.DASHBOARD_GROUP):
                if settings.IS_ORGANIZATION:
                    allow_account_list = Account.objects.values_list('email', flat=True) \
                        .filter(org_parent_account_set__parent=request.user)
                else:
                    return config_view(request)
            else:
                raise PermissionDenied
            _excel(assignment, book, count, allow_account_list)
        except:
            try:
                for line in data:
                    count += 1
                    _push(assignment, line.decode('utf-8').replace(',', '').replace('\n', ''), count)
            except:
                error = True
    member_list = assignment.member_set.select_related('account').all()
    if settings.ACCOUNT__HIDE_INACTIVE:
        member_list = member_list.exclude(account__is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        member_list = member_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)
    alert = Alert.pull_inprogress(request.user, settings.CONTENT_TYPE('assignment', 'assignment'), assignment.id)
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Assignment Management',
         'url': reverse('dashboard:assignment:home')},
        {'is_active': True,
         'title': 'Select Learners to assign.'}
    ]
    return render(request,
                  'assignment/dashboard/account.html',
                  {'SIDEBAR': 'assignment',
                   'TAB': 'account',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'assignment': assignment,
                   'member_list': member_list,
                   'alert': alert,
                   'error': error})
