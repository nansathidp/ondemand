from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt

from account.models import Account
from ..models import Assignment, Member
from department.models import Member as DepartmentMember


@csrf_exempt
def account_department_view(request, assignment_id, department_id):
    if not request.user.has_perm('assignment.add_assignment', group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    if request.method == 'POST':
        department_member_list = DepartmentMember.objects.filter(department_id=department_id)
        
        if request.user.has_perm('assignment.view_assignment', group=request.DASHBOARD_GROUP) or \
           request.user.has_perm('assignment.view_own_assignment', group=request.DASHBOARD_GROUP):
            pass
        else:
            raise PermissionDenied

        # member = Member.objects.filter(assignment=assignment, account=account).first()
    return redirect('')
