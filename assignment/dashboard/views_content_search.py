from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from category.models import Category
from course.models import Course
from dashboard.views import paginator
from program.models import Program
from provider.models import Provider
from question.models import Activity
from ..models import Assignment


@csrf_exempt
def content_search_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)
    if not assignment.check_access(request):
        raise PermissionDenied

    item_list = assignment.item_set.all()
    q_name = ''
    content_list = []
    q_category_list = []
    content_type = None
    q_provider_list = []
    type = None
    title = ''
    if request.method == 'POST':
        type = request.POST.get('type', None)
        q_name = request.POST.get('q_name', '')
        q_provider = request.POST.get('provider', '-1')
        q_category = request.POST.get('category', '-1')
        q_provider_list = Provider.pull_list()
        if type == 'course':
            title = 'Add Courses'
            if request.user.has_perm('assignment.view_assignment', group=request.DASHBOARD_GROUP):
                content_list = Course.objects.all()
                q_category_list = Category.pull_list()
            elif request.user.has_perm('assignment.view_org_assignment', group=request.DASHBOARD_GROUP):
                content_list = Course.objects.filter(is_display=True)
                q_category_list = Category.pull_list()
            elif request.user.has_perm('assignment.view_own_assignment', group=request.DASHBOARD_GROUP):
                if request.DASHBOARD_PROVIDER is not None:
                    content_list = Course.objects.filter(provider=request.DASHBOARD_PROVIDER)
                elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                    content_list = Course.objects.filter(provider_id=request.DASHBOARD_SUB_PROVIDER.provider_id)
                else:
                    content_list = []
            else:
                raise PermissionDenied

            if len(q_name) > 0:
                print(q_name)
                content_list = content_list.filter(name__icontains=q_name)

            if q_provider != '-1':
                content_list = content_list.filter(provider_id=q_provider)

            if q_category != '-1':
                content_list = content_list.filter(category_id=q_category)

            # content_list = content_list[:20]
            content_type = settings.CONTENT_TYPE('course', 'course')
        elif type == 'program':
            title = 'Add Programs'
            if request.user.has_perm('assignment.view_assignment', group=request.DASHBOARD_GROUP):
                content_list = Program.objects.filter(type=1)
                if q_provider != '-1':
                    content_list = content_list.filter(provider_id=q_provider)
            elif request.user.has_perm('assignment.view_org_assignment', group=request.DASHBOARD_GROUP):
                content_list = Program.objects.filter(type=1, is_display=True)
                if q_provider != '-1':
                    content_list = content_list.filter(provider_id=q_provider)
            elif request.user.has_perm('assignment.view_own_assignment', group=request.DASHBOARD_GROUP):
                if request.DASHBOARD_PROVIDER is not None:
                    content_list = Program.objects.filter(type=1, provider=request.DASHBOARD_PROVIDER)
                elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                    content_list = Program.objects.filter(type=1, provider_id=request.DASHBOARD_SUB_PROVIDER.provider_id)
                else:
                    content_list = []
            else:
                raise PermissionDenied

            if len(q_name) > 0:
                content_list = content_list.filter(name__icontains=q_name)
            # content_list = content_list[:settings.CONTENT_LIMIT]
            content_type = settings.CONTENT_TYPE('program', 'program')
        elif type == 'onboard':
            title = 'Add Onboardings'
            if request.user.has_perm('assignment.view_assignment', group=request.DASHBOARD_GROUP):
                content_list = Program.objects.filter(type=2)
                if q_provider != '-1':
                    content_list = content_list.filter(provider_id=q_provider)
            elif request.user.has_perm('assignment.view_org_assignment', group=request.DASHBOARD_GROUP):
                content_list = Program.objects.filter(type=2, is_display=True)
                if q_provider != '-1':
                    content_list = content_list.filter(provider_id=q_provider)
            elif request.user.has_perm('assignment.view_own_assignment', group=request.DASHBOARD_GROUP):
                if request.DASHBOARD_PROVIDER is not None:
                    content_list = Program.objects.filter(type=2, provider=request.DASHBOARD_PROVIDER)
                elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                    content_list = Program.objects.filter(type=2, provider_id=request.DASHBOARD_SUB_PROVIDER.provider_id)
                else:
                    content_list = []
            else:
                raise PermissionDenied

            if len(q_name) > 0:
                content_list = content_list.filter(name__icontains=q_name)
            # content_list = content_list[:settings.CONTENT_LIMIT]
            content_type = settings.CONTENT_TYPE('program', 'onboard')
        elif type == 'question':
            title = 'Add Tests'
            if request.user.has_perm('assignment.view_assignment', group=request.DASHBOARD_GROUP):
                content_list = Activity.objects.all()
                if q_provider != '-1':
                    content_list = content_list.filter(provider_id=q_provider)
            elif request.user.has_perm('assignment.view_org_assignment', group=request.DASHBOARD_GROUP):
                content_list = Activity.objects.filter(is_display=True)
                if q_provider != '-1':
                    content_list = content_list.filter(provider_id=q_provider)
            elif request.user.has_perm('assignment.view_own_assignment', group=request.DASHBOARD_GROUP):
                if request.DASHBOARD_PROVIDER is not None:
                    content_list = Activity.objects.filter(provider=request.DASHBOARD_PROVIDER)
                elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                    content_list = Activity.objects.filter(provider_id=request.DASHBOARD_SUB_PROVIDER.provider_id)
                else:
                    content_list = []
            else:
                raise PermissionDenied

            if len(q_name) > 0:
                content_list = content_list.filter(name__icontains=q_name)
            # content_list = content_list[:settings.CONTENT_LIMIT]
            content_type = settings.CONTENT_TYPE('question', 'activity')
        content_list = paginator(request, content_list)
    return render(request,
                  'assignment/dashboard/content_search.html',
                  {'assignment': assignment,
                   'title': title,
                   'item_list': item_list,
                   'type': type,
                   'content_type': content_type,
                   'q_name': q_name,
                   'q_provider': int(q_provider),
                   'q_category': int(q_category),
                   'q_category_list': q_category_list,
                   'q_provider_list': q_provider_list,
                   'content_list': content_list})
