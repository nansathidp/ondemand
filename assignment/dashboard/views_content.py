from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ..models import Assignment


def content_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment', group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)
    if not assignment.check_access(request):
        raise PermissionDenied

    if assignment.status == 3:
        return redirect('dashboard:assignment:summary', assignment.id)

    item_list = assignment.item_set.all()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Assignment Management',
         'url': reverse('dashboard:assignment:home')},
        {'is_active': True,
         'title': 'Select Items to assign.'}
    ]
    return render(request,
                  'assignment/dashboard/content.html',
                  {'SIDEBAR': 'assignment',
                   'TAB': 'content',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'assignment': assignment,
                   'item_list': item_list})
