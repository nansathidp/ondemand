from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

from ..models import Assignment


def status_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)
    count_item = assignment.item_set.all().count()
    count_member = assignment.member_set.all().count()
    count_result = assignment.result_set.filter(status__in=[1, 2, 3, 20, 22, 23, 24]).count()

    try:
        progress = (count_result / (count_item * count_member)) * 100
    except:
        progress = 0

    return JsonResponse({'assignment_status': assignment.status,
                         'progress': int(progress)})
