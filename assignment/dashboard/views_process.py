from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, redirect, render

from ..models import Assignment


def process_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)

    if assignment.status != 4:
        if assignment.status <= 2:
            return redirect('dashboard:assignment:account', assignment.id)
        else:
            return redirect('dashboard:assignment:summary', assignment.id)

    return render(request,
                  'assignment/dashboard/process.html',
                  {'SIDEBAR': 'assignment',
                   'TAB': 'preview',
                   'BREADCRUMB_LIST': [],
                   'assignment': assignment
                   })
