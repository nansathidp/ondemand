from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404

from ..models import Assignment


def summary_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, id=assignment_id)
    if assignment.status != 3:
        return redirect('dashboard:assignment:account', assignment.id)

    member_list = assignment.member_set.filter(status__in=[-1, 1]).select_related('account').all()
    item_list = assignment.item_set.all()
    result_list = assignment.result_set.all()
    if settings.ACCOUNT__HIDE_INACTIVE:
        member_list = member_list.exclude(account__is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        member_list = member_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Assignment Management',
         'url': reverse('dashboard:assignment:home')},
        {'is_active': True,
         'title': 'Assignment Summary'}
    ]
    return render(request,
                  'assignment/dashboard/summary.html',
                  {'SIDEBAR': 'assignment',
                   'TAB': 'summary',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'assignment': assignment,
                   'item_list': item_list,
                   'member_list': member_list,
                   'result_list': result_list})
