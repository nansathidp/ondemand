from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from department.models import Department
from assignment.models import Assignment, Member


@csrf_exempt
def assign_department_view(request, assignment_id):
    if not request.user.has_perm('assignment.add_assignment',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    if request.method != 'POST':
        return JsonResponse(
            {
                'code': 404,
                'status': 'Not Found.'
            }
        )

    assignment = get_object_or_404(Assignment, id=assignment_id)
    try:
        department = get_object_or_404(Department, id=int(request.POST.get('department_id', -1)))
    except:
        return JsonResponse(
            {
                'code': 404,
                'status': 'Not Found.'
            }
        )

    if request.user.has_perm('assignment.view_assignment', group=request.DASHBOARD_GROUP):
        pass
    elif request.user.has_perm('assignment.view_own_assignment', group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    elif request.user.has_perm('assignment.view_org_assignment', group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    department_child = department.get_all_child()
    department_list = [department]
    for _department in department_child:
        department_list.append(_department)

    account_id_list = list(assignment.member_set.values_list('account_id', flat=True))
    create_member_list = []
    for department in department_list:
        member_list = department.member_set.filter(
            account__is_active=True
        )
        for member in member_list:
            if member.account_id not in account_id_list:
                account_id_list.append(member.account_id)
                create_member_list.append(Member(
                    assignment=assignment,
                    account=member.account
                ))

    Member.objects.bulk_create(create_member_list)
    return JsonResponse({
        'code': 200,
        'status': 'Success'
    })
