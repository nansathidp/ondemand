from django.conf.urls import url

from .views import home_view
from .views_account import account_view
from .views_account_delete import account_delete_view
from .views_account_department import account_department_view
from .views_account_search import account_search_view
from .views_account_search_add import account_search_add_view
from .views_account_search_delete import account_search_delete_view
from .views_account_submit import account_submit_view
from .views_content import content_view
from .views_content_delete import content_delete_view
from .views_content_search import content_search_view
from .views_content_search_add import content_search_add_view
from .views_content_search_delete import content_search_delete_view
from .views_create import create_view
from .views_delete import delete_view
from .views_department_add import assign_department_view
from .views_department_search import department_search_view
from .views_preview import preview_view
from .views_preview_change import preview_change_view
from .views_process import process_view
from .views_status import status_view
from .views_summary import summary_view
from .views_summary_delete import summary_delete_view

app_name = 'assignment'
urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^create/$', create_view, name='create'),
    url(r'^(\d+)/delete/$', delete_view, name='delete'),

    url(r'^(\d+)/content/$', content_view, name='content'),
    url(r'^(\d+)/content/search/$', content_search_view, name='content_search'),
    url(r'^(\d+)/content/search/add/$', content_search_add_view, name='content_search_add'),
    url(r'^(\d+)/content/search/delete/$', content_search_delete_view, name='content_search_delete'),
    url(r'^(\d+)/content/delete/$', content_delete_view, name='content_delete'),

    url(r'^(\d+)/department/search/$', department_search_view, name='department_search'),
    url(r'^(\d+)/department/search/add/$', assign_department_view, name='department_search_add'),

    url(r'^(\d+)/learners/$', account_view, name='account'),
    url(r'^(\d+)/learners/search/$', account_search_view, name='account_search'),
    url(r'^(\d+)/learners/search/add/$', account_search_add_view, name='account_search_add'),
    url(r'^(\d+)/learners/search/delete/$', account_search_delete_view, name='account_search_delete'),
    url(r'^(\d+)/learners/delete/$', account_delete_view, name='account_delete'),
    url(r'^(\d+)/learners/submit/$', account_submit_view, name='account_submit'),

    url(r'^(\d+)/learners/department/$', account_department_view, name='account_department'),

    url(r'^(\d+)/process/$', process_view, name='process'),
    url(r'^(\d+)/status/$', status_view, name='status'),

    url(r'^(\d+)/preview/$', preview_view, name='preview'),
    url(r'^(\d+)/preview/change/$', preview_change_view, name='preview_change'),

    url(r'^(\d+)/summary/$', summary_view, name='summary'),
    url(r'^(\d+)/summary/(\d+)/delete/$', summary_delete_view, name='summary_delete'),
]
