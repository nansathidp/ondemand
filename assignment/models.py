from django.db import models


class Assignment(models.Model):
    import datetime
    from django.conf import settings

    STATUS_CHOICES = (
        # 1 <-> 2 >> 4 >> 3
        (-4, 'Preview (Delete)'),
        (-3, 'Summary (Delete)'),
        (-2, 'Select Learners (Delete)'),
        (-1, 'Select Content (Delete)'),

        (1, 'Select Content'),
        (2, 'Select Learners'),
        (3, 'Summary'),
        (4, 'Preview'),
    )

    account = models.ForeignKey(settings.AUTH_USER_MODEL, null=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=1)
    expired = models.DurationField(default=datetime.timedelta(0))
    expired_date = models.DateTimeField(null=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'view_org', 'add', 'change', 'delete')
        ordering = ['-timestamp']

    def __str__(self):
        return str(self.id)

    @staticmethod
    def pull(id):
        return Assignment.objects.filter(id=id).first()

    @staticmethod
    def access_list(request):
        if request.user.has_perm('assignment.view_assignment',
                                 group=request.DASHBOARD_GROUP):
            return Assignment.objects.filter(status__gt=0, account__isnull=False)
        elif request.user.has_perm('assignment.view_own_assignment',
                                   group=request.DASHBOARD_GROUP):
            return Assignment.objects.filter(account=request.user, status__gt=0)
        elif request.user.has_perm('assignment.view_org_assignment',
                                   group=request.DASHBOARD_GROUP):
            return Assignment.objects.filter(account=request.user, status__gt=0)
        else:
            return None

    @staticmethod
    def check_access(request):
        if request.user.has_perm('assignment.view_assignment',
                                 group=request.DASHBOARD_GROUP):
            return True
        elif request.user.has_perm('assignment.view_own_assignment',
                                   group=request.DASHBOARD_GROUP):
            return True
        elif request.user.has_perm('assignment.view_org_assignment',
                                   group=request.DASHBOARD_GROUP):
            return True
        else:
            return False

    def preview_submit_notification(self):
        from utils.content import get_content, get_content_site
        from django.conf import settings
        import django_rq

        from assignment.job.assignment_send import _send_assignment
        from mailer.job.send_email import _send_assignment_email
        from inbox.models import Inbox

        member_list = self.member_set.filter(status__in=[-1, 1]).select_related('account').all()
        account_list = [member.account for member in member_list]
        item_list = self.item_set.all()
        result_list = Result.objects.filter(assignment=self)
        result_status_map = {}
        for result in result_list:
            result_status_map['%s_%s' % (result.member_id, result.item_id)] = result.status == 1

        item_content_list = []
        for item in item_list:
            content = get_content(item.content_type_id,
                                  item.content)
            site_url = get_content_site(item.content_type_id,
                                        item.content)
            if settings.IS_INBOX:
                if content is None:
                    name = ''
                else:
                    name = content.name
                title = 'Assignment : %s' % name
                Inbox.push_message(type=7,
                                   content_type=item.content_type,
                                   content=item.content,
                                   title=title,
                                   body='%s has been assigned to your library.' % name,
                                   status=1,
                                   account_list=account_list)
            item.site = site_url
            item_content_list.append(item)

        if settings.IS_ALERT_ASSIGNMENT:
            if settings.IS_ALERT_AIS:
                from ais.jobs import push_with_email
                subject = 'Assignment: %s assign content to you' % self.account.email
                django_rq.enqueue(push_with_email, subject, account_list, 'iOS')
                django_rq.enqueue(push_with_email, subject, account_list, 'Android')
            else:
                django_rq.enqueue(_send_assignment, self.id)
        django_rq.enqueue(_send_assignment_email, self, item_content_list)

    def push(self, app, domain, is_preview, is_auto=False):
        from django.conf import settings
        from account.cached import cached_onboard_account_delete

        if self.status == 3:
            return
        item_list = list(self.item_set.all())

        member_list = list(self.member_set.filter(status=1))
        for member in member_list:
            if is_preview:
                member.push_preview(app, domain, item_list, is_auto)
            else:
                member.push(app, domain, item_list, self.expired, self.expired_date)

        if is_preview:
            self.status = 4
            self.save(update_fields=['status'])
        else:
            # Remove On-boarding cached
            is_update_cached = False
            content_type_program = settings.CONTENT_TYPE('program', 'program')
            for item in item_list:
                if item.content_type == content_type_program:
                    is_update_cached = True
            if is_update_cached:
                for member in member_list:
                    cached_onboard_account_delete(member.account.id)

            self.status = 3
            self.save(update_fields=['status'])

    def get_expired_hour(self):
        s = self.expired.total_seconds()
        d, s = divmod(s, 60 * 60 * 24)
        return int(s / (60 * 60))

    def get_expired_minute(self):
        s = self.expired.total_seconds()
        h, s = divmod(s, 60 * 60)
        return int(s / 60)

    def get_member_count(self):
        from django.conf import settings
        member_list = self.member_set.filter(status__in=[-1, 1])
        if settings.ACCOUNT__HIDE_INACTIVE:
            member_list = member_list.exclude(account__is_active=False)
        if settings.ACCOUNT__HIDE_ID_LIST:
            member_list = member_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)
        return member_list.count()


class Item(models.Model):
    assignment = models.ForeignKey(Assignment)
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='assigement_item_set')
    content = models.BigIntegerField(db_index=True)
    sort = models.IntegerField(default=0, db_index=True)

    class Meta:
        ordering = ['sort']


class Member(models.Model):
    from django.conf import settings

    STATUS_CHOICES = (
        (-2, 'Summary Delete'),
        (-1, 'Import Not Found.'),
        (1, 'Basic'),
    )

    assignment = models.ForeignKey(Assignment)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name='assignment_account_set')
    display = models.CharField(max_length=120, blank=True)  # TODO: remove
    status = models.IntegerField(choices=STATUS_CHOICES, default=1)
    sort = models.IntegerField(default=0, db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['sort']

    def push_preview(self, app, domain, item_list, is_auto):
        from order.models import Item as OrderItem

        for item in item_list:
            if not Result.objects.filter(assignment_id=self.assignment_id,
                                         item=item,
                                         member=self).exists():

                if OrderItem.objects.filter(account_id=self.account_id,
                                            content_type_id=item.content_type_id,
                                            content=item.content).exists():
                    if is_auto:
                        status = 13
                    else:
                        status = 11
                else:
                    status = 10

                result = Result.objects.create(assignment_id=self.assignment_id,
                                               item=item,
                                               member=self,
                                               status=status)

    def push(self, app, domain, item_list, expired, expired_date):
        from django.utils import timezone
        from django.conf import settings

        from order.models import Order, Item as OrderItem
        from progress.models import Progress
        from content.models import Location as ContentLocation
        from order.cached import cached_order_item_update
        from utils.content import get_content
        from order.cached import cached_order_item_delete
        from utils.date_time import convert_from_date
        import datetime

        from progress.cached import cached_progress_delete

        def _push_content(order, item):
            status = 1
            order_item = None
            content_type = settings.CONTENT_TYPE_ID(item.content_type_id)

            content = get_content(content_type.id, item.content)

            if content is not None:
                now = timezone.now()
                content_location = ContentLocation.pull_first(content_type, content.id)
                order_item = Order.buy(content_location,
                                       app,
                                       1,
                                       content_type,
                                       content,
                                       self.account,
                                       order=order,
                                       start=now)

                if expired_date is not None:
                    order_item.expired = convert_from_date(expired_date) - now
                    order_item.save(update_fields=['expired'])
                    cached_order_item_update(order_item)
                elif expired and expired.total_seconds() > 0:
                    order_item.expired = expired
                    order_item.save(update_fields=['expired'])
                    cached_order_item_update(order_item)
            else:
                status = -1
            return status, order_item

        order = Order.pull_create(app, self.account, method=21, timecomplete=timezone.now())
        for item in item_list:
            result = Result.objects.filter(assignment_id=self.assignment_id,
                                           item=item,
                                           member=self).first()

            if result is None:
                continue
            elif result.status == 11:
                continue

            if result.status == 10:  # OK
                if OrderItem.objects.filter(account_id=self.account_id,
                                            content_type_id=item.content_type_id,
                                            content=item.content).exists():
                    result.status = 20
                    result.save(update_fields=['status'])
                else:
                    result.status, result.order_item = _push_content(order, item)
                    result.save(update_fields=['status', 'order_item'])
            elif result.status == 12:  # Duplicate (Replace)
                order_item = OrderItem.objects.filter(account_id=self.account_id,
                                                      content_type_id=item.content_type_id,
                                                      content=item.content).first()
                if order_item is None:
                    result.status = 22
                    result.save(update_fields=['status'])
                else:
                    order_item.status = -1
                    order_item.save(update_fields=['status'])
                    cached_order_item_delete(order_item)

                    result.status, result.order_item = _push_content(order, item)
                    result.save(update_fields=['status', 'order_item'])
            elif result.status == 13:
                # Duplicate (Expand)
                order_item = OrderItem.objects.filter(account_id=self.account_id,
                                                      content_type_id=item.content_type_id,
                                                      content=item.content).first()
                if order_item is None:
                    result.status = 23
                    result.save(update_fields=['status'])
                else:
                    content = get_content(item.content_type_id,
                                          item.content)
                    if content is None:
                        result.status = 24
                        result.save(update_fields=['status'])
                    else:
                        result.status = 1
                        result.save(update_fields=['status'])

                        # Expired
                        now = timezone.now()
                        if order_item.start is None:
                            order_item.start = now
                        order_item.credit = content.credit

                        if expired_date is not None:
                            order_item.expired = convert_from_date(expired_date) - order_item.start
                        elif expired and expired.total_seconds() > 0:
                            order_item.expired = (now + expired) - order_item.start
                        elif expired and expired.total_seconds() == 0:
                            order_item.expired = datetime.timedelta(0)
                        else:
                            order_item.expired = (now + content.expired) - order_item.start

                        order_item.save(update_fields=['expired', 'credit', 'start'])
                        cached_order_item_delete(order_item)
                        progress_list = Progress.objects.filter(item=order_item)
                        progress_list.update(status=1)
                        for progress in progress_list:
                            cached_progress_delete(progress)

        order.buy_success(app, domain, method=21)


class Result(models.Model):
    STATUS_CHOICES = (
        (-1, 'Fail'),
        (1, 'Success'),
        (2, 'Duplicate (Cancel)'),
        (3, 'Duplicate (Added)'),

        # Preview
        (10, 'OK'),
        (11, 'Duplicate (Cancel)'),
        (12, 'Duplicate (Replace)'),
        (13, 'Duplicate (Expand)'),

        # Error Preview
        (20, 'OK -> Exists.'),
        (22, 'Duplicate (Replace) -> Not Found OrderItem.'),
        (23, 'Duplicate (Expand) -> Not Found OrderItem.'),
        (24, 'Duplicate (Expand) -> Not Found Content.expired.'),
    )

    assignment = models.ForeignKey(Assignment)
    item = models.ForeignKey(Item)
    member = models.ForeignKey(Member)
    order_item = models.ForeignKey('order.Item', null=True)
    status = models.IntegerField(choices=STATUS_CHOICES)
