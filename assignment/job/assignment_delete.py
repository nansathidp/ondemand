from django.conf import settings

from assignment.models import Assignment, Member
from progress.models import Account as ProgressAccount, Content as ProgressContent

from order.cached import cached_order_delete, cached_order_item_delete
from progress.cached import cached_progress_delete
from account.cached import cached_onboard_account_delete


def _delete_progress(order_item):
    for progress in order_item.progress_set.all():
        print('  - Progress ID:', progress.id)
        cached_progress_delete(progress)
        progress.delete()
        ProgressContent.update_progress(progress.location_id, progress.content_type_id, progress.content)


def _delete_order_item(order_item):
    print(' - Order Item ID:', order_item.id)
    print(' \_', order_item.content_type, ' : ', order_item.content)
    _delete_progress(order_item)
    cached_order_item_delete(order_item)
    order = order_item.order
    order_item.delete()
    if settings.CONTENT_TYPE('program', 'onboard').id == order_item.content_type_id:
        cached_onboard_account_delete(order_item.account_id)
    # print('order item count = ', order.item_set.count())
    if not order.item_set.exists():
        # print('order.delete()')
        order.delete()
        cached_order_delete(order.id)
    ProgressAccount.update_progress(order_item.account_id)


def _assignment_delete(assignment_id):
    assignment = Assignment.objects.filter(id=assignment_id).first()
    if assignment is None:
        return None
    print(assignment)
    for result in assignment.result_set.all():
        if result.order_item is None:
            continue
        _delete_order_item(result.order_item)


def _assignment_summary_delete(member_id):
    member = Member.objects.filter(id=member_id).first()
    if member is None:
        return None
    for result in member.result_set.filter(assignment_id=member.assignment_id):
        if result.order_item is None:
            continue
        _delete_order_item(result.order_item)
