from app.models import App
from assignment.models import Assignment
from inbox.cached import cached_inbox_count
from notification.models import Ios, IphoneMessage, IphoneKey, Message, Android, AndroidKey, AndroidMessage
from utils.content import get_content


def _send_assignment(assignment_id):
    app = App.objects.all().first()
    if app is None:
        return

    assignment = Assignment.objects.get(id=assignment_id)

    content_list = []
    for item in assignment.item_set.all():
        content = get_content(item.content_type_id,
                              item.content)
        if content:
            item.content_object = content
            content_list.append(item)

    for member in assignment.member_set.all():
        inbox_count = cached_inbox_count(member.account.id)
        inbox_count.update_count()

        for content in content_list:
            subject = '%s has been assigned to your library.' % content.content_object.name

            message = Message.objects.create(app=app,
                                             message=subject,
                                             content=content.content,
                                             content_type=content.content_type,
                                             status=-1)

            iphone_list = Ios.objects.filter(account=member.account)
            android_list = Android.objects.filter(account=member.account)

            iphone_key_list = list(IphoneKey.objects.all())
            android_key_list = list(AndroidKey.objects.all())

            for iphone in iphone_list:
                iphone_message = IphoneMessage.objects.create(inbox=None,
                                                              app=app,
                                                              message=message,
                                                              iphone=iphone)
                for iphone_key in iphone_key_list:
                    iphone_message.send(iphone_key)

            for android in android_list:
                android_message = AndroidMessage.objects.create(app=app,
                                                                message=message,
                                                                android=android)
                for android_key in android_key_list:
                    android_message.send(android_key)
