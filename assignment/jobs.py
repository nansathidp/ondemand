import os

from django.conf import settings
from xlrd import open_workbook

from account.jobs import _email
from account.models import Account
from alert.models import Alert
from assignment.models import Assignment, Member

ACCOUNT_LIST = []


def _member(assignment, email):
    global ACCOUNT_LIST

    if email is None:
        return

    account = Account.objects.filter(email=email).first()
    if account is None:
        status = -1
    else:
        status = 1
        member = assignment.member_set.filter(account=account).first()
        if member is None:
            Member.objects.create(
                assignment=assignment,
                account=account,
                status=status
            )
        if account.id in ACCOUNT_LIST:
            ACCOUNT_LIST.remove(account.id)


def _push(assignment, app, host):
    assignment.push(app, host, False)


def _push_notification(assignment):
    assignment.preview_submit_notification()


def _import(alert_id):
    try:
        alert = Alert.objects.get(id=alert_id)
    except:
        return None

    try:
        assignment = Assignment.objects.get(id=alert.content)
    except:
        return None

    global ACCOUNT_LIST
    try:
        ACCOUNT_LIST = []
        for member in assignment.member_set.all():
            ACCOUNT_LIST.append(member.account_id)
        
        path = os.path.join(settings.BASE_DIR, 'alert', 'upload', alert.file_input)
        book = open_workbook(path)
        sheet = book.sheet_by_index(0)
        for i in range(1, sheet.nrows):
            _member(
                assignment,
                _email(
                    sheet.cell(i, 0).value,
                    sheet.cell(i, 0).ctype
                )
            )
        if len(ACCOUNT_LIST) > 0:
            assignment.member_set.filter(account_id__in=ACCOUNT_LIST).delete()
        alert.status = 2
    except:
        alert.status = -1
    alert.save(update_fields=['status'])
