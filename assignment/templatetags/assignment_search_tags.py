from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

from program.models import Program, Item as ProgramItem

register = template.Library()


@register.simple_tag
def content_check_tag(item_list, content_id, content_type):
    is_found = False
    item_id = None
    for item in item_list:
        if item.content_type_id == content_type.id and item.content == content_id:
            is_found = True
            item_id = item.id
            break

    if is_found:
        result = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green search-delete-item" item="%s" style="cursor: pointer"></i>' % item_id
    else:
        result = '<i class="zmdi zmdi-plus-circle-o zmdi-hc-fw insert-content" type="%s" content="%s" style="cursor: pointer"></i>' % (
        content_type.id, content_id)
    return mark_safe(result)


@register.simple_tag
def member_check_tag(member_list, account_id):
    is_found = False
    member_id = None
    for member in member_list:
        if member.account_id == account_id:
            is_found = True
            member_id = member.id
            break

    if is_found:
        result = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green search-delete-member" member="%s" style="cursor: pointer"></i>' % member_id
    else:
        result = '<i class="zmdi zmdi-plus-circle-o zmdi-hc-fw insert-account" account="%s" style="cursor: pointer"></i>' % (
        account_id)
    return mark_safe(result)


@register.simple_tag
def preview_check_tag(member, item, result_list):
    result = None
    for result_obj in result_list:
        if result_obj.member_id == member.id and result_obj.item_id == item.id:
            if result_obj.status == 10:
                result = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
            elif result_obj.status == 11:
                result = '<select class="status" result="%s"><option value="11" selected>Cancel</option><option value="12">Replace</option><option value="13">Expand</option></select>' % (
                result_obj.id)
            elif result_obj.status == 12:
                result = '<select class="status" result="%s"><option value="11">Cancel</option><option value="12" selected>Replace</option><option value="13">Expand</option></select>' % (
                result_obj.id)
            elif result_obj.status == 13:
                result = '<select class="status" result="%s"><option value="11">Cancel</option><option value="12">Replace</option><option value="13" selected>Expand</option></select>' % (
                result_obj.id)
            break
    if result is None:
        result = '<i class="zmdi zmdi-close-circle text-red"></i>'
    return mark_safe(result)


@register.simple_tag
def result_check_tag(member, item, result_list):
    result = None
    for result_obj in result_list:
        if result_obj.member_id == member.id and result_obj.item_id == item.id:
            if result_obj.status == 1:
                result = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green"></i>'
            else:
                result = '<i class="zmdi zmdi-close-circle text-red"></i>'
            break
    if result is None:
        result = '<i class="zmdi zmdi-close-circle text-red"></i>'
    return mark_safe(result)
