# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-16 10:52
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Assignment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.IntegerField(choices=[(1, 'Select Content'), (2, 'Select Learners'), (3, 'Summary'), (4, 'Preview')], default=1)),
                ('expired', models.DurationField(default=datetime.timedelta(0))),
                ('expired_date', models.DateTimeField(null=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-timestamp'],
                'default_permissions': ('view', 'view_own', 'view_org', 'add', 'change', 'delete'),
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.BigIntegerField(db_index=True)),
                ('sort', models.IntegerField(db_index=True, default=0)),
                ('assignment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='assignment.Assignment')),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='assigement_item_set', to='contenttypes.ContentType')),
            ],
            options={
                'ordering': ['sort'],
            },
        ),
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('display', models.CharField(blank=True, max_length=120)),
                ('status', models.IntegerField(choices=[(-1, 'Import Not Found.'), (1, 'Basic')], default=1)),
                ('sort', models.IntegerField(db_index=True, default=0)),
                ('timestamp', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('account', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='assigement_account_set', to=settings.AUTH_USER_MODEL)),
                ('assignment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='assignment.Assignment')),
            ],
            options={
                'ordering': ['sort'],
            },
        ),
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.IntegerField(choices=[(-1, 'Fail'), (1, 'Success'), (2, 'Duplicate (Cancel)'), (3, 'Duplicate (Added)'), (10, 'Ok'), (11, 'Duplicate (Cancel)'), (12, 'Duplicate (Replace)'), (13, 'Duplicate (Expand)'), (20, 'Ok -> Exists.'), (22, 'Duplicate (Replace) -> Not Found OrderItem.'), (23, 'Duplicate (Expand) -> Not Found OrderItem.'), (24, 'Duplicate (Expand) -> Not Found Content.expired.')])),
                ('assignment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='assignment.Assignment')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='assignment.Item')),
                ('member', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='assignment.Member')),
            ],
        ),
    ]
