from django.contrib import admin

from .models import Assignment, Item, Member, Result


@admin.register(Assignment)
class AssignmentAdmin(admin.ModelAdmin):
    list_display = ('account', 'status', 'timestamp')
    # actions = ['send_email', ]

    # @staticmethod
    # def send_email(self, request, queryset):
    #     from mailer.models import Mailer
    #     domain = request.get_host()
    #     for assignment in queryset:
    #         Mailer.send_assignment(assignment, domain)


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('assignment', 'content_type', 'content', 'sort')


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ('assignment', 'account', 'timestamp')


@admin.register(Result)
class ResultAdmin(admin.ModelAdmin):
    list_display = ('assignment', 'item', 'member', 'order_item', 'status')
