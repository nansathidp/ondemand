from django.contrib import admin

from tutor.models import *


@admin.register(Tutor)
class TutorAdmin(admin.ModelAdmin):
    list_display = ('name', 'image', 'is_display','sort')
    sortable = 'sort'
    readonly_fields = ('id', 'account',)
    search_fields = ['name']
    actions = ['make_published','make_unpublished','select_application']

    def uppercase_name(self, obj):
        return ("%s" % (obj.name)).upper()
    uppercase_name.short_description = 'Upercase Name'

    def get_ordering(self, request):
        if request.user.is_superuser:
            return ['name', 'image']
        else:
            return ['name']

    def make_published(self, request, queryset):
        queryset.update(is_display=True)
    make_published.short_description = "Mark selected tutor as published"

    def make_unpublished(self, request, queryset):
        queryset.update(is_display=False)
    make_unpublished.short_description = "Mark selected tutor as unpublished"


