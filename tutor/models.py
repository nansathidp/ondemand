from django.db import models


class Tutor(models.Model):
    from django.conf import settings

    account = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)
    name = models.CharField(max_length=120)

    image = models.ImageField(upload_to='tutor/')
    image_featured = models.ImageField(upload_to='tutor/featured/', null=True, blank=True)  # For App
    image_cover = models.ImageField(upload_to='tutor/cover/', null=True, blank=True)

    desc = models.TextField(blank=True)
    info = models.TextField(blank=True)

    providers = models.ManyToManyField('provider.Provider')

    sort = models.IntegerField(db_index=True, default=0)
    is_display = models.BooleanField(default=False, db_index=True)

    extra = models.TextField(blank=True)

    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    timeupdate = models.DateTimeField(auto_now=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')
        ordering = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        from utils.models_image_delete import image_delete_pre_save
        image_delete_pre_save(self)
        super(self.__class__, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        from utils.models_image_delete import image_delete_pre_delete
        image_delete_pre_delete(self)
        super(self.__class__, self).delete(*args, **kwargs)

    @staticmethod
    def pull_list():
        return Tutor.objects.filter(is_display=True).order_by('sort')

    @staticmethod
    def pull_featured_list():
        # TODO: CACHED
        return Tutor.objects.exclude(image_featured=None).filter(is_display=True).order_by('sort')

    def get_extra(self):
        import json
        try:
            extra = json.loads(self.extra)
        except ValueError:
            extra = {}
        return extra

    def get_providers(self):
        provider_list = []
        for provider in self.providers.all():
            provider_list.append(provider.api_display_title())
        return provider_list

    def api_display_title(self, is_display_provider=False):
        from django.conf import settings
        d = {
            'id': self.id,
            'name': self.name,
            'desc': self.desc,
            'timestamp': self.timestamp.strftime(settings.TIME_FORMAT)
        }

        if bool(self.image):
            d['image'] = self.image.url
        else:
            d['image'] = '%s/static/images/profile_default.jpg' % settings.SITE_URL

        if bool(self.image_featured):
            d['image_featured'] = self.image_featured.url
        else:
            d['image_featured'] = None

        if bool(self.image_cover):
            d['image_cover'] = self.image_cover.url
        else:
            d['image_cover'] = None

        if is_display_provider:
            d['provider_list'] = self.get_providers()
        return d

    def api_display(self, is_display_provider=False, is_display_category=False):
        return self.api_display_title(is_display_provider=is_display_provider)

    def count_content(self):
        from django.db.models import Sum
        from django.conf import settings
        from course.models import Course
        from progress.models import Content
        import json
        try:
            extra = json.loads(self.extra)
        except ValueError:
            extra = {}

        course_list = Course.objects.filter(tutor=self).distinct()
        extra['count_course'] = course_list.count()
        content_type = settings.CONTENT_TYPE('course', 'course')
        result = Content.objects.filter(content_type=content_type,
                                        content__in=course_list.values_list('id', flat=True)).aggregate(Sum('learner'))
        if result['learner__sum'] is None:
            extra['count_learner'] = 0
        else:
            extra['count_learner'] = result['learner__sum']
        self.extra = json.dumps(extra)
        self.save(update_fields=['extra'])
