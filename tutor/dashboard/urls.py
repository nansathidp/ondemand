from django.conf.urls import url

from .views import home_view
from .views_create import create_view
from .views_detail import information_view

app_name = 'tutor-dashboard'
urlpatterns = [
    url(r'^$', home_view, name='home'), #TODO: testing
    url(r'^create/$', create_view, name='create'), #TODO: testing
    url(r'^(\d+)/$', information_view, name='information'), #TODO: testing
]
