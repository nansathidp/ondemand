from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404

from provider.cached import cached_provider_tutor
from .forms import TutorForm
from ..models import Tutor

from utils.crop import crop_image

def information_view(request, tutor_id):
    if not request.user.has_perm('tutor.view_tutor', group=request.DASHBOARD_GROUP) and \
       not request.user.has_perm('tutor.view_own_tutor', group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    tutor = get_object_or_404(Tutor, id=tutor_id)

    if request.method == 'POST':
        if not request.user.has_perm('tutor.change_tutor',
                                 group=request.DASHBOARD_GROUP):
            raise PermissionDenied

        if request.user.has_perm('tutor.view_tutor',
                                 group=request.DASHBOARD_GROUP):
            tutor_form = TutorForm(None, request.POST, request.FILES, instance=tutor)
        elif request.user.has_perm('tutor.view_own_tutor',
                                   group=request.DASHBOARD_GROUP):
            tutor_form = TutorForm(request, request.POST, request.FILES, instance=tutor)
        else:
            raise PermissionDenied

        if tutor_form.is_valid():
            tutor = tutor_form.save()
            crop_image(tutor, request)
            for provider in tutor.providers.all():
                cached_provider_tutor(provider.id, is_force=True)

    if request.user.has_perm('tutor.view_tutor',
                             group=request.DASHBOARD_GROUP):
        tutor_form = TutorForm(None, instance=tutor)
    elif request.user.has_perm('tutor.view_own_tutor',
                               group=request.DASHBOARD_GROUP):
        tutor_form = TutorForm(request, instance=tutor)
    else:
        raise PermissionDenied

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Instructor Management',
         'url': reverse('dashboard:tutor-dashboard:home')},
        {'is_active': True,
         'title': tutor.name},
    ]
    return render(request,
                  'tutor/dashboard/detail.html',
                  {'SIDEBAR': 'tutor',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'information',
                   'tutor_form': tutor_form,
                   'tutor': tutor})
