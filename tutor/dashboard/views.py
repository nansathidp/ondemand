from django.shortcuts import render
from django.core.exceptions import PermissionDenied

from ..models import Tutor
from provider.models import Provider
from utils.paginator import paginator


def home_view(request):
    if request.user.has_perm('tutor.view_tutor', group=request.DASHBOARD_GROUP):
        tutor_list = Tutor.objects.prefetch_related('providers').all()
    elif request.user.has_perm('tutor.view_own_tutor', group=request.DASHBOARD_GROUP):
        tutor_list = Tutor.objects.filter(providers__in=Provider.objects.filter(account__account=request.user))
    else:
        raise PermissionDenied

    tutor_list = paginator(request, tutor_list)

    breadcrumb_list = [
        {'is_active': True,
         'title': 'Instructor Management'}
    ]
    return render(request,
                  'tutor/dashboard/home.html',
                  {'SIDEBAR': 'tutor',
                   'BREADCRUMB_LIST': breadcrumb_list,
                   'tutor_list': tutor_list})
