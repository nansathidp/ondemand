from django.shortcuts import render, redirect
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from provider.cached import cached_provider_tutor

from .forms import TutorForm

from utils.crop import crop_image

def create_view(request):
    if not request.user.has_perm('tutor.add_tutor',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    if request.method == 'POST':
        if request.user.has_perm('tutor.view_tutor',
                                 group=request.DASHBOARD_GROUP):
            tutor_form = TutorForm(None, request.POST, request.FILES)
        elif request.user.has_perm('tutor.view_own_tutor',
                                   group=request.DASHBOARD_GROUP):
            tutor_form = TutorForm(request, request.POST, request.FILES)
        else:
            raise PermissionDenied

        if tutor_form.is_valid():
            tutor = tutor_form.save()
            crop_image(tutor, request)
            for provider in tutor.providers.all():
                cached_provider_tutor(provider.id, is_force=True)
            return redirect('dashboard:tutor-dashboard:home')
    else:
        if request.user.has_perm('tutor.view_tutor',
                                 group=request.DASHBOARD_GROUP):
            tutor_form = TutorForm(None)
        elif request.user.has_perm('tutor.view_own_tutor',
                                   group=request.DASHBOARD_GROUP):
            tutor_form = TutorForm(request)
        else:
            raise PermissionDenied

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Instructor Management',
         'url': reverse('dashboard:tutor-dashboard:home')},
        {'is_active': True,
         'title': 'Create'},
    ]
    return render(request,
                  'tutor/dashboard/create.html',
                  {'SIDEBAR': 'tutor',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'tutor_form': tutor_form})
