from django import forms
from django.conf import settings

from provider.models import Provider
from ..models import Tutor


class TutorForm(forms.ModelForm):
    class Meta:
        model = Tutor
        fields = ['name',
                  'image',
                  'image_featured',
                  'image_cover',
                  'desc',
                  'providers',
                  'is_display']

        labels = {'providers': 'Learning Center', 'desc': 'Description', 'is_display': 'Display'}

        help_texts = {
            'name': 'Limit at 120 characters',
            'image': settings.HELP_TEXT_IMAGE('tutor'),
            'image_featured': settings.HELP_TEXT_IMAGE('featured'),
            'image_cover': settings.HELP_TEXT_IMAGE('cover'),
            'desc': 'Limit at 400 characters',
            'providers': 'กด Ctrl ค้าง และคลิกที่ชื่อเพื่อเลือกหลาย Learning Center'
        }

    def __init__(self, request=None, *args, **kwargs):
        super(TutorForm, self).__init__(*args, **kwargs)
        if request is not None:
            self.fields['providers'].queryset = Provider.access(request)
