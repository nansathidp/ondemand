from django.conf import settings
from django.shortcuts import render, get_object_or_404

from rating.models import Rating
from .models import Tutor
from course.models import Course
from lesson.models import Lesson

from ondemand.views_decorator import check_project


@check_project
def detail_view(request, tutor_id):
    tutor = get_object_or_404(Tutor, id=tutor_id)
    content_type = settings.CONTENT_TYPE('course', 'course')

    lesson_list = []
    for lesson_id in tutor.lesson_tutor.values_list('id', flat=True).filter(tutor=tutor, is_display=True):
        lesson = Lesson.pull(lesson_id)
        if lesson is not None:
            lesson_list.append(lesson)

    course_list = []
    for course_id in tutor.course_set.values_list('id', flat=True).filter(is_display=True):
        course = Course.pull(course_id)
        course.rating = Rating.pull_first(content_type, course.id)
        if course is not None:
            course_list.append(course)

    return render(request,
                  'tutor/detail.html', 
                  {'DISPLAY_SUBMENU': True,
                   'ROOT_PAGE': 'tutor',
                   'tutor': tutor,
                   'is_owner': False,
                   'lesson_list': lesson_list,
                   'course_list': course_list})
