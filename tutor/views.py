from django.shortcuts import render, get_object_or_404

from .models import Tutor

from ondemand.views_decorator import check_project


@check_project
def home_view(request):
    tutor_list = Tutor.objects.filter(is_display=True).order_by('name')
    return render(request,
                  'tutor/home.html',
                  {'DISPLAY_SUBMENU': True,
                   'ROOT_PAGE': 'tutor',
                   'tutor_list': tutor_list
                   })

