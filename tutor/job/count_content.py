import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings

from tutor.models import Tutor

def _count_content(tutor_id):
    tutor = Tutor.objects.get(id=tutor_id)
    tutor.count_content()
