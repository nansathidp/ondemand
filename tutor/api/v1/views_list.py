from api.v5.views import json_render, check_key
from api.v5.views_decorator import param_filter
from .views import check_require
from provider.cached import cached_provider
from tutor.models import Tutor


@param_filter
def list_view(request):
    result = {
        'tutor_list': []
    }
    response = check_key(request)
    if response is not None:
        return response

    # Check Param
    code, token, uuid, store, install_version = check_require(request)
    if code != 200:
        return json_render(result, code)

    tutor_list = Tutor.pull_list()

    # Filter Provider
    if request.provider_id != 'all':
        provider = cached_provider(request.provider_id)
        if provider:
            tutor_list = tutor_list.filter(providers=provider)

    for tutor in tutor_list:
        result['tutor_list'].append(tutor.api_display_title())

    return json_render(result, code)
