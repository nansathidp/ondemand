from api.v4.views import json_render, check_key
from .views import check_require


def detail_view(request, tutor_id):
    from tutor.models import Tutor
    result = {}
    response = check_key(request)
    if response is not None:
        return response

    # Check Param
    code, token, uuid, store, install_version = check_require(request)
    if code != 200:
        return json_render(result, code)

    try:
        tutor = Tutor.objects.get(id=int(tutor_id))
    except Tutor.DoesNotExist:
        return json_render({}, 800)

    result['tutor'] = tutor.api_display(is_display_provider=False, is_display_category=False)
    result['course_list'] = []
    for course in tutor.course_set.filter(is_display=True):
        result['course_list'].append(course.api_display_title(store=store))

    # Change
    # Remove practice_list
    # Remove institute_list
    return json_render(result, code)
