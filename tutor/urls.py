from django.conf.urls import url

from .views import home_view
from .views_detail import detail_view

app_name = 'tutor'
urlpatterns = [
    url(r'^$', home_view, name='home'),  # TODO: testing
    url(r'^(\d+)/$', detail_view, name='detail'),  # TODO: testing
]
