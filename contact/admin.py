from django.contrib import admin
from contact.models import *

@admin.register(Contact)
class ContactTutorAdmin(admin.ModelAdmin):
    list_display = ('name','email','tel','subject', 'status', 'timestamp')
