from django.shortcuts import render
from api.views_api import json_render
from contact.models import Contact


def teach_view(request):
    return render(request,
                  'main/teach.html',
                  {'tagline': '- Best way to broadcast your courses -',
                   'logo_path': 'images/customize/logo-teach.png',
                   'TITLE': 'Best way to broadcast your courses',
                   'hidecontact': True})


def teach_action_view(request):
    result = {}
    code = 200
    try:
        institute = request.POST.get('institute', '')
        email = request.POST.get('email', '')
        tel = request.POST.get('tel', '')
        subject = request.POST.get('subject', '')
        Contact.objects.create(
            name=institute,
            email=email,
            tel=tel,
            subject=subject)
    except:
        code = 5000
    return json_render(result, code)
