from django.db import models

TUTOR_CONTACT_STATUS = (
    (1, 'Waiting'),
    (2, 'Complete'),
    (3, 'Reject'),
)


class Contact(models.Model):

    name = models.CharField(max_length=120)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
    )
    tel = models.CharField(max_length=120,blank=True)
    subject = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(choices=TUTOR_CONTACT_STATUS,default=1)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-timestamp']
