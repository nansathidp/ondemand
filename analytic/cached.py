from django.conf import settings
from django.core.cache import cache
from django.utils import timezone

from analytic.models import Stat

def cached_analytic_stat(code):
    key = '%s_analytic_stat_%s'%(settings.CACHED_PREFIX, code)
    result = cache.get(key)
    if result is None:
        stat = Stat.objects.filter(code=code).first()
        if stat is None:
            stat = Stat.objects.create(code=code,
                                       value=0)
        result = {'time': timezone.now(),
                  'stat': stat,
                  'stat_now': stat.value,
                  'value': 0,
                  'refer_list': []}
        cache.set(key, result, None)
    if 'stat_now' not in result: #TODO: remove for migrate only
        result['stat_now'] = result['stat'].value + result['value']
    return result

def cached_analytic_stat_update(code, result):
    from lesson.cached import cached_lesson
    key = '%s_analytic_stat_%s'%(settings.CACHED_PREFIX, code)
    cache.set(key, result, None)

def cached_analytic_course_view(course_id, is_force=False):
    key = '%s_analytic_course_view_%s'%(settings.CACHED_PREFIX, course_id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = {'timestamp': timezone.now(),
                  'stat': 0}
        cache.set(key, result, None)
    return result
    
def cached_analytic_course_view_update(course_id, result):
    key = '%s_analytic_course_view_%s'%(settings.CACHED_PREFIX, course_id)
    cache.set(key, result, None)

def cached_analytic_course_view_delete(course_id):
    key = '%s_analytic_course_view_%s'%(settings.CACHED_PREFIX, course_id)
    cache.delete(key)

def cached_analytic_lesson_view(lesson_id, is_force=False):
    key = '%s_analytic_lesson_view_%s'%(settings.CACHED_PREFIX, lesson_id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = {'timestamp': timezone.now(),
                  'stat': 0}
        cache.set(key, result, None)
    return result
    
def cached_analytic_lesson_view_update(lesson_id, result):
    key = '%s_analytic_lesson_view_%s'%(settings.CACHED_PREFIX, lesson_id)
    cache.set(key, result, None)

def cached_analytic_lesson_view_delete(lesson_id):
    key = '%s_analytic_lesson_view_%s'%(settings.CACHED_PREFIX, lesson_id)
    cache.delete(key)
