from django.db import models
from django.conf import settings


class Stat(models.Model):
    code = models.CharField(max_length=120, db_index=True)
    value = models.IntegerField()
    last_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s = %s'%(self.code, self.value)

    @staticmethod
    def push(code, refer, user_agent):
        from django.utils import timezone
        from .cached import cached_analytic_stat, cached_analytic_stat_update
        from lesson.cached import cached_lesson
        import datetime

        stat_result = cached_analytic_stat(code)
        now = timezone.now()
        date = now.date()
        is_push = False
        if stat_result['time'].date() != date:
            is_push = True
        elif now - stat_result['time'] > datetime.timedelta(minutes=20):
            is_push = True

        if is_push:
            stat = stat_result['stat']
            value = stat_result['value']+1
            stat.value += value
            stat.save(update_fields=['value', 'last_update'])
            Log.push(stat, date, value)
            if refer is not None or user_agent is not None:
                stat_result['refer_list'].append((refer, user_agent))
            Raw.push(stat, date, stat_result['refer_list'])
            stat_result['value'] = 0
            stat_result['stat_now'] = stat.value
            stat_result['time'] = now
            stat_result['refer_list'] = []
            cached_analytic_stat_update(code, stat_result)
            if code.find('lesson_') == 0:
                lesson = cached_lesson(code.split('_')[1], is_force=True)
        else:
            stat_result['value'] += 1
            stat_result['stat_now'] += 1
            if refer is not None or user_agent is not None:
                stat_result['refer_list'].append((refer, user_agent))
            cached_analytic_stat_update(code, stat_result)

    @staticmethod
    def push_value(code, date, value, is_cron=False):
        from django.db.models import Sum
        stat = Stat.objects.filter(code=code).first()
        if stat is None:
            stat = Stat.objects.create(code=code,
                                       value=value)
            Log.push(stat, date, value, is_cron=is_cron)
        else:
            Log.push(stat, date, value, is_cron=is_cron)
            if is_cron:
                try:
                    stat.value = int(stat.log_set.aggregate(Sum('value'))['value__sum'])
                except:
                    stat.value = 0
                stat.save(update_fields=['value', 'last_update'])
            else:
                stat.value += value
                stat.save(update_fields=['value', 'last_update'])

    @staticmethod
    def set_value(code, date, value):
        from django.utils import timezone
        from django.db.models import Sum
        from analytic.cached import cached_analytic_stat_update

        stat = Stat.objects.filter(code=code).first()
        if stat is None:
            stat = Stat.objects.create(code=code,
                                       value=value)
            Log.push(stat, date, value, is_cron=True)
        else:
            Log.push(stat, date, value, is_cron=True)
            try:
                stat.value = int(stat.log_set.aggregate(Sum('value'))['value__sum'])
            except:
                stat.value = 0
            stat.save(update_fields=['value', 'last_update'])
        cached_analytic_stat_update(code, {'time': timezone.now(),
                                           'stat': stat,
                                           'stat_now': stat.value,
                                           'value': 0,
                                           'refer_list': []})


class Log(models.Model):
    stat = models.ForeignKey(Stat)
    value = models.IntegerField()
    date = models.DateField(db_index=True)
    last_update = models.DateTimeField(auto_now=True)

    @staticmethod
    def push(stat, date, value, is_cron=False):
        log = Log.objects.filter(stat=stat,
                                 date=date).first()
        if log is None:
            Log.objects.create(stat=stat,
                               value=value,
                               date=date)
        else:
            if is_cron:
                log.value = value
            else:
                log.value += value
            log.save(update_fields=['value', 'last_update'])

    @staticmethod
    def pull(code_list, start, end):
        import datetime, time
        chart_result = {}
        start_code = {}
        for code in code_list:
            chart_result[code] = []
            start_code[code] = start
        for log in Log.objects.select_related('stat').filter(stat__code__in=code_list,
                                                             date__range=(start, end)).order_by('date'):
            if log.date == start_code[log.stat.code]:
                d = {}
                d['time'] = int(time.mktime(log.date.timetuple())) * 1000
                d['value'] = log.value
                chart_result[log.stat.code].append(d)
                start_code[log.stat.code] += datetime.timedelta(days=1)
            else:
                while log.date != start_code[log.stat.code]:
                    d = {}
                    d['time'] = int(time.mktime(start_code[log.stat.code].timetuple())) * 1000
                    d['value'] = 0
                    chart_result[log.stat.code].append(d)
                    start_code[log.stat.code] += datetime.timedelta(days=1)
                d = {}
                d['time'] = int(time.mktime(log.date.timetuple())) * 1000
                d['value'] = log.value
                chart_result[log.stat.code].append(d)
                start_code[log.stat.code] += datetime.timedelta(days=1)
        for code in code_list:
            while start_code[code] < end:
                d = {}
                d['time'] = int(time.mktime(start_code[code].timetuple())) * 1000
                d['value'] = 0
                chart_result[code].append(d)
                start_code[code] += datetime.timedelta(days=1)
        return chart_result


class Raw(models.Model):
    stat = models.ForeignKey(Stat)
    date = models.DateField(db_index=True)
    refer = models.TextField()

    @staticmethod
    def push(stat, date, refer_list):
        import json
        try:
            refer = json.dumps(refer_list)
        except:
            refer = ''
        if len(refer_list) > 0:
            Raw.objects.create(stat=stat,
                               date=date,
                               refer=refer)


class Refer(models.Model):
    stat = models.ForeignKey(Stat)
    refer = models.CharField(max_length=120)
    value = models.IntegerField()
    date = models.DateField(db_index=True)
    last_update = models.DateTimeField(auto_now=True)

    @staticmethod
    def pull(code, start, end):
        result = {}
        count = 0
        for refer in Refer.objects.filter(stat__code=code,
                                          date__range=(start, end)):
            count += refer.value
            if refer.refer in result:
                result[refer.refer] += refer.value
            else:
                result[refer.refer] = refer.value
        refer_list = []
        for key, value in result.items():
            d = {'refer': key,
                 'value': value,
                 'percent': int((float(value)/count)*100)}
            refer_list.append(d)
        return refer_list


class PageView(models.Model):
    page = models.CharField(max_length=32, blank=True, db_index=True)
    flavour = models.CharField(max_length=16, blank=True, db_index=True)
    source = models.CharField(max_length=16, blank=True, db_index=True)
    stat = models.IntegerField(default=0)
    timeupdate = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        ordering = ['-timeupdate']

    def __str__(self):
        return self.page

    @staticmethod
    def push(request):
        from api.views_ais_privilege import get_client_ip
        import datetime
        source = request.GET.get('source','')
        prefix_source = source
        if len(source) > 0:
            prefix_source = ' ( %s )' % source
        pageview, created = PageView.objects.get_or_create(page=request.path+prefix_source, flavour='-', source=source)
        pageview.stat += 1
        pageview.save(update_fields=['stat'])
        ip = get_client_ip(request)
        pageview_log = PageViewLog.objects.filter(page_key=pageview,
                                                  date=datetime.date.today(),
                                                  ip=ip).first()
        if pageview_log is None:
            pageview_log = PageViewLog.objects.create(page_key=pageview,
                                                      ip=ip,
                                                      stat=1,
                                                      date=datetime.date.today())
        else:
            pageview_log.stat += 1
            pageview_log.save(update_fields=['stat'])


class PageViewLog(models.Model):
    page_key = models.ForeignKey(PageView)
    ip = models.GenericIPAddressField(db_index=True)
    stat = models.IntegerField(default=0)
    date = models.DateField(db_index=True)
    timeupdate = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        ordering = ['-timeupdate']


class OrderFunnel(models.Model):
    cart = models.IntegerField(default=0)
    payment = models.IntegerField(default=0)
    success = models.IntegerField(default=0)
    date = models.DateField(db_index=True)
    timeupdate = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        ordering = ['-timeupdate']

