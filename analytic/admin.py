from django.contrib import admin
from analytic.models import *

@admin.register(Stat)
class StatAdmin(admin.ModelAdmin):
    list_display = ('code', 'value', 'last_update')

@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    list_display = ('stat', 'value', 'date', 'last_update')

@admin.register(Raw)
class RawAdmin(admin.ModelAdmin):
    list_display = ('stat', 'date')
    
@admin.register(Refer)
class RegerAdmin(admin.ModelAdmin):
    list_display = ('refer', 'value', 'date')
    
@admin.register(PageView)
class PageViewAdmin(admin.ModelAdmin):
    list_display = ('page', 'flavour', 'stat', 'source', 'timeupdate')

@admin.register(PageViewLog)
class PageViewLogLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'page_key', 'ip', 'date', 'stat', 'timeupdate')
    list_filter = ('page_key', 'date')

@admin.register(OrderFunnel)
class OrderFunnelAdmin(admin.ModelAdmin):
    list_display = ('cart', 'payment', 'success', 'date', 'timeupdate')


