from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt

from mpay.models import Request


@csrf_exempt
def inquiry_view(request, order_id):
    mpay_request = Request.objects.filter(order_id=order_id).order_by('timestamp').first()
    if mpay_request is not None:
        mpay_request.inquiry(app=request.APP, domain=request.get_host(), action=1)
    return redirect('order:history')
