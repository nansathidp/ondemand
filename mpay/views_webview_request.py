import json
import optparse
import urllib

from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import redirect
from django.core.urlresolvers import reverse

from mpay.models import Request as MpayRequest, MPAY_METHOD_MAP
from order.models import Order
from account.cached import cached_api_account_auth
from ondemand.views_base import response as views_response
from xml2json import xml2json


def request_webview_view(request, order_id):
    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)

    if uuid is None:
        return views_response(request, 202)
    if token is None:
        return views_response(request, 204)

    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return views_response(request, 703)
    except:
        return views_response(request, 209)

    account = cached_api_account_auth(token, uuid)
    if account is None:
        return views_response(request, 400)

    payment_type = request.GET.get('type', None)
    if payment_type is None:
        return redirect('order:checkout')

    order = Order.objects.get(id=order_id)
    order.method = MPAY_METHOD_MAP[payment_type]
    order.status = 2
    order.save(update_fields=['method', 'status'])
    redirect_url = '%s%s' % (settings.BASE_URL(request), reverse('mpay:web_response', args=[order.id]))
    mpay_request = MpayRequest.create(request, order, payment_type, redirect_url)
    options = optparse.Values({"pretty": False})
    json_result = json.loads(xml2json(mpay_request.response, options, 1))
    response_code = json_result['response']['respCode']
    if response_code == '0000':
        endpoint_url = urllib.parse.unquote(json_result['response']['endPointUrl'])
        return redirect(endpoint_url)
    return HttpResponse(mpay_request.response)
