from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from mpay.models import Response, Request
from order.models import Order


@csrf_exempt
def silence_view(request):
    order_id = request.POST.get('orderId', None)
    if order_id is None:
        return HttpResponse('FAIL')
    order = Order.objects.filter(id=order_id).first()
    if order is None:
        return HttpResponse('ORDER NOT FOUND')
    response = Response.create(request, order=order, type=1)

    # Check Inquiry ###
    # mpay_request = Request.objects.filter(order_id=order_id).order_by('timestamp').first()
    # if mpay_request is not None:
    #     mpay_request.inquiry(app=request.APP, domain=request.get_host(), action=2)


    if response is not None:
        if int(response.resp_code) == 0:
            if response.payment_status == 'SUCCESS':
                order.buy_success(request.APP, request.get_host().split(':')[0], order.method)

    return HttpResponse('SUCCESS')
