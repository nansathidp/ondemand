from django.shortcuts import redirect
from django.http import HttpResponse
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

from mpay.models import Request as MpayRequest, MPAY_METHOD_MAP
from mpay.job.inquiry import _inquiry
from order.models import Order

from xml2json import xml2json
import json
import optparse
import urllib

import django_rq

@login_required
def request_view(request, order_id):
    type = request.GET.get('type', None)
    if type is None:
        return redirect('order:checkout')
    order = Order.objects.get(id=order_id)
    order.method = MPAY_METHOD_MAP[type]
    order.status = 2
    order.save(update_fields=['method', 'status'])
    redirect_url = '%s%s' % (settings.BASE_URL(request), reverse('mpay:response', args=[order.id]))
    # redirect_url = '%s%s/' % (settings.CONFIG(request.get_host().split(':')[0], 'MPAY_RESPONSE'), order.id)
    mpay_request = MpayRequest.create(request, order, type, redirect_url)
    options = optparse.Values({"pretty": False})
    json_result = json.loads(xml2json(mpay_request.response, options, 1))
    response_code = json_result['response']['respCode']
    if response_code == '0000':
        endpoint_url = urllib.parse.unquote(json_result['response']['endPointUrl'])
        # django_rq.enqueue(_inquiry, mpay_request.id, request.APP.id, request.get_host().split(':')[0])
        return redirect(endpoint_url)
    return HttpResponse(mpay_request.response)
