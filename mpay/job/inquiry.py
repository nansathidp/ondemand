import os, sys, django

from django.conf import settings

sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from mpay.models import Inquiry, Request
from app.models import App
import time

import django_rq

# SLEEP_TIME = 60*60*3
SLEEP_TIME = 10


def _inquiry(mpay_request_id, app_id, domain):
    request = Request.objects.get(id=mpay_request_id)
    app = App.objects.get(id=app_id)
    if not request.inquiry(app, domain, action=2):
        time.sleep(SLEEP_TIME)
        if Inquiry.objects.filter(order_id=request.order.id).count() < 10:
            django_rq.enqueue(_inquiry, mpay_request_id, app_id, domain)



