from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt

from mpay.models import Response, Request
from order.models import Order


@csrf_exempt
def response_view(request, order_id):
    order = Order.objects.get(id=order_id)
    response = Response.create(request, order=order, type=0)

    # Check Inquiry ###
    mpay_request = Request.objects.filter(order_id=order_id).order_by('timestamp').first()
    if mpay_request is not None:
        #mpay_request.inquiry(app=request.APP, domain=request.get_host(), action=0, type=0)
        mpay_request.inquiry(app=request.APP, domain=request.get_host(), action=0)

    # @Deprecated
    # if response is not None:
    #     if response.resp_code == 0:
    #         order.buy_success(request.APP, request.get_host().split(':')[0], 18)

    return redirect('profile')
