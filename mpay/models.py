from django.db import models
from django.conf import settings


MPAY_METHOD_MAP = {
    '1': 18,
    '4': 19,
    '5': 20
}


class Request(models.Model):
    order = models.ForeignKey('order.Order', related_name='mpay_request_set', null=True, blank=True)
    request = models.TextField()
    response = models.TextField(blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    #URL = 'https://saichon-beauty.ais.co.th:8002/AISMPAYPartnerInterface/InterfaceService?'
    URL = 'https://www.mpay.co.th/AISMPAYPartnerInterface/InterfaceService'

    @staticmethod
    def create(request, order, type, redirect_url):
        import hashlib
        import requests
        domain = request.get_host().split(':')[0]

        sid = settings.CONFIG(domain, 'MPAY_SID')
        merchant_id = settings.CONFIG(domain, 'MPAY_MERCHANT_ID')
        amount = int(order.net * 100)
        secret_key = settings.CONFIG(domain, 'MPAY_SECRET_KEY')
        integrity_str = '%s%s%s%s%s' % (sid, merchant_id, order.id, amount, secret_key)
        payload = {'projectCode': settings.CONFIG(domain, 'MPAY_PROJECT_CODE'),
                   'command': 'RequestOrderTepsApi',
                   'sid': sid,
                   'redirectUrl': redirect_url,
                   'merchantId': merchant_id,
                   'orderId': order.id,
                   'currency': 'THB',
                   'purchaseAmt': amount,
                   'paymentMethod': type,
                   'productDesc': '',
                   'ref1': '',
                   'ref2': '',
                   'ref3': '',
                   'ref4': '',
                   'ref5': '',
                   'integrityStr': hashlib.sha256(integrity_str.encode('utf-8')).hexdigest()}

        headers = {'content-type': 'application/x-www-form-urlencoded'}
        #data = urllib.parse.urlencode(payload)
        #req = urllib2.Request(url, data, headers=headers)
        #response = urllib2.resuest.urlopen(req, context=ctx)
        response = requests.post(Request.URL, data=payload, headers=headers)
        request = Request.objects.create(request=payload, response=response.text, order=order)
        return request

    def inquiry(self, app, domain, action=-1):
        import requests
        from xml2json import xml2json
        import json
        import optparse

        response_status = False

        # Check Inquiry
        merchant_id = settings.CONFIG(domain, 'MPAY_MERCHANT_ID')
        order_id = self.order_id
        amount = int(self.order.net * 100)
        options = optparse.Values({"pretty": False})
        json_result = json.loads(xml2json(self.response, options, 1))

        try:
            sale_id = json_result['response']['saleId']
        except:
            return response_status

        payload = {'projectCode': settings.CONFIG(domain, 'MPAY_PROJECT_CODE'),
                   'command': 'InquiryApi',
                   'merchantId': merchant_id,
                   'orderId': order_id,
                   'purchaseAmt': amount,
                   'saleId': sale_id}

        headers = {'content-type': 'application/x-www-form-urlencoded'}
        response = requests.post(Request.URL, data=payload, headers=headers)
        inquiry_response_str = response.text

        #Check Order
        try:
            inquiry_response = json.loads(xml2json(inquiry_response_str, options, 1))
            inquiry_response_status = inquiry_response['response']['status']
            inquiry_response_code = inquiry_response['response']['respCode']
            inquiry_payment_status = inquiry_response['response']['paymentStatus']
        except:
            inquiry_response_status = 'Except'
            inquiry_response_code = -1
            inquiry_payment_status = 'FAIL'

        inquiry = Inquiry.objects.create(order_id=order_id, request=payload, response=inquiry_response_str,
                                         action=action, status=inquiry_response_status, payment_status=inquiry_payment_status)

        if inquiry_payment_status == 'SUCCESS':
            response_status = True
            inquiry.order.buy_success(app, domain, inquiry.order.method)

        return response_status


class Response(models.Model):
    TYPE_EXTRA = (
        (-1, '-'),
        (0, 'FRONT'),
        (1, 'BACK'),
    )

    order = models.ForeignKey('order.Order', related_name='mpay_response_set', null=True, blank=True)
    status = models.CharField(max_length=2, default='-')
    payment_status = models.CharField(max_length=16, default='-')
    type = models.IntegerField(choices=TYPE_EXTRA, default=-1)
    response = models.TextField(blank=True)
    resp_code = models.IntegerField(default=-1)
    timestamp = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def create(request, order, type=-1):
        import json
        response = None
        if request.method == 'POST':
            result = {'status': request.POST.get('status', None),
                      'respCode': request.POST.get('respCode', None),
                      'orderId': request.POST.get('orderId', None),
                      'incCustomerFee': request.POST.get('incCustomerFee', None),
                      'currency': request.POST.get('currency', None),
                      'exchangeRate': request.POST.get('exchangeRate', None),
                      'redirectFlag': request.POST.get('redirectFlag', None),
                      'excCustomerFee': request.POST.get('excCustomerFee', None),
                      'paymentStatus': request.POST.get('paymentStatus', None),
                      'tranId': request.POST.get('tranId', None),
                      'urlForward': request.POST.get('urlForward', None),
                      'respDesc': request.POST.get('respDesc', None),
                      'amount': request.POST.get('amount', None),
                      'saleId': request.POST.get('saleId', None),
                      'orderExpireDate': request.POST.get('orderExpireDate', None),
                      'purchaseAmt': request.POST.get('purchaseAmt', None)}
            body = json.dumps(result)
            response = Response.objects.create(response=body,
                                               order=order,
                                               type=type,
                                               status=request.POST.get('status', '-'),
                                               payment_status=request.POST.get('paymentStatus', None),
                                               resp_code=int(request.POST.get('respCode', -1)))
        return response

    def update(self):
        import json
        response = json.loads(self.response)
        if int(response['respCode']) == 0:
            self.payment_status = response['paymentStatus']
            self.save(update_fields=['payment_status'])


class Inquiry(models.Model):

    ACTION_EXTRA = (
        (-1, '-'),
        (0, 'ACTION BY RESPONSE'),
        (1, 'ACTION BY USER'),
        (2, 'ACTION BY SYSTEM'),
        (3, 'ACTION BY ADMIN'),
    )

    order = models.ForeignKey('order.Order', related_name='mpay_inquiry_set', null=True, blank=True)
    status = models.CharField(max_length=8, default='-')
    payment_status = models.CharField(max_length=16, default='-')
    action = models.IntegerField(choices=ACTION_EXTRA, default=-1)
    request = models.TextField()
    response = models.TextField(blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def update(self):
        from xml2json import xml2json
        import json
        import optparse
        options = optparse.Values({"pretty": False})
        response = json.loads(xml2json(self.response, options, 1))
        if int(response['response']['respCode']) == 0:
            self.payment_status = response['response']['paymentStatus']
            self.save(update_fields=['payment_status'])

