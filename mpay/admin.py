from django.contrib import admin
from mpay.models import Request, Response, Inquiry


@admin.register(Request)
class RequestAdmin(admin.ModelAdmin):
    list_display = ('id', 'order', 'response', 'timestamp')
    actions = ['send', 'inquiry']
    search_fields = ['order__id']
    readonly_fields = ['order']

    def send(self, request, queryset):
        for mpay in queryset:
            mpay.send()

    def inquiry(self, request, queryset):
        for mpay_request in queryset:
            mpay_request.inquiry(app=request.APP, domain=request.get_host().split(':')[0], action=1)


@admin.register(Response)
class ResponseAdmin(admin.ModelAdmin):
    list_display = ('id', 'order', 'payment_status', 'type', 'timestamp')
    search_fields = ['order__id']
    readonly_fields = ['order']
    actions = ['update']

    def update(self, request, queryset):
        for response in queryset:
            response.update()


@admin.register(Inquiry)
class InquiryAdmin(admin.ModelAdmin):
    list_display = ('id', 'order', 'payment_status', 'status', 'action', 'timestamp')
    actions = ['update']
    search_fields = ['order__id']
    readonly_fields = ['order']

    def update(self, request, queryset):
        for inquiry in queryset:
            inquiry.update()

