from django.conf.urls import url

from . import views_request, views_response, views_webview_request, views_webview_response, views_inquiry
from .views_silence import silence_view

urlpatterns = [
    url(r'^request/(\d+)/$', views_request.request_view, name='request'), #TODO: testing
    url(r'^webview/request/(\d+)/$', views_webview_request.request_webview_view, name='web_request'), #TODO: testing

    url(r'^response/(\d+)/$', views_response.response_view, name='response'), #TODO: testing
    url(r'^webview/response/(\d+)/$', views_webview_response.response_webview_view, name='web_response'), #TODO: testing

    url(r'^silence/$', silence_view), #TODO: testing
    url(r'^inquiry/(\d+)/$', views_inquiry.inquiry_view, name='inquiry') #TODO: testing
]
