from api.views_api import STATUS_MSG
from django.shortcuts import render


def response(request, code):
    return render(request,
                  'response.html',
                  {'response': STATUS_MSG[code],
                   'code': code})
