from django.db import models


class Price(models.Model):
    import datetime
    from store.models import STORE_CHOICES

    content_type = models.ForeignKey('contenttypes.ContentType')
    content = models.BigIntegerField(db_index=True)

    price = models.FloatField(default=0.0)
    discount = models.FloatField(default=-1.0)

    store = models.IntegerField(choices=STORE_CHOICES)
    credit = models.DurationField(default=datetime.timedelta(0)) #POS, POS OnTop
    ios = models.ForeignKey('purchase.Apple', null=True) #IOS InApp-Purchase
    is_use = models.BooleanField(default=True)

    EXCHANGE_RATE = 35

    @property
    def net(self):
        if self.discount < 0:
            return self.price
        else:
            return self.discount

        # if self.store == 3:
        #     if self.ios is not None:
        #         if self.ios.discount_tier is None:
        #             if self.ios.price_tier is not None:
        #                 return self.ios.price_tier.price_usd
        #         else:
        #             if self.ios.discount_tier is not None:
        #                 return self.ios.discount_tier.price_usd
        # else:
        #     if self.discount == -1.0:
        #         return self.price
        #     else:
        #         return self.discount
        return -1

    @property
    def is_free(self):
        return self.net == 0

    @staticmethod
    def init(content_type, content):
        for store in [1, 3, 4, 5, 6]:
            Price.pull(content_type, content, store)

    @staticmethod
    def pull(content_type, content, store):
        import datetime
        price = Price.objects.filter(content_type=content_type,
                                     content=content,
                                     store=store).first()
        if price is None:
            price = Price.objects.create(content_type=content_type,
                                         content=content,
                                         price=0.0,
                                         discount=-1.0,
                                         store=store,
                                         credit=datetime.timedelta(0),
                                         ios=Price.pull_ios(content_type, content),
                                         is_use=False)
        return price

    @staticmethod
    def pull_ios(content_type, content):
        from purchase.models import PriceMatrix, Apple
        apple = Apple.objects.filter(content_type=content_type,
                                     content=content).first()
        if apple is None:
            apple = Apple.push('', content_type, content, PriceMatrix.objects.first(), PriceMatrix.objects.first())
        else:
            return apple

    def api_display(self):
        price_ios_set = None
        if self.ios is not None:
            price_ios_set = self.ios.api_display_price_tier()

        return {'id': self.id,
                'price': self.price,
                'discount': self.discount,
                'net': self.net,
                'store': self.store,
                'ios_set': price_ios_set,
                'store_display': self.get_store_display()
                }
