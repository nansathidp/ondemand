from django.contrib import admin

from .models import Price


@admin.register(Price)
class PriceAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'content', 'price', 'discount', 'store', 'credit', 'ios', 'is_use')

