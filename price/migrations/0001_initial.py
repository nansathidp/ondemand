# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-16 10:52
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.BigIntegerField(db_index=True)),
                ('price', models.FloatField(default=0.0)),
                ('discount', models.FloatField(default=-1.0)),
                ('store', models.IntegerField(choices=[(-1, 'All'), (0, 'Stater'), (1, 'Web'), (2, 'Api PaysBuy'), (3, 'IOS InApp-Purchase'), (4, 'Android InApp-Purchase'), (5, 'POS'), (6, 'POS OnTop')])),
                ('credit', models.DurationField(default=datetime.timedelta(0))),
                ('is_use', models.BooleanField(default=True)),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
            ],
        ),
    ]
