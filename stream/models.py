from django.db import models
from django.conf import settings


class Config(models.Model):
    TYPE_CHOICES = (
        (2, 'Youtube'),
        (3, 'CloudFront'),
        (4, 'WowZa'),
    )
    
    ENCRYPT_CHOICES = (
        (1, 'SHA-256'),
        (2, 'SHA-384'),
        (3, 'SHA-512'),
    )

    stream_type = models.IntegerField(choices=TYPE_CHOICES)
    name = models.CharField(max_length=120)
    shared_secret = models.CharField(max_length=32, blank=True)
    wowza_app = models.CharField(max_length=32, blank=True)
    wowza_param_prefix = models.CharField(max_length=32, blank=True)
    wowza_stream_lock = models.CharField(max_length=120, blank=True)
    wowza_hash_type = models.IntegerField(choices=ENCRYPT_CHOICES)
    is_default = models.BooleanField(default=False)
    
    def __str__(self):
        return self.name
    
    @staticmethod
    def pull(config_id):
        if config_id is None:
            return Config.objects.filter(is_default=True).first()
        else:
            config = Config.objects.filter(id=config_id).first()
            if config is None:
                return Config.objects.filter(is_default=True).first()
            else:
                return config

    def wowza_hash(self, media, ip, type, is_secure=True):
        from django.conf import settings
        import time, hashlib, base64
        if ip == '127.0.0.1': # for debug in localhost
            ip = settings.MY_IP
        starttime = int(time.time())
        endtime = int(time.time()) + (60*60)
        if type == 'dash':
            type_file = 'manifest.mpd'
        else: #HLS
            type_file = 'playlist.m3u8'
        path = ('%s/%s/_definst_/mp4:%s/%s'%(self.wowza_stream_lock,
                                             self.wowza_app, media,
                                             type_file)).strip()
        param = '%s&%s&%sendtime=%s&%sstarttime=%s'%(ip, self.shared_secret,
                                                     self.wowza_param_prefix, endtime,
                                                     self.wowza_param_prefix, starttime)
        before_hash_string = '%s/_definst_/mp4:%s?%s'%(self.wowza_app,
                                                       media,
                                                       param)
        before_hash_string = before_hash_string.encode('utf-8')
        if self.wowza_hash_type == 1:
            hash = hashlib.sha256(before_hash_string).digest()
        elif self.wowza_hash_type == 2:
            hash = hashlib.sha384(before_hash_string).digest()
        elif self.wowza_hash_type == 3:
            hash = hashlib.sha512(before_hash_string).digest()
        hash_encode = base64.urlsafe_b64encode(hash)
        param_string = ('?%sendtime=%s&%sstarttime=%s&%shash=%s'%(self.wowza_param_prefix, endtime,
                                                                  self.wowza_param_prefix, starttime,
                                                                  self.wowza_param_prefix, hash_encode))
        if is_secure:
            path = '%s%s' % (path, param_string)

        return path


class Allow(models.Model):
    config = models.ForeignKey(Config)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='stream_allow_set')
    is_default = models.BooleanField(default=False)
    
    @staticmethod
    def pull(account, config_id):
        if account is None:
            return None
        if config_id is not None:
            config = Config.pull(config_id)
            if config is None:
                return None
            allow = Allow.objects.filter(account=account,
                                         config_id=config_id).first()
            if allow is None:
                allow = Allow.objects.create(account=account,
                                             config=config,
                                             is_default=False)
        else:
            allow = Allow.objects.filter(account=account,
                                         is_default=True).first()
        if allow is None:
            config = Config.pull(None)
            if config is None:
                return None
            allow = Allow.objects.create(config=config,
                                         account=account,
                                         is_default=True)
        return allow


class Stream(models.Model):
    QUALITY_CHOICES = (
        (0, '720p'),
        (1, '360p'),
    )

    config = models.ForeignKey(Config)
    #video = models.ForeignKey('course.Video', null=True) #Mark Remove
    material = models.ForeignKey('course.Material', null=True)
    media = models.CharField(max_length=256)
    quality = models.IntegerField(choices=QUALITY_CHOICES, default=0, db_index=True)
    is_public = models.BooleanField(default=True)
    is_verify_pass = models.BooleanField(default=False)
    
    class Meta:
        ordering = ['quality']
        
    @staticmethod
    def pull(material, account, config_id):
        if material.type != 0:
            return None
        elif material.video_type not in [2, 3]:
            return None
        allow = Allow.pull(account, config_id)
        if allow is None:
            if material.is_public:
                config = Config.objects.filter(is_default=True).first()
            else:
                return None
        else:
            config = allow.config
        stream = Stream.objects.filter(config=config, material=material, is_public=True).first()
        return stream

    @staticmethod
    def pull_material(material):
        if material.type != 0:
            return None
        elif material.video_type != 2:
            return None
        config = Config.objects.filter(is_default=True).first()
        stream = Stream.objects.filter(config=config, material=material, is_public=True).first()
        if stream is None:
            stream = Stream.objects.create(config=config,
                                           material=material,
                                           media=material.data,
                                           is_public=True)
        return stream

    @staticmethod
    def nimble_sign(ip, key):
        from django.conf import settings
        import base64
        import hashlib
        from time import gmtime, strftime

        #Docs: http://blog.wmspanel.com/2013/11/nimble-streamer-protect-hotlinking-domain-lock.html

        if ip == '127.0.0.1': # for debug in localhost
            ip = settings.MY_IP

        today = strftime("%m/%d/%Y %I:%M:%S %p", gmtime())
        validminutes = "10"

        m = hashlib.md5()
        m.update((ip + key + today + validminutes).encode())
        base64hash = base64.b64encode(m.digest()).decode()
        urlsignature = "server_time=" + today + "&hash_value=" + base64hash + "&validminutes=" + validminutes

        url_sign_base64 = base64.b64encode(urlsignature.encode()).decode()
        return url_sign_base64
    
    def get_url(self, ip):
        import time, hashlib, base64
        if ip == '127.0.0.1': # for debug in localhost
            ip = '49.228.86.72'
        starttime = int(time.time())
        endtime = int(time.time()) + (60*60)
        path = ('http://103.246.16.183:1935/%s/_definst_/mp4:%s/playlist.m3u8' % (self.config.wowza_app, self.media)).strip()
        param = '%s&%s&%sendtime=%s&%sstarttime=%s' % (ip, self.config.shared_secret, self.config.wowza_param_prefix, endtime, self.config.wowza_param_prefix, starttime)
        before_hash_string = '%s/_definst_/mp4:%s?%s' % (self.config.wowza_app, self.media, param)
        before_hash_string = before_hash_string.encode('utf-8')
        if self.config.wowza_hash_type == 1:
            hash = hashlib.sha256(before_hash_string).digest()
        elif self.config.wowza_hash_type == 2:
            hash = hashlib.sha384(before_hash_string).digest()
        elif self.config.wowza_hash_type == 3:
            hash = hashlib.sha512(before_hash_string).digest()
        hash_encode = base64.urlsafe_b64encode(hash)
        param_string = ('?%sendtime=%s&%sstarttime=%s&%shash=%s'%(self.config.wowza_param_prefix, endtime,
                                                                  self.config.wowza_param_prefix, starttime,
                                                                  self.config.wowza_param_prefix, hash_encode))
        return '%s%s' % (path, param_string)

    @staticmethod
    def get_url_mobile_web(material, ip):
        stream = Stream.pull_material(material)
        config = stream.config
        return {'hls': config.wowza_hash(stream.media, ip, 'hls'),
                'dash': config.wowza_hash(stream.media, ip, 'dash')}

    @staticmethod
    def get_mobile_quality(video, ip, quality):
        stream = Stream.objects.select_related('config').filter(config_id=1446185990209523,
                                                                video=video,
                                                                quality=quality,
                                                                is_public=True).first()
        if stream is None:
            return None
        else:
            config = stream.config
            return {'hls': config.wowza_hash(stream.media, ip, 'hls'),
                    'dash': config.wowza_hash(stream.media, ip, 'dash')}

    @staticmethod
    def get_url_mobile_web_quality(material, ip):
        stream_list = Stream.objects.select_related('config').filter(config__is_default=True,
                                                                     material=material,
                                                                     is_public=True)

        stream = None
        config = None
        quality_list = []
        quality_default = '720p'
        for stream_item in stream_list:
            quality_list.append(stream_item.get_quality_display())
            if stream is None:
                config = stream_item.config
                stream = stream_item
                quality_default = stream_item.get_quality_display()

        if config is None:
            return None
        else:
            return {'quality_list': quality_list,
                    'quality_default': quality_default,
                    'hls': config.wowza_hash(stream.media, ip, 'hls'),
                    'hls_noen': config.wowza_hash(stream.media, ip, 'hls', is_secure=False),
                    'dash': config.wowza_hash(stream.media, ip, 'dash'),
                    'dash_noen': config.wowza_hash(stream.media, ip, 'dash', is_secure=False)}
