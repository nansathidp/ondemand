from django.contrib import admin
from .models import Config, Allow, Stream


@admin.register(Config)
class ConfigAdmin(admin.ModelAdmin):
    list_display = ('stream_type', 'name',
                    'wowza_stream_lock',
                    'is_default')


@admin.register(Allow)
class AllowAdmin(admin.ModelAdmin):
    list_display = ('config', 'account', 'is_default')
    search_fields = ['account__email']
    list_filter = ('config', 'is_default')
    actions = ['make_published','make_unpublished']

    def make_published(self, request, queryset):
        queryset.update(is_default=True)
    make_published.short_description = "Set default"

    def make_unpublished(self, request, queryset):
        queryset.update(is_default=False)
    make_unpublished.short_description = "Unset queryset"


@admin.register(Stream)
class StreamAdmin(admin.ModelAdmin):
    list_display = ('config_id_display', 'material_id_display', 'media', 'is_public', 'is_verify_pass')
    search_fields = ['material__id']
    actions = ['reset_verify']

    def config_id_display(self, obj):
        return obj.config_id
    config_id_display.short_description = 'Config Id'

    def material_id_display(self, obj):
        return obj.material_id
    material_id_display.short_description = 'Material Id'
    
    def reset_verify(self, request, queryset):
        queryset.update(is_verify_pass=False)
    reset_verify.short_description = "Reset Verify"

