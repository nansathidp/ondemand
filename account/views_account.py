import urllib

from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from account.login_policy import LoginPolicy
from account.models import Account
from .api.v1.views import json_render


@csrf_exempt
@require_POST
def account_auth(request):
    result = {}
    code = 200
    try:
        name = request.POST.get('fullname', None)
        email = request.POST.get('email', None)
        tel = request.POST.get('tel', None)
        password = request.POST.get('password', None)
        ref = request.POST.get('ref', None)
        email = urllib.parse.unquote(email)
        password = urllib.parse.unquote(password)
        if ref is None:
            result['link'] = request.META.get('HTTP_REFERER')
        else:
            result['link'] = ref
        if name is not None and email is not None and password is not None:
            if Account.objects.filter(email=email).count() == 0:
                account = Account.objects.create_user(email, password)
                account.first_name = name
                account.tel = tel
                account.backend = 'django.contrib.auth.backends.ModelBackend'
                account.login_token()
                if account.first_active is None:
                    account.first_active = timezone.now()
                account.last_active = timezone.now()
                account.save()
                login(request, account)
                session = request.COOKIES.get('sessionid')
            else:
                code = 310
        else:
            code = 300
    except:
        code = 311
    return json_render(result, code)


@csrf_exempt
@require_POST
def account_login(request):
    result = {}
    code = 200
    try:
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        ref = request.POST.get('ref', None)
        next = request.POST.get('next', None)

        email = urllib.parse.unquote(email)
        password = urllib.parse.unquote(password)

        link = None
        if next is not None:
            result['link'] = next
        elif ref is None:
            result['link'] = request.META.get('HTTP_REFERER')
        else:
            result['link'] = ref

        if email is not None and password is not None:
            account = authenticate(email=email, password=password)
            if account is not None:
                login_policy = LoginPolicy(account=account, request=request)
                if login_policy.is_validate():
                    login_policy.login()
                result = login_policy.get_result()
                return JsonResponse(result)
            else:
                return HttpResponse(status=401, content='The username or password were incorrect.')
        else:
            code = 300
            return json_render(result, code)
    except:
        skip = request.POST.get('skip', None)
        if skip == '1':
            request.session["is_skip"] = True
        else:
            code = 306
    return json_render(result, code)


def logout_view(request):
    if request.user.is_authenticated():
        if settings.PROJECT == 'ais' and 'saml' in settings.INSTALLED_APPS:
            return redirect('/saml/?slo')
        logout(request)
        return redirect('home')
    else:
        return redirect('home')
