from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.conf import settings

from order.models import Item

from ondemand.views_decorator import check_project
from progress.cached import cached_progress_account_content


@check_project
@login_required
def live_view(request):
    provider = None
    content_type_live = settings.CONTENT_TYPE('live', 'live')
    item_list = Item.objects.filter(order__account=request.user,
                                    order__status=3,
                                    status=2,
                                    content_type_id=content_type_live.id).order_by('-order__timestamp')
    for item in item_list:
        item.get_content_cached()
        # item.get_progress_cached()

    account_content = cached_progress_account_content(request.user.id, provider)
    return render(request,
                  'account/profile_live.html',
                  {'account': request.user,
                   'is_owner': True,
                   'select': 'live',
                   'item_list': item_list,
                   'account_content': account_content})
