from axes.decorators import watch_login
from django.conf import settings
from django.conf.urls import url

from . import views_account, views_facebook, views_address
from . import views_edit
from . import views_info
from .views_change_password import change_password_view
from .views_library import library_view
from .views_live import live_view
from .views_profile import profile_view
from .views_program import program_view
from .views_question import question_view

app_name = 'account'
urlpatterns = [
    url(r'^auth/$', views_account.account_auth),  # js # TODO: testing
    url(r'^logout/$', views_account.logout_view, name='logout'),  # TODO: testing
    url(r'^fblogin/$', views_facebook.account_fblogin),  # js # TODO: testing
    url(r'^fbconnect/$', views_facebook.verify_facebook),  # js # TODO: testing
    url(r'^address/$', views_address.account_address_view),  # js # TODO: testing
    url(r'^image/upload/$', views_edit.upload_view, name='image_upload'),  # js #TODO: testing
]

if settings.PROJECT == 'unilever':
    from unilever.views_login import login_view
    from unilever.views_push import push_view  # Not Encrypt
    from unilever.views_request import request_view  # Encrypt
    urlpatterns += [
        url(r'^login/$', login_view),  # js # TODO: testing
        url(r'^push/$', push_view),  # TODO: testing
        url(r'^request/$', request_view),  # TODO: testing
    ]
elif settings.PROJECT == 'ais':
    from ais.views_login import login_view
    urlpatterns += [
        url(r'^login/$', login_view),  # js # TODO: testing
    ]
else:
    urlpatterns += [
        url(r'^login/$', watch_login(views_account.account_login)),  # js # TODO: testing
    ]

urlpatterns_profile = [
    url(r'^$', profile_view, name='profile'),
    url(r'^info/$', views_info.view, name='profile_info'),
    url(r'^library/$', library_view, name='profile_library'),
    url(r'^program/$', program_view, name='profile_program'),
    url(r'^question/$', question_view, name='profile_question'),
    url(r'^live/$', live_view, name='profile_live'),

    url(r'^password/change/$', change_password_view, name='change_password'),

    url(r'^edit/$', views_edit.edit_view, name='profile_edit'),
    url(r'^edit/action/$', views_edit.action_view, name='profile_edit_action'),
]
