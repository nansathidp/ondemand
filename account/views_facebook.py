from django.http import JsonResponse
from django.contrib.auth import login
from account.models import Account
from django.conf import settings
from django.utils import timezone
from open_facebook import OpenFacebook

import random, string
import datetime


def verify_facebook(request):
    result = {}
    code = 200
    if not request.user.is_authenticated():
        return JsonResponse({'status': 460,
                             'status_msg': 460,
                             'result': result})
    result['link'] = request.META.get('HTTP_REFERER')
    access_token = request.POST.get('access_token', None)
    if access_token is not None and len(access_token) != 0:
        try:
            graph = OpenFacebook(access_token=access_token, version='v2.5')
            profile = graph.get('me', fields='id,name,email,birthday,gender')
            if Account.objects.filter(facebook_id=profile['id']).count() == 0 or request.user.facebook_id == profile['id']:
                account = request.user
                account.is_login_facebook = True
                account.facebook_access_token = access_token
                account.facebook_id = profile['id']
                account.save()
            else:
                return JsonResponse({'status': 307,
                                     'status_msg': 307,
                                     'result': result})
        except:
            code = 5000
    else:
        code = 308
    return JsonResponse({'status': code,
                         'status_msg': code,
                         'result': result})


def account_fblogin(request):
    try:
        result = {}
        code = 200
        access_token = request.POST.get('access_token', request.GET.get('access_token', None))
        user_email = request.POST.get('email', request.GET.get('email', -1))
        ref = request.POST.get('ref', request.GET.get('ref', None))
        if access_token is None or len(access_token) == 0:
            code = 30
        else:
            graph = OpenFacebook(access_token=access_token, version='v2.5')
            profile = graph.get('me', fields='id,name,email,birthday,gender')
            # graph = facebook.GraphAPI(access_token=access_token)
            # profile = graph.get_object('me')
            if 'email' in profile:
                email = profile['email']
            elif user_email != '-1' and len(user_email) > 3:
                email = user_email
            else:
                email = None

            if email is not None:
                account_list = Account.objects.filter(email=email)
            else:
                account_list = Account.objects.filter(facebook_id=profile['id'])

            if account_list.count() == 0:
                password = ''.join(
                    random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(12))
                if email is not None:
                    can_send_verify_email = True
                else:
                    email = '%s@facebook.com.gen' % (profile['id'])
                    can_send_verify_email = False
                    #return json_render(result, 312)
                    return JsonResponse({'status': 312,
                                         'status_msg': 312,
                                         'result': result})

                # Create
                account = Account.objects.create_user(email, password)
                # Update
                account.facebook_id = profile['id']
                if 'name' in profile:
                    account.first_name = profile['name']
                if 'sername' in profile:
                    account.last_name = profile['sername']
                if 'gender' in profile:
                    if profile['gender'].lower() == 'm' or profile['gender'].lower() == 'male':
                        account.gender = 1
                    elif profile['gender'].lower() == 'f' or profile['gender'].lower() == 'female':
                        account.gender = 2
                    else:
                        account.gender = 0
                if 'birthday' in profile:
                    try:
                        account.birthday = datetime.datetime.strptime(profile['birthday'], '%d/%m/%Y').date()
                    except:
                        account.birthday = None
                account.is_login_facebook = True
                account.facebook_access_token = access_token
                if not account.is_upload:
                    account.api_facebook_image()
                # account.api_facebook_friend(graph)
                if account.is_active:
                    account.backend = 'django.contrib.auth.backends.ModelBackend'
                    account.login_token()
                    login(request, account)
                    account.last_active = timezone.now()
                    account.save()
                    result['profile'] = account.api_profile()
                account.save()
                if can_send_verify_email:
                    from mailer.models import Mailer
                    contact_email = settings.CONFIG(request.get_host(), 'CONTACT_EMAIL')
                    site_url = request.get_host()
                    Mailer.send_verify_email(account, contact_email, site_url)
            else:
                account = account_list[0]
                # account.api_facebook_friend(graph)
                if account.is_active:
                    code = 200
                    result['profile'] = account.api_profile()
                    account.backend = 'django.contrib.auth.backends.ModelBackend'
                    login(request, account)
                    account.last_active = timezone.now()
                    account.save()
                    result['SITE_URL'] = settings.SITE_URL

        if ref is None:
            result['link'] = request.META.get('HTTP_REFERER')
        else:
            result['link'] = ref
    except:

        code = 301
    #return json_render(result, 200)
    return JsonResponse({'status': 200,
                         'status_msg': 200,
                         'result': result})

