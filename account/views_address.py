from django.http import JsonResponse
from .cached import cached_api_account_auth
from ondemand.views_decorator import check_project
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
@check_project
def account_address_view(request):
    code = 200
    account = None
    name = request.POST.get('name', None)
    address = request.POST.get('address', None)
    subdistrict = request.POST.get('subdistrict', None)
    district = request.POST.get('district', None)
    province = request.POST.get('province', None)
    zipcode = request.POST.get('zipcode', None)
    tel = request.POST.get('tel', None)
    token = request.POST.get('token', None)
    uuid = request.POST.get('uuid', None)
    if request.user.is_authenticated():
        account = request.user
    else:
        if uuid is None:
            return JsonResponse({'status': 202,
                                 'status_msg': 202,
                                 'result': {}})

        if token is None:
            return JsonResponse({'status': 204,
                                 'status_msg': 204,
                                 'result': {}})
        account = cached_api_account_auth(token, uuid)

    if account is None:
        return JsonResponse({'status': 309,
                                 'status_msg': 309,
                             'result': {}})
    try:
        account.set_user_address(name, address, subdistrict, district, province, zipcode, tel)
    except:
        code = 455
    return JsonResponse({'status': code,
                         'status_msg': code,
                         'result': {}})
