import random
import string

from django.core.cache import cache
from django.test import TestCase
from django.urls import reverse
from .models import Account


def _random():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(8))


class UserTests(TestCase):

    def setUp(self):
        cache._cache.flush_all()
        self.user = Account.objects.create_user('%s@test.com' % _random(), '123456')
        self.client.force_login(self.user)

    def test_library(self):
        response = self.client.get(reverse('profile_library'))
        self.assertEqual(response.status_code, 200)

    def test_library_course(self):
        response = self.client.get(reverse('profile'))
        self.assertEqual(response.status_code, 200)

    def test_library_question(self):
        response = self.client.get(reverse('profile'))
        self.assertEqual(response.status_code, 200)

    def test_library_program(self):
        response = self.client.get(reverse('profile_program'))
        self.assertEqual(response.status_code, 200)
