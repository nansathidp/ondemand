from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render

from ondemand.views_decorator import check_project
from order.models import Item


@check_project
@login_required
def library_view(request):
    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_question = settings.CONTENT_TYPE('question', 'activity')
    content_type_list = [content_type_program, content_type_course, content_type_question]
    
    item_list = Item.objects.select_related('order').filter(order__account=request.user,
                                                            content_type__in=content_type_list,
                                                            status__in=[2, 3],
                                                            order__status=3) \
                                                    .order_by('-timestamp')
    for item in item_list:
        if item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
            item.url = reverse('program:detail', args=[item.content, item.id])
        elif item.content_type_id == settings.CONTENT_TYPE('course', 'course').id:
            item.url = reverse('course:detail', args=[item.content, item.id])
        elif item.content_type_id == settings.CONTENT_TYPE('question', 'activity').id:
            item.url = reverse('question:detail', args=[item.content, item.id])
    return render(request,
                  'account/library.html',
                  {
                      'item_list': item_list
                  })
