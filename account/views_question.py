from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from order.models import Item
from progress.models import Progress
from content.models import Location as ContentLocation

from ondemand.views_decorator import check_project
from progress.cached import cached_progress_account_content
from .views import item_filter
from .views_base import get_ais_account_info, sum_progress_item

from utils.content import get_content


@check_project
@login_required
def question_view(request):
    progress = int(request.GET.get('progress', -1))
    item_question_list = Item.objects.filter(order__account=request.user,
                                             order__status=3,
                                             status=2,
                                             content_type=settings.CONTENT_TYPE('question', 'activity')).order_by('status', '-timestamp')

    for item_question in item_question_list:
        item_question.content_object = get_content(item_question.content_type_id, item_question.content)
        content_type = ContentLocation.pull_first(settings.CONTENT_TYPE_ID(item_question.content_type_id),
                                                  item_question.content)
        item_question.progress = Progress.pull(item_question,
                                               content_type,
                                               settings.CONTENT_TYPE_ID(item_question.content_type_id),
                                               item_question.content)

    item_list = item_filter(item_question_list, progress)
    progress_stat = sum_progress_item(item_question_list)

    try:
        provider = request.APP.provider
    except:
        provider = None

    account_content = cached_progress_account_content(request.user.id, provider)

    # AIS
    request.user.info = get_ais_account_info(account=request.user)
    return render(request,
                  'account/profile.html',
                  {'is_owner': True,
                   'ROOT_PAGE': 'profile',
                   'select': 'question',
                   'account': request.user,
                   'question_list': item_list,
                   'progress_stat': progress_stat,
                   'account_content': account_content})
