from django.conf import settings
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect, Http404
from django.utils import timezone

from account.models import Forgot
from account.pattern_password import ResetPassword
from term.models import Term


def reset_password_view(request):
    msg = None
    token = request.GET.get('token', None)
    status = request.GET.get('status', None)
    if token is None:
        return redirect('home')
    else:

        try:
            forgot = Forgot.objects.filter(token=token, status=status,
                                           hash_password__isnull=True,
                                           timestamp__gt=timezone.now() - timezone.timedelta(
                                               days=settings.LIMIT_AGE_SITE_FORGOT)).first()
            if forgot is None:
                raise Http404

            if not forgot.account.is_active:
                forgot = None
                msg = 'Your account is inactive.'
                return render(request,
                              'account/reset_password.html',
                              {'msg': msg,
                               'forgot': forgot})

            if settings.IS_ENABLE_ACCOUNT:
                if not forgot.account.is_enable:
                    forgot = None
                    msg = 'Your account is blocked, Please contact admin or reset new password.'
                    return render(request,
                                  'account/reset_password.html',
                                  {'msg': msg,
                                   'forgot': forgot})
        except:
            forgot = None
            msg = 'Invalid token, Please try again.'

    if request.method == 'POST' and forgot is not None:
        p1 = request.POST.get('p1', '')
        p2 = request.POST.get('p2', '')

        if p1 == p2:
            reset_password = ResetPassword(new_password=p1, forgot=forgot)
            if reset_password.is_validate():
                reset_password.save()
                # Reset done
                if reset_password.get_status():
                    if settings.PROJECT == 'cimb':
                        account = authenticate(email=forgot.account.uid, password=p1)
                    else:
                        account = authenticate(email=forgot.account.email, password=p1)
                    if account and account.first_active is None:
                        account.first_active = timezone.now()
                    account.last_active = timezone.now()
                    login(request, account)
                    account.save(update_fields=['last_active', 'first_active'])
                    request.session["is_skip"] = False
                    app_id = settings.CONTENT_TYPE('app', 'app').id
                    term = Term.pull_content(app_id, request.APP.id)
                    if term and Term.is_require_term(app_id, request.APP.id, account.id):
                        return redirect('term')
                    return redirect('home')
            msg = reset_password.message_error()
        else:
            msg = 'Your password not match.'

    return render(request,
                  'account/reset_password.html',
                  {'msg': msg,
                   'forgot': forgot})
