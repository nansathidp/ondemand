import xlrd
from django import forms
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.core.validators import validate_email
from django.shortcuts import render

from department.models import Department, Member
from ..models import Account


def _excel(book):
    account_list = []
    sh = book.sheet_by_index(0)
    for rx in range(sh.nrows):
        email = sh.cell(rx, 0).value
        password = str(sh.cell(rx, 1).value)
        password_type = str(sh.cell(rx, 1).ctype)
        first_name = sh.cell(rx, 2).value
        last_name = sh.cell(rx, 3).value
        dept = sh.cell(rx, 4).value.lower()
        email = email.strip()
        _user = {
            'email': email,
            'first_name': first_name,
            'last_name': last_name,
            'department': dept,
            'status': 'Initialize',
        }
        try:
            validate_email(email)
            account = Account.objects.filter(email=email).first()
            if not account:
                _user['status'] = 'Created'
                account = Account.objects.create(email=email,
                                                 first_name=first_name,
                                                 last_name=last_name)
            else:
                _user['status'] = 'Updated'
                if account.first_name != first_name:
                    account.first_name = first_name
                if account.last_name != last_name:
                    account.last_name = last_name
                    
            account.set_password(password.strip())
            account.save()

            department = Department.pull(dept)
            Member.clear(account)
            department.push(account)
        except forms.ValidationError:
            _user['status'] = 'Validation Error'
        account_list.append(_user)
    return account_list


def import_view(request):
    if not request.user.has_perm('account.add_account', group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    error = False
    account_list = []
    if request.method == 'POST':
        data = request.FILES['import']

        try:
            book = xlrd.open_workbook(file_contents=data.read())
            account_list = _excel(book)
        except:
            error = True

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Learner Management',
         'url': reverse('dashboard:account-dashboard:home')},
        {'is_active': True,
         'title': 'Import Summary'}
    ]

    return render(request,
                  'account/dashboard/import_summary.html',
                  {'SIDEBAR': 'account',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'account_list': account_list,
                   'error': error
                   })
