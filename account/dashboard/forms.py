from django import forms
from django.forms import CharField, PasswordInput

from ..models import Account

import datetime
from django.conf import settings

class AccountForm(forms.ModelForm):

    password = CharField(widget=PasswordInput())

    class Meta:
        model = Account
        fields = ['email', 'password', 'first_name', 'last_name', 'image']
        labels = {
            'email': 'Email',
            'password': 'Password',
            'first_name': 'First Name',
            'last_name': 'Last Name',
            'image': 'Profile Image',
        }

        help_texts = {
            'image': 'Image size 400 x 400 pixel, limit at 100 kb.'
        }

    def __init__(self, *args, **kwargs):
        super(AccountForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({'type': 'file'})


class AccountEditForm(forms.ModelForm):

    class Meta:
        model = Account
        fields = ['email', 'first_name', 'last_name', 'image', 'gender', 'is_active', 'is_force_reset_password', 'is_enable']
        labels = {
            'email': 'Email',
            'password': 'Password',
            'first_name': 'First Name',
            'last_name': 'Last Name',
            'image': 'Profile Image',
        }

        help_texts = {
            'image': 'Image size 400 x 400 pixel, limit at 100 kb.'
        }

    def __init__(self, *args, **kwargs):
        super(AccountEditForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({'type': 'file'})
        # Todo limit user
        if settings.IS_LIMIT_ACTIVE:
            if ((self.instance.last_deactivate and datetime.datetime.now().date() < (
                self.instance.last_deactivate + settings.TIME_ACTIVE).date()) \
                        or (Account.objects.filter(is_active=True).count() >= settings.MAX_USER)) \
                    and not self.instance.is_active:
                self.fields['is_active'].widget.attrs.update({'disabled': 'true'})
