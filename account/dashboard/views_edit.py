import datetime

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404

from .forms import AccountEditForm
from ..cached import cached_account_delete, cached_api_account_token_delete
from ..models import Account


def edit_view(request, account_id):
    account = get_object_or_404(Account, id=account_id)
    is_active = account.is_active
    active_amount = Account.objects.filter(is_active=True).count()

    if not request.user.has_perm('account.change_account', group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    # Todo limit user
    if settings.IS_LIMIT_ACTIVE:
        if account.last_deactivate:
            if (account.last_deactivate + settings.TIME_ACTIVE).date() > datetime.datetime.now().date() and request.POST.get('is_active'):
                request.method = 'GET'
        if active_amount >= settings.MAX_USER and request.POST.get('is_active'):
            request.method = 'GET'

    if request.method == 'POST':
        account_form = AccountEditForm(request.POST, request.FILES, instance=account)
        if account_form.is_valid():
            account = account_form.save(commit=False)

            # Todo limit user
            if settings.IS_LIMIT_ACTIVE:
                if is_active and not account.is_active:
                    account.last_deactivate = datetime.datetime.now()
                else:
                    account.last_deactivate = None

            account.save()
            cached_api_account_token_delete(account.token)
            cached_account_delete(account.id)
            account_form = AccountEditForm(instance=account)

            if is_active != account.is_active:
                account.effect_is_active()
    else:
        account_form = AccountEditForm(instance=account)

    # Todo limit user
    if settings.IS_LIMIT_ACTIVE and account.last_deactivate:
        account.last_deactivate = (account.last_deactivate + settings.TIME_ACTIVE).date()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Learner Management',
         'url': reverse('dashboard:account-dashboard:home')},
        {'is_active': True,
         'title': account.get_display()}
    ]
    return render(request,
                  'account/dashboard/add.html',
                  {'SIDEBAR': 'account',
                   'account': account,
                   'account_form': account_form,
                   'limit_user': settings.MAX_USER,
                   'amount_is_active': active_amount,
                   'date': datetime.datetime.now().date(),
                   'BREADCRUMB_LIST': BREADCRUMB_LIST})

