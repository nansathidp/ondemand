from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect

from account.models import Account
from mailer.models import Mailer
from utils.crop import crop_image
from .forms import AccountForm


def add_view(request):
    if settings.PROJECT == 'cimb':
        raise PermissionDenied

    if not request.user.has_perm('account.add_account',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    if request.method == 'POST':
        account_form = AccountForm(request.POST, request.FILES)
        if account_form.is_valid():
            #Todo limit account
            account = account_form.save(commit=False)
            if settings.IS_LIMIT_ACTIVE:
                if Account.objects.all().filter(is_active=True).count() >= settings.MAX_USER:
                    account.is_active = False

            account.save()
            crop_image(account, request)
            account.login_token()
            account.set_password(account.password)
            account.save(update_fields=['password'])
            try:
                _forgot_password(request, account.email)
            except:
                raise
            return redirect('dashboard:account-dashboard:home')
    else:
        account_form = AccountForm()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Learner Management',
         'url': reverse('dashboard:account-dashboard:home')},
        {'is_active': True,
         'title': 'Add Learner'}
    ]
    return render(request,
                  'account/dashboard/add.html',
                  {'SIDEBAR': 'account',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'account_form': account_form})


def _forgot_password(request, email):
    status = 5
    account = Account.objects.get(email=email)
    token = account.forgot_password(status)
    sender_email = settings.EMAIL_HOST_USER
    site_url = request.get_host()
    Mailer.send_forget_password_web(account, sender_email, token, status)
