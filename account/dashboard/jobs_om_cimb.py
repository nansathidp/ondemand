import datetime
import json
import os
import random
import string
import time
import traceback
import uuid

import django_rq
from django.conf import settings
from django.contrib.auth.models import Group
from django.core.validators import validate_email
from django.utils import timezone
from xlrd import open_workbook

from account.models import Account
from alert.models import Alert
from department.models import Department, Member as DepartmentMember
from mailer.jobs_send import sernd_delay_job
from mailer.models import Mailer
from organization.models import Parent, Child

DATA_LIST = []
ERROR_COUNT = 0
ERROR_LIST = []
UPDATE_COUNT = 0
CREATE_COUNT = 0

row_duplicate_update = []
row_create = []
row_update = []
row_not_update = []

GENDER = {
    'Mr': 1,
    'Mrs': 2,
    'Ms': 2,
    'Mr.': 1,
    'Mrs.': 2,
    'Ms.': 2,
}

PARENT_DICT = {}
IS_COUNT_ACTIVE = True
COUNT_ACTIVE = 0
ACCOUNT_LIST = []
ACCOUNT_DICT = {}
DEPARTMENT_DICT = {}
ACCOUNT_DEPARTMENT_DICT = {}
ACCOUNT_PARENT_DICT = {}
ACCOUNT_CHILD_DICT = {}
# DEPARTMENT_LIST = []
# DEPARTMENT_MEMBER_LIST = []
PARENT_LIST = []


def _str(value, type):
    if type in [2, 3]:
        value = str(int(float(value)))
    else:
        try:
            value = value.strip()
        except:
            value = str(value).strip()
    return value

def _push_account(data, log):
    global ERROR_COUNT, ERROR_LIST
    global CREATE_COUNT, UPDATE_COUNT
    global IS_COUNT_ACTIVE
    global COUNT_ACTIVE
    global ACCOUNT_DICT
    global ACCOUNT_DEPARTMENT_DICT

    # Step 1 log
    log.write('[Row #%s]: ' % data['row'])

    # Step 2 active users
    if IS_COUNT_ACTIVE:
        COUNT_ACTIVE = Account.objects.filter(is_active=True).count()
        IS_COUNT_ACTIVE = False
    if COUNT_ACTIVE >= settings.MAX_USER:
        log.write('[ERROR] Number of Active Users are above the limitation\n')
        ERROR_COUNT += 1
        ERROR_LIST.append(data['row'])
        return

    # Step 3 format column
    is_error = False
    for _ in [
        'email', 'uid', 'employee',
        'first_name', 'last_name',
        # 'company',
        'department', 'position',
        'manager',
        'active',
        # 'title'
    ]:
        if _ == 'email':
            try:
                validate_email(data['email'])
                if not 1 <= len(data[_]) <= 255:
                    is_error = True
            except:
                is_error = True
        elif _ == 'uid':
            if len(data['uid']) != 7:
                is_error = True
        elif _ == 'employee':
            if len(data['employee']) != 5:
                is_error = True
        elif _ == 'manager':
            if len(data['manager']) > 0 and len(data['manager']) != 5:
                is_error = True
        elif _ == 'active':
            if data['active'] not in ['A', 'I']:
                is_error = True
        elif _ == 'department':
            if len(data['department']) > 0 and len(data['department']) != 8:
                is_error = True
        elif _ in ['first_name', 'last_name']:
            if not 1 <= len(data[_]) <= 255:
                is_error = True
        elif len(str(data[_])) > 255: # position
            is_error = True
    if is_error:
        log.write('[ERROR] One of the columns is not the correct format\n')
        ERROR_COUNT += 1
        ERROR_LIST.append(data['row'])
        data['is_error'] = True
        return
    if data['employee'] == data['manager']:
        log.write('[ERROR] Employee_ID same as Manager_ID\n')
        ERROR_COUNT += 1
        ERROR_LIST.append(data['row'])
        data['is_error'] = True
        return

    # Step 4
    try:
        data['gender'] = GENDER[data['gender']]
    except:
        data['gender'] = 0


    # Find Account
    if data['employee'] in ACCOUNT_DICT:
        account = ACCOUNT_DICT[data['employee']]
    else:
        account = None

    is_active = data['active'].lower().strip() in ['a', '1', 'true']

    if account: # Case 4.1 Found Employee_ID
        data['account'] = account
        # Case 4.1.1 Check
        is_error = False
        for _ in DATA_LIST:
            if _['email'] == data['email'] and _['employee'] != data['employee']:
                is_error = True
        if is_error:
            log.write('[ERROR] cannot update: another user using the requested email address\n')
            ERROR_COUNT += 1
            ERROR_LIST.append(data['row'])
            data['is_error'] = True
            return

        # Case 4.1.2 UID
        if data['employee'] != data['uid'][-5:]:
            is_error = True
        if is_error:
            log.write('[ERROR] cannot update: UID not valid\n')
            ERROR_COUNT += 1
            ERROR_LIST.append(data['row'])
            data['is_error'] = True
            return

        _update_list = []
        # _log = ''
        if account.uid != data['uid']:
            account.uid = data['uid']
            _update_list.append('uid')
        if account.employee != data['employee']:
            # _log += '  |_ Employee: %s -> %s\n' % (account.employee, data['employee'])
            account.employee = data['employee']
            _update_list.append('employee')
        if account.gender != data['gender']:
            # _log += '  |_ Gender: %s -> %s\n' % (account.gender, data['gender'])
            account.gender = data['gender']
            _update_list.append('gender')
        # if account.company != data['company']:
        #     # _log += '  |_ Company: %s -> %s\n' % (account.company, data['company'])
        #     account.company = data['company']
        #     _update_list.append('company')
        if account.first_name != data['first_name']:
            # _log += '  |_ First Name: %s -> %s\n' % (account.first_name, data['first_name'])
            account.first_name = data['first_name']
            _update_list.append('first_name')
        if account.last_name != data['last_name']:
            # _log += '  |_ Last Name: %s -> %s\n' % (account.last_name, data['last_name'])
            account.last_name = data['last_name']
            _update_list.append('last_name')
        # if account.title != data['title']:
        #     # _log += '  |_ Title: %s -> %s\n' % (account.title, data['title'])
        #     account.title = data['title']
        #     _update_list.append('title')

        if account.email != data['email']:
            _account = Account.objects.filter(email=data['email']).first()
            if _account:
                _count = 0
                while True:
                    _count += 1
                    _email = _account.email + '.old%s' % _count
                    if not Account.objects.filter(email=_email).exists():
                        break
                _account.email = _email
                _account.save(update_fields=['email'])
                # log.write('Row %s: Employee %s Change Email duplicate (update): %s\n' % (
                #     data['row'], data['employee'], data['email'],
                # ))
                # row_duplicate_update.append(data['row'])
            # else:
            #     _log += '  |_ Email: %s -> %s\n' % (account.email, data['email'])
            #     account.email = data['email']
            #     _update_list.append('email')
            account.email = data['email']
            _update_list.append('email')

        if is_active != account.is_active and settings.IS_LIMIT_ACTIVE:
            if is_active:
                if account.last_deactivate and account.last_deactivate + settings.TIME_ACTIVE < timezone.now():
                    if COUNT_ACTIVE >= settings.MAX_USER:
                        is_active = False
                else:
                    if COUNT_ACTIVE >= settings.MAX_USER:
                        is_active = False
            else:
                account.last_deactivate = timezone.now()
                is_active = False
        if is_active != account.is_active:
            # _log += '  |_ Active: %s -> %s\n' % (account.is_active, is_active)
            IS_COUNT_ACTIVE = True
            account.is_active = is_active
            _update_list.append('active')

        log.write('Employee_ID: (%s) updated\n' % (data['employee']))
        UPDATE_COUNT += 1
        if len(_update_list) > 0:
            row_update.append(data['row'])
            # log.write('Row %s: Account  Update: (%s)%s -> %s\n' % (
            # data['row'], data['employee'], data['email'], _update_list
            # ))
            # log.write(_log)
            account.save()
        # else:
        #     row_not_update.append(data['row'])
    else: # Case 4.2 Not Found Employee_ID
        # Case 4.2.1 Check
        is_error = False
        for _ in DATA_LIST:
            if _['email'] == data['email'] and _['employee'] != data['employee']:
                is_error = True
        if is_error:
            log.write('[ERROR] cannot update: another user using the requested email address\n')
            ERROR_COUNT += 1
            ERROR_LIST.append(data['row'])
            data['is_error'] = True
            return

        # Case 4.2.2 UID
        if data['employee'] != data['uid'][-5:]:
            is_error = True
        if is_error:
            log.write('[ERROR] cannot update: UID not valid\n')
            ERROR_COUNT += 1
            ERROR_LIST.append(data['row'])
            data['is_error'] = True
            return

        _account = Account.objects.filter(email=data['email']).first()
        if _account:
            _count = 0
            while True:
                _count += 1
                _email = _account.email + '.old%s' % _count
                if not Account.objects.filter(email=_email).exists():
                    break
            _account.email = _email
            _account.save(update_fields=['email'])

        row_create.append(data['row'])

        account = Account.objects.create(
            email=data['email'],
            uid=data['uid'],
            employee=data['employee'],
            gender=data['gender'],
            first_name=data['first_name'],
            last_name=data['last_name'],
            is_first=False,
            is_enable=True,
            # company=data['company'],
            # title=data['title'],
            is_force_reset_password=False,
            is_active=is_active
        )
        IS_COUNT_ACTIVE = True
        ACCOUNT_DICT[data['employee']] = account
        data['account'] = account
        password = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
        account.set_password(password)

        status = 5
        token = account.forgot_password(status)
        if account.is_active:
            sender_email = settings.EMAIL_HOST_USER
            Mailer.send_create_account(account, sender_email, token, status)

        log.write('Employee_ID: (%s) created\n' % (data['employee']))
        CREATE_COUNT += 1

    # if account:
    #     data['account'] = account
    #     if data['department'] != 0:
    #         _push_department(account, data['department'], data['position'])

    if data['department'] in DEPARTMENT_DICT:
        department = DEPARTMENT_DICT[data['department']]
        if account.id in ACCOUNT_DEPARTMENT_DICT:
            _ = ACCOUNT_DEPARTMENT_DICT[account.id]
            _['is_found'] = True
            department_member = _['obj']
            department_member.department = department
            department_member.position = data['position']
            department_member.save()
        else:
            department_member = DepartmentMember.objects.create(
                department=department,
                account=account,
                position=data['position']
            )
            ACCOUNT_DEPARTMENT_DICT[account.id] = {
                'is_found': True,
                'obj': department_member
            }
    else:
        ACCOUNT_DEPARTMENT_DICT[account.id] = {
            'is_found': False,
        }

def om_cimb_job(alert_id):
    try:
        alert = Alert.objects.get(id=alert_id)
    except:
        return None

    try:
        _om_cimb_job(alert)
    except:
        alert.status = -1
        alert.result = traceback.format_exc()
        alert.save()

    # System Setting
    if settings.ACCOUNT__FORCE_ACTIVE_ID_LIST:
        Account.objects.filter(id__in=settings.ACCOUNT__FORCE_ACTIVE_ID_LIST).update(is_active=True)
    time.sleep(5)

def _om_cimb_job(alert):
    global DATA_LIST
    global ACCOUNT_LIST, COUNT_ACTIVE
    global DEPARTMENT_DICT, ACCOUNT_DEPARTMENT_DICT
    global EMAIL_DICT
    global ACCOUNT_PARENT_DICT

    _start = timezone.now()
    current_tz = timezone.get_current_timezone()

    alert.filename = '%s.log' % (uuid.uuid4())
    log = open('%s/%s' % (alert.get_export_path(), alert.filename), 'w')
    log.write('File executed: %s\n' % alert.filename_input)

    _alert = Alert.objects.filter(
        code='department',
        status__in=[2, 3]
    ).first()
    if _alert is None:
        log.write('Not Found: Import Department (Success)\n')

        log.close()
        time.sleep(5)
        alert.status = -3
        alert.save()
        return None
    else:
        try:
            msg = json.loads(_alert.json_result)['msg']
        except:
            msg = ''
        log.write(msg)

    path = os.path.join(settings.BASE_DIR, 'alert', 'upload', alert.file_input)
    book = open_workbook(path)
    sh = book.sheet_by_index(0)

    # Check File Error
    try:
        p = alert.filename_input.split('.')[0].split('_')
        is_error = False
    except:
        is_error = True

    if not is_error:
        if len(p) != 6:
            is_error = True
    if not is_error:
        if p[0] != 'CIMB' or p[1] != 'THAI' or p[2] != 'LMS' or p[3] != 'OM':
            is_error = True
    if not is_error:
        try:
            _date = datetime.date(int(p[4][:4]), int(p[4][4:6]), int(p[4][6:]))
        except:
            is_error = True
    if not is_error:
        try:
            if sh.nrows - 1 != int(p[5]):
                is_error = True
        except:
            is_error = True
    if not is_error:
        header = [
            'Email',
            'UID',
            'Employee_ID',
            'Title_TH',
            'First_Name_TH',
            'Last_Name_TH',
            'Title_EN',
            'First_Name_EN',
            'Last_Name_EN',
            'Company_TH',
            'Company_EN',
            'Department_ID', # 11
            'Department_TH',
            'Department_EN',
            'Position_TH', # 14
            'Position_EN',
            'Corporate_Title', # 16
            'Job_Grade', # 17
            'Manager_ID', # 18
            'Status' # 19
        ]
        for cx in range(20):
            if sh.cell(0, cx).value.strip() != header[cx]:
                # print(sh.cell(0, cx).value.strip(), header[cx])
                is_error = True
                break

    if is_error:
        _start = current_tz.normalize(_start.astimezone(current_tz))
        _end = current_tz.normalize(timezone.now().astimezone(current_tz))
        msg = """\n
Start Executing at: %s
Finished Executing at: %s

Please check the file name and format to be according to the agreed standards

File name: 'CIMB_THAI_LMS_OM_(Date)_(Number of rows not including header).(extension)' as .xls or .xlsx
(Date) must be in YearMonthDay (YYYYMMDD): E.g. 20171022 - 8 characters [Year(4)][Month(2)][Day(2)]
(Number of records) must be a valid positive integer which is the number of records (doesn't include header row)

File format: the columns in the excel file must be exactly (case sensitive - in the exact order) as the following
***(Check Data Dict for info)
(Example: FILE_ERROR_LOG_[CIMB_THAI_LMS_OM_20171022_3415.xls]_20171022_223353.log)        
    """ % (_start.strftime("%H:%M:%S %Y%m%d"), _end.strftime("%H:%M:%S %Y%m%d"))
        log.write(msg)
        log.close()

        alert.status = -3
        alert.save()
        return None
        # End Check File Error

    # Current Org Parent
    PARENT_LIST = Parent.objects.all()
    for _ in PARENT_LIST:
        if not _.parent_id in PARENT_DICT:
            PARENT_DICT[_.parent_id] = True

    for rx in range(1, sh.nrows):
        data = {
            'row': rx + 1,
            'account': None,
            'is_error': False,
            'email': _str(sh.cell(rx, 0).value, sh.cell(rx, 0).ctype).lower(),
            'uid': _str(sh.cell(rx, 1).value, sh.cell(rx, 1).ctype).lower(),
            'employee': _str(sh.cell(rx, 2).value, sh.cell(rx, 2).ctype),
            'gender': _str(sh.cell(rx, 6).value, sh.cell(rx, 6).ctype),
            'first_name': _str(sh.cell(rx, 4).value, sh.cell(rx, 4).ctype),
            'last_name': _str(sh.cell(rx, 5).value, sh.cell(rx, 5).ctype),

            # 'company': sh.cell(rx, 9).value,
            'department': _str(sh.cell(rx, 11).value, sh.cell(rx, 11).ctype),
            'position': _str(sh.cell(rx, 14).value, sh.cell(rx, 14).ctype),
            # 'title': sh.cell(rx, 16).value,
            'manager': _str(sh.cell(rx, 18).value, sh.cell(rx, 18).ctype),
            'active': _str(sh.cell(rx, 19).value, sh.cell(rx, 19).ctype),
        }
        DATA_LIST.append(data)

    Account.objects.update(is_active=False)
    ACCOUNT_LIST = Account.objects.all()

    for department in Department.objects.all():
        DEPARTMENT_DICT[department.external_id] = department

    for department_member in DepartmentMember.objects.select_related('account').all():
        if department_member.account_id in ACCOUNT_DEPARTMENT_DICT:
            department_member.delete()
        else:
            ACCOUNT_DEPARTMENT_DICT[department_member.account_id] = {
                'is_found': False,
                'obj': department_member
            }

    log.write('\n\nExecution results\n')
    account_list = Account.objects.filter(is_active=True)
    if settings.ACCOUNT__HIDE_ID_LIST:
        account_list = account_list.exclude(id__in=settings.ACCOUNT__HIDE_ID_LIST)
    COUNT_ACTIVE = account_list.count()

    for account in Account.objects.all():
        ACCOUNT_DICT[account.employee] = account

    for data in DATA_LIST:
        _push_account(data, log)

    log.write('\n\nValidation\n')
    account_list = Account.objects.filter(is_active=True)
    if settings.ACCOUNT__HIDE_ID_LIST:
        account_list = account_list.exclude(id__in=settings.ACCOUNT__HIDE_ID_LIST)
    for account in account_list:
        ACCOUNT_DICT[account.employee] = account

    ACCOUNT_PARENT_DICT = {}
    for parent in Parent.objects.all():
        if parent.account_id in ACCOUNT_PARENT_DICT:
            parent.delete()
        else:
            ACCOUNT_PARENT_DICT[parent.account_id] = {
                'is_found': False,
                'obj': parent,
                'is_delete': True,
            }

    _parent_create_list = []
    for data in DATA_LIST:
        if data['account'] is None:
            continue
        if data['is_error']:
            continue

        if data['account'].id in ACCOUNT_PARENT_DICT and 'obj' in ACCOUNT_PARENT_DICT[data['account'].id]:
            parent = ACCOUNT_PARENT_DICT[data['account'].id]['obj']
            if len(data['manager']) > 0 and data['manager'] in ACCOUNT_DICT:
                _account = ACCOUNT_DICT[data['manager']]
                if parent.parent_id != _account.id:
                    parent.parent = _account
                    parent.save()
                ACCOUNT_PARENT_DICT[data['account'].id] = {
                    'is_found': True,
                    'is_delete': False,
                }
            else:
                parent.delete()
                ACCOUNT_PARENT_DICT[data['account'].id] = {
                    'is_found': False,
                    'is_delete': False,
                }
        elif len(data['manager']) > 0 and data['manager'] in ACCOUNT_DICT:
            _account = ACCOUNT_DICT[data['manager']]
            _parent_create_list.append(
                Parent(
                    parent=_account,
                    account=data['account']
                )
            )
            # Parent.objects.create(
            #     parent=_account,
            #     account=data['account']
            # )
            ACCOUNT_PARENT_DICT[data['account'].id] = {
                'is_found': True,
                'is_delete': False,
            }
        else:
            ACCOUNT_PARENT_DICT[data['account'].id] = {
                'is_found': False,
                'is_delete': False,
            }
    if len(_parent_create_list) > 0:
        Parent.objects.bulk_create(_parent_create_list)

    # Delete Parent
    _delete_id_list = []
    for key, value in ACCOUNT_PARENT_DICT.items():
        if value['is_delete']:
            _delete_id_list.append(value['obj'].id)
            # value['obj'].delete()
    Parent.objects.filter(id__in=_delete_id_list).delete()

    count_without_department = 0
    count_without_manager = 0
    account_list = Account.objects.filter(is_active=True)
    if settings.ACCOUNT__HIDE_ID_LIST:
        account_list = account_list.exclude(id__in=settings.ACCOUNT__HIDE_ID_LIST)
    for account in account_list:
        if account.id in ACCOUNT_DEPARTMENT_DICT:
            _ = ACCOUNT_DEPARTMENT_DICT[account.id]
            if _['is_found'] is False:
                count_without_department += 1
                log.write('Employee_ID: %s does not have a valid department\n' % account.employee)
                if 'obj' in _:
                    _['obj'].delete()
        else:
            log.write('Employee_ID: %s does not have a valid department.\n' % account.employee)
        if account.id in ACCOUNT_PARENT_DICT:
            _ = ACCOUNT_PARENT_DICT[account.id]
            if _['is_found'] is False:
                count_without_manager += 1
                log.write('Employee_ID: %s does not have a valid manager\n' % account.employee)
        else:
            log.write('Employee_ID: %s does not have a valid manager.\n' % account.employee)

    log.write('\n\nManagers\n')
    parent_account_id_list = Parent.objects.values_list(
        'parent_id', flat=True
    ).distinct().order_by('parent_id')

    count_manager = parent_account_id_list.count()
    account_list = Account.objects.filter(
        id__in=parent_account_id_list,
        is_active=True
    )
    if settings.ACCOUNT__HIDE_ID_LIST:
        account_list = account_list.exclude(id__in=settings.ACCOUNT__HIDE_ID_LIST)

    if settings.ROLE_MANAGER_ID != -1:
        group_manager = Group.objects.filter(id=settings.ROLE_MANAGER_ID).first()
    else:
        group_manager = None
    account_manager_id_list = []
    for account in account_list:
        log.write('Employee_ID: %s is a manager\n' % account.employee)
        if group_manager:
            #if not account.groups.filter(group=group_manager).exists():
            account.groups.add(group_manager)
            account_manager_id_list.append(account.id)

    account_list = Account.objects.filter(is_active=True)
    if settings.ACCOUNT__HIDE_ID_LIST:
        account_list = account_list.exclude(id__in=settings.ACCOUNT__HIDE_ID_LIST)
    for account in account_list:
        if not account.id in account_manager_id_list and group_manager:
            account.groups.remove(group_manager)

    # Organization v.2
    parent_dict = {}
    child_dict = {}
    for parent in Parent.objects.all():
        if parent.parent_id in parent_dict:
            parent_dict[parent.parent_id].append(parent.account_id)
        else:
            parent_dict[parent.parent_id] = [parent.account_id]

    def _check_child(key, child):
        return child in child_dict[key]

    def _add_child(key, child_list):
        for child in child_list:
            if not _check_child(key, child):
                child_dict[key].append(child)
            else:
                return None
            if child in parent_dict:
                _add_child(key, parent_dict[child])

    for key, value in parent_dict.items():
        child_dict[key] = []
        _add_child(key, value)

    account_id_dict = {}
    account_list = Account.objects.filter(is_active=True)
    if settings.ACCOUNT__HIDE_ID_LIST:
        account_list = account_list.exclude(id__in=settings.ACCOUNT__HIDE_ID_LIST)
    for account in account_list:
        account_id_dict[account.id] = account

    _child_delete_id_list = []
    for child in Child.objects.all():
        if child.account_id in child_dict:
            if child.child_id in child_dict[child.account_id]: # Exists.
                child_dict[child.account_id].remove(child.child_id)
            else: # Delete (with Child)
                _child_delete_id_list.append(child.id)
                # child.delete()
        else: # Delete (Account)
            _child_delete_id_list.append(child.id)
            # child.delete()
    if len(_child_delete_id_list) > 0:
        Child.objects.filter(id__in=_child_delete_id_list).delete()

    _child_create_list = []
    for account_id, value in child_dict.items():
        for child_id in value:
            if account_id in account_id_dict and child_id in account_id_dict:
                _child_create_list.append(
                    Child(
                        account=account_id_dict[account_id],
                        child=account_id_dict[child_id]
                    )
                )
                # Child.objects.create(
                #     account=account_id_dict[account_id],
                #     child=account_id_dict[child_id],
                # )
    if len(_child_create_list) > 0:
        Child.objects.bulk_create(_child_create_list)

    _start = current_tz.normalize(_start.astimezone(current_tz))
    if _start.date() != _date:
        log.write('\n\nWARNING: THE FILE UPLOADED MIGHT BE THE WRONG DATE\n')

    log.write('\n\n[SUMMARY]\n')
    log.write('Rows executed: %s\n' % (sh.nrows - 1))
    log.write('Rows updated: %s\n' % UPDATE_COUNT)
    log.write('Rows created: %s\n' % CREATE_COUNT)
    log.write('Rows ERROR: %s {' % ERROR_COUNT)
    for error in ERROR_LIST:
        log.write('#%s, ' % error)
    log.write('}')

    log.write('\n\nUsers without departments: %s\n' % count_without_department)
    log.write('Users without managers: %s\n' % count_without_manager)
    log.write('Users that are Managers: %s\n' % count_manager)

    log.write('\n\nTotal number of active users: %s/%s\n' % (
        Account.objects.filter(is_active=True).count(),
        settings.MAX_USER
    ))
    _end = current_tz.normalize(timezone.now().astimezone(current_tz))
    log.write('Start Executing at: %s\n' % _start.strftime("%H:%M:%S %Y%m%d"))
    log.write('Finish Executing at: %s\n' % _end.strftime("%H:%M:%S %Y%m%d"))
    log.close()

    result = {
        'error': ERROR_COUNT
    }
    alert.json_result = json.dumps(result, indent=2)
    alert.status = 2
    alert.save()

    django_rq.enqueue(sernd_delay_job, timeout=21600) # 6 Hrs.
