from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404

from department.models import Member
from ..models import Account


def detail_view(request, account_id):
    account = get_object_or_404(Account, id=account_id)
    if request.user.has_perm('account.view_account',
                                 group=request.DASHBOARD_GROUP):
        pass
    elif request.user.has_perm('account.view_org_account',
                               group=request.DASHBOARD_GROUP):
        if settings.IS_ORGANIZATION:
            if not account.org_child_set.filter(account=request.user).exists():
                raise PermissionDenied
        else:
            raise PermissionDenied
    else:
        raise PermissionDenied

    department_member_list = Member.objects.filter(account=account)
    account_info = None

    if settings.PROJECT == 'ais':
        from ais.models import AccountInfo
        account_info = AccountInfo.objects.filter(account=account).first()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Learner Management',
         'url': reverse('dashboard:account-dashboard:home')},
        {'is_active': True,
         'title': account.get_display()}
    ]
    return render(request,
                  'account/dashboard/detail.html',
                  {'SIDEBAR': 'account',
                   'account': account,
                   'account_info': account_info,
                   'department_member_list': department_member_list,
                   'BREADCRUMB_LIST': BREADCRUMB_LIST})
