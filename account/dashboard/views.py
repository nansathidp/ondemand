from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.shortcuts import render

from alert.models import Alert
from dashboard.views import config_view
from department.cached import cached_department
from department.models import Department
from level.models import Level
from utils.filter import get_q
from utils.filter import get_q_dashboard
from utils.paginator import paginator
from ..models import Account


def home_view(request):
    if request.user.has_perm('account.view_account',
                             group=request.DASHBOARD_GROUP):
        account_list = Account.objects.prefetch_related(
            'department_member_set',
            'level_member_set'
        ).all().order_by('-date_joined')
        q_department_list = Department.objects.filter(parents__isnull=True)
    elif request.user.has_perm('account.view_org_account',
                               group=request.DASHBOARD_GROUP):
        q_department_list = Department.objects.filter(parents__isnull=True)
        if settings.IS_ORGANIZATION:
            account_list = Account.objects.prefetch_related(
                'department_member_set', 'level_member_set'
            ).filter(org_child_set__account=request.user) \
            .distinct() \
            .order_by('is_active', '-date_joined')
        else:
            return config_view(request)
    else:
        raise PermissionDenied

    q_department = get_q_dashboard(request, 'department', 'int')
    if q_department != -1:
        department = cached_department(q_department)
        if department is not None:
            filter_department_list = department.get_all_child()
            account_list = account_list.filter(department_member_set__department__in=filter_department_list)
    q_level = get_q(request, 'q_level')
    if q_level != -1:
        account_list = account_list.filter(level_member_set__level_id=q_level)

    q_name = " ".join(get_q_dashboard(request, 'name', 'str').split())
    if q_name != -1 and len(q_name) > 0:
        account_list = account_list.filter(
            Q(email__icontains=q_name) |
            Q(first_name__icontains=q_name) |
            Q(last_name__icontains=q_name)
        )

    if settings.IS_LIMIT_ACTIVE:
        account_list = account_list.order_by('last_deactivate', '-is_active', 'email')
    else:
        account_list = account_list.order_by('email')

    if settings.ACCOUNT__HIDE_INACTIVE:
        account_list = account_list.exclude(is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        account_list = account_list.exclude(id__in=settings.ACCOUNT__HIDE_ID_LIST)
    account_list = paginator(request, account_list)

    for account in account_list:
        if account.last_deactivate:
            account.last_deactivate = (account.last_deactivate + settings.TIME_ACTIVE).date()

    q_level_list = Level.objects.all()

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Learner Management'}
    ]

    alert = Alert.pull_inprogress(request.user, settings.CONTENT_TYPE('account', 'account'), -1)
    result = {
        'SIDEBAR': 'account',
        'BREADCRUMB_LIST': BREADCRUMB_LIST,
        'account_list': account_list,
        'q_department_list': q_department_list,
        'q_level_list': q_level_list,
        'q_department': q_department,
        'q_level': q_level,
        'q_name': q_name,
        'active_amount': Account.objects.filter(is_active=True).count(),
        'is_enable': settings.IS_ENABLE_ACCOUNT,
        'is_force_password': settings.IS_FORCE_RESET_PASSWORD,
        'alert': alert
    }

    if settings.IS_LIMIT_ACTIVE:
        result.update(
            {
                'IS_LIMIT_ACTIVE': settings.IS_LIMIT_ACTIVE,
                'limit_active': settings.MAX_USER
            }
        )

    return render(
        request,
        'account/dashboard/home.html',
        result
    )
