from django.conf.urls import url

from .views import home_view
from .views_add import add_view
from .views_import import import_view
from .views_detail import detail_view
from .views_edit import edit_view
from .views_force_password import force_password_view

app_name = 'account-dashboard'
urlpatterns = [
    url(r'^$', home_view, name='home'),  # TODO: testing
    url(r'^add/$', add_view, name='add'),  # TODO: testing
    url(r'^import/$', import_view, name='import'),  # TODO: testing
    url(r'^(\d+)/$', detail_view, name='detail'),  # TODO: testing
    url(r'^(\d+)/edit/$', edit_view, name='edit'),  # TODO: testing
    url(r'^(\d+)/force-password', force_password_view, name='force') # Fuck!! end of url?
]
