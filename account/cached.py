import hashlib

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.core.cache import cache

from account.models import Account
from content.models import Location as ContentLocation
from department.models import Department
from order.models import Item
from progress.models import Progress
from utils.cached.time_out import get_time_out_day, get_time_out_week, get_time_out


def cached_auth_group_list(account):
    key = '%s_auth_group_list_%s' % (settings.CACHED_PREFIX, account.id)
    result = cache.get(key)
    if result is None:
        result = account.groups.all()
        cache.set(key, result, get_time_out_week())
    return result


def cached_auth_group_list_delete(account):
    key = '%s_auth_group_list_%s' % (settings.CACHED_PREFIX, account.id)
    cache.delete(key)


def cached_auth_group(group_id):
    key = '%s_auth_group_%s' % (settings.CACHED_PREFIX, group_id)
    result = cache.get(key)
    if result is None:
        try:
            result = Group.objects.get(id=group_id)
            cache.set(key, result, get_time_out_day())
        except:
            return None
    return result


def cached_auth_group_delete(group_id):
    key = '%s_auth_group_%s' % (settings.CACHED_PREFIX, group_id)
    cache.delete(key)


def cached_auth_permission(user_id, group, is_force=False):
    if group is None:
        key = '%s_auth_permission_%s' % (settings.CACHED_PREFIX, user_id)
    else:
        key = '%s_auth_permission_%s_%s' % (settings.CACHED_PREFIX, user_id, group.id)
    result = None if is_force else cache.get(key)
    if result is None:
        user_groups_field = get_user_model()._meta.get_field('groups')
        user_groups_query = 'group__%s' % user_groups_field.related_query_name()
        perms = Permission.objects.filter(**{user_groups_query: user_id})
        if group is not None:
            perms = perms.filter(group=group)
        result = perms.values_list('content_type__app_label', 'codename').order_by()
        cache.set(key, result, get_time_out_day())
    return result


def cached_auth_permission_delete(user_id, group=None):
    if group is None:
        key = '%s_auth_permission_%s' % (settings.CACHED_PREFIX, user_id)
    else:
        key = '%s_auth_permission_%s_%s' % (settings.CACHED_PREFIX, user_id, group.id)
    cache.delete(key)


def cached_account(account_id, is_force=False):
    key = '%s_account_%s' % (settings.CACHED_PREFIX, account_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Account.objects.get(id=account_id)
        except:
            result = -1
        cache.set(key, result, get_time_out_day())
    return None if result == -1 else result


def cached_account_delete(account_id):
    key = '%s_account_%s' % (settings.CACHED_PREFIX, account_id)
    cache.delete(key)


def cached_account_update(account):
    key = '%s_account_%s' % (settings.CACHED_PREFIX, account.id)
    cache.set(key, account, get_time_out_day())


def cached_api_account_auth(token, uuid, is_force=False):
    if token is None:
        return None
    code = '%s-%s' % (token, token)
    h = hashlib.sha1(code.encode('utf-8')).hexdigest()
    key = '%s_api_account_auth_%s' % (settings.CACHED_PREFIX, h)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Account.api_auth(token, uuid)
        if result is not None:
            # if getattr(settings, 'SESSION_COOKIE_AGE', False):
            #     cache.set(key, result, settings.SESSION_COOKIE_AGE)
            # else:
            #     cache.set(key, result, get_time_out_week())
            cache.set(key, result, get_time_out_week())
            key2 = '%s_api_account_auth_mark_%s' % (settings.CACHED_PREFIX, result.id)
            cache.set(key2, key, get_time_out())
        return result
    elif result == -1:
        return None
    else:
        return result


def cached_api_account_token_delete(token):
    code = '%s-%s' % (token, token)
    h = hashlib.sha1(code.encode('utf-8')).hexdigest()
    key = '%s_api_account_auth_%s' % (settings.CACHED_PREFIX, h)
    cache.delete(key)


def cached_api_account_auth_delete(account, is_force=False):
    key = '%s_api_account_auth_mark_%s' % (settings.CACHED_PREFIX, account.id)
    result = None if is_force else cache.get(key)
    if result is not None:
        cache.delete(result)


def cached_api_account_auth_v2(token, is_force=False):
    h = hashlib.sha1(token.encode('utf-8')).hexdigest()
    key = '%s_api_account_auth_v2_%s' % (settings.CACHED_PREFIX, h)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Account.objects.get(token=token)
        except:
            result = -1
        # if getattr(settings, 'SESSION_COOKIE_AGE', False):
        #     cache.set(key, result, settings.SESSION_COOKIE_AGE)
        # else:
        #     cache.set(key, result, get_time_out_week())
        cache.set(key, result, get_time_out_week())
    if result == -1:
        return None
    else:
        return result


def cached_onboard_account(account, is_force=False):
    key = '%s_is_onboard_account_%s' % (settings.CACHED_PREFIX, account.id)
    result = cache.get(key)
    if result is None:
        result = Item.pull_onboard_item(account)
        if result is None:
            result = -1
            cache.set(key, result, get_time_out())

    if result == -1:
        return False
    elif settings.PROJECT == 'cimb':
        return True

    content_type = settings.CONTENT_TYPE('program', 'onboard')
    content_location = ContentLocation.pull_first(
        content_type,
        result.content
    )
    progress = Progress.pull(
        result,
        content_location,
        content_type,
        result.content
    )
    return True if result and progress.status != 2 else False


def cached_onboard_account_delete(account_id):
    key = '%s_is_onboard_account_%s' % (settings.CACHED_PREFIX, account_id)
    cache.delete(key)

def cached_account_department_display(account_id):
    key = '%s_account_department_display_%s' % (settings.CACHED_PREFIX, account_id)
    result = cache.get(key)
    if result is None:
        for department in Department.objects.filter(member__account_id=account_id).distinct():
            if result is None:
                result = department.name.title()
            else:
                result += ', %s' % department.name.title()
        if _ is None:
            result = '-'
        cache.set(key, result, get_time_out())
    return result

def cached_account_department_display_delete(account_id):
    key = '%s_account_department_display_%s' % (settings.CACHED_PREFIX, account_id)
    cache.delete(key)