from django.db import models
from django.conf import settings
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin, Permission
)


class AccountManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(email, password=password, )
        user.is_admin = True
        user.is_superuser = True
        user.is_enable = True
        user.is_first = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser, PermissionsMixin):
    GENDER_CHOICES = (
        (0, '-'),
        (1, 'M'),
        (2, 'F'),
    )

    STATUS_CHOICES = (
        (0, 'General'),
        (1, 'AIS Privilege'),
        (2, 'AIS Privilege (ByPass)'),
    )

    pair_id = models.BigIntegerField(blank=True, default=-1)
    pair_password = models.CharField(max_length=120, blank=True)

    uuid = models.CharField(max_length=120, blank=True, db_index=True)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    uid = models.CharField(max_length=120, blank=True, db_index=True, default='')
    employee = models.CharField(max_length=120, null=True, blank=True, default=None, db_index=True)
    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True)
    birthday = models.DateField(null=True, blank=True)
    gender = models.IntegerField(choices=GENDER_CHOICES, default=0)
    tel = models.CharField(max_length=24, blank=True)
    desc = models.TextField(blank=True)
    title = models.CharField(max_length=120, blank=True)
    location = models.CharField(max_length=120, null=True, blank=True)
    company = models.CharField(max_length=120, blank=True)
    image = models.ImageField(upload_to='account/image/', null=True, blank=True)
    is_upload = models.BooleanField(default=False)
    facebook_id = models.CharField(max_length=120, null=True, db_index=True, blank=True)
    facebook_access_token = models.CharField(max_length=256, null=True, blank=True)  # db_index=True
    is_login_facebook = models.BooleanField(default=False)
    is_verify = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True, db_index=True)
    is_admin = models.BooleanField(default=False)

    # First Login
    is_first = models.BooleanField(default=False)
    # Enable Account
    # Block user
    is_enable = models.BooleanField(default=True)
    is_force_reset_password = models.BooleanField(default=True)

    # Change field name
    datetime_change_password = models.DateTimeField(blank=True, null=True)

    date_joined = models.DateTimeField(auto_now_add=True, db_index=True)

    # Move to session log
    first_active = models.DateTimeField(blank=True, null=True)
    last_active = models.DateTimeField(blank=True, null=True)

    # Last deactivate
    last_deactivate = models.DateTimeField(blank=True, null=True)

    token = models.CharField(max_length=32, null=True, db_index=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=0)
    id_card = models.CharField(max_length=15, blank=True)
    info = models.TextField(blank=True)

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        default_permissions = ('view', 'view_org', 'add', 'change', 'delete')  # not use view_own

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        from utils.models_image_delete import image_delete_pre_save
        from .cached import cached_account_update
        image_delete_pre_save(self)
        super(self.__class__, self).save(*args, **kwargs)
        cached_account_update(self)

    def delete(self, *args, **kwargs):
        from utils.models_image_delete import image_delete_pre_delete
        image_delete_pre_delete(self)
        super(self.__class__, self).delete(*args, **kwargs)

    @staticmethod
    def pull(id):
        from .cached import cached_account
        return cached_account(id)

    def get_display(self):
        return '%s %s' % (self.first_name, self.last_name)

    def is_valid(self):
        return '@gen' not in self.email

    def get_escape_name(self):
        import re
        # The user is identified by their email address
        name = '%s %s' % (self.first_name, self.last_name)
        return re.sub('[^a-zA-Z0-9-_*.]', '', name)

    def get_full_name(self):
        if self.first_name:
            return '%s %s' % (self.first_name, self.last_name)
        else:
            return '-'

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def get_gender(self):
        gender = '-'
        if self.gender == 1:
            gender = 'Male'
        elif self.gender == 2:
            gender = 'Female'
        return gender

    def has_perm(self, perm, group=None, obj=None):
        from .cached import cached_auth_permission

        def _get_permissions(user_obj, group):
            perms = cached_auth_permission(user_obj.id, group)
            return set("%s.%s" % (ct, name) for ct, name in perms)

        # Skip is_superuser
        if group is None:
            code = '_perm_cache'
        else:
            code = '_perm_cache_%s' % (group.id)
        if not hasattr(self, code):
            setattr(self, code, _get_permissions(self, group))
        return perm in getattr(self, code)

    def has_dashboard_account_management(self):
        pass

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    def get_auth_group_list(self):
        from .cached import cached_auth_group_list
        return cached_auth_group_list(self)

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def login_token(self):
        if self.token is not None and len(self.token) == 32:
            return self.token
        import random, string
        token = '0'
        while True:
            token = ''.join(
                random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(32))
            if Account.objects.filter(token=token).count() == 0:
                self.token = token
                self.save(update_fields=['token'])
                break
        return token

    def api_display_title(self):
        d = {}
        d['id'] = self.id
        d['first_name'] = self.first_name
        d['last_name'] = self.last_name
        d['title'] = self.title
        d['email'] = self.email
        d['date_joined'] = self.date_joined.strftime(settings.TIME_FORMAT)
        try:
            d['image'] = self.image.url if bool(self.image) else None
        except:
            d['image'] = '%s/static/images/profile_default.jpg' % settings.SITE_URL
        return d

    def api_profile(self):
        d = {}
        d['id'] = self.id
        d['first_name'] = self.first_name
        d['last_name'] = self.last_name
        d['employee_id'] = self.get_employee_ais_account()
        d['organization'] = self.get_company_ais_account()

        d['position'] = self.get_position_member_display()
        d['department'] = self.get_department_display()

        d['is_temporary_account'] = '@gen' in self.email
        d['facebook_id'] = self.facebook_id
        d['is_active'] = self.is_active
        d['is_enable'] = self.is_enable if settings.IS_ENABLE_ACCOUNT else True
        if settings.IS_FORCE_RESET_PASSWORD:
            if self.is_force_reset_password:
                is_force = False
            else:
                is_force = True
        else:
            is_force = False
        d['is_force_password'] = is_force
        try:
            d['is_login_facebook'] = len(self.facebook_id) > 0
        except:
            d['is_login_facebook'] = False
        d['desc'] = self.desc
        d['title'] = self.title
        try:
            d['birthday'] = self.birthday.strftime('%d/%m/%Y')
        except:
            d['birthday'] = None

        d['gender'] = self.gender
        d['tel'] = self.tel
        d['email'] = self.email
        try:
            d['image'] = self.image.url
        except:
            d['image'] = '%s/static/images/profile_default.jpg' % settings.SITE_URL

        d['status'] = self.status
        d['status_display'] = self.get_status_display()
        d['date_joined'] = self.date_joined.strftime(settings.TIME_FORMAT)
        return d

    def profile(self):
        d = {}
        d['id'] = self.id
        d['first_name'] = self.first_name
        d['last_name'] = self.last_name
        d['facebook_id'] = self.facebook_id
        try:
            d['is_login_facebook'] = len(self.facebook_id) > 0
        except:
            d['is_login_facebook'] = False
        d['is_verify'] = self.is_verify
        d['desc'] = self.desc
        d['title'] = self.title
        try:
            d['birthday'] = self.birthday.strftime('%d/%m/%Y')
        except:
            d['birthday'] = None
        d['gender'] = self.gender
        d['tel'] = self.tel
        d['email'] = self.email
        try:
            d['image'] = self.image.url
        except:
            d['image'] = '%simages/profile_default.jpg' % settings.STATIC_URL

        d['status'] = self.status
        d['status_display'] = self.get_status_display()
        d['date_joined'] = self.date_joined.strftime(settings.TIME_FORMAT)
        return d

    def get_employee_id(self):
        # TODO : Check Ais and use this
        if self.employee is None or len(self.employee) == 0:
            return '-'
        return self.employee

    def get_readable_address(self):
        address = self.address_set.first()
        if address is not None:
            d = address.readable()
        else:
            d = None
        return d

    def is_valid_email(self):
        if '@facebook.com.gen' in self.email or '@gen' in self.email:
            return False
        else:
            return True

    def api_facebook_image(self):
        if self.facebook_id is None:
            return
        from django.utils import timezone
        import uuid, os, requests
        now = timezone.now()
        url = 'https://graph.facebook.com/%s/picture?type=large' % self.facebook_id
        path = '%s/media/account/image/%s/%s/' % (settings.BASE_DIR, now.year, now.month)
        if not os.path.isdir(path):
            os.makedirs(path)
        filename = '%s.jpg' % (uuid.uuid4())
        f = '%s%s' % (path, filename)
        with open(f, 'wb') as handle:
            response = requests.get(url, stream=True)
            if not response.ok:
                return
            for block in response.iter_content(1024):
                if not block:
                    break
                handle.write(block)
        self.image = 'account/image/%s/%s/%s' % (now.year, now.month, filename)

    @staticmethod
    def api_auth(token, uuid, is_uuid_create=False):
        from .cached import cached_api_account_auth_v2
        import random, string
        account = None
        if token is not None and len(token) == 32:
            account = cached_api_account_auth_v2(token)
            return account
        if account is None and uuid is not None and len(uuid) != 0:
            try:
                account_list = Account.objects.filter(uuid=uuid)
                account = account_list[0]
                if account_list.count() > 1:
                    for account_del in account_list[1:]:
                        account_del.delete()
            except:
                if is_uuid_create and uuid is not None and len(uuid) != 0:
                    password = ''.join(
                        random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in
                        range(12))
                    email = '%s@gen' % uuid
                    account_list = Account.objects.filter(email=email)
                    if account_list.count() == 0:
                        account = Account.objects.create_user(email, password)
                        account.uuid = uuid
                        account.first_name = "Guest"
                        account.save()
                    else:
                        account = account_list[0]
                        if account_list.count() > 1:
                            for account_del in account_list[1:]:
                                account_del.delete()
        return account

    @staticmethod
    def api_regis_uuid(uuid, is_uuid_create=False):
        import random, string
        email = '%s@gen' % uuid
        account = Account.objects.filter(email__contains=email).first()
        if account is None:
            password = ''.join(
                random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(12))
            account = Account.objects.create_user(email, password)
            account.uuid = uuid
            account.first_name = "Guest"
            account.save()
        return account

    def get_token_forgot(self, status):
        forgot = self.forgot_set.filter(
            status=status,
            hash_password__isnull=True
        ).last()
        if forgot:
            return forgot.token
        else:
            return None

    def update_uuid(self, uuid, is_login_facebook):
        if uuid is not None:
            Account.objects.filter(uuid=uuid).exclude(id=self.id).update(uuid='')
            self.uuid = uuid
            if is_login_facebook is not None:
                self.is_login_facebook = is_login_facebook
            self.save(update_fields=['uuid', 'is_login_facebook'])

    @staticmethod
    def api_getaccountbyemail(email):
        account = None
        if email is not None and len(email) != 0:
            try:
                account = Account.objects.get(email=email.lower())
            except:
                pass
        return account

    def forgot_password(self, status):
        import random
        import string

        if status == 4:
            _status = -4
        else:
            _status = 0
        if self.forgot_set.filter(status=status):
            self.forgot_set.filter(
                status=status
            ).update(status=_status)

        while True:
            token = ''.join(
                random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(32)
            )
            if not Forgot.objects.filter(token=token).exists():
                Forgot.objects.create(
                    account=self,
                    status=status,
                    token=token
                )
                return token

    def resetpassword(self):
        account = self.forgot_set.get()
        if not account is None:
            u = account.objects.set_password()
            u.save()

    def api_practice_subscribe(self):
        subscribe_list = list()
        for subscribe in self.practice_subscribe.all():
            subscribe_list.append(subscribe.practice.api_display_title(account=self))
        return subscribe_list

    def api_course_subscribe(self):
        subscribe_list = list()
        for subscribe in self.course_subscribe.all():
            subscribe_list.append(subscribe.course.api_display_title())
        return subscribe_list

    def api_lesson_subscribe(self):
        subscribe_list = list()
        for subscribe in self.lesson_subscribe.all():
            if subscribe.lesson.is_display is True:
                subscribe_list.append(subscribe.lesson.api_display_title())
        return subscribe_list

    def lesson_subscribe_list(self):
        from lesson.cached import cached_lesson
        subscribe_list = []
        for lesson_id in self.lesson_subscribe.values_list('lesson_id', flat=True).all().order_by('-timestamp'):
            lesson = cached_lesson(lesson_id)
            if lesson is not None and lesson.is_display is True:
                subscribe_list.append(lesson)
        return subscribe_list

    def lesson_owner_list(self):
        from lesson.cached import cached_lesson
        result = []
        for lesson_id in self.lesson_account.values_list('id', flat=True).filter(is_display=True, tutor=None).order_by(
                '-timestamp'):
            lesson = cached_lesson(lesson_id)
            if lesson is not None:
                result.append(lesson)
        return result

    def api_lesson_owner(self):
        lesson_list = list()
        for lesson in self.lesson_account.filter(is_display=True):
            lesson_list.append(lesson.api_display_title())
        return lesson_list

    def order_course(self):
        from order.models import Item
        from course.models import Course
        course_list = list()
        order_list_id = [order.id for order in self.order_account.filter(status=3)]
        content_list_id = [item.content for item in Item.objects.filter(order__in=order_list_id).distinct()]
        return Course.objects.filter(id__in=content_list_id)

    def device_log(self, uuid_str, action):
        uuid = Uuid.pull(uuid_str)
        device_log = DeviceLog.objects.create(account=self,
                                              parent=None,
                                              uuid=uuid,
                                              action=action)
        if action in [1, 2]:
            device_list = Device.objects.filter(uuid=uuid)
            found = False
            for device in device_list:
                if device.account == self:
                    found = True
                else:
                    device_log = DeviceLog.objects.create(account=device.account,
                                                          parent=self,
                                                          uuid=uuid,
                                                          action=4)  # Logout By Other
                    device.delete()
            if not found:
                device = Device.objects.create(account=self,
                                               uuid=uuid)

    def set_user_address(self, name, address, subdistrict, district, province, zipcode, tel):
        user_address = self.address_set.first()
        if user_address is None:
            user_address = Address.objects.create(account=self,
                                                  name=name,
                                                  address=address,
                                                  subdistrict=subdistrict,
                                                  district=district,
                                                  province=province,
                                                  zipcode=zipcode,
                                                  tel=tel)
        else:
            user_address.name = name
            user_address.address = address
            user_address.subdistrict = subdistrict
            user_address.district = district
            user_address.province = province
            user_address.zipcode = zipcode
            user_address.tel = tel
            user_address.save()
        return user_address

    def get_user_level(self):
        from access.cached import cached_access_account_permission
        return cached_access_account_permission(self.id)

    # TODO : Remove
    # def get_progress_cached(self):
    #     from progress.models import Account as ProgressAccount
    #     self.progress_cached = ProgressAccount.pull(self.id, False)

    # TODO : cached
    def get_direct_manager_display(self):
        from django.conf import settings
        if settings.IS_ORGANIZATION:
            parent = self.org_parent_account_set.select_related('parent').first()
            if parent is None:
                return '-'
            else:
                return '%s %s' % (parent.parent.first_name, parent.parent.last_name)
        else:
            return '-'

    def get_direct_manager_email_display(self):
        from django.conf import settings
        if settings.IS_ORGANIZATION:
            parent = self.org_parent_account_set.select_related('parent').first()
            if parent is None:
                return '-'
            else:
                return '%s' % parent.parent.email
        else:
            return '-'

    # TODO : cached
    def get_department_display(self):
        # from .cached import cached_account_department_display
        # return cached_account_department_display(self.id)
        # Next Deploy 2017-09-22

        from department.models import Department
        _ = None
        for department in Department.objects.filter(member__account=self).distinct():
            if _ is None:
                _ = department.name.title()
            else:
                _ += ', %s' % department.name.title()
        if _ is None:
            return '-'
        else:
            return _

    def get_position_member_display(self):
        from department.models import Member
        _ = None
        for member in Member.objects.filter(account=self).distinct():
            if _ is None:
                _ = member.position.title()
            else:
                _ += ', %s' % member.position.title()
        if _ is None:
            return '-'
        else:
            return _

    def get_company_ais_account(self):
        account_info_list = self.get_account_ais()
        if account_info_list is None:
            return '-'
        _ = None
        for account_info in account_info_list:
            if _ is None:
                _ = account_info.company
            else:
                _ += ', %s' % account_info.company
        if _ is None:
            return '-'
        else:
            return _

    def get_employee_ais_account(self):
        account_info_list = self.get_account_ais()
        if account_info_list is None:
            return '-'

        _ = None
        for account_info in account_info_list:
            if _ is None:
                _ = account_info.employee
            else:
                _ += ', %s' % account_info.employee
        if _ is None:
            return '-'
        else:
            return _

    def get_account_ais(self):
        if 'ais' in settings.INSTALLED_APPS:
            from ais.models import AccountInfo
            return AccountInfo.objects.filter(account=self).distinct()
        else:
            return None

    def deactivate(self, is_logout=True):
        from .cached import cached_account_delete

        cached_account_delete(self.id)
        self.is_active = False
        if is_logout:
            self.token = ''
        self.save(update_fields=['is_active', 'token'])

    def effect_is_active(self):
        import django_rq
        from .jobs_effect_is_active import effect_is_active_job
        django_rq.enqueue(effect_is_active_job, self.id)


class Forgot(models.Model):
    STATUS_CHOICES = (
        (-4, 'UnForce'),

        (0, 'Activate'),
        (0, 'Deactivate'),
        (2, 'Completed'),

        (3, 'forgot'),
        (4, 'Force'),
        (5, 'first'),
        (6, 'expired')
    )

    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    token = models.CharField(max_length=120)
    hash_password = models.CharField(max_length=256, null=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=1)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']


class Address(models.Model):
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=128)
    address = models.CharField(max_length=128)
    subdistrict = models.CharField(max_length=32, blank=True)
    district = models.CharField(max_length=32)
    province = models.CharField(max_length=32)
    zipcode = models.CharField(max_length=8)
    tel = models.CharField(max_length=12, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-timestamp']

    def __str__(self):
        return self.account.email

    def api_display(self):
        d = {}
        d['name'] = self.name
        d['address'] = self.address
        d['subdistrict'] = self.subdistrict
        d['district'] = self.district
        d['province'] = self.province
        d['zipcode'] = self.zipcode
        d['tel'] = self.tel
        return d

    def readable(self):
        return '%s, %s, %s, %s, %s, %s, %s' % (
            self.name, self.address, self.subdistrict, self.district, self.province, self.zipcode, self.tel)


class Uuid(models.Model):
    uuid = models.CharField(max_length=120, blank=True, db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']

    def __str__(self):
        return self.uuid

    @staticmethod
    def pull(_uuid):
        uuid = Uuid.objects.filter(uuid=_uuid).first()
        if uuid is None:
            uuid = Uuid.objects.create(uuid=_uuid)
        return uuid


class Device(models.Model):
    CHANNEL_CHOICES = (
        (-1, 'Unknown'),
        (0, 'Web'),
        (10, 'API'),
        (11, 'API-Android'),
        (12, 'API-IOS'),
    )

    account = models.ForeignKey(Account)
    channel = models.IntegerField(choices=CHANNEL_CHOICES, default=-1)
    device = models.TextField(blank=True, null=True, default='(Not Set)')
    uuid = models.ForeignKey(Uuid, null=True, blank=True)
    session = models.CharField(max_length=120, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.device

    def api_display(self):
        d = {}
        d['id'] = self.id
        d['channel'] = self.channel
        d['channel_display'] = self.get_channel_display()
        d['uuid'] = self.uuid.uuid
        d['session'] = self.session
        d['timestamp'] = self.timestamp.strftime(settings.TIMESTAMP_FORMAT_)
        return d


class DeviceLog(models.Model):
    ACTION_CHOICES = (
        (1, 'Login'),
        (2, 'Login Facebook'),
        (3, 'Logout'),
        (4, 'Logout By Other'),
    )

    account = models.ForeignKey(Account)
    parent = models.ForeignKey(Account, related_name='device_parent', null=True)
    channel = models.IntegerField(choices=Device.CHANNEL_CHOICES, default=-1)
    uuid = models.ForeignKey(Uuid)
    session = models.CharField(max_length=120, blank=True)
    action = models.IntegerField(choices=ACTION_CHOICES)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']


class Session(models.Model):
    account = models.OneToOneField(Account)
    key = models.CharField(max_length=255)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']

    @staticmethod
    def push(account, key):
        from importlib import import_module
        from django.conf import settings
        SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
        session = Session.objects.filter(account=account).first()
        if session is not None:
            if session.key != key:
                s = SessionStore(session.key)
                s.delete()
                session.key = key
                session.save()
        else:
            Session.objects.create(account=account,
                                   key=key)
