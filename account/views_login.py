from django.conf import settings
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.http import require_GET

from active.views import login_report


@require_GET
def login_view(request):
    if len(request.GET) > 0:
        return HttpResponse(status=403)

    if request.user.is_authenticated():
        login_report(request)
        return redirect('home')
    # Debug VA Scan

    # ref = request.GET.get('ref', None)
    # next = request.GET.get('next', None)
    # if ref is None and next is None:
    #     ref = request.get_full_path()
    #     next = request.get_full_path()

    return render(request,
                  'account/login.html',
                  {
                      'is_forgot_password': settings.IS_FORGOT_PASSWORD,
                  })
