from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.conf import settings

from order.models import Item
from progress.models import Progress
from content.models import Location as ContentLocation

from .views import item_filter
from .views_base import get_ais_account_info, sum_progress_item
from progress.cached import cached_progress_account_content
from utils.content import get_content


@login_required
def profile_view(request):

    progress = int(request.GET.get('progress', -1))
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    item_course_list = Item.objects.select_related('order').filter(order__account=request.user,
                                                                   order__status=3,
                                                                   status=2,
                                                                   content_type=content_type_course).order_by('status', '-timestamp')

    for item_course in item_course_list:
        item_course.content_object = get_content(item_course.content_type_id, item_course.content)
        content_type = ContentLocation.pull_first(settings.CONTENT_TYPE_ID(item_course.content_type_id),
                                                  item_course.content)
        item_course.progress = Progress.pull(item_course,
                                             content_type,
                                             settings.CONTENT_TYPE_ID(item_course.content_type_id),
                                             item_course.content)

    item_list = item_filter(item_course_list, progress)
    progress_stat = sum_progress_item(item_course_list)
    account_content = cached_progress_account_content(request.user.id, request.APP.provider)

    # AIS
    request.user.info = get_ais_account_info(account=request.user)
    return render(request,
                  'account/profile.html',
                  {'ROOT_PAGE': 'profile',
                   'item_course_list': item_list,
                   'is_owner': True,
                   'select': 'course',
                   'account': request.user,
                   'progress_stat': progress_stat,
                   'account_content': account_content})
