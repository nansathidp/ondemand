from django.conf import settings
from django.contrib.auth import login
from django.core.urlresolvers import reverse
from docutils.nodes import term

from account.models import Forgot
from datetime import datetime, date, timedelta
from django.utils import timezone
from term.models import Term

STATUS_MSG = {

    2: 'Your Account is blocked, Please contact admin',
    4: 'Your Account do have force password',
    5: 'Your first login, Please check your email and set password.',
    6: 'Your password expired, Please check your email and set password.',

    200: 'Success',
    313: 'Account is inactive, Please contact admin',

}


class LoginPolicy():

    def __init__(self, request, account, uuid=None):
        self._request = request
        self._uuid = uuid
        self._account = account

        # Config
        self._is_config_first = settings.IS_FIRST_LOGIN
        self._is_config_force = settings.IS_FORCE_RESET_PASSWORD
        self._is_config_enable = settings.IS_ENABLE_ACCOUNT

        # Mark remove
        self._is_first = account.is_first

        # True = No Force
        # False = Force
        self._is_force = account.is_force_reset_password
        self._is_enable = account.is_enable
        self._is_login = True

        if account.datetime_change_password is None:
            is_expired = False
        else:
            if account.datetime_change_password > timezone.now() - timezone.timedelta(days=settings.LIMIT_AGE_PASSWORD):
                is_expired = False
            else:
                is_expired = True

        self._is_expired = not is_expired
        self._number_status = 0
        self._data = {}
        self.next = None

    def is_validate(self):
        if self._is_check_config_match_account():
            return True
        else:
            if self._uuid:
                self._set_data_api_error()
            else:
                self._set_data_error()
            return False

    def get_result(self):
        return self._data

    def _get_token_forgot_password(self):
        return Forgot.objects.filter(account=self._account, hash_password__isnull=True,
                                     status=self._number_status).last()

    def _set_data_error(self):
        result = {}
        self._data['status'] = 401
        self._data['status_msg'] = STATUS_MSG[self._number_status]
        if (self._number_status > 3) and (self._number_status < 6):
            forgot = self._get_token_forgot_password()
            if forgot:
                self._data['status'] = 200
                result['link'] = '%s?token=%s&status=%s' % (reverse('reset_password'), forgot.token, forgot.status)
            else:
                self._account.forgot_password(status=self._number_status)
                self._set_data_error()
        else:
            result['link'] = None
        self._data['result'] = result

    def _set_data_api_error(self):
        result = {}
        self._data['status'] = 401
        self._data['status_msg'] = STATUS_MSG[self._number_status]
        result['is_force_reset_password'] = False
        if (self._number_status > 3) and (self._number_status < 6):
            result['is_enable'] = self._is_enable
            forgot = self._get_token_forgot_password()
            if forgot:
                result['token_force_password'] = forgot.token
                result['token_status_password'] = self._number_status
                result['is_force_reset_password'] = True
            else:
                self._account.forgot_password(status=self._number_status)
                self._set_data_api_error()
        else:
            result['is_enable'] = self._is_enable
        self._data['result'] = result

    def _is_get_config_all(self):
        # return all([self._is_config_force, self._is_config_enable, self._is_config_first])
        return self._is_config_force and self._is_config_enable and self._is_config_first


    def _is_get_config_first_enable(self):
        return self._is_config_first and self._is_enable

    def _is_get_config_force_enable(self):
        return self._is_config_force and self._is_config_enable

    def _is_check_config_match_account(self):
        if self._is_get_config_all():
            if self._is_first and self._is_force and self._is_expired and self._is_enable:
                self._is_login = True
            else:
                self._is_login = False
                if self._is_enable is False:
                    self._number_status = 2
                elif self._is_expired is False:
                    self._number_status = 6
                elif self._is_force is False:
                    self._number_status = 4
                else:
                    self._number_status = 5
        elif self._is_get_config_first_enable():
            if self._is_enable and self._is_first:
                self._is_login = True
            else:
                self._is_login = False
                if self._is_enable is False:
                    self._number_status = 2
                else:
                    self._number_status = 5
        elif self._is_get_config_force_enable():
            if self._is_force and self._is_enable:
                self._is_login = True
            else:
                self._is_login = False
                if self._is_enable is False:
                    self._number_status = 2
                else:
                    self._number_status = 4
        elif self._is_config_enable:
            self._is_login = False
            if self._is_enable:
                self._is_login = True
            else:
                self._number_status = 2
        # CIMB #
        elif self._is_config_force:
            self._is_login = False
            if self._is_force:
                self._is_login = True
            else:
                self._number_status = 4

        elif self._is_config_first:
            self._is_login = False
            if self._is_first:
                self._is_login = True
            else:
                self._number_status = 5
        else:
            if self._is_expired:
                self._is_login = True
            else:
                self._number_status = 6
                self._is_login = False
        return self._is_login

    def login(self):
        result = {}
        from mailer.models import Mailer
        self._number_status = 200
        next = None
        if self._account.is_active:
            if self._account.first_active is None:
                self._account.first_active = timezone.now()
            self._account.last_active = timezone.now()
            login(self._request, self._account)
            self._account.save(update_fields=['last_active', 'first_active'])
            self._request.session["is_skip"] = False
            app_id = settings.CONTENT_TYPE('app', 'app').id
            term = Term.pull_content(app_id, self._request.APP.id)
            print(self._request.APP.id, app_id)
            if term and Term.is_require_term(app_id, self._request.APP.id, self._account.id):
                next = reverse('term')

            # Fix Case /login/
            if next is None:
                if self._request.method == 'POST':

                    next = self._request.POST.get('next', None)
                    if next is not None:
                        self._data['link'] = next
            if settings.IS_LOGIN_ALERT:
                account_group_id_list = list(self._account.groups.values_list('id', flat=True).all())
                if set(account_group_id_list) & set(settings.ALERT_AUTH_GROUP_ID):
                    Mailer.send_login_alert(self._request, self._account)
        else:
            self._number_status = 313
        result['link'] = next
        self._data['status'] = self._number_status
        self._data['status_msg'] = STATUS_MSG[self._number_status]
        self._data['result'] = result

    def api_login(self):
        result = {
            'token': self._account.login_token()
        }
        if self._account.first_active is None:
            self._account.first_active = timezone.now()
        self._account.last_active = timezone.now()
        self._account.update_uuid(self._uuid, is_login_facebook=None)
        self._account.device_log(self._uuid, 1)  # Basic Login
        self._account.save(update_fields=['last_active', 'first_active'])
        result['profile'] = self._account.api_profile()

        term = Term.pull_content(settings.CONTENT_TYPE('app', 'app').id,
                                 self._request.APP.id)
        if term:
            result['terms'] = term.api_display()
            result['is_require_terms'] = Term.is_require_term(settings.CONTENT_TYPE('app', 'app').id,
                                                              self._request.APP.id,
                                                              self._account.id)
        else:
            result['terms'] = None
            result['is_require_terms'] = False
        self._number_status = 200
        self._data['status'] = 200
        self._data['status_msg'] = STATUS_MSG[self._number_status]
        self._data['result'] = result
