from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from account.cached import cached_account_update
from ondemand.views_decorator import check_project
from api.views_api import json_render
from PIL import Image
import datetime


@check_project
def edit_view(request):
    if not request.user.is_authenticated():
        return redirect('home')
    is_valid_email = request.user.is_valid_email()
    return render(request, 'account/profile_edit.html', {'is_valid_email': is_valid_email})


def crop_image(img_path, cropW, cropH):
    THUMB_SIZE = cropW, cropH
    img = Image.open(img_path)
    width, height = img.size

    if width > height:
        delta = width - height
        left = int(delta / 2)
        upper = 0
        right = height + left
        lower = height
    else:
        delta = height - width
        left = 0
        upper = int(delta / 2)
        right = width
        lower = width + upper

    img = img.crop((left, upper, right, lower))
    img.thumbnail(THUMB_SIZE, Image.ANTIALIAS)
    img.save(img_path)
    return True


@login_required
def upload_view(request):
    result = {}
    code = 200
    if request.method == 'POST':
        image = request.FILES.get('profile', None)
        if image is not None:
            account = request.user
            account.image = image
            account.save(update_fields=['image'])
            crop_image(account.image.path, 200, 200)
            cached_account_update(account)
        else:
            code = 201
    return json_render(result, code)


@login_required
def action_view(request):
    is_redirect = None
    if request.method == 'POST':
        account = request.user
        account.first_name = request.POST.get('firstname', None)
        account.last_name = request.POST.get('lastname', None)
        account.gender = request.POST.get('gender', None)
        try:
            account.birthday = datetime.datetime.strptime(request.POST.get('birthday', None), "%Y-%m-%d").date()
        except:
            account.birthday = None
        account.title = request.POST.get('title', None)
        account.desc = request.POST.get('desc', None)
        account.location = request.POST.get('location', None)
        account.tel = request.POST.get('tel', None)

        image = request.FILES.get('profile', None)
        if image is not None:
            account.image = image
        account.save(update_fields=['first_name', 'last_name', 'gender', 'birthday', 'title', 'desc', 'location', 'tel', 'image'])
        if image is not None:
            try:
                crop_image(account.image.path, 200, 200)
            except:
                pass
        is_redirect = request.POST.get('redirect', None)

    if is_redirect == '0':
        return redirect('profile_edit')
    else:
        return redirect('profile')
