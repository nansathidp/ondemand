from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from account.views_base import get_ais_account_info
from content.models import Location as ContentLocation
from ondemand.views_decorator import check_project
from order.models import Item
from program.cached import cached_onboard_id_list
from progress.cached import cached_progress_account_content
from progress.models import Progress


@check_project
@login_required
def program_view(request):
    progress = int(request.GET.get('progress', -1))
    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_type_onboard = settings.CONTENT_TYPE('program', 'onboard')

    onboard_id_list = cached_onboard_id_list()
    item_list_program = Item.objects.filter(
        order__account=request.user,
        order__status=3,
        status=2,
        content_type_id=content_type_program.id
    ).exclude(
        content__in=onboard_id_list
    ).order_by('-order__timestamp')
    order_item_program_list = []

    progress_stat = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0
    }
    item_list_onboard = Item.objects.filter(
        order__account=request.user,
        order__status=3,
        status=2,
        content__in=onboard_id_list,
        content_type_id=content_type_onboard
    ).order_by('-order__timestamp')
    order_item_onboard_list = []
    for order_item in item_list_program:
        order_item.progress = Progress.pull(order_item,
                                            ContentLocation.pull(order_item.content_location_id),
                                            settings.CONTENT_TYPE_ID(order_item.content_type_id),
                                            order_item.content)
        if order_item.progress is not None:
            progress_stat[order_item.progress.status] += 1
        if progress == -1:
            order_item_program_list.append(order_item)
        elif order_item.progress is not None and order_item.progress.status == progress:
            order_item_program_list.append(order_item)

    for order_item_onboard in item_list_onboard:
        print(order_item_onboard)
        order_item_onboard.progress = Progress.pull(
            order_item_onboard,
            ContentLocation.pull(order_item_onboard.content_location_id),
            settings.CONTENT_TYPE_ID(order_item_onboard.content_type_id),
            order_item_onboard.content
        )
        if order_item_onboard.progress is not None and order_item_onboard.progress.status == 2 :
            progress_stat[order_item_onboard.progress.status] += 1
        if progress == -1 and order_item_onboard.progress.status == 2:
            order_item_onboard_list.append(order_item_onboard)
        elif order_item_onboard.progress is not None and order_item_onboard.progress.status == progress and order_item_onboard.progress.status == 2:
            order_item_onboard_list.append(order_item_onboard)

    account_content = cached_progress_account_content(request.user.id, request.APP.provider)
    # AIS
    request.user.info = get_ais_account_info(account=request.user)
    return render(request,
                  'account/profile_program.html',
                  {
                      'account': request.user,
                      'is_owner': True,
                      'select': 'program',
                      'ROOT_PAGE': 'profile',
                      'order_item_list': order_item_program_list,
                      'order_item_onboard_list': order_item_onboard_list,
                      'progress_stat': progress_stat,
                      'account_content': account_content
                  })
