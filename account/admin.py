from django.contrib import admin
from account.models import *
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from content.models import Location as ContentLocation
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django import forms

class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Account
        fields = ('email', 'password', 'is_active', 'is_admin')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

class AccountPairFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('User type')
    # # Parameter for the filter that will be used in the URL query.
    parameter_name = 'user_type'

    def lookups(self, request, model_admin):
        return (
            ('Original', _('Original')),
            ('Migrate', _('Migrate'))
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value() == 'Original':
            return queryset.filter(pair_id=-1)
        elif self.value() == 'Migrate':
            return queryset.filter(pair_id__gt=0)


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    # form = UserChangeForm

    list_display = (
        'email', 'uid', 'employee',
        'is_login_facebook', 'facebook_id',
        'first_name', 'last_name', 'gender', 'tel', 'token',
        'date_joined', 'is_enable', 'is_active', 'datetime_change_password')
    exclude = ('school',)
    search_fields = ['email', 'first_name', 'last_name', 'token', 'tel']
    list_filter = ('is_login_facebook', 'gender', 'is_admin', 'is_active', AccountPairFilter)
    actions = ['push_course', 'gen_token']
    list_per_page = 25

    @staticmethod
    def push_course(self, request, queryset):
        from django.conf import settings
        
        from order.models import Item, Order
        from course.models import Course
        content_type = settings.CONTENT_TYPE('course', 'course')
        now = timezone.now()
        for account in queryset:
            purchased_item_list = Item.objects.filter(order__account=account).values_list('content', flat=True)
            for course in Course.objects.filter(is_display=True).exclude(id__in=purchased_item_list):
                content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                              course.id)
                order_item = Order.buy(content_location,
                                       request.APP,
                                       0,
                                       content_type,
                                       course,
                                       account,
                                       is_checkout=True)
                order_item.order.clear_price()
                
    @staticmethod
    def gen_token(self, request, queryset):
        import random, string
        for account in queryset:
            if account.token is not None and len(account.token) == 32:
                pass
            else:
                while True:
                    token = ''.join(
                        random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in
                        range(32))
                    if Account.objects.filter(token=token).count() == 0:
                        account.token = token
                        account.save()
                        break


@admin.register(Forgot)
class Forgot(admin.ModelAdmin):
    list_display = (
        'account',
        'token',
        'status',
        'timestamp'
    )


@admin.register(Address)
class Address(admin.ModelAdmin):
    list_display = ('account', 'name', 'address', 'subdistrict', 'district', 'province', 'zipcode', 'tel', 'timestamp')
    search_fields = ['account__email', 'address', 'zipcode', 'tel']
    readonly_fields = ('account', 'timestamp')


@admin.register(Uuid)
class UuidAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'timestamp')


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    list_display = ('account', 'device', 'uuid', 'timestamp')


@admin.register(DeviceLog)
class DeviceLogAdmin(admin.ModelAdmin):
    list_display = ('account', 'parent', 'uuid', 'action', 'timestamp')


@admin.register(Session)
class SessionAdmin(admin.ModelAdmin):
    list_display = ('account', 'key', 'timestamp')
    
