from django.shortcuts import render, redirect


def signup_view(request):
    if request.user.is_authenticated():
        return redirect('home')
    
    ref = request.GET.get('ref', None)
    return render(request,
                  'account/signup.html',
                  {'ref': ref})
