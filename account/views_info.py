from django.conf import settings
from django.shortcuts import render, redirect

from ondemand.views_decorator import check_project
from .views_edit import crop_image


@check_project
def view(request):
    if not request.user.is_authenticated():
        return redirect('home')

    if request.method == 'POST':
        image = request.FILES.get('profile', None)
        if image is not None:
            account = request.user
            account.image = image
            account.save(update_fields=['image'])
            crop_image(account.image.path, 200, 200)

    if settings.PROJECT == 'ais':
        from ais.models import AccountInfo
        account = request.user
        account_info = AccountInfo.objects.filter(account=account).first()
        return render(request,
                      'account/info.html',
                      {'account_info': account_info})
    else:
        return render(request,
                      'account/info.html',
                      {})
