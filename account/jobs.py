from django.core.validators import validate_email
from django.conf import settings

import os

import time
import xlrd
from django.utils import timezone
from alert.models import Alert
from account.models import Account
from organization.models import Parent
from department.models import Department, Member as DepartmentMember

ORGANIZATION_LIST = []
DEPARTMENT_DICT = {}


def _disabled_account_age():
    # Mark : Definition
    Account.objects.filter(is_force_reset_password=True).exclude(datetime_change_password__gt=timezone.now() - timezone.timedelta(days=settings.LIMIT_AGE_PASSWORD)).update(
        is_force_reset_password=False)
    account = Account.objects.filter(is_force_reset_password=False).exclude(datetime_change_password__gt=timezone.now() - timezone.timedelta(days=settings.LIMIT_AGE_PASSWORD))
    if account:
        for _ in account:
            _forgot_password(_.email)


def _email(email, type):
    if type in [2, 3]:
        email = str(int(float(email))).strip()
    else:
        email = str(email).strip()

    if len(email) == 0:
        return None

    if email.find('@') == -1:
        email = '%s@%s' % (email, settings.DEFAULT_EMAIL_SUFFIX)
    try:
        validate_email(email)
        return email.lower()
    except:
        return None


def _password(password, type):
    # Type: http://www.lexicon.net/sjmachin/xlrd.html#xlrd.Cell-class
    if type in [2, 3]:
        password = str(int(float(password)))
    return password


def _department(_):
    # return [department.strip() for department in _.split(',')]
    return _.strip().lower()


def _position(_):
    return _.strip()


def _status(_):
    _ = str(_).lower()
    if _ in ['a', 'active', '1', 'true']:
        return True
    else:
        return False


def _push_account(data, amount_import, amount_user):
    if data['email'] is None:
        return None
    account = Account.objects.filter(email=data['email']).first()
    if account is None:
        account = Account(email=data['email'])

    account.first_name = data['first_name']
    account.last_name = data['last_name']
    account.is_active = data['status']

    if settings.IS_LIMIT_ACTIVE:
        if amount_user >= settings.MAX_USER:
            account.is_active = False
        if settings.MAX_USER - amount_user < int(amount_import):
            account.is_active = False

    if len(data['password']) > 0:
        account.set_password(data['password'])
    account.save()
    return account


def _push_department(account, name, position):
    global DEPARTMENT_DICT
    if name in DEPARTMENT_DICT:
        department = DEPARTMENT_DICT[name]
    else:
        department = Department.objects.filter(name=name).first()
        if department is None:
            department = Department.objects.create(name=name)
            DEPARTMENT_DICT[name] = department

    # member = DepartmentMember.objects.filter(
    #     department=department,
    #     account=account
    # ).first()
    # if member is None:
    #     member = DepartmentMember.objects.create(
    #         department=department,
    #         account=account,
    #         position=position
    #     )
    # elif member.department_id != department.id or member.position != position:
    #     member.department = department
    #     member.position = position[:120]
    #     member.save()

    # Fix One Department
    member_list = DepartmentMember.objects.filter(
        account=account
    )
    count = 0
    for member in member_list:
        count += 1
        if count == 1:
            if member.department_id != department.id or member.position != position:
                member.department = department
                member.position = position[:255]
                member.save()
        else:
            member.delete()
    if count == 0:
        DepartmentMember.objects.create(
            department=department,
            account=account,
            position=position[:255]
        )

def _push_organization():
    global ORGANIZATION_LIST
    for account_id, parent_email in ORGANIZATION_LIST:
        account_parent = Account.objects.filter(email=parent_email).first()
        if account_parent is not None:
            parent = Parent.objects.filter(account_id=account_id).first()
            if parent is not None and parent.parent_id != account_parent.id:
                parent.parent_id = account_parent.id
                parent.save(update_fields=['parent'])
            else:
                parent = Parent.objects.create(parent=account_parent,
                                               account_id=account_id)


def _import(alert_id):
    try:
        alert = Alert.objects.get(id=alert_id)
    except:
        return None

    global ORGANIZATION_LIST
    ORGANIZATION_LIST = []
    print('Run Alert:', alert.id)

    path = os.path.join(settings.BASE_DIR, 'alert', 'upload', alert.file_input)
    book = xlrd.open_workbook(path)
    sh = book.sheet_by_index(0)
    count = 0

    amount_import = sh.nrows - 1

    for rx in range(sh.nrows):
        count += 1
        if count == 1:
            continue
        data = {
            'email': _email(sh.cell(rx, 0).value, sh.cell(rx, 0).ctype),
            'password': _password(str(sh.cell(rx, 1).value), sh.cell(rx, 1).ctype),
            'first_name': sh.cell(rx, 2).value,
            'last_name': sh.cell(rx, 3).value,
            'department': _department(sh.cell(rx, 4).value),
            'position': _position(sh.cell(rx, 5).value),
            'org': _email(sh.cell(rx, 6).value, sh.cell(rx, 6).ctype),
            'status': _status(sh.cell(rx, 7).value),
        }

        amount_user = Account.objects.filter(is_active=True).count()
        account = _push_account(data, amount_import, amount_user)

        if account is not None:
            _push_department(account, data['department'], data['position'])
            if data['org'] is not None:
                ORGANIZATION_LIST.append([account.id, data['org']])

    time.sleep(2)
    alert.status = 2
    alert.save(update_fields=['status'])


def _forgot_password(email):
    account = Account.objects.get(email=email)
    token = account.forgot_password(4)
    sender_email = settings.EMAIL_HOST_USER
    from mailer.models import Mailer
    Mailer.send_forget_password_web(account, sender_email, token)
