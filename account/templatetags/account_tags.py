from django import template

from ..models import Account

register = template.Library()


@register.simple_tag
def pull_account_tag(id):
    return Account.pull(id)
