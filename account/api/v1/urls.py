from django.conf import settings
from django.conf.urls import url

from .views_forgot_password import view_forgot_password
from .views_image import upload_image_view
from .views_login import login_view
from .views_login_facebook import login_facebook_view
from .views_logout import logout_view
from .views_profile import profile_view
from .views_register import register_view
from .views_reset_password import view_reset_password

urlpatterns = [
    url(r'^profile/$', profile_view),  # TODO: Testing
    url(r'^register/$', register_view),  # TODO: Testing
    url(r'^logout/$', logout_view),  # TODO: Testing
    url(r'^login/facebook/$', login_facebook_view),  # TODO: Testing
    url(r'^image/upload/$', upload_image_view),  # TODO: Testing
    url(r'^resetpassword/$', view_reset_password),
    url(r'^forgotpassword/$', view_forgot_password)
]

if settings.PROJECT == 'unilever':
    from unilever.api.v1.views_login import login_view

    urlpatterns += [
        url(r'^login/$', login_view),
    ]
elif settings.PROJECT == 'ais':
    from ais.api.v1.views_login import login_view

    urlpatterns += [
        url(r'^login/$', login_view),
    ]
else:
    urlpatterns += [
        url(r'^login/$', login_view),
    ]
