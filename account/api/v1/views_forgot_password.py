from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from account.api.v1.views import json_render
from account.models import Account
from api.v4.views import check_key
from mailer.models import Mailer


@csrf_exempt
def view_forgot_password(request):
    code = 200
    result = {}
    response = check_key(request)
    if response is not None:
        return response

    if request.method == 'POST':
        email = request.POST.get('email', None)
        _uuid = request.POST.get('uuid', None)
        if email is not None:
            try:
                account = Account.objects.get(email=email)

                if not account.is_active:
                    return json_render({}, 313)

                if settings.IS_ENABLE_ACCOUNT:
                    if account.is_enable:
                        token = account.forgot_password(2)
                        sender_email = settings.EMAIL_HOST_USER
                        Mailer.send_forget_password_web(account, sender_email, token)
                        return json_render({}, code)
                    else:
                        code = 316
                        return json_render({}, code)
                else:
                    token = account.forgot_password(2)
                    sender_email = settings.EMAIL_HOST_USER
                    Mailer.send_forget_password_web(account, sender_email, token, 2)
                    return json_render({}, code)
            except:
                code = 315
                return json_render({}, code)
        else:
            code = 315
            return json_render({}, code)
    else:
        code = 201
        return json_render(result, code)
