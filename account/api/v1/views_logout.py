from .views import json_render

from account.models import Account, Uuid, Device, DeviceLog

from account.cached import cached_api_account_auth_delete


def logout_view(request):
    result = {}
    code = 200
    if request.method == 'POST':
        token = request.POST.get('token', None)
        _uuid = request.POST.get('uuid', None)
    else:
        token = request.GET.get('token', None)
        _uuid = request.GET.get('uuid', None)

    if _uuid is None:
        return json_render(result, 202)
    if token is None:
        return json_render(result, 204)

    account = Account.api_auth(token, None)
    if account is None:
        return json_render(result, 400)

    uuid = Uuid.pull(_uuid)

    DeviceLog.objects.create(account=account,
                             parent=None,
                             uuid=uuid,
                             action=3)

    Device.objects.filter(account=account, uuid=uuid).delete()
    cached_api_account_auth_delete(account)
    return json_render(result, code)
