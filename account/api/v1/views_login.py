import urllib
from axes.decorators import watch_login

from django.conf import settings
from django.http import JsonResponse, HttpResponse
from django.contrib.auth import authenticate
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from account.models import Forgot
from account.api.v1.views import json_render
from api.v4.views import check_key
from term.models import Term
from account.login_policy import LoginPolicy


@csrf_exempt
@watch_login
def login_view(request):
    result = None
    code = 200
    is_login = True
    is_enable = True

    response = check_key(request)
    if response is not None:
        return response

    if request.method == 'POST':
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        uuid = request.POST.get('uuid', None)
    else:
        email = request.GET.get('email', None)
        password = request.GET.get('password', None)
        uuid = request.GET.get('uuid', None)

    if uuid is not None and len(uuid) > 0:
        pass
    else:
        return json_render({}, 202)

    if email is None or len(email) < 4 or password is None or len(password) < 4:
        return json_render({}, 300)
    else:
        email = urllib.parse.unquote(email)
        password = urllib.parse.unquote(password)
        account = authenticate(email=email, password=password)
        if account is not None:
            if account is not None:
                login_policy = LoginPolicy(account=account, request=request, uuid=uuid)
                if login_policy.is_validate():
                    login_policy.api_login()
                result = login_policy.get_result()
                return JsonResponse(result)
        else:
            return HttpResponse(status=401, content='The username or password you entered is incorrect.')
            # return json_render({}, 300)
    return json_render(result, code)


def login_policy(account):
    is_login = True
    is_force_resetpassword = True
    is_first_login = True
    if settings.IS_ENABLE_ACCOUNT:
        is_login = account.is_enable
    if settings.IS_FIRST_LOGIN:
        is_login = account.is_first
        is_first_login = account.is_first
    if settings.IS_FORCE_RESET_PASSWORD:
        is_login = account.is_force_reset_password
        is_force_resetpassword = account.is_force_reset_password
    return [is_login, is_force_resetpassword, is_first_login]


def _forgot_password(account, is_forgot):
    if is_forgot:
        try:
            forgot = Forgot.objects.filter(account=account, hash_password__isnull=True).last()
            return forgot
        except:
            token = None
            return token
    else:
        token = None
        return token


def _login(request, account, uuid, is_login=True):
    result = {}
    if is_login:
        result['token'] = account.login_token()
        if account.first_active is None:
            account.first_active = timezone.now()
        account.last_active = timezone.now()
        account.update_uuid(uuid, is_login_facebook=None)
        account.device_log(uuid, 1)  # Basic Login
        account.save(update_fields=['last_active', 'first_active'])
        result['profile'] = account.api_profile()

        term = Term.pull_content(settings.CONTENT_TYPE('app', 'app').id,
                                 request.APP.id)
        if term:
            result['terms'] = term.api_display()
            result['is_require_terms'] = Term.is_require_term(settings.CONTENT_TYPE('app', 'app').id,
                                                              request.APP.id,
                                                              account.id)
        else:
            result['terms'] = None
            result['is_require_terms'] = False

    return result
