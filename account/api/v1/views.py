from django.http import JsonResponse

from api.views_api import STATUS_MSG

_STATUS_MSG = {
    200: 'Success',
    201: 'Require POST method.',
    202: 'Require UUID.',
    204: 'Require token.',
    209: 'Require Store.',

    300: 'The username or password you entered is incorrect.',  # The username or password were incorrect.
    301: 'Facebook Login : access token cannot be null',  # Facebook Login : access token cannot be null
    302: 'Facebook Login : Please Signup.',  # Facebook Login : Please Signup.
    303: 'Facebook Login : access token fail.',  # Facebook Login : access token fail.
    304: 'Facebook Connect : access token cannot be null.',  # Facebook Connect : access token cannot be null.
    305: 'Facebook Connect : access token fail.',  # Facebook Connect : access token fail.
    306: 'The username or password were incorrect.',  # The username or password were incorrect.
    307: 'Duplicate Facebook Account',  # The username or password were incorrect.
    308: 'Require Access token',  # The username or password were incorrect.
    309: 'UUID Account not found.',
    310: 'Duplicate account',
    311: 'Register account error.',
    312: 'Register account error. Please register with email',
    313: 'Your account is inactive.',
    314: 'Your verify email for reset password ',
    315: 'Email not found.',
    316: 'Account is blocked, Please contact admin or reset new password',
    317: 'Your token has expired.',
    318: 'Invalid token, Please try again.',
    319: 'Your password has expired, Please reset new password.',
    320: 'Your first login ,Please reset new password.',
    321: 'Please check your email and set password.',
}


def json_render(result, status):
    if status in _STATUS_MSG:
        status_msg = _STATUS_MSG[status]
    else:
        status_msg = STATUS_MSG[status]
    data = {
        'status': status,
        'status_msg': status_msg,
        'result': result
    }
    return JsonResponse(data, json_dumps_params={'indent': 2})


from api.views import check_auth
from api.v5.views import check_key
from api.v5.views import check_require
