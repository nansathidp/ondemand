import datetime
import random
import string

from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from open_facebook import OpenFacebook

from account.models import Account
from api.v4.views import json_render, check_key


@csrf_exempt
def login_facebook_view(request):
    result = {}
    response = check_key(request)
    account = None
    if response is not None:
        return response
    if request.method == 'POST':
        access_token = request.POST.get('access_token', None)
        user_email = request.POST.get('email', -1)
        uuid = request.POST.get('uuid', None)
    else:
        access_token = request.GET.get('access_token', None)
        user_email = request.GET.get('email', -1)
        uuid = request.GET.get('uuid', None)

    if uuid is not None and len(uuid) > 0:
        pass
    else:
        return json_render({}, 202)

    if access_token is None or len(access_token) == 0:
        return json_render({}, 301)
    else:
        try:
            graph = OpenFacebook(access_token=access_token, version='v2.5')
            profile = graph.get('me', fields='id,name,email,birthday,gender')
            if 'email' in profile:
                email = profile['email']
            elif user_email not in [-1, '-1', None, '']:
                email = user_email
            else:
                email = None

            if email is not None:
                account_list = Account.objects.filter(email=email)
            else:
                account_list = Account.objects.filter(facebook_id=profile['id'])

            if account_list.count() == 0:
                if uuid is not None and len(uuid) > 0:
                    account_list = Account.objects.filter(uuid=uuid)
                    if account_list.count() > 0:
                        account = account_list[0]
                        account.facebook_id = profile['id']
                        if 'email' in profile:
                            account.email = profile['email']
                        else:
                            return json_render({}, 312)
                            account.email = '%s@facebook.com.gen' % (profile['id'])

                if account is None:
                    # Create New User
                    password = ''.join(
                        random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in
                        range(12))
                    if 'email' in profile:
                        email = profile['email']
                    else:
                        return json_render({}, 312)
                        email = '%s@facebook.com.gen' % (profile['id'])

                    account_list = Account.objects.filter(email=email)
                    if account_list.count() > 0:
                        account = account_list[0]
                        account.facebook_id = profile['id']
                    else:
                        account = Account.objects.create_user(email, password)
                        account.facebook_id = profile['id']

                        # Update
                if 'name' in profile:
                    account.first_name = profile['name']
                if 'sername' in profile:
                    account.last_name = profile['sername']
                if 'gender' in profile:
                    if profile['gender'].lower() == 'm' or profile['gender'].lower() == 'male':
                        account.gender = 1
                    elif profile['gender'].lower() == 'f' or profile['gender'].lower() == 'female':
                        account.gender = 2
                    else:
                        account.gender = 0
                if 'birthday' in profile:
                    try:
                        account.birthday = datetime.datetime.strptime(profile['birthday'], '%d/%m/%Y').date()
                    except:
                        account.birthday = None
                if not account.is_upload:
                    account.api_facebook_image()
                account.save()
                #AccountApp.api_app_push(request.APP, account)
                account.update_uuid(uuid, is_login_facebook=True)
                account.device_log(uuid, 2)  # Login Facebook
                # account.api_facebook_friend(graph)
                account.facebook_access_token = access_token
                account.last_active = timezone.now()
                account.save()
                result['token'] = account.login_token()
                result['profile'] = account.api_profile()
            else:
                account = account_list[0]
                #AccountApp.api_app_push(request.APP, account)
                account.update_uuid(uuid, is_login_facebook=True)
                account.device_log(uuid, 2)  # Login Facebook
                # account.api_facebook_friend(graph)
                account.last_active = timezone.now()
                account.facebook_access_token = access_token
                account.save()
                result['token'] = account.login_token()
                result['profile'] = account.api_profile()
        except:
            return json_render({}, 303)
    return json_render(result, 200)
