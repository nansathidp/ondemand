from django.conf import settings
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from api.v4.views import check_key
from account.api.v1.views import json_render
from account.models import Forgot
from django.utils import timezone
from account.pattern_password import ResetPassword
from account.api.v1.views_login import _login


@csrf_exempt
def view_reset_password(request):
    code = 200
    response = check_key(request)
    data = {}
    if response is not None:
        return response

    if request.method == 'POST':
        token = request.POST.get('token', None)
        _uuid = request.POST.get('uuid', None)
        passsword = request.POST.get('password', None)
        staus = request.POST.get('status', None)
        if token is None:
            code = 204
            return json_render({}, code)
        try:
            forgot = _get_forgot(token, staus)
            if not forgot.account.is_active:
                code = 313
                return json_render({}, code)
            if not forgot:
                code = 317
                return json_render({}, code)
            else:
                if not forgot.account.is_active:
                    return json_render({}, 313)
                elif settings.IS_ENABLE_ACCOUNT:
                    if forgot.account.is_enable:
                        reset_password = ResetPassword(new_password=passsword, forgot=forgot)
                        if reset_password.is_validate():
                            reset_password.save()
                            is_login = True
                            result = _login(request, forgot.account, _uuid, is_login)
                            return json_render(result, code)
                        else:
                            data['status'] = 318
                            data['result'] = {}
                            data['status_msg'] = reset_password.message_error()
                            return JsonResponse(data, json_dumps_params={'indent': 2})
                    else:
                        code = 316
                        return json_render({}, code)
                else:
                    reset_password = ResetPassword(new_password=passsword, forgot=forgot)
                    if reset_password.is_validate():
                        reset_password.save()
                        is_login = True
                        result = _login(request, forgot.account, _uuid, is_login)
                        return json_render(result, code)
                    else:
                        data['status'] = 318
                        data['result'] = {}
                        data['status_msg'] = reset_password.message_error()
                        return JsonResponse(data, json_dumps_params={'indent': 2})

        except:
            code = 318
            return json_render({}, code)
    else:

        token = request.GET.get('token', None)
        _uuid = request.GET.get('uuid', None)
        code = 201
        return json_render({}, code)


def _get_forgot(token, status):
    forgot = Forgot.objects.filter(token=token, hash_password__isnull=True, status=status,
                                   timestamp__gt=timezone.now() - timezone.timedelta(
                                       days=settings.LIMIT_AGE_SITE_FORGOT)).first()

    return forgot if forgot else None
