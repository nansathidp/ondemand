import base64
import os
import uuid

from django.views.decorators.csrf import csrf_exempt

from ondemand import settings

from account.models import Account
from ...cached import cached_account_update, cached_api_account_token_delete, cached_api_account_auth_v2
from .views import json_render


@csrf_exempt
def upload_image_view(request):
    result = {}
    code = 200
    if request.method == 'POST':
        token = request.POST.get('token', None)
        # Base64 Image
        image = request.POST.get('image', None)

        if token is not None and len(token) == 32:
            account = Account.api_auth(token, None)
            if account is None:
                code = 400
            else:
                path = '%s/media/account/image/' % settings.BASE_DIR
                if not os.path.isdir(path):
                    os.makedirs(path)

                filename = '%s.jpg' % (uuid.uuid4())
                f = '%s%s' % (path, filename)

                try:
                    image_file = open(f, 'wb')
                    if image.find('%2B') == -1:
                        image_file.write(base64.b64decode(image))
                    else:
                        image = image.replace('%2B', '+')
                        image_file.write(base64.decodestring(image))
                except:
                    code = 203

                account.image = 'account/image/%s' % filename
                account.save(update_fields=['image'])
                cached_account_update(account)
                cached_api_account_token_delete(token)
                cached_api_account_auth_v2(token, is_force=True)
                result['profile'] = account.api_profile()
                if request.POST.get('debug', 't').lower() == 't':
                    result['image'] = image
        else:
            code = 400
    else:
        code = 201
    return json_render(result, code)
