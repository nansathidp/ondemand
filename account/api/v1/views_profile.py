from django.conf import settings

from account.cached import cached_onboard_account
from .views import json_render, check_key, check_auth
from inbox.models import Inbox
from account.models import Account


def profile_view(request):
    result = {}
    response = check_key(request)
    if response is not None:
        return response

    account, code = check_auth(request, is_allow_anonymous=False)
    if account is not None:
        account_item = Account.objects.filter(id=account.id).first()
        result['profile'] = account_item.api_profile()
        if result['profile']['is_force_password']:
            token = account_item.get_token_forgot(4)
            result['token_status_password'] = 4
            if token:
                result['token_force_password'] = token
            else:
                result['token_force_password'] = account_item.forgot_password(status=4)

        result['profile_list'] = []
        result['address'] = None
        # For On-boarding
        if settings.IS_ONBOARD:
            result['is_onboarding'] = cached_onboard_account(account_item)
        else:
            result['is_onboarding'] = False

        #  Profile
        if settings.PROJECT == 'ais' and 'ais' in settings.INSTALLED_APPS:

            # AIS Account info
            from ais.models import AccountInfo
            info = AccountInfo.objects.filter(account=account_item).first()
            if info is not None:
                result['profile_list'].append({'name': 'Employee ID', 'data': info.employee})
                result['profile_list'].append({'name': 'Email', 'data': account_item.email})
                result['profile_list'].append({'name': 'Company', 'data': '-'})
                result['profile_list'].append({'name': 'Position', 'data': info.primary_position})
                result['profile_list'].append({'name': 'Hire Date', 'data': info.hire_date.strftime('%d/%m/%Y')})

        result['notification_count'] = Inbox.pull_notification_count(account_item.id).count
    else:
        code = 400
    return json_render(result, code)
