from django.views.decorators.csrf import csrf_exempt
from django import forms
from django.utils import timezone
from django.contrib.auth.hashers import make_password

from account.models import AccountManager, Account
from .views import json_render, check_key
from open_facebook import OpenFacebook
import datetime, urllib


@csrf_exempt
def register_view(request):
    result = {}
    code = 200
    response = check_key(request)
    if response is not None:
        return response

    if request.method == 'POST':
        uuid = request.POST.get('uuid', None)
        if uuid is None or len(uuid) > 0:
            return json_render(result, 202)

        email = request.POST.get('email', None)
        first_name = urllib.parse.unquote(request.POST.get('first_name', ''))
        last_name = urllib.parse.unquote(request.POST.get('last_name', ''))
        password = request.POST.get('password', None)
        birthday = request.POST.get('birthday', None)

        try:
            gender = int(request.POST.get('gender', None))
            if gender in [0, 1, 2]:
                gender = gender
            else:
                gender = 0
        except:
            gender = request.POST.get('gender', None)
            if gender == 'm' or gender == 'M':
                gender = 1
            elif gender == 'f' or gender == 'F':
                gender = 2
            else:
                gender = 0
        tel = request.POST.get('tel', '')
        email_form = forms.EmailField()
        try:
            email = urllib.parse.unquote(email)
            email = email_form.clean(email)
            email = AccountManager.normalize_email(email)
        except:
            return json_render(result, 600)
        if password is not None and len(password) > 4:
            password = urllib.parse.unquote(password)
        else:
            return json_render(result, 601)
        if Account.objects.filter(email=email).count() > 0:
            return json_render(result, 602)
        if Account.objects.filter(email='%s@gen' % uuid).count() > 0:
            account = Account.objects.get(email='%s@gen' % uuid)
            account.email = email
            account.password = make_password(password)
        else:
            account = Account.objects.create_user(email, password)

        #Update
        account.first_name = first_name
        account.last_name = last_name
        try:
            account.birthday = datetime.datetime.strptime(birthday, '%Y-%m-%d').date()
        except:
            account.birthday = None
        account.gender = gender
        account.tel = tel
        access_token = request.GET.get('access_token', None)
        is_login_facebook = False

        if access_token is not None and len(access_token) > 0:
            try:
                graph = OpenFacebook(access_token=access_token, version='v2.5')
                profile = graph.get('me', fields='id,name,email,birthday,gender')
                account.facebook_id = profile['id']
                account.api_facebook_friend()
                is_login_facebook = True
            except:
                code = 603

        if account.first_active is None:
            account.first_active = timezone.now()
        account.last_active = timezone.now()
        account.save()
        account.update_uuid(uuid, is_login_facebook=is_login_facebook)
        result['token'] = account.login_token()
        result['profile'] = account.api_profile()
    else:
        code = 201
    return json_render(result, code)
