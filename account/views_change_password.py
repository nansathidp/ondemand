from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from ondemand.views_decorator import check_project


@check_project
@login_required
def change_password_view(request):
    msg = None
    if request.method == 'POST':
        password = request.POST.get('password', '')
        p1 = request.POST.get('p1', '')
        p2 = request.POST.get('p2', '')
        if request.user.check_password(password):
            if p1 == p2:
                if len(p1) <= 4:
                    msg = 'Password must be at least 5 characters.'
                else:
                    request.user.set_password(p1)
                    request.user.save()
                    msg = 'Reset password success.'
            else:
                msg = 'Your password not match.'
        else:
            msg = 'Password incorrect.'
    return render(request,
                  'account/change_password.html',
                  {'msg': msg})
