from account.models import Account
from order.models import Item
from progress.models import Content as ProgressContent


def effect_is_active_job(account_id):
    account = Account.objects.get(id=account_id)
    if account is None:
        return None
    content_list = {}
    for item in Item.objects.filter(account=account):
        key = '%s_%s_%s' % (item.content_location_id, item.content_type_id, item.content)
        if key not in content_list:
            ProgressContent.update_progress(
                item.content_location_id,
                item.content_type_id,
                item.content
            )
            content_list[key] = True
