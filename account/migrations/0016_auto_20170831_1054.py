# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-31 03:54
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0015_auto_20170830_1802'),
    ]

    operations = [
        migrations.RenameField(
            model_name='account',
            old_name='is_disabled',
            new_name='is_enable',
        ),
    ]
