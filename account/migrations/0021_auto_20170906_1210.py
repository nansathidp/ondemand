# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-06 05:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0020_merge_20170905_1444'),
    ]

    operations = [
        migrations.AlterField(
            model_name='forgot',
            name='status',
            field=models.IntegerField(choices=[(0, 'Activate'), (0, 'Deactivate'), (2, 'Completed'), (3, 'forgot'), (4, 'force'), (5, 'first'), (6, 'expired')], default=1),
        ),
    ]
