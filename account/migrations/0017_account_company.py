# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-01 05:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0016_auto_20170831_1054'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='company',
            field=models.CharField(blank=True, max_length=120),
        ),
    ]
