# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-26 18:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0023_auto_20170914_1640'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='employee',
            field=models.CharField(blank=True, db_index=True, default=None, max_length=120, null=True),
        ),
        migrations.AlterField(
            model_name='account',
            name='is_active',
            field=models.BooleanField(db_index=True, default=True),
        ),
    ]
