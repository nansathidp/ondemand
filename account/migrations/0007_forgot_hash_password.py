# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-25 09:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0006_auto_20170825_1551'),
    ]

    operations = [
        migrations.AddField(
            model_name='forgot',
            name='hash_password',
            field=models.CharField(default=django.utils.timezone.now, max_length=120),
            preserve_default=False,
        ),
    ]
