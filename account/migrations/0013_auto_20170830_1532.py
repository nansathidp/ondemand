# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-30 08:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0012_account_is_empty'),
    ]

    operations = [
        migrations.RenameField(
            model_name='account',
            old_name='is_empty',
            new_name='is_first',
        ),
        migrations.AddField(
            model_name='account',
            name='is_force_reset_password',
            field=models.BooleanField(default=False),
        ),
    ]
