# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-05 05:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0016_auto_20170831_1054'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='account',
            name='datetime_chang_password',
        ),
        migrations.AddField(
            model_name='account',
            name='datetime_change_password',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
