from django.conf import settings


def get_ais_account_info(account):
    account_info = None
    if settings.PROJECT == 'ais':
        from ais.models import AccountInfo
        try:
            account_info = AccountInfo.objects.get(account=account)
        except AccountInfo.DoesNotExist:
            account_info = None
    return account_info


def sum_progress_item(item_list):
    progress = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0
    }
    for item in item_list:
        status = item.progress.status
        if status not in progress:
            progress[status] = 1
        else:
            progress[status] += 1
    return progress


def get_filter_list(item_list):
    category_list = []
    provider_list = []
    for item in item_list:
        if item.content_cached.category not in category_list:
            category_list.append(item.content_cached.category)
        if item.content_cached.provider not in provider_list:
            provider_list.append(item.content_cached.provider)
    return category_list, provider_list
