import pytz
import re

from django.conf import settings
from django.utils import timezone

from account.models import Forgot

STATUS_MSG = {
    111: 'number (1,2,3)',
    112: 'uppercase (A,C,D)',
    113: 'lowercase (a,b,c)',
    114: 'non alphanumeric (#,@,!,-)',
    115: 'is too short',
    116: 'is contain a part of username',
    117: 'is already used',

    200: 'reset success',
    500: 'reset error',
}


class ResetPassword():
    def __init__(self, new_password, forgot):
        self.password = new_password
        self.forgot = forgot
        self.is_status = False
        self.message = list()
        self.message_helper = ""
        self.msg1 = "Your password must contain %s-%s characters and must contain 3 out of 4 of " \
                    "the following 'number (0-9), uppercase letters (A-Z), lowercase letters (a-z)," \
                    " non alphanumeric (!,@,#,$,...)' and must not contain username in password." % \
                    (settings.MINIMUM_LENGTH_PASSWORD, settings.MAXIMUM_LENGTH_PASSWORD)
        self.msg2 = "Your password must not repeat the previous %s passwords, please choose a new password" % \
                    (settings.LIMIT_PASSWORD_USED)
        self.count = 4

    def is_strong_password(self):

        email = self.forgot.account.email.split("@")[0]
        length_regex = re.compile(r'.{%s,%s}' % (settings.MINIMUM_LENGTH_PASSWORD, settings.MAXIMUM_LENGTH_PASSWORD))
        uppercase_regex = re.compile(r'[A-Z]')
        lowercase_regex = re.compile(r'[a-z]')
        digit_regex = re.compile(r'[0-9]')
        alphabet = re.compile(r'[^a-zA-Z0-9]')

        if alphabet.search(self.password) is None:
            self.count -= 1
            self.message.append(STATUS_MSG[114])
        if digit_regex.search(self.password) is None:
            self.count -= 1
            self.message.append(STATUS_MSG[111])
        if lowercase_regex.search(self.password) is None:
            self.count -= 1
            self.message.append(STATUS_MSG[113])
        if uppercase_regex.search(self.password) is None:
            self.count -= 1
            self.message.append(STATUS_MSG[112])
        if length_regex.search(self.password) is None:
            del self.message[:]
            self.message_helper = STATUS_MSG[115]
            self.count = 0
        try:
            _ = email.lower().split('@')[0]
        except:
            _ = ''
        if _ in self.password.lower():
            del self.message[:]
            self.message_helper = STATUS_MSG[116]
            self.count = 0
        # self._make_message()
        self.message_helper = self.msg1
        return True if self.count >= settings.LEVEL_STRENGTH_PASSWORD else False

    def hash_password(self):
        import hashlib
        re_password = self.password.encode('utf-8')
        return hashlib.sha224(re_password).hexdigest()

    def is_validate(self):
        if self.is_strong_password():
            self.is_status = True
            _forgot = Forgot.objects.filter(account=self.forgot.account).order_by('-id')[:settings.LIMIT_PASSWORD_USED]
            for _ in _forgot:
                if _.hash_password == self.hash_password():
                    self.is_status = False
                    # self.message_helper = STATUS_MSG[117]
                    self.message_helper = self.msg2
                    self.count = 0
                    del self.message[:]
                    break
        # self._make_message()
        return self.is_status

    # def _make_message(self):
    #     # 0
    #     if self.count == 0:
    #         self.message_helper = 'Your password ' + self.message_helper
    #     else:
    #         self.message_helper = 'Your password is not allow. Please check pattern to create password such '
    #         if self.count <= 2:
    #             for _ in range(len(self.message)):
    #                 if _ == len(self.message) - 1:
    #                     temp = ' and ' + self.message[_]
    #                 else:
    #                     temp = ' ' + self.message[_]
    #                 self.message_helper += temp
    #         else:
    #             if self.message:
    #                 self.message_helper = ' '+self.message.pop()

    def message_error(self):
        return self.message_helper + "."

    def get_status(self):
        return self.is_status

    def save(self):
        if self.is_status:

            time_zone = timezone.localtime(timezone.now(), pytz.timezone('Asia/Bangkok'))

            self.forgot.account.set_password(self.password)
            self.forgot.account.datetime_change_password = time_zone
            self.forgot.account.is_force_reset_password = True
            self.forgot.account.is_first = True
            self.forgot.account.save()

            self.forgot.status = 2
            self.forgot.hash_password = self.hash_password()
            self.forgot.save()
            self.message_helper = STATUS_MSG[200]
        else:
            self.forgot.status = 0
            self.forgot.save()
            self.message_helper = STATUS_MSG[500]
