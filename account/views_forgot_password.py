from django.shortcuts import render, redirect
from django.conf import settings
from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.csrf import csrf_exempt
from account.models import Account
from mailer.models import Mailer


def forgot_password_view(request):
    msg = None
    is_show = True

    # Fix VA Scan
    if len(request.GET) > 0:
        return redirect('forgot_password')

    return render(request,
                  'account/forgot_password.html',
                  {'msg': msg,
                   'is_show': is_show})


@require_POST
@csrf_exempt
def save_password_view(request):
    from account.models import Forgot
    is_show = False
    msg = ""
    email = request.POST.get('email', None)
    if email is not None:
        try:
            status = 3
            account = Account.objects.get(email=email)

            if not account.is_active:
                return render(request,
                              'account/forgot_password.html',
                              {'msg': 'Your account is inactive.',
                               'is_show': False})

            # Close token
            forgots = Forgot.objects.filter(account=account, status=status)
            if forgots:
                for forgot in forgots:
                    forgot.status = 0
                    forgot.save()

            token = account.forgot_password(status)
            sender_email = settings.EMAIL_HOST_USER
            Mailer.send_forget_password_web(account, sender_email, token, status)
            msg = 'Please check your email inbox for a link to reset your password.'
            is_show = False
        except:
            msg = 'Email not found.'
    return render(request,
                  'account/forgot_password.html',
                  {'msg': msg,
                   'is_show': is_show})
