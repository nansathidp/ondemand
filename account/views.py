def item_filter(item_list, progress=-1):
    _item_list = []
    for item in item_list:
        if progress is -1:
            _item_list.append(item)  # No filter
        else:
            if item.progress.status == progress:
                _item_list.append(item)
    return _item_list
