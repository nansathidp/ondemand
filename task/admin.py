from django.contrib import admin

from .models import Task, Approve

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'sort', 'is_display', 'timestamp')

@admin.register(Approve)
class ApproveAdmin(admin.ModelAdmin):
    list_display = ('task', 'program', 'account', 'is_approve', 'timestamp', 'timestamp_approve')
    
