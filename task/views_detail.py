from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.conf import settings

from task.models import Task
from progress.models import Progress
from content.models import Location as ContentLocation
from order.models import Item as OrderItem

@login_required
def detail_view(request, task_id, order_item_id=None):
    task = Task.pull(task_id)
    if task is None:
        raise Http404

    is_approved = False
    parent_url = None
    if order_item_id is not None:
        order_item = OrderItem.pull(order_item_id)
        if order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
            parent_url = reverse('program:detail', args=[order_item.content, order_item.id])

        if order_item.content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
            content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('task', 'task'),
                                                          task.id,
                                                          parent1_content_type=settings.CONTENT_TYPE('program', 'onboard'),
                                                          parent1_content=order_item.content)
        elif order_item.content_type_id == settings.CONTENT_TYPE('program', 'program').id:
            content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('task', 'task'),
                                                          task.id,
                                                          parent1_content_type=settings.CONTENT_TYPE('program', 'program'),
                                                          parent1_content=order_item.content)
        progress = Progress.pull(order_item,
                                 content_location,
                                 settings.CONTENT_TYPE('task', 'task'),
                                 task_id)
        if progress is not None:
            is_approved = True if progress.status == 2 else False

    return render(request,
                  'task/detail.html',
                  {
                      'PARENT_URL': parent_url,
                      'task': task,
                      'is_approved': is_approved
                  })
