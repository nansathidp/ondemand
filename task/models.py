from django.db import models


class Task(models.Model):
    name = models.CharField(max_length=120)
    provider = models.ForeignKey('provider.Provider', null=True, on_delete=models.SET_NULL)
    intro = models.CharField(max_length=320, blank=True)
    desc = models.TextField(blank=True)
    image = models.ImageField(upload_to='task/images/%Y/%m', blank=True)
    sort = models.IntegerField(db_index=True, default=0)
    is_display = models.BooleanField(default=True, db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        default_permissions = ['view', 'view_own', 'view_org', 'add', 'change', 'delete']
        ordering = ['sort']

    def __str__(self):
        return str(self.id)

    @staticmethod
    def pull(id):
        from .cached import cached_task
        return cached_task(id)

    @staticmethod
    def push_approve(order_item, task_id, program_id, account_id):
        if not Approve.objects.filter(item=order_item,
                                      task_id=task_id,
                                      program_id=program_id,
                                      account_id=account_id).exists():
            Approve.objects.create(item=order_item,
                                   task_id=task_id,
                                   program_id=program_id,
                                   account_id=account_id)

    @staticmethod
    def access_list(request):
        from django.conf import settings
        from provider.models import Provider
        from django.core.exceptions import PermissionDenied

        from utils.filter import get_q_dashboard

        request.q_provider_list = Provider.pull_list()
        request.q_category_list = None
        request.q_provider = get_q_dashboard(request, 'provider', 'int')

        if request.user.has_perm('task.view_task',
                                 group=request.DASHBOARD_GROUP):
            content_list = Task.objects.select_related('provider').all()
            if request.q_provider:
                content_list = content_list.filter(provider_id=request.q_provider)
        elif request.user.has_perm('task.view_own_task',
                                   group=request.DASHBOARD_GROUP):
            request.q_provider_list = None
            if request.DASHBOARD_PROVIDER is not None:
                content_list = Task.objects.select_related('provider').filter(provider=request.DASHBOARD_PROVIDER)
            elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                from subprovider.models import Item
                content_list = Task.objects.select_related('provider').filter(id__in=Item.objects
                                                                              .values_list('content', flat=True)
                                                                              .filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                                                                      content_type=settings.CONTENT_TYPE('task', 'task')))
            else:
                content_list = Task.objects.none()
        elif request.user.has_perm('task.view_org_task',
                                   group=request.DASHBOARD_GROUP):
            content_list = Task.objects.select_related('provider').all()
        else:
            # content_list = Task.objects.none()
            raise PermissionDenied

        request.q_name = " ".join(get_q_dashboard(request, 'name', 'str').split())
        if request.q_name != '':
            content_list = content_list.filter(name__icontains=request.q_name)
        return content_list

    def check_access(self, request):
        from django.conf import settings
        from provider.models import Provider
        if request.user.has_perm('task.view_task',
                                 group=request.DASHBOARD_GROUP):
            return True
        elif request.user.has_perm('task.view_own_task',
                                   group=request.DASHBOARD_GROUP):
            if self.provider.account_set.filter(account=request.user).exists():
                return True
            if settings.IS_SUB_PROVIDER:
                from subprovider.models import Item
                if request.DASHBOARD_SUB_PROVIDER is None:
                    return False
                else:
                    return Item.objects.filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                               content_type=settings.CONTENT_TYPE('task', 'task'),
                                               content=self.id).exists()
            else:
                return False
        elif request.user.has_perm('task.view_org_task',
                                   group=request.DASHBOARD_GROUP):
            return True
        else:
            return False

    def check_change(self, request):
        from django.conf import settings
        from provider.models import Provider
        if request.user.has_perm('task.change_task',
                                 group=request.DASHBOARD_GROUP):
            if request.user.has_perm('task.view_task',
                                     group=request.DASHBOARD_GROUP):
                return True
            elif request.user.has_perm('task.view_own_task',
                                       group=request.DASHBOARD_GROUP):
                if self.provider.account_set.filter(account=request.user).exists():
                    return True
                if settings.IS_SUB_PROVIDER:
                    from subprovider.models import Item
                    if request.DASHBOARD_SUB_PROVIDER is None:
                        return False
                    else:
                        return Item.objects.filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                                   content_type=settings.CONTENT_TYPE('task', 'task'),
                                                   content=self.id).exists()
                else:
                    return False
            else:
                return False
        else:
            return False

    def api_display(self):
        result = {'id': self.id,
                  'name': self.name,
                  'intro': self.intro,
                  'image': self.image.url if bool(self.image) else None,
                  'desc': self.desc}
        return result

    def progress_count(self):
        result = {'content_count': 1,
                  'section_count': 1,
                  'material_count': 1}
        return result

    def is_approved(self, program_id, account_id):
        return Approve.objects.filter(task_id=self.id,
                                      program_id=program_id,
                                      account_id=account_id).exists()


class Approve(models.Model):
    from django.conf import settings

    task = models.ForeignKey(Task)
    item = models.ForeignKey('order.Item')
    program = models.ForeignKey('program.Program')
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    is_approve = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    timestamp_approve = models.DateTimeField(null=True)

    def push_approve(self):
        from django.conf import settings
        from django.utils import timezone

        from order.models import Item
        from progress.models import Progress
        from content.models import Location as ContentLocation

        from program.cached import cached_program_item_content

        self.is_approve = not self.is_approve
        if self.is_approve:
            self.timestamp_approve = timezone.now()
        else:
            self.timestamp_approve = None
        self.save(update_fields=['is_approve', 'timestamp_approve'])
        Stat.update_stat(self.task_id, self.program_id)

        if self.program.type == 2:
            content_type = settings.CONTENT_TYPE('program', 'onboard')
        else:
            content_type = settings.CONTENT_TYPE('program', 'program')
        _content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('task', 'task'),
                                                       self.task_id,
                                                       parent1_content_type=content_type,
                                                       parent1_content=self.program_id)
        progress = Progress.pull(self.item,
                                 _content_location,
                                 settings.CONTENT_TYPE('task', 'task'),
                                 self.task_id)
        if progress is not None:
            progress.check_complete_task(self)


class Stat(models.Model):
    task = models.ForeignKey(Task)
    program = models.ForeignKey('program.Program')
    count = models.IntegerField(default=0)
    approve = models.IntegerField(default=0)

    @property
    def not_approve(self):
        return self.count - self.approve

    @staticmethod
    def pull(task_id, program_id):
        stat = Stat.objects.filter(task_id=task_id,
                                   program_id=program_id).first()
        if stat is None:
            stat = Stat.objects.create(task_id=task_id,
                                       program_id=program_id)
            Stat.update_stat(task_id, program_id, stat=stat)
        return stat

    @staticmethod
    def update_stat(task_id, program_id, stat=None):
        stat = Stat.pull(task_id, program_id)
        stat.count = Approve.objects.filter(task_id=task_id, program_id=program_id).count()
        stat.approve = Approve.objects.filter(task_id=task_id, program_id=program_id, is_approve=True).count()
        stat.save(update_fields=['count', 'approve'])
