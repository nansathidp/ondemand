from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from dashboard.views import paginator

from ..models import Task
from dependency.models import Dependency, Parent as DependencyParent


@csrf_exempt
def dependency_search_view(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    if not task.check_access(request):
        raise PermissionDenied

    q_name = None
    type = None
    content_type = settings.CONTENT_TYPE('task', 'task')
    dependency = Dependency.pull(content_type, task.id)
    parent_list = Dependency.pull_parent(content_type,
                                         task.id)
    if request.method == 'POST':
        type = request.POST.get('type')
        q_name = request.POST.get('q_name', '')
        content_type, content_list = Dependency.get_content_list(type)
        if content_list is None:
            content_list = []
        else:
            if q_name:
                content_list = content_list.filter(name__contains=q_name)
            content_list = paginator(request, content_list)
    else:
        content_list = []
    return render(request,
                  'task/dashboard/dependency_search.html',
                  {'content_list': content_list,
                   'condition_choices': DependencyParent.CONDITION_CHOICES,
                   'task': task,
                   'q_name': q_name,
                   'type': type,
                   'parent_list': parent_list,
                   'content_type': content_type})
