from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from ..models import Task
from dependency.models import Dependency

@csrf_exempt
def dependency_delete_view(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    if not task.check_change(request):
        raise PermissionDenied

    content_type = settings.CONTENT_TYPE('task', 'task')
    dependency = Dependency.pull(content_type,
                                 task.id)
    if request.method == 'POST':
        try:
            parent_id = int(request.POST.get('parent'))
            dependency.delete_parent(parent_id)
        except:
            pass

    parent_list = Dependency.pull_parent(content_type, task.id)
    return render(request,
                  'task/dashboard/dependency_block.html',
                  {'task': task,
                   'parent_list': parent_list})
