from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.conf import settings

from dependency.models import Dependency
from ..models import Task

def dependency_view(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    if not task.check_access(request):
        raise PermissionDenied

    parent_list = Dependency.pull_parent(settings.CONTENT_TYPE('task', 'task'),
                                         task.id)
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'TO-DO Management',
         'url': reverse('dashboard:task-dashboard:home')},
        {'is_active': True,
         'title': 'Dependency: %s'%task.name},
    ]
    return render(request,
                  'task/dashboard/dependency.html',
                  {'SIDEBAR': 'task',
                   'TAB': 'dependency',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'task': task,
                   'parent_list': parent_list})
