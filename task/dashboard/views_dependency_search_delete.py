from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.conf import settings

from dependency.models import Dependency
from ..models import Task

@csrf_exempt
def dependency_search_delete_view(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    if not task.check_change(request):
        raise PermissionDenied
    
    content_type = settings.CONTENT_TYPE('task', 'task')
    dependency = Dependency.pull(content_type,
                                 task.id)

    html = ''
    if request.method == 'POST':
        try:
            parent_id = int(request.POST.get('parent'))

            parent = dependency.delete_parent(parent_id)
            if parent is None:
                html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-red"></i>'
            else:
                html = parent.get_status_html()
        except:
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-red"></i>'   
        
    return JsonResponse({'html': html})
