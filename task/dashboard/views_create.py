from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.conf import settings

from .forms import TaskForm

from utils.crop import crop_image

def create_view(request):
    if request.method == 'POST':
        if request.user.has_perm('task.view_task',
                                 group=request.DASHBOARD_GROUP):
            task_form = TaskForm(None, request.POST, request.FILES)
        elif request.user.has_perm('task.view_own_task',
                                   group=request.DASHBOARD_GROUP):
            task_form = TaskForm(request, request.POST, request.FILES)
        else:
            raise PermissionDenied

        if task_form.is_valid():
            task = task_form.save()
            crop_image(task, request)
            if settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
                request.DASHBOARD_SUB_PROVIDER.push_item(settings.CONTENT_TYPE('task', 'task'),
                                                         task.id)
            return redirect('dashboard:task-dashboard:detail', task.id)
    else:
        if request.user.has_perm('task.view_task',
                                 group=request.DASHBOARD_GROUP):
            task_form = TaskForm(None)
        elif request.user.has_perm('task.view_own_task',
                                   group=request.DASHBOARD_GROUP):
            task_form = TaskForm(request)
        else:
            raise PermissionDenied

    BREADCRUMB_LIST = [
        {'is_active': False,
         'url': reverse('dashboard:task-dashboard:home'),
         'title': 'TO-DO Management'},
        {'is_active': True,
         'title': 'Create'}
    ]
    return render(request,
                  'task/dashboard/create.html',
                  {'SIDEBAR': 'task',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'task_form': task_form})
