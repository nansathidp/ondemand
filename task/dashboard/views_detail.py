from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ..models import Task
from .forms import TaskForm
from ..cached import cached_task_update

from utils.crop import crop_image

def detail_view(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    if not task.check_access(request):
        raise PermissionDenied

    is_change = task.check_change(request)

    if request.method == 'POST':
        if not is_change:
            raise PermissionDenied

        if request.user.has_perm('task.view_task',
                                 group=request.DASHBOARD_GROUP):
            task_form = TaskForm(None, request.POST, request.FILES, instance=task)
        elif request.user.has_perm('task.view_own_task',
                                   group=request.DASHBOARD_GROUP):
            task_form = TaskForm(request, request.POST, request.FILES, instance=task)
        else:
            raise PermissionDenied

        if task_form.is_valid():
            task = task_form.save()
            crop_image(task, request)
            cached_task_update(task)

    if request.user.has_perm('task.view_task',
                             group=request.DASHBOARD_GROUP):
        task_form = TaskForm(None, instance=task)
    elif request.user.has_perm('task.view_own_task',
                               group=request.DASHBOARD_GROUP):
        task_form = TaskForm(request, instance=task)
    elif request.user.has_perm('task.view_org_task',
                               group=request.DASHBOARD_GROUP):
        task_form = TaskForm(None, instance=task)
    else:
        raise PermissionDenied

    BREADCRUMB_LIST = [
        {'is_active': False,
         'url': reverse('dashboard:task-dashboard:home'),
         'title': 'TO-DO Management'},
        {'is_active': True,
         'title': 'Task: %s'%task.name}
    ]
    return render(request,
                  'task/dashboard/detail.html',
                  {'SIDEBAR': 'task',
                   'TAB': 'information',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'task': task,
                   'task_form': task_form,
                   'is_change': is_change})
