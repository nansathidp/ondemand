from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.conf import settings

from ..models import Task, Stat
from program.models import Program

from dashboard.views import paginator

from ..cached import cached_task_count_program
from analytic.cached import cached_analytic_stat

def _task_approve_program_access_list(request, task):
    content_type = settings.CONTENT_TYPE('task', 'task')
    if request.user.has_perm('task.view_task',
                             group=request.DASHBOARD_GROUP):
        return Program.objects.filter(item__content_type=content_type,
                                      item__content=task.id)
    elif request.user.has_perm('task.view_own_task',
                               group=request.DASHBOARD_GROUP):
        if request.DASHBOARD_PROVIDER is not None:
            return Program.objects.filter(provider=request.DASHBOARD_PROVIDER,
                                          item__content_type=content_type,
                                          item__content=task.id)
        elif settings.IS_SUB_PROVIDER and request.DASHBOARD_SUB_PROVIDER is not None:
            from subprovider.models import Item
            return Program.objects.filter(item__content_type=content_type,
                                          item__content=task.id,
                                          id__in=Item.objects
                                          .values_list('content', flat=True)
                                          .filter(sub_provider=request.DASHBOARD_SUB_PROVIDER,
                                                  content_type=settings.CONTENT_TYPE('program', 'program')))
        else:
            return []
    elif request.user.has_perm('task.view_org_task',
                               group=request.DASHBOARD_GROUP):
        return Program.objects.filter(item__content_type=content_type,
                                      item__content=task.id)
    else:
        return None

def approve_view(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    if not task.check_access(request):
        raise PermissionDenied

    program_list = _task_approve_program_access_list(request, task)
    if program_list is None:
        raise PermissionDenied

    program_list = paginator(request, program_list)

    for program in program_list:
        program.stat = Stat.pull(task.id, program.id)
        Stat.update_stat(task.id, program.id)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'url': reverse('dashboard:task-dashboard:home'),
         'title': 'TO-DO Management'},
        {'is_active': False,
         'url': reverse('dashboard:task-dashboard:detail', args=[task.id]),
         'title': 'Detail: %s'%task.name},
        {'is_active': True,
         'title': 'Approvement'}
    ]
    return render(request,
                  'task/dashboard/approve.html',
                  {'SIDEBAR': 'task',
                   'TAB': 'approve',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'task': task,
                   'program_list': program_list})
