from django import forms

from ..models import Task
from provider.models import Provider

class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['name', 'intro', 'image', 'desc', 'provider', 'is_display']
        labels = {
            'intro': 'Introduction',
            'desc': 'Description',
            'provider': 'Learning Center',
            'is_display': 'Display'
        }
        help_texts = {
            'name': 'Limit at 120 characters',
            'image': 'Limit file size at 100 kb.',
            'desc': 'Limit at 250 characters'
        }

    def __init__(self, request=None, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        if request is not None:
            self.fields['provider'].queryset = Provider.access(request)
