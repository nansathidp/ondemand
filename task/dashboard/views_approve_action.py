from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from ..models import Task, Approve
if settings.IS_ORGANIZATION:
    from organization.models import Parent

from dashboard.views import config_view

@csrf_exempt
def approve_action_view(request, task_id, program_id):
    task = get_object_or_404(Task, id=task_id)
    if not task.check_access(request):
        raise PermissionDenied

    if request.method == 'POST':
        approve = get_object_or_404(Approve, id=int(request.POST.get('approve', -1)), task_id=task_id, program_id=program_id)
        if request.user.has_perm('task.view_task',
                                 group=request.DASHBOARD_GROUP):
            pass
        elif request.user.has_perm('task.view_own_task',
                                   group=request.DASHBOARD_GROUP):
            pass
        elif request.user.has_perm('task.view_org_task',
                                   group=request.DASHBOARD_GROUP):
            if settings.IS_ORGANIZATION:
                if not Approve.objects.filter(account__in=Parent.objects
                                              .values_list('account_id', flat=True)
                                              .filter(parent=request.user)):
                    raise PermissionDenied
            else:
                return config_view(request)
        else:
            raise PermissionDenied
        approve.push_approve()
    else:
        approve = None
    return render(request,
                  'task/dashboard/approve_block.html',
                  {'approve': approve})
