from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.shortcuts import render

from dashboard.views import condition_view
from dashboard.views_decorator import filter_param
from provider.models import Provider
from utils.filter import get_q
from utils.paginator import paginator
from ..cached import cached_task_count_program
from ..models import Task


@filter_param
def home_view(request):
    task_list = Task.access_list(request)
    q_category = get_q(request, 'category')
    sort_by = get_q(request, 'sort_by', type='str')
    q_provider = get_q(request, 'provider')
    q_name = get_q(request, 'q_name', type='str')
    q_provider_list = Provider.objects.filter().all()

    if task_list is None:
        raise PermissionDenied
    elif task_list == []:
        return condition_view(request)

    task_list = paginator(request, task_list)
    content_type = settings.CONTENT_TYPE('task', 'task')
    for task in task_list:
        task.count_program = cached_task_count_program(task.id)

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'TO-DO Management'}
    ]
    return render(request,
                  'task/dashboard/home.html',
                  {'SIDEBAR': 'task',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'task_list': task_list,
                   'q_name': request.q_name,
                   'q_provider': request.q_provider,
                   'q_provider_list': request.q_provider_list,
                   'q_category_list': request.q_category_list})
