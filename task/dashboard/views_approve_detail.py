from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404

from dashboard.views import config_view
from dashboard.views import paginator
from program.models import Program
from ..models import Task

if settings.IS_ORGANIZATION:
    from organization.models import Parent


def approve_detail_view(request, task_id, program_id):
    task = get_object_or_404(Task, id=task_id)
    if not task.check_access(request):
        raise PermissionDenied

    program = get_object_or_404(Program, id=program_id)

    if request.user.has_perm('task.view_task',
                             group=request.DASHBOARD_GROUP):
        approve_list = program.approve_set.filter(task=task)
    elif request.user.has_perm('task.view_own_task',
                               group=request.DASHBOARD_GROUP):
        approve_list = program.approve_set.filter(task=task)
    elif request.user.has_perm('task.view_org_task',
                               group=request.DASHBOARD_GROUP):
        if settings.IS_ORGANIZATION:
            approve_list = program.approve_set.filter(task=task,
                                                      account__in=Parent.objects
                                                      .values_list('account_id', flat=True)
                                                      .filter(parent=request.user))
        else:
            return config_view(request)
    else:
        approve_list = program.approve_set.filter(task=task)
    if settings.ACCOUNT__HIDE_INACTIVE:
        approve_list = approve_list.exclude(account__is_active=False)
    if settings.ACCOUNT__HIDE_ID_LIST:
        approve_list = approve_list.exclude(account_id__in=settings.ACCOUNT__HIDE_ID_LIST)
    approve_list = paginator(request, approve_list)
    BREADCRUMB_LIST = [
        {'is_active': False,
         'url': reverse('dashboard:task-dashboard:home'),
         'title': 'TO-DO Management'},
        {'is_active': False,
         'url': reverse('dashboard:task-dashboard:detail', args=[task.id]),
         'title': 'Detail: %s' % task.name},
        {'is_active': True,
         'title': 'Approvement'}
    ]
    return render(request,
                  'task/dashboard/approve_detail.html',
                  {
                      'SIDEBAR': 'task',
                      'TAB': 'approve',
                      'BREADCRUMB_LIST': BREADCRUMB_LIST,
                      'task': task,
                      'program': program,
                      'approve_list': approve_list
                  })
