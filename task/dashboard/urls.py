from django.conf.urls import url

from .views import home_view
from .views_create import create_view
from .views_detail import detail_view

from .views_approve import approve_view
from .views_approve_detail import approve_detail_view
from .views_approve_action import approve_action_view

from .views_dependency import dependency_view
from .views_dependency_search import dependency_search_view
from .views_dependency_search_add import dependency_search_add_view
from .views_dependency_search_delete import dependency_search_delete_view
from .views_dependency_delete import dependency_delete_view

app_name = 'task-dashboard'
urlpatterns = [
    url(r'^$', home_view, name='home'), #TODO: testing
    url(r'^create/$', create_view, name='create'), #TODO: testing
    url(r'^(\d+)/$', detail_view, name='detail'), #TODO: testing
    
    url(r'^(\d+)/approve/$', approve_view, name='approve'), #TODO: testing
    url(r'^(\d+)/approve/(\d+)/$', approve_detail_view, name='approve_detail'), #TODO: testing
    url(r'^(\d+)/approve/(\d+)/action/$', approve_action_view, name='approve_action'), #TODO: testing
    
    #Dependency
    url(r'^(\d+)/dependency/$', dependency_view, name='dependency'),
    url(r'^(\d+)/dependency/search/$', dependency_search_view, name='dependency_search'),
    url(r'^(\d+)/dependency/search/add/$', dependency_search_add_view, name='dependency_search_add'),
    url(r'^(\d+)/dependency/search/delete/$', dependency_search_delete_view, name='dependency_search_delete'),
    url(r'^(\d+)/dependency/delete/$', dependency_delete_view, name='dependency_delete'),
]
