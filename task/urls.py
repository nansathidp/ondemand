from django.conf.urls import url

from .views_detail import detail_view

app_name = 'task'
urlpatterns = [
    url(r'^(\d+)/$', detail_view, name='detail'), #TODO: testing
    url(r'^(\d+)/(\d+)/$', detail_view, name='detail_item'), #TODO: testing
]
