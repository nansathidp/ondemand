from django.conf import settings

from task.models import Task
from order.models import Item as OrderItem
from progress.models import Progress
from content.models import Location as ContentLocation

from account.cached import cached_api_account_auth
from .views import check_key, check_require, json_render


def detail_view(request, task_id, order_item_id):
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render({}, code)

    account = cached_api_account_auth(token, uuid)
    if account is None:
        return json_render({}, 400)

    task = Task.pull(task_id)
    if task is None:
        return json_render({}, 4100)

    result = task.api_display()

    order_item = OrderItem.pull(order_item_id)
    if order_item is None:
        return json_render({}, 720)
    content_location = ContentLocation.pull_first(
        settings.CONTENT_TYPE('task', 'task'),
        task.id,
        parent1_content_type=settings.CONTENT_TYPE_ID(order_item.content_type_id),
        parent1_content=order_item.content
    )
    progress = Progress.pull(
        order_item,
        content_location,
        settings.CONTENT_TYPE('task', 'task'),
        task_id
    )
    if progress is not None:
        is_approved = True if progress.status == 2 else False
    else:
        is_approved = False
    result.update({'is_approved': is_approved})

    return json_render(result, code)
