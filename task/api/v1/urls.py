from django.conf.urls import url

from .views_detail import detail_view

urlpatterns = [
    url(r'^(\d+)/(\d+)/$', detail_view),
]
