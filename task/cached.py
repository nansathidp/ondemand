from django.conf import settings
from django.core.cache import cache

from .models import Task
from program.models import Item as ProgramItem

from utils.cached.time_out import get_time_out


def cached_task(task_id, is_force=False):
    key = '%s_task_%s' % (settings.CACHED_PREFIX, task_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Task.objects.get(id=task_id)
        except Task.DoesNotExist:
            result = -1
        cache.set(key, result, get_time_out())
    return None if result == -1 else result


def cached_task_update(task):
    key = '%s_task_%s' % (settings.CACHED_PREFIX, task.id)
    cache.set(key, task, get_time_out())


def cached_task_count_program(task_id, is_force=False):
    key = '%s_task_count_program_%s' % (settings.CACHED_PREFIX, task_id)
    result = None if is_force else cache.get(key)
    if result is None:
        content_type = settings.CONTENT_TYPE('task', 'task')
        result = ProgramItem.objects.filter(content_type=content_type, content=task_id).count()
        cache.set(key, result, get_time_out())
    return result
