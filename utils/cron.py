import os

def check_pid(path):
    pid = os.getpid()
    pid_now = None
    if os.path.exists(path):
        pid_now = open(path, 'r').read()
        try:
            pid_now = int(pid_now)
        except:
            f = open(path, 'w')
            f.write(str(pid))
            f.close()
            return False
    else:
        f = open(path, 'w')
        f.write(str(pid))
        f.close()
        return False

    try:
        os.kill(pid_now, 0)
    except OSError:
        f = open(path, 'w')
        f.write(str(pid))
        f.close()
        return False
    else:
        return True
