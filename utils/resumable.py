from django.core.files.storage import FileSystemStorage

import fnmatch
import hashlib
CHUNK_SUFFIX = '_chunk_'

def _get_md5(_):
    return str(hashlib.md5(_.encode('utf-8')).hexdigest())

class Resumable(object):
    def __init__(self, chunks_dir, _filename, kwargs):
        self.storage = FileSystemStorage(location=chunks_dir)
        self._filename = _get_md5(_filename)
        self.kwargs = kwargs

    @property
    def chunkname(self):
        return '%s%s%s'%(self._filename,
                         CHUNK_SUFFIX,
                         self.kwargs.get('resumableChunkNumber').zfill(8))
        
    @property
    def filename(self):
        filename = self.kwargs.get('resumableFilename')
        if '/' in filename:
            raise Exception('Invalid filename')
        return '%s_%s'%(self.kwargs.get('resumableTotalSize'), self._filename)

    @property
    def size(self):
        size = 0
        for chunk in self.chunk_names():
            size += self.storage.size(chunk)
        return size
    
    @property
    def is_complete(self):
        try:
            if self.storage.exists(self.filename):
                return True
            return self.size == int(self.kwargs.get('resumableTotalSize'))
        except:
            return False

    @property
    def chunk_exists(self):
        try:
            if not self.storage.exists(self.chunkname):
                return False
            return self.storage.size(self.chunkname) == int(self.kwargs.get('resumableCurrentChunkSize'))
        except:
            return False

    def chunk_names(self):
        file_list = sorted(self.storage.listdir('')[1])
        pattern = '%s%s*'%(self._filename, CHUNK_SUFFIX)
        for name in file_list:
            if fnmatch.fnmatch(name, pattern):
                yield name

    def chunks(self):
        for name in self.chunk_names():
            yield self.storage.open(name).read()

    def process_chunk(self, file):
        if not self.chunk_exists:
            self.storage.save(self.chunkname, file)

    def delete_chunks(self):
        for chunk in self.chunk_names():
            self.storage.delete(chunk)
