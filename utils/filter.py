from django.utils import timezone
import datetime


def get_q_dashboard(request, code, type):
    _ = request.GET.get(code, None)
    if type == 'int':
        try:
            _ = int(_)
            if _ == -1:
                _ = None
        except:
            _ = None
    elif type == 'str':
        if _ is None:
            _ = ''
    return _


def get_q(request, code, is_get=False, default=None, type='int'):
    result = request.session.get(code, -1)
    if is_get:
        tmp = request.GET.get(code, None)
        if tmp is not None:
            request.session[code] = result = tmp
        elif result == -1:
            request.session[code] = result = default
    elif request.method == 'POST':
        tmp = request.POST.get(code, None)
        if tmp is not None:
            request.session[code] = result = tmp
        elif result == -1:
            request.session[code] = result = default
    if type == 'int':
        try:
            result = int(result)
        except:
            result = -1
    elif type == 'date':
        try:
            tmp = result.split('/')
            if code == 'end':
                result = timezone.make_aware(datetime.datetime(int(tmp[2]), int(tmp[1]), int(tmp[0]), 23, 59, 59))
            else:
                result = timezone.make_aware(datetime.datetime(int(tmp[2]), int(tmp[1]), int(tmp[0]), 0, 0, 0))
        except:
            result = default
    else:
        pass
    return result

