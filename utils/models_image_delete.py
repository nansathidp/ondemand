import os

def image_delete_pre_save(self):
    try:
        i = self.__class__.objects.get(id=self.id)
    except:
        i = None
    else:
        for field in i._meta.fields:
            if field.get_internal_type() == 'FileField' or field.get_internal_type() == 'ImageField':
                if getattr(i, field.name) != getattr(self, field.name):
                    try:
                        path = getattr(i, field.name).path
                        if os.path.isfile(path):
                            os.remove(path)
                    except:
                        pass

def image_delete_pre_delete(self):
    for field in self._meta.fields:
        if field.get_internal_type() == 'FileField' or field.get_internal_type() == 'ImageField':
            try:
                path = getattr(self, field.name).path
                if os.path.isfile(path):
                    os.remove(path)
            except:
                pass
                    
