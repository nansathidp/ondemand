from program.models import Program


def get_content_type_name(content_type, id=None, code=None, content=None):
    if content_type.app_label == 'question':
        return 'test'
    elif content_type.app_label == 'task':
        return 'to-do'
    elif content_type.app_label == 'program' and content_type.model == 'milestone':
        return 'Milestone'
    elif content_type.app_label == 'program' and content is not None:
        if content.type == 2:
            return 'onboard'
        else:
            return 'program'
    elif content_type.app_label == 'program' and id is not None:
        program = Program.pull(id)
        if program.type == 2:
            return 'onboard'
        else:
            return 'program'
    elif content_type.app_label == 'program' and code is not None:
        return code
    elif content_type.app_label == 'course' and content_type.model == 'material':
        return 'Learning Material'
    else:
        return content_type.app_label
