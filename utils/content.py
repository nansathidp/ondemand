from django.conf import settings
from django.urls import reverse
from course.models import Course, Material
from question.models import Activity
from program.models import Program, Milestone
from featured.models import Highlight
from live.models import Live
from task.models import Task


def get_content(content_type_id, content_id):
    if content_type_id == settings.CONTENT_TYPE('course', 'course').id:
        return Course.pull(content_id)
    elif content_type_id == settings.CONTENT_TYPE('course', 'material').id:
        return Material.pull(content_id)
    elif content_type_id == settings.CONTENT_TYPE('question', 'activity').id:
        return Activity.pull(content_id)
    elif content_type_id == settings.CONTENT_TYPE('program', 'program').id:
        return Program.pull(content_id)
    elif content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
        return Program.pull(content_id)
    elif content_type_id == settings.CONTENT_TYPE('program', 'milestone').id:
        return Milestone.pull(content_id)
    elif content_type_id == settings.CONTENT_TYPE('featured', 'highlight').id:
        return Highlight.pull(content_id)
    elif content_type_id == settings.CONTENT_TYPE('live', 'live').id:
        return Live.pull(content_id)
    elif content_type_id == settings.CONTENT_TYPE('task', 'task').id:
        return Task.pull(content_id)
    else:
        return None


def get_content_site(content_type_id, content_id):
    if content_type_id == settings.CONTENT_TYPE('course', 'course').id:
        return settings.SITE_URL + reverse('course:detail', args=[content_id])
    elif content_type_id == settings.CONTENT_TYPE('question', 'activity').id:
        return settings.SITE_URL + reverse('question:detail', args=[content_id])
    elif content_type_id == settings.CONTENT_TYPE('program', 'program').id:
        return settings.SITE_URL + reverse('program:detail', args=[content_id])
    elif content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
        return settings.SITE_URL + reverse('program:onboard')
    else:
        return None


def get_content_include(content_type_id, content_id):
    if content_type_id == settings.CONTENT_TYPE('course', 'course').id:
        content = get_content(content_type_id, content_id)
        try:
            return content.material_set.filter(type=3).count()  # TODO : cached?
        except:
            return 0
    elif content_type_id == settings.CONTENT_TYPE('program', 'program').id:
        content = get_content(content_type_id, content_id)
        return content.item_set.count()  # TODO : cached?
    else:
        return '-'
