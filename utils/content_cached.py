from django.conf import settings
from django.core.urlresolvers import reverse

from question.models import Activity

from program.cached import cached_program, cached_program_milestone
from course.cached import cached_course, cached_course_material
from task.cached import cached_task
from live.cached import cached_live

content_type_onboard = settings.CONTENT_TYPE('program', 'onboard')
content_type_program = settings.CONTENT_TYPE('program', 'program')
content_type_course = settings.CONTENT_TYPE('course', 'course')
content_type_course_material = settings.CONTENT_TYPE('course', 'material')
content_type_question = settings.CONTENT_TYPE('question', 'activity')
content_type_task = settings.CONTENT_TYPE('task', 'task')
content_type_milestone = settings.CONTENT_TYPE('program', 'milestone')
content_type_live = settings.CONTENT_TYPE('live', 'live')
content_type_highlight = settings.CONTENT_TYPE('featured', 'highlight')


# TODO: migrate to utils.content get_content
def _get_content_cached(item, order_item_id=None):
    if item.content_type_id == content_type_program.id:
        item.content_cached = cached_program(item.content)
        item.content_type_cached = content_type_program
        if order_item_id:
            item.url = reverse('program:detail', args=[item.content, item.id])
        else:
            item.url = reverse('program:detail', args=[item.content])
    elif item.content_type_id == content_type_course.id:
        item.content_cached = cached_course(item.content)
        item.content_type_cached = content_type_course
        if order_item_id:
            item.url = reverse('course:detail', args=[item.content, item.id])
        else:
            item.url = reverse('course:detail', args=[item.content])
    elif item.content_type_id == content_type_course_material.id:
        material = cached_course_material(item.content)
        if material.type == 3:
            item.content_cached = Activity.pull(material.question_id)
            item.content_type_cached = content_type_question
        else:
            item.content_cached = material
            item.content_type_cached = content_type_course_material

    elif item.content_type_id == content_type_question.id:
        item.content_cached = Activity.pull(item.content)
        item.content_type_cached = content_type_question
        if order_item_id:
            item.url = reverse('question:detail', args=[item.content, item.id])
        else:
            item.url = reverse('question:detail', args=[item.content])

    elif item.content_type_id == content_type_task.id:
        item.content_cached = cached_task(item.content)
        item.content_type_cached = content_type_task
        if order_item_id:
            item.url = reverse('task:detail_item', args=[item.content, item.id])
        else:
            item.url = reverse('task:detail', args=[item.content])

    elif item.content_type_id == content_type_milestone.id:
        item.content_cached = cached_program_milestone(item.content)
        item.content_type_cached = content_type_milestone
        item.url = None
    elif item.content_type_id == content_type_live.id:
        item.content_cached = cached_live(item.content)
        item.content_type_cached = content_type_live
        item.url = reverse('live:detail', args=[item.content])
    elif item.content_type_id == content_type_onboard.id:
        item.content_cached = cached_program(item.content)
        item.content_type_cached = content_type_program
        if order_item_id:
            item.url = reverse('program:detail', args=[item.content, item.id])
        else:
            item.url = reverse('program:detail', args=[item.content])
