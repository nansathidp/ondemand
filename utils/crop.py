from django.db import models

import base64

def crop_image(object, request):
    for field in object._meta.get_fields():
        if type(field) == models.fields.files.ImageField:
            name = field.name
            name_crop = '%s-crop'%(name)
            if name_crop in request.POST:
                with open(getattr(object, name).path, 'wb') as f:
                    f.write(base64.b64decode(request.POST.get(name_crop).split(',')[1]))
