
def convert_from_date(date):
    from django.utils import timezone
    from datetime import datetime
    return timezone.make_aware(datetime.combine(date, datetime.min.time()))
