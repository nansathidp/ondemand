from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def paginator(request, content_list):
    # Copy From https://docs.djangoproject.com/en/1.9/topics/pagination/#using-paginator-in-a-view
    paginator = Paginator(content_list, 100)
    page = request.GET.get('page')
    try:
        content = paginator.page(page)
    except PageNotAnInteger:
        content = paginator.page(1)
    except EmptyPage:
        content = paginator.page(paginator.num_pages)

    return content
