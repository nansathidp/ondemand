import datetime


def duration(request, day, hour, minute, second):
    def _int(request, code):
        if code is None:
            return 0
        try:
            _ = int(request.POST.get(code))
            return 0 if _ < 0 else _
        except:
            return 0
        
    d = _int(request, day)
    h = _int(request, hour)
    m = _int(request, minute)
    s = _int(request, second)
    return datetime.timedelta(days=d,
                              hours=h,
                              minutes=m,
                              seconds=s)


def duration_display(_duration):
    # Fix CIMB 2017-10-12
    try:
        s = _duration.total_seconds()
        d, r = divmod(s, 60 * 60 * 24)
        h, r = divmod(r, 3600)
        m, s = divmod(r, 60)
        # return '%02d:%02d:%02d' % (h, m, s)
        if d > 0:
            return '%d Day %02d:%02d:%02d' % (d, h, m, s)
        else:
            return '%02d:%02d:%02d' % (h, m, s)
        # elif h > 0:
        #     return '%d:%02d:%02d' % (h, m, s)
        # else:
        #     return '%d:%02d' % (m, s)
    except:
        # return '0:00'
        return '00:00:00'
