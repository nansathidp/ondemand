import inspect

from django.conf import settings
from django.core.urlresolvers import reverse

def _breadcrumb(stack, level, deep, **kwargs):
    d = {}
    if stack == 'progress.account_view':
        d['title'] = 'Learner Progress'
        d['url'] = reverse('dashboard:progress:account')
    elif stack == 'progress.account_detail_view':
        d['title'] = 'Learner Progress Summary'
        d['url'] = reverse('dashboard:progress:account_detail', args=[kwargs['account_id']])
    elif stack == 'progress.detail_account_view':
        d['title'] = '%s Progress Summary'%kwargs['detail_account__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_account', args=[kwargs['detail_account__progress_id']])
    elif stack == 'progress.detail_course_view':
        d['title'] = 'Learner\'s %s Progress Detail'%kwargs['detail_course__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_course', args=[kwargs['detail_course__progress_id'],
                                                                     kwargs['detail_course__content_id']])
    elif stack == 'progress.detail_course_question_view':
        d['title'] = 'Learner\'s %s Progress Detail'%kwargs['detail_course_question__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_course_question', args=[kwargs['detail_course_question__progress_id'],
                                                                              kwargs['detail_course_question__content_id']])
    elif stack == 'progress.detail_course_question_program_view':
        d['title'] = 'Learner\'s %s Progress Detail'%kwargs['detail_course_question_program__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_course_question_program',
                           args=[kwargs['detail_course_question_program__progress_id'],
                                 kwargs['detail_course_question_program__content_id'],
                                 kwargs['detail_course_question_program__progress_program_id']])
    elif stack == 'progress.detail_course_question_onboard_view':
        d['title'] = 'Learner\'s %s Progress Detail'%kwargs['detail_course_question_onboard__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_course_question_onboard',
                           args=[kwargs['detail_course_question_onboard__progress_id'],
                                 kwargs['detail_course_question_onboard__content_id'],
                                 kwargs['detail_course_question_onboard__progress_program_id']])
    elif stack == 'progress.detail_question_view':
        d['title'] = 'Learner\'s %s Progress Detail'%kwargs['detail_question__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_question', args=[kwargs['detail_question__progress_id'],
                                                                       kwargs['detail_question__content_id']])
    elif stack == 'progress.detail_account_program_view':
        d['title'] = '%s Progress Summary'%kwargs['detail_account_program__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_account_program',
                           args=[kwargs['detail_account_program__progress_id'],
                                 kwargs['detail_account_program__progress_program_id']])
    elif stack == 'progress.detail_account_course_view':
        d['title'] = '%s Progress Summary'%kwargs['detail_account_course__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_account_course',
                           args=[kwargs['detail_account_course__progress_id'],
                                 kwargs['detail_account_course__progress_course_id']])
    elif stack == 'progress.detail_account_program_course_view':
        d['title'] = '%s Progress Summary'%kwargs['detail_account_program_course__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_account_program_course',
                           args=[kwargs['detail_account_program_course__progress_id'],
                                 kwargs['detail_account_program_course__progress_program_id'],
                                 kwargs['detail_account_program_course__progress_course_id']])
    elif stack in ['progress.question_score_view',
                   'progress.question_score_account_view',
                   'progress.question_score_account_program_view',
                   'progress.question_score_account_course_view',
                   'progress.question_score_account_program_course_view',
                   'progress.question_score_course_view',
                   'progress.question_score_program_view',
                   'progress.question_score_program_course_view',
                   'progress.question_score_onboard_view',
                   'progress.question_score_onboard_course_view']:
        d['title'] = 'Verifying Test Score'
        d['url'] = '#'
    elif stack == 'progress.detail_program_view':
        d['title'] = 'Learner\'s %s Progress Detail'%kwargs['detail_program__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_program', args=[kwargs['detail_program__progress_id'],
                                                                      kwargs['detail_program__content_id']])
    elif stack == 'progress.detail_program_item_view':
        d['title'] = 'Learner\'s %s Progress Detail'%kwargs['detail_program_item__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_program_item',
                           args=[kwargs['detail_program_item__progress_id'],
                                 kwargs['detail_program_item__content_id'],
                                 kwargs['detail_program_item__progress_program_id']])
    elif stack == 'progress.detail_onboard_view':
        d['title'] = 'Learner\'s %s Progress Detail'%kwargs['detail_onboard__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_onboard', args=[kwargs['detail_onboard__progress_id'],
                                                                      kwargs['detail_onboard__content_id']])
    elif stack == 'progress.detail_onboard_item_view':
        d['title'] = 'Learner\'s %s Progress Detail'%kwargs['detail_onboard_item__content_type_name'].title()
        d['url'] = reverse('dashboard:progress:detail_onboard_item',
                           args=[kwargs['detail_onboard_item__progress_id'],
                                 kwargs['detail_onboard_item__content_id'],
                                 kwargs['detail_onboard_item__progress_onboard_id']])
    elif stack in ['progress.course_material_account_view',
                   'progress.course_material_account_program_view',
                   'progress.course_material_view',
                   'progress.course_material_program_view',
                   'progress.course_material_onboard_view']:
        d['title'] = 'Progress Log'
        d['url'] = '#'
    elif stack == 'progress.content_course_view':
        d['title'] = 'Course Progress'
        d['url'] = reverse('dashboard:progress:content_course')
    elif stack == 'progress.content_question_view':
        d['title'] = 'Test Progress'
        d['url'] = reverse('dashboard:progress:content_question')
    elif stack == 'progress.content_program_view':
        d['title'] = 'Program Progress'
        d['url'] = reverse('dashboard:progress:content_program')
    elif stack == 'progress.content_onboard_view':
        d['title'] = 'Onboard Progress'
        d['url'] = reverse('dashboard:progress:content_onboard')
    elif stack == 'progress.content_detail_course_view':
        d['title'] = 'Progress Summary'
        d['url'] = reverse('dashboard:progress:content_detail_course',
                           args=[kwargs['content_id']])
    elif stack == 'progress.content_detail_question_view':
        d['title'] = 'Progress Summary'
        d['url'] = reverse('dashboard:progress:content_detail_question',
                           args=[kwargs['content_id']])
    elif stack == 'progress.content_detail_program_view':
        d['title'] = 'Progress Summary'
        d['url'] = reverse('dashboard:progress:content_detail_program',
                           args=[kwargs['content_id']])
    elif stack == 'progress.content_detail_onboard_view':
        d['title'] = 'Progress Summary'
        d['url'] = reverse('dashboard:progress:content_detail_onboard',
                           args=[kwargs['content_id']])
        
    if level == deep:
        d['is_active'] = True
    else:
        d['is_active'] = False
    return d

def get_breadcrumb(stack_list, **kwargs):
    result = []
    level = 1
    deep = len(stack_list)
    for stack in stack_list:
        result.append(_breadcrumb(stack, level, deep, **kwargs))
        level += 1
    return result
