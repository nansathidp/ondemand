from django.conf import settings
from django.core.cache import cache

from django.contrib.contenttypes.models import ContentType
from utils.cached.time_out import get_time_out_week


def cached_content_type_admin_item_inline(is_force=False):
    key = '%s_content_type_admin_item_inline' % settings.CACHED_PREFIX
    result = None if is_force else cache.get(key)
    if result is None:
        result = ContentType.objects.filter(app_label__in=['app', 'course', 'package', 'coin', 'program', 'question', 'live'],
                                            model__in=['app', 'course', 'package', 'coin', 'program', 'activity', 'live'])
        cache.set(key, result, get_time_out_week())
    return result
