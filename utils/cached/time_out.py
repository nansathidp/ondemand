import random

TIME_RANGE = range(60 * 3, 60 * 10, 60)  # 3 - 9 m.

TIME_RANGE_HOUR = range(60 * 60, 60 * 60 * 3, 60 * 5)  # 1 - 3 hr

TIME_RANGE_DAY = range(60 * 60 * 24, 60 * 60 * 24 * 5, 60 * 30)  # Day

TIME_RANGE_WEEK = range(60 * 60 * 24 * 7, 60 * 60 * 24 * 7 * 4, 60 * 60 * 4)  # Week


def get_time_out():
    return random.choice(TIME_RANGE)


def get_time_out_hour():
    return random.choice(TIME_RANGE_DAY)


def get_time_out_day():
    return random.choice(TIME_RANGE_DAY)


def get_time_out_week():
    return random.choice(TIME_RANGE_WEEK)
