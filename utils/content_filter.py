def filter_all(list, provider, category, name, sort):
    if list is not None and list != []:
        if name and name != -1:
            list = list.filter(name__icontains=name)

        if category != -1:
            list = list.filter(category=category)

        if sort == 'latest':
            list = list.order_by('timestamp')
        elif sort == 'atoz':
            list = list.order_by('name')
        elif sort == 'ztoa':
            list = list.order_by('-name')

        if provider and provider != -1:
            list = list.filter(provider=provider)

    return list
