from django.contrib import admin

from .models import Level, Member

@admin.register(Level)
class LevelAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_display', 'timestamp')

@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ('level', 'account')
