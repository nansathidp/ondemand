from django.shortcuts import render
from django.core.exceptions import PermissionDenied

from ..models import Level

def home_view(request):
    if not request.user.has_perm('level.view_level',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    level_list = Level.objects.all()
    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Level Management'}
    ]
    return render(request,
                  'level/dashboard/home.html',
                  {'SIDEBAR': 'level',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'level_list': level_list})
