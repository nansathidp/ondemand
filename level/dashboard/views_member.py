from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ..models import Level

from utils.paginator import paginator

def member_view(request, level_id):
    if not request.user.has_perm('level.view_level',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    level = get_object_or_404(Level, id=level_id)
    member_list = level.member_set.prefetch_related('account').all()
    member_list = paginator(request, member_list)
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Level Management',
         'url': reverse('dashboard:level-dashboard:home')},
        {'is_active': True,
         'title': 'Career Learner Management'}
    ]
    return render(request,
                  'level/dashboard/member.html',
                  {'SIDEBAR': 'level',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'level': level,
                   'member_list': member_list})
