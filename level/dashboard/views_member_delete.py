from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Level, Member

def member_delete_view(request, level_id, member_id):
    if not request.user.has_perm('level.change_level',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    member = get_object_or_404(Member, level_id=level_id, id=member_id)
    member.delete()
    return redirect('dashboard:level-dashboard:member', level_id)
