from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ..models import Level, Member
from account.models import Account

def member_add_view(request, level_id):
    if not request.user.has_perm('level.change_level',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    level = get_object_or_404(Level, id=level_id)
    if request.method == 'POST':
        email = request.POST.get('email', '')
        if len(email) > 0:
            account = Account.objects.filter(email__icontains=email).first()
            if account is not None and not Member.objects.filter(level=level, account=account).exists():
                Member.objects.create(level=level,
                                      account=account)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Level Management',
         'url': reverse('dashboard:level-dashboard:home')},
        {'is_active': False,
         'title': 'Level (%s)'%level.name,
         'url': reverse('dashboard:level-dashboard:member', args=[level.id])},
        {'is_active': True,
         'title': 'Add Learner'}
    ]
    return render(request,
                  'level/dashboard/member_add.html',
                  {'SIDEBAR': 'level',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'level': level})
