from django.db import models


class Level(models.Model):
    name = models.CharField(max_length=120)
    is_display = models.BooleanField(default=True, db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')


class Member(models.Model):
    from django.conf import settings

    level = models.ForeignKey(Level)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='level_member_set')
