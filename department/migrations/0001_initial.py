# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-16 10:52
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Admin',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='department_admin_set', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120)),
                ('is_display', models.BooleanField(db_index=True, default=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('parents', models.ManyToManyField(blank=True, to='department.Department')),
            ],
            options={
                'permissions': (('view_department', 'Can view department'), ('view_own_department', "Can view own's department")),
            },
        ),
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.CharField(blank=True, max_length=120)),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='department_member_set', to=settings.AUTH_USER_MODEL)),
                ('department', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='department.Department')),
            ],
        ),
        migrations.AddField(
            model_name='admin',
            name='department',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='department.Department'),
        ),
    ]
