# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-20 06:02
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('department', '0005_department_external_id'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='department',
            options={'default_permissions': ('view', 'view_own', 'add', 'change', 'delete'), 'ordering': ['name']},
        ),
    ]
