# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-17 10:03
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('department', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='department',
            options={'ordering': ['name'], 'permissions': (('view_department', 'Can view department'), ('view_own_department', "Can view own's department"))},
        ),
    ]
