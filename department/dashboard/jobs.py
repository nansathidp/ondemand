import os
import time
import uuid
import datetime
import json

import xlrd
from django.conf import settings
from django.utils import timezone

from alert.models import Alert
from department.models import Department

data_list = []

def _str(value, type):
    if type in [2, 3]:
        value = str(int(float(value)))
    else:
        value = value.strip()
    return value

def _id(value):
    try:
        return int(value)
    except:
        return 0


def _name(value):
    try:
        return str(value).strip()
    except:
        return ''

def _department(external_id, name, name_en, log):
    log.write('Department_ID: %s ' % external_id)
    department = Department.objects.filter(external_id=external_id).first()
    if department is None:
        department = Department.objects.create(
            external_id=external_id,
            name=name,
            name_en=name_en,
        )
        log.write('created')
    else:
        department.external_id = external_id
        department.name = name
        department.name_en = name_en
        department.save()
        log.write('updated')
    return department

def _push_department(data):
    if data['id'] == 0:
        department = Department.objects.filter(name__iexact=data['name']).first()
    else:
        department = Department.objects.filter(external_id=data['id']).first()

    if department is None:
        department = Department.objects.create(
            external_id=data['id'],
            name=data['name']
        )
    elif department.external_id != data['id'] or department.name != data['name']:
        department.external_id = data['id']
        department.name = data['name']
        department.save(update_fields=['external_id', 'name'])
    data['department'] = department


def _push_parent(data):
    if data['parent_id'] == 0:
        parent = Department.objects.filter(name=data['parent_name']).first()
    else:
        parent = Department.objects.filter(external_id=data['parent_id']).first()
    if parent is None:
        parent = Department.objects.create(
            external_id=data['parent_id'],
            name=data['parent_name']
        )
    elif parent.external_id != data['parent_id'] or parent.name != data['parent_name']:
        parent.external_id = data['parent_id']
        parent.name = data['parent_name']
        parent.save(update_fields=['external_id', 'name'])
    data['department'].parents.set([parent])


def import_department_job(alert_id):
    try:
        alert = Alert.objects.get(id=alert_id)
    except:
        return None
    _start = timezone.now()
    current_tz = timezone.get_current_timezone()

    alert.filename = '%s.log' % (uuid.uuid4())
    log = open('%s/%s' % (alert.get_export_path(), alert.filename), 'w')
    log.write('File executed: %s\n' % alert.filename_input)

    path = os.path.join(settings.BASE_DIR, 'alert', 'upload', alert.file_input)
    book = xlrd.open_workbook(path)
    sh = book.sheet_by_index(0)

    # Check File Error
    try:
        p = alert.filename_input.split('.')[0].split('_')
        is_error = False
    except:
        is_error = True

    if not is_error:
        if len(p) != 6:
            is_error = True
    if not is_error:
        if p[0] != 'CIMB' or p[1] != 'THAI' or p[2] != 'LMS' or p[3] != 'DEPARTMENT':
            is_error = True
    if not is_error:
        try:
            _date = datetime.date(int(p[4][:4]), int(p[4][4:6]), int(p[4][6:]))
        except:
            is_error = True
    if not is_error:
        try:
            if sh.nrows -1 != int(p[5]):
                is_error = True
        except:
            is_error = True
    if not is_error:
        header = [
            'Department_ID',
            'Department_Name_TH',
            'Department_Name_EN',
            'Parent_Department_ID',
            'Parent_Department_Name_TH',
            'Parent_Department_Name_EN'
        ]
        for cx in range(6):
            if sh.cell(0, cx).value.strip() != header[cx]:
                is_error = True
                break

    if is_error:
        _start = current_tz.normalize(_start.astimezone(current_tz))
        _end = current_tz.normalize(timezone.now().astimezone(current_tz))
        msg = """\n
Start Executing at: %s
Finished Executing at: %s

Please check the file name and format to be according to the agreed standards

File name: 'CIMB_THAI_LMS_DEPARTMENT_(Date)_(Number of records).(extension)' as .xls or .xlsx
(Date) must be in YearMonthDay (YYYYMMDD): E.g. 20171022 - 8 characters [Year(4)][Month(2)][Day(2)] of the file created date
(Number of records) must be a valid positive integer which is the number of records (doesn't include header row)

File format: the columns of the first row in the excel file must be exactly (case sensitive - in the exact order) as the following
1.Department_ID
2.Department_Name_TH
3.Department_Name_EN
4.Parent_Department_ID
5.Parent_Department_Name_TH
6.Parent_Department_Name_EN       
""" % (_start.strftime("%H:%M:%S %Y%m%d"), _end.strftime("%H:%M:%S %Y%m%d"))
        log.write(msg)
        log.close()
        time.sleep(5)
        alert.status = -3
        alert.save()
        return None
    # End Check File Error

    # Flag to not update
    department_flag = {}
    for department in Department.objects.all():
        department_flag[department.external_id] = {
            'found': False,
            'obj': department
        }
        department.parents.clear()

    log.write('\n\nExecution results\n')
    error_count = 0
    error_list = []
    for rx in range(sh.nrows):
        if rx == 0:
            continue
        data = {
            'id': _str(sh.cell(rx, 0).value, sh.cell(rx, 0).ctype),
            'name': _name(sh.cell(rx, 1).value),
            'name_en': _name(sh.cell(rx, 2).value),
            'parent_id': _str(sh.cell(rx, 3).value, sh.cell(rx, 3).ctype),
            'parent_name': _name(sh.cell(rx, 4).value),
            'parent_name_en': _name(sh.cell(rx, 5).value),
            'parent': None
        }
        log.write('[Row #%s]: ' % (rx+1))
        is_error = False
        for _ in ['id', 'name', 'name_en', 'parent_id', 'parent_name', 'parent_name_en']:
            if _ in ['id', 'parent_id']:
                if len(data[_]) != 8:
                    is_error = True
            elif not 1 <= len(data[_]) <= 255:
                is_error = True
        if is_error:
            log.write('[ERROR] One of the columns in not the correct format\n')
            error_count += 1
            error_list.append((rx+1))
            continue
        if not is_error:
            if data['id'] == data['parent_id']:
                log.write('[ERROR] Department_ID same as Parent_Department_ID\n')
                error_count += 1
                error_list.append((rx+1))
                continue
        department = _department(data['id'], data['name'], data['name_en'], log)
        log.write(', ')
        parent = _department(data['parent_id'], data['parent_name'], data['parent_name_en'], log)
        log.write('\n')
        department.parents.set([parent])
        if data['id'] in department_flag:
            department_flag[data['id']]['found'] = True
        if data['parent_id'] in department_flag:
            department_flag[data['parent_id']]['found'] = True

    log.write('\n\nDeletion\n')
    delete_count = 0
    for key, value in department_flag.items():
        if value['found'] is False:
            log.write('DELETED: Department_ID: %s | %s | %s\n' % (
                value['obj'].external_id,
                value['obj'].name,
                value['obj'].name_en,
            ))
            value['obj'].delete()
            delete_count += 1

    log.write('\n\nValidation\n')
    no_parent = 0
    for department in Department.objects.filter(parents=None):
        log.write('Department_ID: %s does not have parent\n' % department.external_id)
        no_parent += 1

    _start = current_tz.normalize(_start.astimezone(current_tz))
    if _start.date() != _date:
        log.write('\n\nWARNING: THE FILE UPLOADED MIGHT BE THE WRONG DATE\n')

    log.write('\n\n[SUMMARY]\n')
    log.write('Rows executed: %s\n' % (sh.nrows-1))
    log.write('Rows ERROR: %s {' % error_count)
    for error in error_list:
        log.write('#%s, ' % error)
    log.write('}')

    log.write('\n\nDepartments deleted: %s\n' % delete_count)
    log.write('Departments without parent: %s\n' % no_parent)

    log.write('\n\nTotal number of current departments: %s\n' % Department.objects.count())
    _end = current_tz.normalize(timezone.now().astimezone(current_tz))
    log.write('Start Executing at: %s\n' % _start.strftime("%H:%M:%S %Y%m%d"))
    log.write('Finish Executing at: %s\n' % _end.strftime("%H:%M:%S %Y%m%d"))
    log.close()

    time.sleep(5)
    msg = """DEPARTMENT latest update: %s
Start Executing at: %s
Finish Executing at: %s\n""" % (
        alert.filename_input,
        _start.strftime("%H:%M:%S %Y%m%d"),
        _end.strftime("%H:%M:%S %Y%m%d")
    )
    result = {
        'error': error_count,
        'msg': msg
    }
    alert.json_result = json.dumps(result, indent=2)
    alert.status = 2
    alert.save()
