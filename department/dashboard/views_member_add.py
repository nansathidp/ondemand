from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ..models import Department, Member as DepartmentMember
from account.models import Account


def member_add_view(request, department_id):
    if not request.user.has_perm('department.change_department',group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    department = get_object_or_404(Department, id=department_id)
    if request.method == 'POST':
        email = request.POST.get('email', '')
        position = request.POST.get('position', '')
        if len(email) > 0:
            account = Account.objects.filter(email__iexact=email).first()
            if account is not None:
                DepartmentMember.push(department, account, position)
        return redirect('dashboard:department-dashboard:member', department_id)

    breadcrumb_list = [
        {'is_active': False,
         'title': 'Organization Management',
         'url': reverse('dashboard:department-dashboard:home')},
        {'is_active': False,
         'title': 'Department (%s)' % department.name,
         'url': reverse('dashboard:department-dashboard:member', args=[department.id])},
        {'is_active': True,
         'title': 'Add Learner'}
    ]

    return render(request,
                  'department/dashboard/member_add.html',
                  {'SIDEBAR': 'department',
                   'BREADCRUMB_LIST': breadcrumb_list,
                   'department': department})
