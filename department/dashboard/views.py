from django.shortcuts import render
from django.core.exceptions import PermissionDenied

from ..models import Department

from utils.paginator import paginator


def home_view(request):
    if not request.user.has_perm('department.view_department',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    department_list = Department.objects.all()
    department_list = paginator(request, department_list)
    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Organization Management'}
    ]
    return render(request,
                  'department/dashboard/home.html',
                  {'SIDEBAR': 'department',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'department_list': department_list})
