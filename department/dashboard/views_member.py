from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ..models import Department

from utils.paginator import paginator

# test rebase 2

def member_view(request, department_id):
    if not request.user.has_perm('department.view_department',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    department = get_object_or_404(Department, id=department_id)
    member_list = department.member_set.prefetch_related('account').all()
    member_list = paginator(request, member_list)
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Organization Management',
         'url': reverse('dashboard:department-dashboard:home')},
        {'is_active': True,
         'title': 'Learner Management'}
    ]
    return render(request,
                  'department/dashboard/member.html',
                  {'SIDEBAR': 'department',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'department': department,
                   'member_list': member_list})
