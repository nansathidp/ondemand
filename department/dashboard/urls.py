from django.conf.urls import url

from .views import home_view

from .views_member import member_view
from .views_member_add import member_add_view
from .views_member_delete import member_delete_view

app_name = 'department-dashboard'
urlpatterns = [
    url(r'^$', home_view, name='home'), #TODO: testing

    url(r'^(\d+)/member/$', member_view, name='member'), #TODO: testing
    url(r'^(\d+)/member/add/$', member_add_view, name='member_add'), #TODO: testing
    url(r'^(\d+)/member/(\d+)/delete/$', member_delete_view, name='member_delete'), #TODO: testing
]
