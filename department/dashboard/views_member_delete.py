from django.shortcuts import redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Member


def member_delete_view(request, department_id, member_id):
    if not request.user.has_perm('department.change_department',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    member = get_object_or_404(Member, department_id=department_id, id=member_id)
    member.delete()
    return redirect('dashboard:department-dashboard:member', department_id)
