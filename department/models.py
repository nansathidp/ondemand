from django.db import models


class Department(models.Model):
    # external_id = models.IntegerField(default=0, db_index=True)
    external_id = models.CharField(max_length=255, db_index=True)

    parents = models.ManyToManyField('self', symmetrical=False, blank=True)
    name = models.CharField(max_length=255)
    name_en = models.CharField(max_length=255, blank=True, default='')
    is_display = models.BooleanField(default=True, db_index=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['name']
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')

    def __str__(self):
        return self.name

    def get_child(self):
        from .cached import cached_department_child
        return cached_department_child(self)

    def get_all_child(self):
        result = [self]
        for child in self.get_child():
            result.extend(child.get_all_child())
        return result

    # TODO: CACHED
    @staticmethod
    def pull(name):
        name = name.lower()
        department = Department.objects.filter(name=name).first()
        if department is None:
            department = Department.objects.create(name=name)
        return department

    @staticmethod
    def pull_list():
        return Department.objects.filter(parents=None)

    @staticmethod
    def pull_user_level(account):
        department_list = Department.objects.filter(
            member__account=account
        )
        result_list = []
        _department_list = list(department_list)

        def _check_root(department):
            for parent in department.parents.all():
                for _ in _department_list:
                    if _ == parent:
                        return False
                _check_root(parent)
            return True

        for _ in department_list:
            if _check_root(_):
                result_list.append(_)
        return result_list

    def push(self, account):
        if not Member.objects.filter(department=self, account=account).exists():
            Member.objects.create(department=self, account=account)

    def display(self, child_list=[]):
        return {
            'id': self.id,
            'name': self.name,
            'child_list': [child.display(child.get_child()) for child in child_list],
        }


class Member(models.Model):
    from django.conf import settings

    department = models.ForeignKey(Department)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='department_member_set')
    position = models.CharField(max_length=255, blank=True)

    @staticmethod
    def push(department, account, position=''):
        if not Member.objects.filter(department=department, account=account).exists():
            Member.objects.create(department=department,
                                  account=account,
                                  position=position)

    @staticmethod
    def clear(account):
        return Member.objects.filter(account=account).delete()

    @staticmethod
    def pull_list(account):
        return Member.objects.filter(account=account)


class Admin(models.Model):
    from django.conf import settings

    department = models.ForeignKey(Department)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='department_admin_set')
