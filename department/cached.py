from django.conf import settings
from django.core.cache import cache

from .models import Department

from utils.cached.time_out import get_time_out_day

def cached_department(department_id, is_force=False):
    key = '%s_department_%s' % (settings.CACHED_PREFIX, department_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Department.objects.get(id=department_id)
        except:
            result = -1
        cache.set(key, result, get_time_out_day())
    return None if result == -1 else result

def cached_department_child(department, is_force=False):
    key = '%s_department_child_%s' % (settings.CACHED_PREFIX, department.id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = list(department.department_set.all())
        except:
            result = -1
        cache.set(key, result, get_time_out_day())
    return [] if result == -1 else result
