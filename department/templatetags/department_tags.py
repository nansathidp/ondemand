from django import template

register = template.Library()

from ..cached import cached_department


@register.inclusion_tag('department/dashboard/filter_tag.html')
def filter_tag(department, q_department, n):
    return {'department_list': list(department.get_child()),
            'q_department': q_department,
            'n': n + 2,
            'n_list': range(n + 2)}


@register.filter
def department_name_tag(department_id):
    department = cached_department(department_id)
    if department is None:
        return ''
    else:
        return department.name
