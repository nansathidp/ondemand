from django.contrib import admin
from .models import Department, Member, Admin


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('external_id', 'name', 'name_en', 'is_display', 'timestamp')
    search_fields = ('name',)


@admin.register(Member)
class MemberAdin(admin.ModelAdmin):
    list_display = ('department', 'account', 'position')


@admin.register(Admin)
class AdminAdmin(admin.ModelAdmin):
    list_display = ('department', 'account')
