from django.db import models


def _de(code):
    from Crypto.Cipher import AES
    import base64

    code = code.replace(' ', '+')
    ciphertext_dec = base64.b64decode(code)
    iv_dec = ciphertext_dec[:16]
    ciphertext = ciphertext_dec[16:]
    aes = AES.new(Request.KEY_AES, AES.MODE_CBC, iv_dec)
    text = aes.decrypt(ciphertext).decode('utf-8').replace('\x00', '')
    #text = base64.b64encode(bytes(text_byte, 'utf-8'))
    return text


def _en(text):
    def pad(s):
        return str.encode(s) + b"\0" * (AES.block_size - len(s) % AES.block_size)

    def _t(vi, s):
        return base64.b64encode(vi+s)

    from Crypto.Cipher import AES
    from Crypto import Random
    import base64

    iv = Random.new().read(16)
    aes = AES.new(Login.KEY_AES, AES.MODE_CBC, iv)

    return _t(iv, aes.encrypt(pad(text)))


class Request(models.Model):
    code = models.CharField(max_length=512)
    email = models.CharField(max_length=512)
    text = models.TextField()
    status_code = models.IntegerField()
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    KEY = '084243855820f9ca47f466f645784636'
    KEY_AES = 'IPgwhmNOoBjDfHEeFIySOkKTeXP+zhJD'
    #URL = 'http://pintotest.com/unet-uat/businesssso/index/getProfileByEmailConicle'
    URL = 'http://pintotest.com/unet-uat/businesssso/index/getProfileByEmailConicle2'
    
    
    class Meta:
        ordering = ['-timestamp']

    @staticmethod
    def send(code, is_encryption):
        from account.models import Account
        from access.models import Account as AccessAccount

        from Crypto.Cipher import AES
        import requests
        import base64
        import uuid
        import json
        
        if is_encryption:
            code = code.replace(' ', '+')
            ciphertext_dec = base64.b64decode(code)
            iv_dec = ciphertext_dec[:16]
            ciphertext = ciphertext_dec[16:]
            aes = AES.new(Request.KEY_AES, AES.MODE_CBC, iv_dec)
            email = aes.decrypt(ciphertext).decode('utf-8').replace('\x00', '')
            email_send = base64.b64encode(bytes(email, 'utf-8'))
        else:
            email = code
            email_send = base64.b64encode(bytes(code, 'utf-8'))
        payload = {'key': Request.KEY,
                   'email': email_send}
        r = requests.get(Request.URL, params=payload)
        try:
            result = r.json()
            status_code = r.status_code
            text = json.dumps(result, indent=2)
        except:
            status_code = 204
            text = '-'
            
        Request.objects.create(code=code,
                               email=email,
                               text=text,
                               status_code=status_code)
        if status_code != 200:
            return None
        if result['header']['code'] == 200:
            account = Account.objects.filter(email=email).first()
            if account is None:
                password = str(uuid.uuid4())
                account = Account.objects.create_user(email, password)
            if 'full_name' in result['item']:
                account.first_name = _de(result['item']['full_name'])
            if 'unet_account' in result['item'] and result['item']['unet_account'] is not None:
                account.info = _de(result['item']['unet_account'])
            else:
                account.info = ''
            account.save()
            if 'class_id' in result['item'] and result['item']['class_id'] is not None:
                AccessAccount.push_debug(account, result['item']['class_id'], result['item']['class'])
            return account
        else:
            return None


class Login(models.Model):
    text = models.TextField()
    status_code = models.IntegerField()
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    KEY_AES = 'IPgwhmNOoBjDfHEeFIySOkKTeXP+zhJD'
    
    URL = 'http://pintotest.com/unet-uat/businesssso/index/loginConiclePost?key=084243855820f9ca47f466f645784636'
    #URL = 'http://pintotest.com/unet-uat/businesssso/index/loginPost?key=084243855820f9ca47f466f645784636'
    
    class Meta:
        ordering = ['-timestamp']

    @staticmethod
    def send(email, password):
        from account.models import Account
        from access.models import Account as AccessAccount
        
        from Crypto.Cipher import AES
        from Crypto import Random
        import requests
        import base64
        import json
        import hashlib
        m = hashlib.md5()

        email_send = base64.b64encode(bytes(email, 'utf-8'))
        password_send = base64.b64encode(bytes(password, 'utf-8'))

        #email_send = _en(email)
        #password_send = _en(password)
        
        payload = {'email': email_send,
                   'password': password_send}
        r = requests.post(Login.URL, data=payload)
        try:
            result = r.json()
            status_code = r.status_code
            text = json.dumps(result, indent=2)
        except:
            raise
            status_code = 204
            text = '-'
        Login.objects.create(text=text,
                             status_code=status_code)
        if status_code != 200:
            return None
        if result['header']['code'] == 200:
            account = Account.objects.filter(email=email).first()
            if account is None:
                account = Account.objects.create_user(email, password)
            if 'full_name' in result['item']:
                account.first_name = _de(result['item']['full_name'])
            #if 'gender' in result['item']:
            #    account.gender = result['item']['gender']
            if 'unet_account' in result['item'] and result['item']['unet_account'] is not None:
                account.info = _de(result['item']['unet_account'])
            else:
                account.info = ''
            account.save()
            if 'class_id' in result['item'] and result['item']['class_id'] is not None:
                AccessAccount.push_debug(account, result['item']['class_id'], result['item']['class'])
            return account
        else:
            return None
