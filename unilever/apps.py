from django.apps import AppConfig


class UnileverConfig(AppConfig):
    name = 'unilever'
