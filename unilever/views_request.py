from django.shortcuts import redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login

from unilever.models import Request


def request_view(request):
    code = request.GET.get('code', None)
    if code is not None:
        account = Request.send(code, True)
        if account is not None:
            account = authenticate(email=account.email, password=None)
            login(request, account)
            return redirect('home')
    return HttpResponse("^_^")
