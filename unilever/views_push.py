from django.shortcuts import redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login

from .models import Request


def push_view(request):
    email = request.GET.get('email', None)
    if email is not None:
        account = Request.send(email, False)
        if account is not None:
            account = authenticate(email=account.email, password=None)
            login(request, account)
            return redirect('home')
        
        #account = authenticate(email=email, password=None)
        #account = None
        #if account is not None:
        #    login(request, account)
        #    return redirect('home')
    return HttpResponse("Here's the text of the Web page.")
