from django.views.decorators.http import require_POST
from django.contrib.auth import authenticate, login
from django.utils import timezone

from .models import Login

from api.views_api import json_render

import urllib

@require_POST
def login_view(request):
    result = {}
    code = 200
    try:
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        ref = request.POST.get('ref', None)
        email = urllib.parse.unquote(email)
        password = urllib.parse.unquote(password)
        if ref is None:
            result['link'] = request.META.get('HTTP_REFERER')
        else:
            result['link'] = ref

        if email is not None and password is not None:
            account = Login.send(email, password)
            if account is not None:
                account = authenticate(email=account.email, password=None)
                login(request, account)

                request.session["is_skip"] = False
                    
                #Fix case /login/
                if request.method == 'POST':
                    next = request.POST.get('next', None)
                    if next is not None:
                        result['link'] = next
            else:
                code = 300
        else:
            code = 300
    except:
        raise
        skip = request.POST.get('skip', None)
        if skip == "1":
            request.session["is_skip"] = True
        else:
            code = 306

    return json_render(result, code)
