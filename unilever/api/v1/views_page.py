from .views import check_key, check_require
from .views import json_render

# from highlight.models import HIGHLIGHT_TYPE
from django.contrib.staticfiles.templatetags.staticfiles import static

type_dict = dict((x, y) for x, y in HIGHLIGHT_TYPE)


def _push(item_list, type, image, data):
    item_list.append({
                'type': type,
                'type_display': type_dict[type],
                'image':  static(image),
                'data': data
            },)


def academy_view(request):
    code = 200
    item_list = []
    result = {
        'item_list': item_list
    }

    # response = check_key(request)
    # if response is not None:
    #     return response

    # code, token, uuid, store, version = check_require(request)
    # if code != 200:
    #     return json_render(result, code)

    _push(item_list, 6, 'unilever/images/cover/academy.jpg', 'https://webview.unilever.cocodemy.com/roadmap/')
    _push(item_list, 7, 'unilever/images/cover/bt.jpg', 1464957558252333)
    _push(item_list, 7, 'unilever/images/cover/pt.jpg', 1464958394252419)
    _push(item_list, 7, 'unilever/images/cover/pk.jpg', 1464958458252423)

    return json_render(result, code)


def inspiration_view(request):
    code = 200
    item_list = []
    result = {
        'item_list': item_list
    }

    # Open mind
    _push(item_list, 7, 'unilever/images/RoadtoSuccess_SBA_600x600.jpg', 1464959128252469)

    # Success Story
    _push(item_list, 7, 'unilever/images/RoadtoSuccess_SBA_600x600.jpg', 1464959221252476)

    return json_render(result, code)
