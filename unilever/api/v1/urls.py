from django.conf.urls import url

from .views_page import academy_view, inspiration_view

urlpatterns = [
    url(r'^page/academy/$', academy_view), #TODO: testing
    url(r'^page/inspiration/$', inspiration_view), #TODO: testing
]