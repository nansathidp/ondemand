from .views import json_render, check_key
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.utils import timezone

from ...models import Login
# from account.models import App as AccountApp

import urllib


@csrf_exempt
def login_view(request):
    result = {}
    code = 200
    response = check_key(request)
    if response is not None:
        return response
    if request.method == 'POST':
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        uuid = request.POST.get('uuid', None)
    else:
        email = request.GET.get('email', None)
        password = request.GET.get('password', None)
        uuid = request.GET.get('uuid', None)

    if uuid is not None and len(uuid) > 0:
        pass
    else:
        return json_render({}, 202)

    if email is None or len(email) < 4 or password is None or len(password) < 4:
        return json_render({}, 300)
    else:
        email = urllib.parse.unquote(email)
        password = urllib.parse.unquote(password)
        account = Login.send(email, password)
        #account = authenticate(email=email, password=password)

        if account is not None:
            # AccountApp.api_app_push(request.APP, account)
            result['token'] = account.login_token()
            if account.first_active is None:
                account.first_active = timezone.now()
            account.last_active = timezone.now()
            account.update_uuid(uuid, is_login_facebook=None)
            account.device_log(uuid, 1)
            account.save(update_fields=['last_active'])
            
            result['profile'] = account.api_profile()
            result['is_require_information'] = False
        else:
            return json_render({}, 300)
    return json_render(result,
                       code)
