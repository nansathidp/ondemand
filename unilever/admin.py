from django.contrib import admin

from .models import Request, Login

@admin.register(Request)
class RequestAdmin(admin.ModelAdmin):
    list_display = ('code', 'email', 'timestamp')

@admin.register(Login)
class LoginAdmin(admin.ModelAdmin):
    list_display = ('status_code', 'timestamp')
