from django.contrib import admin

from .models import Log, Error, Patch

@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    list_display = ('code', 'admin_email', 'account_email', 'content_type', 'content', 'timestamp')

@admin.register(Error)
class ErrorAdmin(admin.ModelAdmin):
    list_display = ('subject', 'level', 'timestamp')

@admin.register(Patch)
class PatchAdmin(admin.ModelAdmin):
    list_display = ('path', 'is_deploy', 'timestamp')
    readonly_fields = ('result', )
