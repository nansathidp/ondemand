from django.db import models


class Log(models.Model):
    code = models.CharField(max_length=60, db_index=True)

    provider_id = models.BigIntegerField(db_index=True, default=-1)
    sub_provider_id = models.BigIntegerField(db_index=True, default=-1)
    
    admin_id = models.BigIntegerField()
    admin_email = models.CharField(max_length=512)
    admin_display = models.CharField(max_length=120)
    account_id = models.BigIntegerField()
    account_email = models.CharField(max_length=512, blank=True)
    account_display = models.CharField(max_length=120, blank=True)

    note = models.TextField(blank=True)

    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+', null=True)
    content = models.BigIntegerField(db_index=True, default=-1)

    old = models.TextField(blank=True) #Json
    new = models.TextField(blank=True) #Json
    
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']

    @staticmethod
    def access_list(request):
        if request.user.has_perm('log.view_log',
                                 group=request.DASHBOARD_GROUP):
            return Log.objects.all()
        else: #TODO: implements Own Content...
            return Log.objects.all()
            
    @staticmethod
    def push(code, provider_id, sub_provider_id, admin, account, note,
             content_type, content, old, new):
        if admin is not None:
            admin_id = admin.id
            admin_email = admin.email
            admin_display = '%s %s'%(admin.first_name, admin.last_name)
        else:
            admin_id = -1
            admin_email = ''
            admin_display = ''
        if account is not None:
            account_id = account.id
            account_email = account.email
            account_display = '%s %s'%(account.first_name, account.last_name)
        else:
            account_id = -1
            account_email = ''
            account_display = ''
        Log.objects.create(code=code,
                           provider_id=provider_id,
                           sub_provider_id=sub_provider_id,
                           admin_id=admin_id,
                           admin_email=admin_email,
                           admin_display=admin_display,
                           account_id=account_id,
                           account_email=account_email,
                           account_display=account_display,
                           note=note,
                           content_type=content_type,
                           content=content,
                           old=old,
                           new=new)

    def get_fields(self, data_old, data_new):
        from django.conf import settings
        result = []

        def _data(d, f):
            if d is not None:
                return d['fields'][f]
            else:
                return None
        
        if self.content_type_id == settings.CONTENT_TYPE('course', 'course').id and self.code in ['create', 'edit']:
            FIELD_LIST = ['name', 'image', 'provider', 'tutor', 'category', 'overview', 'condition', 'desc', 'is_display']
            for field in FIELD_LIST:
                d = {'field': field,
                     'old': _data(data_old, field),
                     'new': _data(data_new, field)}
                result.append(d)
        else:
            if data_old is not None:
                data = data_old
            else:
                data = data_new
            for field in data['fields']:
                d = {'field': field,
                     'old': _data(data_old, field),
                     'new': _data(data_new, field)}
                result.append(d)
        return result
    
    def get_data(self):
        import json
        new = old = True
        try:
            data_old = json.loads(self.old)[0]
        except:
            data_old = None
            old = False
        try:
            data_new = json.loads(self.new)[0]
        except:
            data_new = None
            new = False
        if data_old is None and data_new is None:
            result = []
        else:
            result = self.get_fields(data_old, data_new)
        if old and new:
            for _ in result:
                if _['old'] != _['new']:
                    _['is_change'] = True
        return old, new, result


class Error(models.Model):
    subject = models.CharField(max_length=255)
    level = models.CharField(max_length=255)
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']


class Patch(models.Model):
    path = models.CharField(max_length=255)
    file = models.FileField(upload_to='log/patch')
    is_deploy = models.BooleanField(default=False)
    result = models.TextField(blank=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    touch = models.CharField(max_length=255, blank=True)

    class Meta:
        ordering = ['-timestamp']

    def save(self):
        from django.utils import timezone
        import os
        import shutil
        super().save()
        run_deploy = getattr(self, 'run_deploy', True)
        if self.is_deploy and run_deploy:
            result = 'At: %s\r\n' % str(timezone.now())
            is_fail = False
            try:
                shutil.copy(self.file.path, self.path)
                result += 'Copy File.\r\n'
            except:
                is_fail = True
                result += 'Copy File => Fail.\r\n'
            if not is_fail and len(self.touch) > 0:
                result += 'Touch.\r\n'
                try:
                    os.utime(self.touch, None)
                except OSError:
                    open(self.touch, 'a').close()
            self.result = result
            self.run_deploy = False
            self.save()
