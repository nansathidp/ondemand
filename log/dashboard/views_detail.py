from django.shortcuts import render, get_object_or_404

from ..models import Log
from .views import pull_breadcrumb


def detail_view(request, log_id):
    log = get_object_or_404(Log, id=log_id)
    old, new, data_list = log.get_data()
    return render(request,
                  'log/dashboard/detail.html',
                  {'SIDEBAR': 'log',
                   'BREADCRUMB_LIST': pull_breadcrumb(),
                   'log': log,
                   'old': old,
                   'new': new,
                   'data_list': data_list})
