import inspect

from django.core.urlresolvers import reverse
from django.shortcuts import render
from utils.paginator import paginator

from ..models import Log


def home_view(request):
    log_list = Log.access_list(request)
    log_list = paginator(request, log_list)
    breadcrumb_list = [
        {'is_active': True,
         'title': 'Log'},
    ]
    return render(request,
                  'log/dashboard/home.html',
                  {'SIDEBAR': 'log',
                   'BREADCRUMB_LIST': breadcrumb_list,
                   'log_list': log_list})


def pull_breadcrumb(course=None):
    breadcrumb_list = [{'is_active': False,
                        'title': 'Log',
                        'url': reverse('dashboard:log:home')}]
    caller = inspect.stack()[1][3]
    if caller == 'detail_view':
        breadcrumb_list.append({'is_active': True,
                                'title': 'Information'})
    return breadcrumb_list
