from django.conf import settings
from django.db import models
from django.urls import reverse


class Banner(models.Model):
    # Use Permission Only

    class Meta:
        default_permissions = ('view', 'add', 'change', 'delete')


class Announcement(models.Model):
    # Use Permission Only

    class Meta:
        default_permissions = ('view', 'add', 'change')

        
class Highlight(models.Model):
    code = models.CharField(max_length=32, db_index=True, blank=True, null=True)
    target = models.CharField(max_length=32, db_index=True, blank=True, null=True)

    content = models.BigIntegerField(default=0)

    title = models.CharField(max_length=16, blank=True)
    desc = models.TextField(blank=True)
    url = models.CharField(max_length=256, blank=True)
    image = models.ImageField(upload_to='featured/highlight/image/')
    sort = models.IntegerField(default=0, db_index=True)
    is_display = models.BooleanField(default=True, db_index=True)

    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    timeupdate = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['sort']
        
    @staticmethod
    def pull(id):
        # TODO: Cached
        return Highlight.objects.filter(id=id).first()

    @staticmethod
    def pull_list(code):
        # TODO: Cached
        return Highlight.objects.filter(code=code, is_display=True).order_by('sort')

    def get_target_url(self):

        if self.target == 'course':
            return '%s%s' % (settings.SITE_URL, reverse('course:detail', args=(self.content,)))
        elif self.target == 'question':
            return '%s%s' % (settings.SITE_URL, reverse('question:detail', args=(self.content,)))
        elif self.target == 'program':
            return '%s%s' % (settings.SITE_URL, reverse('program:detail', args=(self.content,)))
        elif self.target == 'task':
            return '%s%s' % (settings.SITE_URL, reverse('task:detail', args=(self.content,)))
        elif self.target == 'link':
            return self.url if 'http' in self.url else 'http://%s' % self.url
        else:
            return None

    def api_display(self):
        return {
            'id': self.id,
            'code': self.code,
            
            'target': self.target,
            'content': self.content,
            'url': self.url,
            
            'title': self.title,
            'desc': self.desc,
            'image': self.image.url if self.image else None,
            'timeupdate': self.timeupdate.strftime(settings.TIMESTAMP_FORMAT_),
        }


class Featured(models.Model):
    content_type = models.ForeignKey('contenttypes.ContentType')
    content = models.BigIntegerField(db_index=True)

    def __str__(self):
        return str(self.content)

    @staticmethod
    def pull(content_type, content):
        if content_type is None:
            # TODO: Cached
            return Featured.objects.filter(id=content).first()
        else:
            featured = Featured.objects.filter(content_type=content_type, content=content).first()
            if featured is None:
                featured = Featured.objects.create(content_type=content_type, content=content)
            return featured

    @staticmethod
    def pull_item(content_type):
        return Item.objects.filter(content_type=content_type)

    def add_item(self, content_type, content):
        item = self.item_set.filter(content_type=content_type,
                                    content=content).first()
        if item is None:
            item = Item.objects.create(featured=self,
                                       content_type=content_type,
                                       content=content,
                                       sort=Item.objects.filter(featured=self,
                                                                content_type=content_type).count())
        return item


class Item(models.Model):
    featured = models.ForeignKey(Featured, null=True, blank=True)
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    content = models.BigIntegerField(db_index=True)
    sort = models.IntegerField(default=0, db_index=True)

    class Meta:
        ordering = ['sort']

    def get_content(self):
        from utils.content import get_content
        self.content_object = get_content(self.content_type_id, self.content) #TODO: remove
        return get_content(self.content_type_id, self.content)

    # TODO: Remove (use get_content)
    def get_content_cached(self):
        from utils.content_cached import _get_content_cached
        _get_content_cached(self)
