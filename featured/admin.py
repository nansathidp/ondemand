from django.contrib import admin

from .models import Highlight, Featured, Item

@admin.register(Highlight)
class HighlightAdmin(admin.ModelAdmin):
    list_display = ('code', 'title', 'content', 'sort', 'is_display')
    
@admin.register(Featured)
class FeaturedAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'content')

@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('featured', 'content_type', 'content', 'sort')
