from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import redirect, render

from utils.crop import crop_image
from .forms import HighlightStandAloneForm
from .views_stand_alone import check_code
from .views_stand_alone_add import find_target, pack_url
from ..models import Highlight


def stand_alone_detail_view(request, code, highlight_id):
    check_code(code)    
    highlight = Highlight.pull(highlight_id)
    if highlight is None:
        raise Http404

    # provider = get_object_or_404(Provider, id=provider_id)
    # if not provider.check_access(request):
    #    raise PermissionDenied

    # if not request.user.has_perm('provider.change_provider', group=request.DASHBOARD_GROUP):
    #    raise PermissionDenied

    if request.method == 'POST':
        highlight_form = HighlightStandAloneForm(request.POST, request.FILES, instance=highlight, code=code)
        if highlight_form.is_valid():
            highlight = highlight_form.save()
            item = find_target(highlight.url, settings.SITE_URL)
            if item['status']:
                highlight.target = item['target']
                highlight.content = item['item']
                highlight.save()
            else:
                item_url = pack_url(highlight.target, highlight.content)
                if item_url['status']:
                    highlight.url = item_url['url']
                    highlight.save()
                else:
                    highlight.url = item['item']
                    highlight.save()
            crop_image(highlight, request)
            return redirect('dashboard:featured:stand_alone', code)
    else:
        highlight_form = HighlightStandAloneForm(instance=highlight, code=code)

    request.code = code
    return render(request,
                  'featured/dashboard/highlight_add.html',
                  {
                      'SIDEBAR': code,
                      'BREADCRUMB_LIST': [{'title': '%s Management' % code.title(),
                                           'is_active': False,
                                           'url': reverse('dashboard:featured:stand_alone', args=[code])},
                                          {'title': highlight.title,
                                           'is_active': True,
                                           'url': None}],
                      'code': code,
                      'highlight_form': highlight_form,
                      'highlight': highlight,
                  })
