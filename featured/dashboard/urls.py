from django.conf.urls import url, include

from .views import home_view
from .views_search import search_view
from .views_search_add import search_add_view
from .views_search_delete import search_delete_view
from .views_sort import sort_view
from .views_delete import delete_view

from .views_highlight import highlight_view
from .views_highlight_add import highlight_add_view
from .views_highlight_detail import highlight_detail_view
from .views_highlight_sort import highlight_sort_view
from .views_highlight_delete import highlight_delete_view

from .views_stand_alone import stand_alone_view
from .views_stand_alone_add import stand_alone_add_view
from .views_stand_alone_detail import stand_alone_detail_view
from .views_stand_alone_sort import stand_alone_sort_view
from .views_stand_alone_delete import stand_alone_delete_view

app_name = 'featured'
urlpatterns = [
    url('^(\d+)/(\w+)/$', home_view, name='home'),
    url('^(\d+)/(\w+)/search/$', search_view, name='search'),
    url('^(\d+)/(\w+)/search/add/$', search_add_view, name='search_add'),
    url('^(\d+)/(\w+)/search/delete/$', search_delete_view, name='search_delete'),
    url('^(\d+)/(\w+)/sort/$', sort_view, name='sort'),
    url('^(\d+)/(\w+)/delete/$', delete_view, name='delete'),
    
    url('^(\d+)/highlight/(\w+)/$', highlight_view, name='highlight'),
    url('^(\d+)/highlight/(\w+)/add/$', highlight_add_view, name='highlight_add'),
    url('^(\d+)/highlight/(\w+)/(\d+)/$', highlight_detail_view, name='highlight_detail'),
    url('^(\d+)/highlight/(\w+)/(\d+)/sort/$', highlight_sort_view, name='highlight_sort'),
    url('^(\d+)/highlight/(\w+)/(\d+)/delete/$', highlight_delete_view, name='highlight_delete'),
    
    url('^stand-alone/(\w+)/$', stand_alone_view, name='stand_alone'),
    url('^stand-alone/(\w+)/add/$', stand_alone_add_view, name='stand_alone_add'),
    url('^stand-alone/(\w+)/(\d+)/$', stand_alone_detail_view, name='stand_alone_detail'),
    url('^stand-alone/(\w+)/(\d+)/sort/$', stand_alone_sort_view, name='stand_alone_sort'),
    url('^stand-alone/(\w+)/(\d+)/delete/$', stand_alone_delete_view, name='stand_alone_delete'),
]
