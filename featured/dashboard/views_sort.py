from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, redirect, render
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from ..models import Featured, Item

from .views import get_content_type

@csrf_exempt
def sort_view(request, featured_id, code):
    featured = Featured.pull(None, featured_id)
    if featured is None:
        raise Http404
    content_type = get_content_type(code)

    if request.method == 'POST':
        action = request.POST.get('action')
        item = get_object_or_404(Item, featured=featured, id=request.POST.get('item'))
        if action == 'up':
            swap = featured.item_set.filter(content_type=content_type,
                                            sort__lt=item.sort) \
                                    .exclude(id=item.id) \
                                    .order_by('-sort').first()
            if swap is not None:
                swap.sort = item.sort
                swap.save(update_fields=['sort'])
            item.sort -= 1
            item.save(update_fields=['sort'])
        elif action == 'down':
            swap = featured.item_set.filter(content_type=content_type,
                                            sort__gt=item.sort) \
                                    .exclude(id=item.id) \
                                    .first()
            if swap is not None:
                swap.sort = item.sort
                swap.save(update_fields=['sort'])
            item.sort += 1
            item.save(update_fields=['sort'])

    item_list = featured.item_set.filter(content_type=content_type)
    for item in item_list:
        item.get_content()
    return render(request,
                  'featured/dashboard/item_block.html',
                  {'item_list': item_list})
