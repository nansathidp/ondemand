from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect, render
from django.http import Http404
from django.conf import settings

from ..models import Featured, Item, Highlight
from .forms import HighlightForm

from utils.crop import crop_image
from .views_stand_alone import check_code


def stand_alone_delete_view(request, code, highlight_id):
    check_code(code)    
    highlight = Highlight.pull(highlight_id)
    if highlight is None:
        raise Http404

    highlight.delete()
    return redirect(reverse('dashboard:featured:stand_alone', args=[code]))
