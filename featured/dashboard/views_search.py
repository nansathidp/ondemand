from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.http import Http404

from ..models import Featured
from course.models import Course
from program.models import Program
from provider.models import Provider
from .views import get_content_type


@csrf_exempt
def search_view(request, featured_id, code):
    featured = Featured.pull(None, featured_id)
    if featured is None:
        raise Http404
    content_type = get_content_type(code)

    if featured.content_type_id == settings.CONTENT_TYPE('provider', 'provider').id:
        provider = Provider.pull(featured.content)
        if code == 'course':
            content_list = Course.objects.filter(provider=provider)
        elif code == 'program':
            content_list = Program.objects.filter(type=1)
        else:
            raise Http404
    else:
        raise Http404
    if request.method == 'POST':
        q_name = request.POST.get('name', '')
        if len(q_name) > 0:
            content_list = content_list.filter(name__icontains=q_name)
            
    item_list = featured.item_set.filter(content_type=content_type)
    return render(request,
                  'featured/dashboard/search.html',
                  {
                      'provider': provider,
                      'content_list': content_list,
                      'item_list': item_list,
                      'q_name': q_name
                  })
