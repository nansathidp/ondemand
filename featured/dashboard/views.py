from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render
from django.views.decorators.csrf import csrf_exempt
from django.http import Http404

from featured.models import Featured

from utils.breadcrumb import get_breadcrumb


def get_content_type(code):
    if code == 'course':
        return settings.CONTENT_TYPE('course', 'course')
    elif code == 'program':
        return settings.CONTENT_TYPE('program', 'program')
    else:
        raise Http404


def home_view(request, featured_id, code):
    featured = Featured.pull(None, featured_id)
    if featured is None:
        raise Http404
    content_type = get_content_type(code)

    item_list = featured.item_set.filter(content_type=content_type)
    count = 0
    for item in item_list:
        count += 1
        item.get_content()
        if item.sort != count:
            item.sort = count
            item.save(update_fields=['sort'])

    if featured.content_type_id == settings.CONTENT_TYPE('provider', 'provider').id:
        request.is_provider = True
        sidebar = 'provider'
    else:
        request.is_provider = False
        sidebar = ''
    request.code = code
    request.featured = featured
    return render(request,
                  'featured/dashboard/home.html',
                  {
                      'SIDEBAR': sidebar,
                      'TAB': code,
                      'BREADCRUMB_LIST': [], #get_breadcrumb_old(request),
                      'featured': featured,
                      'code': code,
                      'item_list': item_list
                  })
