from django.conf import settings
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect, render

from utils.crop import crop_image
from .forms import HighlightForm
from ..models import Featured, Item, Highlight


def highlight_detail_view(request, featured_id, code, item_id):
    featured = Featured.pull(None, featured_id)
    if featured is None:
        raise Http404
    
    item = get_object_or_404(Item, id=item_id)
    
    highlight = Highlight.pull(item.content)
    if highlight is None:
        raise Http404
    
    #provider = get_object_or_404(Provider, id=provider_id)
    #if not provider.check_access(request):
    #    raise PermissionDenied

    #if not request.user.has_perm('provider.change_provider', group=request.DASHBOARD_GROUP):
    #    raise PermissionDenied

    if request.method == 'POST':
        highlight_form = HighlightForm(request.POST, request.FILES, instance=highlight)
        if highlight_form.is_valid():
            highlight = highlight_form.save()
            crop_image(highlight, request)
            featured.add_item(settings.CONTENT_TYPE('featured', 'highlight'),
                              highlight.id)
            return redirect('dashboard:featured:highlight', featured.id, code)
    else:
        highlight_form = HighlightForm(instance=highlight)
        
    if code.find('provider_') == 0:
        sidebar = 'provider'
        request.is_provider = True
        request.title = code.replace('provider_', '')
    else:
        sidebar = code
        request.is_provider = False
        request.title = code
    request.code = code
    request.featured = featured
    return render(request,
                  'featured/dashboard/highlight_add.html',
                  {
                      'SIDEBAR': sidebar,
                      'BREADCRUMB_LIST': [],
                      'highlight_form': highlight_form,
                      'highlight': highlight
                  })
