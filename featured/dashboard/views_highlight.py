from django.conf import settings
from django.http import Http404
from django.shortcuts import render

from utils.paginator import paginator
from ..models import Featured, Highlight


def highlight_view(request, featured_id, code):

    # FIXME @kidsdev
    #if not request.user.has_perm('highlight.view_webbanner', group=request.DASHBOARD_GROUP):
    #    raise PermissionDenied

    featured = Featured.pull(None, featured_id)
    if featured is None:
        raise Http404

    item_list = featured.item_set.filter(content_type=settings.CONTENT_TYPE('featured', 'highlight'),
                                         content__in=Highlight.objects.values_list('id', flat=True).filter(code=code))
    item_list = paginator(request, item_list)
    for item in item_list:
        item.get_content()

    if code.find('provider_') == 0:
        sidebar = 'provider'
        request.is_provider = True
        request.title = code.replace('provider_', '')
    else:
        sidebar =  code
        request.is_provider = False
        request.title = code
    request.code = code
    request.featured = featured
    return render(request,
                  'featured/dashboard/highlight.html',
                  {
                      'SIDEBAR': sidebar,
                      'TAB': code,
                      'BREADCRUMB_LIST': [], #get_breadcrumb_old(request),
                      'featured': featured,
                      'code': code,
                      'item_list': item_list
                  })
