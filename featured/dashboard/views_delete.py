from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, redirect, render
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.http import Http404

from progress.models import Progress
from ..models import Featured

from .views import get_content_type


@csrf_exempt
def delete_view(request, featured_id, code):
    featured = Featured.pull(None, featured_id)
    if featured is None:
        raise Http404
    content_type = get_content_type(code)

    if request.method == 'POST':
        try:
            featured.item_set.filter(content_type=content_type,
                                     id=int(request.POST.get('item'))).delete()
        except:
            pass

    item_list = featured.item_set.filter(content_type=content_type)
    for item in item_list:
        item.get_content()
    return render(request,
                  'featured/dashboard/item_block.html',
                  {'item_list': item_list})
