from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect, render
from django.http import Http404
from django.conf import settings
from ..models import Featured, Highlight
from .forms import HighlightStandAloneForm
from django.conf import settings
from .views_stand_alone import check_code
from utils.crop import crop_image
from utils.breadcrumb import get_breadcrumb

TARGET = ['course', 'task', 'program', 'question']


def stand_alone_add_view(request, code):
    check_code(code)
    # provider = get_object_or_404(Provider, id=provider_id)
    # if not provider.check_access(request):
    #    raise PermissionDenied

    # if not request.user.has_perm('provider.change_provider', group=request.DASHBOARD_GROUP):
    #    raise PermissionDenied

    if request.method == 'POST':
        highlight_form = HighlightStandAloneForm(request.POST, request.FILES, instance=Highlight(code=code))
        if highlight_form.is_valid():
            highlight = highlight_form.save()
            item = find_target(highlight.url, settings.SITE_URL)
            if item['status']:
                highlight.target = item['target']
                highlight.content = item['item']
                highlight.save()
            else:
                item_url = pack_url(highlight.target, highlight.content)
                if item_url['status']:
                    highlight.url = item_url['url']
                    highlight.save()
                else:
                    highlight.url = item['item']
                    highlight.save()
            crop_image(highlight, request)
            return redirect('dashboard:featured:stand_alone', code)
    else:
        highlight_form = HighlightStandAloneForm(code=code)

    request.code = code
    return render(request,
                  'featured/dashboard/stand_alone_add.html',
                  {
                      'SIDEBAR': code,
                      'BREADCRUMB_LIST': [],  # get_breadcrumb_old(request),
                      'highlight_form': highlight_form,
                      'code': code
                  })


def find_target(path_url, site):
    pack_item = {}
    if path_url.find(site) != -1:
        path = path_url[len(site) + 1:]
        for target_item in TARGET:
            if path.find(target_item) > -1:
                pack_item['item'] = path[len(target_item) + 1:-1]
                if pack_item['item'] == '':
                    pack_item['item'] = path[len(target_item) + 1:]
                pack_item['target'] = target_item
                pack_item['status'] = True
    else:
        if path_url.find('http://') > -1:
            pack_item = {'target': 'link', 'item': path_url, "status": False}
        elif path_url.find('https://') > -1:
            pack_item = {'target': 'link', 'item': path_url, "status": False}
        else:
            pack_item = {'target': 'link', 'item': 'http://' + path_url, "status": False}
    return pack_item


def pack_url(target, id):
    result = {}
    if target != 'link' and target != '':
        result['url'] = '' #settings.SITE_URL + reverse(target + ':detail', args=[id])
        result['status'] = True
    else:
        result['status'] = False
    return result
