from django.shortcuts import render
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import Http404
from django.conf import settings

from ..models import Highlight

from utils.paginator import paginator
from utils.breadcrumb import get_breadcrumb

def check_code(code):
    if code in ['announcement', 'banner']:
        pass
    else:
        raise Http404

def stand_alone_view(request, code):
    # FIXME @kidsdev
    #if not request.user.has_perm('highlight.view_webbanner', group=request.DASHBOARD_GROUP):
    #    raise PermissionDenied

    check_code(code)

    highlight_list = Highlight.objects.filter(code=code)
    highlight_list = paginator(request, highlight_list)
    count = 0
    for _ in highlight_list:
        count += 1
        if _.sort != count:
            _.sort = count
            _.save(update_fields=['sort'])

    request.code = code
    return render(request,
                  'featured/dashboard/stand_alone.html',
                  {
                      'SIDEBAR': code,
                      'BREADCRUMB_LIST': [{'title': '%s Management'%code.title(),
                                           'is_active': False,
                                           'url': reverse('dashboard:featured:stand_alone', args=[code])}],
                      'code': code,
                      'highlight_list': highlight_list
                  })
