from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect, render
from django.http import Http404
from django.conf import settings

from ..models import Featured, Item, Highlight
from .forms import HighlightForm

from utils.crop import crop_image


def highlight_delete_view(request, featured_id, code, item_id):
    featured = Featured.pull(None, featured_id)
    if featured is None:
        raise Http404

    item = get_object_or_404(Item, id=item_id)
    
    highlight = Highlight.pull(item.content)
    if highlight is None:
        raise Http404

    highlight.delete()
    item.delete()
    return redirect(reverse('dashboard:featured:highlight', args=[featured.id, code]))
