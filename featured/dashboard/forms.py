from django import forms
from django.conf import settings

from ..models import Highlight


class HighlightForm(forms.ModelForm):

    target = forms.CharField(
        required=False,
        widget=forms.Select(choices=(('', 'Unlink'),
                                     ('course', 'Course'),
                                     ('question', 'Test'),
                                     ('program', 'Program'),
                                     ('task', 'TO-DO'),
                                     ('link', 'External Link'))),
    )

    class Meta:
        model = Highlight
        fields = ['title', 'image', 'target', 'content', 'url', 'is_display']
        labels = {'is_display': 'แสดงบนหน้าเว็บ?<br />Display',
                  'target': 'Link to'
                  }
        help_texts = {'name': 'Limit at 120 characters',
                      'image': settings.HELP_TEXT_IMAGE('banner'),
                      }


class HighlightStandAloneForm(forms.ModelForm):

    target = forms.CharField(
        required=False,
        widget=forms.Select(choices=(('', 'Unlink'),
                                     ('course', 'Course'),
                                     ('question', 'Test'),
                                     ('program', 'Program'),
                                     ('task', 'TO-DO'),
                                     ('link', 'External Link'))),
    )
    
    class Meta:
        model = Highlight
        fields = ['title', 'desc', 'image', 'target', 'content', 'url', 'is_display']
        labels = {'is_display': 'Display',
                  'target': 'Link to'}

        help_texts = {'name': 'Limit at 120 characters',
                      'image': settings.HELP_TEXT_IMAGE('banner'),
                      }

    def __init__(self, *args, **kwargs):
        code = kwargs.get('code', None)
        if code is not None:
            del(kwargs['code'])
        super(HighlightStandAloneForm, self).__init__(*args, **kwargs)
        if code is not None:
            if code == 'banner':
                del(self.fields['desc'])
