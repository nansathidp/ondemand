from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.conf import settings


from ..models import Featured, Item, Highlight

from .views_stand_alone import check_code


def stand_alone_sort_view(request, code, highlight_id):
    check_code(code)    
    highlight = Highlight.pull(highlight_id)
    if highlight is None:
        raise Http404

    #if not request.user.has_perm('highlight.change_webbanner',
    #                             group=request.DASHBOARD_GROUP):
    #    raise PermissionDenied

    move = request.GET.get('move', None)
    if move == 'up':
        highlight_swap = Highlight.objects.filter(code=code,
                                                  sort__lt=highlight.sort) \
                                          .exclude(id=highlight.id) \
                                          .order_by('-sort').first()
        if highlight_swap is not None:
            highlight_swap.sort = highlight.sort
            highlight_swap.save(update_fields=['sort'])
        highlight.sort -= 1
        highlight.save(update_fields=['sort'])
    elif move == 'down':
        highlight_swap = Highlight.objects.filter(code=code,
                                                  sort__gt=highlight.sort) \
                                .exclude(id=highlight.id) \
                                .order_by('sort').first()
        if highlight_swap is not None:
            highlight_swap.sort = highlight.sort
            highlight_swap.save(update_fields=['sort'])
        highlight.sort += 1
        highlight.save(update_fields=['sort'])
    return redirect('dashboard:featured:stand_alone', code)
