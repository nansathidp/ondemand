from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.http import Http404

from course.models import Course
from program.models import Program
from ..models import Featured

from .views import get_content_type


@csrf_exempt
def search_add_view(request, featured_id, code):
    featured = Featured.pull(None, featured_id)
    if featured is None:
        raise Http404
    content_type = get_content_type(code)


    if request.method == 'POST':
        try:
            content = int(request.POST.get('content', -1))
            if code == 'course':
                content = Course.pull(content)
            elif code == 'program':
                content = Program.pull(content)
            else:
                raise Http404
            item = featured.add_item(content_type,
                                     content.id)
        except:
            raise
            content = None

        html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green search-delete-item" item="%s" style="cursor: pointer"></i>'%item.id
    else:
        html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-red search-delete-item"></i>'
    return JsonResponse({'html': html})
