from django.core.exceptions import PermissionDenied
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from ..models import Featured

from .views import get_content_type


@csrf_exempt
def search_delete_view(request, featured_id, code):
    featured = Featured.pull(None, featured_id)
    if featured is None:
        raise Http404
    content_type = get_content_type(code)


    html = ''
    if request.method == 'POST':
        try:
            item = featured.item_set.filter(content_type=settings.CONTENT_TYPE('course', 'course'),
                                            id=int(request.POST.get('item'))).first()
            html = '<i class="zmdi zmdi-plus-circle-o zmdi-hc-fw insert-content" content="%s" style="cursor: pointer"></i>'%(item.content)
            item.delete()
        except:
            html = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-red"></i>'
            return JsonResponse({'html': html})

    return JsonResponse({'html': html})
