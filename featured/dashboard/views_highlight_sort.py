from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.conf import settings


from ..models import Featured, Item, Highlight


def highlight_sort_view(request, featured_id, code, item_id):
    featured = Featured.pull(None, featured_id)
    if featured is None:
        raise Http404
    
    item = get_object_or_404(Item, id=item_id)
    
    highlight = Highlight.pull(item.content)
    if highlight is None:
        raise Http404

    #if not request.user.has_perm('highlight.change_webbanner',
    #                             group=request.DASHBOARD_GROUP):
    #    raise PermissionDenied

    move = request.GET.get('move', None)
    if move == 'up':
        item_swap = Item.objects.filter(featured=featured,
                                        content_type=settings.CONTENT_TYPE('featured', 'highlight'),
                                        sort__lt=item.sort) \
                                .exclude(id=item.id) \
                                .order_by('-sort').first()
        if item_swap is not None:
            item_swap.sort = item.sort
            item_swap.save(update_fields=['sort'])
        item.sort -= 1
        item.save(update_fields=['sort'])
    elif move == 'down':
        item_swap = Item.objects.filter(featured=featured,
                                        content_type=settings.CONTENT_TYPE('featured', 'highlight'),
                                        sort__gt=item.sort) \
                                .exclude(id=item.id) \
                                .order_by('sort').first()
        if item_swap is not None:
            item_swap.sort = item.sort
            item_swap.save(update_fields=['sort'])
        item.sort += 1
        item.save(update_fields=['sort'])
    return redirect('dashboard:featured:highlight', featured.id, code)
