from django import template
from django.utils.safestring import mark_safe

register = template.Library()

@register.simple_tag
def item_check_tag(item_list, content_id):
    is_found = False
    item_id = None
    for item in item_list:
        if item.content == content_id:
            is_found = True
            item_id = item.id
            break

    if is_found:
        result = '<i class="zmdi zmdi-check-circle zmdi-hc-fw text-green search-delete-item" item="%s" style="cursor: pointer"></i>'%(item_id)
    else:
        result = '<i class="zmdi zmdi-plus-circle-o zmdi-hc-fw insert-content" content="%s" style="cursor: pointer"></i>'%(content_id)
    return mark_safe(result)
