from django.conf import settings
from django.core.cache import cache
from .models import Highlight

from utils.cached.time_out import get_time_out

#
def cached_announcement(is_force=False):
    code = 'announcement'
    key = '%s_highlight_%s' % (settings.CACHED_PREFIX, code)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Highlight.objects.filter(code=code,
                                          is_display=True).order_by('timestamp').first()
        if result is None:
            result = -1
        cache.set(key, result, get_time_out())
    return None if result == -1 else result

#
# def cached_announcement_delete():
#     code = 'announcement'
#     key = '%s_highlight_%s' % (settings.CACHED_PREFIX, code)
#     cache.delete(key)
#
#
# def cached_banner(is_force=False):
#     code = 'banner'
#     key = '%s_highlight_%s' % (settings.CACHED_PREFIX, code)
#     result = None if is_force else cache.get(key)
#     if result is None:
#         result = Highlight.objects.filter(
#             code=code,
#             is_display=True).order_by('timestamp').first()
#         if result is None:
#             result = -1
#         cache.set(key, result, get_time_out())
#     return None if result == -1 else result
