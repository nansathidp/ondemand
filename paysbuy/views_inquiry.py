from django.shortcuts import get_object_or_404, redirect

from order.models import Order
from order.views_status import paysbuy_success


def inquiry_views(request, order_id):
    order = get_object_or_404(Order, id=order_id)
    if order.method in [1, 2, 3, 5]:
        paysbuy_success(request, order)
    return redirect('order:history')
