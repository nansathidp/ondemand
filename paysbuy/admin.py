from django.contrib import admin
from paysbuy.models import *


@admin.register(Request)
class RequestAdmin(admin.ModelAdmin):
    list_display = ('order', 'type', 'step', 'result', 'timestamp')
    search_fields = ['order__id', 'order__account__email']


@admin.register(Response)
class ResponseAdmin(admin.ModelAdmin):
    list_display = ('order', 'approve_code', 'amt', 'status', 'method', 'type', 'timestamp')
    list_filter = ('type', 'status')
    search_fields = ['order__id', 'order__account__email', 'approve_code']
