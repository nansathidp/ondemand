from django.db import models

REQUEST_TYPE = (
    (1, 'Web'),
    (2, 'Mobile API'),
    (3, 'Mobile WebView'),
)

REQUEST_STEP = (
    (1, 'Request Pay'),
    (2, 'Check Pay Success'),
    (3, 'Response Back RE-Check'),
)

REQUEST_RESULT = (
    (0, '-'),
    (20, 'Success'),
    (21, 'Except Search Result'),
    (22, 'Result != 00'),
)

RESPONSE_TYPE = (
    (1, 'Front'),
    (2, 'Back'),
    (3, 'Front (Mobile WebView)'),
)

RESPONSE_CODE = (
    (-1, '-'),
    (0, 'Success'),
    (2, 'In progress'),
    (9, 'Incomplete'),
)

PAYSBUY_METHOD = (
    (-1, '-'),
    (1, 'Paysbuy'),
    (2, 'Credit'),
    (3, 'Paypal'),
    (4, 'American express'),
    (5, 'Online Banking'),
    (6, 'Cash'),
    (8, 'Smart purse'),
    (9, 'Pay in installments'),
)


class Request(models.Model):
    order = models.ForeignKey('order.Order')
    request = models.TextField()
    response = models.TextField(blank=True)
    type = models.IntegerField(choices=REQUEST_TYPE)
    step = models.IntegerField(choices=REQUEST_STEP)
    result = models.IntegerField(choices=REQUEST_RESULT, default=0)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']


class Response(models.Model):
    text = models.TextField(blank=True)
    order = models.ForeignKey('order.Order', blank=True, null=True)
    approve_code = models.CharField(max_length=8, blank=True, null=True)
    amt = models.FloatField(default=0.0)
    status = models.IntegerField(choices=RESPONSE_CODE, default=-1)
    type = models.IntegerField(choices=RESPONSE_TYPE)
    method = models.IntegerField(choices=PAYSBUY_METHOD, default=-1)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']
