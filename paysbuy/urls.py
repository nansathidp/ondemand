from django.conf.urls import url

#TODO: migrate url format to from .views_xxx import xxx_view :)
from . import views_inquiry

app_name = 'paysbuy'
urlpatterns = [
    url(r'^(\d+)/inquiry/$', views_inquiry.inquiry_views, name='inquiry'), #TODO: testing
]
