import requests
payload = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.sso.gsso/">
  <soapenv:Header/>
  <soapenv:Body>
    <ws:confirmOneTimePW_PassKey>
      <msisdn>66877975636</msisdn>
      <pwd></pwd>
      <transactionID></transactionID>
    </ws:confirmOneTimePW_PassKey>
  </soapenv:Body>
</soapenv:Envelope>
"""
r = requests.post("https://ws2-carrierbill.ais.co.th/directcharging/gssoweb", data=payload, verify=False)

print '------'
print r.text
print '------'
