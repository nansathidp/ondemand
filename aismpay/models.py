from django.db import models
from django.conf import settings

IS_AISMPAY_PRODUCTION = True

MPAY_SERVER = (
    (0, 'Dev'),
    (1, 'Production')
)

NETWORK_TYPE = (
    (1, 'Wi-Fi->OTP'),
    (2, 'AIS'),
)

SOURCE = (
    (1, 'API'),
    (2, 'Web'),
    (3, 'WebView'),
)


class Level(models.Model):
    service = models.CharField(max_length=30)
    volume = models.IntegerField(db_index=True)

    @staticmethod
    def pull(content_type, content):
        from django.conf import settings

        content_type_order = settings.CONTENT_TYPE('order', 'order')

        if content_type == content_type_order:
            volume = int(content.net*100)
            try:
                level = Level.objects.get(volume=volume)
                return level
            except:
                return None
        else:
            return None


class Price(models.Model):
    level = models.ForeignKey(Level)
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='aismpay_price_set')
    content = models.BigIntegerField(db_index=True)


class OtpRequest(models.Model):
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    tel = models.CharField(max_length=24)
    tel_input = models.CharField(max_length=24)
    content_type = models.ForeignKey('contenttypes.ContentType')
    content = models.BigIntegerField(db_index=True)
    source = models.IntegerField(choices=SOURCE, default=1)
    server = models.IntegerField(choices=MPAY_SERVER)
    request = models.TextField()
    response = models.TextField(blank=True)
    transaction_id = models.CharField(max_length=120)
    status = models.CharField(max_length=120)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        ordering = ['-timestamp']
    
    @staticmethod
    def send(account, tel, tel_input, content_type, content):
        import re, requests, traceback
        payload = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.sso.gsso/">
  <soapenv:Header/>
  <soapenv:Body>
    <ws:sendOneTimePW>
      <msisdn>%s</msisdn>
      <emailAddr></emailAddr>
      <otpChannel>sms</otpChannel>
      <service>DirectCharging</service>
      <accountType>all</accountType>
      <addTimeoutMins>15</addTimeoutMins>
    </ws:sendOneTimePW>
  </soapenv:Body>
</soapenv:Envelope>"""%(tel)
        otp_request = OtpRequest.objects.create(account=account,
                                                tel=tel,
                                                tel_input=tel_input,
                                                content_type=content_type,
                                                content=content,
                                                server=1 if IS_AISMPAY_PRODUCTION else 0,
                                                request=payload,
                                                transaction_id='',
                                                status='-')
        try:
            if IS_AISMPAY_PRODUCTION:
                requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'
                try:
                    requests.packages.urllib3.contrib.pyopenssl.DEFAULT_SSL_CIPHER_LIST += 'HIGH:!DH:!aNULL'
                except AttributeError:
                    # no pyopenssl support used / needed / available
                    pass
                r = requests.post("https://ws1-carrierbill.ais.co.th/directcharging/gssoweb", data=payload, timeout=60.0, verify=False)
            else:
                r = requests.post("https://ws2-carrierbill.ais.co.th/directcharging/gssoweb", data=payload, timeout=60.0, verify=False)
            otp_request.response = r.text
        except:
            otp_request.response = traceback.format_exc()
            otp_request.status = 'Request Fail'
            otp_request.save(update_fields=['response', 'status'])
            return False, otp_request
        try:
            status = re.search('<code>([0-9]*)</code>', r.text).group(1)
            msg = re.search('<description>([^*]*)</description>', r.text).group(1)
            otp_request.status = '%s:%s'%(status, msg)
            if status == '000':
                otp_request.transaction_id = re.search('<transactionID>([0-9]*)</transactionID>', r.text).group(1)
                is_success = True
            else:
                is_success = False
        except:
            otp_request.response += '\n---\n'+traceback.format_exc()
            is_success = False
            otp_request.status = 'except'
        otp_request.save(update_fields=['response', 'transaction_id', 'status'])
        return is_success, otp_request


class OtpConfirm(models.Model):
    otp_request = models.ForeignKey(OtpRequest, null=True)
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    tel = models.CharField(max_length=24)
    otp = models.CharField(max_length=8)
    source = models.IntegerField(choices=SOURCE, default=1)
    server = models.IntegerField(choices=MPAY_SERVER)
    request = models.TextField()
    response = models.TextField(blank=True)
    pass_key = models.CharField(max_length=200)
    status = models.CharField(max_length=120)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        ordering = ['-timestamp']
    
    @staticmethod
    def send(otp_request, otp):
        import re, requests, traceback
        payload = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.sso.gsso/">
  <soapenv:Header/>
  <soapenv:Body>
    <ws:confirmOneTimePW_PassKey>
      <msisdn>%s</msisdn>
      <pwd>%s</pwd>
      <transactionID>%s</transactionID>
    </ws:confirmOneTimePW_PassKey>
  </soapenv:Body>
</soapenv:Envelope>"""%(otp_request.tel, otp, otp_request.transaction_id)
        otp_confirm = OtpConfirm(otp_request=otp_request,
                                 account=otp_request.account,
                                 tel=otp_request.tel,
                                 otp=otp,
                                 server=1 if IS_AISMPAY_PRODUCTION else 0,
                                 request=payload,
                                 pass_key='',
                                 status='-')
        otp_confirm.save()
        try:
            if IS_AISMPAY_PRODUCTION:
                requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'
                try:
                    requests.packages.urllib3.contrib.pyopenssl.DEFAULT_SSL_CIPHER_LIST += 'HIGH:!DH:!aNULL'
                except AttributeError:
                    # no pyopenssl support used / needed / available
                    pass
                r = requests.post("https://ws1-carrierbill.ais.co.th/directcharging/gssoweb", data=payload, timeout=60.0, verify=False)
            else:
                r = requests.post("https://ws2-carrierbill.ais.co.th/directcharging/gssoweb", data=payload, timeout=60.0, verify=False)
            otp_confirm.response = r.text
        except:
            otp_confirm.response = traceback.format_exc()
            otp_confirm.status = 'Request Fail'
            otp_confirm.save(update_fields=['response', 'status'])
            return False, otp_confirm
        
        try:
            status = re.search('<code>([0-9]*)</code>', r.text).group(1)
            msg = re.search('<description>([^*]*)</description>', r.text).group(1)
            otp_confirm.status = '%s:%s'%(status, msg)
            if status == '000':
                otp_confirm.pass_key = re.search('<passKey>([^*]*)</passKey>', r.text).group(1)    
                is_success = True
            else:
                is_success = False
        except:
            otp_confirm.response += '\n---\n'+traceback.format_exc()
            is_success = False
            otp_confirm.status = 'except'
        otp_confirm.save(update_fields=['response', 'pass_key', 'status'])
        return is_success, otp_confirm


class ReserveVolume(models.Model):
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    type = models.IntegerField(choices=NETWORK_TYPE)
    otp_confirm = models.ForeignKey(OtpConfirm, null=True)
    content_type = models.ForeignKey('contenttypes.ContentType')
    content = models.BigIntegerField(db_index=True)
    tel = models.CharField(max_length=24)
    service_number = models.CharField(max_length=120)
    volume = models.CharField(max_length=120)
    transaction_id = models.CharField(max_length=12)
    info = models.CharField(max_length=120)
    pass_key = models.CharField(max_length=200)
    source = models.IntegerField(choices=SOURCE, default=1)
    server = models.IntegerField(choices=MPAY_SERVER)
    request = models.TextField()
    captcha_id = models.CharField(max_length=120)
    captcha_url = models.CharField(max_length=512)
    response = models.TextField(blank=True)
    result = models.CharField(max_length=120)
    status = models.CharField(max_length=12)
   
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return str(self.id)
    
    class Meta:
        ordering = ['-timestamp']
        
    @staticmethod
    def send(account, type, content_type, content, tel, volume, service_number, amount, transaction_id, pass_key, info, otp_confirm=None):
        import re, requests, traceback
        payload = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:loc="http://www.csapi.org/schema/parlayx/payment/reserve_volume_charging/v3_1/local">
  <soapenv:Header/>
  <soapenv:Body>
    <loc:reserveVolume>
      <loc:endUserIdentifier>%s</loc:endUserIdentifier>
      <loc:volume>%s</loc:volume>
      <loc:billingText>test</loc:billingText>
      <loc:parameters>
        <name>Service_Number</name>
        <value>%s</value>
      </loc:parameters>
      <loc:parameters>
        <name>amount</name>
        <value>%s</value>
      </loc:parameters>
      <loc:parameters>
        <name>Tax_Dev</name>
        <value><![CDATA[0]]></value>
      </loc:parameters>
      <loc:parameters>
        <name>Service_Type</name>
        <value>Download</value>
      </loc:parameters>
      <loc:parameters>
        <name>Transaction_ID</name>
        <value>%s</value>
      </loc:parameters>
      <loc:parameters>
        <name>PassKey</name>
        <value><![CDATA[%s]]></value>
      </loc:parameters>
      <loc:parameters>
        <name>product_Info</name>
        <value><![CDATA[%s]]></value>
      </loc:parameters>
    </loc:reserveVolume>
  </soapenv:Body>
</soapenv:Envelope>
"""%(tel, volume, service_number, amount, transaction_id, pass_key, info)
        reserve_volume = ReserveVolume(account=account,
                                       type=type,
                                       otp_confirm=otp_confirm,
                                       content_type=content_type,
                                       content=content,
                                       tel=tel,
                                       service_number=service_number,
                                       volume=volume,
                                       transaction_id=transaction_id,
                                       info=info,
                                       pass_key=pass_key,
                                       server=1 if IS_AISMPAY_PRODUCTION else 0,
                                       request=payload,
                                       result='',
                                       captcha_id='',
                                       captcha_url='',
                                       status='-')
        reserve_volume.save()
        try:
            if IS_AISMPAY_PRODUCTION:
                requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'
                try:
                    requests.packages.urllib3.contrib.pyopenssl.DEFAULT_SSL_CIPHER_LIST += 'HIGH:!DH:!aNULL'
                except AttributeError:
                    # no pyopenssl support used / needed / available
                    pass
                r = requests.post("https://ws1-carrierbill.ais.co.th/directcharging/services/reserveVolumeCharging", data=payload, timeout=60.0, verify=False)
            else:
                r = requests.post("https://ws2-carrierbill.ais.co.th/directcharging/services/reserveVolumeCharging", data=payload, timeout=60.0, verify=False)
            reserve_volume.response = r.text
        except:
            reserve_volume.response = traceback.format_exc()
            reserve_volume.status = 'Request Fail'
            reserve_volume.save(update_fields=['response', 'status'])
            return False, reserve_volume
        try:
            reserve_volume.result = re.search('<ns2:result>([^*]*)</ns2:result>', r.text).group(1)
            if reserve_volume.result.find('SUCCESS') != -1 or reserve_volume.result.find('AOC') != -1:
                is_success = True
                reserve_volume.captcha_id = reserve_volume.result.split('|')[1]
                reserve_volume.captcha_url = reserve_volume.result.split('|')[2]
                reserve_volume.result = reserve_volume.result.split('|')[0]
                reserve_volume.status = 'ok'
            else:
                is_success = False
                reserve_volume.status = 'fail'
        except:
            reserve_volume.response += '\n---\n'+traceback.format_exc()
            is_success = False
            reserve_volume.status = 'except'
        reserve_volume.save(update_fields=['response', 'result', 'captcha_id', 'captcha_url', 'status'])
        return is_success, reserve_volume


class ConfirmCaptha(models.Model):
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    reserve_volume = models.ForeignKey(ReserveVolume, null=True)
    tel = models.CharField(max_length=24)
    captcha_id = models.CharField(max_length=120)
    captcha_password = models.CharField(max_length=12)
    volume = models.CharField(max_length=120)
    pass_key = models.CharField(max_length=200)
    source = models.IntegerField(choices=SOURCE, default=1)
    server = models.IntegerField(choices=MPAY_SERVER)
    request = models.TextField()
    response = models.TextField(blank=True)
    result = models.CharField(max_length=120)
    purchase_id = models.CharField(max_length=120)
    status = models.CharField(max_length=12)

    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']
        
    @staticmethod
    def send(reserve_volume, captcha_password):
        import re, requests, traceback
        payload = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:loc="http://www.csapi.org/schema/parlayx/payment/reserve_volume_charging/v3_1/local">
  <soapenv:Header/>
  <soapenv:Body>
    <loc:reserveVolume>
      <loc:endUserIdentifier>%s</loc:endUserIdentifier>      
      <loc:volume>%s</loc:volume>
      <loc:billingText>test</loc:billingText>
      <loc:parameters>
        <name>Service_Number</name>
        <value>%s</value>
      </loc:parameters>
      <loc:parameters>
        <name>amount</name>
        <value>%s</value>
      </loc:parameters>
      <loc:parameters>
        <name>Tax_Dev</name>
        <value><![CDATA[0]]></value>
      </loc:parameters>
      <loc:parameters>
        <name>Service_Type</name>
        <value>AOC confirm</value>
      </loc:parameters>
      <loc:parameters>
        <name>Transaction_ID</name>
        <value>%s</value>
      </loc:parameters>
      <loc:parameters>
        <name>PassKey</name>
        <value><![CDATA[%s]]></value>
      </loc:parameters>
      <loc:parameters>
        <name>product_Info</name>
        <value><![CDATA[%s]]></value>
      </loc:parameters>
      <loc:parameters>
        <name>Captcha_ID</name>
        <value>%s</value>
      </loc:parameters>
      <loc:parameters>
        <name>Captcha_Password</name>
        <value>%s</value>
      </loc:parameters>
    </loc:reserveVolume>
  </soapenv:Body>
</soapenv:Envelope>"""%(reserve_volume.tel, reserve_volume.volume, reserve_volume.service_number, reserve_volume.volume, reserve_volume.transaction_id, reserve_volume.pass_key, reserve_volume.info, reserve_volume.captcha_id, captcha_password)
        confirm_captha = ConfirmCaptha(account=reserve_volume.account,
                                       reserve_volume=reserve_volume,
                                       tel=reserve_volume.tel,
                                       captcha_id=reserve_volume.captcha_id,
                                       captcha_password=captcha_password,
                                       volume=reserve_volume.volume,
                                       pass_key=reserve_volume.pass_key,
                                       server=1 if IS_AISMPAY_PRODUCTION else 0,
                                       request=payload,
                                       result='',
                                       status='-')
        confirm_captha.save()
        try:
            if IS_AISMPAY_PRODUCTION:
                requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'
                try:
                    requests.packages.urllib3.contrib.pyopenssl.DEFAULT_SSL_CIPHER_LIST += 'HIGH:!DH:!aNULL'
                except AttributeError:
                    # no pyopenssl support used / needed / available
                    pass
                r = requests.post("https://ws1-carrierbill.ais.co.th/directcharging/services/reserveVolumeCharging", data=payload, timeout=60.0, verify=False)
            else:
                r = requests.post("https://ws2-carrierbill.ais.co.th/directcharging/services/reserveVolumeCharging", data=payload, timeout=60.0, verify=False)
            confirm_captha.response = r.text
        except:
            confirm_captha.response = traceback.format_exc()
            confirm_captha.status = 'Request Fail'
            confirm_captha.save(update_fields=['response', 'status'])
            return False, confirm_captha
        try:
            confirm_captha.result = re.search('<ns2:result>([^*]*)</ns2:result>', r.text).group(1)
            if confirm_captha.result.find('SUCCESS') != -1:
                is_success = True
                confirm_captha.purchase_id = confirm_captha.result.split('|')[-1]
                confirm_captha.result = confirm_captha.result.split('|')[0]
                confirm_captha.status = 'ok'
            else:
                is_success = False
                confirm_captha.status = 'fail'
        except:
            confirm_captha.response += '\n---\n'+traceback.format_exc()
            is_success = False
            confirm_captha.status = 'except'
        confirm_captha.save(update_fields=['response', 'result', 'purchase_id', 'status'])
        return is_success, confirm_captha

class ChargeReservation(models.Model):
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    reserve_volume = models.ForeignKey(ReserveVolume, null=True)
    tel = models.CharField(max_length=24)
    pass_key = models.CharField(max_length=200)
    source = models.IntegerField(choices=SOURCE, default=1)
    server = models.IntegerField(choices=MPAY_SERVER)
    request = models.TextField()
    response = models.TextField(blank=True)
    result = models.CharField(max_length=120)
    status = models.CharField(max_length=12)

    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']
        
    @staticmethod
    def send(confirm_captha):
        import re, requests, traceback
        payload = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:loc="http://www.csapi.org/schema/parlayx/payment/reserve_volume_charging/v3_1/local">
  <soapenv:Header/>
  <soapenv:Body>
    <loc:chargeReservation>
      <loc:reservationIdentifier><![CDATA[%s|%s]]></loc:reservationIdentifier>
      <loc:volume>%s</loc:volume>
      <loc:billingText></loc:billingText>
      <loc:referenceCode>%s</loc:referenceCode>
    </loc:chargeReservation>
  </soapenv:Body>
</soapenv:Envelope>
"""%(confirm_captha.pass_key, confirm_captha.purchase_id, confirm_captha.volume, confirm_captha.tel)
        charge_reservation = ChargeReservation(account=confirm_captha.account,
                                               reserve_volume=confirm_captha.reserve_volume,
                                               tel=confirm_captha.tel,
                                               pass_key=confirm_captha.pass_key,
                                               server=1 if IS_AISMPAY_PRODUCTION else 0,
                                               request=payload,
                                               result='',
                                               status='-')
        charge_reservation.save()
        try:
            if IS_AISMPAY_PRODUCTION:
                requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'
                try:
                    requests.packages.urllib3.contrib.pyopenssl.DEFAULT_SSL_CIPHER_LIST += 'HIGH:!DH:!aNULL'
                except AttributeError:
                    # no pyopenssl support used / needed / available
                    pass
                r = requests.post("https://ws1-carrierbill.ais.co.th/directcharging/services/reserveVolumeCharging", data=payload, timeout=60.0, verify=False)
            else:
                r = requests.post("https://ws2-carrierbill.ais.co.th/directcharging/services/reserveVolumeCharging", data=payload, timeout=60.0, verify=False)
            charge_reservation.response = r.text
        except:
            charge_reservation.response = traceback.format_exc()
            charge_reservation.status = 'Request Fail'
            charge_reservation.save(update_fields=['response', 'status'])
            return False, charge_reservation        
        try:
            charge_reservation.result = re.search('<ns2:result>([^*]*)</ns2:result>', r.text).group(1)
            charge_reservation.status = 'ok'
            is_success = True
        except:
            charge_reservation.response += '\n---\n'+traceback.format_exc()
            is_success = False
            charge_reservation.status = 'except'
        charge_reservation.save(update_fields=['response', 'result', 'status'])
        return is_success, charge_reservation


class ResponseBack(models.Model):
    body = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']
