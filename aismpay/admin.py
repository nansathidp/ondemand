from django.contrib import admin
from aismpay.models import *

@admin.register(Level)
class LevelAdmin(admin.ModelAdmin):
    list_display = ('id', 'service', 'volume')
    
@admin.register(Price)
class PriceAdmin(admin.ModelAdmin):
    list_display = ('level', 'content_type', 'content')
    
@admin.register(OtpRequest)
class OtpRequestAdmin(admin.ModelAdmin):
    list_display = ('account', 'tel', 'tel_input', 'content_type', 'content', 'server', 'transaction_id', 'status', 'timestamp')
    readonly_fields = ('id', 'account',)

@admin.register(OtpConfirm)
class OtpConfirmAdmin(admin.ModelAdmin):
    list_display = ('otp_request', 'account', 'otp', 'server', 'pass_key', 'status', 'timestamp')
    readonly_fields = ('id', 'account', 'otp_request')

@admin.register(ReserveVolume)
class ReserveVolumeAdmin(admin.ModelAdmin):
    list_display = ('account', 'type', 'otp_confirm', 'tel', 'service_number', 'volume', 'transaction_id', 'info', 'pass_key', 'server', 'result', 'captcha_id', 'captcha_url', 'status', 'timestamp')
    readonly_fields = ('id', 'account', 'content_type' ,'otp_confirm')
    
@admin.register(ConfirmCaptha)
class ConfirmCapthaAdmin(admin.ModelAdmin):
    list_display = ('account', 'reserve_volume', 'tel', 'captcha_id', 'captcha_password', 'volume', 'pass_key', 'server', 'result', 'purchase_id', 'status', 'timestamp')
    readonly_fields = ('id', 'account', 'reserve_volume')

@admin.register(ChargeReservation)
class ChargeReservationAdmin(admin.ModelAdmin):
    list_display = ('account', 'reserve_volume', 'tel', 'pass_key', 'server', 'result', 'status', 'timestamp')
    readonly_fields = ('id', 'account', 'reserve_volume')

@admin.register(ResponseBack)
class ResponseBackAdmin(admin.ModelAdmin):
    list_display = ('id', 'timestamp')
