# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-11 04:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pool', '0002_pool_provider'),
    ]

    operations = [
        migrations.AddField(
            model_name='pool',
            name='material_type',
            field=models.IntegerField(choices=[(0, 'Video'), (1, 'Web Link'), (2, 'Document'), (3, 'Test'), (4, 'Audio')], default=-1),
        ),
    ]
