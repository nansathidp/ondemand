import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings

from pool.models import Pool
from course.models import Course
from account.models import Account

if __name__ == '__main__':
    account = Account.objects.filter(email='nakorn@conicle.com').first()

    for course in Course.objects.all():
        for material in course.material_set.all():
            if material.type == 3:
                continue
            print(material.id)
            pool = Pool.objects.filter(content_type=settings.CONTENT_TYPE('course', 'material'),
                                       content=material.id).first()
            if pool is None:
                Pool.objects.create(provider_id=course.provider_id,
                                    content_type=settings.CONTENT_TYPE('course', 'material'),
                                    content=material.id,
                                    name=material.name,
                                    material_type=material.type)
            else:
                pool.provider_id = course.provider_id
                pool.material_type = material.type
                pool.save()
