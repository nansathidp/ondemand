from rest_framework.response import Response
from rest_framework import serializers, viewsets
from rest_framework import filters
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.decorators import list_route
from django_filters.rest_framework import DjangoFilterBackend
from django.conf import settings

from ..models import Pool

from utils.content_type import get_content_type_name
from utils.content import get_content

from collections import OrderedDict

class PoolSerializer(serializers.ModelSerializer):
    provider_name = serializers.SerializerMethodField()
    content_type_display = serializers.SerializerMethodField()
    content_display = serializers.SerializerMethodField()
    material_type_display = serializers.SerializerMethodField()
    
    class Meta:
        model = Pool
        fields = ('provider', 'provider_name',
                  'content_type', 'content_type_display',
                  'content', 'content_display',
                  'material_type', 'material_type_display',
                  'timestamp')

    def get_provider_name(self, pool):
        return pool.provider.name
    
    def get_content_type_display(self, obj):
        return get_content_type_name(obj.content_type)

    def get_content_display(self, obj):
        content = get_content(obj.content_type_id, obj.content)
        f = getattr(content, 'api_display_title', None)
        if f is None:
            return None
        else:
            return f()

    def get_material_type_display(self, pool):
        return pool.get_material_type_display()
    
# class PoolViesSet(viewsets.ModelViewSet):
class PoolViesSet(viewsets.ReadOnlyModelViewSet):
    queryset = Pool.objects.select_related('provider', 'content_type').all()
    serializer_class = PoolSerializer
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    search_fields = ('name', )
    filter_fields = ('provider', 'content_type', 'material_type')

    queryset_my_material = None
    queryset_pool_material = None
    
    permission_classes_action = {
        # 'create': [IsAdminUser],
        'list': [AllowAny],
        'retrieve': [AllowAny],
        # 'update': [IsAdminUser],
        # 'partial_update': [IsAdminUser],
        # 'destroy': [IsAdminUser]
    }

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    @list_route(url_path='my-material')
    def my_material(self, request):
        queryset = self._queryset_my_material(request)
        
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(url_path='pool-material')
    def pool_material(self, request):
        queryset = self._queryset_pool_material(request)
        
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            _response = self.get_paginated_response(serializer.data)
        else:
            serializer = self.get_serializer(queryset, many=True)
            _response = Response(serializer.data)
            
        _ = self._count(queryset)
        _response.data = OrderedDict(list(_response.data.items()) + list(_.items()))
        return _response

    def _queryset_my_material(self, request):
        if self.queryset_my_material is not None:
            return self.queryset_my_material
        else:
            queryset = self.filter_queryset(self.get_queryset())
            if request._request.user.has_perm('course.view_course',
                                              group=request._request.DASHBOARD_GROUP):
                pass
            elif request._request.user.has_perm('course.view_own_course',
                                                group=request._request.DASHBOARD_GROUP):
                if request._request.DASHBOARD_PROVIDER is not None:
                    queryset = queryset.filter(provider=request._request.DASHBOARD_PROVIDER)
                elif settings.IS_SUB_PROVIDER and request._request.DASHBOARD_SUB_PROVIDER is not None:
                    from subprovider.models import Item
                    queryset = queryset.filter(id__in=Item.objects
                                               .values_list('content', flat=True)
                                               .filter(sub_provider=request._request.DASHBOARD_SUB_PROVIDER,
                                                       content_type=settings.CONTENT_TYPE('course', 'material')))
                else:
                    queryset = Pool.objects.none()
            else:
                queryset = Pool.objects.none()
            self.queryset_my_material = queryset
            return queryset

    def _queryset_pool_material(self, request):
        if self.queryset_pool_material is not None:
            return self.queryset_pool_material
        else:
            queryset = self.filter_queryset(self.get_queryset()).filter(type=1)
            self.queryset_pool_material = queryset
            return queryset
    
    def _count(self, queryset):
        return OrderedDict([
            ('count_my_material', self._queryset_my_material().count()),
            ('count_pool_material', self._queryset_pool_material().count()),
            ('count_vdo', queryset.filter(material_type=0).count()),
            ('count_audio', queryset.filter(material_type=4).count()),
            ('count_document', queryset.filter(material_type=2).count()),
            ('count_external', queryset.filter(material_type=1).count()),
            ('count_scorm', queryset.filter(material_type=5).count()),
        ])

