from django.db import models

class Pool(models.Model):

    from course.models import Material

    TYPE_CHOICES = (
        (1, 'Public'),
        (2, 'Private'),
    )
    
    # Provider Rule
    # - Material keep Course Provider
    # ...
    provider = models.ForeignKey('provider.Provider', null=True, blank=True, related_name='+', on_delete=models.SET_NULL)
    
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    content = models.BigIntegerField(db_index=True)

    name = models.CharField(max_length=120) # For Search Only!!!
    material_type = models.IntegerField(choices=Material.TYPE_CHOICES, default=-1)

    type = models.IntegerField(choices=TYPE_CHOICES, default=2)
    
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    timeupdate = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']

    @staticmethod
    def push(provider, content_type, content):
        pass
    
class Access(models.Model):
    from django.conf import settings

    pool = models.ForeignKey(Pool)
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    
