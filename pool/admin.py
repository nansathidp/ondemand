from django.contrib import admin
from .models import Pool, Access

@admin.register(Pool)
class PoolAdmin(admin.ModelAdmin):
    list_display = ('provider', 'content_type', 'content', 'name', 'material_type', 'type', 'timestamp')
    
