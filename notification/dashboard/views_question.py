from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.conf import settings

from ..models import Config

def question_view(request):
    if not request.user.has_perm('notification.view_message',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    content_type = settings.CONTENT_TYPE('question', 'activity')
    config_list = Config.objects.filter(content_type=content_type)
    
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Notification Management',
         'url': reverse('dashboard:notification-dashboard:home')},
        {'is_active': True,
         'title': 'Test'}
    ]
    return render(request,
                  'notification/dashboard/question.html',
                  {'SIDEBAR': 'notification',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'question',
                   'config_list': config_list})
