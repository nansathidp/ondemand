from django.shortcuts import redirect
from django.core.exceptions import PermissionDenied

def home_view(request):
    if not request.user.has_perm('notification.view_message',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    return redirect('dashboard:notification-dashboard:course')
