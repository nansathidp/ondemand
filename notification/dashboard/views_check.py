from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import PermissionDenied
from django.conf import settings

from ..models import Config

@csrf_exempt
def check_view(request):
    if not request.user.has_perm('notification.change_message',
                                 group=request.DASHBOARD_GROUP):
        raise PermissionDenied
    
    result = {}
    if request.method == 'POST':
        try:
            content = request.POST.get('content', '')
            if content == 'course':
                content_type = settings.CONTENT_TYPE('course', 'course')
            elif content == 'question':
                content_type = settings.CONTENT_TYPE('question', 'activity')
            elif content == 'program':
                content_type = settings.CONTENT_TYPE('program', 'program')
            else:
                content_type = None
            config = Config.objects.get(id=int(request.POST.get('config', -1)),
                                        content_type=content_type)
            tag = request.POST.get('tag', '')
            if tag == 'mobile':
                config.is_mobile = not config.is_mobile
                result['is_check'] = config.is_mobile
                config.save(update_fields=['is_mobile'])
            elif tag == 'web':
                config.is_web = not config.is_web
                result['is_check'] = config.is_web
                config.save(update_fields=['is_web'])
            elif tag == 'email':
                config.is_email = not config.is_email
                result['is_check'] = config.is_email
                config.save(update_fields=['is_email'])
        except:
            pass
    return JsonResponse(result)
