from django.conf.urls import url

from .views import home_view

from .views_course import course_view
from .views_question import question_view
from .views_program import program_view

from .views_check import check_view

app_name = 'notification-dashboard'
urlpatterns = [
    url(r'^$', home_view, name='home'), #TODO: testing

    url(r'^course/$', course_view, name='course'), #TODO: testing
    url(r'^question/$', question_view, name='question'), #TODO: testing
    url(r'^program/$', program_view, name='program'), #TODO: testing

    url(r'^check/$', check_view, name='check'), #TODO: testing
]
