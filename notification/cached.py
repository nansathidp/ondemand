from django.conf import settings
from django.core.cache import cache
from django.utils import timezone

from .models import Message

from utils.cached.time_out import get_time_out_day, get_time_out
        
def cached_notification_is_send(is_force=False):
    key = '%s_notification_is_send'%(settings.CACHED_PREFIX)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Message.objects.filter(status=0).exists()
        cache.set(key, result, get_time_out())
    return None if result == -1 else result
