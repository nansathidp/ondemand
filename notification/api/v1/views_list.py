from account.cached import cached_api_account_auth
from .views import check_key, check_require
from .views import json_render
from ...models import Message


def list_view(request):
    result = {}

    # Check Required
    response = check_key(request)
    if response is not None:
        return response
    code, token, uuid, store, version = check_require(request, require_token=True)
    if code != 200:
        return json_render(result, code)

    # Authen Require
    account = cached_api_account_auth(token, uuid)
    if account is None:
        return json_render({}, 400)

    # Query Notification
    notification_list = Message.objects.all().first()
    for notification in notification_list:
        result['notification_list'] = []
    return json_render(result, 200)
