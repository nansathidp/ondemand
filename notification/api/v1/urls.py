from django.conf.urls import url

from .views_list import list_view
from .views_register import register_view

urlpatterns = [
    url(r'^$', list_view),  # TODO: testing
    url(r'^register/$', register_view),  # TODO: Testing
]
