from django.http import JsonResponse

from api.views_api import STATUS_MSG

_STATUS_MSG = {
    200: 'Success',
    201: 'Require POST method.',
    202: 'Require UUID.',
    204: 'Require token.',
    209: 'Require Store.',
}


def json_render(result, status):
    if status in _STATUS_MSG:
        status_msg = _STATUS_MSG[status]
    else:
        status_msg = STATUS_MSG[status]
    data = {'status': status,
            'status_msg': status_msg,
            'result': result}
    return JsonResponse(data, json_dumps_params={'indent': 2})

from api.views import check_auth
from api.v5.views import check_key
from api.v5.views import check_require
