from django.conf import settings
from .views import json_render, check_key
from notification.models import Android, Ios
from account.cached import cached_api_account_auth


def register_view(request):
    result = {}
    code = 200
    response = check_key(request)
    if response is not None:
        return response

    uuid = request.GET.get('uuid', None)
    token = request.GET.get('token', None)
    android_token = request.GET.get('android_token', None)
    iphone_token = request.GET.get('iphone_token', None)
    device = request.GET.get('device', None)
    version = request.GET.get('version', None)

    if uuid is None:
        return json_render({}, 202)
    elif len(uuid) == 0:
        return json_render({}, 202)
    if token is None:
        account = None
    else:
        account = cached_api_account_auth(token, uuid)
        if account is None:
            return json_render({}, 400)

    if android_token is not None and len(android_token) != 0:
        android, created = Android.objects.get_or_create(
            app=request.APP,
            uuid=uuid
        )

        if account is not None and account.is_valid():
            if android.account != account:
                android.account = account
                android.save(update_fields=['account'])

        if account is not None and android.token != android_token:
            android.token = android_token
            android.save(update_fields=['token'])

        if account is not None and settings.PROJECT == 'ais' and settings.IS_ALERT_AIS:
            from ais.models import PNSSubscriber
            PNSSubscriber.push(android=android)

    elif iphone_token is not None and len(iphone_token) != 0:
        iphone = Ios.objects.filter(token=iphone_token).first()
        if iphone is None:
            iphone, created = Ios.objects.get_or_create(
                app=request.APP,
                uuid=uuid
            )
        else:
            iphone.account = account
            iphone.save(update_fields=['account'])

        if account is not None and account.is_valid():
            if iphone.account != account:
                iphone.account = account
                iphone.save(update_fields=['account'])

        if account is not None and iphone.token != iphone_token:
            iphone.token = iphone_token
            iphone.save(update_fields=['token'])

        if account is not None and settings.PROJECT == 'ais' and settings.IS_ALERT_AIS:
            from ais.models import PNSSubscriber
            PNSSubscriber.push(iphone=iphone)
    else:
        code = 650
    return json_render(result, code)
