from .models import Message


def _push(app_id, title, account_id):
    Message.push_send(app_id, title, account_id)
