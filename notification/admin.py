from django.contrib import admin
from notification.models import *
from app.models import App

from utils.cached.content_type import cached_content_type_admin_item_inline


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('app', 'message', 'count_success', 'count_error', 'timestamp', 'timestart', 'timeend', 'status')



@admin.register(Android)
class AndroidAdmin(admin.ModelAdmin):
    list_display = ('app', 'uuid', 'account', 'timeupdate')
    list_filter = ['app']
    search_fields = ['account__email']
    readonly_fields = ['account']


@admin.register(Ios)
class IphoneAdmin(admin.ModelAdmin):
    list_display = ('app', 'uuid', 'account', 'token', 'timeupdate')


@admin.register(AndroidKey)
class AndroidKeyAdmin(admin.ModelAdmin):
    list_display = ('app', 'key')


@admin.register(IphoneKey)
class IphoneKeyAdmin(admin.ModelAdmin):
    list_display = ('app', 'server', 'cert')


@admin.register(AndroidMessage)
class AndroidMessageAdmin(admin.ModelAdmin):
    list_display = ('app', 'message', 'timestamp', 'timesend', 'is_send', 'is_success')
    # exclude = ('is_send', 'is_success')
    readonly_fields = ('app', 'message', 'android', 'data', 'response', 'timestamp', 'timesend')
    actions = ['send', ]

    def send(self, request, queryset):
        android_key = AndroidKey.objects.filter(app=request.APP).first()
        if android_key:
            for instance in queryset:
                instance.send(android_key=android_key)
    send.short_description = "Send"


@admin.register(IphoneMessage)
class IphoneMessageAdmin(admin.ModelAdmin):
    list_display = ('app', 'message', 'iphone', 'timestamp', 'timesend', 'is_send', 'is_success')
    exclude = ('timestamp', 'timesend', 'is_send', 'is_success')
    readonly_fields = ('data', 'response')
    actions = ['send', ]

    def send(self, request, queryset):
        iphone_key = IphoneKey.objects.filter(app=request.APP).first()
        if iphone_key:
            for instance in queryset:
                instance.send(iphone_key=iphone_key)


@admin.register(Config)
class ConfigAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'name', 'code', 'is_mobile', 'is_web', 'is_email', 'sort')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'content_type':
            kwargs["queryset"] = cached_content_type_admin_item_inline()
        return super(ConfigAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Queue)
class QueueAdmin(admin.ModelAdmin):
    list_display = (
    'code', 'content_type', 'order_item', 'content', 'account', 'timecreate', 'timenext', 'timeupdate', 'is_active')
    readonly_fields = ['account']

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'content_type':
            kwargs["queryset"] = cached_content_type_admin_item_inline()
        return super(QueueAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    list_display = ('code', 'content_type', 'order_item', 'content', 'account', 'timestamp')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'content_type':
            kwargs["queryset"] = cached_content_type_admin_item_inline()
        return super(LogAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
