from django.db import models


class Message(models.Model):

    PAGE_CHOICES = (
        (0, 'none'),
        (1, 'course'),
        (2, 'question'),
        (3, 'task'),
        (4, 'program'),
        (5, 'onboard'),
    )
    
    STATUS_CHOICES = (
        (-2, 'Test Send'),
        (-1, 'Direct Msg'),  # Create auto
        (0, 'Wait'),
        (1, 'Creating'),
        (2, 'Send Pause'),
        (3, 'Sending'),
        (4, 'Success'),
    )

    app = models.ForeignKey('app.App', null=True)
    message = models.CharField(max_length=120)
    page = models.IntegerField(choices=PAGE_CHOICES, default=0)

    content_type = models.ForeignKey('contenttypes.ContentType', related_name='notification_message_set', null=True)
    content = models.BigIntegerField(default=-1)

    count_success = models.IntegerField(default=0)
    count_error = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now_add=True)
    timestart = models.DateTimeField(null=True, blank=True)
    timeend = models.DateTimeField(null=True, blank=True)
    status = models.IntegerField(choices=STATUS_CHOICES, db_index=True, default=0)

    class Meta:
        default_permissions = ('view', 'add', 'change', 'delete')  # Not view_own
        
    def __str__(self):
        return self.message

    def save(self, *args, **kwargs):
        from notification.cached import cached_notification_is_send
        super(self.__class__, self).save(*args, **kwargs)
        cached_notification_is_send(is_force=True)

    @staticmethod
    def push(app_id, message, account_id):
        from .cached import cached_notification_is_send
        message = Message.objects.create(app_id=app_id,
                                         message=message,
                                         status=-1)

        # for android in Android.objects.filter(app_id=app_id, account_id=account_id):
        #     android_message = android.push(message)
        #     android_message.send()
        #
        # for iphone in Ios.objects.filter(app_id=app_id, account_id=account_id):
        #     iphone_message = iphone.push(message)
        #     iphone_message.send()

        cached_notification_is_send(is_force=True)

    @staticmethod
    def push_send(app_id, message, account_id):
        from .cached import cached_notification_is_send
        message = Message.objects.create(app_id=app_id,
                                         message=message,
                                         status=-1)

        iphone_key_list = list(IphoneKey.objects.all())
        android_key_list = list(AndroidKey.objects.all())
        for android in Android.objects.filter(app_id=app_id, account_id=account_id):
            android_message = android.push(message)
            for android_key in android_key_list:
                android_message.send(android_key)

        for iphone in Ios.objects.filter(app_id=app_id, account_id=account_id):
            iphone_message = iphone.push(message)
            for iphone_key in iphone_key_list:
                iphone_message.send(iphone_key)
        cached_notification_is_send(is_force=True)
        return message

    def push_all(self):
        self.status = 1
        self.save(update_fields=['status'])
        for android in Android.objects.filter(app=self.app):
            android.push(self)

        for iphone in Ios.objects.filter(app=self.app):
            iphone.push(self)

        self.status = 2
        self.save(update_fields=['status'])


class Android(models.Model):
    from django.conf import settings
    
    app = models.ForeignKey('app.App')
    account = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, default=None)
    uuid = models.CharField(max_length=120, db_index=True)
    token = models.CharField(max_length=256)
    device = models.CharField(max_length=12, blank=True)
    version = models.CharField(max_length=12, blank=True)
    timeupdate = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        ordering = ['-timeupdate']
        
    def __str__(self):
        try:
            return self.account.email
        except:
            return '-'

    def api_display(self):
        return {
            'id': self.id
        }

    def push(self, message):
        return AndroidMessage.objects.create(app=self.app, message=message, android=self)


class Ios(models.Model):
    from django.conf import settings
    
    app = models.ForeignKey('app.App')
    account = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, default=None)
    uuid = models.CharField(max_length=120, db_index=True)
    token = models.CharField(max_length=120)
    device = models.CharField(max_length=12, blank=True)
    version = models.CharField(max_length=12, blank=True)
    timeupdate = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        ordering = ['-timeupdate']
        
    def __str__(self):
        return self.token

    def push(self, message):
        return IphoneMessage.objects.create(app=self.app, message=message, iphone=self)


class AndroidKey(models.Model):
    app = models.ForeignKey('app.App')
    key = models.CharField(max_length=255)


class IphoneKey(models.Model):
    SERVER_CHOICES = (
        (1, 'gateway.sandbox.push.apple.com'),
        (2, 'gateway.push.apple.com'),
    )

    app = models.ForeignKey('app.App')
    server = models.IntegerField(choices=SERVER_CHOICES)
    #cert = models.FileField(upload_to='notification/iphonekey/') #Folder Public - -"
    #cert = models.FileField(storage=FileSystemStorage(location='%s/notification/iphonekey'%settings.BASE_DIR))
    #cert = models.FileField(upload_to='../notification/iphonekey', blank=True)
    cert = models.CharField(max_length=120, blank=True)


class AndroidMessage(models.Model):
    app = models.ForeignKey('app.App')
    inbox = models.ForeignKey('inbox.Inbox', null=True, blank=True)
    message = models.ForeignKey(Message)
    android = models.ForeignKey(Android)
    data = models.TextField(blank=True)
    response = models.TextField(blank=True)
    
    is_send = models.BooleanField(default=False, db_index=True)
    is_success = models.BooleanField(default=False)
    is_read = models.BooleanField(default=False)
    
    timestamp = models.DateTimeField(auto_now_add=True)
    timesend = models.DateTimeField(db_index=True, null=True, blank=True)

    class Meta:
        ordering = ['-timesend']
        
    def send(self, android_key):
        from django.utils import timezone
        import requests, json
        if self.inbox is not None:
            inbox_id = self.inbox_id
        else:
            inbox_id = -1

        # TODO: Remove msg field
        json_data = {'data': {'inbox_id': inbox_id,
                              'msg': self.message.message,
                              'message': self.message.message,
                              'page': self.message.page,
                              'page_code': self.message.get_page_display(),
                              'content': self.message.content,
                              'content_type_name': self.message.content_type.name if self.message.content_type else None,
                              },
                     'registration_ids': [self.android.token]}
        url = 'https://gcm-http.googleapis.com/gcm/send'
        key = android_key.key
        data = json.dumps(json_data)
        self.data = data
        headers = {'Content-Type': 'application/json', 'Authorization': 'key=%s'%key}
        try:
            r = requests.post(url, data=data, headers=headers)
            self.is_success = True
        except:
            self.is_success = False
        self.response = r.text
        self.is_send = True
        self.timesend = timezone.now()
        self.save(update_fields=['data', 'is_success', 'response', 'is_send', 'timesend'])


class IphoneMessage(models.Model):
    app = models.ForeignKey('app.App')
    inbox = models.ForeignKey('inbox.Inbox', null=True, blank=True)
    message = models.ForeignKey(Message)
    iphone = models.ForeignKey(Ios)
    data = models.TextField()
    response = models.TextField(blank=True)
   
    is_send = models.BooleanField(default=False, db_index=True)
    is_success = models.BooleanField(default=False)
    is_read = models.BooleanField(default=False)

    timestamp = models.DateTimeField(auto_now_add=True)
    timesend = models.DateTimeField(null=True, blank=True)

    def send(self, iphone_key):
        from django.conf import settings
        import socket, ssl, json, struct
        import traceback
        # from inbox.cached import cached_inbox_count
        # inbox_count = cached_inbox_count(self.iphone.account.id)
        payload = {
            'aps': {
                'alert': self.message.message,
                'sound': 'k1DiveAlarm.caf',
                'badge': 0,
                'page': self.message.page,
                'content': self.message.content,
                'content_type_name': self.message.content_type.name if self.message.content_type else None,
            }
        }

        #cert = '%s/ck.pem'%os.path.join(settings.BASE_DIR, 'aisadmission/ios')
        #cert = iphone_key.cert.path
        cert = '%s/%s' % (settings.BASE_DIR, iphone_key.cert)

        #host = ('gateway.sandbox.push.apple.com', 2195)
        #host = ('gateway.push.apple.com', 2195)
        host = (iphone_key.get_server_display(), 2195)

        data = json.dumps(payload)
        
        self.data = data
        data = str.encode(data)
        token = bytes.fromhex(self.iphone.token) # Python 3
        #token = self.iphone.token.decode('hex') # Python 2

        format = '!BH32sH%ds'%len(data)
        noti = struct.pack(format, 0, 32, token, len(data), data )

        try:
            ssl_sock = ssl.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM), certfile=cert)
            ssl_sock.connect(host)
            ssl_sock.write(noti)
            ssl_sock.close()
            self.is_success = True
        except:
            self.is_success = False
            self.response = '\n%s\n---\n%s'%(cert, traceback.format_exc())
        self.is_send = True
        self.save(update_fields=['data', 'is_send', 'is_success', 'response'])


class Config(models.Model):
    content_type = models.ForeignKey('contenttypes.ContentType')
    name = models.CharField(max_length=120)
    code = models.CharField(max_length=24, db_index=True)
    is_mobile = models.BooleanField(default=False)
    is_web = models.BooleanField(default=False)
    is_email = models.BooleanField(default=False)
    sort = models.IntegerField(db_index=True, default=0)
    
    class Meta:
        ordering = ['sort']

    def __str__(self):
        return self.code


class Queue(models.Model):
    from django.conf import settings
    
    app = models.ForeignKey('app.App')
    config = models.ForeignKey(Config)
    code = models.CharField(max_length=24, db_index=True)
    content_type = models.ForeignKey('contenttypes.ContentType')
    order_item = models.BigIntegerField(db_index=True)
    content = models.BigIntegerField(db_index=True)
    account = models.ForeignKey(settings.AUTH_USER_MODEL)
    
    timecreate = models.DateTimeField(auto_now_add=True)
    timenext = models.DateTimeField(db_index=True)
    timeupdate = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(db_index=True, default=True)

    class Meta:
        ordering = ['-timenext']

    def push(app_id, content_type, order_item, content, account_id):
        from django.utils import timezone
        for config in Config.objects.filter(content_type=content_type):
            if not Queue.objects.filter(config=config, order_item=order_item, content=content).exists():
                # Debug
                # if not config.code in ['new_assignment', '50_percent_befor_expired']:
                #    continue
                Queue.objects.create(app_id=app_id,
                                     config=config,
                                     code=config.code,
                                     content_type=content_type,
                                     order_item=order_item,
                                     content=content,
                                     account_id=account_id,
                                     timenext=timezone.now())
        
    @staticmethod
    def push_course(app_id, item_id, course_id, account_id):
        from django.conf import settings
        content_type = settings.CONTENT_TYPE('course', 'course')
        Queue.push(app_id, content_type, item_id, course_id, account_id)

    @staticmethod
    def push_question(app_id, item_id, activity_id, account_id):
        from django.conf import settings
        content_type = settings.CONTENT_TYPE('question', 'activity')
        Queue.push(app_id, content_type, item_id, activity_id, account_id)

    @staticmethod
    def push_program(app_id, item_id, program_id, account_id):
        from django.conf import settings
        content_type = settings.CONTENT_TYPE('program', 'program')
        Queue.push(app_id, content_type, item_id, program_id, account_id)


class Log(models.Model):
    from django.conf import settings
    
    config = models.ForeignKey(Config)
    code = models.CharField(max_length=24, db_index=True)
    content_type = models.ForeignKey('contenttypes.ContentType')
    order_item = models.BigIntegerField(db_index=True)
    content = models.BigIntegerField(db_index=True)
    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='notification_log')

    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        ordering = ['-timestamp']
