from django.utils import timezone

from notification.models import Message
from inbox.models import Inbox
from mailer.models import Mailer

from ._utils import _message, _percent

import datetime

MSG_TEMPLATE = '25%% Befor Expired: %s'

def run_25_percent_befor_expired(queue):
    message = _message(queue.content_type_id, queue.content, MSG_TEMPLATE)
    order_item, percent, total = _percent(queue)
    if percent is None:
        return

    if order_item.expired - (timezone.now() - order_item.start) < datetime.timedelta(days=1):
        queue.is_active = False
        queue.save(update_fields=['is_active'])
        return
    
    if percent < 75.0:
        next = total/10
        if next < 600:
            next = 600
        queue.timenext = queue.timenext + datetime.timedelta(seconds=next)
        queue.save(update_fields=['timenext'])
        return
    
    #Push
    config = queue.config
    if config.is_mobile:
        Message.push(queue.app_id, message, queue.account_id)
    if config.is_web:
        Inbox.push(-1, -1,
                   queue.app, queue.account, 5, message, message,
                   image=None,
                   is_push_mobile=False)
    if config.is_email:
        Mailer.push_send('contact@conicle.com',
                         queue.account.email,
                         message,
                         ':P',
                         6,
                         2)
    #End
    queue.is_active = False
    queue.save(update_fields=['is_active'])
