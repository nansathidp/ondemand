from django.conf import settings

from notification.models import Message
from inbox.models import Inbox
from mailer.models import Mailer
from progress.models import Progress
from order.models import Item as OrderItem
from content.models import Location as ContentLocation

from ._utils import _message

import datetime

MSG_TEMPLATE = 'Completed: %s'

def run_completed(queue):
    message = _message(queue.content_type_id, queue.content, MSG_TEMPLATE)

    order_item = OrderItem.pull(queue.order_item)
    content_type = settings.CONTENT_TYPE_ID(queue.content_type_id)
    
    progress = Progress.pull(order_item,
                             ContentLocation.pull(order_item.content_location_id),
                             content_type,
                             queue.content)
    if all(_ is not None for _ in [progress, order_item, content_type]):
        if progress.status != 2:
            queue.timenext = queue.timenext + datetime.timedelta(hours=2)
            queue.save(update_fields=['timenext'])
            return
    else:
        queue.is_active = False
        queue.save(update_fields=['is_active'])
        return
    
    #Push
    config = queue.config
    if config.is_mobile:
        Message.push(queue.app_id, message, queue.account_id)
    if config.is_web:
        Inbox.push(-1, -1,
                   queue.app, queue.account, 5, message, message,
                   image=None,
                   is_push_mobile=False)
    if config.is_email:
        Mailer.push_send('contact@conicle.com',
                         queue.account.email,
                         message,
                         ':P',
                         6,
                         2)
    #End
    queue.is_active = False
    queue.save(update_fields=['is_active'])
