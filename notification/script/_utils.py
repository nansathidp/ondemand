from django.conf import settings
from django.utils import timezone

from order.models import Item as OrderItem
from utils.content import get_content

import datetime

def _message(content_type_id, content, MSG_TEMPLATE):
    content_object = get_content(content_type_id, content)
    if content_object is not None:
        message = MSG_TEMPLATE%(content_object.name)
    else:
        message = MSG_TEMPLATE%('-')
    return message

def _percent(queue):
    order_item = OrderItem.pull(queue.order_item)
    if order_item.start is None:
        queue.timenext = queue.timenext + datetime.timedelta(hours=2)
        queue.save(update_fields=['timenext'])
        return order_item, None, None
    if order_item.expired == datetime.timedelta(0):
        queue.is_active = False
        queue.save(update_fields=['is_active'])
        return order_item, None, None
    total = order_item.expired.total_seconds()
    use = (timezone.now() - order_item.start).total_seconds()
    percent = use/total*100.0
    return order_item, percent, total
