import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings
from django.utils import timezone

from notification.models import Message, AndroidMessage, IphoneMessage, AndroidKey, IphoneKey

from notification.cached import cached_notification_is_send
from utils.cron import check_pid
import datetime, time

ANDROID_KEY_CACHED = {}
def get_android_key(app_id):
    if app_id in ANDROID_KEY_CACHED:
        return ANDROID_KEY_CACHED[app_id]
    else:
        ANDROID_KEY_CACHED[app_id] = AndroidKey.objects.get(app_id=app_id)
        return ANDROID_KEY_CACHED[app_id]

IPHONE_KEY_CACHED = {}
def get_iphone_key(app_id):
    if app_id in IPHONE_KEY_CACHED:
        return IPHONE_KEY_CACHED[app_id]
    else:
        IPHONE_KEY_CACHED[app_id] = IphoneKey.objects.get(app_id=app_id)
        return IPHONE_KEY_CACHED[app_id]
    
if __name__ == '__main__':
    path = '%s/notification/script/pid.send'%settings.BASE_DIR
    if check_pid(path):
        exit()

    time_start = timezone.now()
    count = 0
    while True:
        is_send = cached_notification_is_send()
        if not is_send:
            time.sleep(6)
            if timezone.now() - time_start > datetime.timedelta(hours=2):
                break
            continue

        #Create
        for message in Message.objects.filter(status=0):
            message.push_all()
            
        #Send
        for android_message in AndroidMessage.objects.select_related('message', 'android').filter(is_send=False):
            android_message.send(get_android_key(android_message.app_id))
            count += 1
            if count % 1000 == 0 and (time_start-timezone.now()) > datetime.timedelta(hours=2):
                sys.exit()
            
        for iphone_message in IphoneMessage.objects.filter(is_send=False):
            iphone_message.send(get_iphone_key(iphone_message.app_id))
            count += 1
            if count % 1000 == 0 and (time_start-timezone.now()) > datetime.timedelta(hours=2):
                sys.exit()
            
        if count == 0:
            Message.objects.filter(status=3).update(status=4)
            count = -1

        cached_notification_is_send(is_force=True)
            
