from django.template.loader import render_to_string

from notification.models import Message
from inbox.models import Inbox
from mailer.models import Mailer

from ._utils import _message

MSG_TEMPLATE = 'Assigned: %s'

def run_new_assignment(queue):
    message = _message(queue.content_type_id, queue.content, MSG_TEMPLATE)

    #Push
    config = queue.config
    if config.is_mobile:
        Message.push(queue.app_id, message, queue.account_id)
    if config.is_web:
        Inbox.push(-1, -1,
                   queue.app, queue.account, 5, message, message,
                   image=None,
                   is_push_mobile=False)
    if config.is_email:
        html = render_to_string('mailer/notification/code_new_assignment.html',
                                {})
        Mailer.push_send('contact@conicle.com',
                         queue.account.email,
                         message,
                         html,
                         6,
                         2)
    #End
    queue.is_active = False
    queue.save(update_fields=['is_active'])
