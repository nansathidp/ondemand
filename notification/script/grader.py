import os, sys, django
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ondemand.settings")
django.setup()

from django.conf import settings
from django.utils import timezone

from notification.models import Queue

from utils.cron import check_pid
from notification.script.code_new_assignment import run_new_assignment
from notification.script.code_50_percent_befor_expired import run_50_percent_befor_expired
from notification.script.code_25_percent_befor_expired import run_25_percent_befor_expired
from notification.script.code_1_day_befor_expired import run_1_day_befor_expired
from notification.script.code_expired import run_expired
from notification.script.code_completed import run_completed
import datetime, time

if __name__ == '__main__':
    path = '%s/notification/script/pid.grader'%settings.BASE_DIR
    if check_pid(path):
        exit()

    time_start = timezone.now()
    while True:
        for queue in Queue.objects.select_related('config').filter(timenext__lte=timezone.now(), is_active=True):
            if queue.code == 'new_assignment':
                run_new_assignment(queue)
            elif queue.code == '50_percent_befor_expired':
                run_50_percent_befor_expired(queue)
            elif queue.code == '25_percent_befor_expired':
                run_25_percent_befor_expired(queue)
            elif queue.code == '1_day_befor_expired':
                run_1_day_befor_expired(queue)
            elif queue.code == 'expired':
                run_expired(queue)
            elif queue.code == 'completed':
                run_completed(queue)

        #End
        #break #For Debug
        if timezone.now() - time_start > datetime.timedelta(hours=2):
            break
        else:
            time.sleep(60)
            
