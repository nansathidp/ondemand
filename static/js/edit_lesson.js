var count = 10;

String.prototype.unescape_html = function(){
    var t = document.createElement( 'div' );
    t.innerHTML = this;
    return t.firstChild.nodeValue;
}

String.prototype.escape_html = function(){
    return $('<div/>').text(this).html();
}

function iconcheck(div){
    $(div+" > a > span").addClass("green").removeClass("red");
    $(div+" > a > span > i").addClass("icon-check").removeClass("icon-cancel");
}

function iconcancel(div){
    $(div+" > a > span").addClass("red").removeClass("green");
    $(div+" > a > span > i").addClass("icon-cancel").removeClass("icon-check");
}


$.get(setup_select_form_path,function(data){
    
    var  form_data = data.result;
    var root = form_data["category"]["None"];

    for(i=0;i< root.length;i++){
        $(".lesson-category").append(new Option(root[i]["name"], root[i]["val"]));    
    }

    for(i=0;i<form_data["language"].length;i++){
        $(".lesson-language").append(new Option(form_data["language"][i]["name"],form_data["language"][i]["val"]));    
    }

    
    $(document).on('change', '.lesson-category', function() {

        var subcategory = $($(this).attr("subcategory"));

        subcategory.empty().append('<option value="0" disabled="disabled">ประเภทรอง</option>');
        var category_val = form_data['category'][ $(this).val()];
        if(category_val !=null){
            for(i=0;i<category_val.length;i++){
                var v = category_val[i];
                subcategory.append(new Option(v["name"],v["val"]));        
            }
            $($($(this).attr("subcategory")+" option")[1]).attr("selected",true)
        }
    });
    var time_in_hours_format = default_time/60; 
    var hours = time_in_hours_format - (time_in_hours_format%1);
    var minutes = default_time - (hours*60);

    $("#lesson-language").val(default_language);
    $("#hours").val(hours);
    $("#minutes").val(minutes);
    if(default_category!=""){
        $("#lesson-category").val(default_category);
        setTimeout(function(){ 
            $("#lesson-subcategory").val(default_subcategory);
        }, 100);
    }
    

    $(".lesson-category").change();
    
})



var count = 1;

function readImage(file,name,id,callback) {
    var FR= new FileReader();
    FR.onload = function(e) {
        createHidden(name, e.target.result,id)
        callback()
    };       
    FR.readAsDataURL( file );   
}

function createHidden(name,val,id){
    var input = document.createElement("input");
    input.setAttribute("type", "hidden");
    input.setAttribute("class", "slideimg");
    input.setAttribute("name", name);
    input.setAttribute("value", val);
    input.setAttribute("id", id);    

    $("#lesson").append(input);
}


function create_file_upload(id){
    var input = document.createElement("input");
    input.setAttribute("type", "file");
    input.setAttribute("style", "display:none");
    input.setAttribute("name", "slide[]");
    input.setAttribute("accept", "image/*");
    input.setAttribute("img-ref", "#preview-slide");
    input.setAttribute("class", "slide-input");
    input.setAttribute("id", id);    

    $("#lesson").append(input);
}

function image_preview(file,type,callback){
    var url = URL.createObjectURL(file);
    var div = document.createElement('div');
    $(div).attr("style"," background-image: url("+url+")")
    $(div).addClass("preview-img");
    
    if(type == "slide"){
        $(div).addClass("addimg");
        $(div).attr("ref-hidden", "addimg-"+count);
        /*
        readImage(file,"addimg[]","addimg-"+count,function(){
            callback(div)
        });*/

        count+=1;

    }

    callback(div)
    
}

function over_slide(ref){
    var tmp = ref.html();
    ref.html("<span class='red' style='font-size: 15px;'>FILE not compatible OR more than 4 image</span>");
    setTimeout(function(){
        ref.fadeOut();
        ref.html(tmp);
        ref.fadeIn();

    },1000);
}


$("#banner").change(function(){
    var file = this.files[0];
    var ref = $(this).attr("img-ref")

    image_preview(file,"banner" , function(data){
        $(ref).html(data);    
    })
    
})


$('body').on('change','.slide-input',function(e){
    var files = this.files;
    var ref = $(this).attr("img-ref")
    for(var i=0;i<files.length ;i++){               

        if( $(ref+" .preview-img:visible").length >= 4 ){
            over_slide( $(ref) );
            break;
        }

        image_preview(files[i],"slide" , function(data){
            $(ref).append(data); 
        })

        newfile_toggle = "addimg-"+(count)
        create_file_upload(newfile_toggle)
        $("#add-slide").attr("file-toggle","#"+newfile_toggle)
    }
})

$('body').on('click','.preview-img',function(e){
    $(this).fadeOut();
    if( $(this).hasClass("delimg")){   

        var name = $(this).attr("ref-hidden");
        createHidden( "delimg[]" , name.split("-")[1] , "delimg-"+name.split("-")[1]);

    }else{
        var name = $(this).attr("ref-hidden");
        $("#"+name).remove();
    }
    
})



$("#addtag").val("");

$('#tag').keyup(function(e) {
    var code = e.keyCode || e.which;

    
   if (code == '32') {

        var newval = $("#tag").val();
        newval = $.trim(newval);
        if( newval.length >= 1){
            var oldval = $("#addtag").val();
            var tag = "<a>"+$("#tag").val()+' <i class="icon icon-cancel"></i></a>'
            $(".trends-list").append(tag);
            $("#tag").val("")
            $("#addtag").val(oldval+","+newval)
            console.log("add tag : "+newval)
        }
    }
 });



$('body').on('click', ".trends-list .icon-cancel", function(e){
    $(this).parent().hide();
    var txt = $(this).parent()[0].childNodes[0].data;
    txt = txt.trim();

    var oldval = $("#deltag").val();
    $("#deltag").val(oldval+","+txt)
    console.log("del tag : "+txt);
});


$("#delcourse").click(function(){
    window.location.href = del_course_url;
});

var fullEditor = new Quill('#full-editor', {
        modules: {
          'toolbar': { container: '#full-toolbar' },
          'link-tooltip': true,
          "image-tooltip" : true,
          'vdo-tooltip':true,
        },
        theme: 'snow',
    });

// default_desc = fullEditor.getText().unescape_html()
default_desc = fullEditor.getText()
$("#desc").val( fullEditor.getText() );
fullEditor.setHTML(default_desc);
fullEditor.on('text-change', function(delta, source) {
    // var data = fullEditor.getHTML(); 
    // $("#desc").val( data.escape_html() );
    $("#desc").val(fullEditor.getHTML());
});


function validate(){

    ////////////// Video ///////////
    if($("#video")[0].validity.valid && $("#video").val().length > 0){
        iconcheck("#mark-video")
    }
    $('#video').keyup(function(e) {
        if($(this)[0].validity.valid)
            iconcheck("#mark-video")
        else
            iconcancel("#mark-video")
     });

    ////////////// Banner ///////////
    if($("#preview-banner .preview-img").length > 0){
        iconcheck("#mark-banner")
    }else{
        iconcancel("#mark-banner")
    }
    $('body').on('click','#preview-banner .preview-img',function(e){
        iconcancel("#mark-banner")
    });

    $("#banner").change(function(){
        iconcheck("#mark-banner")
    })

    ////////////// Desc ///////////
    if( $("#desc").val().length > 1){
        iconcheck("#mark-desc")   
    }else{
        iconcancel("#mark-desc")
    }

    fullEditor.on('text-change', function(delta, source){
        if(fullEditor.getLength() > 1){
            iconcheck("#mark-desc")   
        }else{
            iconcancel("#mark-desc")
        }
    });

    ////////////// Tag ///////////
    if($(".trends-list a").length > 0){
        iconcheck("#mark-tag")
    }
        

    $('#tag').keyup(function(e) {
        var code = e.keyCode || e.which;
        if (code == '32') {
            iconcheck("#mark-tag")
        }
     });
}
validate();

$("#tmp-submit").click(function(){

    if( ! $("#lesson")[0].checkValidity()){
        $("#submit").click();
    }else{
        if( !$("#mark-banner i").hasClass("icon-check") ){
            window.location.href = "#label-banner";
            alert("Please choose banner")
        }else if( !$("#mark-desc i").hasClass("icon-check") ){
            window.location.href = "#label-desc";
            alert("Please enter description")
        }else{
            $("#submit").click();
        }    
    }
        
    
})




