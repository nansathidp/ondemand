$(document).ready(function() {
    // $("#menu .section-submenu .fa:lt("+v+")").attr("class","fa fa-check-circle")
    $("#menu .section-submenu .fa:eq("+v+")").attr("class","fa fa-play-circle")
    $("#menu .section-submenu .fa:gt("+v+")").attr("class","fa fa-circle-thin")
    
    // $(".section-menu").each(function(){
    //   var submenu = $(this).attr("data-target");
    //   if ($(submenu+"i").hasClass("fa-play-circle") ){
    //     $(this).find("i").addClass("fa-play-circle")
    //   }else if( $(submenu+"i").hasClass("fa-circle-thin") ){
    //     $(this).find("i").addClass("fa-circle-thin")
    //   }else{
    //     $(this).find("i").addClass("fa-check-circle")
    //   }
    // })

    var ratio = 0.5625;
    $('#wrap-video').height($(window).width()*ratio);
    $('#player-frame').height($(window).width()*ratio);
    $('#player').height($(window).width()*ratio);
    $(window).resize(function() {
      $('#wrap-video').height($(window).width()*ratio);
      $('#player-frame').height($(window).width()*ratio);
      $('#player').height($(window).width()*ratio);
    });

});

function redirectNext() {
    if($('#button-next').length > 0)
        window.location.replace($('#button-next').attr('href'));
}

function init(course_id,video_id)
{
    var request = $.ajax({
        url: "/course/"+course_id+"/video/"+video_id+"?rand="+new Date().getTime(),
        type: "POST",
        cache:false,
        data: { rand: new Date().getTime(),  },
        dataType: "text",
        beforeSend: function(request) {
            return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
        }
    }).done(function( msg ) {
        flowplayer(function (api, root) {
          api.bind("load", function (video,callback) {
            console.log('load');
          }).bind("finish", function () {
            console.log('finish');
            setTimeout(redirectNext, 2000);
          });
        });
        $("#player").flowplayer({
          playlist: [
             [
                { mp4: msg }
             ]
          ],
        }); 
    }).fail(function( jqXHR, textStatus) {
       console.log('Url :' +  textStatus)
    });
}


function initYoutube(videoId)
{
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    var player;
    function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
      height: '100%',
      width: '100%',
      videoId: videoId,
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
      }
    });
    }
    function onPlayerReady(event) {
        event.target.playVideo();
    }
    var done = false;
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED) {
          setTimeout(redirectNext, 1500);
        }
    }
    function stopVideo() {
        player.stopVideo();
    }
}