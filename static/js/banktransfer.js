$(function() {
	$("#slip-file").bind('change', function() {
		var file_size = this.files[0].size;
		if( file_size > 2000000){
			alert("กรุณาใช้ไฟล์ที่มีขนาดไม่เกิน 2 MB");
		}
	});

	$("#bank-confirm-from").submit( function() {
		if( !$("#bank-selector").val() ) return false;
		if( !$("#hours-selector").val() ) return false;
		if( !$("#minutes-selector").val() ) return false;
		if( !$("#slip-file").val() ) return false;
		var file_size = $("#slip-file")[0].files[0].size;
		if( file_size > 2000000 ){
			alert("กรุณาใช้ไฟล์ที่มีขนาดไม่เกิน 2 MB");
			return false;
		}
		if( !isImage($("#slip-file").val()) ){
			alert("กรุณาใช้ไฟล์นามสกุล .jpg, .jpeg, .bmp, .png");
			return false;
		}
		var paid_date = $("#transfer_date").val();
		var regex = new RegExp("^[0-9]{2}/[0-9]{2}/[0-9]{4}$", "i");
		if( !regex.test( paid_date ) ){
			return false;
		}
		$('.tranfer.button-loading').show();
	});
	$("#transfer_date").datepicker();
});
var getExtension = function(filename) {
	var parts = filename.split('.');
	return parts[parts.length - 1];
}

var isImage = function(filename) {
	var ext = getExtension(filename);
	switch (ext.toLowerCase()) {
		case 'jpg':
		case 'jpeg':
		case 'bmp':
		case 'png':
		return true;
	}
	return false;
}