$(function(){
	$("#form-become").submit(function(event) {
		var request = $.ajax({
			url: base_path+"/service/subscribe",
			type: "POST",
			data: { 
				subject : this.subject.value,
				course_type : this.course_type.value,
				location : this.location.value,
				email : this.email.value,
				tel : this.tel.value,
			},
			dataType: "json",
			beforeSend: function(request) {
				return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
			}
		}).done(function( msg ) {
			var Response =  JSON.stringify(msg);
			$("#form-become").trigger('reset');
			$("#display-message").fadeIn('slow');
			// $('#becomeModal').modal('show');
		}).fail(function( jqXHR, textStatus ) {
			console.log("Request failed: " + textStatus);
			if(jqXHR.responseJSON.code == 481){
				$('#loginModal').modal('show');
			}else{
			}
		});
		event.preventDefault(); //STOP default action
	});
	
});
