var count = 10;

function iconcheck(div){
    $(div+" > a > span").addClass("green").removeClass("red");
    $(div+" > a > span > i").addClass("icon-check").removeClass("icon-cancel");
}

function iconcancel(div){
    $(div+" > a > span").addClass("red").removeClass("green");
    $(div+" > a > span > i").addClass("icon-cancel").removeClass("icon-check");
}


function image_preview(file,part){
    var url = URL.createObjectURL(file);
    var div = document.createElement('div');
    $(div).attr("style"," background-image: url("+url+")")
    $(div).addClass("preview");
    
    if(part == "slide"){
        $(div).addClass("newimg");
        $(div).attr("ref-hidden", "newimg-"+count);
        readImage(file,"newimg[]","newimg-"+count);

        count+=1;
    }
    return div;
}

function moreSlideImg(){
    var tmp = $("#hint-slide").html();
    $("#hint-slide").html("<span class='red' style='font-size: 15px;'>FILE not compatible OR more than 4 image</span>");
    setTimeout(function(){
        $("#hint-slide").fadeOut();
        $("#hint-slide").html(tmp);
        $("#hint-slide").fadeIn();

    },1000);
}

$('#edit-course').verify({
    prepare : function(){

        $.get(base_path+"/lesson/create/json",function(json){
            var form_data = JSON.parse(json);
            $("#topic").val( $("input[name=old-subject]").val() );
            $('#category option[value='+$("input[name=old-category]").val()+']').attr('selected','selected');
            $('#type option[value='+(form_data["type"].indexOf($("input[name=old-type]").val())+1)+']').attr('selected','selected');        
            $('#level option[value='+(form_data["level"].indexOf($("input[name=old-level]").val())+1)+']').attr('selected','selected');        

        })

    },
    before  : function(id){
        $(".tick").removeClass("active");
        $(id).addClass("active");

    },
    input   : ["#banner" , "#slide" , "#vdo" , "#overview", "#tag"],
    condition   : {
        "#banner" : function(){
            var file = $("#banner")[0].files[0];
            var filetype = file.type;
            return (filetype.split('/')[0]=="image")
        },
        "#slide" : function(){
            //var files = $("#slide")[0].files;
            //$("#hint-slide").html("")
            return (  $("#hint-slide .preview:visible").length <= 4)
        },
        "#vdo" : function(){

            var id = "#tick-vdo";
            function learnRegExp(s) {    
              var urlregex = new RegExp(
                    "^(http|https|ftp)\://(www\.|m\.)?(youtube|vimeo)\.com");
              return urlregex.test(s);   
              //return true;
            }   

            var url = $("#vdo").val();
            return (learnRegExp(url) )

        },
        "#overview" : function(){
            var overview = $("#overview").val();
            return true;//(overview.length > 0)
        },
        "#tag" : function(){
            return ($("#tag > span").length > 0)
        },


    },
    attr    : {
        "#banner" : {
            "pass" : function(){
                var file = $("#banner")[0].files[0];
                $("#hint-banner").html( image_preview(file,"banner") );

            },
            "fail" : function(){
                $("#hint-banner").html("<span class='red'  style='font-size: 15px;'>FILE not compatible</span>");
            },
            "tick" : "#tick-banner"
        },

        "#slide" :{
            "pass" : function(){
                var files = $("#slide")[0].files;

                ///clear error /// 
                if($("#hint-slide div").length == 0)
                    $("#hint-slide").html("")

                for(var i=0;i<files.length ;i++){
                    
                    if( $("#hint-slide .preview:visible").length >= 4 ){
                        moreSlideImg();
                        break;
                    }
                    var div = image_preview(files[i],"slide");
                    $("#hint-slide").append( div);
                }

            },
            "fail" : moreSlideImg,
            "tick" : "#tick-slide"
        },
        "#vdo"      : {  "tick" : "#tick-vdo"  },
        "#overview" : {  "tick" : "#tick-overview"  },
        "#tag"      : {  "tick" : "#tick-tag"  },

    },
});


$("#tmp-submit").click(function(){
	var err = $(".tick a span.red");
	if(err.length == 0)
		$("#submit").click();
	else
		alert("input error");
})



$('form').keyup(function(e) {
    var code = e.keyCode || e.which;
   if (code == '9') {
        var newval = $("#tmptag").val();
        if( newval.length >= 1){


            var oldval = $("#tag").val();
            var tag = "<a>"+$("#tmptag").val()+' <i class="icon icon-cancel"></i></a>'
            $(".trends-list").append(tag);
            $("#tmptag").val("")
            $("#tag").val(oldval+","+newval)
            console.log("add tag : "+newval)
        }
    }
 });

$('body').on('click', ".trends-list .icon-cancel", function(e){
    $(this).parent().hide();
    var txt = $(this).parent()[0].childNodes[0].data;
    txt = txt.trim();
    var oldval = $("#tag").val();
    $("#tag").val(oldval+",*"+txt)
    console.log("del tag : "+txt);
});



function readImage(file,name,id) {
    
    var FR= new FileReader();
    FR.onload = function(e) {
        createHidden(name, e.target.result,id)
    };       

    FR.readAsDataURL( file );
    
}


function createHidden(name,val,id){

    var input = document.createElement("input");
    input.setAttribute("type", "hidden");
    input.setAttribute("class", "slideimg");
    input.setAttribute("name", name);
    input.setAttribute("value", val);

    //console.log(id);
    input.setAttribute("id", id);
    
    $("#edit-course").append(input);
}


$('body').on('click','.preview',function(e){
    $(this).fadeOut();
    if( $(this).hasClass("oldimg")){
        
        var name = $(this).attr("ref-hidden");
        createHidden( "oldimg[]" , name.split("-")[1] , "oldimg-"+name.split("-")[1]);

    }else{
        var name = $(this).attr("ref-hidden");
        $("#"+name).remove();
    }

    //var img =  readImage($("#slide")[0]);
    
})