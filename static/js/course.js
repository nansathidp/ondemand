$(function(){
	$('#outline_playlist1').on('hidden.bs.collapse', function () {
  		console.log('hidden.bs.collapse');
	});

  	$('.login-button').click(function() {
		$('#loginModal').modal('show');
	});	

		var slider = null;

		// Readmore
		initExpandableSection('.expandable-section.about',10000);
		initExpandableSection('.expandable-section.outline',750);
		initExpandableSection('.expandable-section.schedule',200);

		//Scroll down link
		$('.event-head').localScroll({
				duration:1000,
				offset: -80
		});
		$('.navmenu ul').localScroll({
				duration:1000,
				offset: -80
		});

		//More Course Information
		$('.more-course-info').click(function()   {
				$('.hidden-course-info').slideDown();
		});

		//More Course Outline
		$('.more-course-outline').click(function()   {
				$('.hidden-course-outline').fadeIn();
		});

		$('.wrap-featured').waypoint(function(direction) {
				if(direction == 'down') {
						$('.price-widget').fadeOut(100);
				}else{
						$('.price-widget').fadeIn(100);
				}
		}, {offset: 700});

		$('.expandable-toggle').click(function()  {
				var expandableInner = $(this).parent().find('.expandable-inner');
				var aboutCourseContentHeight = expandableInner.innerHeight();
				expandableInner.parent().css('height',aboutCourseContentHeight);
				expandableInner.parent().find('.fade-background').hide();
				$(this).hide();
		});

		$('#wantbtn').popover({
				trigger: 'focus',
				delay: { show: 200, hide: 200 }
		});

	//Changing the selected navigation link
	// $('#aboutCourse').waypoint(function(direction) {
	// 	console.log("in couse");
	// 	$( ".navmenu ul li" ).each(function() {
	// 		$(this).children('a').removeClass('selected');
	// 	});
	// 	if(direction == 'down') {
	// 		$( ".navmenu ul li:nth-child(2) a" ).addClass('selected');
	// 	}
	// 	else {
	// 		$( ".navmenu ul li:nth-child(1) a" ).addClass('selected');
	// 	}
	// }, { offset: 200 });

	// $('#courseOutline').waypoint(function() {
	// 	console.log("in outline");
	// 	$( ".navmenu ul li" ).each(function() {
	// 		$(this).children('a').removeClass('selected');
	// 	});
	// 	$( ".navmenu ul li:nth-child(3) a" ).addClass('selected');
	// }, { offset: 100 });

	// $('#review').waypoint(function() {
	// 	console.log("in review");
	// 	$( ".navmenu ul li" ).each(function() {
	// 		$(this).children('a').removeClass('selected');
	// 	});
	// 	$( ".navmenu ul li a[href='#review']" ).addClass('selected');
	// }, { offset: 200 });

	// $('#aboutInstructor').waypoint(function() {
	// 	console.log("in aboutInstructor");
	// 	$( ".navmenu ul li" ).each(function() {
	// 		$(this).children('a').removeClass('selected');
	// 	});
	// 	$( ".navmenu ul li a[href='#aboutInstructor']" ).addClass('selected');
	// }, { offset: 200 });

	// $('#aboutLocation').waypoint(function() {
	// 	console.log("in aboutLocation");
	// 	$( ".navmenu ul li" ).each(function() {
	// 		$(this).children('a').removeClass('selected');
	// 	});
	// 	$( ".navmenu ul li:nth-child(6) a" ).addClass('selected');
	// }, { offset: 200 });

	// $('#aboutPolicy').waypoint(function() {
	// 	console.log("in aboutPolicy");
	// 	$( ".navmenu ul li" ).each(function() {
	// 		$(this).children('a').removeClass('selected');
	// 	});
	// 	$( ".navmenu ul li:nth-child(7) a" ).addClass('selected');
	// }, { offset: 200 });


		//Start Slideshow
		$('.open-slideshow').click(function() {
				slider = $("#imageGallery").lightSlider({
						gallery:true,
						minSlide:1,
						currentPagerPosition:'left',
						thumbWidth: 144,
						thumbMargin: 10,
						slideMove:1,
						mode: "blur"
				});
				$('.slide-show-wrapper').addClass('active');
				slider.goToSlide($(this).attr("rel"));
		});

		$('.slide-show-wrapper').click(function() {
				var _this = $(this);
				_this.addClass('slide-fade-out');
				setTimeout(function () {
						_this.removeClass('active');
						_this.removeClass('slide-fade-out');
				}, 550);
		});

		$('.slide-inner').click(function(e) {
				e.stopPropagation()
		});

		$('#review-desc').keyup(function(){
				var length = $(this).val().length;
				$('#textcount').text(300 - length);
			})

		$('.fancybox.preview').fancybox({
    		tpl: {
      			wrap: '<div class="fancybox-wrap" tabIndex="-1">' +
            '<div class="fancybox-skin">' +
            '<div class="fancybox-outer">' +
            '<div id="player" data-embed="false" class="flowplayer no-toggle">' +
            '</div></div></div></div>'
    		},
    		beforeShow: function () {
						var path = $(this).attr('href').replace("#", "");
						var height = Math.round( ($( window ).width()*0.7) * 0.5 ).toString();
						jwplayer("player").setup({
            		flashplayer: flashplayer,
            		file: video_preview_path,
            		width: '100%',
            		height: height,
        		});
        		jwplayer().play();
        		jwplayer().onComplete(function(event){});
    		},
    		beforeClose: function () {}
		});
			$('.fancybox.public').fancybox({
    		tpl: {
      			wrap: '<div class="fancybox-wrap" tabIndex="-1">' +
          	'<div class="fancybox-skin">' +
          	'<div class="fancybox-outer">' +
          	'<div id="player" data-embed="false" class="flowplayer no-toggle">' +
          	'</div></div></div></div>'
    		},
    		beforeShow: function () {
						var path = $(this).attr('href').replace("#", "");
						var height = Math.round( $( window ).height()*0.80 ).toString();
						jwplayer("player").setup({
            		flashplayer: flashplayer,
            		file: "https://www.youtube.com/watch?v="+path,
            		width: '100%',
            		height: height,
        		});
        		jwplayer().play();
        		jwplayer().onComplete( function(event){});
    		},
    		beforeClose: function () {}
		});
});

function takeSubscribe(element,lesson_id){
		$.ajax({
				url: "/lesson/subscribe/",
				type: "POST",
				data: { lesson_id : lesson_id},
				dataType: "json",
				beforeSend: function(request) {
					return request.setRequestHeader('X-CSRFToken',$.cookie('csrftoken'));
				}
		}).done(function(msg){
				if(msg.status == 200){
						$(element).hide();
						$('.play-vdo-lesson').width(213)
						$('#pinned-button').fadeIn();
						$('#pin-button-subtitle').html('Lesson has pinned.');
						$('#pin-count').html(msg.result.subscribe_count);
				}else{
						$('#loginModal').modal('show');
				}
		}).fail(function( jqXHR, textStatus) {
				$('#loginModal').modal('show');
		});
}

function takeUnsubscribe(lesson_id){
		$.ajax({
				url: "/lesson/unsubscribe/",
				type: "POST",
				data: {lesson_id : lesson_id},
				dataType: "json",
				beforeSend: function(request) {
						return request.setRequestHeader('X-CSRFToken',$.cookie('csrftoken'));
				}
		}).done(function(msg) {

		}).fail(function(jqXHR, textStatus) {
				$('#loginModal').modal('show');
		});
}

function showVideo(){
		$(".event-head").fadeOut("slow");
		$(".videoview").fadeIn("slow");
}

function hideVideo(){
		$(".event-head").fadeIn("slow");
		$(".videoview").fadeOut("slow");
}

function initExpandableSection(element,maxheight){
		var aboutCourseContentHeight = $(element +' .expandable-inner').innerHeight();
		if(aboutCourseContentHeight < maxheight) {
				$(element).css('height','auto');
				$(element + ' + .expandable-toggle').hide();
				$(element).find('.fade-background').hide();
		}
}
