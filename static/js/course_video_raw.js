$(document).ready(function() {
    $('.menulink').bigSlide({
    	"init":function(){
    		$(".vdo-option").width( $(".wrap").width());
    	},
    	"open":function(){
    		$(".vdo-option").width( $(".wrap").width())
    		$("#vdo-mode").html('<i class="fa fa-expand"></i> Full Mode');
    	},
    	"closed" : function(){
    		$(".vdo-option").width( $(".wrap").width() )
	        $("#vdo-mode").html('<i class="fa fa-compress"></i> Default Mode');
    	},
	});
    $('#wrap-video').height($(window).height()-(388));
    $(window).resize(function() {
        $('#wrap-video').height($(window).height()-(388));
    });
	
	$("#menu .section-submenu .fa:lt("+v+")").attr("class","fa fa-check-circle")
	$("#menu .section-submenu .fa:eq("+v+")").attr("class","fa fa-play-circle")
	$("#menu .section-submenu .fa:gt("+v+")").attr("class","fa fa-circle-thin")
	$(".section-menu").each(function(){
	  var submenu = $(this).attr("data-target");
	  if ( $(submenu+" i").hasClass("fa-play-circle") ){
	  	$(this).find("i").addClass("fa-play-circle")
	  }else if( $(submenu+" i").hasClass("fa-circle-thin") ){
	  	$(this).find("i").addClass("fa-circle-thin")
	  }else{
	  	$(this).find("i").addClass("fa-check-circle")
	  }
	})
});

function init(course_id,video_id)
{
    var request = $.ajax({
        url: "/course/"+course_id+"/video/"+video_id+"/",
        type: "POST",
        data: {},
        dataType: "text",
        beforeSend: function(request) {
            return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
        }
    }).done(function( msg ) {
        console.log(msg)
        $("#player").flowplayer({
          playlist: [
             [
                { mp4: msg }
             ]
          ],
          splash: true,
        }); 
        flowplayer().load(msg);      
    }).fail(function( jqXHR, textStatus) {
       console.log('Url :' +  textStatus)
    });
}
