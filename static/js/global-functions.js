function getBrowserApp () {
  var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
  var isFirefox = typeof InstallTrigger !== 'undefined';
  var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
  var isIE = false || !!document.documentMode;
  var isEdge = !isIE && !!window.StyleMedia;
  var isChrome = !!window.chrome && !!window.chrome.webstore;
  var isBlink = (isChrome || isOpera) && !!window.CSS;
  if(isOpera){
    return "Opera";
  }else if(isFirefox){
    return "Firefox";
  }else if(isSafari){
    return "Safari";
  }else if(isIE){
    return "IE";
  }else if(isEdge){
    return "Edge";
  }else if(isChrome){
    return "Chrome";
  }else if(isBlink){
    return "Blink";
  }else{
    return "Unknown";
  }
}
