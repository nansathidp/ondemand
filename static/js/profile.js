$(function() {
    $('#review-btn').click(function(event)
    {
        $('#pride-box').hide();
        $('#review-box').show();
        $(this).addClass('active');
        $('#pride-btn').removeClass('active');
    });
    $('#pride-btn').click(function(event)
    {
        $('#pride-box').show();
        $('#review-box').hide();
        $(this).addClass('active');
        $('#review-btn').removeClass('active');
    });
});


/*$(window).on('hashchange', function() {
      if (window.location.hash) {
          var page = window.location.hash.replace('#', '');
          if (page == Number.NaN || page <= 0) {
              return false;
          } else {
              getPosts(page,"hash");
          }
      }
});*/

$(document).ready(function() {
      $(document).on('click', '.pagination a', function (e) {
          var ajax = $($(this).parentsUntil( "body" )[4]).attr('class').split(' ')[0];
          getPosts($(this).attr('href').split(ajax+'=')[1] , ajax);

          e.preventDefault();
      });

      getPosts(1,"lesson-data");
      //getPosts(1,"teaching-data");
      getPosts(1,"pinned-data");
      //getPosts(1,"wish-data");

});

function getPosts(page,ajax_type) {
      console.log('?'+ajax_type+'=' + page)
      $.ajax({
          url : '?'+ajax_type+'=' + page,
          dataType: 'json',
          data: { ajax: ajax_type},
      }).done(function (data) {

        $('.'+ajax_type+" > .tab-results > .unstyle > .pagination").hide();
        $('.'+ajax_type+" > .tab-results > .unstyle").append(data);

        var len_result = $("."+ajax_type+" > .tab-results > .unstyle > .result").length
        var expect_result = +$("#"+ajax_type.split("-")[0]+"-counter").html()
        if(len_result >= expect_result)
          $('.'+ajax_type+" > .tab-results > .unstyle > .pagination").hide();


          //location.hash = page;
      }).fail(function () {
          alert('Posts could not be loaded.'+ajax_type);
      });
 }



$(window).scroll(function() { //detect page scroll
        var interval;
        function load_wait(loadmore){
          var link = loadmore.find("a");
          var i = 1
          interval = setInterval(function(){
            var c=0;
            var s ="";
            while(c<i){
              s+="*";
              c+=1;
            }

            if(i >=4){
              i=1;
            }

            link.html(s);
            i+=1;
          }, 150);

        }
        if($(window).scrollTop() + $(window).height() == $(document).height() )  //user scrolled to bottom of the page?
        {
            var loadmore = $(".tab-pane.active").find(".pagination");
            if(loadmore.is(':visible'))
            {
              load_wait(loadmore);
              setTimeout(function(){
                loadmore.find("a").click();
                clearInterval(interval)
              }, 2500);

            }
        }
});
