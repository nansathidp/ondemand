$(function() {
    videojs.options.flash.swf = "video-js.swf";
    var videoelement = null;
    videojs(document.getElementById('htmlvideo'), {}, function(){
      console.log('Ready');
      videoelement = this;
      this.on('play', function(e) {
        console.log('playback has started!');
      });
      this.on('pause', function(e) {
         hideVideo();
      });
      this.on('ended', function(e) {
         hideVideo();
     });
    });

    $('#play-vdo').bind('click', function() {
        $(".event-head" ).fadeOut("slow");
        $(".videoview" ).fadeIn("slow",function()
        {
            videoelement.play();
        });
    });

    $('.videoview').click(function(){
        console.log('videoview');
        // videoelement.pause();
        // hideVideo();
    });

    $('#htmlvideo').click(function(){
        console.log('htmlvideo');
        // videoelement.pause();
        // hideVideo();
    });
});