var cocodemyFilters = angular.module('cocodemyFilters', []);

cocodemyFilters
    .filter('trustAsResourceUrl', ['$sce', function($sce) {
        return function(val) {
            return $sce.trustAsResourceUrl(val);
        };
    }]);
