$('.fancybox.preview').fancybox({
    tpl: {
      wrap: '<div class="fancybox-wrap" tabIndex="-1">' +
            '<div class="fancybox-skin">' +
            '<div class="fancybox-outer">' +
            '<div id="player" data-embed="false" class="flowplayer no-toggle">' + 
            '</div></div></div></div>' 
    },
    beforeShow: function () {
		var id = $(this).attr('href').replace("#", "");
		var height = Math.round( ($( window ).width()*0.7) * 0.5 ).toString();
		jwplayer("player").setup({
            flashplayer: flashplayer,
            file: 'https://www.youtube.com/watch?v='+id,
            width: '100%',
            height: height,
        });
        jwplayer().play();
        jwplayer().onComplete( function(event){
        });
    },
    beforeClose: function () {
    }
});