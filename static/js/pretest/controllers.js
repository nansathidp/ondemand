﻿﻿var cocodemyControllers = angular.module('cocodemyControllers', ['cocodemyServices','cocodemyFilters']);

cocodemyControllers
    .controller('PatMaxMenuCtrl', function ($scope) {
        var Menu = function(link,title){
            this.link=link;
            this.title=title;
        };
        $scope.menus=[
            new Menu('','หน้าแรก'),
            new Menu('example','เฉลย PAT1 ฟรี!'),
            new Menu('setup','ซ้อมสอบ PAT1'),
            new Menu('introduction','รู้จักอาจารย์โต้ง'),
            new Menu('howto','วิธีสมัครเรียน'),
        ];
    })
    .controller('PatMaxCampaignCtrl', function ($scope,PatMax) {
        //$scope.courses = PatMax.getCampaignCourses();
        $scope.coursePackages = PatMax.getCoursePackages();
        $scope.tips = PatMax.getCampaignTips();
        $scope.articles = PatMax.getArticles();

        PatMax.getLandingObj().success(function(result){
            $scope.dekTongWorlds = result.result.student_group1_list;
            $scope.dekTongDrs = result.result.student_group2_list;
            $scope.lesson = result.result.lesson_list;
            $scope.course = result.result.course_list;
            $scope.relate_course_list = result.result.relate_course_list;
        }).error(function(){
            alert('getPreviewVDO failed');
        });

        var DekTong = function(name,achieve,year){
            this.name=name;
            this.achieve=achieve;
            this.year=year;
        };
        
        // $scope.dekTongWorlds = [];
        // $scope.dekTongDrs = [];
        // for(var i=0;i<12;i++){
        //     $scope.dekTongWorlds.push(new DekTong('ชื่อนักเรียน','ความสำเร็จ','ปัจจุบัน'));
        //     $scope.dekTongDrs.push(new DekTong('ชื่อนักเรียน','ความสำเร็จ','ปัจจุบัน'));
        // }

        var currentDate = new Date();
        var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;
        var clock = $('.countdownTime').FlipClock(diff, {
            clockFace: 'DailyCounter',
            countdown: true
        });
    })
    .controller('PreTestSetupCtrl', function ($scope,PatMax) {
        var Rule = function(choice,text){
            this.choice=choice;
            this.text=text;
        }
        $scope.rules=[
            new Rule(1,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'),
            new Rule(2,'Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.'),
            new Rule(3,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'),
            new Rule(4,'Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.'),
        ];

        $scope.targetFaculties = PatMax.getTargetFaculties();
        $scope.nodeWide = 100/($scope.targetFaculties.length-1);

        $scope.selected=8;
        createBarChart($scope.selected);
        //createRadarChart($scope.selected);
        $scope.selectFaculty=function($index,$event) {
            $event.preventDefault();
            $scope.selected = $index;
            localStorage.setItem('targetScore',$scope.targetFaculties[$index].targetScore);
            $('#myChart').remove();
            $('.graph .chart').append('<canvas id="myChart" width="600" height="400"></canvas>');
            createBarChart($scope.selected);
            //createRadarChart($scope.selected);
        };

        $scope.isSelectScore=function(){
            if(localStorage.targetScore==undefined){
                alert('กรุณาเลือกคณะที่สนใจ');
            }else{
                window.location.href='/patmax/pretest/';
            }
        };

        function createBarChart(index) {
            var data = {
                labels: [
                "เซต", 
                "ตรรกศาสตร์", 
                "เมทริซ์", 
                "ทฤษฎีจำนวนเบื้องต้น", 
                "กำหนดการเชิงเส้น", 
                "สถิติ", 
                "ตรีโกณมิติ", 
                "แคลคูลัส", 
                "โจทย์ปัญหาเชาว์คณิต", 
                "ลำดับและอนุกรม", 
                "ความน่าจะเป็น", 
                "เอกซ์โพแนนเชียลและลอการิธึม", 
                "ระบบจำนวนจริงและการแก้สมการ", 
                "เรขาคณิตวิเคราะห์และภาคตัดกรวย", 
                "ฟังก์ชั่น", 
                "เวกเตอร์", 
                "จำนวนเชิงซ้อน"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,0.8)",
                        highlightFill: "rgba(220,220,220,0.75)",
                        highlightStroke: "rgba(220,220,220,1)",
                        data: [9, 11, 13, 5, 5, 30, 25, 28, 27, 23, 26, 23, 18, 18, 15, 12, 11]
                        //data: [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
                    },
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(151,187,205,0.5)",
                        strokeColor: "rgba(151,187,205,0.8)",
                        highlightFill: "rgba(151,187,205,0.75)",
                        highlightStroke: "rgba(151,187,205,1)",
                        data: $scope.targetFaculties[index].scoreList
                    }
                ]
            };
            var options = {
                //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                scaleBeginAtZero: false,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: true,
                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - If there is a stroke on each bar
                barShowStroke: true,
                //Number - Pixel width of the bar stroke
                barStrokeWidth: 2,
                //Number - Spacing between each of the X value sets
                barValueSpacing: 5,
                //Number - Spacing between data sets within X values
                barDatasetSpacing: 1,
                responsive: true,
                //String - A legend template
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
            };
            var ctx = $("#myChart").get(0).getContext("2d");
            var myBarChart = new Chart(ctx).Bar(data, options);
        };

        function createRadarChart(index) {
            var data = {
                labels: [
                "เซต", 
                "ตรรกศาสตร์", 
                "เมทริซ์", 
                "ทฤษฎีจำนวนเบื้องต้น", 
                "กำหนดการเชิงเส้น", 
                "สถิติ", 
                "ตรีโกณมิติ", 
                "แคลคูลัส", 
                "โจทย์ปัญหาเชาว์คณิต", 
                "ลำดับและอนุกรม",
                "ความน่าจะเป็น", 
                "เอกซ์โพแนนเชียลและลอการิธึม", 
                "ระบบจำนวนจริงและการแก้สมการ", 
                "เรขาคณิตวิเคราะห์และภาคตัดกรวย", 
                "ฟังก์ชั่น", 
                "เวกเตอร์", 
                "จำนวนเชิงซ้อน"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(220,220,220,0.2)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [9, 11, 13, 5, 5, 30, 25, 28, 27, 23, 26, 23, 18, 18, 15, 12, 11]
                        //data: [30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30]
                    },
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(151,187,205,0.2)",
                        strokeColor: "rgba(151,187,205,1)",
                        pointColor: "rgba(151,187,205,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: $scope.targetFaculties[index].scoreList
                    }
                ]
            };
            var options = {
                //Boolean - Whether to show lines for each scale point
                scaleShowLine : true,
                //Boolean - Whether we show the angle lines out of the radar
                angleShowLineOut : true,
                //Boolean - Whether to show labels on the scale
                scaleShowLabels : false,
                // Boolean - Whether the scale should begin at zero
                scaleBeginAtZero : true,
                //String - Colour of the angle line
                angleLineColor : "rgba(0,0,0,.1)",
                //Number - Pixel width of the angle line
                angleLineWidth : 1,
                //String - Point label font declaration
                pointLabelFontFamily : "'Arial'",
                //String - Point label font weight
                pointLabelFontStyle : "normal",
                //Number - Point label font size in pixels
                pointLabelFontSize : 10,
                //String - Point label font colour
                pointLabelFontColor : "#666",
                //Boolean - Whether to show a dot for each point
                pointDot : true,
                //Number - Radius of each point dot in pixels
                pointDotRadius : 3,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth : 1,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius : 20,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke : true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth : 2,
                //Boolean - Whether to fill the dataset with a colour
                datasetFill : true,
                responsive:true,
                //String - A legend template
                legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
            };
            var ctx = $("#myChart").get(0).getContext("2d");
            var myRadarChart = new Chart(ctx).Radar(data, options);
        };
    })
    .controller('TrialCoursesCtrl', function ($scope,TrialCourses) {
        console.log(TrialCourses.getVideoLists());
        $scope.videoLists = TrialCourses.getVideoLists();
    })

    .controller('PreTestCtrl', function ($scope,PatMax) {
        if(localStorage.targetScore==undefined){
            window.location.href='/patmax/setup/';
        }else{
            $scope.finishPreTest = false;
            $scope.order = 0;
            //$scope.getQuestions = PatMax.getPreTestQuestions(localStorage.targetScore);

            PatMax.getPreTestQuestions(localStorage.targetScore).success(function(result){
                $scope.questionLists = result.result.question_list;
                $scope.pretest_id = result.result.pretest_id;
            }).error(function(){
                alert('Failed');
            });

            PatMax.getLandingObj().success(function(result){
                $scope.course = result.result.course_list;
            }).error(function(){
                alert('getPreviewVDO failed');
            });

            var answers = [];
            
            $scope.selectChoice=function(choice){
                answers.push({id:$scope.questionLists[$scope.order].id, answer:choice});
                //console.log('answers : %o',answers);
                isFinishedPreTest();
            };

            $scope.submitAns=function(ans){
                if(ans==''||ans==undefined||ans==null) {
                    alert('โปรดใส่คำตอบ');
                }else{
                    answers.push({id: $scope.questionLists[$scope.order].id, answer: ans});
                    //console.log('answers : %o', answers);
                    isFinishedPreTest();
                }
            };

            function isFinishedPreTest() {
                if ($scope.order < ($scope.questionLists.length - 1)) {
                    $scope.order++;
                } else {
                    var data = {
                        pretest_id: $scope.pretest_id,
                        question_answer: answers
                    };

                    //console.log('submitData : %o', data);

                    PatMax.submitPreTestAnswers(data).success(function (result) {
                        $scope.finishPreTest = true;
                        $scope.fullScore = 300;
                        $scope.targetScore = localStorage.targetScore;
                        $scope.score = result.result.result_score;
                        localStorage.removeItem('targetScore');
                    }).error(function (result) {
                        alert('Error!');
                    });
                }
            };
            /*
            $scope.selectChoice=function(choice){
                answers.push({id:$scope.questionLists[$scope.order].id ,choice:choice});
                console.log('answers : %o',answers);

                if($scope.order<($scope.questionLists.length-1)) {
                    $scope.order++;
                }else{
                    var data = {
                        pretest_id: $scope.pretest_id,
                        question_answer: answers
                    };

                    console.log('submitData : %o',data);

                    PatMax.submitPreTestAnswers(data).success(function(result){
                        $scope.finishPreTest = true;
                        $scope.fullScore = 300;
                        $scope.targetScore = localStorage.targetScore;
                        $scope.score = result.result.result_score;
                        localStorage.removeItem('targetScore');
                    }).error(function(result){
                        alert('Error!');
                    });
                }
            };
            */
        }
    })
