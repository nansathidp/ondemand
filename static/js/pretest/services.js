var cocodemyServices = angular.module('cocodemyServices', []);

cocodemyServices
    .factory('PatMax', function ($http, $q) {
        var objReturn = {};

        objReturn.getPreviewVDO = function () {
            var deferred = $q.defer();
            var promise = deferred.promise;

            var req = {
                method: 'GET',
                url: '/api/patmax/preview/video/'
            };

            $http(req)
                .success(function (result, status, headers, config) {
                    console.log("getPreviewVDO Succeeded Result: %o", result);
                    deferred.resolve(result);
                })
                .error(function (result, status, headers, config) {
                    console.log("getPreviewVDO Failed Result: %o", result);
                    deferred.reject();
                });
            promise.success = function (fn) {
                promise.then(fn);
                return promise;
            };
            promise.error = function (fn) {
                promise.then(null, fn);
                return promise;
            };
            return promise;

        };

        objReturn.getLandingObj = function () {
            var deferred = $q.defer();
            var promise = deferred.promise;

            var req = {
                method: 'GET',
                url: '/api/patmax/'
            };
            
            $http(req)
                .success(function (result, status, headers, config) {
                    console.log("getLandingObj Succeeded Result: %o", result);
                    deferred.resolve(result);
                })
                .error(function (result, status, headers, config) {
                    console.log("getLandingObj Failed Result: %o", result);
                    deferred.reject();
                });
            promise.success = function (fn) {
                promise.then(fn);
                return promise;
            };
            promise.error = function (fn) {
                promise.then(null, fn);
                return promise;
            };
            return promise;

        };

        objReturn.getCoursePackages = function(){
            var packages = [
                {
                    title:'Part 1 ข้อ 1-15 ทั้ง 6 ครั้ง',
                    image:'/static/images/pretest/course-image-01.jpg',
                    price:1390,
                    desc:'ชั่วโมง / จำนวนข้อ /บท',
                    btnText:'ดูรายละเอียดคอร์ส',
                    btnLink:'go-to-buy-course-patmax-1',
                    btnCheckoutText:'ซื้อคอร์ส PATMAX ตอนที่ 1',
                    btnCheckoutLink:'go-to-buy-course-patmax-1',
                    tryText:'ทดลองเรียนตอนที่ 1',
                    tryLink:'go-to-try-course-patmax-1'
                },
                {
                    title:'Part 2 ข้อ 16-30 ทั้ง 6 ครั้ง',
                    image:'/static/images/pretest/course-image-02.jpg',
                    price:1390,
                    desc:'ชั่วโมง / จำนวนข้อ /บท',
                    btnText:'ดูรายละเอียดคอร์ส',
                    btnLink:'go-to-buy-course-patmax-2',
                    btnCheckoutText:'ซื้อคอร์ส PATMAX ตอนที่ 2',
                    btnCheckoutLink:'go-to-buy-course-patmax-2',
                    tryText:'ทดลองเรียนตอนที่ 2',
                    tryLink:'go-to-try-course-patmax-2'
                },
                {
                    title:'Part 3 ข้อ 31-45 ทั้ง 6 ครั้ง',
                    image:'/static/images/pretest/course-image-03.jpg',
                    price:1390,
                    desc:'ชั่วโมง / จำนวนข้อ /บท',
                    btnText:'ดูรายละเอียดคอร์ส',
                    btnLink:'go-to-buy-course-patmax-3',
                    btnCheckoutText:'ซื้อคอร์ส PATMAX ตอนที่ 2',
                    btnCheckoutLink:'go-to-buy-course-patmax-2',
                    tryText:'ทดลองเรียนตอนที่ 3',
                    tryLink:'go-to-try-course-patmax-3'
                }
            ];
            return packages;
        };

        objReturn.getCampaignCourses = function () {
            var courses = [
                {
                    score:80,
                    name:'Course A',
                    image:'/static/images/pretest/course-image-01.jpg',
                    price:1190,
                    link:'go-to-course-A-page',
                    title:'PAT1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.',
                    desc:'PAT1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.',
                    teacherImg:'/static/images/pretest/teacher.jpg',
                    teacherName:'Aj. Tong',
                    institute:'HSM'
                },
                {
                    score:140,
                    name:'Course B',
                    image:'/static/images/pretest/course-image-02.jpg',
                    price:2290,
                    link:'go-to-course-B-page',
                    title:'PAT1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.',
                    desc:'PAT1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.',
                    teacherImg:'/static/images/pretest/teacher.jpg',
                    teacherName:'Aj. Tong',
                    institute:'HSM'
                },
                {
                    score:160,
                    name:'Course C',
                    image:'/static/images/pretest/course-image-03.jpg',
                    price:3390,
                    link:'go-to-course-C-page',
                    title:'PAT1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.',
                    desc:'PAT1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.',
                    teacherImg:'/static/images/pretest/teacher.jpg',
                    teacherName:'Aj. Tong',
                    institute:'HSM'
                }
            ];
            return courses;
        };

        objReturn.getCampaignTips = function () {
            var tips = [
                {
                    youtube:'https://www.youtube.com/embed/EmT3TUon7cg?showinfo=0&rel=0',
                    detail:'PPAT1 Hack คือ บริการเตรียมสอบออนไลน์แบบใหม่ที่ช่วยให้น้องๆ พร้อมสอบแอดมิชชั่น PAT1 คณิตศาสตร์ได้ทันใจ เพียงแค่ระบุคะแนน เป้าหมาย โครงข่ายปัญญาประดิษฐ์ของ PAT1 Hack ก็จะวิเคราะห์และกำหนดไกด์ไลน์การเตรียมสอบให้ โดยเลือกเฉพาะเทคนิคและแนวโจทย์ที่ง่าย',
                    teacherImg:'/static/images/pretest/teacher.jpg',
                    teacherDetail:'Aj. Tong สถาบัน HMS'
                },
                {
                    youtube:'https://www.youtube.com/embed/usDLu9aUqGs?showinfo=0&rel=0',
                    detail:'PPAT1 Hack คือ บริการเตรียมสอบออนไลน์แบบใหม่ที่ช่วยให้น้องๆ พร้อมสอบแอดมิชชั่น PAT1 คณิตศาสตร์ได้ทันใจ เพียงแค่ระบุคะแนน เป้าหมาย โครงข่ายปัญญาประดิษฐ์ของ PAT1 Hack ก็จะวิเคราะห์และกำหนดไกด์ไลน์การเตรียมสอบให้ โดยเลือกเฉพาะเทคนิคและแนวโจทย์ที่ง่าย',
                    teacherImg:'/static/images/pretest/teacher.jpg',
                    teacherDetail:'Aj. Tong สถาบัน HMS'
                }
            ];
            return tips;
        };

        objReturn.getArticles = function () {
            var articles = [
                {
                    image:'/static/images/pretest/course-image-01.jpg',
                    view:111,
                    link:'go-to-article-A-page',
                    title:'PAT1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.',
                    desc:'PAT1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.',
                    authorImg:'/static/images/pretest/teacher.jpg',
                    authorName:'Aj. Tong',
                    authorPosition:'HSM'
                },
                {
                    image:'/static/images/pretest/course-image-02.jpg',
                    view:222,
                    link:'go-to-article-B-page',
                    title:'PAT1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.',
                    desc:'PAT1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.',
                    authorImg:'/static/images/pretest/teacher.jpg',
                    authorName:'Aj. Tong',
                    authorPosition:'HSM'
                },
                {
                    image:'/static/images/pretest/course-image-03.jpg',
                    view:333,
                    link:'go-to-article-C-page',
                    title:'PAT1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.',
                    desc:'PAT1 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.',
                    authorImg:'/static/images/pretest/teacher.jpg',
                    authorName:'Aj. Tong',
                    authorPosition:'HSM'
                }
            ];
            return articles;
        };

        objReturn.getTargetFaculties = function () {
            var TargetFaculty = function(targetScore,faculty,scoreList){
                this.targetScore=targetScore;
                this.faculty=faculty;
                this.scoreList = scoreList;
            }
            var targetFaculties = [
                new TargetFaculty(50,'ศิลปศาสตร์ มช.',[3,6,4,0,0,21,0,0,0,0,0,11,0,5,0,8,5]),
                new TargetFaculty(60,'วิศวะ พระจอมเกล้า',[3,6,11,0,0,21,0,0,0,0,3,11,0,5,0,8,5]),
                new TargetFaculty(70,'ประมง ม.เกษตร',[3,6,11,0,0,21,0,0,0,0,3,11,0,5,10,8,5]),
                new TargetFaculty(80,'สิ่งแวดล้อม ม.มหิดล',[3,6,13,3,0,21,0,0,0,0,9,11,0,5,10,8,5]),
                new TargetFaculty(90,'สถาปัตย์ จุฬาฯ',[9,6,13,3,0,26,0,0,0,0,9,11,0,5,10,8,5]),
                new TargetFaculty(100,'วิทยาศาสตร์ ม.มหิดล',[9,6,13,3,0,26,0,0,0,0,9,11,0,12,10,8,5]),
                new TargetFaculty(110,'ครุศาสตร์ จุฬาฯ',[9,6,13,3,0,26,0,0,0,0,9,11,0,12,10,8,8]),
                new TargetFaculty(120,'เศรษฐศาสตร์ ม.เกษตร',[9,6,13,3,0,30,6,0,0,0,9,11,0,12,10,8,8]),
                new TargetFaculty(130,'บริหารธุรกิจ จุฬาฯ',[9,6,13,5,0,30,6,0,0,0,9,11,0,12,10,8,8]),
                new TargetFaculty(140,'รัฐศาสตร์ ม.ธรรมศาสตร์',[9,6,13,5,0,30,6,0,0,0,9,17,5,18,10,8,8]),
                new TargetFaculty(150,'อักษร จุฬาฯ',[9,6,13,5,0,30,12,0,0,0,9,17,5,18,10,8,8]),
                new TargetFaculty(160,'นิติศาสตร์ ม.ธรรมศาสตร์',[9,6,13,5,0,30,12,7,0,0,9,17,12,18,10,8,8]),
                new TargetFaculty(170,'บัญชี จุฬาฯ',[9,6,13,5,0,30,12,10,0,5,9,19,12,18,10,8,8]),
                new TargetFaculty(180,'นิเทศ จุฬาฯ',[9,11,13,5,0,30,12,14,0,5,9,19,12,18,10,8,8]),
                new TargetFaculty(190,'ทันตะ ม.ธรรมศาสตร์, นิติ จุฬาฯ',[9,11,13,5,0,30,12,17,7,5,9,19,12,18,10,8,8])
            ];
            return targetFaculties;
        };

        objReturn.getPreTestQuestions = function (score) {
            var deferred = $q.defer();
            var promise = deferred.promise;

            var req = {
                method: 'GET',
                url: '/api/patmax/question/?score='+score
            };
            $http(req)
                .success(function (result, status, headers, config) {
                    console.log("getPreTestQuestions Succeeded Result: %o", result);
                    deferred.resolve(result);
                })
                .error(function (result, status, headers, config) {
                    console.log("getPreTestQuestions Failed Result: %o", result);
                    deferred.reject();
                });
            promise.success = function (fn) {
                promise.then(fn);
                return promise;
            };
            promise.error = function (fn) {
                promise.then(null, fn);
                return promise;
            };
            return promise;
        };

        objReturn.submitPreTestAnswers = function (data) {
            var deferred = $q.defer();
            var promise = deferred.promise;

            var req = {
                method: 'POST',
                url: '/api/patmax/question/submit/',
                headers: { 'Content-Type': 'application/json' },
                data: JSON.stringify(data)
            }
            $http(req)
                .success(function (result, status, headers, config) {
                    console.log("submitPreTestAnswers Succeeded Result: %o", result);
                    deferred.resolve(result);
                })
                .error(function (result, status, headers, config) {
                    console.log("submitPreTestAnswers Failed Result: %o", result);
                    deferred.reject();
                });
            promise.success = function (fn) {
                promise.then(fn);
                return promise;
            };
            promise.error = function (fn) {
                promise.then(null, fn);
                return promise;
            };
            return promise;
        };

        return objReturn;
    })
    .factory('TrialCourses', function ($http, $q) {
        var objReturn = {};

        objReturn.getVideoLists = function(){
            var fakeData = [
                {
                    image:'/static/images/pretest/course-image-03.jpg',
                    author:'Rich Benjamin',
                    title:'My road trip through the whitest town',
                    date:'Aug 2015',
                    rate:'Funny Courageous'
                },
                {
                    image:'/static/images/pretest/course-image-03.jpg',
                    author:'Rich Benjamin',
                    title:'My road trip through the whitest town',
                    date:'Aug 2015',
                    rate:'Funny Courageous'
                },
                {
                    image:'/static/images/pretest/course-image-03.jpg',
                    author:'Rich Benjamin',
                    title:'My road trip through the whitest town',
                    date:'Aug 2015',
                    rate:'Funny Courageous'
                },
                {
                    image:'/static/images/pretest/course-image-03.jpg',
                    author:'Rich Benjamin',
                    title:'My road trip through the whitest town',
                    date:'Aug 2015',
                    rate:'Funny Courageous'
                },
                {
                    image:'/static/images/pretest/course-image-03.jpg',
                    author:'Rich Benjamin',
                    title:'My road trip through the whitest town',
                    date:'Aug 2015',
                    rate:'Funny Courageous'
                },
                {
                    image:'/static/images/pretest/course-image-03.jpg',
                    author:'Rich Benjamin',
                    title:'My road trip through the whitest town',
                    date:'Aug 2015',
                    rate:'Funny Courageous'
                },
                {
                    image:'/static/images/pretest/course-image-03.jpg',
                    author:'Rich Benjamin',
                    title:'My road trip through the whitest town',
                    date:'Aug 2015',
                    rate:'Funny Courageous'
                },
                {
                    image:'/static/images/pretest/course-image-03.jpg',
                    author:'Rich Benjamin',
                    title:'My road trip through the whitest town',
                    date:'Aug 2015',
                    rate:'Funny Courageous'
                },
                {
                    image:'/static/images/pretest/course-image-03.jpg',
                    author:'Rich Benjamin',
                    title:'My road trip through the whitest town',
                    date:'Aug 2015',
                    rate:'Funny Courageous'
                },
                {
                    image:'/static/images/pretest/course-image-03.jpg',
                    author:'Rich Benjamin',
                    title:'My road trip through the whitest town',
                    date:'Aug 2015',
                    rate:'Funny Courageous'
                }
            ];
            return fakeData;
        };

        return objReturn;
    });