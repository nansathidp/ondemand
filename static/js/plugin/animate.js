﻿$(document).ready(function () {

    var nsAnimate = window.nsAnimate || {};

    nsAnimate.homeinview = function () {
        //disable on mobile
        if (mobileDetect.mobile() != null || $('html').hasClass('ie9')) {
                $('.bounce-in, .bounce-in-down, .bounce-in-left, .bounce-in-right, .bounce-in-up, .bounce-out, .bounce-out-down, .bounce-out-left, .bounce-out-right, .bounce-out-up, .fade-in, .fade-in-down, .fade-in-down-big, .fade-in-left, .fade-in-left-big, .fade-in-right, .fade-in-right-big, .fade-in-up, .fade-in-up-delay, .fade-in-up-big, .fade-out, .fade-out-down, .fade-out-down-big, .fade-out-left, .fade-out-left-big, .fade-out-right, .fade-out-right-big, .fade-out-up, .fade-out-up-big, .flip, .flip-in-x, .flip-in-y, .flip-out-x, .flip-out-y, .light-speed-in, .light-speed-out, .rotate-in, .rotate-in-down-left, .rotate-in-down-right, .rotate-in-up-left, .rotate-in-up-right, .rotate-out, .rotate-out-down-left, .rotate-out-down-right, .rotate-out-up-left, .rotate-out-up-right, .hinge, .roll-in, .roll-out, .zoom-in, .zoom-in-down, .zoom-in-left, .zoom-in-right, .zoom-in-up, .zoom-out, .zoom-out-down, .zoom-out-left, .zoom-out-right, .zoom-out-up').css('opacity', '1');
            } else {
                $('.bounce-in').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated bounceIn');
                        }

                    });
                });

                $('.bounce-in-down').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated bounceInDown');
                        }

                    });
                });

                $('.bounce-in-left').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated bounceInLeft');
                        }

                    });
                });

                $('.bounce-in-right').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated bounceInRight');
                        }

                    });
                });

                $('.bounce-in-up').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated bounceInUp');
                        }

                    });
                });

                $('.bounce-out').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated bounceOut');
                        }

                    });
                });

                $('.bounce-out-down').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated bounceOutDown');
                        }

                    });
                });

                $('.bounce-out-left').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated bounceOutLeft');
                        }

                    });
                });

                $('.bounce-out-right').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated bounceOutRight');
                        }

                    });
                });

                $('.bounce-out-up').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated bounceOutUp');
                        }

                    });
                });

                $('.fade-in').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeIn');
                        }

                    });
                });

                $('.fade-in-down').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeInDown');
                        }

                    });
                });

                $('.fade-in-down-big').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeInDownBig');
                        }

                    });
                });

                $('.fade-in-left').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeInLeft');
                        }

                    });
                });

                $('.fade-in-left-big').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeInLeftBig');
                        }

                    });
                });

                $('.fade-in-right').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeInRight');
                        }

                    });
                });

                $('.fade-in-right-big').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeInRightBig');
                        }

                    });
                });

                $('.fade-in-up').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeInUp');
                        }

                    });
                });

		$('.fade-in-up-delay').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated animate1 fadeInUp');
                        }

                    });
                });

                $('.fade-in-up-big').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeInUpBig');
                        }

                    });
                });

                $('.fade-out').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeOut');
                        }

                    });
                });

                $('.fade-out-down').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeOutDown');
                        }

                    });
                });

                $('.fade-out-down-big').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeOutDownBig');
                        }

                    });
                });

                $('.fade-out-left').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeOutLeft');
                        }

                    });
                });

                $('.fade-out-left-big').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeOutLeftBig');
                        }

                    });
                });

                $('.fade-out-right').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeOutRight');
                        }

                    });
                });

                $('.fade-out-right-big').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeOutRightBig');
                        }

                    });
                });

                $('.fade-out-up').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeOutUp');
                        }

                    });
                });

                $('.fade-out-up-big').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated fadeOutUpBig');
                        }

                    });
                });

                $('.flip').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated flip');
                        }

                    });
                });

                $('.flip-in-x').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated flipInX');
                        }

                    });
                });

                $('.flip-in-y').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated flipInY');
                        }

                    });
                });

                $('.flip-out-x').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated flipOutX');
                        }

                    });
                });

                $('.flip-out-y').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated flipOutY');
                        }

                    });
                });

                $('.light-speed-in').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated lightSpeedIn');
                        }

                    });
                });

                $('.light-speed-out').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated lightSpeedOut');
                        }

                    });
                });

                $('.rotate-in').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated rotateIn');
                        }

                    });
                });

                $('.rotate-in-down-left').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated rotateInDownLeft');
                        }

                    });
                });

                $('.rotate-in-down-right').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated rotateInDownRight');
                        }

                    });
                });

                $('.rotate-in-up-left').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated rotateInUpLeft');
                        }

                    });
                });

                $('.rotate-in-up-right').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated rotateInUpRight');
                        }

                    });
                });

                $('.rotate-out').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated rotateOut');
                        }

                    });
                });

                $('.rotate-out-down-left').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated rotateOutDownLeft');
                        }

                    });
                });

                $('.rotate-out-down-right').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated rotateOutDownRight');
                        }

                    });
                });

                $('.rotate-out-up-left').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated rotateOutUpLeft');
                        }

                    });
                });

                $('.rotate-out-up-right').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated rotateOutUpRight');
                        }

                    });
                });

                $('.hinge').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated hinge');
                        }

                    });
                });

                $('.roll-in').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated rollIn');
                        }

                    });
                });

                $('.roll-out').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated rollOut');
                        }

                    });
                });

                $('.zoom-in').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated zoomIn');
                        }

                    });
                });

                $('.zoom-in-down').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated zoomInDown');
                        }

                    });
                });

                $('.zoom-in-left').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated zoomInLeft');
                        }

                    });
                });

                $('.zoom-in-right').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated zoomInRight');
                        }

                    });
                });

                $('.zoom-in-up').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated zoomInUp');
                        }

                    });
                });

                $('.zoom-out').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated zoomOut');
                        }

                    });
                });

                $('.zoom-out-down').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated zoomOutDown');
                        }

                    });
                });

                $('.zoom-out-left').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated zoomOutLeft');
                        }

                    });
                });

                $('.zoom-out-right').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated zoomOutRight');
                        }

                    });
                });

                $('.zoom-out-up').each(function (index, element) {
                    $(this).bind('inview', function (event, visible) {
                        if (visible == true) {
                            $(this).addClass('animated zoomOutUp');
                        }

                    });
                });
            }
    }

    //init
    nsAnimate.homeinview();
});
