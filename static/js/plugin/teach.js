$(function(){
    $("#form-tutor").submit(function(event) {
        var element = this;
        var request = $.ajax({
            url: base_path+"/teach/action/",
            type: "POST",
            data: { 
                institute : this.institute.value,
                email : this.email.value,
                tel : this.tel.value,
                subject : this.subject.value,
            },
            dataType: "json",
            beforeSend: function(request) {
               return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
            }
        }).done(function( msg ) {
            f(msg.status == 200){
                $(element).trigger('reset');
                $('#modal-teach').modal('show');
            }
        }).fail(function( jqXHR, textStatus ) {
        });
        event.preventDefault(); //STOP default action
    });
});
