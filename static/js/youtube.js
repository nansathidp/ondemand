// Load the IFrame Player API code asynchronously.

//#var tag = document.createElement('script');
//#tag.src = "https://www.youtube.com/player_api";
//#var firstScriptTag = document.getElementsByTagName('script')[0];
//#firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// Replace the 'ytplayer' element with an <iframe> and
// YouTube player after the API code downloads.
var player;

var is_mobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );

function onYouTubePlayerAPIReady() {
  var source = $('#play-vdo').attr("rel");
  player = new YT.Player('ytplayer', {
    height: '480',
    width: '100%',
    playerVars: { 'autoplay': 0, 'controls': 2 },
    videoId: source,
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}

function downloadVideoScript() {
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/player_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

function onPlayerReady(event) {
    if( !is_mobile )
        player.playVideo();
}

var done = false;
var is_active = false;
function onPlayerStateChange(event) {
  console.log('onPlayerStateChange : ' + event.data);
  if (event.data == YT.PlayerState.PLAYING && !done) {
  // setTimeout(stopVideo, 6000);
  // done = true;
    console.log('PLAYING');
    is_active = true;
  }
  else if(event.data == YT.PlayerState.PAUSED ){
    console.log('PAUSED');
  }else if(event.data == YT.PlayerState.ENDED )
  {
    console.log('ENDED');
    if( nextVideo ) {
        setTimeout( function(){
            window.location = "/lesson/"+nextVideo+"/?autoplay=yes";
        }, 5000);
    }
  }
}

function stopVideo() {
  player.stopVideo();
}

$(function() {

$('#play-vdo').bind('click', function() {
    $("#vdo-cover").fadeOut( "fast", function() {
      $("#video-section").fadeIn("slow",function(){
          downloadVideoScript();
      });
    });
    
});

$('.videoview').click(function(){
    //if( !is_active) {
    //    player.pauseVideo();
    //    hideVideo();
    //}
});

});
