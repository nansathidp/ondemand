window.fbAsyncInit = function() {
    FB.init({
	appId      : facebook_id,
	status     : true,
	xfbml      : true,
	version    : 'v2.5'
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

var HTTPSTATE = {
    LOGIN: 0,
    CONNECT: 1, 
};
function facebookEmail(email){   
    if(email === undefined)
        email = -1
    return email
}

function getFbRegister(element,access_token,email)
{

	var request = $.ajax({
		url: "/account/fblogin/",
		type: "POST",
		data: { access_token : access_token, email : facebookEmail(email) },
		dataType: "json",
		beforeSend: function(request) {
			return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
		}
	}).done(function( msg ) {
        if(msg.status == 200){
            window.location.replace(msg.result.link);
        }else{
            $('.error-msg',element).fadeIn().html(msg.status_msg);
            $('.button-loading').fadeOut('fast');
        }
	}).fail(function( jqXHR, textStatus) {
	  $('.button-loading').fadeOut('fast');
	});
    event.preventDefault(); 
}

function getFbConnect(access_token,email)
{
    var request = $.ajax({
        url: "/account/fbconnect/",
        type: "POST",
        data: { access_token : access_token, email : facebookEmail(email) },
        dataType: "json",
        beforeSend: function(request) {
            return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
        }
    }).done(function( msg ) {
        if(msg.status == 200){
            $('#fbconnect-error-msg,#fbconnect-button').hide();
            $('#fbconnect-success-msg').fadeIn();
            if( callback_function ){
                callback_function();
            }
        }else{
            $('#fbconnect-error-msg').fadeIn().html(msg.status_msg);
            $('.button-loading').fadeOut('fast');
            $('#fbconnect-success-msg').fadeOut();
        }
    }).fail(function( jqXHR, textStatus) {
      $('.button-loading').fadeOut('fast');
    });
}

function FbLogin(element,type)
{
    try {
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                if(type == HTTPSTATE.LOGIN)
                    FB.api('/me?fields=id,name,email,birthday', function(me) {
                        getFbRegister(element,response.authResponse.accessToken,me.email);
                    });
                else if(type == HTTPSTATE.CONNECT)
                    FB.api('/me?fields=id,name,email,birthday', function(me) {
                        getFbConnect(response.authResponse.accessToken,me.email);
                    });
            }
            else {
                FB.login(function(response) {
                    if (response.status === 'connected') {
                        if(type == HTTPSTATE.LOGIN)
                            FB.api('/me?fields=id,name,email,birthday', function(me) {
                                getFbRegister(element,response.authResponse.accessToken,me.email);
                            });
                        else if(type == HTTPSTATE.CONNECT)
                            FB.api('/me?fields=id,name,email,birthday', function(me) {
                                getFbConnect(response.authResponse.accessToken,me.email);
                            });
                    } else if (response.status === 'not_authorized') {
                        $('.button-loading').fadeOut('fast');
                    } else {
                        $('.button-loading').fadeOut('fast');
                    }
            }, {scope: 'public_profile,email,user_friends,publish_actions,user_birthday'});
            }
        });
        $('.button-loading').fadeIn('fast');
    }
    catch(err) {
        console.log(err);
    }
	
}

function FbLoginForQuizAreanFriendScore()
{
    try{
        FB.getLoginStatus( function( response ){
            if ( response.status === 'connected' ){
                FB.api('/me/friends', function( response ) {
                    if( response && !response.error ) {
                        var friend_list_objects = response.data;
                        var friend_list_id  = [];
                        for(var index = 0 in friend_list_objects) {
                            friend_list_id.push( friend_list_objects[index].id );
                        }
                        if( friend_list_id ) {
                            $.ajax({
                                type: "POST",
                                url: get_friend_score_url,
                                data: { 'friend_list' : JSON.stringify(friend_list_id) },
                                dataType: "html",
                                beforeSend: function(request) {
                                    return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
                                }
                            }).done( function( html ) {
                                $("#friend-score-table").html( html );
                                $("#friendscore-content").show();
                            });
                        }
                    }
                });
            }
            else {
                $("#connect-with-facebook").show();
            }
        });
    }
    catch(err){
        console.log(err);
    }
}

var callback_function = null;
function connectFB(_callback)
{
   callback_function = typeof _callback !== 'undefined' ? _callback : null; 
   FbLogin(null,HTTPSTATE.CONNECT);
}

$(function() {
	$( "#fb-login,#fb-signup" ).click(function() {
        FbLogin($(this).parent().parent().find('form'),HTTPSTATE.LOGIN);
    });
    $('#loginModal,#signupModal')
    .on('show.bs.modal', function (e) {
        $('.error-msg').html('');
    })

});


