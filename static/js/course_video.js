$(document).ready(function() {
  
    // $(".section-menu").each(function(){
    //   var submenu = $(this).attr("data-target");
    //   if ($(submenu+"i").hasClass("fa-play-circle") ){
    //     $(this).find("i").addClass("fa-play-circle")
    //   }else if( $(submenu+"i").hasClass("fa-circle-thin") ){
    //     $(this).find("i").addClass("fa-circle-thin")
    //   }else{
    //     $(this).find("i").addClass("fa-check-circle")
    //   }
    // })
    
    $('.menulink').bigSlide({
        "init":function(){
            $(".vdo-option").width( $(".wrap").width());
        },
        "open":function(){
            $(".vdo-option").width( $(".wrap").width());
            $("#vdo-mode").html('<i class="fa fa-expand"></i> Full Mode');
        },
        "closed" : function(){
            $(".vdo-option").width( $(".wrap").width());
            $("#vdo-mode").html('<i class="fa fa-compress"></i> Default Mode');
        },
    });

    // setInterval(videotimer, 1000);
});

function videotimer()
{
  console.log('videotimer');
      var request = $.ajax({
        url: "/api/v2/accessibility/00/",
        type: "POST",
        cache:false,
        dataType: "text",
        beforeSend: function(request) {
            return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
        }
    }).done(function( msg ) {    
    }).fail(function( jqXHR, textStatus) {
       console.log('Url :' +  textStatus)
    });
}



function init(course_id,video_id)
{   
    var request = $.ajax({
        url: "/course/"+course_id+"/video/"+video_id+"/",
        type: "POST",
        cache:false,
        dataType: "text",
        beforeSend: function(request) {
            return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
        }
    }).done(function( msg ) {
        flowplayer(function (api, root) {
          api.bind("load", function (video,callback) {
            console.log('load');
          }).bind("finish", function () {
            console.log('finish');
            setTimeout(redirectNext, 2000);
          });
        });
        $("#player").flowplayer({
          playlist: [
             [
                { mp4: msg }
             ]
          ],
          splash: true,
        }); 
        flowplayer().load(msg);
        var version = flowplayer.version;
        flowplayer.conf = {
           splash: true,
           autoplay: true,
           analytics: "UA-27182341-1"
        };     
    }).fail(function( jqXHR, textStatus) {
       console.log('Url :' +  textStatus)
    });
}


function initurl(path)
{
    flowplayer(function (api, root) {
      api.bind("load", function (video,callback) {
        console.log('load');
      }).bind("finish", function () {
        console.log('finish');
        setTimeout(redirectNext, 2000);
      });
    });
    $("#player").flowplayer({
      playlist: [
         [
            { mp4: path }
         ]
      ],
      splash: true,
    }); 
    flowplayer().load(path);
    var version = flowplayer.version;
    flowplayer.conf = {
       splash: true,
       autoplay: true,
       analytics: "UA-27182341-1"
    };  
}


function initYoutube(videoId)
{
    var autoplay = 1;
    var is_mobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );
    if(is_mobile)
      autoplay = 0

    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    var player;
    function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
      height: '100%',
      width: '100%',
      videoId: videoId,
      playerVars: { 'autoplay': autoplay, 'controls': 1 },
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
      }
    });
    }
    function onPlayerReady(event) {
        event.target.playVideo();
    }
    var done = false;
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED) {
          setTimeout(redirectNext, 1500);
        }
    }
    function stopVideo() {
        player.stopVideo();
    }
}
