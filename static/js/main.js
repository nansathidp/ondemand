var is_notification_load = false
var SLICK_OPTIONS = {
    dots: true,
    arrows: false,
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    centerMode: true,
    variableWidth: true,
    autoplay: true,
    autoplaySpeed: 5000,
    lazyLoad: 'ondemand',
    responsive: [
    {
        breakpoint: 768,
        settings: {
          arrows: false,
          variableWidth: false,
          centerMode: false,
        }
    }
  ]
};

$(function() {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
     $('html').addClass('ismobile');
    }

    initScrollEvent();
    initFormLogin();
    notificationHandle();

    var windowHeight = $(window).height();
    $('.sub-menu').css({'max-height':(windowHeight-60)+'px'});
    $('.btn-open-search').click(function(){
      $('#header').addClass('open-search');
      $('.search input').focus();
    });
    $('.btn-close-search').click(function(){
      $('#header').removeClass('open-search');
    });
    $('.open-side-menu').click(function(){
      $('body').addClass('side-menu-opened');
      $('.body-overlay').addClass('show');
    });
    $('.close-side-menu, .body-overlay').click(function(){
      $('body').removeClass('side-menu-opened');
      $('.body-overlay').removeClass('show');
    });
    $('.has-secondary-menu').click(function(){
      $('#side-menu-wrapper').addClass('secondary-menu-opened');
      $('#side-menu-wrapper').find('ul.'+$(this).attr('data-menu')).addClass('opened');
    });
    $('.close-secondary-menu').click(function(){
      $('#side-menu-wrapper').removeClass('secondary-menu-opened');
      $(this).closest('ul').removeClass('opened');
    });

    $('ul.left-menu li.hidden-some-menu').each(function(i,e){

      if(i==localStorage.hiddenSomeMenuShowAll){
        $(this).addClass('showAll');
      }
    });
    $('.hidden-some-menu .btn-show-all').click(function(){
        var parent = $(this).closest('.hidden-some-menu');
        localStorage.setItem("hiddenSomeMenuShowAll",parent.index('ul.left-menu li.hidden-some-menu'));
        $(this).closest('.hidden-some-menu').addClass('showAll');
    });
    $('.hidden-some-menu .btn-hide').click(function(){
        var parent = $(this).closest('.hidden-some-menu');
        localStorage.removeItem("hiddenSomeMenuShowAll");
        $(this).closest('.hidden-some-menu').removeClass('showAll');
    });

    $("#form-review,#form-rating").submit(function(event) {
  			event.preventDefault();
  			var form = $(this);
  			var doAjax = true;
  			var request_url = null;
  			var request_url = "/rating/push/";
  			if(this.ratinghidden.value == 0){
  			    doAjax = false;
  			    form.parent().find('.alert-empty-rating').fadeIn('slow');
  			}
  			if(doAjax){
  			    var request = $.ajax({
  				url: request_url,
  				type: "POST",
  				data: {
  				    content_id : this.content_id.value,
  				    content_type : this.content_type.value,
  				    message : this.message.value,
  				    rating : this.ratinghidden.value
  				},
  				dataType: "json",
  				beforeSend: function(request) {
  				    return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
  				}
  			    }).done(function(msg){
  				if(msg.status == 200){
  				    $('#reviewlist').append('<h4 class="bold main-color">We got your review. Thank you!</h4>'+msg.result.template).hide().fadeIn(1000);
  				    $("#form-review,#form-rating").trigger('reset');
  				    $('#textcount').text(300);
  				    $('.noreview, #review-form, a[data-target="#rateContentModal"]').hide();
  				    form.parent().find('.alert-empty-rating').hide();
  				    form.parent().find('select').val(0);
  				    $('#rateContentModal').modal('hide');
  				    $('html, body').animate({ scrollTop: ($("#review").offset().top)-($(window).height()-$("#review").outerHeight()-100) }, 500);
  				}else{
  				    $('#loginModal').modal('show');
  				}
  			    }).fail(function( jqXHR, textStatus ) {
  				$('#loginModal').modal('show');
  			    });
  			}
  			event.preventDefault();
  		});

      $('#header li.browse').click(function(){
        if($('body').width()>=993){
          if($('body').hasClass('has-left-menu')){
            $('body').removeClass('has-left-menu');
            $(this).removeClass('open');
          }else{
            $('body').addClass('has-left-menu');
            $(this).addClass('open');
          }
        }
      });

      var crop;
      $('#profile-image-input').change(function(){
        var input = $(this);
        var file = this.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/jpg", "image/gif"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]) || (imagefile == match[3]))) {
            alert('Please select image file (.jpg .png .gif)');
            return false;
        // } else if(file.size > 3000*1024) {
        //     alert('Limit file size at 500 kb');
        //     return false;
        } else {
            var reader = new FileReader();
            var image = new Image();

            reader.readAsDataURL(file);
            reader.onload = function (e) {
                image.src = e.target.result;
                image.onload = function () {
                        $('#cropImageModal').fadeIn();
                        $('.crop-image-overlay').addClass('show');
                        crop = $('.crop').croppie({
                          viewport: { width: 250, height: 250, type: 'square' },
                          boundary: { width: 300, height: 300 },
                          enableOrientation: true
                        });

                        crop.croppie('bind', {
                            url: image.src,
                        });

                        $('body').on('click','.btn-crop', function(){
                          crop.croppie('result','blob').then(function(blob) {
                            var formData = new FormData();
                            formData.append('profile', blob, 'abc.png');
                            $.ajax({
                                url: "/account/image/upload/",
                                type: "POST",
                                data: formData,
                                contentType: false,
                                cache: false,
                                processData: false,
                                beforeSend: function(request) {
                                    return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
                                },
                                success: function (data) {
                                    console.log('Upload Success');
                                    crop.croppie('destroy');
                                    $('#cropImageModal').fadeOut();
                                    $('.crop-image-overlay').removeClass('show');
                                    input.val('');
                                }
                            });
                            console.log(formData);
                          });

                          crop.croppie('result','base64').then(function(base64) {
                              $('.profile-image .avatar').attr('src', base64);
                          });

                        });
                };
                image.onerror = function () {
                    alert('Invalid file type: ' + file.type);
                };
            };
        }
      });

      $('body').on('click','#cropImageModal .close-modal', function(){
        crop.croppie('destroy');
        $('#cropImageModal').fadeOut();
        $('.crop-image-overlay').removeClass('show');
      });

      $('body').on('click','.btn-rotate-left', function(){
          crop.croppie('rotate',-90);
      });
      $('body').on('click','.btn-rotate-right', function(){
          crop.croppie('rotate',90);
      });

      $(".count-word").each(function() {
          var count = $(this);
          var formtext = $(this).prev();
          var init = $(this).attr("form-init");
          count.html(init - $(formtext).val().length);
          $(formtext).keyup(function(event) {
              count.html(init - $(formtext).val().length);
          })
      });

    $(".fixed-top").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    $('a').click(function (event) {
        var Href = $(this).attr('href');
        if (Href == '#') {
            event.preventDefault();
        }
    });

    $("body").mousedown(function() {
        return true;
    });
});

function modalWalkThrough(modalId,lastIndex){
  SLICK_OPTIONS.autoplay = false;
  SLICK_OPTIONS.infinite = false;
  var modal = $('#'+modalId)
  var walkthrough = modal.find('ul');
  walkthrough.slick(SLICK_OPTIONS);

  var open = function(){
      walkthrough.slick("slickGoTo",0);
      modal.modal('show');
      setTimeout(function(){
          walkthrough.addClass('show-slide');
      },1000);
  }

  if(!getCookie('howToWalkthrough')){
      open();
      setCookie('howToWalkthrough', true, 365)
  }

  $('.btn-open-walkthrough').click(open);

  modal.on('hidden.bs.modal', function () {
      walkthrough.removeClass('show-slide');
  })
  modal.find('.btn-prev').click(function(){
      walkthrough.slick("slickPrev");
  });
  modal.find('.btn-next').click(function(){
      if(walkthrough.slick('slickCurrentSlide')==lastIndex){
          setTimeout(function(){
              modal.modal('hide');
          },1000);
      }else{
          walkthrough.slick("slickNext");
      }
  });
  modal.on('afterChange', function(event, slick, currentSlide){
    if(currentSlide==0){
      $('.btn-prev').fadeOut();
    }else{
      $('.btn-prev').fadeIn().css({'display':'inline-block'});
    }
    if(currentSlide==lastIndex){
      $('.btn-next').text('Got it!');
    }else{
      $('.btn-next').text('Next');
    }
  });
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function initFormLogin() {
    $("#form-login").submit(function(event) {
        var element = this;
        var request = $.ajax({
            url: "/account/login/",
            type: "POST",
            data: {
                email: this.email.value,
                password: this.password.value,
                ref: this.ref.value,
                next: this.next.value
            },
            dataType: "json",
            beforeSend: function(request) {
                return request.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
            }
        }).done(function(msg) {
            if (msg.status == 200) {
                $('#loginModal').modal('hide');
                window.location = msg.result.link;
            }
            else {
                swal(
                  'Unauthorized',
                  'The username or password you entered is incorrect.',
                  'error'
                );
            }
        }).fail(function(jqXHR, textStatus, msg) {
          if(jqXHR.status == 401){
            swal(
              'Unauthorized',
              'The username or password you entered is incorrect.',
              'error'
            );
          }else if(jqXHR.status == 403){
            // window.location = window.location;
            // window.location.reload(); Ban IP
            swal(
              'Account locked',
              'Too many login attempts. Please try again later.',
              'error'
            );
          }
          console.log('#form-login fail');
        });
        event.preventDefault();
    });

    $("#skip-button").click(function(event) {
        var element = $("#form-login");
        var request = $.ajax({
            url: "/account/login/",
            type: "POST",
            data: {
                skip: 1,
            },
            dataType: "json",
            beforeSend: function(request) {
                return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
            }
        }).done(function(msg) {
            if (msg.status == 200) {
                link = msg.result.link;
                if (link.substr(-13) != "?autoplay=yes") {
                    link = link + "?autoplay=yes";
                }
                $('#loginModal').modal('hide');
                window.location = link;
            } else {
                $('.error-msg', element).fadeIn().html("Can't Skip");
            }
        }).fail(function(jqXHR, textStatus) {
            console.log('#form-login fail');
        });
        event.preventDefault();
    });

    $(".form-signup").submit(function(event) {
        var validate = true;
        var element = this;
        if (this.password.value != this.password_confirm.value)
            validate = false;
        if (validate) {
            var request = $.ajax({
                url: "/account/auth/",
                type: "POST",
                data: {
                    fullname: this.fullname.value,
                    email: this.email.value,
                    tel: this.tel.value,
                    password: this.password.value,
                    ref: this.ref.value
                },
                dataType: "json",
                beforeSend: function(request) {
                    return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
                }
            }).done(function(msg) {
                if (msg.status == 200) {
                    $('#signupModal').modal('hide');
                    console.log(msg.result.link);
                    window.location = msg.result.link;
                } else {
                    $('.error-msg', element).fadeIn().html(msg.status_msg);
                }
            }).fail(function(jqXHR, textStatus) {
                $('.error-msg', element).fadeIn().html('Wrong assword');
            });
        } else {
            $('.error-msg', element).fadeIn().html('รหัสผ่านไม่ถูกต้อง');
        }
        event.preventDefault();
    });

    function submitFBLogin() {
        var request = $.ajax({
            url: '',
            type: "POST",
            data: {
                email: email,
                password: password
            },
            dataType: "json",
            beforeSend: function(request) {
                return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
            }
        }).done(function(msg) {}).fail(function(jqXHR, textStatus) {});
    }
}

function switchSignup() {
    $('#loginModal').modal('hide');
    $('#signupModal').modal('show');
}

function switchLogin() {
    $('#signupModal').modal('hide');
    $('#loginModal').modal('show');
}

function openSignupModal(path) {
    $('.modal-promotion-header').show();
    $('#signupModal').modal('show');
    $("input[name*='ref']").val(path);
}

function openLoginModal(path) {
    $('.modal-promotion-header').show();
    $('#loginModal').modal('show');
    $("input[name*='ref']").val(path);
}

function dismissEvent() {
    $('.header-event').fadeOut(100);
}

function initScrollEvent() {
    $('.scroller-list .event').mouseover(function() {
        var activeCourse = $(this);
        var content = activeCourse.find('.event-hidden-inner');
        var contentHeight = content.innerHeight();
        content.parent().css('height', contentHeight);
    });

    $('.scroller-list .event').mouseleave(function() {
        var activeCourse = $(this);
        var content = activeCourse.find('.event-hidden-inner');
        content.parent().css('height', '0');
    });

    $('.scroller-list-lesson .event').mouseover(function() {
        var activeCourse = $(this);
        var content = activeCourse.find('.event-hidden-inner');
        var contentHeight = content.innerHeight();
        content.parent().css('height', contentHeight);
    });

    $('.scroller-list-lesson .event').mouseleave(function() {
        var activeCourse = $(this);
        var content = activeCourse.find('.event-hidden-inner');
        content.parent().css('height', '0');
    });
}

$(".btn.file").click(function() {
    var file = $(this).attr('file-toggle');
    console.log(file);
    $(file).click();
})

$("#send-new-email-button").click(function() {
    var email = $("#new-email").val();
    if (email) {
        $("#error-msg").hide();
        $.ajax({
            url: '/mailer/resend_verify_email_with_specific_email/',
            type: 'POST',
            data: {
                email: email
            },
            dataType: "json",
            beforeSend: function(request) {
                return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
            }
        }).done(function(msg) {
            if (msg.status == 200) {
                $("#modal-content-new-email").html('<h4 class="text-center">Email has been send to your address</h4>');
                window.location.reload();
            } else {
                $('.error-msg', element).fadeIn().html(msg.status_msg);
            }
        });
        return;
    } else {
        $("#error-msg").html("Please Enter Your Email");
        $("#error-msg").show();
    }
});

$("#resend-email-button").click(function() {
    resendVerifyEmail();
});

$("#resend-email-button-profile-edit").click(function() {
    resendVerifyEmail();
});

$('body').on('click','.noti-message',function(){
  data = $(this).attr("data")
});

function readnoti(id, isRead){
  if(isRead == "False"){
    $.ajax({
      url: '/inbox/read/',
      type: "POST",
      data: { inbox_id : id }
    }).done(function(result) {
      $( this ).addClass( "done" );
      if(result.result.count < 1){
        $("#notificationBell").removeClass("new-alert");
      }
    });
  }
}

function resendVerifyEmail() {
    $.ajax({
        url: '/mailer/resend_verify_email/',
        type: 'POST',
        dataType: "json",
        beforeSend: function(request) {
            return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
        }
    }).done(function(msg) {
        if (msg.status == 200) {
            $("#modal-content").html('<h4 class="text-center">Email has been send to your address</h4>');
            $("#emailVerifyResendModal").modal('show');
        }
    });
}

function notify(message, delay) {
    delay = typeof delay !== 'undefined' ? delay : 3000;
    $.notify({
        message: message,
    }, {
        type: 'danger',
        timer: 1000,
        delay: delay,
        z_index: 100031,
        showProgressbar: false,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        placement: {
            from: "top",
            align: "center"
        },
    });
}

function acceptTerms(url) {
    var request = $.ajax({
        url: url,
        type: "GET",
        dataType: "json"
    })
}

function goSignup() {
    $('#suggestSignupModal').modal('hide');
    $('#signupModal').modal('show');
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function notificationHandle() {
    $('.notification-dropdown').on('mouseenter', function() {
        var notification_footer = $(this).find('.dropdown-footer');
        var notification_empty = $(this).find('.dropdown-empty');
        if (!is_notification_load) {
            var request = $.ajax({
                url: '/inbox/popup/item/',
                type: "GET",
                dataType: "json"
            }).done(function(msg) {
                $( ".noti-message" ).remove();
                if (msg.status == 200) {
                    is_notification_load = true;
                    if (msg.result.inbox_list.length > 0) {
                        notification_empty.remove();
                        msg.result.inbox_list.forEach(function(inbox) {
                            notification_footer.before(inbox);
                        });
                    } else {
                        notification_footer.remove();
                    }
                }
            });
        }
    });
}
