$(function() {
    var iframe = $('#vimeoplayer')[0];
    var player = $f(iframe);
    var status = $('.status');

    // // When the player is ready, add listeners for pause, finish, and playProgress
    player.addEvent('ready', function() {
        status.text('ready');
        player.addEvent('pause', onPause);
        player.addEvent('finish', onFinish);
        player.addEvent('ready', onReady);
        // player.addEvent('playProgress', onPlayProgress);
    });

    $('#play-vdo').bind('click', function() {
        $(".event-head" ).fadeOut("slow");
        $(".videoview" ).fadeIn("slow",function()
        {
            player.api('play');
        });
    });

    $('.videoview').click(function(){
        player.api('pause');
        hideVideo();
    });

    function onPause(id) {
        hideVideo();
    }
    function onFinish(id) {
        hideVideo();
    }
    function onReady(id) {
        if($('.videoview').is(':visible')) {
            player.api('play');
        }
    }
    function onPlayProgress(data, id) {
        // status.text(data.seconds + 's played');
    }

});