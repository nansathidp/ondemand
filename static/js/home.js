function filterClick(type,title,filter)
{
	$('#dropdown-select').html(title);
	$('#filter').val(filter);
	console.log($('#filter').val());
	getCourseByTag(type,$('#filter').val(),$('#tag').val(),6);
}

function getCourseByTag(type,filter,tag,take)
{
	tag = tag.toLowerCase() == 'all' ? '0' : tag;
	if(type == 'lesson'){
		$('#lesson-section').html('');
		$('#no-lesson').hide();
	}else {
		$('#course-section').html('')
		$('#no-course').hide();
	}
	
	$('.content-loading').fadeIn('fast');
	var url =  base_path+'/getcoursebytag/'+type+'/'+filter+'/'+tag+'?take='+take;
	console.log(url);
	var request = $.ajax({
		url:url,
		type: "POST",
		dataType: "json",
		beforeSend: function(request) {
			return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
		}
	}).done(function( msg ) {

		$('.content-loading').fadeOut('fast');
		if(type == 'lesson')
			$('#lesson-section').html('');
		else if(type == 'course')
			$('#course-section').html('');
		if(msg.content.length > 0)
		{
			$.each(msg.content, function(key,value) {
				if(type == 'lesson'){
					$('#lesson-section').append(value);
					console.log(type);
				}
			 	else if(type == 'course'){
			 		$('#course-section').append(value);
			 		console.log(type);
			 	}
   			 });
			if(type == 'lesson')
				$('#lesson-section').hide().delay( 400 ).fadeIn('slow');
			else if(type == 'course')
				$('#course-section').hide().delay( 400 ).fadeIn('slow');
			console.log(type);
			initScrollEvent();
		}
		else
		{
			$('.content-loading').fadeOut('fast');
			if(type == 'lesson')
			{
				$('#no-lesson').delay( 400 ).fadeIn('fast');
			}
			else if(type == 'course')
			{
				$('#no-course').delay( 400 ).fadeIn('fast');
			}
			console.log(type);
				
		}
	}).fail(function( jqXHR, textStatus) {
		$('.content-loading').fadeOut('fast');
		if(type == 'lesson')
			$('#no-lesson').fadeIn('fast');
		else if(type == 'course')
			$('#no-course').fadeIn('fast');
	});
}

$(function()   {

	$('#dropdownMenu1').on('show.bs.dropdown', function () {
	  console.log('dropdownMenu1');
	})
	
	$('.trends-list a').click(function() {
		$('.trends-list a').removeClass('active');
		$(this).addClass('active')
  		console.log($(this).html());
  		$('#tag').val($(this).html());
  		getCourseByTag('course',$('#filter').val(),$('#tag').val(),6);
  		getCourseByTag('lesson',$('#filter').val(),$('#tag').val(),6);
	});
});


