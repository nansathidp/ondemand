var count = 10;

function iconcheck(div){
    $(div+" > a > span").addClass("green").removeClass("red");
    $(div+" > a > span > i").addClass("icon-check").removeClass("icon-cancel");
}

function iconcancel(div){
    $(div+" > a > span").addClass("red").removeClass("green");
    $(div+" > a > span > i").addClass("icon-cancel").removeClass("icon-check");
}

function image_preview(file,part){
    var url = URL.createObjectURL(file);
    var div = document.createElement('div');
    $(div).attr("style"," background-image: url("+url+")")
    $(div).addClass("preview");
    
    if(part == "slide"){
        $(div).addClass("newimg");
        $(div).attr("ref-hidden", "newimg-"+count);
        readImage(file,"newimg[]","newimg-"+count);

        count+=1;
    }
    return div;
}

function moreSlideImg(){
    var tmp = $("#hint-slide").html();
    $("#hint-slide").html("<span class='red' style='font-size: 15px;'>FILE not compatible OR more than 4 image</span>");
    setTimeout(function(){
        $("#hint-slide").fadeOut();
        $("#hint-slide").html(tmp);
        $("#hint-slide").fadeIn();
    },1000);
}

/*
$('form').keyup(function(e) {
    var code = e.keyCode || e.which;
   if (code == '9') {
        var newval = $("#tmptag").val();
        if( newval.length >= 1){


            var oldval = $("#tag").val();
            var tag = "<a>"+$("#tmptag").val()+' <i class="icon icon-cancel"></i></a>'
            $(".trends-list").append(tag);
            $("#tmptag").val("")
            $("#tag").val(oldval+","+newval)
            console.log("add tag : "+newval)
        }
    }
 });




$('body').on('click', ".trends-list .icon-cancel", function(e){
    $(this).parent().hide();
    var txt = $(this).parent()[0].childNodes[0].data;
    txt = txt.trim();
    var oldval = $("#tag").val();
    $("#tag").val(oldval+",*"+txt)
    console.log("del tag : "+txt);
});


*/

function readImage(file,name,id) {
    
    var FR= new FileReader();
    FR.onload = function(e) {
        createHidden(name, e.target.result,id)
    };       

    FR.readAsDataURL( file );
    
}


function createHidden(name,val,id){

    var input = document.createElement("input");
    input.setAttribute("type", "hidden");
    input.setAttribute("class", "slideimg");
    input.setAttribute("name", name);
    input.setAttribute("value", val);

    //console.log(id);
    input.setAttribute("id", id);
    
    $("#edit-course").append(input);
}


$('body').on('click','.preview',function(e){
    $(this).fadeOut();
    if( $(this).hasClass("oldimg")){
        
        var name = $(this).attr("ref-hidden");
        createHidden( "oldimg[]" , name.split("-")[1] , "oldimg-"+name.split("-")[1]);

    }else{
        var name = $(this).attr("ref-hidden");
        $("#"+name).remove();
    }

    //var img =  readImage($("#slide")[0]);
    
})