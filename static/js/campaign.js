$('.fancybox.preview').fancybox({
    tpl: {

      wrap: '<div class="fancybox-wrap" tabIndex="-1">' +
      '<div class="fancybox-skin">' +
      '<div class="fancybox-outer">' +
      '<video id="example-video" autoplay width="900" height="500"'+
      'poster="{{institute.image.url}}" controls>'+
      '<source src="" type="video/mp4"></video>' + 
      '</div></div></div>' 
  },
  beforeShow: function () {
    var exampleVideo = document.getElementById("example-video");
    console.log(exampleVideo)
    exampleVideo.setAttribute('src', $(this).attr('title'));
    exampleVideo.play();
},
helpers : {
    title : null            
}   
});