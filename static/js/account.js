$("#form-address").submit(function(event) {
    var element = this;
    var request = $.ajax({
        url: "/account/address/",
        type: "POST",
        data: { 
            name : this.name.value , 
            address : this.address.value ,
            subdistrict : this.subdistrict.value ,
            district : this.district.value ,
            province : this.province.value ,
            zipcode : this.zipcode.value ,
            tel : this.tel.value ,
        },
        dataType: "json",
        beforeSend: function(request) {
            return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
        }
    }).done(function( msg ) {
        if(msg.status == 200)
        {
            $('#display-name').text(element.name.value);
            $('#display-address').text(element.address.value);
            $('#display-district').text(element.subdistrict.value +' , '+element.district.value);
            $('#display-zipcode').text(element.province.value+' '+element.zipcode.value);
            $('#display-tel').text(element.tel.value);

            $('#input-name').val(element.name.value);
            $('#input-address').val(element.address.value);
            $('#input-subdistrict').val(element.subdistrict.value);
            $('#input-district').val(element.district.value);
            $('input[name=province]').val(element.province.value);
            $('#input-zipcode').val(element.zipcode.value);
            $('#input-tel').val(element.tel.value);

            $('#panal-address').show();
            $('#button-address').show();
            $('#form-address').hide();
            $('#button-address').html('แก้ไขที่อยู่ในการจัดส่งหนังสือ');
            is_complete_address = true;
        }else{
            console.log('Erroe');
        }
    }).fail(function( jqXHR, textStatus) {
        console.log('#form-login fail');
    });
    event.preventDefault(); 
});