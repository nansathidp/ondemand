$(document).ready(function() {
  $(".slide-panel.col-4").owlCarousel({
    items : 4,
    itemsDesktop : [1150,4],
    itemsMobile : false,
    pagination : false,
  });  
  $(".slide-panel.col-5").owlCarousel({
    items : 5,
    itemsDesktop : [1150,5],
    itemsMobile : false,
    pagination : false,
  });
});
$(".slide-arw.next").click(function(){
  console.log('next');
  $(this).parent().siblings(".slide-panel").trigger('owl.next');
})
$(".slide-arw.prev").click(function(){
  console.log('prev');
  $(this).parent().siblings(".slide-panel").trigger('owl.prev');
})
$(".scroller-mask-event")
  .mouseenter(function(){
      $(this).children(".navigation-panel").fadeIn();
  }).mouseleave(function(){ 
      $(this).children(".navigation-panel").fadeOut();
});