$(function(){
    $("#form-teach").submit(function(event) {
        var element = this;
        console.log(this)
        $.ajax({
            url: "/teach/action",
            type: "POST",
            data: { 
                institute : this.institute.value,
                email : this.email.value,
                tel : this.tel.value,
                subject : this.subject.value,
            },
            dataType: "json",
            beforeSend: function(request) {
                return request.setRequestHeader('X-CSRFToken', $.cookie('csrftoken'));
            }
        }).done(function( msg ) {
            $(element).trigger('reset');
            $('#modal-teach').modal('show');
        }).fail(function( jqXHR, textStatus ) {
         
        });
        event.preventDefault(); //STOP default action
    });
});
