$(function(){
	$("#form-become").submit(function(event) {
		var request = $.ajax({
			url: base_path+"/service/subscribe",
			type: "POST",
			data: { 
				institute : this.institute.value,
				location : this.location.value,
				subject : this.subject.value,
				description : this.description.value,
			},
			dataType: "json",
			beforeSend: function(request) {
				return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
			}
		}).done(function( msg ) {
			// var Response =  JSON.stringify(msg);
			$("#form-become").trigger('reset');
			// $("#display-message").fadeIn('slow');
			$('#wait-queue').html(msg.count);
			$('#becomeModal').modal('show');
		}).fail(function( jqXHR, textStatus ) {
			// console.log("Request failed: " + textStatus);
			if(jqXHR.responseJSON.code == 481){
				$('#loginModal').modal('show');
			}else{
			}
		});
		event.preventDefault(); //STOP default action
	});
});
