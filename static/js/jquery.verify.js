(function($) {


    $.fn.verify = function( options ) {

        // Establish our default settings
        var settings = $.extend({
            prepare         : null,
            before          : null,
            input           : [],
            condition       : {},
            attr            : {},
        }, options);

        return this.each( function() {
            
            /*if ( settings.color ) {
                $(this).css( 'color', settings.color );
            }*/

            if ( $.isFunction( settings.prepare ) ) {
                settings.prepare.call( this );
            }

            $.each(settings.input,function(i,v){
                var selector = v;
                var obj = $(selector);

                if(settings.attr.hasOwnProperty(selector))
                    var attr = settings.attr[selector];
                else
                    var attr = {};


                obj.change(function(e){
                    
                    if($.isFunction( settings.before)){
                        if(attr.hasOwnProperty("tick"))
                            settings.before.call(this,attr["tick"])
                    }


                    if(settings.condition.hasOwnProperty(selector)){

                        if(settings.condition[selector].call(this)){

                            if(attr.hasOwnProperty("tick"))
                                iconcheck(attr["tick"]);

                            if(attr.hasOwnProperty("pass")){
                                attr["pass"].call(this);
                            }
                                

                        }else{
                            if(attr.hasOwnProperty("tick"))
                                iconcancel(attr["tick"]);

                            if(attr.hasOwnProperty("fail")){
                                attr["fail"].call(this);
                            }
                                
                        }
                    }
                        

                })

            })


        });

    }

}(jQuery));
