function initialize(lat,lon,title) {
    var myLatlng=  new google.maps.LatLng(lat,lon);
    var mapOptions = {
     center: myLatlng,
     draggable : false,
     streetViewControl : false,
     mapTypeControl : false,
     scrollwheel : false,
     language : 'en',
     zoom: 14
   };
   var map = new google.maps.Map(document.getElementById("map-canvas"),
     mapOptions);
   var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    title: title
  });
 }