jQuery(function( $ ){
	/**
	 * Most jQuery.localScroll's settings, actually belong to jQuery.ScrollTo, check it's demo for an example of each option.
	 * @see http://flesler.demos.com/jquery/scrollTo/
	 * You can use EVERY single setting of jQuery.ScrollTo, in the settings hash you send to jQuery.LocalScroll.
	 */
	
	// The default axis is 'y', but in this demo, I want to scroll both
	// You can modify any default like this
	$.localScroll.defaults.axis = 'y';
	
	/**
	 * NOTE: I use $.localScroll instead of $('#navigation').localScroll() so I
	 * also affect the >> and << links. I want every link in the page to scroll.
	 */
	// $.localScroll({
	// 	target: '.nav li', // could be a selector or a jQuery object too.
	// 	queue:true,
	// 	duration:1000,
	// 	hash:false,
	// 	onBefore:function( e, anchor, $target ){
	// 		// The 'this' is the settings object, can be modified
	// 		console.log('onBefore');
	// 	},
	// 	onAfter:function( anchor, settings ){
	// 		// The 'this' contains the scrolled element (#content)
	// 		console.log('onAfter');
	// 	}
	// });

	$('.nav li').localScroll({
			duration:1000,
			offset: -70
	});
	var offset = 0;
	$('#home').waypoint(function() {
		$( ".navbar-nav" ).each(function() {
			$(this).children('li').removeClass('active');
		});
		$( ".navbar-nav li:nth-child(1)" ).addClass('active');
	}, { offset: offset });
	$('#home-footer').waypoint(function() {
		$( ".navbar-nav" ).each(function() {
			$(this).children('li').removeClass('active');
		});
		$( ".navbar-nav li:nth-child(1)" ).addClass('active');
	}, { offset: 250 });
	$('#about,#about-footer').waypoint(function(direction) {
		$( ".navbar-nav" ).each(function() {
			$(this).children('li').removeClass('active');
		});
		console.log(direction);
		$( ".navbar-nav li:nth-child(2)" ).addClass('active');
	}, { offset: 80 });
	$('#sec1,#sec1-footer').waypoint(function() {
		$( ".navbar-nav" ).each(function() {
			$(this).children('li').removeClass('active');
		});
		$( ".navbar-nav li:nth-child(3)" ).addClass('active');
	}, { offset: 90 });
	$('#sec2,#sec2-footer').waypoint(function() {
		$( ".navbar-nav" ).each(function() {
			$(this).children('li').removeClass('active');
		});
		$( ".navbar-nav li:nth-child(4)" ).addClass('active');
	}, { offset: 90 });
	$('#sec3,#sec3-footer').waypoint(function() {
		$( ".navbar-nav" ).each(function() {
			$(this).children('li').removeClass('active');
		});
		$( ".navbar-nav li:nth-child(5)" ).addClass('active');
	}, { offset: 90 });
	$('#sec4').waypoint(function() {
		$( ".navbar-nav" ).each(function() {
			$(this).children('li').removeClass('active');
		});
		$( ".navbar-nav li:nth-child(6)" ).addClass('active');
	}, { offset: 90 });
	$('#sec4-footer').waypoint(function() {
		$( ".navbar-nav" ).each(function() {
			$(this).children('li').removeClass('active');
		});
		$( ".navbar-nav li:nth-child(6)" ).addClass('active');
	}, { offset: 250 });
	$('#sec5').waypoint(function() {
		$( ".navbar-nav" ).each(function() {
			$(this).children('li').removeClass('active');
		});
		console.log(this);
		$( ".navbar-nav li:nth-child(7)" ).addClass('active');
	}, { offset: 250 });
	

});