from django import template
from django.conf import settings

from content.models import Location
from utils.content import get_content, get_content_include
from utils.content_type import get_content_type_name

register = template.Library()


@register.filter
def content_readable(content_type_name):
    if content_type_name == 'activity':
        return 'test'
    elif content_type_name == 'task':
        return 'to-do'
    else:
        return content_type_name


@register.filter
def check_none(content):
    if content:
        return content
    else:
        return '-'


@register.simple_tag
def get_content_tag(content_type_id, content_id):
    return get_content(content_type_id, content_id)


@register.simple_tag
def get_content_include_tag(content_type_id, content_id):
    return get_content_include(content_type_id, content_id)


@register.simple_tag
def pull_content_location_tag(id):
    return Location.pull(id)


@register.simple_tag
def pull_content_location_first_tag(content_type, content_id,
                                    parent1_content_type, parent1_content,
                                    parent2_content_type, parent2_content,
                                    parent3_content_type, parent3_content):
    return Location.pull_first(content_type, content_id,
                               parent1_content_type, parent1_content,
                               parent2_content_type, parent2_content,
                               parent3_content_type, parent3_content)


@register.simple_tag
def pull_content_type_tag(id):
    return settings.CONTENT_TYPE_ID(id)


@register.simple_tag
def get_content_type_tag(app, model):
    return settings.CONTENT_TYPE(app, model)


@register.simple_tag
def get_content_type_name_tag(content_type, id=None):
    return get_content_type_name(content_type, id)
