from django.conf import settings
from django.core.cache import cache

from .models import Location
from utils.cached.time_out import get_time_out, get_time_out_day, get_time_out_week

def cached_content_location(id):
    key = '%s_content_location_%s' % (settings.CACHED_PREFIX, id)
    result = cache.get(key)
    if result is None:
        result = Location.objects.filter(id=id).first()
        if result is not None:
            cache.set(key, result, get_time_out_day())
        else:
            return None
    return result

def _content_type_id(_):
    if _ is None:
        return -1
    else:
        return _.id
    
def cached_content_location_first(content_type, content_id,
                                  parent1_content_type, parent1_content,
                                  parent2_content_type, parent2_content,
                                  parent3_content_type, parent3_content):
    content_type_id = _content_type_id(content_type)
    parent1_content_type_id = _content_type_id(parent1_content_type)
    parent2_content_type_id = _content_type_id(parent2_content_type)
    parent3_content_type_id = _content_type_id(parent3_content_type)
    key = '%s_content_location_first_%s_%s_%s_%s_%s_%s_%s_%s' % (settings.CACHED_PREFIX,
                                                                 content_type_id, content_id,
                                                                 parent1_content_type_id, parent1_content,
                                                                 parent2_content_type_id, parent2_content,
                                                                 parent3_content_type_id, parent3_content)
    result = cache.get(key)
    if result is None:
        result = Location.objects.filter(content_type=content_type,
                                         content=content_id,
                                         parent1_content_type=parent1_content_type,
                                         parent1_content=parent1_content,
                                         parent2_content_type=parent2_content_type,
                                         parent2_content=parent2_content,
                                         parent3_content_type=parent3_content_type,
                                         parent3_content=parent3_content) \
                                   .first()
        if result is None:
            result = Location.objects.create(content_type=content_type,
                                             content=content_id,
                                             parent1_content_type=parent1_content_type,
                                             parent1_content=parent1_content,
                                             parent2_content_type=parent2_content_type,
                                             parent2_content=parent2_content,
                                             parent3_content_type=parent3_content_type,
                                             parent3_content=parent3_content)
        cache.set(key, result, get_time_out_day())
    return result
