from django.contrib import admin

from .models import Location

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ('id', 'content_type', 'content',
                    'parent1_content_type', 'parent1_content',
                    'parent2_content_type', 'parent2_content',
                    'parent3_content_type', 'parent3_content')
    
    # parent1_content_type.short_description = 'PCT1'
