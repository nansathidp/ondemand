from django.db import models


class Location(models.Model):
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    content = models.BigIntegerField(db_index=True)
    
    parent1_content_type = models.ForeignKey('contenttypes.ContentType', null=True, related_name='+')
    parent1_content = models.BigIntegerField(default=-1, db_index=True)

    parent2_content_type = models.ForeignKey('contenttypes.ContentType', null=True, related_name='+')
    parent2_content = models.BigIntegerField(default=-1, db_index=True)

    parent3_content_type = models.ForeignKey('contenttypes.ContentType', null=True, related_name='+')
    parent3_content = models.BigIntegerField(default=-1, db_index=True)
    
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    
    def __str__(self):
        return str(self.id)

    @staticmethod
    def pull(id):
        from .cached import cached_content_location
        return cached_content_location(id)
    
    @staticmethod
    def pull_first(content_type, content_id,
                   parent1_content_type=None, parent1_content=-1,
                   parent2_content_type=None, parent2_content=-1,
                   parent3_content_type=None, parent3_content=-1):
        # import inspect
        # print(content_type, content_id,
        #       parent1_content_type, parent1_content,
        #       parent2_content_type, parent2_content,
        #       parent3_content_type, parent3_content)
        # for _ in inspect.stack()[:4]:
        #     print(_[1], _[3])
        # print('----------')
        from .cached import cached_content_location_first
        return cached_content_location_first(content_type, content_id,
                                             parent1_content_type, parent1_content,
                                             parent2_content_type, parent2_content,
                                             parent3_content_type, parent3_content)

    def get_parent(self):
        from django.conf import settings
        from program.models import Program
        from course.models import Course
        from utils.content import get_content

        if self.parent1_content_type_id is not None and self.parent2_content_type_id is not None:
            if self.parent1_content_type_id == settings.CONTENT_TYPE('course', 'course').id:
                parent = Course.pull(self.parent1_content)
                try:
                    name = 'In Course: %s' % (parent.name)
                except:
                    name = '-'
            else:
                parent = Program.pull(self.parent1_content)
                course = Course.pull(self.parent2_content)
                try:
                    name = 'In Program: %s -> In Course: %s' % (parent.name, course.name)
                except:
                    name = '-'
        elif self.parent1_content_type_id is not None and self.parent2_content_type_id is None:
            if self.parent1_content_type_id == settings.CONTENT_TYPE('program', 'onboard').id:
                parent = Program.pull(self.parent1_content)
                name = 'In Onboard: %s' % parent.name
            elif self.parent1_content_type_id == settings.CONTENT_TYPE('program', 'program').id:
                parent = Program.pull(self.parent1_content)
                name = 'In Program: %s' % parent.name
            elif self.parent1_content_type_id == settings.CONTENT_TYPE('course', 'course').id:
                parent = Course.pull(self.parent1_content)
                name = 'In Course: %s' % parent.name
            else:
                parent = None
                name = '-'
        else:
            parent = get_content(self.content_type_id, self.content)
            name = 'Stand alone'
        return {'content': parent,
                'name': name}


class Level(models.Model):
    name = models.CharField(max_length=120)
    sort = models.IntegerField(db_index=True, default=0)


class Starter(models.Model):
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    content = models.BigIntegerField(db_index=True)
    sort = models.IntegerField(default=0)


class Type(models.Model):
    name = models.CharField(max_length=24)
    image = models.ImageField(upload_to='content/type/', null=True, blank=True)
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
