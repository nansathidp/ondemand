from django.conf.urls import url, include

app_name = 'api'
urlpatterns = [
    # API v5. (reusable apps)
    url(r'^v5/', include('api.v5.urls')),
]
