from django.http import HttpResponse, JsonResponse
import json

STATUS_MSG = {
    191: 'API Old Version. (Not Use And Remove It!!)',
    199: 'Fail.',
    200: 'Success.',
    201: 'Require POST method.',
    202: 'Require UUID.',
    203: 'Base64 TypeError: Incorrect padding',
    204: 'Require token.',
    205: 'Require parameter.',
    206: 'Account ของคุณถูกใช้ Login เกิน 3 เครื่อง',
    207: 'Account ของคุณ ไม่สามารถ Login ใน App นี้ได้ กรุณา SingUp',
    208: 'Require response.',
    209: 'Require Store.',
    210: 'Require Content Type (type=?)',
    211: 'Content Type: Not Allow.',
    212: 'Require Tel',
    213: 'Require Content',
    214: 'Object Not Allow for this Account',
    215: 'Require OTP',
    216: 'Require Captcha Password',
    217: 'Can\'t get Price Level (call /api/v4/payment/list/ befor)',
    218: 'Require Transaction ID (transaction_id)',
    219: 'Require Pass Key (pass_key)',
    220: 'Require App Version',

    300: 'The username or password were incorrect.',  # The username or password were incorrect.
    301: 'การเชื่อมต่อกับ Facebook มีปัญหา กรุณาตรวจสอบและลองอีกครั้ง [301]',# Facebook Login : access token cannot be null
    302: 'Facebook Login : Please Signup.',  # Facebook Login : Please Signup.
    303: 'การเชื่อมต่อกับ Facebook มีปัญหา กรุณาตรวจสอบและลองอีกครั้ง [303]',  # Facebook Login : access token fail.
    304: 'การเชื่อมต่อกับ Facebook มีปัญหา กรุณาตรวจสอบและลองอีกครั้ง [304]',# Facebook Connect : access token cannot be null.
    305: 'การเชื่อมต่อกับ Facebook มีปัญหา กรุณาตรวจสอบและลองอีกครั้ง [305]',  # Facebook Connect : access token fail.
    306: 'มีความผิดพลาดเกิดขึ้นจากการเข้าสู่ระบบ โปรดลองใหม่อีกครั้ง [306]',  # The username or password were incorrect.
    307: 'Duplicate Facebook Account',  # The username or password were incorrect.
    308: 'Require Access token',  # The username or password were incorrect.
    309: 'UUID Account not found.',
    310: 'อีเมล์นี้มีผู้ใช้แล้ว โปรดเข้าสู้ระบบ',
    311: 'มีความผิดพลาดเกิดขึ้นจากการสมัครสมาชิก โปรดลองใหม่อีกครั้ง',
    312: 'สมัครสามาชิกด้วย Facebook ไม่สมบูรณ์, โปรดสมัครสมาชิกด้วย Email',
    313: 'บัญชี้นี้ไม่อยู่ใน Onboarding Program',

    400: 'Account not Found , Please try again.',  # Account not Found.
    450: 'Acount : image not found.',  # Acount : image not found.
    460: 'Need login permission',  # Acount : image not found.
    455: 'Acount : Address update fail.',  # Acount : image not found.

    500: 'Quiz Exception.',  # Quiz Exception.

    600: 'อีเมลนี้ไม่สามารถใช้สมัครได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง',  # Regis : invalid e-mail address
    601: 'การตั้งรหัสผ่านต้องเกิน 4 ตัวอักษรขึ้นไป',  # Regis : password less than 4 characters
    602: 'อีเมลนี้ถูกสมัครใช้งานแล้ว',  # Regis : email already exists
    603: 'การเชื่อมต่อกับ Facebook มีปัญหา กรุณาตรวจสอบและลองอีกครั้ง [603]', # Regis with Facebook : access token fail.
    650: 'Regis : token cannot be null',  # Regis : token cannot be null

    660: 'กรุณากรอกเบอร์โทรศัพท์ 10 หลักให้ถูกต้อง',
    661: 'Require OTP ID.',
    662: 'OTP Not Found.',
    663: 'Account ของคุณมีสถานะเป็น AIS Privilege อยู่แล้ว',
    664: 'เบอร์โทรศัพท์นี้ถูกใช้ไปแล้ว.',
    665: 'หมายเลข IP Address ของคุณไม่ใช่เครือข่าย AIS',
    666: 'ไม่สามารถระบุ เบอร์โทรศัพท์ของคุณได้.',

    700: 'Item Not Found.',
    701: 'Certify Not Found.',
    702: 'Require parameter subject and level',
    703: 'Store not Allowed.',
    704: 'Subscribe only Free courses',

    710: 'Not Price (<= 0.0)',
    711: 'Order Not Permission',
    712: 'PaysBuy GateWay Error.',
    713: 'Free item only',
    714: 'Course Price is None',
    715: 'Item Exists',

    720: 'Order Item: Not Found.',
    721: 'Order Item: Wait Payment.',
    723: 'Order Item: Expired.',
    724: 'Order Price Exception',
    725: 'Permission denied',
    730: 'No device detect',
    740: 'Require Duration.',
    741: 'Duration Format Fail.',
    742: 'Duration Greater than Video.',
    743: 'Order method is not match',

    800: 'Tutor not found',

    # 900 Subject
    900: 'Subject not found',

    1000: 'Institutes not found',

    1100: 'Tag not found',
    1110: 'Duplicate entry',

    1200: 'Lesson not Found',
    1210: 'Lesson models exception',

    1300: 'Course not Found',

    1400: 'Promotion code is not valid',

    1500: 'Question Set not Found',
    1510: 'Please Submit Score',
    1520: 'Pretest ID Not Found',
    1530: 'This Pretest Has Finish',

    #Activity
    3001: 'Required type.',
    3002: 'Type: format Fail.',
    3003: 'Type: not Allow.',
    3004: 'Activity not found.',
    3005: 'Required Question.',
    3006: 'Choice not found.',
    3007: 'Question and Choice not found.',
    3011: 'Test not submit.',

    4000: 'Program not found',
    4100: 'Task not found',
    4200: 'Terms not found',

    5000: 'Unknown Exception',

    # Live 6000
    # Provider 7000

    # Content ( Rating ) 9000
}

SECTION_TYPE = {
    0: 'Highlight',
    1: 'Subject',
    2: 'Institute',
    3: 'Tutor',
    4: 'Course',
    5: 'Event',
    6: 'Bundle',
    7: 'Question',
    8: 'Program',
    9: 'Provider',

}

SECTION_GROUP_TYPE = {
    -1: 'No Group',
    0: 'Featured',
    1: 'By Level',
    2: 'By Category',
    3: 'View All',
    4: 'Sort by Latest',
    5: 'Sort by Popular'
}


def json_render(result, status):
    data = {'status': status,
            'status_msg': STATUS_MSG[status],
            'result': result}
    return JsonResponse(data)


def init_section(section_name, section_type, section_group_type, section_id, is_more):
    d = dict()
    d['is_more'] = is_more
    d['section_id'] = section_id
    d['section_name'] = section_name
    d['section_type'] = section_type
    d['section_type_display'] = SECTION_TYPE[section_type]
    d['section_group_type'] = section_group_type
    d['section_group_type_display'] = SECTION_GROUP_TYPE[section_group_type]
    return d


def client_upload_key(key):
    if key == 'mGilcYYHaKnehuvTzSWwF7TK4lLLiLOwUvzLe5V7M=':
        return True
    else:
        return False
