from django.shortcuts import render
from django.http import HttpResponse

from app.models import App

import json

def json_render(result, status=200, indent=2, content_type='application/json', location=None):
    response = HttpResponse(json.dumps(result, indent=indent),
                            status=status,
                            content_type=content_type)
    if location is not None:
        response['Location'] = location
    return response

def check_key(request):
    code = request.GET.get('code', None)
    if code is None:
        return json_render({}, status=401)
    else:
        try:
            app = App.objects.get(code=code)
            request.APP = app
        except:
            return json_render({}, status=401)

    key = request.GET.get('key', None)
    try:
        key = app.key_set.get(code=key)
    except:
        return json_render({},
                           status=403)
    return None


def check_require(request):
    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)
    if uuid is None:
        return 202, token, uuid, None
    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return 703, token, uuid, store
    except:
        return 209, token, uuid, store
    return 200, token, uuid, store

def dashboard_view(request):
    return json_render({'msg': 'Hello.'}, 200)
