import datetime
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from api.app.views import json_render, check_key

from account.models import Account, Device # , App as AccountApp #FIXME @kidsdev

from api.views_api import STATUS_MSG
from api.views_ip import get_client_ip
import random, string, urllib
from open_facebook import OpenFacebook

#import logging
#logger = logging.getLogger(__name__)

@csrf_exempt
def login_view(request):
    result = {}
    response = check_key(request)
    if response is not None:
        return response
    if request.method == 'POST':
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        uuid = request.POST.get('uuid', None)
    else:
        email = request.GET.get('email', None)
        password = request.GET.get('password', None)
        uuid = request.GET.get('uuid', None)

    if uuid is None:
        result['status'] = 202
        result['status_msg'] = STATUS_MSG[202]
        return json_render(result)

    if email is None or len(email) < 4 or password is None or len(password) < 4:
        result['status'] = 300
        result['status_msg'] = STATUS_MSG[300]
        return json_render(result)
    else:
        email = urllib.parse.unquote(email)
        password = urllib.parse.unquote(password)

        account = authenticate(email=email, password=password)
        if account is not None:

            # if Device.objects.filter(account=account).count() >= 3:
            #     result['status'] = 206
            #     result['status_msg'] = STATUS_MSG[206]
            #     return json_render(result)
            
            #if AccountApp.objects.filter(app=request.APP, account=account).count() == 0:
            #    result['status'] = 207
            #    result['status_msg'] = STATUS_MSG[207]
            #    return json_render(result)
                    
            result['token'] = account.login_token()
            account.update_uuid(uuid, is_login_facebook=None)
            account.device_log(uuid, 1) #Basic Login
            result['profile'] = account.api_profile()
            result['status'] = 200
            result['status_msg'] = STATUS_MSG[200]
        else:
            result['status'] = 300
            result['status_msg'] = STATUS_MSG[300]
            return json_render(result)
    return json_render(result,
                       status=200)


@csrf_exempt
def login_facebook_view(request):
    result = {}
    response = check_key(request)
    if response is not None:
        return response
    if request.method == 'POST':
        access_token = request.POST.get('access_token', None)
        uuid = request.POST.get('uuid', None)
        token = request.POST.get('token', None)
    else:
        access_token = request.GET.get('access_token', None)
        uuid = request.GET.get('uuid', None)
        token = request.GET.get('token', None)

    if uuid is not None and len(uuid) != 0:
        pass
    else:
        result['status'] = 202
        result['status_msg'] = STATUS_MSG[202]
        return json_render(result)
        
    if access_token is None or len(access_token) == 0:
        result['status'] = 301
        result['status_msg'] = STATUS_MSG[301]
        return json_render(result)
    else:
        try:
            graph = OpenFacebook(access_token=access_token, version='v2.5')
            profile = graph.get('me', fields='id,name,email,birthday,gender')
            
            #graph = facebook.GraphAPI(access_token)
            #profile = graph.get_object('me')
            account_list = Account.objects.filter(facebook_id=profile['id'])
            account = None
            if account_list.count() == 0:
                if uuid is not None and len(uuid) > 0:
                    account_list = Account.objects.filter(uuid=uuid)
                    if account_list.count() > 0:
                        account = account_list[0]
                        account.facebook_id = profile['id']
                        if 'email' in profile:
                            account.email = profile['email']
                        else:
                            account.email = '%s@facebook.com.gen'%(profile['id'])
                if account is None:
                    #Create New User
                    password = ''.join(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(12))
                    if 'email' in profile:
                        email = profile['email']
                    else:
                        email = '%s@facebook.com.gen'%(profile['id'])
                    
                    account_list = Account.objects.filter(email=email)
                    if account_list.count() > 0:
                        account = account_list[0]
                        account.facebook_id = profile['id']
                    else:
                        account = Account.objects.create_user(email, password)
                        account.facebook_id = profile['id']        

                #Update
                if 'name' in profile:
                    account.first_name = profile['name']
                if 'sername' in profile:
                    account.last_name = profile['sername']
                if 'gender' in profile:
                    if profile['gender'].lower() == 'm' or profile['gender'].lower() == 'male':
                        account.gender = 1
                    elif profile['gender'].lower() == 'f' or profile['gender'].lower() == 'female':
                        account.gender = 2
                    else:
                        account.gender = 0
                if 'birthday' in profile:
                    try:
                        account.birthday = datetime.datetime.strptime(profile['birthday'], '%d/%m/%Y').date()
                    except:
                        account.birthday = None
                if not account.is_upload:
                    account.api_facebook_image()
                account.save()
                # AccountApp.api_app_push(request.APP, account)

                # if Device.objects.filter(account=account).count() >= 3:
                #     result['status'] = 206
                #     result['status_msg'] = STATUS_MSG[206]
                #     return json_render(result)

                account.update_uuid(uuid, is_login_facebook=True)
                account.device_log(uuid,2) #Login Facebook
                account.api_facebook_friend(graph)

                result['token'] = account.login_token()
                result['profile'] = account.api_profile()
                result['status'] = 200
                result['status_msg'] = STATUS_MSG[200]
            else:
                account = account_list[0]
                # AccountApp.api_app_push(request.APP, account)
                if Device.objects.filter(account=account).count() >= 3:
                    result['status'] = 206
                    result['status_msg'] = STATUS_MSG[206]
                    return json_render(result)

                account.update_uuid(uuid, is_login_facebook=True)
                account.device_log(uuid,2) #Login Facebook
                account.api_facebook_friend(graph)
                result['token'] = account.login_token()
                result['profile'] = account.api_profile()
                result['status'] = 200
                result['status_msg'] = STATUS_MSG[200]

        except:
            #import traceback
            #logger.info('access_token: %s'%access_token)
            #logger.info('token: %s'%token)
            #logger.info('uuid: %s'%uuid)
            #logger.error(traceback.format_exc())
            result['status'] = 303
            result['status_msg'] = STATUS_MSG[303]
            return json_render(result)
    return json_render(result, 200)
