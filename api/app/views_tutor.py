from api.app.views import json_render, check_key
#from tutor.models import App as TutorApp
from tutor.models import Tutor

def tutor_view(request):
    result = {}
    response = check_key(request)
    if response is not None:
        return response
    result['result'] = []
    for tutor in Tutor.objects.all():
        result['result'].append(tutor.api_display())

    return json_render(result,
                       status=200)
