from functools import wraps


def param_filter(view_func):
    def _decorator(request, *args, **kwargs):
        request.provider_id = request.GET.get('provider', 'all')
        request.category_id = request.GET.get('category', 'all')
        request.sort = request.GET.get('sort', 'latest')
        request.page = request.GET.get('page', 1)
        request.is_display_provider = request.GET.get('is_display_provider', 't')
        request.is_display_category = request.GET.get('is_display_category', 't')
        response = view_func(request, *args, **kwargs)
        return response
    return wraps(view_func)(_decorator)
