from django.conf import settings
from django.conf.urls import url, include

from .views_config import config_view
from .views_feature import feature_view
from .views_flatpages import about_view, contact_view, term_view
from .views_my_library import library_view
from .views_my_library_remove import library_remove_view
from .views_search import search_view

urlpatterns = [
    url(r'^feature/$', feature_view),  # TODO: testing
    url(r'^my-library/$', library_view),  # TODO: testing
    url(r'^my-library/(\d+)/remove/$', library_remove_view),  # TODO: testing
    url(r'^config/$', config_view),  # TODO: testing
    url(r'^search/$', search_view),  # TODO: testing

    url(r'^account/', include('account.api.v1.urls')),  # TODO: testing
    url(r'^course/', include('course.api.v1.urls')),  # TODO: testing
    url(r'^question/', include('question.api.v1.urls')),  # TODO: testing
    url(r'^program/', include('program.api.v1.urls')),  # TODO: testing
    url(r'^inbox/', include('inbox.api.v1.urls')),  # TODO: testing

    url(r'^category/', include('category.api.v1.urls')),  # TODO: testing

    url(r'^term/', include('term.api.v1.urls')),  # TODO: testing
    url(r'^tutor/', include('tutor.api.v1.urls')),  # TODO: testing
    url(r'^task/', include('task.api.v1.urls')),  # TODO: testing
    url(r'^lesson/', include('lesson.api.v1.urls')),  # TODO: testing
    url(r'^live/', include('live.api.v1.urls')),  # TODO: testing
    url(r'^provider/', include('provider.api.v1.urls')),  # TODO: testing
    url(r'^notification/', include('notification.api.v1.urls')),  # TODO: testing

    url(r'^rating/', include('rating.api.v1.urls')),  # TODO: testing
    url(r'^about/$', about_view),
    url(r'^contact/$', contact_view),
    url(r'^term/$', term_view),
]

if settings.PROJECT == 'unilever':
    urlpatterns += [
        url(r'^unilever/', include('unilever.api.v1.urls')),  # TODO: testing
    ]
