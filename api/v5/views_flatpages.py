from django.contrib.flatpages.models import FlatPage
from api.views_api import json_render
import json


def _view(request, flatpage):
    if request.method == "GET":
        if flatpage is None:
            return json_render({}, 700)
        flatpage_item = {
            'id': flatpage.id,
            'title': flatpage.title,
            'content': flatpage.content
        }
        return json_render(flatpage_item, 200)
    else:
        return json_render({}, 700)


def about_view(request):
    flatpage = FlatPage.objects.filter(url="/about/").first()
    return _view(request, flatpage)


def contact_view(request):
    flatpage = FlatPage.objects.filter(url="/contact/").first()
    return _view(request, flatpage)


def term_view(request):
    flatpage = FlatPage.objects.filter(url="/term/").first()
    return _view(request, flatpage)

