from account.models import Account
from order.models import Item
from .views import json_render, check_key


def library_remove_view(request, order_item_id):
    result = {}
    code = 200
    response = check_key(request)
    if response is not None:
        return response

    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)
    if uuid is None:
        return json_render(result, 202)
    if token is None:
        return json_render(result, 204)
    account = Account.api_auth(token, uuid)
    if account is None:
        return json_render(result, 400)

    item = Item.objects.get(id=order_item_id, order__account=account)
    if item:
        item.is_display = False
        item.save(update_fields=['is_display'])
    else:
        code = 720

    return json_render(result, code)
