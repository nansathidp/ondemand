import json
from django.http import HttpResponse

from api.views_api import STATUS_MSG
from app.cached import cached_api_app
from key.cached import cached_api_key


def json_render(result, code=None, status=200, indent=2, content_type='application/json', location=None):
    if code is None:
        data = result
    else:
        data = {'status': code,
                'status_msg': STATUS_MSG[code],
                'result': result}
    response = HttpResponse(json.dumps(data, indent=indent),
                            status=status,
                            content_type=content_type)
    if location is not None:
        response['Location'] = location
    return response


def check_key(request):
    code = request.GET.get('code', None)
    if code is None:
        return json_render({}, status=401)
    else:
        app = cached_api_app(code)
        if app is None:
            return json_render({}, status=401)
        else:
            request.APP = app

    key = request.GET.get('key', None)
    if key is None:
        return json_render({}, status=403)
    else:
        key = cached_api_key(app, key)
        if not key:
            return json_render({}, status=403)
    request.code = code
    request.key = key
    return None


def check_require(request, require_token=False):
    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)
    version = request.GET.get('version', None)
    store = None
    if uuid is None:
        return 202, token, uuid, None, version
    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return 703, token, uuid, store, version
    except:
        return 209, token, uuid, store, version
    if require_token:
        if token is None:
            return 204, token, uuid, store, version

    if version is None:
        return 220, token, uuid, store, version

    return 200, token, uuid, store, version


def get_filter_list(result, category_list=[], is_display_provider=True, is_display_category=True, is_display_popular=True):
    from provider.cached import cached_provider_list
    result['filter'] = []

    # Sort
    sort = {'name': 'Sort', 'param': 'sort', 'list': []}
    sort['list'].append({'name': 'Latest', 'value': 'latest'})
    if is_display_popular:
        sort['list'].append({'name': 'Popular', 'value': 'popular'})

    sort['list'].append({'name': 'A - Z', 'value': 'atoz'})
    sort['list'].append({'name': 'Z - A', 'value': 'ztoa'})
    result['filter'].append(sort)

    if len(category_list) > 0 and is_display_category:
        _category = {'name': 'By Categories', 'param': 'category', 'list': []}
        _category['list'].append({
            'name': 'All Categories',
            'value': 'all'
        })
        for category in category_list:
            _category['list'].append({
                'name': category.name,
                'value': category.id
            })
        result['filter'].append(_category)

    # Provider
    if is_display_provider:
        _provider = {'name': 'By Learning Center', 'param': 'provider', 'list': []}
        _provider['list'].append({
            'name': 'All Learning Center',
            'value': 'all'
        })
        for provider in cached_provider_list():
            _provider['list'].append({
                'name': provider.name,
                'value': provider.id
            })
        result['filter'].append(_provider)
