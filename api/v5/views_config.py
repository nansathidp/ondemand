from django.conf import settings

from api.views_api import json_render
from featured.models import Highlight
from .views import json_render, check_require


def config_view(request):
    result = {
        'is_force_update': False,
        'is_forgot_password': False
    }

    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render(result, code)

    # Check Version
    version = float(request.GET.get('version', 0))
    store_version = request.APP.app_version
    result.update({
        'is_force_update': store_version > version,
        'is_forgot_password': settings.IS_FORGOT_PASSWORD
    })

    highlight = Highlight.pull_list('announcement').first()
    if highlight is not None:
        result.update({
            'highlight': highlight.api_display()
        })
    return json_render(result, 200)
