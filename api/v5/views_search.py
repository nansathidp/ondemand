from .views import json_render, check_key
from course.models import Course


def search_view(request):
    result = {}
    code = 200
    result['course_list'] = []
    response = check_key(request)
    if response is not None:
        return response
    q = " ".join(request.GET.get('q', None).split())
    store = request.GET.get('store', 1)
    if q is not None and len(q) > 0:
        result['_keyword'] = q
        for course in Course.objects.select_related('category').filter(name__icontains=q)[:20]:
            result['course_list'].append(course.api_display(store=store))
    else:
        code = 205
    return json_render(result, code)
