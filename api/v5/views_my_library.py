from django.conf import settings

from account.models import Account
# Old query use Model
from course.cached import cached_course_app_id_list
from order.models import Item
from .views import json_render, check_key, check_require

RIBBON_STATUS_MAP = {
    '0': 'Normal',
    '1': 'New Added',
    '2': 'New Assigned',
    '3': 'Expired'
}


def _ribbon_status(status):
    return {'status_ribbon': status,
            'status_ribbon_display': RIBBON_STATUS_MAP[str(status)]}


def library_view(request):
    result = {'item_list': [],
              'item_question_list': [],
              'item_program_list': [],
              'item_live_list': []}

    item_check = {}

    response = check_key(request)
    if response is not None:
        return response

    code, token, uuid, store, version = check_require(request)
    if code != 200:
        return json_render(result, code)

    account = Account.api_auth(token, uuid)
    if account is None:
        return json_render({}, 400)

    # Get content_type
    content_type_course = settings.CONTENT_TYPE('course', 'course')
    content_type_question = settings.CONTENT_TYPE('question', 'activity')
    content_type_program = settings.CONTENT_TYPE('program', 'program')
    content_type_live = settings.CONTENT_TYPE('live', 'live')

    # Get Purchase Item
    content_id_list = cached_course_app_id_list(request.APP)
    item_list = Item.objects.filter(
        order__account=account,
        order__status=3,
        status=2,
        is_display=True
    ).order_by('-timestamp')

    for item in item_list:
        expire_status, expired_display = item.api_expired_day()
        expire_item = {'expired_display': expired_display,
                       'expired': expire_status}
        if item.content_type_id == content_type_course.id and item.content not in item_check:
            if item.content in content_id_list:
                course = item.get_content()
                if course is not None:
                    _result = item.api_display()
                    _result.update(expire_item)
                    _result.update(_ribbon_status(1))
                    _result['content'] = course.api_display_title(store)
                    result['item_list'].append(_result)
                    item_check[item.content] = True
        elif item.content_type_id == content_type_program.id:
            program = item.get_content()
            if program is not None and program.type == 1:
                _result = item.api_display()
                _result.update(expire_item)
                _result.update({'content': program.api_display_title(store)})
                result['item_program_list'].append(_result)
        elif item.content_type_id == content_type_question.id:
            question = item.get_content()
            if question is not None:
                _result = item.api_display()
                _result.update({'content': question.api_display_title(store)})
                _result.update(expire_item)
                result['item_question_list'].append(_result)
        elif item.content_type_id == content_type_live.id:
            live = item.get_content()
            if live is not None:
                _result = item.api_display()
                _result.update({'content': live.api_display_title(store)})
                _result.update(expire_item)
                result['item_live_list'].append(_result)
    return json_render(result, code)
