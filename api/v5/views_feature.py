from django.conf import settings

from course.models import Course
from featured.models import Highlight
from rating.models import Rating

from api.views_api import init_section
from course.cached import cached_api_course_feature
from program.cached import cached_api_program_list
from category.models import Category
from tutor.models import Tutor
from provider.models import Provider
from .views import json_render, check_key


def feature_view(request):
    result = {}
    code = 200
    response = check_key(request)
    if response is not None:
        return response
    try:
        store = int(request.GET.get('store', None))
    except:
        return json_render({}, 209)

    version = request.GET.get('version', 0.0)
    app_version = request.APP.app_version

    result['highlight_list'] = [highlight.api_display() for highlight in Highlight.pull_list('banner')]
    result['section_list'] = []

    d = init_section('Latest Courses', 4, 4, -1, True)
    d['course_list'] = [course.api_display_title(store, version, app_version) for course in
                        Course.pull_latest_list(request.APP, 10)]
    if len(d['course_list']) > 0:
        result['section_list'].append(d)

    d = init_section('Popular Courses', 4, 5, -1, True)
    d['course_list'] = []
    for _ in Rating.popular_list(settings.CONTENT_TYPE('course', 'course'), 8):
        d['course_list'].append(_.api_display_title(store, version, app_version))
    if len(d['course_list']) > 0:
        result['section_list'].append(d)

    # d = init_section('Recommend Courses', 4, -1, -1, False)
    # d['course_list'] = cached_api_course_feature(request.APP, store, version, app_version)
    # if len(d['course_list']) > 0:
    #     result['section_list'].append(d)

    if settings.PROJECT in ['ais']:
        d = init_section('By Categories', 1, -1, -1, False)
        d['category_list'] = [category.api_display() for category in Category.featured_list()]
        if len(d['category_list']) > 2:
            result['section_list'].append(d)

    d = init_section('Instructors', 3, -1, -1, False)
    d['tutor_list'] = []
    for tutor in Tutor.pull_featured_list():
        if tutor.image_featured:
            d['tutor_list'].append(tutor.api_display_title())
    if len(d['tutor_list']) > 0:
        result['section_list'].append(d)

    if settings.PROJECT == 'unilever':
        d = init_section('Program', 8, 3, -1, True)
        d['program_list'] = cached_api_program_list(store=store, is_force=True)
        result['section_list'].append(d)

    if settings.PROJECT in ['ais']:
        d = init_section('By Learning Center', 9, 3, -1, False)
        d['provider_list'] = [provider.api_display_title() for provider in Provider.featured_list()]
        result['section_list'].append(d)

        d = init_section('Latest Program', 8, 3, -1, True)
        d['program_list'] = cached_api_program_list(store=store, is_force=True)
        result['section_list'].append(d)

    return json_render(result, code)
