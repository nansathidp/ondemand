from django.views.decorators.csrf import csrf_exempt

from account.models import Account

from .views_api import json_render
import json


@csrf_exempt
def request_view(request):
    result = {}
    code = 200
    if request.method == 'POST':
        token = request.POST.get('token', None)
        tel = request.POST.get('tel', None)
    else:
        token = request.GET.get('token', None)
        tel = request.GET.get('tel', None)

    if token is None:
        return json_render({}, 204)

    account = Account.api_auth(token, None)
    if account is None:
        return json_render({}, 400)
    if tel is None:
        return json_render({}, 660)
    else:
        tel = tel.replace('', '')
        if int(tel[0]) == 0:
            tel = '66%s' % tel[1:]
        if len(tel) != 11:
            return json_render({
            }, 660)

    # if tel is not None and Account.objects.filter(tel=tel, status__in=[1, 2]).exclude(id=account.id).count() > 0:
    #     return json_render({}, 664)

    result = LogRequestCustomerProfile.send(account, tel)
    result['tel'] = tel
    return json_render(result, code)


def otp_view(request):
    result = {}
    code = 200
    if request.method == 'POST':
        token = request.POST.get('token', None)
        otp_id = request.POST.get('otp_id', None)
        otp = request.POST.get('otp', None)
    else:
        token = request.GET.get('token', None)
        otp_id = request.GET.get('otp_id', None)
        otp = request.GET.get('otp', None)

    if token is None:
        return json_render({}, 204)

    if otp_id is None or otp is None:
        return json_render({}, 661)
    
    account = Account.api_auth(token, None)
    if account is None:
        return json_render({}, 400)
        
    log_get_otp_request = LogGetOtpRequest.api_pull(account, otp_id)
    if log_get_otp_request is None:
        return json_render({}, 662)
    else:
        result = LogLoginByOtp.send(log_get_otp_request, otp)
    return json_render(result, code)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def check_ais_ip(ip):
    is_ais = False
    try:
        ip_list = ip.split('.')
        if int(ip_list[0]) == 110 and int(ip_list[1]) == 49 and 224 <= int(ip_list[2]) <= 255 and 0 <= int(ip_list[3]) <= 255:
            is_ais = True
        elif int(ip_list[0]) == 49 and int(ip_list[1]) == 230 and 0 <= int(ip_list[2]) <= 255 and 0 <= int(ip_list[3]) <= 255:
            is_ais = True
    except:
        pass
    return is_ais


def bypass_view(request):
    result = {}
    code = 200
    if request.method == 'POST':
        token = request.POST.get('token', None)
        tel = request.POST.get('tel', None)
    else:
        token = request.GET.get('token', None)
        tel = request.GET.get('tel', None)

    if token is None:
        return json_render({}, 204)

    account = Account.api_auth(token, None)
    if account is None:
        return json_render({}, 400)

    if tel is None:
        return json_render({}, 660)
    #else:
    #    tel = tel.replace('', '')
    #    if len(tel) == 10 and tel[0] == '0':
    #        tel = '66%s'%tel[1:]
    #    if len(tel) != 11:
    #        return json_render({}, 660)

    ip = request.META.get('HTTP_X_FORWARDED_FOR')
    is_ais = check_ais_ip(ip)
    if not is_ais:
        try:
            ip = request.META.get('HTTP_X_FORWARDED_FOR').split(',')[0]
            is_ais = check_ais_ip(ip)
        except:
            pass
    if not is_ais:
        ip = request.META.get('REMOTE_ADDR')
        is_ais = check_ais_ip(ip)
    if not is_ais:
        try:
            ip = request.META['HTTP_FORWARDED'].split('=')[1]
            is_ais = check_ais_ip(ip)
        except:
            pass

    try:
        meta = {}
        for key, value in request.META.items():
            meta[key] = str(value)
        meta = json.dumps(meta, indent=2)
    except:
        meta = result.META
                                      
    log_by_pass = LogByPass(account=account,
                            tel='',
                            ip=ip,
                            is_ais=is_ais,
                            meta=meta)
    log_by_pass.save()
    if not is_ais:
        return json_render({'ip': ip}, 665)
    
    #try:
    #    tel = request.META["HTTP_X_MSISDN"]
    #    log_by_pass.tel = tel
    #    log_by_pass.save(update_fields=['tel'])
    #except:
    #    return json_render({}, 666)

    log_by_pass.tel = tel
    log_by_pass.save(update_fields=['tel'])
    
    if account.status == 0:
        account.tel = tel
        account.status = 2
        account.save()
    else:
        return json_render({}, 663)
    return json_render(result, code)

