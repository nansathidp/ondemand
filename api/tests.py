"""
from django.test import TestCase, Client, SimpleTestCase
import json
import datetime

class ApiRegisterTestCase ( TestCase ) :
    def test_request_get_to_register( self ):
        response = self.client.get('/api/regis/')
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals( response_json['status'], 201 )

    def translate_datetime_to_text( self, datetime ) :
        self.year = datetime.strftime('%Y')
        self.month = datetime.strftime('%B')
        self.day = datetime.strftime('%d')
        if ( self.day.startswith("0") ) :
            self.day = self.day[1:]

        return " " + self.day + " " + self.month + " " + self.year

    def test_api_register_success( self ):
        email = "kittitochp@gmail.com"
        first_name = "Kittitoch"
        last_name = "Phuekphiphatmat"
        password = "123456"
        birthday = "21/02/1992"
        gender = 1
        school_name = "saraburiwitthayakhon"
        school_id = "99"
        tel = "0869776857"
        response = self.client.post('/api/regis/', {
            'uuid' : "1111111111111",
            'email' : email,
            'first_name' : first_name,
            'last_name' : last_name,
            'password' : password,
            'birthday' : birthday,
            'gender' : gender,
            'school_name' : school_name,
            'school_id' : school_id,
            'tel' : tel
        })
        response_json = json.loads(response.content.decode('utf-8'))
        now = datetime.datetime.now()
        now_text = self.translate_datetime_to_text( now )
        self.assertEquals( response_json['status'] == 200, True, "Can't Regist" )
        self.assertEquals( response_json['status_msg'] == 'Success.', True, "Can't Regist" )
        self.assertEquals( response_json['result']['profile']['first_name'], first_name, "Wrong FirstName" )
        self.assertEquals( response_json['result']['profile']['last_name'], last_name, "Wrong LastName" )
        self.assertEquals( response_json['result']['profile']['gender'], gender, "Wrong Gender" )
        self.assertEquals( response_json['result']['profile']['is_login_facebook'], False, "is_facebook" )
        self.assertEquals( response_json['result']['profile']['facebook_id'], None, "is_facebook" )
        self.assertEquals( response_json['result']['profile']['email'], email, "Wrong Email" )
        self.assertEquals( response_json['result']['profile']['date_joined'], now_text, "Wrong date joined" )
        self.assertEquals( response_json['result']['profile'].has_key("id"), True, "Don't have ID" )
        self.assertEquals( response_json['result']['profile'].has_key("image"), True, "Don't have Image" )
        self.assertEquals( response_json['result'].has_key("token"), True, "Don't have Token" )

    def test_api_register_short_password(self):
        email = "kittitochp@gmail.com"
        first_name = "Kittitoch"
        last_name = "Phuekphiphatmat"
        password = "12345"
        birthday = "21/02/1992"
        gender = 1
        school_name = "saraburiwitthayakhon"
        school_id = "99"
        tel = "0869776857"
        response = self.client.post('/api/regis/', {
            'uuid' : "1111111111111",
            'email' : email,
            'first_name' : first_name,
            'last_name' : last_name,
            'password' : password,
            'birthday' : birthday,
            'gender' : gender,
            'school_name' : school_name,
            'school_id' : school_id,
            'tel' : tel
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals(response_json['status'] == 601, True, "Wrong status code" )

    def test_api_register_short_password_2(self):
        email = "kittitochp@gmail.com"
        first_name = "Kittitoch"
        last_name = "Phuekphiphatmat"
        password = "1234"
        birthday = "21/02/1992"
        gender = 1
        school_name = "saraburiwitthayakhon"
        school_id = "99"
        tel = "0869776857"
        response = self.client.post('/api/regis/', {
            'uuid' : "1111111111111",
            'email' : email,
            'first_name' : first_name,
            'last_name' : last_name,
            'password' : password,
            'birthday' : birthday,
            'gender' : gender,
            'school_name' : school_name,
            'school_id' : school_id,
            'tel' : tel
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals(response_json['status'] == 601, True, "Wrong status code" )

    def test_api_register_with_same_email(self):
        email = "kittitochp@gmail.com"
        first_name = "Kittitoch"
        last_name = "Phuekphiphatmat"
        password = "123456"
        birthday = "21/02/1992"
        gender = 1
        school_name = "saraburiwitthayakhon"
        school_id = "99"
        tel = "0869776857"
        first_response = self.client.post('/api/regis/', {
            'uuid' : "1111111111111",
            'email' : email,
            'first_name' : first_name,
            'last_name' : last_name,
            'password' : password,
            'birthday' : birthday,
            'gender' : gender,
            'school_name' : school_name,
            'school_id' : school_id,
            'tel' : tel
        })
        first_response_json = json.loads(first_response.content.decode('utf-8'))
        self.assertEquals( first_response_json['status'] == 200, True, "Can't Regist" )
        second_response = self.client.post('/api/regis/', {
            'uuid' : "1111111111111",
            'email' : email,
            'first_name' : first_name,
            'last_name' : last_name,
            'password' : password,
            'birthday' : birthday,
            'gender' : gender,
            'school_name' : school_name,
            'school_id' : school_id,
            'tel' : tel
        })
        second_response_json = json.loads(second_response.content.decode('utf-8'))
        self.assertEquals( second_response_json['status'] == 602, True, "Wrogn status code" )

    def test_api_register_wrong_email( self ):
        email = "kittitochpgmail.com"
        first_name = "Kittitoch"
        last_name = "Phuekphiphatmat"
        password = "123"
        birthday = "21/02/1992"
        gender = 1
        school_name = "saraburiwitthayakhon"
        school_id = "99"
        tel = "0869776857"
        response = self.client.post('/api/regis/', {
            'uuid' : "1111111111111",
            'email' : email,
            'first_name' : first_name,
            'last_name' : last_name,
            'password' : password,
            'birthday' : birthday,
            'gender' : gender,
            'school_name' : school_name,
            'school_id' : school_id,
            'tel' : tel
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals( response_json['status'] == 600, True, "Wrong status code" )

    def test_api_register_without_email( self ):
        first_name = "Kittitoch"
        last_name = "Phuekphiphatmat"
        password = "123"
        birthday = "21/02/1992"
        gender = 1
        school_name = "saraburiwitthayakhon"
        school_id = "99"
        tel = "0869776857"
        response = self.client.post('/api/regis/', {
            'uuid' : "1111111111111",
            'first_name' : first_name,
            'last_name' : last_name,
            'password' : password,
            'birthday' : birthday,
            'gender' : gender,
            'school_name' : school_name,
            'school_id' : school_id,
            'tel' : tel
        })
        response_json = json.loads( response.content )
        self.assertEquals( response_json['status'] == 600, True, "Wrong status code" )

    def test_api_register_with_out_uuid( self ):
        email = "kittitochp@gmail.com"
        first_name = "Kittitoch"
        last_name = "Phuekphiphatmat"
        password = "123"
        birthday = "21/02/1992"
        gender = 1
        school_name = "saraburiwitthayakhon"
        school_id = "99"
        tel = "0869776857"
        response = self.client.post('/api/regis/', {
            'email' : email,
            'first_name' : first_name,
            'last_name' : last_name,
            'password' : password,
            'birthday' : birthday,
            'gender' : gender,
            'school_name' : school_name,
            'school_id' : school_id,
            'tel' : tel
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals( response_json['status'] == 202, True, "Wrong status code" )

class ApiLoginTestCase ( TestCase ) :

    def setUp(self):
        self.email = "kittitochp@gmail.com"
        self.first_name = "Kittitoch"
        self.last_name = "Phuekphiphatmat"
        self.password = "123456"
        self.birthday = "21/02/1992"
        self.gender = 1
        self.school_name = "saraburiwitthayakhon"
        self.school_id = "99"
        self.tel = "0869776857"
        self.response = self.client.post('/api/regis/', {
            'uuid' : "1111111111111",
            'email' : self.email,
            'first_name' : self.first_name,
            'last_name' : self.last_name,
            'password' : self.password,
            'birthday' : self.birthday,
            'gender' : self.gender,
            'school_name' : self.school_name,
            'school_id' : self.school_id,
            'tel' : self.tel
        })
        self.response_json = json.loads(self.response.content.decode('utf-8'))
        self.assertEquals( self.response_json['status'] == 200, True, "Can't Regist" )
        self.assertEquals( self.response_json['status_msg'] == 'Success.', True, "Can't Regist" )

    def test_login_success( self ):
        email = "kittitochp@gmail.com"
        password = "123456"
        response = self.client.get('/api/login/', {
            'uuid' : 111111111,
            'email' : email,
            'password' : password,
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals( response_json['status'] == 200, True, "Wrong status code" )

    def test_login_send_by_post( self ):
        email = "kittitochp@gmail.com"
        password = "123456"
        response = self.client.post('/api/login/', {
            'uuid' : 111111111,
            'email' : email,
            'password' : password,
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals( response_json['status'] == 300, True, "Wrong status code" )

    def test_login_without_email( self ):
        email = None
        password = "123456"
        response = self.client.get('/api/login/', {
            'uuid' : 111111111,
            'email' : email,
            'password' : password
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals( response_json['status'] == 300, True, "Wrong Status code" )

    def test_login_with_short_email( self ):
        email = "a@a.c"
        password = "123456"
        response = self.client.get('/api/login/', {
            'uuid' : 11111111,
            'email' : email,
            'password' : password
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals( response_json['status'] == 300, True, "Wrong Status code" )

    def test_login_with_wrong_email_form( self ):
        email = "kittitochgmail.com"
        password = "123456"
        response = self.client.get('/api/login/', {
            'uuid' : 1111111,
            'email' : email,
            'password' : password
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals( response_json['status'] == 300, True, "Wrong Status code" )

    def test_login_with_not_available_email( self ):
        email = "carriline@gmail.com"
        password = "123456"
        response = self.client.get('/api/login/', {
            'uuid' : 111111111,
            'email' : email,
            'password' : password
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals( response_json['status'] == 300, True, "Wrong Status code" )

    def test_login_password_is_none( self ):
        email = "kittitochp@gmail.com"
        password = None
        response = self.client.get('/api/login/', {
            'uuid' : 111111111,
            'email' : email,
            'password' : password
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals( response_json['status'] == 300, True, "Wrong Status code" )

    def test_login_with_wrong_password( self ):
        email = "kittitochp@gmail.com"
        password = "12345"
        response = self.client.get('/api/login/', {
            'uuid' : 111111111,
            'email' : email,
            'password' : password
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals( response_json['status'] == 300, True, "Wrong Status code" )

    def test_login_with_out_uuid( self ):
        email = "kittitochp@gmail.com"
        password = "123456"
        response = self.client.get('/api/login/', {
            'email' : email,
            'password' : password
        })
        response_json = json.loads(response.content.decode('utf-8'))
        self.assertEquals( response_json['status'] == 300, True, "Wrong Status Code" )
"""
