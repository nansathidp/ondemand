from django.conf import settings

from .views import json_render, check_key

from course.models import Course
from order.models import Order
from content.models import Location as ContentLocation

from account.cached import cached_api_account_auth


def buy_view(request, course_id):
    result = {}
    code = 200
    response = check_key(request)
    if response is not None:
        return response

    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)
    if uuid is None:
        return json_render({}, 202)
    if token is None:
        return json_render({}, 204)
    try:
        store = int(request.GET.get('store', None))
        if store not in [4]:
            return json_render({}, 703)
    except:
        return json_render({}, 209)

    account = cached_api_account_auth(token, uuid)
    if account is None:
        return json_render({}, 400)
    try:
        course = Course.objects.get(id=course_id)
    except:
        return json_render({}, 700)
    
    content_location = ContentLocation.pull_first(settings.CONTENT_TYPE('course', 'course'),
                                                  course.id)
    order_item = Order.buy(content_location,
                           request.APP,
                           store,
                           settings.CONTENT_TYPE('course', 'course'),
                           course,
                           account,
                           is_checkout=True)

    if order_item is None:
        return json_render({}, 724)
    result['order_id'] = order_item.order_id        
    return json_render(result, code)
