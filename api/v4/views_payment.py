from .views import json_render, check_key
from django.conf import settings

from order.models import Order
from aismpay.models import Level


def list_view(request):
    result = {}
    code = 200
    response = check_key(request)
    if response is not None:
        return response

    device = request.GET.get('device', None)
    display_mpay = request.GET.get('display_mpay', None)
    result['payment_list'] = list()

    # AIS Mpay
    if display_mpay is not None and display_mpay.lower() == 't':
        type = request.GET.get('type', None)
        if type is None:
            return json_render({}, 210)
        else:
            if type == 'order':
                content_type = settings.CONTENT_TYPE('order', 'order')
            else:
                return json_render({}, 211)
        content = request.GET.get('content', None)
        if content is None:
            return json_render({}, 213)
        if type == 'order':
            try:
                order = Order.objects.get(id=int(content))
            except:
                return json_render({}, 700)
        level = Level.pull(content_type, order)
        if level is not None:
            result['payment_list'].append({'id': 1,
                                           'code': Order.PAYMENT_METHOD_CHOICES[6][1],
                                           'image': '%s/static/images/api/payment/payment_method04.jpg'%settings.BASE_URL(request),
                                           'price': '%.2f' % (level.volume/100.0)})

    # Online Payent
    result['payment_list'].append({'id': 5,
                                   'code': Order.PAYMENT_METHOD_CHOICES[5][1],
                                   'image': '%s/static/images/api/payment/payment_method03.png' % settings.BASE_URL(request),})
    # Counter Service
    result['payment_list'].append({'id': 6,
                                   'code': Order.PAYMENT_METHOD_CHOICES[3][1],
                                   'image': '%s/static/images/api/payment/payment_method02.png' % settings.BASE_URL(request),})

    # Credit card
    result['payment_list'].append({'id': 2,
                                   'code': Order.PAYMENT_METHOD_CHOICES[2][1],
                                   'image': '%s/static/images/api/payment/payment_method01.png' % settings.BASE_URL(request),})
    return json_render(result, code)
