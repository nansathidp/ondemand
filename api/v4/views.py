import json
from django.http import HttpResponse

from api.views_api import STATUS_MSG
from app.cached import cached_api_app
from key.cached import cached_api_key


def json_render(result, code=None, status=200, indent=2, content_type='application/json', location=None):
    if code is None:
        data = result
    else:
        data = {'status': code,
                'status_msg': STATUS_MSG[code],
                'result': result}
    response = HttpResponse(json.dumps(data, indent=indent),
                            status=status,
                            content_type=content_type)
    if location is not None:
        response['Location'] = location
    return response


def check_key(request):
    code = request.GET.get('code', None)
    if code is None:
        return json_render({}, status=401)
    else:
        app = cached_api_app(code)
        if app is None:
            return json_render({}, status=401)
        else:
            request.APP = app

    key = request.GET.get('key', None)
    if key is None:
        return json_render({}, status=403)
    else:
        key = cached_api_key(app, key)
        if not key:
            return json_render({}, status=403)

    request.code = code
    request.key = key
    return None


def check_require(request):
    token = request.GET.get('token', None)
    uuid = request.GET.get('uuid', None)
    if uuid is None:
        return 202, token, uuid, None
    try:
        store = int(request.GET.get('store', None))
        if store not in [3, 4]:
            return 703, token, uuid, store
    except:
        return 209, token, uuid, store
    return 200, token, uuid, store
