from .views_api import json_render


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def ip_view(request):
    result = {}
    code = 200
    result['ip'] = get_client_ip(request)
    result['url'] = request.build_absolute_uri()
    return json_render(result, code)
