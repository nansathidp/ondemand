from account.cached import cached_api_account_auth


def check_auth(request, is_post=False, is_allow_anonymous=False):
    code = 200
    account = None
    if is_post:
        token = request.POST.get('token', None)
    else:
        token = request.GET.get('token', None)

    if token is None and is_allow_anonymous is False:
        code = 204
    if is_post:
        uuid = request.POST.get('uuid', None)
    else:
        uuid = request.GET.get('uuid', None)
    if uuid is None:
        code = 202

    if code == 200:
        account = cached_api_account_auth(token, uuid)
        if account is None and is_allow_anonymous is False:
            code = 400
    return account, code
