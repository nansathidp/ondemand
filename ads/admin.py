from django.contrib import admin
from .models import Ads


@admin.register(Ads)
class LevelAdmin(admin.ModelAdmin):
    list_display = ('name', 'link', 'is_display', 'timestamp')
