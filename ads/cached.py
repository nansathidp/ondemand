from django.conf import settings
from django.core.cache import cache
from .models import Ads

from utils.cached.time_out import get_time_out_week


def cached_ads(ads_id, is_force=False):
    key = '%s_ads_%s' % (settings.CACHED_PREFIX, ads_id)
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Ads.objects.get(id=ads_id)
        except Ads.DoesNotExist:
            result = -1
        cache.set(key, result, get_time_out_week())
    return None if result == -1 else result


def cached_ads_list(is_force=False):
    key = '%s_ads' % settings.CACHED_PREFIX
    result = None if is_force else cache.get(key)
    if result is None:
        try:
            result = Ads.objects.filter(is_display=True).order_by('sort')
        except Ads.DoesNotExist:
            result = -1
        cache.set(key, result, get_time_out_week())
    return None if result == -1 else result


def cached_ads_list_delete():
    key = '%s_ads' % settings.CACHED_PREFIX
    cache.delete(key)
