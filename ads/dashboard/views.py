from django.shortcuts import render
from django.core.exceptions import PermissionDenied

from ..models import Ads

from utils.paginator import paginator


def home_view(request):
    if not request.user.has_perm('subject.view_subject'):
        raise PermissionDenied
    
    ads_list = Ads.objects.all()
    ads_list = paginator(request, ads_list)
    
    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Ads Management'}
    ]

    return render(request,
                  'ads/dashboard/home.html',
                  {'SIDEBAR': 'ads',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'ads_list': ads_list})
