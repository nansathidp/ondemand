from django.contrib.auth.decorators import permission_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect

from .forms import AdsForm


def create_view(request):
    if request.method == 'POST':
        ads_form = AdsForm(request.POST, request.FILES)
        if ads_form.is_valid():
            ads = ads_form.save()
            return redirect('dashboard:ads-dashboard:home')
    else:
        ads_form = AdsForm()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Ads Management',
         'url': reverse('dashboard:ads-dashboard:home')},
        {'is_active': True,
         'title': 'Create'},
    ]
    return render(request,
                  'ads/dashboard/create.html',
                  {'SIDEBAR': 'ads',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'ads_form': ads_form})
