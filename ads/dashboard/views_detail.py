from django.contrib.auth.decorators import permission_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404
from .forms import AdsForm
from ..models import Ads


@permission_required('subject.change_subject', raise_exception=True)
def information_view(request, ads_id):
    ads = get_object_or_404(Ads, id=ads_id)

    if request.method == 'POST':
        ads_form = AdsForm(request.POST, request.FILES, instance=ads)
        if ads_form.is_valid():
            ads = ads_form.save()
            ads_form = AdsForm(instance=ads)
    else:
        ads_form = AdsForm(instance=ads)

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Ads Management',
         'url': reverse('dashboard:ads-dashboard:home')},
        {'is_active': True,
         'title': ads.name},
    ]
    return render(request, 'ads/dashboard/information.html',
                  {'SIDEBAR': 'ads',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'TAB': 'information',
                   'ads_form': ads_form,
                   'ads': ads})
