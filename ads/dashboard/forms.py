# -*- coding: utf-8 -*-
from django import forms

from ..models import Ads


class AdsForm(forms.ModelForm):

    class Meta:
        model = Ads
        fields = ['name', 'image', 'link', 'is_display']

    def __init__(self, *args, **kwargs):
        super(AdsForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({'type': 'file'})