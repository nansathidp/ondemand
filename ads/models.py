from django.db import models

from django.db.models.signals import post_save
from django.dispatch import receiver


class Ads(models.Model):
    name = models.CharField(max_length=120)
    image = models.ImageField(upload_to='ads/image/')
    link = models.CharField(max_length=120)
    sort = models.IntegerField(db_index=True, default=0)
    is_display = models.BooleanField(db_index=True, default=False)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.name

    def api_display(self):
        return {
          'name': self.name,
          'link': self.link
        }


@receiver(post_save, sender=Ads)
def ads_post_save(sender, **kwargs):
    from .cached import cached_ads_list_delete
    cached_ads_list_delete()

