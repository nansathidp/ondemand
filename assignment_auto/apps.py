from django.apps import AppConfig


class AssignmentAutoConfig(AppConfig):
    name = 'assignment_auto'
