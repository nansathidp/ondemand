from django import forms

from assignment_auto.models import AssignmentAuto


class InformationForm(forms.ModelForm):
    content = forms.ChoiceField()

    class Meta:
        model = AssignmentAuto
        fields = ['content_type', 'content', 'hire_start', 'hire_end', 'condition', 'execution_date', 'is_active']
        labels = {

            'condition': 'Condition <br> เงื่อนไข',
            'hire_start': 'Hire Date Start </br> เริ่มจาก',
            'hire_end': 'Hire Date End </br> สิ้นสุด',
            'execution_date': 'Day  Assign </br> ทำงานทุกวันที่',
            'is_active': 'Active </br> ทำงาน',
        }
        help_texts = {
            'execution_date': 'Limit at 31 day. and minimum current day'

        }

    def __init__(self, *args, **kwargs):
        super(InformationForm, self).__init__(*args, **kwargs)
        from django.conf import settings
        from program.models import Program
        CHOICES = list()

        for program in Program.objects.filter(type=2):
            CHOICES.append((program.id, program.name))
        self.fields['content_type'].widget = forms.HiddenInput(attrs={'value': settings.CONTENT_TYPE('program', 'onboard').id})
        self.fields['content'].choices = CHOICES

        self.fields['hire_start'].widget.attrs.update({'max': 31, 'min': 1})
        self.fields['hire_end'].widget.attrs.update({'max': 31, 'min': 1})
        self.fields['execution_date'].widget.attrs.update({'max': 31, 'min': 1})