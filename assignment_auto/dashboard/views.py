from django.shortcuts import render, get_object_or_404, reverse, Http404
from django.core.exceptions import PermissionDenied
from assignment_auto.models import AssignmentAuto, AssignmentAutoLog
from assignment.models import Assignment
from program.models import Program
from account.models import Account


def home_view(request):
    assign_list = AssignmentAuto.objects.all()

    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Auto Assignment Management'}
    ]
    return render(request,
                  'assignment_auto/dashboard/home.html',
                  {'SIDEBAR': 'onboard',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'assign_list': assign_list,
                   })


def detail_view(request, assign_auto_id):
    assign_auto = get_object_or_404(AssignmentAuto, id=assign_auto_id)
    if assign_auto is None:
        raise Http404

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': 'Auto Assignment Management',
         'url': reverse('dashboard:assignment_auto:home')},
        {'is_active': True,
         'title': 'Auto Assignment Log'}
    ]
    return render(request,
                  'assignment_auto/dashboard/log.html',
                  {'SIDEBAR': 'assignment_auto',
                   'TAB': 'summary',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'assign_auto': assign_auto,
                   })


def tes_assign(request, assign_auto_id):
    assign_auto = AssignmentAuto.objects.filter(id=assign_auto_id).first()
    assign_auto.push()
    return detail_view(request, assign_auto.id)
