from django.conf.urls import url
from .views import home_view, detail_view, tes_assign
from .views_create import create_view
from .views_edite import edite_view

app_name = 'assignment_auto'
urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^create/$', create_view, name='create'),
    url(r'^(\d+)/edite/$', edite_view, name='edite'),
    url(r'^(\d+)/log/$', detail_view, name='log'),
    url(r'^(\d+)/test_assign', tes_assign, name='test')

]
