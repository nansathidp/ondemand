from django.shortcuts import render, reverse
from assignment_auto.dashboard.forms import InformationForm


def create_view(request):
    if request.method == 'POST':
        assign_form = InformationForm(request.POST)
        if assign_form.is_valid():
            assign_auto = assign_form.save(commit=False)
            assign_auto.save()
    assign_form = InformationForm()

    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': ' Auto Assignment Management',
         'url': reverse('dashboard:assignment_auto:home')},
        {'is_active': True,
         'title': 'Select Items to assign.'}
    ]

    return render(request, 'assignment_auto/dashboard/create.html',
                  {'TAB': 'information', 'SIDEBAR': 'assignment_auto',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'assign_form': assign_form})
