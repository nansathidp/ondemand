from django.shortcuts import render, get_object_or_404, reverse
from assignment_auto.dashboard.forms import InformationForm
from assignment_auto.models import AssignmentAuto


def edite_view(request, assign_auto_id):
    assign_auto = get_object_or_404(AssignmentAuto, id=assign_auto_id)
    if request.method == 'POST':
        assign_form = InformationForm(request.POST, instance=assign_auto)
        if assign_form.is_valid():
            assign_auto = assign_form.save(commit=False)
            assign_auto.save()
    assign_form = InformationForm(instance=assign_auto)
    BREADCRUMB_LIST = [
        {'is_active': False,
         'title': ' Auto Assignment Management',
         'url': reverse('dashboard:assignment_auto:home')},
        {'is_active': True,
         'title': 'Select Items to assign.'}
    ]
    return render(request, 'assignment_auto/dashboard/create.html',
                  {'TAB': 'information', 'SIDEBAR': 'assignment_auto',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'assign_form': assign_form})
