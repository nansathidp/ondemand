from django.test import TestCase,Client

from django.conf import  settings
from datetime import datetime
from assignment_auto.models import AssignmentAuto, AssignmentAutoLog

class TestAutoAassign(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.assign = AssignmentAuto(content_type=settings.CONTENT_TYPE('program', 'onboard'),
                                     content=3,
                                     day=2,
                                     time=datetime.now())
        
    def setUp(self):
        self.data = {
            'content_type': settings.CONTENT_TYPE('program', 'onboard'),
            'content': 3,
            'day': datetime.day,
            'day': datetime.now(),
        }
        
    # def test_post(self):
    #     self.client.post()

