from django.db import models


class AssignmentAuto(models.Model):
    CONDITION_CHOICES = (
        (1, "Hire date temp"),
        (2, "Hire data permanent"),
    )
    content_type = models.ForeignKey('contenttypes.ContentType', related_name='+')
    content = models.BigIntegerField(db_index=True)
    condition = models.IntegerField(choices=CONDITION_CHOICES)
    # Condition
    hire_start = models.PositiveIntegerField(default=1)
    hire_end = models.PositiveIntegerField(default=1)

    # TODO: Add temp/permanent condition

    execution_date = models.PositiveIntegerField(default=1)

    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.content_type.name

    @property
    def get_program(self):
        from program.models import Program
        program = Program.objects.filter(id=self.content).first()
        return program

    def get_account_list(self, now):
        from django.utils import timezone
        from django.db.models.functions import Length

        from ais.models import AccountInfo
        import calendar
        import datetime
        days = calendar.monthrange(datetime.datetime.now().year, datetime.datetime.now().month)
        max_day = days[1]
        hire_end = max_day if self.hire_end > max_day else self.hire_end
        hire_date_temp_start = timezone.datetime(year=now.year, month=now.month, day=self.hire_start)
        hire_date_temp_end = timezone.datetime(year=now.year, month=now.month, day=hire_end)
        if self.condition == 1:
            account_list = AccountInfo.objects.annotate(text_len=Length('employee')).filter(text_len=6,
                                                                                            hire_date__gte=hire_date_temp_start,
                                                                                            hire_date__lte=hire_date_temp_end)
        elif self.condition == 2:

            account_list = AccountInfo.objects.annotate(text_len=Length('employee')).filter(text_len__lt=6,
                                                                                            hire_date__gte=hire_date_temp_start,
                                                                                            hire_date__lte=hire_date_temp_end)

        return account_list

    def assing_auto_member(self, assignment, time_now):

        from assignment.models import Member
        account_list = self.get_account_list(now=time_now)
        for account_item in account_list:
            Member.objects.create(assignment=assignment, account=account_item.account)

    def assign_auto_item(self, assignment):
        from assignment.models import Item as AssignmentItem

        AssignmentItem.objects.create(assignment=assignment,
                                      content_type=self.content_type,
                                      content=self.content,
                                      sort=assignment.item_set.count() + 1).save()

    def log(self, assignment, status, next_date, amount):
        log = AssignmentAutoLog.objects.create(assignment_auto=self,
                                               assignment=assignment,
                                               next_date=next_date,
                                               status=status,
                                               amount=amount)
        log.save()

    def log_list(self):
        return AssignmentAutoLog.objects.filter(assignment_auto=self)

    def sum_amount(self):
        from django.db.models import Sum
        return AssignmentAutoLog.objects.filter(assignment_auto=self).aggregate(total_amount=Sum('amount'))

    def assign_auto_result_push(self, member, app, domain, item_list, expired, expired_date):

        from django.utils import timezone
        from django.conf import settings
        from assignment.models import Result
        from order.models import Order, Item as OrderItem
        from progress.models import Progress
        from content.models import Location as ContentLocation
        from order.cached import cached_order_item_update
        from utils.content import get_content
        from order.cached import cached_order_item_delete
        from utils.date_time import convert_from_date
        import datetime

        from progress.cached import cached_progress_delete

        def _push_content(order, item):
            status = 1
            order_item = None
            content_type = settings.CONTENT_TYPE_ID(item.content_type_id)

            content = get_content(content_type.id, item.content)

            if content is not None:
                now = timezone.now()
                content_location = ContentLocation.pull_first(content_type, content.id)
                order_item = Order.buy(content_location,
                                       app,
                                       1,
                                       content_type,
                                       content,
                                       self.account,
                                       order=order,
                                       start=now)
                if expired_date is not None:
                    order_item.expired = convert_from_date(expired_date) - now
                    order_item.save(update_fields=['expired'])
                    cached_order_item_update(order_item)
                elif expired and expired.total_seconds() > 0:
                    order_item.expired = expired
                    order_item.save(update_fields=['expired'])
                    cached_order_item_update(order_item)
            else:
                status = -1
            return status, order_item

        order = Order.pull_create(app, member.account, method=21, timecomplete=timezone.now())
        for item in item_list:
            result = Result.objects.filter(assignment_id=member.assignment_id,
                                           item=item,
                                           member=member).first()

            if result is None:
                continue
            elif result.status == 11:
                continue

            if result.status == 10:  # OK
                if OrderItem.objects.filter(account_id=member.account_id,
                                            content_type_id=item.content_type_id,
                                            content=item.content).exists():
                    result.status = 20
                    result.save(update_fields=['status'])
                else:
                    result.status, result.order_item = _push_content(order, item)
                    result.save(update_fields=['status', 'order_item'])
            elif result.status == 12:  # Duplicate (Replace)
                order_item = OrderItem.objects.filter(account_id=member.account_id,
                                                      content_type_id=item.content_type_id,
                                                      content=item.content).first()
                if order_item is None:
                    result.status = 22
                    result.save(update_fields=['status'])
                else:
                    order_item.status = -1
                    order_item.save(update_fields=['status'])
                    cached_order_item_delete(order_item)

                    result.status, result.order_item = _push_content(order, item)
                    result.save(update_fields=['status', 'order_item'])
            elif result.status == 13:  # Duplicate (Expand)
                order_item = OrderItem.objects.filter(account_id=member.account_id,
                                                      content_type_id=item.content_type_id,
                                                      content=item.content).first()
                if order_item is None:
                    result.status = 23
                    result.save(update_fields=['status'])
                else:
                    content = get_content(item.content_type_id,
                                          item.content)
                    if content is None:
                        result.status = 24
                        result.save(update_fields=['status'])
                    else:
                        result.status = 1
                        result.save(update_fields=['status'])

                        # Expired
                        now = timezone.now()
                        if order_item.start is None:
                            order_item.start = now
                        order_item.credit = content.credit

                        if expired_date is not None:
                            order_item.expired = convert_from_date(expired_date) - order_item.start
                        elif expired and expired.total_seconds() > 0:
                            order_item.expired = (now + expired) - order_item.start
                        elif expired and expired.total_seconds() == 0:
                            order_item.expired = datetime.timedelta(0)
                        else:
                            order_item.expired = (now + content.expired) - order_item.start

                        order_item.save(update_fields=['expired', 'credit', 'start'])
                        cached_order_item_delete(order_item)
                        progress_list = Progress.objects.filter(item=order_item)
                        progress_list.update(status=1)
                        for progress in progress_list:
                            cached_progress_delete(progress)

        order.buy_success(app, domain, method=21)

    @property
    def assign_auto_preview(self, member, item_list, assignment):

        from order.models import Item as OrderItem
        from assignment.models import Result
        for item in item_list:

            if not Result.objects.filter(assignment_id=assignment.id,
                                         item=item,
                                         member=member).exists():

                if OrderItem.objects.filter(account_id=member.account_id,
                                            content_type_id=item.content_type_id,
                                            content=item.content).exists():

                    status = 13
                else:
                    status = 10
                result = Result.objects.create(assignment_id=assignment.id,
                                               item=item,
                                               member=member,
                                               status=status)

                # member.push_preview(app, domain, item_list, is_auto=True)


class AssignmentAutoLog(models.Model):
    assignment_auto = models.ForeignKey(AssignmentAuto, related_name='auto_item_set')
    assignment = models.ForeignKey('assignment.Assignment', related_name='assign_item_set')
    next_date = models.DateTimeField()
    status = models.BooleanField(default=False)
    amount = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return self.assignment_auto.content_type.name
