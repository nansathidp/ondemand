from assignment_auto.models import AssignmentAuto
from django.utils import timezone
from django_rq import job


@job
def assignment_auto():
    import datetime

    from django.utils import timezone
    from django.conf import settings
    if 'ais' in settings.INSTALLED_APPS:

        from app.models import App
        from assignment.models import Assignment

        domain = None
        app = App.objects.first()
        now = timezone.now()
        next_date = timezone.datetime(year=now.year, month=now.month + 1, day=now.day)
        assign_auto_list = AssignmentAuto.objects.filter(is_active=True, execution_date=timezone.now().day)

        if assign_auto_list:
            for assign_auto in assign_auto_list:

                assignment = Assignment.objects.create(account=None, status=2)
                if assign_auto.get_account_list(now).first() is None:
                    assign_auto.log(assignment=assignment, status=False, next_date=next_date, amount=0)
                    return None

                assign_auto.assign_auto_item(assignment=assignment)

                assign_auto.assing_auto_member(assignment=assignment, time_now=now)
                item_list = list(assignment.item_set.all())
                member_list = list(assignment.member_set.filter(status=1))

                if len(item_list) < 1:
                    assign_auto.log(assignment=assignment, status=False, next_date=next_date, amount=0)
                    return None
                if len(member_list) < 1:
                    assign_auto.log(assignment=assignment, status=False, next_date=next_date, amount=0)
                    return None

                for member in member_list:
                    _assign_auto_preview(assignment, member, item_list)

                assignment.status = 4
                assignment.save(update_fields=['status'])
                assignment.expired = datetime.timedelta(0)

                assignment.expired_date = None

                for member in member_list:
                    _assign_auto_result_push(member, app, domain, item_list, assignment.expired,
                                             assignment.expired_date)
                assignment.status = 3
                assignment.save(update_fields=['status'])

                for member in member_list:
                    from account.cached import cached_onboard_account_delete
                    cached_onboard_account_delete(member.account.id)

                assignment.preview_submit_notification()
                assign_auto.log(assignment=assignment, status=True, next_date=next_date,
                                amount=assign_auto.get_account_list(now).count())


def _assign_auto_preview(assignment, member, item_list):
    from order.models import Item as OrderItem
    from assignment.models import Result
    for item in item_list:

        if not Result.objects.filter(assignment_id=assignment.id,
                                     item=item,
                                     member=member).exists():

            if OrderItem.objects.filter(account_id=member.account_id,
                                        content_type_id=item.content_type_id,
                                        content=item.content).exists():

                status = 13
            else:
                status = 10
            result = Result.objects.create(assignment_id=assignment.id,
                                           item=item,
                                           member=member,
                                           status=status)


def _assign_auto_result_push(member, app, domain, item_list, expired, expired_date):
    from django.utils import timezone

    from assignment.models import Result
    from order.models import Order, Item as OrderItem
    from progress.models import Progress

    from utils.content import get_content
    from order.cached import cached_order_item_delete
    from utils.date_time import convert_from_date
    import datetime

    from progress.cached import cached_progress_delete

    order = Order.pull_create(app, member.account, method=21, timecomplete=timezone.now())
    for item in item_list:
        result = Result.objects.filter(assignment_id=member.assignment_id,
                                       item=item,
                                       member=member).first()

        if result is None:
            continue
        elif result.status == 11:
            continue

        if result.status == 10:  # OK
            if OrderItem.objects.filter(account_id=member.account_id,
                                        content_type_id=item.content_type_id,
                                        content=item.content).exists():
                result.status = 20
                result.save(update_fields=['status'])
            else:
                result.status, result.order_item = _push_content(order, item, member, app, expired, expired_date)
                result.save(update_fields=['status', 'order_item'])
        elif result.status == 12:  # Duplicate (Replace)
            order_item = OrderItem.objects.filter(account_id=member.account_id,
                                                  content_type_id=item.content_type_id,
                                                  content=item.content).first()
            if order_item is None:
                result.status = 22
                result.save(update_fields=['status'])
            else:
                order_item.status = -1
                order_item.save(update_fields=['status'])
                cached_order_item_delete(order_item)

                result.status, result.order_item = _push_content(order, item, member, app, expired, expired_date)
                result.save(update_fields=['status', 'order_item'])
        elif result.status == 13:  # Duplicate (Expand)
            order_item = OrderItem.objects.filter(account_id=member.account_id,
                                                  content_type_id=item.content_type_id,
                                                  content=item.content).first()
            if order_item is None:
                result.status = 23
                result.save(update_fields=['status'])
            else:
                content = get_content(item.content_type_id,
                                      item.content)
                if content is None:
                    result.status = 24
                    result.save(update_fields=['status'])
                else:
                    result.status = 1
                    result.save(update_fields=['status'])

                    # Expired
                    now = timezone.now()
                    if order_item.start is None:
                        order_item.start = now
                    order_item.credit = content.credit

                    if expired_date is not None:
                        order_item.expired = convert_from_date(expired_date) - order_item.start
                    elif expired and expired.total_seconds() > 0:
                        order_item.expired = (now + expired) - order_item.start
                    elif expired and expired.total_seconds() == 0:
                        order_item.expired = datetime.timedelta(0)
                    else:
                        order_item.expired = (now + content.expired) - order_item.start

                    order_item.save(update_fields=['expired', 'credit', 'start'])
                    cached_order_item_delete(order_item)
                    progress_list = Progress.objects.filter(item=order_item)
                    progress_list.update(status=1)
                    for progress in progress_list:
                        cached_progress_delete(progress)

    order.buy_success(app, domain, method=21)


def _push_content(order, item, member, app, expired, expired_date):
    from utils.content import get_content
    from utils.date_time import convert_from_date

    from django.conf import settings

    from content.models import Location as ContentLocation
    from order.cached import cached_order_item_update
    from order.models import Order

    status = 1
    order_item = None
    content_type = settings.CONTENT_TYPE_ID(item.content_type_id)

    content = get_content(content_type.id, item.content)

    if content is not None:
        now = timezone.now()
        content_location = ContentLocation.pull_first(content_type, content.id)
        order_item = Order.buy(content_location,
                               app,
                               1,
                               content_type,
                               content,
                               member.account,
                               order=order,
                               start=now)
        if expired_date is not None:
            order_item.expired = convert_from_date(expired_date) - now
            order_item.save(update_fields=['expired'])
            cached_order_item_update(order_item)
        elif expired and expired.total_seconds() > 0:
            order_item.expired = expired
            order_item.save(update_fields=['expired'])
            cached_order_item_update(order_item)
    else:
        status = -1
    return status, order_item
