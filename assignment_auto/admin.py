from django.contrib import admin

from .models import AssignmentAuto, AssignmentAutoLog

@admin.register(AssignmentAuto)
class AssignmentAutoAdmin(admin.ModelAdmin):
    list_display = ('id', 'content_type', 'content','hire_start','hire_end','condition' ,'execution_date', 'is_active')


@admin.register(AssignmentAutoLog)
class AssignmentAutoLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'assignment_auto', 'assignment', 'next_date','amount','timestamp', 'status')
