from django.conf.urls import url

from .views import home_view
from .views_account import account_view
from .views_cancel import cancel_view

app_name = 'coin-dashboard'
urlpatterns = [
    url(r'^$', home_view, name='home'), #TODO: testing
    url(r'^account/(\d+)/$', account_view, name='account'), #TODO: testing
    url(r'^cancel/(\d+)/$', cancel_view, name='cancel'), #TODO: testing
]
