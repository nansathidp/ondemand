from django.shortcuts import render, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse

from ..models import Log, Volume
from account.models import Account

from utils.paginator import paginator


def account_view(request, account_id):
    if not request.user.has_perm('program.view_program') and not request.user.has_perm('program.view_own_program'):
        raise PermissionDenied

    account = get_object_or_404(Account, id=account_id)
    log_list = Log.objects.select_related('account').filter(account=account)
    log_list = paginator(request, log_list)

    volume = Volume.pull(account)
    volume.check_volume(request.APP)

    breadcrumb_list = [
        {'is_active': False,
         'title': 'Coin Management',
         'url': reverse('dashboard:coin-dashboard:home')},
        {'is_active': True,
         'title': '%s %s'%(account.first_name, account.last_name)}
    ]
    return render(request,
                  'coin/dashboard/account.html',
                  {'SIDEBAR': 'coin',
                   'BREADCRUMB_LIST': breadcrumb_list,
                   'volume': volume,
                   'log_list': log_list})
