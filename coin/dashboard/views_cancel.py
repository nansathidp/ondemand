from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied

from ..models import Volume, Log
from order.models import Order


def cancel_view(request, log_id):
    if not request.user.has_perm('coin.view_log'):
        raise PermissionDenied

    log = get_object_or_404(Log, id=log_id)
    if log.is_cancel is False:
        return redirect('dashboard:coin-dashboard:account', log.account_id)
    
    if log.type == 1:
        type = -1
        note = 'cancel log: %s \r\n admin: %s %s (%s)' % (log.id,
                                                          request.user.first_name,
                                                          request.user.last_name,
                                                          request.user.email)
    elif log.type == 2:
        type = -2
        try:
            order_id = int(log.note.split('Buy Order:')[1].strip())
        except:
            return redirect('dashboard:coin-dashboard:account', log.account_id)
        try:
            order = Order.objects.get(id=order_id)
        except:
            return redirect('dashboard:coin-dashboard:account', log.account_id)
        order.status = -1
        order.save(update_fields=['status'])
        order.item_set.update(status=-1)
        
        note= """cancel log: %s \r\n
        order id: %s \r\n 
        admin: %s %s (%s)"""%(log.id,
                              order_id,
                              request.user.first_name,
                              request.user.last_name,
                              request.user.email)
    else:
        return redirect('dashboard:coin-dashboard:account', log.account_id)
    
    volume = Volume.pull(log.account)
    Log.objects.create(account=log.account,
                       type=type,
                       volume=log.volume,
                       note=note,
                       is_cancel=False)
    log.is_cancel = False
    log.save(update_fields=['is_cancel'])
    volume.check_volume(request.APP)
    return redirect('dashboard:coin-dashboard:account', log.account_id)
