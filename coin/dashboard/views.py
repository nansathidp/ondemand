from django.shortcuts import render
from django.core.exceptions import PermissionDenied

from ..models import Log

from utils.paginator import paginator

def home_view(request):
    if not request.user.has_perm('program.view_program', group=request.DASHBOARD_GROUP) and \
       not request.user.has_perm('program.view_own_program', group=request.DASHBOARD_GROUP):
        raise PermissionDenied

    if request.method == 'POST':
        log_list = Log.objects.select_related('account').filter(account__email__icontains=request.POST.get('email', ''))
    else:
        log_list = Log.objects.select_related('account').all()
    log_list = paginator(request, log_list)
    
    BREADCRUMB_LIST = [
        {'is_active': True,
         'title': 'Coin Management'}
    ]
    return render(request,
                  'coin/dashboard/home.html',
                  {'SIDEBAR': 'coin',
                   'BREADCRUMB_LIST': BREADCRUMB_LIST,
                   'log_list': log_list})
