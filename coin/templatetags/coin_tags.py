from django import template

register = template.Library()

@register.filter(name='coin_volume')
def coin_volumn(value):
    return value.volume - value.price

@register.filter(name='click_point')
def click_point(price):
    if price > 0:
        return price / 10
    else:
        return 0

@register.inclusion_tag('coin/volume.html')
def coin_volume_tag(coin, leverage):
    value = (coin.volume - coin.volume_free) * leverage.value
    return {'value': value}

@register.inclusion_tag('coin/volume.html')
def coin_volume_free_tag(volume, leverage):
    value = volume * leverage.value
    return {'value': value}

@register.inclusion_tag('coin/volume.html')
def coin_volume_net_tag(volume, leverage):
    value = volume * leverage.value
    return {'value': value}

