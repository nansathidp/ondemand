from django.conf.urls import url

#TODO: migrate url format to from .views_xxx import xxx_view :)
from . import views, views_buy

app_name = 'coin'
urlpatterns = [
    #url(r'^$', views.home_view, name='home'), #TODO: testing
    url(r'^list/$', views.list_view, name='list'), #TODO: testing
    url(r'^(\d+)/checkout/$', views_buy.checkout_view, name='checkout'), #TODO: testing
]
