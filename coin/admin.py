from django.contrib import admin
from coin.models import Leverage, Coin, Volume, Log

@admin.register(Leverage)
class LeverageAdmin(admin.ModelAdmin):
    list_display = ('app', 'value')
    
@admin.register(Coin)
class CoinAdmin(admin.ModelAdmin):
    list_display = ('name', 'volume', 'price', 'discount', 'start', 'end', 'is_allow_promote', 'is_use', 'sort', 'store')

@admin.register(Volume)
class VolumeAdmin(admin.ModelAdmin):
    list_display = ('account', 'volume')
    readonly_fields = ('account', )
    
@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    list_display = ('account', 'type', 'volume', 'note', 'timestamp')

    search_fields = ['account__email']
