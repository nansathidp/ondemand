from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from coin.models import Coin
from order.models import Order

@login_required
def checkout_view(request, coin_id):
    coin = get_object_or_404(Coin, id=coin_id)
    order = Order.pull_first(request.user, 1, request.APP)
    order.buy_coin(coin, request.user, 1)
    return redirect('order:home')
    # return redirect('order:checkout', order.id)
