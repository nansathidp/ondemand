from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone

from coin.models import Coin
from order.models import Order

from .cached import cached_leverage
from highlight.cached import cached_web_banner


def list_view(request):
    now = timezone.now()
    type = request.GET.get('type', 0)
    if int(type) == 1:
      coin_list = Coin.objects.filter(is_use=True, start__lte=now, end__gte=now).order_by('sort')
    else:
      coin_list = Coin.objects.filter(is_use=True, start__lte=now, end__gte=now, type=0).order_by('sort')
    banner = cached_web_banner(request.APP, 2)
    leverage = cached_leverage(request.APP)
    if request.method == 'POST':
        coin = get_object_or_404(Coin, id=request.POST.get('coin'))
        try:
            num = int(request.POST.get('num'))
        except:
            num = None

        if num is not None:
            order = Order.pull_create(request.APP, request.user, 1)
            order.buy_coin(coin, request.user, 1, {'price': float(num)/leverage.value,
                                                   'volume': num})
            return redirect('order:checkout', order.id)

    return render(request,
                  'coin/list.html',
                  {'coin_list': coin_list,
                   'leverage': leverage,
                   'type': type,
                   'banner': banner})
