from django.conf import settings
from django.core.cache import cache

from coin.models import Leverage, Volume, Log

from utils.cached.time_out import get_time_out_day


def cached_leverage(app, is_force=False):
    key = '%s_coin_leverage_%s' % (settings.CACHED_PREFIX, app.id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Leverage.objects.filter(app=app).first()
        if result is None:
            result = Leverage.objects.create(app=app,
                                             value=1.0)
        cache.set(key, result, get_time_out_day())
    return result


def cached_coin_volume(account, is_force=False):
    key = '%s_coin_volume_%s' % (settings.CACHED_PREFIX, account.id)
    result = None if is_force else cache.get(key)
    if result is None:
        result = Volume.objects.filter(account=account).first()
        if result is None:
            result = Volume.objects.create(account=account,
                                           volume=0.0)
            Log.push(account, 0, 0.0, 'Init: Cached')
        cache.set(key, result, get_time_out_day())
    return result


def cached_coin_volume_update(account, volume):
    key = '%s_coin_volume_%s'%(settings.CACHED_PREFIX, account.id)
    cache.set(key, volume, get_time_out_day())
