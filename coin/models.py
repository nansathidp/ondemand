from django.db import models
from django.conf import settings


class Leverage(models.Model):
    app = models.ForeignKey('app.App')
    value = models.FloatField()


class Coin(models.Model):
    from store.models import STORE_CHOICES
    
    TYPE_CHOICES = (
        (0, 'Basic'),
        (1, 'User Volume'),
    )
    
    name = models.CharField(max_length=120)
    desc = models.TextField(blank=True)
    type = models.IntegerField(choices=TYPE_CHOICES, default=0)
    volume_free = models.FloatField()
    volume = models.FloatField() #net
    price = models.FloatField()
    discount = models.FloatField(default=-1)
    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)
    
    is_allow_promote = models.BooleanField(default=True)
    is_use = models.BooleanField(default=True)

    sort = models.IntegerField(default=0, db_index=True)
    store = models.IntegerField(choices=STORE_CHOICES, db_index=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')
        ordering = ['sort']
        
    def __str__(self):
        return self.name

    def is_active(self):
        from django.utils import timezone
        now = timezone.now()
        if self.is_use is False:
            return False
        elif self.start is not None and self.end is not None:
            if self.start <= now <= self.end:
                return True
            else:
                return False
        elif self.start is not None:
            if self.start <= now:
                return True
            else:
                return False
        elif self.end is not None:
            if now <= self.end:
                return True
            else:
                return False
        else:
            return True
        
    def get_net(self):
        if self.discount != -1:
            return self.discount
        else:
            return self.price


class Volume(models.Model):
    account = models.OneToOneField(settings.AUTH_USER_MODEL)
    volume = models.FloatField()

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')

    @staticmethod
    def pull(account):
        if account is None:
            return 0
        volume = Volume.objects.filter(account=account).first()
        if volume is None:
            volume = Volume.objects.create(account=account,
                                           volume=0.0)
            Log.push(account, 0, 0.0, 'Init: Volume.pull')
        return volume

    @staticmethod
    def push(app, account, add_volume):
        from .cached import cached_leverage, cached_coin_volume_update
        volume = Volume.objects.filter(account=account).first()
        leverage = cached_leverage(app)
        add_volume *= leverage.value
        if volume is None:
            volume = Volume.objects.create(account=account,
                                           volume=add_volume)
            Log.push(account, 1, add_volume, 'Buy: Init Volume')
        else:
            volume.volume += add_volume
            volume.save(update_fields=['volume'])
            Log.push(account, 1, add_volume, 'Buy: Add Volume')
        cached_coin_volume_update(account, volume)

    def check_volume(self, app):
        from .cached import cached_leverage, cached_coin_volume_update
        leverage = cached_leverage(app)
        volume = 0.0
        for log in Log.objects.filter(account_id=self.account_id).order_by('timestamp'):
            if log.type in [-2, 1]:
                volume += log.volume
            elif log.type in [-1, 2]:
                volume -= log.volume
        self.volume = volume
        self.save(update_fields=['volume'])
        cached_coin_volume_update(self.account, self)


class Log(models.Model):
    TYPE_CHOICES = (
        (-2, 'Cancel Use Coin'),
        (-1, 'Cancel Buy Coin'),
        (0, 'init'),
        (1, 'Buy Coin'),
        (2, 'Use Coin'),
    )

    account = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='coin_log_set')
    type = models.IntegerField(choices=TYPE_CHOICES)
    volume = models.FloatField()
    note = models.TextField(blank=True)
    is_cancel = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        default_permissions = ('view', 'view_own', 'add', 'change', 'delete')
        ordering = ['-timestamp']

    @staticmethod
    def push(account, type, volume, note):
        log = Log.objects.create(account=account,
                                 type=type,
                                 volume=volume,
                                 note=note)
